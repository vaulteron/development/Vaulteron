using VaulteronDatabase.Models;

namespace VaulteronUtilities.Authorization;

public static class RoleAuthorization
{
    public static bool IsAdmin(User user) => user.Admin;
}