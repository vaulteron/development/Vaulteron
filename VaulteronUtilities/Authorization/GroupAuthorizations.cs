﻿using System;
using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;

namespace VaulteronUtilities.Authorization;

public static class GroupAuthorizations
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mandatorGroups">Needs Mandator Groups by Extension Method .RecursiveBase(mId) !important</param>
    /// <param name="user">Needs user with .IncludeGroups() or .IncludeUserGroups() !important</param>
    /// <param name="groupId"></param>
    /// <param name="requiredRole"></param>
    /// <returns></returns>
        public static bool UserHasGroupAuthorization(List<Group> mandatorGroups, User user, Guid? groupId, GroupRole requiredRole = GroupRole.GroupViewer)
    {
        if (user.Admin) return true;
        if (!groupId.HasValue) return user.Admin;

        var role = GetGroupRoleForUserInGroup(mandatorGroups, user.Id, groupId.Value);
        return role.HasValue && CheckRole(role.Value, requiredRole);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mandatorGroups">Needs Mandator Groups by Extension Method .RecursiveBase(mId) !important</param>
    /// <param name="user">Needs user with .IncludeGroups() or .IncludeUserGroups() !important</param>
    /// <param name="groupId"></param>
    /// <param name="requiredRole"></param>
    /// <returns></returns>
        public static bool UserHasGroupRoleAuthorization(List<Group> mandatorGroups, Guid userId, Guid groupId, GroupRole requiredRole = GroupRole.GroupViewer)
    {
        var role = GetGroupRoleForUserInGroup(mandatorGroups, userId, groupId);
        return role.HasValue && CheckRole(role.Value, requiredRole);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mandatorGroups">Needs Mandator Groups by Extension Method .RecursiveBase(mId) !important</param>
    /// <param name="user">Needs user with .IncludeGroups() or .IncludeUserGroups() !important</param>
    /// <param name="groupId"></param>
    /// <returns></returns>
    public static GroupRole? GetGroupRoleForUserInGroup(List<Group> mandatorGroups, Guid userId, Guid groupId)
    {
        var group = mandatorGroups
            .Single(g => g.Id == groupId);

        return GetGroupRoleRecursive(userId, group);
    }

    private static GroupRole? GetGroupRoleRecursive(Guid userId, Group group)
    {
        // When recursion results in empty group, user does not have a role
        if (group == null)
            return null;

        var role = group.UserGroups?
            .SingleOrDefault(ug => ug.UserId == userId);

        if (role != null)
        {
            // Role within current group
            return role.GroupRole;
        }

        // Role from parent-group
        return GetGroupRoleRecursive(userId, group.ParentGroup);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mandatorGroups">Needs Mandator Groups by Extension Method .RecursiveBase(mId) !important</param>
    /// <param name="user">Needs user with .IncludeGroups() or .IncludeUserGroups() !important</param>
    /// <param name="requiredRole"></param>
    /// <param name="includeSubGroups"></param>
    /// <param name="parentGroupId"></param>
    /// <param name="byArchived"></param>
        public static IEnumerable<Group> GetGroupsForUser(List<Group> mandatorGroups, User user, GroupRole requiredRole = GroupRole.GroupViewer,
            bool includeSubGroups = false, Guid? parentGroupId = null, bool? byArchived = false)
    {
        var groupsOfUser = GetRootGroupsForUser(user, mandatorGroups, requiredRole);

        if (!parentGroupId.IsNullOrEmpty())
        {
            groupsOfUser = IncludeSubGroupsRecursive(groupsOfUser)
                .Where(g => g.ParentGroupId == parentGroupId);
        }

        if (includeSubGroups)
        {
            groupsOfUser = IncludeSubGroupsRecursive(groupsOfUser);
        }

        return groupsOfUser
            .ByArchived(byArchived);
    }

        public static IEnumerable<Group> IncludeSubGroups(IEnumerable<Group> groups) => new HashSet<Group>(IncludeSubGroupsRecursive(groups));

    private static IEnumerable<Group> IncludeSubGroupsRecursive(IEnumerable<Group> groupsOfUser)
    {
        if (!groupsOfUser.Any()) return groupsOfUser;

        return groupsOfUser.Concat(
            groupsOfUser.SelectMany(g =>
                                        g.ChildGroups == null
                                            ? new List<Group>() // No entries when Children are null
                                            : IncludeSubGroupsRecursive(g.ChildGroups)
            )
        );
    }

    private static IEnumerable<Group> GetRootGroupsForUser(User user, List<Group> mandatorGroups, GroupRole requiredRole)
    {
        var groupsOfUser = user.Admin
            ? mandatorGroups.Where(mg => mg.ParentGroupId.IsNullOrEmpty()).ToList()
            : mandatorGroups.Where(g => g.UserGroups.Any(ug => ug.UserId == user.Id && CheckRole(ug.GroupRole, requiredRole))).ToList();

        return groupsOfUser
            .Where(g => !IsRecursiveChild(groupsOfUser, g));
    }

    private static bool IsRecursiveChild(IEnumerable<Group> groupsOfUser, Group group)
    {
        if (group.ParentGroup == null) return false;

        return groupsOfUser.Select(g => g.Id).Contains(group.ParentGroup.Id) || IsRecursiveChild(groupsOfUser, group.ParentGroup);
    }

    public static bool CheckRole(GroupRole groupAuthToCheck, GroupRole requiredRole)
    {
        return groupAuthToCheck switch
        {
            GroupRole.GroupAdmin => true,
            GroupRole.GroupEditor => requiredRole is GroupRole.GroupEditor or GroupRole.GroupViewer,
            GroupRole.GroupViewer => requiredRole == GroupRole.GroupViewer,
            _ => false
        };
    }
}