﻿using System;
using System.Collections.Generic;

namespace VaulteronUtilities.FilterTypes;

public class UserFilterType
{
    public List<Guid> ExcludeUsersInGroupIds { get; set; }
    public List<Guid> WithoutUserIds { get; set; }
    public List<Guid> GroupIds { get; set; }
    public string SearchTerm { get; set; }
    public bool? ByArchived { get; set; }
    public List<Guid> UserIds { get; set; }
}