﻿using System;

namespace VaulteronUtilities.FilterTypes;

public class SharedTagFilterType
{
    public string SearchTerm { get; set; }

    public Guid GroupId { get; set; }
}