﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace VaulteronUtilities.FilterTypes;

public class GroupFilterType
{
    [Description("The search term (string) to search for. Must match a part of the name.")]
    public string SearchTerm { get; set; }

    [Description("Limits the search to contain only groups that are a child of this parent group.")]
    public Guid? ParentId { get; set; }

    [Description("Limits the result to contain only groups with one of these IDs.")]
    public List<Guid> Ids { get; set; }

    [Description("If true it will disable the search parameter ParentId. Otherwise has no effect.")]
    public bool IgnoreParentIdArgument { get; set; } = false;


    [Description("If false active Groups, if true archived Groups, if null(or not set) active AND archived Groups are returned")]
    public bool? ByArchived { get; set; } = false;
}