﻿using System;
using System.Collections.Generic;

namespace VaulteronUtilities.FilterTypes;

public class AccountPasswordFilterType
{
    public string SearchTerm { get; set; }
    public List<Guid> TagIds { get; set; }
    public bool? ByArchived { get; set; }
}