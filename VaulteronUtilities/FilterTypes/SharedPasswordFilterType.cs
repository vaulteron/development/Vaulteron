﻿using System;
using System.Collections.Generic;

namespace VaulteronUtilities.FilterTypes;

public class SharedPasswordFilterType
{
    public string SearchTerm { get; set; }
    public List<Guid> GroupIds { get; set; }
    public List<Guid> CreatedByIds { get; set; }
    public bool? ByArchived { get; set; }
}