﻿using System;
using System.Threading.Tasks;

namespace VaulteronUtilities.Services.Interfaces;

public interface IEmailSenderService
{
    Task SendConfirmEmailAsync(string emailConfirmationToken, Guid registeredUserId, string targetEmail, string receiverName);
    Task SendEmailAfterUserAddedAsync(string emailConfirmationToken, string tempLoginUserPassword, Guid registeredUserId, string targetEmail, string receiverName);
    Task SendEmailAfterUserChangedPasswordAsync(Guid userGuid, string userEmail, string receiverName);
    Task SendEmailAddMultiFactorAuthenticationAsync(string userEmail, string receiverName);
    Task SendEmailRemoveMultiFactorAuthenticationAsync(string userEmail, string receiverName);
    Task SendEmailInformationThatUserEmailWasChanged(Guid targetUserId, string oldEmail, string newEmail, string receiverName, string changeEmailTokenAsync);
    Task SendEmailPasswordReset(string userEmail, Guid userId, string receiverName, string passwordResetToken, string question1, string question2, string question3);
    Task SendEmailPasswordResetUnavailable(string userEmail, string receiverName);
    Task SendScamMail(Guid currentUserId, string userEmail, string receiverName, string templateId, string senderName, string redirectURL);
}