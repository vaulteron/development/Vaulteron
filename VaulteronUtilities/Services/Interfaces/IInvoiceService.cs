﻿using System;
using VaulteronDatabase.Models;
using Stripe;

namespace VaulteronUtilities.Services.Interfaces;

public interface IInvoiceService
{
    string CreateCustomer(Mandator mandator);
    void CreateDefaultSubscription(string foreignInvoiceServiceCustomerId, string paymentMethodId, long quantity = long.MaxValue);
    string CustomerPortalUrl(string foreignInvoiceServiceCustomerId);
    string CheckoutUrl(string foreignInvoiceServiceCustomerId);
    string ClientSecret(string foreignInvoiceServiceCustomerId);
    void DeleteCustomer(string paymentProviderId);
    void UpdateSubscription(string foreignInvoiceServiceCustomerId, int amount);
    void UpdateDefaultPayment(string foreignInvoiceServiceCustomerId);
    Customer GetCustomer(string foreignInvoiceServiceCustomerId);
}