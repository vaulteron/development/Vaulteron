﻿using System.Threading.Tasks;

namespace VaulteronUtilities.Services.Interfaces;

public interface IPwnedService
{
    Task<string> GetBreach(string mail);
}