﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using VaulteronDatabase.Models;
using Stripe;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Services;

public class StripeInvoiceService : IInvoiceService
{
    private readonly IConfiguration config;

    private readonly string defaultPriceId;

    public StripeInvoiceService(IConfiguration config)
    {
        this.config = config;
        defaultPriceId = config.GetValue<string>("Stripe:DefaultCustomerSubscriptionPrice");
    }

    public string CreateCustomer(Mandator mandator)
    {
        var customerOptions = new CustomerCreateOptions
        {
            Name = mandator.Name,
            Address = new AddressOptions() {Country = "AT"}
        };
        var service = new CustomerService();
        var customer = service.Create(customerOptions);

        return customer.Id;
    }

    public string CheckoutUrl(string foreignInvoiceServiceCustomerId)
    {
        var options = new Stripe.Checkout.SessionCreateOptions
        {
            SuccessUrl = config.GetValue<string>("DomainUrl") + "/",
            CancelUrl = config.GetValue<string>("DomainUrl") + "/",
            Mode = "setup",
            Customer = foreignInvoiceServiceCustomerId,
            PaymentMethodTypes = new List<string>() {"card"},
            BillingAddressCollection = "required",
            CustomerUpdate = new Stripe.Checkout.SessionCustomerUpdateOptions() {Name = "auto", Address = "auto"}
        };
        var service = new Stripe.Checkout.SessionService();
        var session = service.Create(options);
        return session.Url;
    }

    public string ClientSecret(string foreignInvoiceServiceCustomerId)
    {
        var options = new SetupIntentCreateOptions()
        {
            Customer = foreignInvoiceServiceCustomerId,
            PaymentMethodTypes = new List<string>() { "card" }
        };

        var service = new SetupIntentService();
        var session = service.Create(options);

        return session.ClientSecret;
    }

    public void DeleteCustomer(string foreignInvoiceServiceCustomerId)
    {
        var service = new CustomerService();
        service.Delete(foreignInvoiceServiceCustomerId);
    }

    public void UpdateSubscription(string foreignInvoiceServiceCustomerId, int quantity)
    {
        var subscriptionListOptions = new SubscriptionListOptions
        {
            Customer = foreignInvoiceServiceCustomerId,
            Limit = 1,
        };
        var subscriptionService = new SubscriptionService();
        var subscriptions = subscriptionService.List(
            subscriptionListOptions);

        var subscription = subscriptions.FirstOrDefault();

        if (subscription == null)
        {
            throw new ApiException(ErrorCode.Forbidden, "No subscription found");
        }

        var items = new List<SubscriptionItemOptions>
        {
            new()
            {
                Id = subscription.Items.Data[0].Id,
                Quantity = quantity,
                Price = defaultPriceId
            },
        };
        var options = new SubscriptionUpdateOptions
        {
            CancelAtPeriodEnd = false,
            ProrationBehavior = "none",
            Items = items,
        };
        var service = new SubscriptionService();
        service.Update(subscription.Id, options);
    }

    public void UpdateDefaultPayment(string foreignInvoiceServiceCustomerId)
    {
        var customerService = new CustomerService();
        var customer = customerService.Get(foreignInvoiceServiceCustomerId);

        // Customer already has a default payment method
        if (!string.IsNullOrEmpty(customer.InvoiceSettings.DefaultPaymentMethodId))
        {
            return;
        }

        var paymentMethodService = new PaymentMethodService();
        var paymentMethods = paymentMethodService.ListAutoPaging(new PaymentMethodListOptions
        {
            Customer = foreignInvoiceServiceCustomerId,
            Type = "card"
        });

        // Check if he has any payment Methods
        if (!paymentMethods.Any())
        {
            return;
        }

        var paymentMethod = paymentMethods.First();
        customerService.Update(foreignInvoiceServiceCustomerId, new CustomerUpdateOptions
        {
            InvoiceSettings = new CustomerInvoiceSettingsOptions
            {
                DefaultPaymentMethod = paymentMethod.Id,
            }
        });
    }

    public void CreateDefaultSubscription(string foreignInvoiceServiceCustomerId, string paymentMethodId, long quantity = 0)
    {
        var paymentMethodService = new PaymentMethodService();
        paymentMethodService.Attach(
            paymentMethodId,
            new PaymentMethodAttachOptions
            {
                Customer = foreignInvoiceServiceCustomerId,
            });

        UpdateDefaultPayment(foreignInvoiceServiceCustomerId);

        var options = new SubscriptionCreateOptions
        {
            Customer = foreignInvoiceServiceCustomerId,
            DefaultPaymentMethod = paymentMethodId,

            Items = new List<SubscriptionItemOptions>
            {
                new()
                {
                    Price = defaultPriceId,
                    Quantity = quantity,
                }
            },
            AutomaticTax = new SubscriptionAutomaticTaxOptions() { Enabled = true },
        };

        var service = new SubscriptionService();
        service.Create(options);
    }

    public string CustomerPortalUrl(string foreignInvoiceServiceCustomerId)
    {
        var options = new Stripe.BillingPortal.SessionCreateOptions
        {
            Customer = foreignInvoiceServiceCustomerId,
            ReturnUrl = config.GetValue<string>("DomainUrl") + "/",
        };
        var service = new Stripe.BillingPortal.SessionService();
        var session = service.Create(options);

        return session.Url;
    }

    public Customer GetCustomer(string foreignInvoiceServiceCustomerId)
    {
        var service = new CustomerService();
        return service.Get(foreignInvoiceServiceCustomerId);
    }
}