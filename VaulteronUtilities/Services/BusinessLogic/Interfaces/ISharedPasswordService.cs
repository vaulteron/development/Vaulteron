﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface ISharedPasswordService
{
    Task<bool> UserHasAuthorisationForSharedPasswordAsync(Guid userId, Guid passwordId, GroupRole groupRole);
    bool UserHasAuthorisationForSharedPassword(User user, SharedPassword password, GroupRole groupRole);
    Task<ICollection<SharedPassword>> GetAllGroupPasswordsByUserIdAsync(Guid userId, bool? byArchived = false);
}