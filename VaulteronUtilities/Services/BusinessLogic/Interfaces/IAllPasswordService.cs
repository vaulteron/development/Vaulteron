﻿using System;
using System.Threading.Tasks;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface IAllPasswordService
{
    Task<Password> GetPasswordByIdAsync(Guid passwordId);
}