﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface IGroupService
{
    bool UserHasGroupRoleAuthorization(Guid userId, Guid groupId, GroupRole requiredRole);
    Task<bool> UserHasGroupAuthorizationAsync(Guid userId, Guid groupId, GroupRole requiredRole = GroupRole.GroupViewer);
    bool UserHasGroupAuthorization(User user, Guid groupId, GroupRole requiredRole = GroupRole.GroupViewer);
    Task<IEnumerable<Group>> GetGroupsForUserAsync(Guid userId, GroupRole requiredRole = GroupRole.GroupViewer, bool includeSubGroups = false,
        Guid? parentGroupId = null, bool? byArchived = false, List<Guid> filterGroupIds = null, string filterSearchTerm = null);
    IEnumerable<Group> GetGroupsForUser(User user, GroupRole requiredRole = GroupRole.GroupViewer, bool includeSubGroups = false,
        Guid? parentGroupId = null, bool? byArchived = false, List<Guid> filterGroupIds = null, string filterSearchTerm = null);
}