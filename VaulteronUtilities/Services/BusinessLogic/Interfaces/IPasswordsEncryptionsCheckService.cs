﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface IPasswordsEncryptionsCheckService
{
    bool EncryptionsProvidedForSharedPasswordsAndKeyPairs(IEnumerable<SharedPassword> passwords, IEnumerable<KeyPair> keys, ICollection<EncryptedSharedPassword> encryptedPasswords);
    bool EncryptionsProvidedForSharedPasswords(ICollection<SharedPassword> passwords, ICollection<EncryptedSharedPassword> encryptedPasswords);
    bool EncryptionsProvidedForAccountPasswords(ICollection<AccountPassword> passwords, ICollection<EncryptedAccountPassword> encryptedPasswords);
    bool EncryptionsProvidedForKeyPairs(ICollection<KeyPair> keyPairs, ICollection<EncryptedSharedPassword> encryptedPasswords);
    Task<bool> EncryptionsProvidedForGroupIncludingSubGroupsAsync(Guid groupId, ICollection<EncryptedSharedPassword> encryptedSharedPasswords);
}