﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface IUserService
{
    Task<bool> UserIsOfMandatorAsync(Guid userId, Guid mandatorId);
    Task<bool> UserIsSuperAdminAsync(Guid userId);
    Task<IEnumerable<User>> GetUsersByGroupIdAsync(Guid groupId, bool? byArchived = false);
    Task<IEnumerable<User>> GetUsersWithAccessToGroupIncludingAdminsAsync(Guid? baseGroupId);
    IEnumerable<User> GetUsersWithAccessToGroupExcludingAdmins(Guid? baseGroupId);
}