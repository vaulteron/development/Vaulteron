﻿using System;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Services.BusinessLogic.Interfaces;

public interface IMandatorService
{
    Task<bool> HasLicenceSlotsAvailableAsync();
    bool HasValidLicence();
    Task<Mandator> DeleteMandatorAsync(Guid mandatorId);
}