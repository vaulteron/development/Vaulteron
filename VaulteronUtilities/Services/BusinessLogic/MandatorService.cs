﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class MandatorService : IMandatorService
{
    private readonly ICurrentUserInfoService currentUserService;
    private readonly IUserRepository userRepository;
    private readonly VaulteronContext db;

    public MandatorService(ICurrentUserInfoService currentUserService, IUserRepository userRepository, VaulteronContext dbContext)
    {
        this.currentUserService = currentUserService;
        this.userRepository = userRepository;
        db = dbContext;
    }

    public async Task<bool> HasLicenceSlotsAvailableAsync()
    {
        var mandatorUsers = await userRepository.GetUsersAsync(false);
        var mandator = currentUserService.CurrentMandator;

        return HasValidLicence() && (mandatorUsers.Count < mandator.SubscriptionUserLimit || mandator.PaymentProviderId != "");
    }

    public bool HasValidLicence()
    {
        var mandator = currentUserService.CurrentMandator;
        return mandator.SubscriptionStatus is LicenceStatus.Trial or LicenceStatus.Standard;
    }

    public async Task<Mandator> DeleteMandatorAsync(Guid mandatorId)
    {
        var mandator = await db.Mandators.Base(mandatorId)
            .Include(m => m.Users)
            .Include(m => m.Groups)
            .Include(m => m.SharedPasswords)
            .Include(m => m.AccountPasswords)
            .SingleAsync();

        // db.AccountPasswordAccessLogs
        // db.AccountPasswordTags
        // db.EncryptedAccountPasswords
        // db.EncryptedSharedPasswords
        // db.PublicKeys
        // db.SharedPasswordAccessLogs
        // db.SharedPasswordTags
        // db.UserGroups
        // are not deleted manually: removed from DB by constraints

        db.RemoveRange(db.ChangeLogs.Where(cl => mandator.Users.Contains(cl.CreatedBy)));

        db.RemoveRange(db.AccountTags.Where(at => mandator.Users.Contains(at.Owner)));
        db.RemoveRange(db.SharedTags.Where(st => mandator.Groups.Contains(st.Group)));

        db.RemoveRange(mandator.AccountPasswords);
        db.RemoveRange(mandator.SharedPasswords);
        db.RemoveRange(mandator.Users);
        db.RemoveRange(mandator.Groups);

        db.Remove(mandator);
        return mandator;
    }
}