﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class PasswordsEncryptionsCheckService : IPasswordsEncryptionsCheckService
{
    private readonly IGroupRepository groupRepository;
    private readonly ISharedPasswordRepository sharedPasswordRepository;

    public PasswordsEncryptionsCheckService(IGroupRepository groupRepository, ISharedPasswordRepository sharedPasswordRepository)
    {
        this.groupRepository = groupRepository;
        this.sharedPasswordRepository = sharedPasswordRepository;
    }

    public async Task<bool> EncryptionsProvidedForGroupIncludingSubGroupsAsync(Guid groupId, ICollection<EncryptedSharedPassword> encryptedSharedPasswords)
    {
        var groupIds = groupRepository.GetGroupsIncludingSubGroups(new List<Guid> { groupId })
            .Select(g => g.Id)
            .ToList();

        var sharedPasswordsForWhichEncryptionsHaveToBeProvided = await sharedPasswordRepository.GetGroupPasswordsByGroupIdsAsync(groupIds, null);

        return EncryptionsProvidedForSharedPasswords(sharedPasswordsForWhichEncryptionsHaveToBeProvided, encryptedSharedPasswords);
    }

    // Helper methods (TODO refactoring: not to be public)

    public bool EncryptionsProvidedForSharedPasswords(ICollection<SharedPassword> passwords, ICollection<EncryptedSharedPassword> encryptedPasswords)
        => EncryptionsProvided(passwords, encryptedPasswords, enc => enc.SharedPasswordId);

    public bool EncryptionsProvidedForAccountPasswords(ICollection<AccountPassword> passwords, ICollection<EncryptedAccountPassword> encryptedPasswords)
        => EncryptionsProvided(passwords, encryptedPasswords, enc => enc.AccountPasswordId);

        private static bool EncryptionsProvided<T1, T2>(ICollection<T1> passwords, ICollection<T2> encryptedPasswords, Func<T2, Guid> encryptedPasswordsKeySelector) where T1 : Password
    {
        passwords ??= new List<T1>();
        encryptedPasswords ??= new List<T2>();

        var matching = passwords
            .Select(p => p.Id)
            .Intersect(encryptedPasswords.Select(encryptedPasswordsKeySelector))
            .ToList();

        return matching.Count == passwords.Count && matching.Count == encryptedPasswords.Count;
    }

    public bool EncryptionsProvidedForKeyPairs(ICollection<KeyPair> keyPairs, ICollection<EncryptedSharedPassword> encryptedPasswords)
    {
        keyPairs ??= new List<KeyPair>();
        encryptedPasswords ??= new List<EncryptedSharedPassword>();

        var matching = keyPairs
            .Select(p => p.Id)
            .Intersect(encryptedPasswords.Select(enc => enc.KeyPairId))
            .ToList();

        return matching.Count == keyPairs.Count && matching.Count == encryptedPasswords.Count;
    }

    public bool EncryptionsProvidedForSharedPasswordsAndKeyPairs(IEnumerable<SharedPassword> passwords,
        IEnumerable<KeyPair> keys, ICollection<EncryptedSharedPassword> encryptedPasswords)
    {
        passwords ??= new List<SharedPassword>();
        keys ??= new List<KeyPair>();
        encryptedPasswords ??= new List<EncryptedSharedPassword>();

        var expectedEncryptions = passwords
            .Join(keys, _ => true, _ => true, (p, pk) => (p.Id, pk.Id))
            .ToList();

        var matching = expectedEncryptions
            .Intersect(encryptedPasswords.Select(enc => (enc.SharedPasswordId, enc.KeyPairId)))
            .ToList();

        return matching.Count == expectedEncryptions.Count && matching.Count == encryptedPasswords.Count;
    }
}