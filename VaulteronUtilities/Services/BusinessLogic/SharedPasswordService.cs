﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class SharedPasswordService : ISharedPasswordService
{
    private readonly IUserRepository userRepository;
    private readonly IGroupService groupService;
    private readonly IGroupRepository groupRepository;
    private readonly ISharedPasswordRepository sharedPasswordRepository;

    public SharedPasswordService(IUserRepository userRepository, IGroupService groupService, IGroupRepository groupRepository,
        ISharedPasswordRepository sharedPasswordRepository)
    {
        this.userRepository = userRepository;
        this.groupService = groupService;
        this.groupRepository = groupRepository;
        this.sharedPasswordRepository = sharedPasswordRepository;
    }

    public async Task<bool> UserHasAuthorisationForSharedPasswordAsync(Guid userId, Guid passwordId, GroupRole groupRole)
    {
        var user = await userRepository
            .GetUserByIdAsync(userId);

        var password = await sharedPasswordRepository
            .GetSharedPasswordByIdAsync(passwordId);

        return UserHasAuthorisationForSharedPassword(user, password, groupRole);
    }

    public bool UserHasAuthorisationForSharedPassword(User user, SharedPassword password, GroupRole groupRole)
    {
        if (user.Admin)
            return true;
        if (password is null)
            return false;

        return IsGroupPassword(password)
            ? groupService.UserHasGroupRoleAuthorization(user.Id, password.GroupId!.Value, groupRole)
            : UserHasAuthorisationForCompanyPassword(user, password);
    }

    private static bool IsGroupPassword(SharedPassword password) => password.GroupId.HasValue;

    private static bool UserHasAuthorisationForCompanyPassword(User user, SharedPassword password) => user.Admin || password.CreatedById == user.Id;

    public async Task<ICollection<SharedPassword>> GetAllGroupPasswordsByUserIdAsync(Guid userId, bool? byArchived = false)
    {
        var user = await userRepository.GetUserByIdAsync(userId);
        var groupsToView = groupRepository.GetGroupsForUser(user, includeSubGroups: true, byArchived: byArchived)
            .Select(g => g.Id)
            .ToList();
        return await sharedPasswordRepository.GetGroupPasswordsByGroupIdsAsync(groupsToView, false);
    }

}