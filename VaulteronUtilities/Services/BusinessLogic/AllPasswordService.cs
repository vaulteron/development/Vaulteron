﻿using System;
using System.Threading.Tasks;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class AllPasswordService : IAllPasswordService
{
    private readonly IAccountPasswordRepository accountPasswordRepository;
    private readonly ISharedPasswordRepository sharedPasswordRepository;

    public AllPasswordService(IAccountPasswordRepository accountPasswordRepository, ISharedPasswordRepository sharedPasswordRepository)
    {
        this.accountPasswordRepository = accountPasswordRepository;
        this.sharedPasswordRepository = sharedPasswordRepository;
    }

    public async Task<Password> GetPasswordByIdAsync(Guid passwordId)
    {
        return (Password)
            await accountPasswordRepository.GetAccountPasswordByIdAsync(passwordId)
            ?? await sharedPasswordRepository.GetSharedPasswordByIdAsync(passwordId);
    }
}