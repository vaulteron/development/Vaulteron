﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class GroupService : IGroupService
{
    private readonly IGroupRepository groupRepository;
    private readonly IUserRepository userRepository;

    public GroupService(IGroupRepository groupRepository, IUserRepository userRepository)
    {
        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
    }

    public IEnumerable<Group> GetGroupsForUser(User user, GroupRole requiredRole = GroupRole.GroupViewer, bool includeSubGroups = false,
        Guid? parentGroupId = null, bool? byArchived = false, List<Guid> filterGroupIds = null, string filterSearchTerm = null)
    {
        var mandatorGroups = groupRepository.MandatorGroups;

        var groupsOfUser = GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, requiredRole, includeSubGroups, parentGroupId, byArchived);

        return groupsOfUser.AsQueryable()
            .ByIds(filterGroupIds)
            .BySearchTerm(filterSearchTerm)
            .Where(g => !g.IsArchived()
                       || // Archived Groups can only be seen by group-admins
                       UserHasGroupAuthorization(user, g.Id, GroupRole.GroupAdmin));
    }
    public async Task<IEnumerable<Group>> GetGroupsForUserAsync(Guid userId, GroupRole requiredRole = GroupRole.GroupViewer, bool includeSubGroups = false,
        Guid? parentGroupId = null, bool? byArchived = false, List<Guid> filterGroupIds = null, string filterSearchTerm = null)
    {
        var user = await userRepository.GetUserByIdAsync(userId);
        return GetGroupsForUser(user, requiredRole, includeSubGroups, parentGroupId, byArchived, filterGroupIds, filterSearchTerm);
    }

    public bool UserHasGroupAuthorization(User user, Guid groupId, GroupRole requiredRole = GroupRole.GroupViewer) =>
        user.Admin || UserHasGroupRoleAuthorization(user.Id, groupId, requiredRole);

    public async Task<bool> UserHasGroupAuthorizationAsync(Guid userId, Guid groupId, GroupRole requiredRole = GroupRole.GroupViewer)
    {
        var user = await userRepository.GetUserByIdAsync(userId);
        return UserHasGroupAuthorization(user, groupId, requiredRole);
    }

    public bool UserHasGroupRoleAuthorization(Guid userId, Guid groupId, GroupRole requiredRole)
    {
        var mandatorGroups = groupRepository.MandatorGroups;
        return GroupAuthorizations.UserHasGroupRoleAuthorization(mandatorGroups, userId, groupId, requiredRole);
    }

}