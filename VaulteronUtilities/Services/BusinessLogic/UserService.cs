﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using VaulteronDatabase.Models;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Services.BusinessLogic;

public class UserService : IUserService
{
    private readonly IUserRepository userRepository;
    private readonly IConfiguration config;
    private readonly IGroupRepository groupRepository;

    public UserService(IUserRepository userRepository, IConfiguration config, IGroupRepository groupRepository)
    {
        this.userRepository = userRepository;
        this.config = config;
        this.groupRepository = groupRepository;
    }

    public async Task<bool> UserIsSuperAdminAsync(Guid userId)
    {
        var callingUser = await userRepository.GetUserByIdAsync(userId);

        var superAdminUserName = JsonSerializer.Deserialize<List<string>>(config.GetSection("SuperAdminUserNames").Value);
        return superAdminUserName != null && superAdminUserName.Select(u => u.ToUpper()).Contains(callingUser.NormalizedEmail);
    }

    public async Task<bool> UserIsOfMandatorAsync(Guid userId, Guid mandatorId)
    {
        var callingUser = await userRepository.GetUserByIdAsync(userId);
        return callingUser.MandatorId == mandatorId;
    }

    public async Task<IEnumerable<User>> GetUsersByGroupIdAsync(Guid groupId, bool? byArchived = false)
    {
        return await userRepository
            .BaseQuery(false)
            .Where(u => u.UserGroups.Any(ug => ug.GroupId == groupId))
            .ToListAsync();
    }

    public async Task<IEnumerable<User>> GetUsersWithAccessToGroupIncludingAdminsAsync(Guid? baseGroupId)
    {
        var users = GetUsersWithAccessToGroupExcludingAdmins(baseGroupId);
        var admins = await userRepository.GetAdminsAsync();

        return users.Union(admins);
    }

    public IEnumerable<User> GetUsersWithAccessToGroupExcludingAdmins(Guid? baseGroupId)
    {
        var baseGroup = groupRepository.GetGroupById(baseGroupId);
        return UsersWithAccessRecursive(baseGroup).ToList();
    }

    private static IEnumerable<User> UsersWithAccessRecursive(Group g)
    {
        if (g == null) return new List<User>();
        if (g.UserGroups == null || !g.UserGroups.Any()) return UsersWithAccessRecursive(g.ParentGroup);
        return g.UserGroups
            .Select(ug => ug.User)
            .Union(UsersWithAccessRecursive(g.ParentGroup));
    }

}