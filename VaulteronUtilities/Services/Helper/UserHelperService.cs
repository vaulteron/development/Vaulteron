﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Services.Helper;

public class UserHelperService : IUserHelperService
{
    public IEnumerable<string> AreRegisterFieldsValid(User user)
    {
        var registerFailedReasons = new List<string>();
        if (user == null)
        {
            registerFailedReasons.Add("No user data submitted");
        }
        else
        {
            if (string.IsNullOrEmpty(user?.Firstname) || user.Firstname.Length < 2)
                registerFailedReasons.Add("The firstname should be at least 2 character long");
            if (string.IsNullOrEmpty(user?.Lastname) || user.Lastname.Length < 2)
                registerFailedReasons.Add("The lastname should be at least 2 character long");
        }

        try
        {
            if (user?.Email == null)
            {
                registerFailedReasons.Add("Please provide a value for 'Email'");
            }
            else
            {
                var checkedEmail = new MailAddress(user.Email).Address;
                if (checkedEmail != user.Email)
                    throw new ApiException(ErrorCode.InputInvalid, "Unknown reason, please verify that this email is valid");
            }
        }
        catch (Exception exception)
        {
            registerFailedReasons.Add("This is not a valid value for 'Email': " + exception.Message);
        }

        return registerFailedReasons;
    }

    bool IUserHelperService.IsPasswordValid(string password) => password.Length > 6; // TODO: Move hard coded values to resource file
}