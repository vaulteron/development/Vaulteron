﻿using System;
using Microsoft.AspNetCore.Http;
using VaulteronDatabase;

namespace VaulteronUtilities.Services.Helper;

public class CurrentUserClaimsService : ICurrentUserClaimsService
{
    private readonly IHttpContextAccessor httpContextAccessor;

    public CurrentUserClaimsService(IHttpContextAccessor httpContextAccessor)
    {
        this.httpContextAccessor = httpContextAccessor;
    }

    public Guid UserId => new Guid(httpContextAccessor.HttpContext?.User.Identity?.Name!);
}