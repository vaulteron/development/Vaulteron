﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Services.Helper;

public class CurrentUserInfoService : ICurrentUserInfoService
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserClaimsService currentUserIdService;

    public CurrentUserInfoService(VaulteronContext db, ICurrentUserClaimsService currentUserClaimsService)
    {
        this.db = db;
        currentUserIdService = currentUserClaimsService;
    }

    // Lifetime "Scoped" => once per client-request
    public Mandator CurrentMandator => GetCurrentUser().Mandator;
    public Guid CurrentMandatorId => GetCurrentUser().MandatorId;

    private User activeUserCache;
    public User GetCurrentUser(bool trackingEntity = false)
    {
        if (activeUserCache != null)
            return activeUserCache;

        var foundUser = activeUserCache = db.Users
            .CurrentUser(currentUserIdService.UserId, trackingEntity)
            .Include(u => u.Mandator)
            .Include(u=>u.WebautnCredentials)
            .IncludeGroups()
            .IncludeLastKeyPair()
            .SingleOrDefault();
        if (foundUser == null)
            throw new ApiException(ErrorCode.UnknownError, "No non-archived user was found for the current user");

        return activeUserCache = foundUser;
    }

    public Guid CurrentUserId => currentUserIdService.UserId;
}