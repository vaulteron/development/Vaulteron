﻿using System;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Services.Helper.Interfaces;

public interface ICurrentUserInfoService
{
    Guid CurrentUserId { get; }
    public User GetCurrentUser(bool trackingEntity = false);
    Guid CurrentMandatorId { get; }
    Mandator CurrentMandator { get; }
}