﻿using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Services.Helper.Interfaces;

public interface IUserHelperService
{
    IEnumerable<string> AreRegisterFieldsValid(User user);
    bool IsPasswordValid(string password);
}