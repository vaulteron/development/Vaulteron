﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Services;

public class EMailSenderService : IEmailSenderService
{
    private readonly IConfiguration config;
    private readonly ILogger<EMailSenderService> logger;
    private readonly string apiKey;
    private readonly string serverHost;
    private readonly bool isDevelopmentEnvironment;
    private const string VaulteronSenderEmail = "office@vaulteron.com";
    private const string VaulteronSenderName = "Vaulteron";

    public EMailSenderService(IConfiguration config, ILogger<EMailSenderService> logger, IHostEnvironment env)
    {
        this.config = config;
        this.logger = logger;
        apiKey = config.GetSection("SendGrid:ApiKey").Value;
        serverHost = config.GetSection("DomainUrl").Value;
        isDevelopmentEnvironment = env.IsDevelopment();
    }

    public async Task SendConfirmEmailAsync(string emailConfirmationToken, Guid registeredUserId, string targetEmail, string receiverName)
    {
        var confirmationUrl = ConversionHelper.ConstructUrlWithQueryParameters(
            $"{serverHost}/emailConfirmation",
            new Dictionary<string, string>
            {
                { "token", emailConfirmationToken },
                { "userId", registeredUserId.ToString() }
            }
        );

        if (isDevelopmentEnvironment)
        {
            logger.LogInformation($"Generated confirmation URL: {confirmationUrl}\n(before encoding: {emailConfirmationToken}");
        }

        var dynamicTemplateData = new ConfirmEmailTemplateData { Link = confirmationUrl };

        var templateId = config.GetSection("SendGrid:TemplateIDConfirmEmailAddress").Value;
        await SendMail(targetEmail, receiverName, templateId, dynamicTemplateData);
    }

    public async Task SendEmailAfterUserAddedAsync(string emailConfirmationToken, string tempLoginUserPassword, Guid registeredUserId, string targetEmail, string receiverName)
    {
        var confirmationUrl = ConversionHelper.ConstructUrlWithQueryParameters(
            $"{serverHost}/activate-account",
            new Dictionary<string, string>
            {
                { "activationCode", emailConfirmationToken },
                { "loginPassword", tempLoginUserPassword },
                { "userId", registeredUserId.ToString() },
                { "mail", targetEmail },
            }
        );

        if (isDevelopmentEnvironment)
        {
            logger.LogInformation($"Generated confirmation URL: {confirmationUrl}\n(before encoding: token[{emailConfirmationToken}] password[{tempLoginUserPassword}])");
        }

        var dynamicTemplateData = new ConfirmEmailTemplateData { Link = confirmationUrl };

        var templateId = config.GetSection("SendGrid:TemplateIDAccountWasCreated").Value;
        await SendMail(targetEmail, receiverName, templateId, dynamicTemplateData);
    }

    public async Task SendEmailInformationThatUserEmailWasChanged(Guid targetUserId, string oldEmail, string newEmail, string receiverName, string changeEmailTokenAsync)
    {
        #region Send information to old email address

        var dynamicTemplateData = new EmailWasChangedInformationTemplateData { NewEmail = newEmail };
        var templateId = config.GetSection("SendGrid:TemplateIDEmailWasChangedInformation").Value;
        await SendMail(oldEmail, receiverName, templateId, dynamicTemplateData);

        #endregion

        #region Send activation link to new email address

        var confirmationUrl = ConversionHelper.ConstructUrlWithQueryParameters(
            $"{serverHost}/emailChangedVerification",
            new Dictionary<string, string>
            {
                { "token", changeEmailTokenAsync },
                { "newEmail", newEmail },
                { "userId", targetUserId.ToString() },
            });

        if (isDevelopmentEnvironment)
        {
            logger.LogInformation($"Generated confirmation URL: {confirmationUrl}");
        }

        var dynamicTemplateData2 = new ConfirmEmailTemplateData { Link = confirmationUrl };

        var templateId2 = config.GetSection("SendGrid:TemplateIDEmailWasChangedConfirmationLink").Value;
        await SendMail(newEmail, receiverName, templateId2, dynamicTemplateData2);

        #endregion
    }

    public async Task SendEmailPasswordReset(string userEmail, Guid userId, string receiverName, string passwordResetToken, string question1, string question2, string question3)
    {
        var templateId = config.GetSection("SendGrid:TemplateIdPasswordReset").Value;

        var confirmationUrl = ConversionHelper.ConstructUrlWithQueryParameters(
            $"{serverHost}/resetPassword",
            new Dictionary<string, string>
            {
                { "resetToken", passwordResetToken },
                { "userId", userId.ToString() },
                { "email", userEmail },
                { "q1", question1 },
                { "q2", question2 },
                { "q3", question3 },
            }
        );

        var dynamicTemplateData = new ConfirmEmailTemplateData { Link = confirmationUrl };

        await SendMail(userEmail, receiverName, templateId, dynamicTemplateData);
    }

    public async Task SendEmailPasswordResetUnavailable(string userEmail, string receiverName)
    {
        var templateId = config.GetSection("SendGrid:TemplateIdPasswordResetUnavailable").Value;
        await SendMail(userEmail, receiverName, templateId);
    }

    public async Task SendScamMail(Guid currentUserId, string userEmail, string receiverName, string templateId, string senderName, string redirectURL)
    {
        var confirmationUrl = ConversionHelper.ConstructUrlWithQueryParameters(
            $"{serverHost}/log",
            new Dictionary<string, string>
            {
                { "userId", currentUserId.ToString() },
                { "email", userEmail },
                { "r", redirectURL }
            }
        );

        var dynamicTemplateData = new ConfirmEmailTemplateData { Link = confirmationUrl };


        await SendMail(userEmail, receiverName, templateId, dynamicTemplateData, "no-reply@vaulteron.com", senderName);
    }

    public async Task SendEmailAfterUserChangedPasswordAsync(Guid userGuid, string userEmail, string receiverName)
    {
        var templateId = config.GetSection("SendGrid:TemplateIDLoginPasswordWasChanged").Value;
        await SendMail(userEmail, receiverName, templateId);
    }

    public async Task SendEmailAddMultiFactorAuthenticationAsync(string userEmail, string receiverName)
    {
        var templateId = config.GetSection("SendGrid:TemplateIdAddTwoFactorAuth").Value;
        await SendMail(userEmail, receiverName, templateId);
    }

    public async Task SendEmailRemoveMultiFactorAuthenticationAsync(string userEmail, string receiverName)
    {
        var templateId = config.GetSection("SendGrid:TemplateIdRemoveTwoFactorAuth").Value;
        await SendMail(userEmail, receiverName, templateId);
    }

    #region helper

    private async Task SendMail(string receiverEmail, string receiverDisplayedName, string templateId, object templateData = null,
        string senderEmail = VaulteronSenderEmail,
        string senderName = VaulteronSenderName)
    {
        var client = new SendGridClient(apiKey);
        var msg = new SendGridMessage();
        msg.SetFrom(new EmailAddress(senderEmail, senderName));
        msg.AddTo(new EmailAddress(receiverEmail, receiverDisplayedName));
        msg.SetTemplateId(templateId);
        if (templateData != null) msg.SetTemplateData(templateData);

        logger.LogInformation(isDevelopmentEnvironment
            ? $"Sending Email to {receiverEmail} ({receiverDisplayedName}) with templateId {templateId} and templateData: {templateData}"
            : $"Sending Email to {receiverEmail} ({receiverDisplayedName}) with templateId {templateId}");

        msg.SetClickTracking(false, false); // Otherwise URLs are replaced by sendgrid tracking ones

        var response = await client.SendEmailAsync(msg);

        if (response.StatusCode != HttpStatusCode.Accepted)
        {
            var error = await response.Body.ReadAsStringAsync();
            logger.LogError(error);
            throw new ApiException(ErrorCode.UnableToSendEmail, "Unable to send email", new[] { error.Replace('"', '\'') });
        }
    }

    #endregion
}

internal class EmailWasChangedInformationTemplateData
{
    [JsonProperty("newEmail")] public string NewEmail { get; set; }
}

internal class ConfirmEmailTemplateData
{
    [JsonProperty("link")] public string Link { get; set; }
    [JsonProperty("text")] public string Text { get; set; }

}