﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Services;

public class PwnedService : IPwnedService
{

    private readonly IConfiguration config;

    public PwnedService(IConfiguration config)
    {
        this.config = config;
    }

    public async Task<string> GetBreach(string mail)
    {
        var client = new HttpClient();
        var apiKey = config.GetSection("Haveibeenpwned:ApiKey").Value;
        client.DefaultRequestHeaders.Add("hibp-api-key", apiKey);
        client.DefaultRequestHeaders
            .UserAgent
            .TryParseAdd("Vaulteron");

        var response = await client.GetAsync("https://haveibeenpwned.com/api/v3/breachedaccount/" + mail + "?truncateResponse=false");
        var content = await response.Content.ReadAsStringAsync();
        return content;
    }
}