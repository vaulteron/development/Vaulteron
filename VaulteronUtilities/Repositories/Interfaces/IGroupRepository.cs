﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IGroupRepository
{
    List<Group> MandatorGroups { get; }

    IEnumerable<Group> GetGroupsIncludingSubGroups(List<Guid> guids);
    Group GetGroupById(Guid? id);
    IEnumerable<Group> GetGroupsForUser(User user, bool includeSubGroups, bool? byArchived);
}