﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IAccountEncryptionsRepository
{
    Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsForAccountPasswordAsync(AccountPassword password);
}