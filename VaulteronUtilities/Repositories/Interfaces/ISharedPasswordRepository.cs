﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface ISharedPasswordRepository
{
    Task<ICollection<SharedPassword>> GetGroupPasswordsByGroupIdsAsync(List<Guid> groupIds, bool? byArchived = false);
    Task<SharedPassword> GetSharedPasswordByIdAsync(Guid passwordId);
    Task<IEnumerable<SharedPassword>> GetSharedPasswordsByGroupIdAsync(Guid groupId);
}