﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface ISharedEncryptionsRepository
{
    public void AddEncryptionsToDatabaseAndIgnoreExistingOnes(ICollection<EncryptedSharedPassword> encryptions,
        ICollection<Guid> keyPairIds);

    public void AddEncryptionsToDatabaseAndIgnoreExistingOnes(ICollection<EncryptedSharedPassword> encryptions,
        Guid singleKeyPairId);
    Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsByUserAndSharedPasswordAsync(User user, SharedPassword password);
    Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsByUserAndSharedPasswordAsync(Guid userId, Guid passwordId);
}