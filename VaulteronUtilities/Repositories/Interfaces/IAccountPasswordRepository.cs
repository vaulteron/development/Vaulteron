﻿using System;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IAccountPasswordRepository
{
    Task<AccountPassword> GetAccountPasswordByIdAsync(Guid passwordId);
}