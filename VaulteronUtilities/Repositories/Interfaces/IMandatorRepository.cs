﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IMandatorRepository
{
    Task<ICollection<Mandator>> GetAllMandatorsAsync();
}