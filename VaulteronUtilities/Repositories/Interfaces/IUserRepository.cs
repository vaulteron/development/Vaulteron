﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IUserRepository
{
    Task<ICollection<User>> GetUsersAsync(bool? byArchived = null);
    Task<User> GetUserByIdAsync(Guid userId, bool trackingEntity = false);
    Task<KeyPair> GetLastActiveKeyPairByUserIdAsync(Guid userId);
    IQueryable<User> BaseQuery(bool? byArchived);
    Task<bool> UserIsAdminAsync(Guid userId);
    Task<IEnumerable<User>> GetAdminsAsync();
}