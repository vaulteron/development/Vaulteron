﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Repositories.Interfaces;

public interface IKeyPairRepository
{
    Task<ICollection<KeyPair>> GetKeyPairsForUserIdWithinTimeAsync(Guid userId, DateTime? from, DateTime? to);
}