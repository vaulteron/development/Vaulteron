﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class UserRepository : IUserRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserInfoService;

    public UserRepository(VaulteronContext vaulteronContext, ICurrentUserInfoService currentUserInfoService)
    {
        db = vaulteronContext;
        this.currentUserInfoService = currentUserInfoService;
    }

    public IQueryable<User> BaseQuery(bool? byArchived) => UserBase().ByArchived(byArchived);

    public async Task<KeyPair> GetLastActiveKeyPairByUserIdAsync(Guid userId)
    {
        if (currentUserInfoService.GetCurrentUser().Id == userId)
            return currentUserInfoService.GetCurrentUser().KeyPairs.LastActive();

        return await UserBase()
            .ById(userId)
            .Select(u => u.KeyPairs.LastActive())
            .SingleAsync();
    }

    public async Task<User> GetUserByIdAsync(Guid userId, bool trackingEntity = false)
    {
        if (!trackingEntity && currentUserInfoService.GetCurrentUser(trackingEntity).Id == userId)
            return currentUserInfoService.GetCurrentUser(trackingEntity);

        if (trackingEntity)
            return await UserBase(trackingEntity)
                .ById(userId)
                .SingleAsync();

        return await UserBase()
            .ById(userId)
            .SingleAsync();
    }

    public async Task<ICollection<User>> GetUsersAsync(bool? byArchived = null)
    {
        return await UserBase()
            .ByArchived(byArchived)
            .ToListAsync();
    }
    public async Task<IEnumerable<User>> GetAdminsAsync()
    {
        return await UserBase()
            .Where(u => u.Admin)
            .ToListAsync();
    }

    private IQueryable<User> UserBase(bool trackingEntity = false)
    {
        return db.Users
            .Base(currentUserInfoService.CurrentMandatorId, trackingEntity)
            .IncludeGroups()
            .Include(u => u.Mandator)
            .IncludeLastKeyPair();
    }

    public async Task<bool> UserIsAdminAsync(Guid userId) => (await GetUserByIdAsync(userId)).Admin;
}