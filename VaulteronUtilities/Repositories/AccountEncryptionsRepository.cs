﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class AccountEncryptionsRepository : IAccountEncryptionsRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserService;

    public AccountEncryptionsRepository(VaulteronContext db, ICurrentUserInfoService currentUserService)
    {
        this.db = db;
        this.currentUserService = currentUserService;
    }

    public async Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsForAccountPasswordAsync(AccountPassword password)
    {
        return await db.EncryptedAccountPasswords
            .Include(eap => eap.CreatedBy)
            .Where(eap => eap.KeyPair.UserId == currentUserService.CurrentUserId)
            .Where(eap => eap.AccountPassword.OwnerId == currentUserService.CurrentUserId)
            .Where(eap => eap.AccountPasswordId == password.Id)
            .OrderByDescending(eap => eap.CreatedAt)
            .Cast<EncryptedPassword>()
            .ToListAsync();
    }
}