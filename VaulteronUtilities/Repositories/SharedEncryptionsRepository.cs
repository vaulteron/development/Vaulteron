﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class SharedEncryptionsRepository : ISharedEncryptionsRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserService;

    public SharedEncryptionsRepository(VaulteronContext db, ICurrentUserInfoService currentUserService)
    {
        this.db = db;
        this.currentUserService = currentUserService;
    }

    public void AddEncryptionsToDatabaseAndIgnoreExistingOnes(ICollection<EncryptedSharedPassword> encryptions, Guid singleKeyPairId) =>
        ComplementEncryptions(encryptions, new List<Guid> { singleKeyPairId }, singleKeyPairId);

    public void AddEncryptionsToDatabaseAndIgnoreExistingOnes(ICollection<EncryptedSharedPassword> encryptions, ICollection<Guid> keyPairIds) =>
        ComplementEncryptions(encryptions, keyPairIds);

    public async Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsByUserAndSharedPasswordAsync(User user, SharedPassword password)
        => await GetEncryptedPasswordsByUserAndSharedPasswordAsync(user.Id, password.Id);
    public async Task<ICollection<EncryptedPassword>> GetEncryptedPasswordsByUserAndSharedPasswordAsync(Guid userId, Guid passwordId)
    {
        return await db.EncryptedSharedPasswords
            .Include(esp => esp.CreatedBy)
            .Where(esp => esp.SharedPassword.MandatorId == currentUserService.CurrentMandatorId)
            .Where(esp => esp.KeyPair.UserId == userId)
            .Where(esp => esp.SharedPasswordId == passwordId)
            .OrderByDescending(esp => esp.CreatedAt)
            .Cast<EncryptedPassword>()
            .ToListAsync();
    }

    private void ComplementEncryptions(ICollection<EncryptedSharedPassword> encryptions, ICollection<Guid> keyPairIds, Guid? singleKeyPairGuidToSet = null)
    {
        var sentPasswordIds = encryptions.Select(esp => esp.SharedPasswordId).ToList();

        var encryptionsForThesePasswordsInDb =
            db.EncryptedSharedPasswords
                .AsNoTracking()
                .Where(esp => sentPasswordIds.Contains(esp.SharedPasswordId))
                .Where(esp => keyPairIds.Contains(esp.KeyPairId));

        foreach (var enc in encryptions)
        {
            var keyPairIdToUse = singleKeyPairGuidToSet ?? enc.KeyPairId;
                if (encryptionsForThesePasswordsInDb.Any(e => e.SharedPasswordId == enc.SharedPasswordId && keyPairIdToUse == e.KeyPairId))
                continue; // This one is already present in DB

            db.EncryptedSharedPasswords.Add(new EncryptedSharedPassword
            {
                KeyPairId = keyPairIdToUse,
                SharedPasswordId = enc.SharedPasswordId,
                EncryptedPasswordString = enc.EncryptedPasswordString,
                CreatedById = enc.CreatedById,
            });
        }
    }
}