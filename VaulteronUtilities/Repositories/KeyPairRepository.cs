﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Repositories.Interfaces;

namespace VaulteronUtilities.Repositories;

public class KeyPairRepository : IKeyPairRepository
{
    private readonly VaulteronContext db;

    public KeyPairRepository(VaulteronContext db)
    {
        this.db = db;
    }

    public async Task<ICollection<KeyPair>> GetKeyPairsForUserIdWithinTimeAsync(Guid userId, DateTime? from, DateTime? to)
    {
        return await db.KeyPairs
            //.ByMandatorId(currentUserService.CurrentMandatorId) // This would secure the query to not get something from different Mandators, but causes an extra Query when its not already chached.
            .ByUserId(userId)
            .ByCreatedAtBetween(from, to)
            .OrderByDescending(kp => kp.CreatedAt)
            .ToListAsync();
    }
}