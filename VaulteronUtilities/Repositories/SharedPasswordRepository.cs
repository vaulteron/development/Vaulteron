﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class SharedPasswordRepository : ISharedPasswordRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserService;

    public SharedPasswordRepository(VaulteronContext db, ICurrentUserInfoService currentUserService)
    {
        this.db = db;
        this.currentUserService = currentUserService;
    }

    public async Task<IEnumerable<SharedPassword>> GetSharedPasswordsByGroupIdAsync(Guid groupId) => await GetGroupPasswordsByGroupIdsAsync(new List<Guid> { groupId });
    public async Task<ICollection<SharedPassword>> GetGroupPasswordsByGroupIdsAsync(List<Guid> groupIds, bool? byArchived = false)
    {
        return await db.SharedPasswords
            .ByMandatorId(currentUserService.CurrentMandatorId)
            .ByArchived(byArchived)
            .ByGroupPasswords()
            .Where(sp => groupIds.Contains(sp.GroupId!.Value))
            .ToListAsync();
    }

    public async Task<SharedPassword> GetSharedPasswordByIdAsync(Guid passwordId)
    {
        return await db.SharedPasswords
            .ByMandatorId(currentUserService.CurrentMandatorId)
            .ById(passwordId)
            .SingleOrDefaultAsync();
    }
}