﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Repositories.Interfaces;

namespace VaulteronUtilities.Repositories;

public class MandatorRepository : IMandatorRepository
{
    private readonly VaulteronContext db;

    public MandatorRepository(VaulteronContext db)
    {
        this.db = db;
    }

    public async Task<ICollection<Mandator>> GetAllMandatorsAsync() => await db.Mandators.ToListAsync();
}