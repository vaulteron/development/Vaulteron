﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class AccountPasswordRepository : IAccountPasswordRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserService;

    public AccountPasswordRepository(VaulteronContext db, ICurrentUserInfoService currentUserService)
    {
        this.db = db;
        this.currentUserService = currentUserService;
    }

    public async Task<AccountPassword> GetAccountPasswordByIdAsync(Guid passwordId)
    {
        return await db.AccountPasswords
            .ByMe(currentUserService.CurrentUserId)
            .ById(passwordId)
            .SingleOrDefaultAsync();
    }
}