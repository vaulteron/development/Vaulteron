﻿using System;
using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;

namespace VaulteronUtilities.Repositories;

public class GroupRepository : IGroupRepository
{
    private readonly VaulteronContext db;
    private readonly ICurrentUserInfoService currentUserInfoService;

    // Lifetime "Scoped" => once per client-request
    private readonly Dictionary<Guid, List<Group>> mandatorGroupsCache = new();

    public GroupRepository(VaulteronContext vaulteronContext, ICurrentUserInfoService currentUserInfoService)
    {
        db = vaulteronContext;
        this.currentUserInfoService = currentUserInfoService;
    }

    public IEnumerable<Group> GetGroupsIncludingSubGroups(List<Guid> guids)
    {
        var groups = MandatorGroups.Where(g => guids.Contains(g.Id));
        return GroupAuthorizations.IncludeSubGroups(groups);
    }

    public IEnumerable<Group> GetGroupsForUser(User user, bool includeSubGroups, bool? byArchived)
    {
        return GroupAuthorizations.GetGroupsForUser(MandatorGroups, user, includeSubGroups: includeSubGroups, byArchived: byArchived);
    }

    public Group GetGroupById(Guid? id) => id.HasValue ? MandatorGroups.ById(id.Value).SingleOrDefault() : null;

    public List<Group> MandatorGroups {
        get {
            var cachedGroups = mandatorGroupsCache.GetValueOrDefault(currentUserInfoService.CurrentMandatorId);
            if (cachedGroups != null) return cachedGroups;

            var mandatorGroups = db.Groups
                .RecursionBase(currentUserInfoService.CurrentMandatorId)
                .IncludeUserGroup()
                .IncludeUsersWithLastKeyPair()
                .ToList();
            mandatorGroupsCache.Add(currentUserInfoService.CurrentMandatorId, mandatorGroups);
            return mandatorGroups;
        }
    }

}