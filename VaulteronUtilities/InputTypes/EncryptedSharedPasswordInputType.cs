﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.InputTypes;

public sealed class EncryptedSharedPasswordInputType : InputObjectGraphType<EncryptedSharedPassword>
{
    public EncryptedSharedPasswordInputType()
    {
        Description = "EncryptedSharedPasswordType";
        Field(x => x.KeyPairId);
        Field(x => x.SharedPasswordId, true);
        Field(x => x.EncryptedPasswordString);
    }
}