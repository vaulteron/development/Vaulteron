﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.InputTypes;

public sealed class GroupInputType : InputObjectGraphType<Group>
{
    public GroupInputType()
    {
        Name = "GroupInputType";
        Field(x => x.Name);
        Field(x => x.ParentGroupId, nullable: true);
    }
}