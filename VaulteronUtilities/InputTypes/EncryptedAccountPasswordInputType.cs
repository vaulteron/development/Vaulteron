﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.InputTypes;

public sealed class EncryptedAccountPasswordInputType : InputObjectGraphType<EncryptedAccountPassword>
{
    public EncryptedAccountPasswordInputType()
    {
        Description = "EncryptedPasswordType";
        Field(x => x.KeyPairId);
        Field(x => x.EncryptedPasswordString);
    }
}