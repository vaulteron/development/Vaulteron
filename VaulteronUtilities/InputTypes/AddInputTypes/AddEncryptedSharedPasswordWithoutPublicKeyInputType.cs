﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddEncryptedSharedPasswordWithoutPublicKeyInputType : InputObjectGraphType<EncryptedSharedPassword>
{
    public AddEncryptedSharedPasswordWithoutPublicKeyInputType()
    {
        Field(x => x.EncryptedPasswordString);
        Field(x => x.SharedPasswordId);
    }
}