﻿using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Types;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddUserInputType : InputObjectGraphType<AddUserModel>
{
    public AddUserInputType()
    {
        Name = "AddUserInputType";

        Field(u => u.Firstname);
        Field(u => u.Lastname);
        Field(u => u.Email);
        Field(u => u.Admin);
        Field(u => u.ForeignObjectId, nullable: true);
        Field(u => u.GroupsToBeAddedTo, nullable: true);
        Field(u => u.PublicKey);
        Field(u => u.EncryptedPrivateKey);
        Field(u => u.NewTemporaryLoginPassword);
        Field(u => u.NewTemporaryLoginPasswordHashed);
        Field<NonNullGraphType<SexType>>(nameof(User.Sex));

        Field(u => u.EncryptedSharedPasswords, type: typeof(ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>))
            .Description("Encryptions for SharedPasswords of all groups the user will have access to, all of mandator if admin.");
    }
}