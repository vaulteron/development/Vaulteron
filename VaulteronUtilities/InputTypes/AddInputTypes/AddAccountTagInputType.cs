﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddAccountTagInputType : InputObjectGraphType<AddAccountTagModel>
{
    public AddAccountTagInputType()
    {
        Name = "AddAccountTagInputType";
        Field(x => x.Name);
        Field(x => x.Color);
    }
}