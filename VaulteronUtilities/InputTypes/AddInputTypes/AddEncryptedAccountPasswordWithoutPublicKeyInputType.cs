﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddEncryptedAccountPasswordWithoutPublicKeyInputType : InputObjectGraphType<EncryptedAccountPassword>
{
    public AddEncryptedAccountPasswordWithoutPublicKeyInputType()
    {
        Field(x => x.EncryptedPasswordString);
        Field(x => x.AccountPasswordId);
    }
}