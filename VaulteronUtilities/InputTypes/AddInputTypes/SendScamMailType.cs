﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public class SendScamMailType : InputObjectGraphType<SendScamEmailModel>
{
    public SendScamMailType()
    {
        Name = "SendScamMailType";
        Field(f => f.Mails);
        Field(f => f.TemplateId);
        Field(f => f.RedirectURL);
        Field(f => f.SenderName);
    }
}