﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddOfflinePasswordInputType : InputObjectGraphType<AddSharedPasswordModel>
{
    public AddOfflinePasswordInputType()
    {
        Name = "AddOfflinePasswordInputType";

        Field(x => x.Name);
        Field(x => x.Login, true);
        Field(x => x.WebsiteUrl, true);
        Field(x => x.Notes, true);

        Field(x => x.EncryptedPasswords, type: typeof(ListGraphType<EncryptedSharedPasswordInputType>))
            .Description("Encryptions for company-pw-owner and admins.");
    }
}