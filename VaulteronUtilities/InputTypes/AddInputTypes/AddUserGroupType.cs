﻿using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Types;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddUserGroupType : InputObjectGraphType<AddUserGroupModel>
{
    public AddUserGroupType()
    {
        Name = "AddUserGroupType";

        Field(u => u.UserId);
        Field(u => u.GroupId);
        Field<NonNullGraphType<GroupRoleType>>(nameof(UserGroup.GroupRole));
        Field(u => u.EncryptedSharedPasswords, type: typeof(ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>))
            .Description("Encryptions for all SharedPasswords user will have access to (from group and subgroups).");
    }
}