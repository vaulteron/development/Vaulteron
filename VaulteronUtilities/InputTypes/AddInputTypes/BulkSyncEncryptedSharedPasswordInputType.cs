﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class BulkSyncEncryptedSharedPasswordInputType : InputObjectGraphType<BulkSyncEncryptedSharedPasswordModel>
{
    public BulkSyncEncryptedSharedPasswordInputType()
    {
        Name = "BulkSyncEncryptedSharedPasswordInputType";
        Field(x => x.SharedPasswordToAddTo);
        Field<NonNullGraphType<ListGraphType<EncryptedSharedPasswordInputType>>>(nameof(BulkSyncEncryptedSharedPasswordModel.EncryptedPasswords));
    }
}