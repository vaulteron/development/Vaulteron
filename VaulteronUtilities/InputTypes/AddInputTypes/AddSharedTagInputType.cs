﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddSharedTagInputType : InputObjectGraphType<AddSharedTagModel>
{
    public AddSharedTagInputType()
    {
        Name = "AddSharedTagInputType";
        Field(x => x.Name);
        Field(x => x.GroupId);
        Field(x => x.Color);
    }
}