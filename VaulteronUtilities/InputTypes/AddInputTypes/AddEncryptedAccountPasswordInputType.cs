﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddEncryptedAccountPasswordInputType : InputObjectGraphType<AddEncryptedAccountPasswordModel>
{
    public AddEncryptedAccountPasswordInputType()
    {
        Name = "AddEncryptedAccountPasswordInputType";
        Field(x => x.EncryptedPasswordString);
        Field(x => x.KeyPairId);
        Field(x => x.AccountPasswordId);
    }
}