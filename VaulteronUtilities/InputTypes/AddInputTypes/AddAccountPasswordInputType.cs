﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddAccountPasswordInputType : InputObjectGraphType<AddAccountPasswordModel>
{
    public AddAccountPasswordInputType()
    {
        Name = "AddAccountPasswordInputType";
        Field(x => x.Name);
        Field(x => x.EncryptedPassword);
        Field(x => x.KeyPairId);
        Field(x => x.Login);
        Field(x => x.WebsiteUrl);
        Field(x => x.Notes);
        Field(x => x.Tags, true);
    }
}