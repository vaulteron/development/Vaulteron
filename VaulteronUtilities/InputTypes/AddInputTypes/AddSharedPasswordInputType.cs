﻿using GraphQL.Types;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.InputTypes.AddInputTypes;

public sealed class AddSharedPasswordInputType : InputObjectGraphType<AddSharedPasswordModel>
{
    public AddSharedPasswordInputType()
    {
        Name = "AddSharedPasswordInputType";

        Field(x => x.Name);
        Field<GuidGraphType>(nameof(AddSharedPasswordModel.GroupId));
        Field(x => x.Login, true);
        Field(x => x.WebsiteUrl, true);
        Field(x => x.Notes, true);
        Field(x => x.Tags, true);

        Field(x => x.EncryptedPasswords, type: typeof(ListGraphType<EncryptedSharedPasswordInputType>))
            .Description("Encryptions for all users (including admins) having access to the group the SharedPassword is added to.");
    }
}