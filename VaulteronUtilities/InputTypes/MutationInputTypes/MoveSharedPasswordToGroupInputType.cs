﻿using GraphQL.Types;
using VaulteronUtilities.Models.MutationModels;

namespace VaulteronUtilities.InputTypes.MutationInputTypes;

public class MoveSharedPasswordToGroupInputType : InputObjectGraphType<BulkMoveSharedPasswordModel>
{
    public MoveSharedPasswordToGroupInputType()
    {
        Field<NonNullGraphType<ListGraphType<GuidGraphType>>>(nameof(BulkMoveSharedPasswordModel.PasswordIds));
        Field(m => m.DestinationGroupId);
        Field(m => m.EncryptedPasswords, type: typeof(NonNullGraphType<ListGraphType<EncryptedSharedPasswordInputType>>))
            .Description("Encryptions for all users (including admins) having access to the group the SharedPassword is moved to");
    }
}