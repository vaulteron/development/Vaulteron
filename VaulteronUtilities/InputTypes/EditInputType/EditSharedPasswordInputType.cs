﻿using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditSharedPasswordInputType : InputObjectGraphType<EditSharedPasswordModel>
{
    public EditSharedPasswordInputType()
    {
        Name = "EditSharedPasswordInputType";
        Field(x => x.Id).Description("PasswordId to be updated");
        Field(x => x.Name, true);
        Field(x => x.Login, true);
        Field(x => x.WebsiteURL, true);
        Field(x => x.Notes, true);
        Field(x => x.Tags, true);
        Field(x => x.ModifyLock);

        Field(x => x.EncryptedPasswords, type: typeof(ListGraphType<EncryptedSharedPasswordInputType>))
            .Description("Encryptions for all users (including admins) having access to the group the SharedPassword is added to.");
    }
}