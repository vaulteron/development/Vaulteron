﻿using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditAccountTagInputType : InputObjectGraphType<EditAccountTagModel>
{
    public EditAccountTagInputType()
    {
        Name = "EditAccountTagInputType";
        Field(x => x.Id).Description("TagId to updated");
        Field(x => x.Name);
    }
}