﻿using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditUserInputType : InputObjectGraphType<EditUserModel>
{
    public EditUserInputType()
    {
        Name = "EditUserInputType";
        Field(u => u.Id, true);
        Field(u => u.Firstname, true);
        Field(u => u.Lastname, true);
        Field(u => u.Email, true);
        Field(f => f.ForeignObjectId, true);
        Field(f => f.SecurityQuestion1, true);
        Field(f => f.SecurityQuestion2,true);
        Field(f => f.SecurityQuestion3,true);
        Field(f => f.EncryptedLoginPassword,true);
    }
}