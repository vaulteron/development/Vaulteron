﻿using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditSharedTagInputType : InputObjectGraphType<EditSharedTagModel>
{
    public EditSharedTagInputType()
    {
        Name = "EditSharedTagInputType";
        Field(x => x.Id).Description("TagId to updated");
        Field(x => x.Name);
        Field(x => x.GroupId);
    }
}