﻿using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Types;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditUserGroupInputType : InputObjectGraphType<EditUserGroupModel>
{
    public EditUserGroupInputType()
    {
        Name = "EditUserGroupType";

        Field(u => u.UserId);
        Field(u => u.GroupId);
        Field<NonNullGraphType<GroupRoleType>>(nameof(UserGroup.GroupRole));
    }
}