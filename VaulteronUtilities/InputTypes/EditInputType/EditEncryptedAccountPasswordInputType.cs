﻿using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditEncryptedAccountPasswordInputType : InputObjectGraphType<EditEncryptedAccountPasswordModel>
{
    public EditEncryptedAccountPasswordInputType()
    {
        Name = "EditEncryptedAccountPasswordInputType";
        Field(x => x.EncryptedPasswordString);
        Field(x => x.KeyPairId);
        Field(x => x.AccountPasswordId);
    }
}