using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditAccountPasswordInputType : InputObjectGraphType<EditAccountPasswordModel>
{
    public EditAccountPasswordInputType()
    {
        Name = "EditAccountPasswordInputType";

        Field(x => x.Id).Description("PasswordId to updated");

        Field(x => x.Name);
        Field(x => x.Login);
        Field(x => x.WebsiteURL);
        Field(x => x.Notes);
        Field(x => x.Tags, true);
        Field(x => x.EncryptedPassword, true, type: typeof(EncryptedAccountPasswordInputType));
    }
}