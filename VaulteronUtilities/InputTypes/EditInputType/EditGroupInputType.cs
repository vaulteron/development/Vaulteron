﻿using GraphQL.Types;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.InputTypes.EditInputType;

public sealed class EditGroupInputType : InputObjectGraphType<EditGroupModel>
{
    public EditGroupInputType()
    {
        Name = "EditGroupInputType";

        Field(g => g.Id);
        Field(g => g.Name);
    }
}