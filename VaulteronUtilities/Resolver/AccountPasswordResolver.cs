﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Resolver;

public class AccountPasswordResolver : IAccountPasswordResolver
{
    private readonly IUserResolver userResolver;
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;

    public AccountPasswordResolver(VaulteronContext dbContext, IUserResolver userResolver, IUserRepository userRepository)
    {
        db = dbContext;
        this.userResolver = userResolver;
        this.userRepository = userRepository;
    }

    public async Task<List<AccountPassword>> GetAccountPasswordsAsync(Guid userId, AccountPasswordFilterType filter)
    {
        return await db.AccountPasswords
            .Include(up => up.Owner)
            .ByMe(userId)
            .ByArchived(filter.ByArchived)
            .BySearchTerm(filter.SearchTerm)
            .ByTagIds(filter.TagIds)
            .ToListAsync();
    }

    public async Task<AccountPassword> AddAccountPasswordAsync(Guid userId, AddAccountPasswordModel passwordForm)
    {
        if (string.IsNullOrWhiteSpace(passwordForm.Name) || string.IsNullOrWhiteSpace(passwordForm.EncryptedPassword))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");
        if (!db.KeyPairs.Any(kp => kp.Id == passwordForm.KeyPairId))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input", new List<string> { "this keyPair does not exist" });

        var baseUser = await userRepository.GetUserByIdAsync(userId, true);

        var newAccountPassword = new AccountPassword
        {
            Name = passwordForm.Name,
            Login = passwordForm.Login,
            WebsiteURL = passwordForm.WebsiteUrl,
            Notes = passwordForm.Notes,
            OwnerId = userId,
            MandatorId = baseUser.MandatorId,
        };
        await db.AccountPasswords.AddAsync(newAccountPassword);

        if (passwordForm.Tags != null && passwordForm.Tags.Count > 0)
        {
            var loadedTags = await db.AccountTags
                .ByMe(userId)
                .ByIds(passwordForm.Tags)
                .ToListAsync();

            var newAccountPasswordTags = loadedTags
                .Select(tag => new AccountPasswordTag { Password = newAccountPassword, Tag = tag })
                .ToList();

            await db.AccountPasswordTags.AddRangeAsync(newAccountPasswordTags);
        }

        var keyPair = await db.KeyPairs
            .Include(pk => pk.EncryptedAccountPasswords)
            .ByMandatorId(baseUser.MandatorId)
            .SingleAsync(pk => pk.Id == passwordForm.KeyPairId);
        var encryptedPassword = new EncryptedAccountPassword
        {
            KeyPairId = passwordForm.KeyPairId,
            EncryptedPasswordString = passwordForm.EncryptedPassword,
            AccountPassword = newAccountPassword,
            CreatedBy = baseUser
        };
        keyPair.EncryptedAccountPasswords.Add(encryptedPassword);
        newAccountPassword.EncryptedAccountPasswords = new List<EncryptedAccountPassword> { encryptedPassword };

        await db.SaveChangesAsync();
        return newAccountPassword;
    }

    public async Task<AccountPassword> EditAccountPasswordAsync(Guid userId, EditAccountPasswordModel passwordModelForm)
    {
        if (string.IsNullOrWhiteSpace(passwordModelForm.Name))
            throw new ApiException(ErrorCode.InputInvalid, "The name must not be empty");

        var loadedPassword = await db.AccountPasswords
            .ByMe(userId)
            .ByArchived(false)
            .ById(passwordModelForm.Id)
            .IncludeTags()
            .SingleOrDefaultAsync();

        if (loadedPassword == null) throw new ApiException(ErrorCode.DataNotFound, "This password does not exist.");

        if (passwordModelForm.EncryptedPassword != null)
        {
            var keyPair = await userRepository.GetLastActiveKeyPairByUserIdAsync(userId);
            if (passwordModelForm.EncryptedPassword.KeyPairId != keyPair.Id)
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, "Encryption not provided for latest key-pair.");

            db.EncryptedAccountPasswords
                .Add(new EncryptedAccountPassword
                {
                    AccountPassword = loadedPassword,
                    KeyPairId = passwordModelForm.EncryptedPassword.KeyPairId,
                    EncryptedPasswordString = passwordModelForm.EncryptedPassword.EncryptedPasswordString,
                    CreatedById = userId
                });
        }

        if (passwordModelForm.Tags != null)
        {
            var loadedTags = await db.AccountTags
                .ByMe(userId)
                .ByIds(passwordModelForm.Tags)
                .ToListAsync();
            if (loadedTags.Count != passwordModelForm.Tags.Count)
                throw new ApiException(ErrorCode.DataNotFound, "At least one of these tag guids does not exist");

            var newAccountPasswordTags = new List<AccountPasswordTag>();
            foreach (var accountTag in loadedTags)
            {
                var lookForAccountTag = loadedPassword.AccountPasswordTags.SingleOrDefault(t => t.Tag.Id == accountTag.Id);
                newAccountPasswordTags.Add(lookForAccountTag ?? new AccountPasswordTag { Tag = accountTag, Password = loadedPassword });
            }

            loadedPassword.AccountPasswordTags = newAccountPasswordTags;
        }

        loadedPassword.Name = passwordModelForm.Name;
        loadedPassword.Login = passwordModelForm.Login;
        loadedPassword.WebsiteURL = passwordModelForm.WebsiteURL;
        loadedPassword.Notes = passwordModelForm.Notes;

        db.AccountPasswords.Update(loadedPassword);
        await db.SaveChangesAsync();

        return loadedPassword;
    }

    public async Task<AccountPassword> RemoveAccountPasswordAsync(Guid userId, Guid passwordId)
    {
        var loadedPassword = await db.AccountPasswords
            .ByMe(userId)
            .ById(passwordId)
            .SingleOrDefaultAsync();

        if (loadedPassword == null) throw new ApiException(ErrorCode.DataNotFound, "This password does not exist");

        loadedPassword.ArchivedAt = DateTime.UtcNow;
        db.AccountPasswords.Update(loadedPassword);
        await db.SaveChangesAsync();

        return loadedPassword;
    }

    public async Task<IEnumerable<AccountPasswordAccessLog>> GetAccessLogForAccountPasswordAsync(Guid userId, Guid passwordId)
    {
        var accessLogs = await db.AccountPasswordAccessLogs
            .Where(u => u.AccountPassword.OwnerId == userId)
            .Where(p => p.AccountPasswordId == passwordId)
            .ToListAsync();

        return accessLogs;
    }

    public async Task<string> GetEncryptedStringForAccountPasswordAsync(Guid userId, Guid passwordId, Guid keyPairId = default, bool? byArchived = false)
    {
        var userCurrentKeyPairId = keyPairId != Guid.Empty
            ? keyPairId
            : (await userResolver.GetKeyPairAsync(userId, userId)).Id;

        db.AccountPasswordAccessLogs.Add(new AccountPasswordAccessLog
        {
            AccountPasswordId = passwordId,
            Agent = Agent.Web
        });

        var accountPassword = await db.AccountPasswords
            .Include(ap => ap.Owner)
            .Include(ap => ap.EncryptedAccountPasswords)
            .ByMe(userId)
            .ByArchived(byArchived)
            .SingleOrDefaultAsync(ap => ap.Id == passwordId);

        var encryptedString = accountPassword?.EncryptedAccountPasswords
            .OrderByDescending(eap => eap.CreatedAt)
            .First(ep => ep.KeyPairId == userCurrentKeyPairId)?
            .EncryptedPasswordString;

        if (encryptedString == null)
            throw new ApiException(ErrorCode.DataNotFound);

        await db.SaveChangesAsync();

        return encryptedString;
    }

    public async Task<IEnumerable<Tag>> GetTagsForAccountPasswordAsync(Guid userId, Guid passwordId)
    {
        var pw = await db.AccountPasswords
            .Where(p => p.OwnerId == userId)
            .Include(t => t.AccountPasswordTags)
            .ThenInclude(t => t.Tag)
            .SingleAsync(p => p.Id == passwordId);

        return pw.AccountPasswordTags.Select(pwAccountPasswordTag => pwAccountPasswordTag.Tag).Cast<Tag>().ToList();
    }
}