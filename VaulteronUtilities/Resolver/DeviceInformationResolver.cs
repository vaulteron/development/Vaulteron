﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Resolver;

public class DeviceInformationResolver: IDeviceInformationResolver
{
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;
    private readonly IEmailSenderService emailSenderService;

    public DeviceInformationResolver(VaulteronContext dbContext, IUserRepository userRepository, IEmailSenderService emailSenderService)
    {
        db = dbContext;
        this.userRepository = userRepository;
        this.emailSenderService = emailSenderService;
    }

    public async Task<ICollection<DeviceInformation>> GetDeviceInformationAsync(Guid userId)
    {
        return await db.DeviceInformations.Where(d => d.ClientId == userId).OrderByDescending(d=>d.CreatedAt).ToListAsync();
    }

    public async Task<DeviceInformation> AddDeviceInformationAsync(AddDeviceInformationModel form)
    {
        var newDeviceInformationLogged = new DeviceInformation
        {
            ClientId = form.ClientId,
            Mail = form.Mail,
            JSONInput = form.JSONInput,
            IpAddress = form.IpAddress,
            CountryCode = form.CountryCode,
            CountryName = form.CountryName,
            City = form.City,
            CityLatLong = form.CityLatLong,
            Browser = form.Browser,
            BrowserVersion = form.BrowserVersion,
            DeviceBrand = form.DeviceBrand,
            DeviceModel = form.DeviceModel,
            DeviceFamily = form.DeviceFamily,
            Os = form.Os,
            OsVersion = form.OsVersion
        };

        db.DeviceInformations.Add(newDeviceInformationLogged);
        await db.SaveChangesAsync();

        return newDeviceInformationLogged;
    }

    public async Task<bool> SendScamEmailAsync(Guid userId, SendScamEmailModel form)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var scheduleTaskList = form.Mails.Select(mail =>
            emailSenderService.SendScamMail(currentUser.Id, mail, mail, form.TemplateId, form.SenderName, form.RedirectURL));

        await Task.WhenAll(scheduleTaskList);

        return true;
    }

    public  Task<DeviceInformation> RemoveDeviceInformationAsync(Guid userId, Guid deviceInformationId)
    {
        throw new NotImplementedException();
    } 
}