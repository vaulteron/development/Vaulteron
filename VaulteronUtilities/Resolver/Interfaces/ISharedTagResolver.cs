﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface ISharedTagResolver
{
    Task<IEnumerable<Tag>> GetTagsAsync(Guid userId, SharedTagFilterType filter);
    Task<SharedTag> AddSharedTagAsync(Guid userId, AddSharedTagModel tagForm);
    Task<SharedTag> EditSharedTagAsync(Guid userId, EditSharedTagModel tagModelForm);
    Task<SharedTag> RemoveSharedTagAsync(Guid userId, Guid tagId);
    Task<Group> GetGroupForSharedTagAsync(Guid userId, Guid tagId);

}