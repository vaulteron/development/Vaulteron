﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using Stripe;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IMandatorResolver
{
    Mandator GetMandator(Guid userId, Guid mandatorGuid);
    Mandator ArchiveMandator(Guid userId, Guid mandatorGuid);
    Task<Mandator> EditMandator(Guid userId, bool twoFactorRequired);
    Task<Mandator> DeleteMandatorAsync(Guid userId, Guid mandatorId);
    Task<ICollection<Mandator>> GetAllMandatorsAsync(Guid userId);

    // Stripe
    Task<Customer> GetCustomerInformationAsync(Guid userId);
    Task<string> GetPaymentProviderCustomerPortalLinkAsync(Guid userId);
    Task<string> GetPaymentProviderCheckoutSessionLinkAsync(Guid userId);
    Task<string> GetPaymentProviderClientSecretAsync(Guid userId);
    Task<Mandator> ChangePlan(SetupIntent setupIntent);

}
