﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Models.MutationModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface ISharedPasswordResolver
{
    Task<IEnumerable<SharedPassword>> GetGroupPasswordsAsync(Guid userId, SharedPasswordFilterType filter);

    Task<string> GetEncryptedStringForSharedPasswordAsync(Guid userId, Guid passwordId);
    Task<string> GetEncryptedStringForSharedPasswordAsync(Guid userId, SharedPassword sharedPassword);

    Task<SharedPassword> AddOfflinePasswordAsync(Guid userId, AddSharedPasswordModel passwordForm);
    Task<SharedPassword> AddCompanyPasswordAsync(Guid userId, AddSharedPasswordModel passwordForm, Guid? overrideCreatedBy = null);
    Task<SharedPassword> AddSharedPasswordAsync(Guid userId, AddSharedPasswordModel passwordForm);

    Task<SharedPassword> EditSharedPasswordAsync(Guid userId, EditSharedPasswordModel passwordModelForm);
    Task<SharedPassword> DeleteSharedPasswordAsync(Guid userId, Guid passwordId);
    Task<IEnumerable<SharedPasswordAccessLog>> GetAccessLogForSharedPasswordAsync(Guid userId, Guid passwordId);
    Task<IEnumerable<Tag>> GetTagsForSharedPasswordAsync(Guid userId, Guid passwordId);

    Task<SharedPassword> BulkSyncEncryptedPasswordAsync(Guid userId, BulkSyncEncryptedSharedPasswordModel bulkSyncEncryptedSharedPasswordModel);

    Task<IEnumerable<SharedPassword>> BulkMoveSharedPasswordsBetweenGroupsAsync(Guid userId, BulkMoveSharedPasswordModel movePasswordModel);

    Task<List<KeyPair>> GetMissingEncryptionsForPasswordAsync(Guid userId, Guid sharedPasswordId);

    Task<List<Guid>> GetMissingEncryptionsForUserAsync(Guid userId, Guid userWithMissingEncryptionsId);
    Task<IEnumerable<SharedPassword>> GetCompanyPasswordsAsync(Guid userId, SharedPasswordFilterType filter);
    Task<IEnumerable<SharedPassword>> GetOfflinePasswordsAsync(Guid userId, SharedPasswordFilterType filter);
    Task<User> GetCreatedByUserAsync(Guid userId, Guid createdById);

    Task<IEnumerable<SharedPassword>> BulkMoveGroupToCompanyPasswordsAsync(Guid userId, List<Guid> passwordsToMoveIds, Guid userToMoveToId, List<EncryptedSharedPassword> encryptedSharedPasswords);

    Task<IEnumerable<SharedPassword>> BulkMoveCompanyToGroupPasswordsAsync(Guid userId, List<Guid> passwordsToMoveIds, Guid groupToMoveToId, List<EncryptedSharedPassword> encryptedSharedPasswords);
}