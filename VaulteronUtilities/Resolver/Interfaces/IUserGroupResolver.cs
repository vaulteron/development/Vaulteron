﻿using System;
using System.Threading.Tasks;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IUserGroupResolver
{
    Task<Group> GetGroupAsync(Guid userId, Guid groupId, Guid userToLookupId);

    Task<User> GetUserAsync(Guid userId, Guid groupId, Guid userToLookupId);
}