﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IAccountPasswordResolver
{
    Task<List<AccountPassword>> GetAccountPasswordsAsync(Guid userId, AccountPasswordFilterType filter);
    Task<AccountPassword> AddAccountPasswordAsync(Guid userId, AddAccountPasswordModel passwordForm);
    Task<AccountPassword> EditAccountPasswordAsync(Guid userId, EditAccountPasswordModel passwordModelForm);
    Task<AccountPassword> RemoveAccountPasswordAsync(Guid userId, Guid passwordId);

    Task<IEnumerable<AccountPasswordAccessLog>> GetAccessLogForAccountPasswordAsync(Guid userId, Guid passwordId);
    Task<IEnumerable<Tag>> GetTagsForAccountPasswordAsync(Guid userId, Guid passwordId);
    Task<string> GetEncryptedStringForAccountPasswordAsync(Guid userId, Guid passwordId, Guid keyPairId = default, bool? byArchived = false);
}