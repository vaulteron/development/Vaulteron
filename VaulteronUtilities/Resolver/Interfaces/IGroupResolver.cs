﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IGroupResolver
{
    Task<IEnumerable<Group>> GetGroupsAsync(Guid userId, GroupFilterType filter);
    Task<IEnumerable<Group>> GetSubGroupsAsync(Guid userId, Guid groupId);
    Task<IEnumerable<User>> GetUsersAsync(Guid userId, Guid groupId);
    Task<IEnumerable<Password>> GetPasswordsAsync(Guid userId, Guid groupId);

    Task<int> CountUsersAsync(Guid userId, Guid groupId);
    Task<int> CountPasswordsAsync(Guid userId, Guid groupId);
    Task<int> CountSubGroupsAsync(Guid userId, Guid groupId);
    Task<int> CountTagsAsync(Guid userId, Guid groupId);

    // Group Tree
    Task<string> GetGroupTreeJsonAsync(Guid userId, GroupRole requiredRole, bool? byArchived);
    Task<IEnumerable<GroupTreeNode>> GetGroupTreeAsync(Guid userId, GroupRole requiredRole, bool? byArchived);

    // CRUD
    Task<Group> AddGroupAsync(Guid userId, Group groupForm);
    Task<Group> EditGroupAsync(Guid userId, EditGroupModel groupModel);
    Task<IEnumerable<Group>> ArchiveGroupAsync(Guid userId, Guid groupId, bool reactivate);
    Task<IEnumerable<Group>> RemoveGroupAsync(Guid userId, Guid groupId);

    Task<User> AddUserToGroupAsync(Guid userId, AddUserGroupModel userGroupModelForm);
    Task<User> EditUserInGroupAsync(Guid userId, EditUserGroupModel userGroupModelForm);
    Task<User> RemoveUserFromGroupAsync(Guid userId, Guid groupId, Guid userIdToRemove);
    Task<IEnumerable<User>> GetUsersWithAccessAsync(Guid userId, Guid baseGroupId);
    Task<IEnumerable<Tag>> GetTagsOfGroupAsync(Guid userId, Guid baseGroupId);
    Task<Group> MoveGroupAsync(Guid userId, Guid groupToMoveId, Guid? newParentGroupId, ICollection<EncryptedSharedPassword> encryptedSharedPasswords);
}