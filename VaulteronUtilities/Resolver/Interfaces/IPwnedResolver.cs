﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IPwnedResolver
{
    Task<string> GetBreach(Guid userId, string mail);
}