﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronUtilities.Models;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IChangeLogResolver
{
    Task<IEnumerable<GraphQLChangeLogModel>> GetChangesAsync(Guid entityId, string propertyName = null, DateTime? from = null, DateTime? to = null);
}