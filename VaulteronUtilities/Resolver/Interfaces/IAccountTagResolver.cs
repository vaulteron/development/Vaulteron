using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IAccountTagResolver
{
    Task<IEnumerable<AccountTag>> GetTagsAsync(Guid userId, AccountTagFilterType filter);
    Task<AccountTag> AddAccountTagAsync(Guid userId, AddAccountTagModel tagForm);
    Task<AccountTag> EditAccountTagAsync(Guid userId, EditAccountTagModel tagModelForm);
    Task<AccountTag> RemoveAccountTagAsync(Guid userId, Guid tagId);
}