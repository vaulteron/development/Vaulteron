﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronUtilities.Models.AddModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IDeviceInformationResolver
{
    Task<ICollection<DeviceInformation>> GetDeviceInformationAsync(Guid userId);
    Task<DeviceInformation> AddDeviceInformationAsync(AddDeviceInformationModel form);
    
    Task<bool> SendScamEmailAsync(Guid userId, SendScamEmailModel form);

    Task<DeviceInformation> RemoveDeviceInformationAsync(Guid userId, Guid deviceInformationId);
}