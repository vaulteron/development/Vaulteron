﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IAllPasswordResolver
{
    Task<ICollection<EncryptedPassword>> GetEncryptedPasswordChangeLogAsync(Guid userId, Guid passwordId);
    Task<IEnumerable<Password>> GetPasswordsByUrlAsync(Guid userId, string url);
}