﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fido2NetLib;
using VaulteronDatabase.Models;
using VaulteronUtilities.Models.MutationModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IAuthenticationResolver
{
    // Get User Credentials with login information
    Task<User> LoginAsync(string email, string password, bool isPersistent);
    Task<User> LdapLoginAsync(string email, string password, bool isPersistent);

    // Register a user
    Task<User> RegisterAsync(User user, string registerFormPassword);

    Task<User> ValidateEmailConfirmationTokenAsync(Guid userId, string confirmationModelToken);
    Task<User> ChangeEmailAndUsernameViaTokenAsync(Guid userId, string newEmail, string confirmationToken);

    Task<User> ChangeLoginPasswordAsync(Guid userId,
        string newPublicKeyString, string newEncryptedPrivateKey, string previousPasswordHashEncryptedWithPublicKey,
        string currentPassword, string newPassword,
        ICollection<EncryptedSharedPassword> encryptedSharedPasswords, ICollection<EncryptedAccountPassword> encryptedAccountPasswords);

    Task<(KeyPair, List<EncryptedSharedPassword>)> GetKeyPairAndSharedPasswordsViaEmailTokenAsync(Guid userId, string token, string oldPasswordHashed);

    Task<User> AddUserViaEmailTokenAsync(Guid userId, string token, string oldPassword, string newPassword,
        string publicKey, string encryptedPrivateKey, string previousPasswordHashEncryptedWithPublicKey, ICollection<EncryptedSharedPassword> encryptions);

    Task LogoutAsync();

    // 2FA - activate
    Task<ToptAuthenticatorKey> GetTotpAuthenticatorKeyAsync(Guid userId);
    Task<User> SetEnabledTotpAsync(Guid userId, string totpCode, bool enable);
    Task<WebauthnAttestationChallenge> GetWebAuthnChallengeAsync(Guid userId);
    Task<User> AddWebAuthnAsync(Guid userId, string rawAttestationResponse, string description);
    Task<User> RemoveWebAuthnAsync(Guid userId, Guid webAuthKeyId);
    
    
    // 2FA - login
    Task<User> TwoFATOTPLoginAsync(string totp, bool isPersistent, bool rememberClient);

    // 
    Task RequestEmailConfirmationAsync(Guid userId);
    Task<AssertionOptions> AssertionOptionsAsync();
    Task<User> LoginUsingWebauthnAssertion(AuthenticatorAssertionRawResponse assertionResponse, bool isPersistent);
    Task ResetPassword(string email);
    Task<string> GetEncryptedResetPasswordByToken(Guid userId, string token);
}