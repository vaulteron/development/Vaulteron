﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;

namespace VaulteronUtilities.Resolver.Interfaces;

public interface IUserResolver
{
    // Get user by id
    Task<User> GetUserAsync(Guid userId);

    // Get viewable user in company
    Task<List<User>> GetUsersAsync(Guid userId, UserFilterType filter);
    Task<List<User>> GetAdminsAsync(Guid userId);
    Task<Mandator> GetMandatorAsync(Guid userId);
    
    // Get additional information
    Task<bool> IsPasswordResetEnabled(Guid userId, Guid userIdToLookup);
    
    // Get viewable groups of a user
    Task<IEnumerable<Group>> GetGroupsAsync(Guid userId, Guid userIdToLookup);
    Task<IEnumerable<UserGroup>> GetUserGroupsAsync(Guid userId, Guid userIdToLookup);

    Task<KeyPair> GetKeyPairAsync(Guid userId, Guid userIdToLookup);
    Task<KeyPair> GetEncryptedPrivateKeyAsync(Guid userId, Guid userIdToLookup);

    // Add user
    Task<User> AddUserAsync(Guid userId, AddUserModel userModelForm);

    // Edit user
    Task<User> EditUserAsync(Guid userId, EditUserModel userModelForm);
    Task<User> GrantAdminPermissionsAsync(Guid userId, Guid targetUserId, ICollection<EncryptedSharedPassword> encryptions);
    Task<User> RevokeAdminPermissionsAsync(Guid userId, Guid targetUserId);

    // Archive user
    Task<User> ArchiveUserAsync(Guid userId, Guid userToArchiveId, bool reenable, ICollection<EncryptedSharedPassword> encryptedSharedPasswords = null);

    // Send Emails
    Task<User> ResendAccountActivationEmailAsync(Guid userId, Guid targetUserId, string publicKey, string encryptedPrivateKey, string newTempLoginPassword, string newTempLoginPasswordHashed,
        List<EncryptedSharedPassword> newEncryptions);

    // Get custom user-attributes
    Task<bool> GetIsSuperAdminAsync(Guid userId);
    Task<ICollection<KeyPair>> GetKeyPairChangeLogsAsync(Guid userId, DateTime? from, DateTime? to);
}