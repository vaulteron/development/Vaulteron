﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Resolver;

public class AccountTagResolver : IAccountTagResolver
{
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;

    public AccountTagResolver(VaulteronContext dbContext, IUserRepository userRepository)
    {
        db = dbContext;
        this.userRepository = userRepository;
    }

    public async Task<IEnumerable<AccountTag>> GetTagsAsync(Guid userId, AccountTagFilterType filter)
    {
        return await db.AccountTags
            .Base(userId)
            .BySearchTerm(filter.SearchTerm)
            .ToListAsync();
    }

    public async Task<AccountTag> AddAccountTagAsync(Guid userId, AddAccountTagModel tagForm)
    {
        if (string.IsNullOrEmpty(tagForm.Name))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        if (string.IsNullOrEmpty(tagForm.Color)) tagForm.Color = "rgb(184, 184, 184)";

        var baseUser = await userRepository.GetUserByIdAsync(userId);

        var newTag = new AccountTag
        {
            OwnerId = baseUser.Id,
            Name = tagForm.Name,
            Color = tagForm.Color
        };

        await db.AccountTags.AddAsync(newTag);

        await db.SaveChangesAsync();
        return newTag;
    }

    public async Task<AccountTag> EditAccountTagAsync(Guid userId, EditAccountTagModel tagModelForm)
    {
        if (string.IsNullOrEmpty(tagModelForm.Id.ToString()))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        var loadedTag = await db.AccountTags
            .Base(userId)
            .ById(tagModelForm.Id)
            .SingleAsync();

        loadedTag.Name = tagModelForm.Name;

        db.AccountTags.Update(loadedTag);
        await db.SaveChangesAsync();

        return loadedTag;
    }

    public async Task<AccountTag> RemoveAccountTagAsync(Guid userId, Guid tagId)
    {
        if (string.IsNullOrEmpty(tagId.ToString()))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        var loadedTag = await db.AccountTags
            .Base(userId)
            .ById(tagId)
            .SingleAsync();

        db.AccountTags.Remove(loadedTag);
        await db.SaveChangesAsync();

        return loadedTag;
    }
}