﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Resolver;

public class GroupResolver : IGroupResolver
{
    private readonly VaulteronContext db;

    private readonly IPasswordsEncryptionsCheckService encryptionsCheckService;
    private readonly ISharedEncryptionsRepository encryptionsRepository;
    private readonly IGroupService groupService;
    private readonly IUserService userService;
    private readonly IUserRepository userRepository;
    private readonly ISharedPasswordRepository sharedPasswordRepository;
    private readonly IGroupRepository groupRepository;

    public GroupResolver(VaulteronContext dbContext, IPasswordsEncryptionsCheckService encryptionsCheckService, ISharedEncryptionsRepository encryptionsRepository,
        IGroupService groupService, IUserRepository userRepository, IUserService userService,
        ISharedPasswordRepository sharedPasswordRepository, IGroupRepository groupRepository)
    {
        db = dbContext;

        this.encryptionsCheckService = encryptionsCheckService;
        this.encryptionsRepository = encryptionsRepository;
        this.groupService = groupService;
        this.userRepository = userRepository;
        this.userService = userService;
        this.sharedPasswordRepository = sharedPasswordRepository;
        this.groupRepository = groupRepository;
    }

    /// <summary>
    /// Renders root or child groups. Renders the root groups, if the parent id is null.
    /// </summary>
    /// <param name="userId">The user making the request</param>
    /// <param name="filter">The filter to be applied</param>
    /// <returns>List of groups that this user has access to and satisfy the filter requirements</returns>
    public async Task<IEnumerable<Group>> GetGroupsAsync(Guid userId, GroupFilterType filter)
    {
        var groupsForUser = await groupService.GetGroupsForUserAsync(
            userId,
            includeSubGroups: filter.IgnoreParentIdArgument,
            parentGroupId: filter.IgnoreParentIdArgument ? null : filter.ParentId,
            byArchived: filter.ByArchived,
            filterGroupIds: filter.Ids,
            filterSearchTerm: filter.SearchTerm
        );

        return groupsForUser;
    }

    public async Task<IEnumerable<User>> GetUsersAsync(Guid userId, Guid groupId)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "No Access to group");

        return await userService.GetUsersByGroupIdAsync(groupId);
    }

    public async Task<IEnumerable<Password>> GetPasswordsAsync(Guid userId, Guid groupId)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "No Access to group");

        return await sharedPasswordRepository.GetSharedPasswordsByGroupIdAsync(groupId);
    }

    public async Task<IEnumerable<Group>> GetSubGroupsAsync(Guid userId, Guid groupId)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "No Access to group");

        var groupsForUser = await groupService.GetGroupsForUserAsync(
            userId,
            includeSubGroups: false,
            parentGroupId: groupId,
            byArchived: false
        );

        return groupsForUser;
    }

    public async Task<int> CountUsersAsync(Guid userId, Guid groupId) => (await GetUsersAsync(userId, groupId)).Count();

    public async Task<int> CountPasswordsAsync(Guid userId, Guid groupId) => (await GetPasswordsAsync(userId, groupId)).Count();

    public async Task<int> CountSubGroupsAsync(Guid userId, Guid groupId) => (await GetSubGroupsAsync(userId, groupId)).Count();

    public async Task<int> CountTagsAsync(Guid userId, Guid groupId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var group = await db.Groups
            .Base(currentUser.MandatorId, null)
            .ById(groupId)
            .IncludeTags()
            .SingleAsync();

        return group.Tags.Count;
    }

    public async Task<string> GetGroupTreeJsonAsync(Guid userId, GroupRole requiredRole, bool? byArchived)
    {
        var groupTree = await GetGroupTreeAsync(userId, requiredRole, byArchived);
        var jsonString = JsonConvert.SerializeObject(groupTree, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, });
        return jsonString;
    }

    public async Task<IEnumerable<GroupTreeNode>> GetGroupTreeAsync(Guid userId, GroupRole requiredRole, bool? byArchived)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var rootGroups = groupService.GetGroupsForUser(currentUser, requiredRole, byArchived: byArchived);

        var groupTree = GetGroupTreeRecursive(rootGroups, userId, byArchived, currentUser.Admin);
        return groupTree;
    }

    private static IEnumerable<GroupTreeNode> GetGroupTreeRecursive(IEnumerable<Group> groups, Guid callingUserId, bool? byArchived = false, bool isAdmin = false)
    {
        return groups.Select(g =>
        {
            var userGroup = g.UserGroups.SingleOrDefault(ug => ug.UserId == callingUserId);
            return new GroupTreeNode(g.Id, g.Name, userGroup?.GroupRole ?? (isAdmin ? GroupRole.GroupAdmin : GroupRole.GroupViewer))
            {
                Children = GetGroupTreeRecursive(g.ChildGroups.ByArchived(byArchived), callingUserId, byArchived, isAdmin)
            };
        });
    }

    public async Task<Group> AddGroupAsync(Guid userId, Group groupForm)
    {
        if (string.IsNullOrEmpty(groupForm.Name))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (groupForm.ParentGroupId.IsNullOrEmpty() && !RoleAuthorization.IsAdmin(currentUser)
            || // Root Group can only be added by Admin 
            !groupForm.ParentGroupId.IsNullOrEmpty()
            && // Sub  Group can only be added by GroupAdmin 
            !groupService.UserHasGroupAuthorization(currentUser, groupForm.ParentGroupId!.Value, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Not authorized");

        var newGroup = new Group
        {
            MandatorId = currentUser.MandatorId,
            Name = groupForm.Name,
            ParentGroupId = groupForm.ParentGroupId
        };
        db.Groups.Add(newGroup);
        await db.SaveChangesAsync();

        return newGroup;
    }

    public async Task<Group> EditGroupAsync(Guid userId, EditGroupModel groupModel)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupModel.Id, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Not authorized to edit this group");

        var group = groupRepository.GetGroupById(groupModel.Id);
        group.Name = groupModel.Name;
        db.Update(group);

        await db.SaveChangesAsync();

        return group;
    }

    public async Task<Group> MoveGroupAsync(Guid userId, Guid groupToMoveId, Guid? newParentGroupId, ICollection<EncryptedSharedPassword> encryptedSharedPasswords)
    {
        var groupToMove = groupRepository.GetGroupById(groupToMoveId);
        var newParentGroup = newParentGroupId == null ? null : groupRepository.GetGroupById(newParentGroupId.Value);

        if (groupToMove == null
            || // Group to move does not exist,
            newParentGroupId.HasValue
            && newParentGroupId != Guid.Empty
            && // Not moved to root group and
            newParentGroup == null) // Given newParentGroupId does not exist
            throw new ApiException(ErrorCode.InputInvalid);

        // The newParentGroupId is not in a subtree of group
        if (IsChildOf(groupToMove, newParentGroup))
            throw new ApiException(ErrorCode.InputInvalid, "Cannot move group into subtree of the same group");

        static bool IsChildOf(Group parent, Group possibleChild)
        {
            if (possibleChild == null) return false;
            return possibleChild.ParentGroupId == parent.Id || IsChildOf(parent, possibleChild.ParentGroup);
        }

        // User needs GroupAdmin(or sys-Admin) for both Parent Groups (old & new)
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupToMove.ParentGroupId ?? Guid.Empty, GroupRole.GroupAdmin)
            || !newParentGroupId.HasValue && !await userRepository.UserIsAdminAsync(userId)
            || newParentGroupId.HasValue && !await groupService.UserHasGroupAuthorizationAsync(userId, newParentGroupId.Value, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        // Check encryptions
        var usersForWhichEncryptionsHaveToBeProvided =
            userService.GetUsersWithAccessToGroupExcludingAdmins(newParentGroupId)
                .Except(userService.GetUsersWithAccessToGroupExcludingAdmins(groupToMove.ParentGroupId));

        var keyPairsForWhichEncryptionsHaveToBeProvided =
            usersForWhichEncryptionsHaveToBeProvided
                .Select(u => u.KeyPairs.LastActive())
                .ToList();

        var sharedPasswordsForWhichEncryptionsHaveToBeProvided = newParentGroupId == Guid.Empty
            ? new List<SharedPassword>()
            : await db.SharedPasswords
                .ByGroupIds(GroupAuthorizations.IncludeSubGroups(new List<Group> {groupToMove})
                    .Select(g => g.Id)
                    .ToList())
                .ToListAsync();

        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswordsAndKeyPairs(
                sharedPasswordsForWhichEncryptionsHaveToBeProvided,
                keyPairsForWhichEncryptionsHaveToBeProvided,
                encryptedSharedPasswords))
        {
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // Update DB (ignore passwords that are sent for users that are already in target parent group(s) and have all encryptions)
        var sentPairKeyIds = encryptedSharedPasswords.Select(esp => esp.KeyPairId).ToList();
        encryptionsRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(encryptedSharedPasswords,
            sentPairKeyIds);

        groupToMove.ParentGroupId = newParentGroupId == Guid.Empty ? null : newParentGroupId;
        db.Update(groupToMove);
        await db.SaveChangesAsync();

        return groupToMove;
    }

    public async Task<IEnumerable<Group>> ArchiveGroupAsync(Guid userId, Guid groupId, bool reactivate)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var group = groupRepository.GetGroupById(groupId);

        if (group.ParentGroupId.IsNullOrEmpty() && !RoleAuthorization.IsAdmin(currentUser)
            || // Root Group can only be added by Admin 
            !group.ParentGroupId.IsNullOrEmpty()
            && // Sub  Group can only be added by GroupAdmin 
            !groupService.UserHasGroupAuthorization(currentUser, groupId,
                                                    GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Not authorized");

        group.ArchivedAt = reactivate ? null : DateTime.UtcNow;

        var groupsToArchive = groupService.GetGroupsForUser(currentUser, GroupRole.GroupAdmin, true, groupId, reactivate).ToList();
        foreach (var g in groupsToArchive)
        {
            g.ArchivedAt = reactivate ? null : DateTime.UtcNow;
        }

        db.Groups.Update(group);
        db.Groups.UpdateRange(groupsToArchive);

        await db.SaveChangesAsync();

        return await groupService.GetGroupsForUserAsync(
            userId
        );
    }

    public async Task<IEnumerable<Group>> RemoveGroupAsync(Guid userId, Guid groupId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var group = groupRepository.GetGroupById(groupId);

        if (group.ParentGroupId.IsNullOrEmpty() && !RoleAuthorization.IsAdmin(currentUser)
            || // Root Group can only be removed by Admin 
            !group.ParentGroupId.IsNullOrEmpty()
            && // Sub  Group can only be removed by GroupAdmin 
            !groupService.UserHasGroupAuthorization(currentUser, groupId, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Not authorized");

        db.Groups.Remove(group);
        await db.SaveChangesAsync();

        return db.Groups.Base(currentUser.MandatorId);
    }

    public async Task<User> AddUserToGroupAsync(Guid userId, AddUserGroupModel userGroupModelForm)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, userGroupModelForm.GroupId, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User doesn't have permission to add a user to this group");

        var userToAdd = await userRepository.GetUserByIdAsync(userGroupModelForm.UserId);

        if (userToAdd == null)
            throw new ApiException(ErrorCode.DataNotFound, "The user with this id cannot be found");
        if (userToAdd.Admin)
            throw new ApiException(ErrorCode.Forbidden, "Can't Add System-Admin to Groups");

        var isUserAlreadyInGroup = db.UserGroups
            .Base(userGroupModelForm.UserId, userGroupModelForm.GroupId)
            .Any();
        if (isUserAlreadyInGroup)
            throw new ApiException(ErrorCode.InputInvalid, "This user is already present in this group");

        if (!await encryptionsCheckService.EncryptionsProvidedForGroupIncludingSubGroupsAsync(userGroupModelForm.GroupId, userGroupModelForm.EncryptedSharedPasswords))
        {
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // Save Encryptions (ignore passwords that are sent for users that are already in target parent group(s) and have all encryptions)
        encryptionsRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(
            userGroupModelForm.EncryptedSharedPasswords, userToAdd.KeyPairs.LastActive().Id);

        // Save UserGroup
        var userGroup = new UserGroup
        {
            GroupId = userGroupModelForm.GroupId,
            UserId = userToAdd.Id,
            GroupRole = userGroupModelForm.GroupRole
        };

        db.UserGroups.Add(userGroup);
        await db.SaveChangesAsync();

        return userToAdd;
    }

    public async Task<User> EditUserInGroupAsync(Guid userId, EditUserGroupModel userGroupModelForm)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, userGroupModelForm.GroupId, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User doesn't have permission to edit a user in this group");

        var group = groupRepository.GetGroupById(userGroupModelForm.GroupId);
        var userGroup = group.UserGroups
            .Single(ug => ug.UserId == userGroupModelForm.UserId);

        userGroup.GroupRole = userGroupModelForm.GroupRole;
        db.UserGroups.Update(userGroup);
        await db.SaveChangesAsync();

        return userGroup.User;
    }

    public async Task<User> RemoveUserFromGroupAsync(Guid userId, Guid groupId, Guid userIdToRemove)
    {
        if (userId == userIdToRemove)
            throw new ApiException(ErrorCode.ForbiddenSelfOperation, "User is not allow to remove himself");

        if (!await groupService.UserHasGroupAuthorizationAsync(userId, groupId, GroupRole.GroupAdmin))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User doesn't have permission to remove a user from this group");

        var group = groupRepository.GetGroupById(groupId);
        var userToRemove = await userRepository.GetUserByIdAsync(userIdToRemove);

        var userGroup = group.UserGroups
            .SingleOrDefault(ug => ug.UserId == userToRemove.Id);
        if (userGroup == null)
            throw new ApiException(ErrorCode.DataNotFound, "This user is not in the target group");

        db.UserGroups.Remove(userGroup);
        group.UserGroups.Remove(userGroup);

        // Remove encrypted passwords user not does not has access to
        var groupsOfUser = groupService.GetGroupsForUser(userToRemove, GroupRole.GroupViewer, true);
        var sharedGroupPasswordGuidsUserHasNowAccessTo =
            (await sharedPasswordRepository.GetGroupPasswordsByGroupIdsAsync(groupsOfUser.Select(g => g.Id).ToList()))
            .Select(sp => sp.Id)
            .ToList();

        // Get all encryptedPasswords of this user and then aggregate into list all those that that do not belong to sharedPasswords this user now has access to
        var allKeyPairGuidsOfUser = await db.KeyPairs
            .Where(pk => pk.UserId == userToRemove.Id)
            .Select(pk => pk.Id)
            .ToListAsync();

        var encryptedSharedGroupPasswordsOfUserToRemove = await db.EncryptedSharedPasswords
            .Include(esp => esp.SharedPassword)
            .Where(esp => esp.SharedPassword.GroupId.HasValue)
            .Where(esp => allKeyPairGuidsOfUser.Contains(esp.KeyPairId))
            .Where(esp => !sharedGroupPasswordGuidsUserHasNowAccessTo.Contains(esp.SharedPasswordId))
            .ToListAsync();

        db.EncryptedSharedPasswords.RemoveRange(encryptedSharedGroupPasswordsOfUserToRemove);
        await db.SaveChangesAsync();

        return userToRemove;
    }

    public async Task<IEnumerable<User>> GetUsersWithAccessAsync(Guid userId, Guid baseGroupId)
    {
        if (!await groupService.UserHasGroupAuthorizationAsync(userId, baseGroupId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User doesn't have permission to view the group requested");

        return await userService.GetUsersWithAccessToGroupIncludingAdminsAsync(baseGroupId);
    }

    public async Task<IEnumerable<Tag>> GetTagsOfGroupAsync(Guid userId, Guid baseGroupId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .IncludeTags()
            .ToListAsync();

        if (!GroupAuthorizations.UserHasGroupAuthorization(mandatorGroups, currentUser, baseGroupId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User doesn't have permission to view the group requested");

        return mandatorGroups
            .Single(g => g.Id == baseGroupId)
            .Tags;
    }
}