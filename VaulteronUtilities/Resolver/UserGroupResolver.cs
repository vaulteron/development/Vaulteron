﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Resolver;

public class UserGroupResolver : IUserGroupResolver
{
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;

    public UserGroupResolver(VaulteronContext dbContext, IUserRepository userRepository)
    {
        db = dbContext;
        this.userRepository = userRepository;
    }

    public async Task<Group> GetGroupAsync(Guid userId, Guid groupId, Guid userToLookupId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var user = await userRepository.GetUserByIdAsync(userToLookupId);

        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        return GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, includeSubGroups: true)
            .Single(g => g.Id == groupId);
    }

    public async Task<User> GetUserAsync(Guid userId, Guid groupId, Guid userToLookupId)
    {
        var baseUser = await userRepository.GetUserByIdAsync(userId);

        var userGroup = await db.UserGroups
            .Where(u => u.GroupId == groupId && u.UserId == userToLookupId)
            .Include(u => u.User)
            .SingleAsync(u => u.Group.MandatorId == baseUser.MandatorId);

        return userGroup.User;
    }
}