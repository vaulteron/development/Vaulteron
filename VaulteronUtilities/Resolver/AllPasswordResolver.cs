﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Resolver;

public class AllPasswordResolver : IAllPasswordResolver
{
    private readonly IAllPasswordService allPasswordService;
    private readonly IAccountEncryptionsRepository accountEncryptionsRepository;
    private readonly ISharedEncryptionsRepository sharedEncryptionsRepository;
    private readonly IAccountPasswordRepository accountPasswordRepository;
    private readonly ISharedPasswordResolver sharedPasswordResolver;
    private readonly IAccountPasswordResolver accountPasswordResolver;
    private readonly ISharedPasswordService sharedPasswordService;

    public AllPasswordResolver(IAllPasswordService allPasswordService, ISharedPasswordService sharedPasswordService,
        IAccountEncryptionsRepository accountEncryptionsRepository, ISharedEncryptionsRepository sharedEncryptionsRepository,
        IAccountPasswordRepository accountPasswordRepository, ISharedPasswordResolver sharedPasswordResolver, IAccountPasswordResolver accountPasswordResolver)
    {
        this.allPasswordService = allPasswordService;
        this.sharedPasswordService = sharedPasswordService;
        this.accountEncryptionsRepository = accountEncryptionsRepository;
        this.sharedEncryptionsRepository = sharedEncryptionsRepository;
        this.accountPasswordRepository = accountPasswordRepository;
        this.sharedPasswordResolver = sharedPasswordResolver;
        this.accountPasswordResolver = accountPasswordResolver;
    }

    public async Task<ICollection<EncryptedPassword>> GetEncryptedPasswordChangeLogAsync(Guid userId, Guid passwordId)
    {
        var password = await allPasswordService.GetPasswordByIdAsync(passwordId);

        return password switch
        {
            SharedPassword when !await sharedPasswordService.UserHasAuthorisationForSharedPasswordAsync(userId, passwordId, GroupRole.GroupEditor) => throw new ApiException(
                ErrorCode.UserIsNotAuthorized),
            SharedPassword => await sharedEncryptionsRepository.GetEncryptedPasswordsByUserAndSharedPasswordAsync(userId, password.Id),
            AccountPassword accountPassword => await accountEncryptionsRepository.GetEncryptedPasswordsForAccountPasswordAsync(accountPassword),
            _ => throw new ApiException(ErrorCode.DataNotFound)
        };
    }

    public async Task<IEnumerable<Password>> GetPasswordsByUrlAsync(Guid userId, string url)
    {
        var accountPasswords = await accountPasswordResolver.GetAccountPasswordsAsync(userId, new AccountPasswordFilterType { ByArchived = false });
        var groupPasswords = await sharedPasswordService.GetAllGroupPasswordsByUserIdAsync(userId, false);
        var companyPasswords = await sharedPasswordResolver.GetCompanyPasswordsAsync(userId, new SharedPasswordFilterType
        {
            ByArchived = false,
            CreatedByIds = new List<Guid> { userId }
        });

        var list = new List<Password>()
            .Concat(accountPasswords)
            .Concat(groupPasswords)
            .Concat(companyPasswords);

        if (string.IsNullOrEmpty(url))
            return list;

        var givenHost = UrlToHost(url);
        return list.Where(pw => givenHost == UrlToHost(pw.WebsiteURL));
    }

    private static string UrlToHost(string url)
    {
        const string pattern = @"(http[s]?:\/\/)?(www\.)?([^\/\s\?]+)?(.*)";
        var match = Regex.Match(url, pattern);
        return match.Groups[3].Value;
    }
}