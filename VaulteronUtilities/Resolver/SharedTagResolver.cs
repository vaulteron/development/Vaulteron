﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Resolver;

public class SharedTagResolver : ISharedTagResolver
{
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;

    public SharedTagResolver(VaulteronContext dbContext, IUserRepository userRepository)
    {
        db = dbContext;
        this.userRepository = userRepository;
    }

    public async Task<IEnumerable<Tag>> GetTagsAsync(Guid userId, SharedTagFilterType filter)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var mandatorGroups = await db.Groups
            .IncludeUserGroup()
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var groupsToView = GroupAuthorizations
            .GetGroupsForUser(mandatorGroups, currentUser, includeSubGroups: true)
            .ToList();

        return await db.SharedTags
            .Base(currentUser.MandatorId)
            .ByGroups(groupsToView)
            .ByGroupId(filter.GroupId)
            .BySearchTerm(filter.SearchTerm)
            .ToListAsync();
    }

    public async Task<SharedTag> AddSharedTagAsync(Guid userId, AddSharedTagModel tagForm)
    {
        if (string.IsNullOrWhiteSpace(tagForm.Name))
            throw new ApiException(ErrorCode.InputInvalid, "Tag name is not set");

        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var groupsToEdit = GroupAuthorizations
            .GetGroupsForUser(allGroups, currentUser, GroupRole.GroupEditor, true)
            .ToList();

        var groupToAdd = groupsToEdit.Single(g => g.Id == tagForm.GroupId);

        var newTag = new SharedTag
        {
            Name = tagForm.Name,
            Color = tagForm.Color,
            GroupId = groupToAdd.Id
        };

        db.SharedTags.Add(newTag);
        await db.SaveChangesAsync();
        return newTag;
    }

    public async Task<SharedTag> EditSharedTagAsync(Guid userId, EditSharedTagModel tagModelForm)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var loadedTag = await db.SharedTags
            .Base(currentUser.MandatorId)
            .ById(tagModelForm.Id)
            .SingleAsync();

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var groupsToEdit = GroupAuthorizations
            .GetGroupsForUser(allGroups, currentUser, GroupRole.GroupEditor, true)
            .ToList();

        var groupToAdd = groupsToEdit.Single(g => g.Id == tagModelForm.GroupId);

        loadedTag.Name = tagModelForm.Name;
        loadedTag.Color = tagModelForm.Color;
        loadedTag.Group = groupToAdd;

        db.SharedTags.Update(loadedTag);
        await db.SaveChangesAsync();

        return loadedTag;
    }

    public async Task<SharedTag> RemoveSharedTagAsync(Guid userId, Guid tagId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var groupsToEdit = GroupAuthorizations
            .GetGroupsForUser(allGroups, currentUser, GroupRole.GroupEditor, true)
            .ToList();

        var loadedTag = await db.SharedTags
            .Base(currentUser.MandatorId)
            .ById(tagId)
            .ByGroups(groupsToEdit)
            .SingleOrDefaultAsync();
        if (loadedTag is null)
            throw new ApiException(ErrorCode.DataNotFound, "This sharedTag does not exist or you do not have sufficient permissions");

        db.SharedTags.Remove(loadedTag);
        await db.SaveChangesAsync();

        return loadedTag;
    }

    public async Task<Group> GetGroupForSharedTagAsync(Guid userId, Guid tagId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .IncludeTags()
            .ToListAsync();

        var groupsToView = GroupAuthorizations.GetGroupsForUser(allGroups, currentUser, includeSubGroups: true);

        var groupsFilteredByTags = groupsToView.AsQueryable().ByTagId(tagId).ToList();
        return groupsFilteredByTags.Count == 0 ? null : groupsFilteredByTags[0];
    }
}