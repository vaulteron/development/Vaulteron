﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Resolver;

public class UserResolver : IUserResolver
{
    private readonly VaulteronContext db;

    private readonly UserManager<User> userManager;
    private readonly IUserHelperService userHelperService;
    private readonly IGroupResolver groupResolver;
    private readonly IUserService userService;
    private readonly IEmailSenderService emailSenderService;
    private readonly ISharedEncryptionsRepository encryptionsHelperRepository;
    private readonly IUserRepository userRepository;
    private readonly IPasswordsEncryptionsCheckService encryptionsCheckService;
    private readonly IMandatorService mandatorService;
    private readonly IKeyPairRepository keyPairRepository;
    private readonly IInvoiceService invoiceService;

    public UserResolver(VaulteronContext dbContext,
        UserManager<User> userManager,
        IUserHelperService userHelperService,
        IGroupResolver groupResolver,
        IUserService userService,
        IEmailSenderService emailSenderService,
        ISharedEncryptionsRepository encryptionsHelperRepository,
        IUserRepository userRepository,
        IPasswordsEncryptionsCheckService encryptionsCheckService,
        IMandatorService mandatorService,
        IKeyPairRepository keyPairRepository,
        IInvoiceService invoiceService)
    {
        db = dbContext;

        this.userManager = userManager;
        this.userHelperService = userHelperService;
        this.groupResolver = groupResolver;
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.encryptionsHelperRepository = encryptionsHelperRepository;
        this.userRepository = userRepository;
        this.encryptionsCheckService = encryptionsCheckService;
        this.mandatorService = mandatorService;
        this.keyPairRepository = keyPairRepository;
        this.invoiceService = invoiceService;
    }

    public async Task<Mandator> GetMandatorAsync(Guid userId)
    {
        var user = await db.Users
            .CurrentUser(userId)
            .Include(u => u.Mandator)
            .SingleAsync();
        return user.Mandator;
    }

    public async Task<bool> IsPasswordResetEnabled(Guid userId, Guid userIdToLookup)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var userToEdit = userIdToLookup == Guid.Empty
            ? currentUser
            : await userRepository.GetUserByIdAsync(userIdToLookup);

        if (currentUser.MandatorId != userToEdit.MandatorId)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User must be in the same company");

        return !string.IsNullOrWhiteSpace(userToEdit.EncryptedLoginPassword);
    }

    public async Task<KeyPair> GetEncryptedPrivateKeyAsync(Guid userId, Guid userIdToLookup)
    {
        if (userId != userIdToLookup) return null;

        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var keyPair = await db.KeyPairs
            .Base(currentUser.MandatorId, userIdToLookup)
            .ToListAsync();
        keyPair.Sort((kp1, kp2) => kp1.CreatedAt.CompareTo(kp2.CreatedAt));
        return keyPair.LastActive();
    }

    /// <summary>
    /// Return the KeyPair if the user is admin, wants to view own key
    /// Or as access to at least one group the target user also has access to 
    /// </summary>
    /// <param name="userId">The user making this request</param>
    /// <param name="userIdToLookup">The user to get the KeyPair from</param>
    /// <returns>The KeyPair of the requested user or null</returns>
    public async Task<KeyPair> GetKeyPairAsync(Guid userId, Guid userIdToLookup)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var keyPair = await db.KeyPairs
            .Base(currentUser.MandatorId, userIdToLookup)
            .LastActiveAsync();

        keyPair.EncryptedPrivateKey = null; // Make sure encryptedPrivateKey is never exposed beyond here
        return keyPair;
    }

    public async Task<User> GetUserAsync(Guid userId) => await db.Users.ById(userId).Include(u=>u.WebautnCredentials).SingleAsync();

    public async Task<User> AddUserAsync(Guid userId, AddUserModel userModelForm)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (!currentUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User must be an admin to add users");

        if (!mandatorService.HasValidLicence())
            throw new ApiException(ErrorCode.LicenseInvalid);
        if (!await mandatorService.HasLicenceSlotsAvailableAsync())
            throw new ApiException(ErrorCode.LicenseInsufficient);

        if (string.IsNullOrWhiteSpace(userModelForm.PublicKey)
            || string.IsNullOrWhiteSpace(userModelForm.EncryptedPrivateKey)
            || string.IsNullOrWhiteSpace(userModelForm.NewTemporaryLoginPassword))
            throw new ApiException(ErrorCode.UnableToRegister, "A temporary key pair must be passed along");

        if (userModelForm.EncryptedSharedPasswords == null)
            throw new ApiException(ErrorCode.InputInvalid, "Necessary encryptions not provided");

        var newUser = new User
        {
            ForeignObjectId = userModelForm.ForeignObjectId,
            Email = userModelForm.Email,
            UserName = userModelForm.Email,
            Firstname = userModelForm.Firstname,
            Lastname = userModelForm.Lastname,
            Sex = userModelForm.Sex,
            Admin = false,
            MandatorId = currentUser.MandatorId
        };
        var reasonsFormDataIncorrect = userHelperService.AreRegisterFieldsValid(newUser).ToList();
        if (await db.Users.ExistEmailAsync(newUser.Email))
            reasonsFormDataIncorrect.Add("This email is already taken");
        if (reasonsFormDataIncorrect.Any())
            throw new ApiException(ErrorCode.UnableToRegister, "Unable to register new user", reasonsFormDataIncorrect);

        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .IncludeSharedPasswords()
            .ToListAsync();

        // Check groups
        var groups = new List<Group>();
        if (userModelForm.GroupsToBeAddedTo != null && userModelForm.GroupsToBeAddedTo.Any())
        {
            groups = mandatorGroups
                .ByIds(userModelForm.GroupsToBeAddedTo)
                .ToList();

            if (groups.Count != userModelForm.GroupsToBeAddedTo.Count)
                throw new ApiException(ErrorCode.UnableToRegister, "Unable to register user", new[] { "Some of the supplied groupIds do not exist" });
        }

        // Check provided Encryptions
        var encryptionsProvided = userModelForm.EncryptedSharedPasswords
            .Select(enc => new EncryptedSharedPassword
            {
                SharedPasswordId = enc.SharedPasswordId,
                EncryptedPasswordString = enc.EncryptedPasswordString
            })
            .ToList();

        if (userModelForm.Admin)
        {
            var allGroups = await db.Groups
                .Base(currentUser.MandatorId, null)
                .IncludeSharedPasswords()
                .ToListAsync();
            var allGroupIds = allGroups.Select(g => g.Id).ToList();
            var passwordsToCheck = await db.SharedPasswords
                .AsNoTracking()
                .BasePasswords(currentUser.MandatorId, allGroupIds)
                .ToListAsync();
            if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(passwordsToCheck, encryptionsProvided))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }
        else
        {
            var groupsForEncryptedPasswords = GroupAuthorizations.IncludeSubGroups(groups);
            var encryptedGroupPasswords = groupsForEncryptedPasswords.SelectMany(g => g.SharedPasswords).ToList();
            if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(encryptedGroupPasswords, encryptionsProvided))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // Save first public key
        newUser.KeyPairs = new List<KeyPair>
        {
            new()
            {
                PublicKeyString = userModelForm.PublicKey,
                EncryptedPrivateKey = userModelForm.EncryptedPrivateKey,
                // Will be set during granting admin permissions
                EncryptedSharedPasswords = userModelForm.Admin ? null : encryptionsProvided
            }
        };

        // Create new user
        var res = await userManager.CreateAsync(newUser, userModelForm.NewTemporaryLoginPasswordHashed);
        if (!res.Succeeded)
            throw new ApiException(ErrorCode.UnableToRegister, "Unable to register user", res.Errors.Select(e => e.Description));

        // Add to groups
        if (groups.Any())
        {
            foreach (var group in groups)
            {
                db.UserGroups.Add(new UserGroup
                {
                    GroupId = group.Id,
                    GroupRole = GroupRole.GroupViewer,
                    UserId = newUser.Id
                });
            }
        }

        if (userModelForm.Admin)
            await GrantAdminPermissionsAsync(currentUser.Id, newUser.Id, encryptionsProvided);

        await db.SaveChangesAsync();


        // Update Subscription
        await UpdateSubscriptionQuantity(currentUser);

        var passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(newUser);
        await emailSenderService.SendEmailAfterUserAddedAsync(passwordResetToken, userModelForm.NewTemporaryLoginPassword, newUser.Id,
                                                              newUser.Email, $"{currentUser.Firstname} {currentUser.Lastname}");


        return newUser;
    }

    public async Task<User> EditUserAsync(Guid userId, EditUserModel userModelForm)
    {
        if (
            string.IsNullOrEmpty(userModelForm.Firstname)
            && string.IsNullOrEmpty(userModelForm.Lastname)
            && string.IsNullOrEmpty(userModelForm.Email)
            && string.IsNullOrEmpty(userModelForm.EncryptedLoginPassword)
            && string.IsNullOrEmpty(userModelForm.SecurityQuestion1)
            && string.IsNullOrEmpty(userModelForm.SecurityQuestion2)
            && string.IsNullOrEmpty(userModelForm.SecurityQuestion3)
            && userModelForm.ForeignObjectId == null
        )
            throw new ApiException(ErrorCode.InputInvalid, "All fields to be changed are empty.");

        var currentUser = await userRepository.GetUserByIdAsync(userId, true);

        var userToEdit = userModelForm.Id == Guid.Empty
            ? currentUser
            : await userRepository.GetUserByIdAsync(userModelForm.Id, true);

        if (!currentUser.Admin && userToEdit.Id != currentUser.Id)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User has to be admin");

        if (userModelForm.Firstname != null) userToEdit.Firstname = userModelForm.Firstname;
        if (userModelForm.Lastname != null) userToEdit.Lastname = userModelForm.Lastname;
        if (userModelForm.EncryptedLoginPassword != null) userToEdit.EncryptedLoginPassword = userModelForm.EncryptedLoginPassword;
        if (userModelForm.SecurityQuestion1 != null) userToEdit.SecurityQuestion1 = userModelForm.SecurityQuestion1;
        if (userModelForm.SecurityQuestion2 != null) userToEdit.SecurityQuestion2 = userModelForm.SecurityQuestion2;
        if (userModelForm.SecurityQuestion3 != null) userToEdit.SecurityQuestion3 = userModelForm.SecurityQuestion3;
        if (userModelForm.ForeignObjectId != null) userToEdit.ForeignObjectId = userModelForm.ForeignObjectId;

        // Only admin can change an email address (a normal user was given the company email -> why should he change it?)
        if (currentUser.Admin && !string.IsNullOrEmpty(userModelForm.Email) && userToEdit.Email != userModelForm.Email)
        {
            if (db.Users.Any(u => u.Email == userModelForm.Email))
                throw new ApiException(ErrorCode.InputInvalid, "Email already exists.");
            var changeEmailTokenAsync =
                await userManager.GenerateChangeEmailTokenAsync(userToEdit, userModelForm.Email);
            await emailSenderService.SendEmailInformationThatUserEmailWasChanged(userToEdit.Id, userToEdit.Email, userModelForm.Email,
                                                                                 $"{userToEdit.Firstname} {userToEdit.Lastname}", changeEmailTokenAsync);
        }

        await db.SaveChangesAsync();

        if (!string.IsNullOrEmpty(userModelForm.SecurityQuestion1) || !string.IsNullOrEmpty(userModelForm.SecurityQuestion2) || !string.IsNullOrEmpty(userModelForm.SecurityQuestion3))
        {
            await userManager.UpdateSecurityStampAsync(currentUser);
        }

        return userToEdit;
    }

    public async Task<User> GrantAdminPermissionsAsync(Guid userId, Guid targetUserId, ICollection<EncryptedSharedPassword> encryptions)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (!currentUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Only an admin can grant or revoke other user's admin permissions");

        if (!mandatorService.HasValidLicence())
            throw new ApiException(ErrorCode.LicenseInvalid);

        var targetUser = await db.Users
            .ByMandatorId(currentUser.MandatorId)
            .ByArchived(false)
            .ById(targetUserId)
            .IncludeKeyPairs()
            .SingleOrDefaultAsync();

        if (targetUser == null)
            throw new ApiException(ErrorCode.DataNotFound, "This user does not exist");
        if (targetUser.Admin)
            throw new ApiException(ErrorCode.Forbidden, "This user is already an admin");

        var allGroups = await db.Groups
            .Base(currentUser.MandatorId, null)
            .Include(g => g.UserGroups)
            .IncludeSharedPasswords()
            .ToListAsync();

        // Make sure Encryptions are provided for all passwords in all Groups
        var allGroupIds = allGroups.Select(g => g.Id).ToList();
        var passwordsToCheck = await db.SharedPasswords
            .AsNoTracking()
            .BasePasswords(currentUser.MandatorId, allGroupIds)
            .ToListAsync();
        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(passwordsToCheck, encryptions))
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

        await db.SaveChangesAsync(); // Next thing is removing the groups, but in that method SaveChanges() is also called. So better we throw a DB error here, then in there.

        // Remove this user from all groups because admins cannot be in groups
        var groupsThisUserIsIn = allGroups.Where(g => g.UserGroups.Any(ug => ug.UserId == targetUserId));
        foreach (var group in groupsThisUserIsIn)
        {
            await groupResolver.RemoveUserFromGroupAsync(currentUser.Id, @group.Id, targetUserId);
        }

        // Save Encryptions (ignore passwords that are sent for users that are already in target parent group(s) and have all encryptions)
        encryptionsHelperRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(encryptions, targetUser.KeyPairs.LastActive().Id);

        targetUser.Admin = true;
        await db.SaveChangesAsync();

        return targetUser;
    }

    public async Task<User> RevokeAdminPermissionsAsync(Guid userId, Guid targetUserId)
    {
        if (!mandatorService.HasValidLicence())
            throw new ApiException(ErrorCode.LicenseInvalid);

        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (await db.Users.ByMandatorId(currentUser.MandatorId).CountAsync(u => u.Admin) <= 1)
            throw new ApiException(ErrorCode.Forbidden, "Can not revoke admin permissions of last admin standing");

        if (!currentUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Only an admin can grant or revoke other user's admin permissions");

        var targetUser = await db.Users.ById(targetUserId).Include(u => u.KeyPairs).SingleAsync();

        if (!targetUser.Admin)
            throw new ApiException(ErrorCode.Forbidden, "This user does not have admin permissions");

        var numAdmins = await db.Users.Base(currentUser.MandatorId)
            .ByArchived(false)
            .CountAsync(u => u.Admin);
        if (numAdmins == 1)
            throw new ApiException(ErrorCode.Forbidden, "Can not revoke the admin permissions of the last non-archived admin.");

        // Now delete all stuff user does not have access to anymore
        // Because this user was an admin beforehand we know that that user is not in any group --> this user cannot access any sharedPasswords
        // The accountPasswords are left unchanged
        var targetUsersKeyPairIds = targetUser.KeyPairs.Select(kp => kp.Id).ToList();
        var encryptedSharedPasswords = await db.EncryptedSharedPasswords
            .Where(esp => !(esp.SharedPassword.CreatedById.HasValue && esp.SharedPassword.CreatedById.Value == targetUser.Id))
            .Where(esp => targetUsersKeyPairIds.Contains(esp.KeyPairId))
            .ToListAsync();
        db.EncryptedSharedPasswords.RemoveRange(encryptedSharedPasswords);

        targetUser.Admin = false;
        await db.SaveChangesAsync();

        return targetUser;
    }

    public async Task<User> ArchiveUserAsync(Guid userId, Guid userToArchiveId, bool reenable, ICollection<EncryptedSharedPassword> encryptedSharedPasswords = null)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (!currentUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User has to be admin");

        if (!mandatorService.HasValidLicence())
            throw new ApiException(ErrorCode.LicenseInvalid);
        if (reenable && !await mandatorService.HasLicenceSlotsAvailableAsync())
            throw new ApiException(ErrorCode.LicenseInsufficient);

        var loadedUser = await db.Users.Base(currentUser.MandatorId)
            .IncludeGroups()
            .ById(userToArchiveId)
            .SingleOrDefaultAsync();
        if (loadedUser == null)
            throw new ApiException(ErrorCode.DataNotFound, "This user does not exist");

        var numAdmins = await db.Users.Base(currentUser.MandatorId)
            .ByArchived(false)
            .CountAsync(u => u.Admin);
        if (!reenable && loadedUser.Admin && numAdmins == 1)
            throw new ApiException(ErrorCode.Forbidden, "Can not archive the last admin in the system. There must be at least one admin.");

        var isUserArchived = loadedUser.ArchivedAt != null;
        if (isUserArchived && !reenable)
            throw new ApiException(ErrorCode.InputInvalid, "User is already archived");
        if (!isUserArchived && reenable)
            throw new ApiException(ErrorCode.InputInvalid, "User must be archived to be reactivated");

        loadedUser.ArchivedAt = reenable ? null : DateTime.UtcNow;
        db.Users.Update(loadedUser);

        // Load public keys
        var allKeyPairsOfTargetUser = await db.KeyPairs
            .Where(kp => kp.UserId == userToArchiveId)
            .ToListAsync();

        if (reenable)
        {
            if (encryptedSharedPasswords == null)
                throw new ApiException(ErrorCode.InputInvalid, "No encryptions provided. An empty list can also be sent.");

            // Load Groups
            var mandatorGroups = await db.Groups
                .RecursionBase(currentUser.MandatorId)
                .IncludeSharedPasswords()
                .ToListAsync();

            var groupsForEncryptedPasswords = GroupAuthorizations.GetGroupsForUser(mandatorGroups, loadedUser, GroupRole.GroupViewer, true);

            // Check provided Encryptions
            var usersLastActiveKeyPairId = allKeyPairsOfTargetUser.LastActive().Id;
            var encryptions = encryptedSharedPasswords
                .Select(enc => new EncryptedSharedPassword
                {
                    SharedPasswordId = enc.SharedPasswordId,
                    EncryptedPasswordString = enc.EncryptedPasswordString,
                    KeyPairId = usersLastActiveKeyPairId
                })
                .ToList();

            var encryptedSharedPasswordsToCheck = loadedUser.Admin
                ? mandatorGroups.SelectMany(g => g.SharedPasswords)
                : groupsForEncryptedPasswords.SelectMany(g => g.SharedPasswords).ByUnArchived();

            var companyPasswordsToCheck = loadedUser.Admin
                ? db.SharedPasswords.BaseCompany(loadedUser.MandatorId, null).AsEnumerable()
                : db.SharedPasswords.BaseCompany(loadedUser.MandatorId).ByCreatedBy(loadedUser.Id).AsEnumerable();

            var syncedOfflinePasswordsToCheck = db.SharedPasswords
                .BaseOffline(loadedUser.MandatorId)
                .ByCreatedBy(loadedUser.Admin ? null : new List<Guid> { loadedUser.Id })
                .AsEnumerable();

            var allEncryptionsToCheck = encryptedSharedPasswordsToCheck
                .Concat(companyPasswordsToCheck)
                .Concat(syncedOfflinePasswordsToCheck)
                .ToList();

            if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(allEncryptionsToCheck, encryptions))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

            // No encryptions should be set for this user. In spite of that remove all and set newly
            var allEncryptedSharedPasswordsForUser = await db.EncryptedSharedPasswords
                .ByKeyPairId(usersLastActiveKeyPairId)
                .ToListAsync();
            if (allEncryptedSharedPasswordsForUser.Count > 0)
                db.EncryptedSharedPasswords.RemoveRange(allEncryptedSharedPasswordsForUser);

            var encryptionsToAdd = encryptions.Select(enc => new EncryptedSharedPassword
            {
                KeyPairId = enc.KeyPairId,
                SharedPasswordId = enc.SharedPasswordId,
                EncryptedPasswordString = enc.EncryptedPasswordString,
            });
            db.EncryptedSharedPasswords.AddRange(encryptionsToAdd);
        }
        else
        {
            var allKeyPairIdsOfTargetUser = allKeyPairsOfTargetUser
                .Select(kp => kp.Id)
                .ToList();
            // Remove all EncryptedSharedPasswords for all keyPairs of this user
            var encryptedSharedPasswordsToRemove = db.EncryptedSharedPasswords
                .Where(esp => allKeyPairIdsOfTargetUser.Contains(esp.KeyPairId));
            db.EncryptedSharedPasswords.RemoveRange(encryptedSharedPasswordsToRemove);
        }

        await db.SaveChangesAsync();

        await UpdateSubscriptionQuantity(currentUser);

        return loadedUser;
    }

    public async Task<List<User>> GetUsersAsync(Guid userId, UserFilterType filter)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        return await db.Users.Base(currentUser.MandatorId)
            .BySearchTerm(filter.SearchTerm)
            .ByGroupIds(filter.GroupIds)
            .ExcludeUsersInGroupIds(filter.ExcludeUsersInGroupIds)
            .ByWithoutUserIds(filter.WithoutUserIds)
            .ByUserIds(filter.UserIds)
            .ByArchived(currentUser.Admin ? filter.ByArchived : false)
            .OrderBy(u => u.Firstname)
            .ToListAsync();
    }

    public async Task<List<User>> GetAdminsAsync(Guid userId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        return await db.Users.Base(currentUser.MandatorId)
            .Where(u => u.Admin)
            .ToListAsync();
    }

    public async Task<IEnumerable<UserGroup>> GetUserGroupsAsync(Guid userId, Guid userIdToLookup)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var user = await userRepository.GetUserByIdAsync(userIdToLookup);

        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .IncludeUserGroup()
            .ToListAsync();

        var groups = GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, includeSubGroups: true).ToList();

        var userGroups = groups.Select(g =>
        {
            var groupRole = user.Admin
                ? GroupRole.GroupAdmin
                : GroupAuthorizations.GetGroupRoleForUserInGroup(mandatorGroups, user.Id, g.Id);

            if (groupRole is null)
                throw new ApiException(ErrorCode.UnknownError, "Unable to retrieve the group role of " + user.Id + " for group " + g.Id);

            return new UserGroup
            {
                GroupId = g.Id,
                UserId = user.Id,
                GroupRole = groupRole.Value
            };
        });

        return userGroups;
    }

    public async Task<IEnumerable<Group>> GetGroupsAsync(Guid userId, Guid userIdToLookup)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var user = await userRepository.GetUserByIdAsync(userIdToLookup);

        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        // Permission that the caller has the right to see the groups
        var callerGroups = GroupAuthorizations.GetGroupsForUser(mandatorGroups, currentUser, includeSubGroups: true).ToList();

        // Permission that the user has the right to see the groups
        var userGroups = GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, includeSubGroups: true).ToList();

        return userGroups.Intersect(callerGroups);
    }

    public async Task<User> ResendAccountActivationEmailAsync(Guid userId, Guid targetUserId, string publicKey,
        string encryptedPrivateKey, string newTempLoginPassword, string newTempLoginPasswordHashed, List<EncryptedSharedPassword> newEncryptions)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var targetUser = await db.Users
            .ByMandatorId(currentUser.MandatorId)
            .ById(targetUserId)
            .SingleOrDefaultAsync();

        if (!currentUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Only an admin can resend another user's account activation email");
        if (targetUser == null)
            throw new ApiException(ErrorCode.DataNotFound, "Target user not found");
        if (targetUser.EmailConfirmed)
            throw new ApiException(ErrorCode.InputInvalid, "This user has already verified the email");

        #region Verify input

        var encryptionsProvided = newEncryptions
            .Select(enc => new EncryptedSharedPassword
            {
                SharedPasswordId = enc.SharedPasswordId,
                EncryptedPasswordString = enc.EncryptedPasswordString
            })
            .ToList();

        if (targetUser.Admin)
        {
            var allGroupIds = await db.Groups
                .Base(currentUser.MandatorId, null)
                .IncludeSharedPasswords()
                .Select(g => g.Id)
                .ToListAsync();
            var passwordsToCheck = await db.SharedPasswords
                .AsNoTracking()
                .BaseSyncedPasswords(currentUser.MandatorId, allGroupIds)
                .ToListAsync();
            if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(passwordsToCheck, encryptionsProvided))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }
        else
        {
            var groups = await db.Groups
                .RecursionBase(targetUser.MandatorId)
                .ByUserId(targetUser.Id)
                .ToListAsync();
            var groupIdsUserHasAccessTo = GroupAuthorizations.IncludeSubGroups(groups)
                .Select(g => g.Id)
                .ToList();
            var encryptedPasswords = await db.SharedPasswords
                .ByCompanyOrGroupPasswordForUser(targetUser.Id, groupIdsUserHasAccessTo)
                .ToListAsync();
            if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(encryptedPasswords, encryptionsProvided))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        #endregion

        #region Delete all encryptions for this user

        var oldKeyPair = await GetKeyPairAsync(currentUser.Id, targetUser.Id);
        var encryptedAccountPasswords = await db.EncryptedAccountPasswords
            .ByKeyPairId(oldKeyPair.Id)
            .ToListAsync();
        db.EncryptedAccountPasswords.RemoveRange(encryptedAccountPasswords);
        var encryptedSharedPasswords = await db.EncryptedSharedPasswords
            .ByKeyPairId(oldKeyPair.Id)
            .ToListAsync();
        db.EncryptedSharedPasswords.RemoveRange(encryptedSharedPasswords);

        #endregion

        #region Assign new encryptions

        // Save first public key
        targetUser.KeyPairs = new List<KeyPair>
        {
            new()
            {
                PublicKeyString = publicKey,
                EncryptedPrivateKey = encryptedPrivateKey,
                EncryptedSharedPasswords = encryptionsProvided
            }
        };

        await db.SaveChangesAsync();

        #endregion

        // Invalidates old tokens because those are contained in an email with an expired temp-password
        await userManager.UpdateSecurityStampAsync(targetUser);
        // Reset password to this new one
        var passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(targetUser);
        await userManager.ResetPasswordAsync(targetUser, passwordResetToken, newTempLoginPasswordHashed);

        passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(targetUser);

        await emailSenderService.SendEmailAfterUserAddedAsync(passwordResetToken, newTempLoginPassword, targetUser.Id, targetUser.Email, $"{targetUser.Firstname} {targetUser.Lastname}");

        return targetUser;
    }

    public async Task<bool> GetIsSuperAdminAsync(Guid userId) => await userService.UserIsSuperAdminAsync(userId);

    public async Task<ICollection<KeyPair>> GetKeyPairChangeLogsAsync(Guid userId, DateTime? from, DateTime? to) => await keyPairRepository.GetKeyPairsForUserIdWithinTimeAsync(userId, @from, to);

    private async Task UpdateSubscriptionQuantity(User currentUser)
    {
        if (string.IsNullOrEmpty(currentUser.Mandator.PaymentProviderId)) return;

        var userCount = await db.Users.Base(currentUser.MandatorId).ByArchived(false).CountAsync();
        invoiceService.UpdateSubscription(currentUser.Mandator.PaymentProviderId, userCount);
        await db.SaveChangesAsync();
    }
}