﻿using System;
using System.Threading.Tasks;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Resolver;

public class PwnedResolver : IPwnedResolver
{
    private readonly IUserRepository userRepository;
    private readonly IPwnedService pwnedService;

    public PwnedResolver(IUserRepository userRepository, IPwnedService pwnedService)
    {
        this.userRepository = userRepository;
        this.pwnedService = pwnedService;

    }

    public async Task<string> GetBreach(Guid userId, string mail)
    {

        var currentUser = await userRepository.GetUserByIdAsync(userId);

        if (currentUser == null) throw new ApiException(ErrorCode.Forbidden, "No Mandator found");

        return await pwnedService.GetBreach(mail);
    }
}