﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fido2NetLib;
using Fido2NetLib.Objects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.MutationModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronUtilities.Services.Helper.Interfaces;
using VaulteronUtilities.Services.Interfaces;
using static System.Int32;

namespace VaulteronUtilities.Resolver;

public class AuthenticationResolver : IAuthenticationResolver
{
    private readonly VaulteronContext db;

    private readonly UserManager<User> userManager;
    private readonly CustomSignInManager<User> signInManager;
    private readonly IUserHelperService userService;
    private readonly IEmailSenderService emailSenderService;
    private readonly ILogger<AuthenticationResolver> logger;
    private readonly IPasswordsEncryptionsCheckService encryptionsCheckService;
    private readonly IFido2 fido2;
    private readonly IInvoiceService invoiceService;
    private readonly IConfiguration config;
    private readonly IUserRepository userRepository;

    public AuthenticationResolver(VaulteronContext dbContext, UserManager<User> userManager, IUserHelperService userService,
        IEmailSenderService emailSenderService, CustomSignInManager<User> signInManager, ILogger<AuthenticationResolver> logger, IFido2 fido2,
        IPasswordsEncryptionsCheckService encryptionsCheckService, IInvoiceService invoiceService, IConfiguration config, IUserRepository userRepository)
    {
        db = dbContext;
        this.userManager = userManager;
        this.userService = userService;
        this.emailSenderService = emailSenderService;
        this.signInManager = signInManager;
        this.logger = logger;
        this.encryptionsCheckService = encryptionsCheckService;
        this.fido2 = fido2;
        this.invoiceService = invoiceService;
        this.config = config;
        this.userRepository = userRepository;
    }

    public async Task<User> LoginAsync(string email, string password, bool isPersistent)
    {
        var result = await signInManager.PasswordSignInAsync(email, password, isPersistent, false);

        var user = await db.Users
            .AsNoTracking()
            .Include(u => u.Mandator).ThenInclude(m => m.SyncSettings)
            .SingleOrDefaultAsync(u => u.Email == email);

        if (user == null || user.IsArchived())
            throw new ApiException(ErrorCode.UnableToLogin, "Email or password incorrect");

        if (!result.Succeeded && user.Mandator.SyncSettings?.SyncType == SyncType.ActiveDirectoryCloud)
            throw new ApiException(ErrorCode.UnableToLogin_LdapRequired, "LDAP Login required");

        if (result.RequiresTwoFactor)
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorRequired,
                                   "Two-Factor-Authentication required",
                                   user.TwoFATypesEnabled.ToString().Replace(" ", "").Split(","));

        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnableToLogin, "Email or password incorrect");

        return user;
    }

    public async Task<User> LdapLoginAsync(string email, string password, bool isPersistent)
    {
        var user = await db.Users.AsNoTracking()
            .Include(u => u.Mandator).ThenInclude(m => m.SyncSettings)
            .SingleOrDefaultAsync(u => u.Email == email);

        if (user == null || user.IsArchived())
            throw new ApiException(ErrorCode.UnableToLogin, "Email or password incorrect");

        var settings = user.Mandator.SyncSettings;

        if (settings?.SyncType != SyncType.ActiveDirectoryCloud)
            throw new ApiException(ErrorCode.UnableToLogin_LdapNotApplicable, "LDAP login not applicable. Use other login.");

        var ldapConnection = new PLLdapConnection(settings.UseSSLPort, settings.Location, settings.SSLCertificate);

        if (!ldapConnection.CheckCredentials(email, password))
            throw new ApiException(ErrorCode.UnableToLogin, "Email or password incorrect.");

        var result = await signInManager.SignInOrTwoFactorAsync(user, isPersistent);

        if (result.RequiresTwoFactor)
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorRequired, "Two-Factor-Authentication required");

        return user;
    }

    #region 2FA TOTP

    public async Task<ToptAuthenticatorKey> GetTotpAuthenticatorKeyAsync(Guid userId)
    {
        var currentUser = await db.Users
            .CurrentUser(userId)
            .Include(u => u.WebautnCredentials)
            .SingleAsync();

        if (await TwoFATotpEnabled(currentUser))
            throw new ApiException(ErrorCode.UnknownError, "Unable to get new Authenticator Key.",
                                   new[] { "Two-Factor-Authentication via TOTP already enabled" });

        var prev = db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        var result = await userManager.ResetAuthenticatorKeyAsync(currentUser);
        db.ChangeTracker.QueryTrackingBehavior = prev;

        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnknownError, "Unable to get new Athenticator Key.");

        var authenticatorCode = await userManager.GetAuthenticatorKeyAsync(currentUser);

        return new ToptAuthenticatorKey(authenticatorCode, currentUser);
    }

    public async Task<User> SetEnabledTotpAsync(Guid userId, string totpCode, bool enable)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        // No verification if TOTP is being disabled
        if (enable)
        {
            var validToken = await userManager.VerifyTwoFactorTokenAsync(currentUser, userManager.Options.Tokens.AuthenticatorTokenProvider, totpCode);
            if (!validToken)
                throw new ApiException(ErrorCode.InputInvalid, "Invalid Token");
        }

        if (enable) currentUser.TwoFATypesEnabled |= TwoFATypes.TOTP; // Set TOTP Flag
        else currentUser.TwoFATypesEnabled &= ~TwoFATypes.TOTP; // Clear TOTP Flag

        var prev = db.ChangeTracker.QueryTrackingBehavior;
        db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        var result = await userManager.SetTwoFactorEnabledAsync(
            currentUser,
            currentUser.TwoFATypesEnabled.HasFlag(TwoFATypes.TOTP) || currentUser.TwoFATypesEnabled.HasFlag(TwoFATypes.WebAuthn));
        db.ChangeTracker.QueryTrackingBehavior = prev;

        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnknownError, "Unable to Set Two-Factor-Authentication");

        // Send info mail to user
        if (enable)
            await emailSenderService.SendEmailAddMultiFactorAuthenticationAsync(
                currentUser.Email,
                currentUser.Firstname + " " + currentUser.Lastname);
        else
            await emailSenderService.SendEmailRemoveMultiFactorAuthenticationAsync(
                currentUser.Email,
                currentUser.Firstname + " " + currentUser.Lastname);

        return currentUser;
    }


    public async Task<User> RemoveWebAuthnAsync(Guid userId, Guid webAuthKeyId)
    {
        var currentUser = db.Users
            .Include(u => u.WebautnCredentials)
            .CurrentUser(userId)
            .Single();

        // Remove Key
        db.Remove(currentUser.WebautnCredentials.Single(wc => wc.Id == webAuthKeyId));
        await db.SaveChangesAsync();

        if(!currentUser.WebautnCredentials.Any())
            currentUser.TwoFATypesEnabled &= ~TwoFATypes.WebAuthn; // Clear Flag

        await setTwoFAEnabledBasedOnTypesEnabled(currentUser);

        // Send info mail to user
        await emailSenderService.SendEmailRemoveMultiFactorAuthenticationAsync(currentUser.Email,
            currentUser.Firstname + " " + currentUser.Lastname);

        return currentUser;
    }

    public async Task<User> TwoFATOTPLoginAsync(string totp, bool isPersistent, bool rememberClient)
    {
        var currentUser = await signInManager.GetTwoFactorAuthenticationUserAsync();

        if (!await TwoFATotpEnabled(currentUser))
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorFailed, "Two-Factor-Authentication failed");

        var result = await signInManager.TwoFactorAuthenticatorSignInAsync(totp, isPersistent, rememberClient);

        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorFailed, "Two-Factor-Authentication failed");

        return currentUser;
    }

    private async Task<bool> TwoFATotpEnabled(User user)
    {
        return await userManager.GetTwoFactorEnabledAsync(user) && user.TwoFATypesEnabled.HasFlag(TwoFATypes.TOTP);
    }

    #endregion

    #region 2FA FIDO2 / WebAuthn

    private const string Fido2AttestationOptions = "fido2.attestationOptions";
    private const string Fido2AssertionOptions = "fido2.assertionOptions";

    public async Task<WebauthnAttestationChallenge> GetWebAuthnChallengeAsync(Guid userId)
    {
        var currentUser = await db.Users
            .CurrentUser(userId)
            .Include(u => u.WebautnCredentials)
            .SingleAsync();

        var authExtensionsClientInputs = new AuthenticationExtensionsClientInputs
        {
            Extensions = true,
            UserVerificationIndex = true,
            Location = true,
            UserVerificationMethod = true,
            BiometricAuthenticatorPerformanceBounds = new AuthenticatorBiometricPerfBounds
            {
                FAR = float.MaxValue,
                FRR = float.MaxValue
            }
        };
        var fidoCredentials = fido2.RequestNewCredential(
            new Fido2User
            {
                Id = currentUser.Id.ToByteArray(),
                DisplayName = currentUser.Firstname + " " + currentUser.Lastname,
                Name = currentUser.UserName,
            },
            new List<PublicKeyCredentialDescriptor>(),
            authExtensionsClientInputs
        );

        signInManager.Context.Session.SetString(Fido2AttestationOptions, fidoCredentials.ToJson());

        return new WebauthnAttestationChallenge(fidoCredentials);
    }


    public async Task<User> AddWebAuthnAsync(Guid userId, string rawAttestationResponse, string description)
        {
            var currentUser = db.Users
                .Include(u => u.WebautnCredentials)
                .CurrentUser(userId)
                .Single();

            // Parse JSON Attestation
            var attestationResponse =
                JsonConvert.DeserializeObject<AuthenticatorAttestationRawResponse>(rawAttestationResponse);

            // Get the options we sent the client 
            var options =
                CredentialCreateOptions.FromJson(signInManager.Context.Session.GetString(Fido2AttestationOptions));
            signInManager.Context.Session.Remove(Fido2AttestationOptions);

            // Verify and Make Credentials
            IsCredentialIdUniqueToUserAsyncDelegate callback = async (IsCredentialIdUniqueToUserParams args) =>
                !await db.Users.AnyAsync(u => u.WebautnCredentials.Any(c => c.CredentialId == args.CredentialId));

            var success = await fido2.MakeNewCredentialAsync(attestationResponse, options, callback);

            if (success.Status != "ok")
                throw new ApiException(ErrorCode.UnknownError, "WebAuthn-Attestation Failed");

            // Save public key
            currentUser.WebautnCredentials.Add(new WebauthnPublicKeys
            {
                CredentialId = success.Result.CredentialId,
                PublicKey = success.Result.PublicKey,
                Counter = success.Result.Counter,
                Description = description,
            });

            // Enable TwoFactorEnabled
            currentUser.TwoFATypesEnabled |= TwoFATypes.WebAuthn;

            db.Update(currentUser);
            await db.SaveChangesAsync();

            if (!currentUser.TwoFATypesEnabled.HasFlag(TwoFATypes.TOTP))
                await userManager
                    .ResetAuthenticatorKeyAsync(
                        currentUser); // Lets SignInManager know 2FA is needed (i hate it, it's a cheap fix)

            await setTwoFAEnabledBasedOnTypesEnabled(currentUser);

            // Send info mail to user
            await emailSenderService.SendEmailAddMultiFactorAuthenticationAsync(currentUser.Email,
                                                                                currentUser.Firstname + " " + currentUser.Lastname);

            return currentUser;
        }

 
    private async Task setTwoFAEnabledBasedOnTypesEnabled(User currentUser)
    {
        var prev = db.ChangeTracker.QueryTrackingBehavior;
        db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        var result = await userManager
            .SetTwoFactorEnabledAsync(currentUser,
                                      currentUser.TwoFATypesEnabled.HasFlag(TwoFATypes.TOTP)
                                      || currentUser.TwoFATypesEnabled.HasFlag(TwoFATypes.WebAuthn));
        db.ChangeTracker.QueryTrackingBehavior = prev;

        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnknownError, "Unable to enable/disable 2FA");
    }

 
    public async Task<AssertionOptions> AssertionOptionsAsync()
    {
        var currentUser = await signInManager.GetTwoFactorAuthenticationUserAsync();

        if (currentUser == null)
            throw new ApiException(ErrorCode.UnknownError, "Current user for 2FA was not found");

        IEnumerable<PublicKeyCredentialDescriptor> existingCredentials = await db.Users.CurrentUser(currentUser.Id)
            .SelectMany(u => u.WebautnCredentials)
            .Select(c => c.CredentialId)
            .Select(c => new PublicKeyCredentialDescriptor(c))
            .ToListAsync();

        var extensions = new AuthenticationExtensionsClientInputs
        {
            SimpleTransactionAuthorization = "FIDO",
            GenericTransactionAuthorization = new TxAuthGenericArg
            {
                ContentType = "text/plain",
                Content = new byte[] { 0x46, 0x49, 0x44, 0x4F }
            },
            UserVerificationIndex = true,
            Location = true,
            UserVerificationMethod = true
        };

        var options = fido2.GetAssertionOptions(existingCredentials, UserVerificationRequirement.Preferred, extensions);

        signInManager.Context.Session.SetString(Fido2AssertionOptions, options.ToJson());

        return options;
    }

    public async Task<User> LoginUsingWebauthnAssertion(AuthenticatorAssertionRawResponse assertionResponse, bool isPersistent)
    {
        var currentUser = await signInManager.GetTwoFactorAuthenticationUserAsync();
        signInManager.Context.Response.Cookies.Delete(IdentityConstants.TwoFactorUserIdScheme);

        // Get the options we sent the client
        var jsonOptions = signInManager.Context.Session.GetString(Fido2AssertionOptions);
        if (jsonOptions == null)
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorFailed, "Fido2 login failed because no data was present in the session.");
        var options = AssertionOptions.FromJson(jsonOptions);

        // Get Credential
        var cred = await db.Users
            .CurrentUser(currentUser.Id)
            .SelectMany(u => u.WebautnCredentials)
            .SingleAsync(c => c.CredentialId == assertionResponse.Id);

        // Create callback to check if userHandle owns the credentialId
        async Task<bool> Callback(IsUserHandleOwnerOfCredentialIdParams args) =>
            await db.Users.CurrentUser(currentUser.Id)
                .SelectMany(u => u.WebautnCredentials)
                .AnyAsync(c => c.CredentialId == args.CredentialId);

        // Make the assertion
        var res = await fido2.MakeAssertionAsync(assertionResponse, options, cred.PublicKey, cred.Counter, Callback);

        // Trow Error when failing
        if (res.Status != "ok")
            throw new ApiException(ErrorCode.UnableToLogin_TwoFactorFailed, "WebAuthn-Assertion Failed");

        // Update Credential
        cred.Counter = res.Counter;

        db.Update(cred);
        await db.SaveChangesAsync();

        // Sign in
        await signInManager.SignInAsync(currentUser, isPersistent);

        signInManager.Context.Session.Clear();

        return currentUser;
    }

    public async Task ResetPassword(string email)
    {
        var user = db.Users.SingleOrDefault(u => u.NormalizedEmail == email.Normalize());

        if (user == null) return;

        if (!string.IsNullOrEmpty(user.SecurityQuestion1) && !string.IsNullOrEmpty(user.SecurityQuestion2) && !string.IsNullOrEmpty(user.SecurityQuestion3))
        {
            var passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(user);

            await emailSenderService.SendEmailPasswordReset(email, user.Id, $"{user.Firstname} {user.Lastname}", passwordResetToken,
                                                            user.SecurityQuestion1, user.SecurityQuestion2, user.SecurityQuestion3);
        }
        else
        {
            await emailSenderService.SendEmailPasswordResetUnavailable(email, $"{user.Firstname} {user.Lastname}");
        }
    }

    public async Task<string> GetEncryptedResetPasswordByToken(Guid userId, string token)
    {
        var user = GetUser(userId);

        var isTokenValid = await userManager.VerifyUserTokenAsync(user, userManager.Options.Tokens.PasswordResetTokenProvider, UserManager<User>.ResetPasswordTokenPurpose, token);
        if (!isTokenValid)
            throw new ApiException(ErrorCode.EmailTokenInvalid, "The password reset token is invalid or expired.");

        return user.EncryptedLoginPassword;
    }

    private async Task<bool> TwoFAWebauthnEnabled(User user) =>
        await userManager.GetTwoFactorEnabledAsync(user) && user.TwoFATypesEnabled.HasFlag(TwoFATypes.WebAuthn);

    #endregion

    public async Task RequestEmailConfirmationAsync(Guid userId)
    {
        var user = GetUser(userId);

        if (await userManager.IsEmailConfirmedAsync(user))
            throw new ApiException(ErrorCode.EmailAlreadyVerified, "This email is already verified");

        var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);
        await emailSenderService.SendConfirmEmailAsync(emailConfirmationToken, user.Id, user.Email, $"{user.Firstname} {user.Lastname}");
    }

    public async Task<(KeyPair, List<EncryptedSharedPassword>)> GetKeyPairAndSharedPasswordsViaEmailTokenAsync(Guid userId, string token, string oldPasswordHashed)
    {
        var user = await db.Users
            .CurrentUser(userId)
            .IncludeLastKeyPair()
            .SingleOrDefaultAsync();

        if (user is null)
            throw new ApiException(ErrorCode.DataNotFound, "No user with this ID can be found");

        if (!await userManager.CheckPasswordAsync(user, oldPasswordHashed)
            || !await userManager.VerifyUserTokenAsync(user, userManager.Options.Tokens.PasswordResetTokenProvider, UserManager<User>.ResetPasswordTokenPurpose, token))
        {
            throw new ApiException(ErrorCode.InputInvalid, "Token or password invalid.");
        }

        var keyPair = user.KeyPairs.LastActive();
        var encryptedPasswords = await db.EncryptedSharedPasswords
            .Include(esp => esp.SharedPassword)
            .ByKeyPairId(keyPair.Id)
            .GroupBy(enc => enc.SharedPasswordId, (_, g) => g
                         .OrderBy(enc => enc.CreatedAt)
                         .Last()
            )
            .ToListAsync();

        return (keyPair, encryptedPasswords);
    }

    public async Task<User> AddUserViaEmailTokenAsync(Guid userId, string token, string oldPassword, string newPassword,
        string publicKey, string encryptedPrivateKey, string previousPasswordHashEncryptedWithPublicKey, ICollection<EncryptedSharedPassword> encryptions)
    {
        var user = GetUser(userId);

        if (!await userManager.CheckPasswordAsync(user, oldPassword)
            || !await userManager.VerifyUserTokenAsync(user, userManager.Options.Tokens.PasswordResetTokenProvider, UserManager<User>.ResetPasswordTokenPurpose, token))
        {
            throw new ApiException(ErrorCode.InputInvalid, "Token or password invalid.");
        }

        // Check supplied encryptions
        var mandatorGroups = await db.Groups
            .RecursionBase(user.MandatorId)
            .ToListAsync();
        var accessibleGroups = GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, includeSubGroups: true, byArchived: null);
        var sharedPasswords = await db.SharedPasswords
            .ByGroupIds(accessibleGroups.Select(g => g.Id).ToList())
            .ToListAsync();
        var companyPasswords = await db.SharedPasswords
            .BaseCompany(user.MandatorId, null, true)
            .Where(pw => user.Admin || pw.CreatedById.HasValue && pw.CreatedById.Value == user.Id)
            .ToListAsync();
        sharedPasswords.AddRange(companyPasswords);
        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswords(sharedPasswords, encryptions))
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

        var identityResult = await userManager.ResetPasswordAsync(user, token, newPassword);
        if (!identityResult.Succeeded)
        {
            // Invalidates old tokens because those are contained in an email with an expired temp-password
            await userManager.UpdateSecurityStampAsync(user);

            var passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(user);
            await emailSenderService.SendEmailAfterUserAddedAsync(passwordResetToken, oldPassword,
                                                                  user.Id, user.Email, $"{user.Firstname} {user.Lastname}");

            throw new ApiException(ErrorCode.InputInvalid, "Unable to reset this password",
                                   new[] { identityResult.Errors.Aggregate("", (s, error) => s + error.Description) });
        }

        user.KeyPairs.Add(new KeyPair
        {
            PublicKeyString = publicKey,
            EncryptedPrivateKey = encryptedPrivateKey,
            PreviousPasswordHashEncryptedWithPublicKey = previousPasswordHashEncryptedWithPublicKey,

            EncryptedSharedPasswords = encryptions
                .Select(enc => new EncryptedSharedPassword
                {
                    SharedPasswordId = enc.SharedPasswordId,
                    EncryptedPasswordString = enc.EncryptedPasswordString,
                })
                .ToList()
        });

        var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);
        await userManager.ConfirmEmailAsync(user, emailConfirmationToken);

        await db.SaveChangesAsync();

        return user;
    }

    public async Task<User> ChangeLoginPasswordAsync(Guid userId,
        string newPublicKeyString, string newEncryptedPrivateKey,
        string previousPasswordHashEncryptedWithPublicKey, string currentPassword, string newPassword,
        ICollection<EncryptedSharedPassword> encryptedSharedPasswords, ICollection<EncryptedAccountPassword> encryptedAccountPasswords)
    {
        if (string.IsNullOrWhiteSpace(newPublicKeyString))
            throw new ApiException(ErrorCode.InputInvalid, "The publicKey string cannot be empty");
        var stringToCheck = newPublicKeyString.Replace("\r", "").Replace("\n", "");
        if (!stringToCheck.StartsWith("-----BEGIN RSA PUBLIC KEY-----") || !stringToCheck.EndsWith("-----END RSA PUBLIC KEY-----"))
            throw new ApiException(ErrorCode.InputInvalid, "The publicKey string is invalid");

        if (string.IsNullOrWhiteSpace(newEncryptedPrivateKey))
            throw new ApiException(ErrorCode.InputInvalid, "The encrypted privateKey string cannot be empty");
        if (string.IsNullOrWhiteSpace(previousPasswordHashEncryptedWithPublicKey))
            throw new ApiException(ErrorCode.InputInvalid, "The encrypted previous passwordHash string cannot be empty");

        if (string.IsNullOrWhiteSpace(newPassword))
            throw new ApiException(ErrorCode.InputInvalid, "The new password cannot be empty");
        if (string.IsNullOrWhiteSpace(currentPassword))
            throw new ApiException(ErrorCode.InputInvalid, "The current password cannot be empty");

        var user = GetUser(userId);

        // Check provided Encryptions
        var accountPasswords = await db.AccountPasswords
            .ByMe(userId)
            .ToListAsync();

        var mandatorGroups = await db.Groups
            .RecursionBase(user.MandatorId)
            .IncludeSharedPasswords()
            .ToListAsync();
        var accessibleGroups = GroupAuthorizations.GetGroupsForUser(mandatorGroups, user, includeSubGroups: true, byArchived: null);
        var sharedPasswordsToCheck = user.Admin
            ? mandatorGroups.SelectMany(g => g.SharedPasswords)
            : accessibleGroups.SelectMany(g => g.SharedPasswords);

        var companyPasswordsToCheck = user.Admin
            ? db.SharedPasswords.BaseCompany(user.MandatorId, null).AsEnumerable()
            : db.SharedPasswords.BaseCompany(user.MandatorId, null).ByCreatedBy(user.Id).AsEnumerable();

        var syncedOfflinePasswordsToCheck = db.SharedPasswords
            .BaseOffline(user.MandatorId)
            .ByCreatedBy(user.Admin ? null : new List<Guid> { user.Id })
            .AsEnumerable();

        var allEncryptionsToCheck = sharedPasswordsToCheck
            .Concat(companyPasswordsToCheck)
            .Concat(syncedOfflinePasswordsToCheck)
            .ToList();

        // Check if encryptions are provided
        if (!encryptionsCheckService.EncryptionsProvidedForAccountPasswords(accountPasswords, encryptedAccountPasswords)
            || !encryptionsCheckService.EncryptionsProvidedForSharedPasswords(allEncryptionsToCheck, encryptedSharedPasswords))
        {
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // add new Public Key
        var newKeyPair = new KeyPair
        {
            UserId = userId,
            PublicKeyString = newPublicKeyString,
            EncryptedPrivateKey = newEncryptedPrivateKey,
            PreviousPasswordHashEncryptedWithPublicKey = previousPasswordHashEncryptedWithPublicKey,

            EncryptedAccountPasswords = encryptedAccountPasswords
                .Select(enc => new EncryptedAccountPassword
                {
                    AccountPasswordId = enc.AccountPasswordId,
                    EncryptedPasswordString = enc.EncryptedPasswordString,
                })
                .ToList(),
            EncryptedSharedPasswords = encryptedSharedPasswords
                .Select(enc => new EncryptedSharedPassword
                {
                    SharedPasswordId = enc.SharedPasswordId,
                    EncryptedPasswordString = enc.EncryptedPasswordString,
                })
                .ToList()
        };
        await db.KeyPairs.AddAsync(newKeyPair);

        // Change login-password
        var result = await userManager.ChangePasswordAsync(user, currentPassword, newPassword);
        if (!result.Succeeded)
            throw new ApiException(
                ErrorCode.UnableToChangeLoginPassword,
                $"Unable to change password: current password is wrong or new password is invalid -> {result.Errors.Aggregate("", (acc, e) => acc + e.Description)}");

        await LogoutAsync();

        user.MarkPasswordChange();

        // Remove security questions
        user.EncryptedLoginPassword = null;
        user.SecurityQuestion1 = null;
        user.SecurityQuestion2 = null;
        user.SecurityQuestion3 = null;

        await db.SaveChangesAsync();
        await emailSenderService.SendEmailAfterUserChangedPasswordAsync(user.Id, user.Email, $"{user.Firstname} {user.Lastname}");

        return user;
    }

    public async Task<User> ValidateEmailConfirmationTokenAsync(Guid userId, string confirmationModelToken)
    {
        var user = GetUser(userId);

        if (await userManager.IsEmailConfirmedAsync(user))
            throw new ApiException(ErrorCode.EmailAlreadyVerified, "This email is already verified");

        var result = await userManager.ConfirmEmailAsync(user, confirmationModelToken);
        if (!result.Succeeded)
            throw new ApiException(ErrorCode.EmailTokenInvalid,
                                   "This email token is not valid or does not belong to this user",
                                   result.Errors.Select(e => e.Description));

        return user;
    }

    public async Task<User> RegisterAsync(User user, string registerFormPassword)
    {
        #region check input

        var reasonsFormDataIncorrect = userService.AreRegisterFieldsValid(user).ToList();

        if (!userService.IsPasswordValid(registerFormPassword))
            reasonsFormDataIncorrect.Add("Please specify a valid value for 'Password'. It must be at least 6 character long");
        if (user != null && string.IsNullOrEmpty(user.Mandator.Name))
            reasonsFormDataIncorrect.Add("Please add a value for 'NewCompanyName'");
        if (user != null && await db.Users.ExistEmailAsync(user.Email))
            reasonsFormDataIncorrect.Add("This email is already taken");
        if (user != null && db.Mandators.ExistCompanyName(user.Mandator.Name))
            reasonsFormDataIncorrect.Add("This company already exists in our database");
        if (reasonsFormDataIncorrect.Any() || user == null)
            throw new ApiException(ErrorCode.UnableToRegister, "Unable to register new user", reasonsFormDataIncorrect);

        #endregion

        // New user & mandator cannot exist, when Stripe creation fails (but userManager does not care -> transaction)
        await db.Database
            .CreateExecutionStrategy()
            .ExecuteAsync(async () =>
            {
                var transaction = await db.Database.BeginTransactionAsync();

                #region create DB data

                var res = await userManager.CreateAsync(user, registerFormPassword);
                if (!res.Succeeded)
                    throw new ApiException(ErrorCode.UnableToRegister, "Unable to register user", new List<string> { res.ToString() });

                logger.LogInformation($"A user was created with the email {user.Email} and Password {registerFormPassword}");

                #endregion

                #region send PW confirmation email

                var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(user);
                await emailSenderService.SendConfirmEmailAsync(emailConfirmationToken, user.Id, user.Email, $"{user.Firstname} {user.Lastname}");

                #endregion

                await transaction.CommitAsync();
            });

        return user;
    }

    public async Task<User> ChangeEmailAndUsernameViaTokenAsync(Guid userId, string newEmail, string confirmationToken)
    {
        var user = GetUser(userId);

        if (db.Users.Any(u => u.Email == newEmail))
            throw new ApiException(ErrorCode.InputInvalid, "Email already exists.");

        var changeEmailTokenPurpose = UserManager<User>.GetChangeEmailTokenPurpose(newEmail);
        var isTokenValid = await userManager.VerifyUserTokenAsync(user,
                                                                  userManager.Options.Tokens.ChangeEmailTokenProvider, changeEmailTokenPurpose, confirmationToken);
        if (!isTokenValid) throw new ApiException(ErrorCode.InputInvalid, "Token is not valid");

        var result = await userManager.ChangeEmailAsync(user, newEmail, confirmationToken);
        if (!result.Succeeded)
            throw new ApiException(ErrorCode.UnknownError, "Unable to change the email",
                                   result.Errors.Select(e => e.Description));

        // Changing the username must happen after changing the email because the email token was issued for the old username
        var changeUsernameResult = await userManager.SetUserNameAsync(user, newEmail);
        if (!changeUsernameResult.Succeeded)
            throw new ApiException(ErrorCode.UnknownError, "Unable to change the username",
                                   changeUsernameResult.Errors.Select(e => e.Description));
        await userManager.UpdateNormalizedUserNameAsync(user);

        await db.SaveChangesAsync();

        return user;
    }

    public async Task LogoutAsync()
    {
        await signInManager.SignOutAsync();
    }

    private User GetUser(Guid userId)
    {
        var user = db.Users.ById(userId).SingleOrDefault();
        if (user is null) throw new ApiException(ErrorCode.DataNotFound, "No user with this ID can be found");
        return user;
    }
}