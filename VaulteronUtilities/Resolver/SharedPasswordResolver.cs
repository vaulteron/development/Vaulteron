﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Models.MutationModels;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;

namespace VaulteronUtilities.Resolver;

public class SharedPasswordResolver : ISharedPasswordResolver
{
    private readonly VaulteronContext db;

    private readonly IPasswordsEncryptionsCheckService encryptionsCheckService;
    private readonly ISharedPasswordService sharedPasswordService;
    private readonly ISharedEncryptionsRepository encryptionsHelperRepository;
    private readonly IMandatorService mandatorService;
    private readonly IUserRepository userRepository;
    private readonly IUserService userService;

    public SharedPasswordResolver(VaulteronContext dbContext, IPasswordsEncryptionsCheckService encryptionsCheckService, ISharedPasswordService sharedPasswordService,
        ISharedEncryptionsRepository encryptionsHelperRepository, IMandatorService mandatorService, IUserRepository userRepository, IUserService userService)
    {
        db = dbContext;
        this.encryptionsCheckService = encryptionsCheckService;
        this.sharedPasswordService = sharedPasswordService;
        this.encryptionsHelperRepository = encryptionsHelperRepository;
        this.mandatorService = mandatorService;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public async Task<IEnumerable<SharedPassword>> GetGroupPasswordsAsync(Guid userId, SharedPasswordFilterType filter)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var groupsToView = GroupAuthorizations.GetGroupsForUser(mandatorGroups, currentUser, includeSubGroups: true, byArchived: filter.ByArchived).ToList();
        var filteredGroups = filter.GroupIds == null || !filter.GroupIds.Any()
            ? groupsToView
            : groupsToView.Where(g => filter.GroupIds.Contains(g.Id)).ToList();
        if (!filteredGroups.Any()) return new List<SharedPassword>();

        return await db.SharedPasswords
            .BaseGroup(currentUser.MandatorId, filter.ByArchived)
            .Where(p => filteredGroups.Contains(p.Group))
            .BySearchTerm(filter.SearchTerm)
            .AsNoTracking()
            .ToListAsync();
    }

    public async Task<IEnumerable<SharedPassword>> GetCompanyPasswordsAsync(Guid userId, SharedPasswordFilterType filter)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        return await db.SharedPasswords
            .BaseCompany(currentUser.MandatorId, filter.ByArchived)
            .ByCreatedBy(currentUser.Admin ? filter.CreatedByIds : new List<Guid> { currentUser.Id })
            .BySearchTerm(filter.SearchTerm)
            .AsNoTracking()
            .ToListAsync();
    }

    public async Task<IEnumerable<SharedPassword>> GetOfflinePasswordsAsync(Guid userId, SharedPasswordFilterType filter)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);

        return await db.SharedPasswords
            .BaseOffline(currentUser.MandatorId)
            .ByCreatedBy(currentUser.Admin ? filter.CreatedByIds : new List<Guid> { currentUser.Id })
            .BySearchTerm(filter.SearchTerm)
            .AsNoTracking()
            .ToListAsync();
    }

    public async Task<User> GetCreatedByUserAsync(Guid userId, Guid createdById)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        if (!currentUser.Admin && userId != createdById)
            throw new ApiException(ErrorCode.Forbidden, "Can not access createdBy");

        return await db.Users.Base(currentUser.MandatorId).ById(createdById).SingleAsync();
    }

    public async Task<string> GetEncryptedStringForSharedPasswordAsync(Guid userId, Guid passwordId)
    {
        var currentUser = await db.Users.CurrentUser(userId).IncludeGroups().SingleAsync();

        var sharedPassword = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(sp => sp.EncryptedSharedPasswords)
            .SingleOrDefaultAsync(p => p.Id == passwordId);

        return await GetEncryptedStringForSharedPasswordAsync(userId, sharedPassword);
    }

    public async Task<string> GetEncryptedStringForSharedPasswordAsync(Guid userId, SharedPassword sharedPassword)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        if (sharedPassword == null) throw new ApiException(ErrorCode.UserIsNotAuthorized);

        if (!sharedPasswordService.UserHasAuthorisationForSharedPassword(currentUser, sharedPassword, GroupRole.GroupViewer))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        var userCurrentKeyPairId = currentUser.KeyPairs.LastActive().Id;

        var encryptedPassword = sharedPassword
            .EncryptedSharedPasswords
            .Where(ep => ep.KeyPairId == userCurrentKeyPairId)
            .MaxBy(ep => ep.CreatedAt);
        if (encryptedPassword == null)
            throw new ApiException(ErrorCode.DataNotFound, "No encrypted password for this public key");

        db.SharedPasswordAccessLogs.Add(new SharedPasswordAccessLog
        {
            UserId = currentUser.Id,
            SharedPasswordId = sharedPassword.Id,
        });

        await db.SaveChangesAsync();

        return encryptedPassword.EncryptedPasswordString;
    }

    public async Task<SharedPassword> AddOfflinePasswordAsync(Guid userId, AddSharedPasswordModel passwordForm)
    {
        passwordForm.IsSavedAsOfflinePassword = true;
        return await AddSharedPasswordAsync(userId, passwordForm, null);
    }

    public async Task<SharedPassword> AddCompanyPasswordAsync(Guid userId, AddSharedPasswordModel passwordForm, Guid? overrideCreatedBy = null) =>
        await AddSharedPasswordAsync(userId, passwordForm, overrideCreatedBy);

    public async Task<SharedPassword> AddSharedPasswordAsync(Guid userId, AddSharedPasswordModel passwordForm) =>
        await AddSharedPasswordAsync(userId, passwordForm, null);

    private async Task<SharedPassword> AddSharedPasswordAsync(Guid userId, AddSharedPasswordModel passwordForm, Guid? overrideCreatedBy)
    {
        var baseUser = await db.Users.CurrentUser(userId).IncludeGroups().IncludeKeyPairs().SingleAsync();

        // Check permission
        var mandatorGroups = await db.Groups
            .RecursionBase(baseUser.MandatorId)
            .IncludeSharedPasswords()
            .IncludeUsersWithLastKeyPair()
            .ToListAsync();

        if (passwordForm.GroupId.HasValue
            && !GroupAuthorizations.UserHasGroupAuthorization(mandatorGroups, baseUser, passwordForm.GroupId.Value, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "You do not have permission to add");

        // Admins can create (Company) Passwords for other Users
        if (overrideCreatedBy.HasValue && !baseUser.Admin)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "You do not have permission to add");

        // Check if Encryptions provided
        var keyPairs = (await GetKeyPairsForSharedGroupOrCompanyAsync(baseUser, passwordForm.GroupId, overrideCreatedBy)).ToList();
        if (!encryptionsCheckService.EncryptionsProvidedForKeyPairs(keyPairs, passwordForm.EncryptedPasswords))
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

        // Create new Password
        var newSharedPassword = new SharedPassword
        {
            GroupId = passwordForm.GroupId,
            Name = passwordForm.Name,
            Login = passwordForm.Login,
            WebsiteURL = passwordForm.WebsiteUrl,
            Notes = passwordForm.Notes,
            CreatedById = overrideCreatedBy ?? baseUser.Id,
            MandatorId = baseUser.MandatorId,
            IsSavedAsOfflinePassword = passwordForm.IsSavedAsOfflinePassword
        };

        // Tags not supported By Company Passwords yet
        if (passwordForm.GroupId.HasValue && passwordForm.Tags != null && passwordForm.Tags.Count > 0)
        {
            var loadedTags = await db.SharedTags
                .ByMandatorId(baseUser.MandatorId)
                .ByGroupId(passwordForm.GroupId.Value)
                .ByIds(passwordForm.Tags)
                .ToListAsync();

            if (loadedTags.Count != passwordForm.Tags.Count)
                throw new ApiException(ErrorCode.DataNotFound, "Not all tags could be found");

            var newSharedPasswordTag = loadedTags
                .Select(tag => new SharedPasswordTag { Password = newSharedPassword, Tag = tag })
                .ToList();

            db.SharedPasswordTags.AddRange(newSharedPasswordTag);
        }

        // Create Encrypted PWs
        var newEncryptedPasswords = passwordForm.EncryptedPasswords
            .Select(esp => new EncryptedSharedPassword
            {
                KeyPairId = esp.KeyPairId,
                EncryptedPasswordString = esp.EncryptedPasswordString,
                SharedPassword = newSharedPassword,
                CreatedById = userId,
            }).ToList();

        newSharedPassword.EncryptedSharedPasswords = newEncryptedPasswords;
        newSharedPassword.SetNewModifyLock();

        db.SharedPasswords.Add(newSharedPassword);
        await db.SaveChangesAsync();

        return newSharedPassword;
    }

    public async Task<SharedPassword> BulkSyncEncryptedPasswordAsync(Guid userId, BulkSyncEncryptedSharedPasswordModel bulkSyncEncryptedSharedPasswordModel)
    {
        if (!bulkSyncEncryptedSharedPasswordModel.EncryptedPasswords.Any()
            || bulkSyncEncryptedSharedPasswordModel.EncryptedPasswords.Any(ep => string.IsNullOrEmpty(ep.EncryptedPasswordString)))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        var currentUser = await db.Users
            .CurrentUser(userId)
            .IncludeGroups()
            .SingleAsync();
        var mandatorGroups = db.Groups.RecursionBase(currentUser.MandatorId).ToList();

        var sharedPassword = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(sp => sp.Group)
            .ById(bulkSyncEncryptedSharedPasswordModel.SharedPasswordToAddTo)
            .SingleOrDefaultAsync();
        if (sharedPassword == null)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User can not access sharedPassword");
        if (sharedPassword.ArchivedAt != null)
            throw new ApiException(ErrorCode.DataExpired, "This password is archived and cannot not be modified until unarchived");

        if (!await sharedPasswordService.UserHasAuthorisationForSharedPasswordAsync(userId, bulkSyncEncryptedSharedPasswordModel.SharedPasswordToAddTo, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        var usersOfRequestedKeyPairs = await db.Users.Base(currentUser.MandatorId)
            .IncludeGroups()
            .IncludeKeyPairs()
            .ByKeyPairIds(bulkSyncEncryptedSharedPasswordModel.EncryptedPasswords
                              .Select(ep => ep.KeyPairId)
                              .ToList())
            .ToListAsync();
        if (usersOfRequestedKeyPairs.Any(user => !HasAuthorisationOrIsCompanyPasswordOwner(mandatorGroups, user, sharedPassword)))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "At least one of the users (of the supplied publicKeys) cannot access the sharedPassword");

        db.EncryptedSharedPasswords.AddRange(
            bulkSyncEncryptedSharedPasswordModel.EncryptedPasswords
                .Select(ep => new EncryptedSharedPassword
                {
                    KeyPairId = ep.KeyPairId,
                    EncryptedPasswordString = ep.EncryptedPasswordString,
                    SharedPasswordId = sharedPassword.Id,
                    CreatedById = userId,
                }));

        await db.SaveChangesAsync();

        return sharedPassword;
    }

    public async Task<SharedPassword> EditSharedPasswordAsync(Guid userId, EditSharedPasswordModel passwordModelForm)
    {
        if (string.IsNullOrWhiteSpace(passwordModelForm.ModifyLock) || (passwordModelForm.EncryptedPasswords != null && passwordModelForm.EncryptedPasswords.Count == 0))
            throw new ApiException(ErrorCode.InputInvalid, "Invalid form input");

        var baseUser = await db.Users.CurrentUser(userId).IncludeGroups().Include(u => u.KeyPairs).SingleAsync();

        // Load data necessary for checks  
        var loadedPassword = await db.SharedPasswords
            .ByMandatorId(baseUser.MandatorId)
            .Include(sp => sp.EncryptedSharedPasswords)
            .ThenInclude(ep => ep.KeyPair)
            .Include(sp => sp.SharedPasswordTags)
            .SingleOrDefaultAsync(p => p.Id == passwordModelForm.Id);

        // Check availability
        if (loadedPassword == null)
            throw new ApiException(ErrorCode.DataNotFound, "No password was found");

        if (loadedPassword.ArchivedAt != null)
            throw new ApiException(ErrorCode.DataExpired, "This password is archived and cannot be changed");

        // Check permission
        if (loadedPassword.ModifyLock != passwordModelForm.ModifyLock)
            throw new ApiException(ErrorCode.UnableToModifySharedPassword_ModifyLockInvalid, "The provided modify lock is invalid. Please pull again for the latest version");

        if (!sharedPasswordService.UserHasAuthorisationForSharedPassword(baseUser, loadedPassword, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        // Update TAGS - not supported with Company Passwords
        if (loadedPassword.GroupId.HasValue)
        {
            // TODO check for specific tags? currently every tag of a mandator is possible
            var loadedTags = await db.SharedTags
                .ByMandatorId(baseUser.MandatorId)
                .Include(t => t.Group)
                .ByIds(passwordModelForm.Tags)
                .ToListAsync();

            // Should be working, not tested^^
            var newSharedPasswordTags = new List<SharedPasswordTag>();
            foreach (var sharedTag in loadedTags)
            {
                var lookForSharedPasswordTag = loadedPassword.SharedPasswordTags.SingleOrDefault(t => t.TagId == sharedTag.Id);
                newSharedPasswordTags.Add(lookForSharedPasswordTag ?? new SharedPasswordTag { Tag = sharedTag, Password = loadedPassword });
            }

            loadedPassword.SharedPasswordTags = newSharedPasswordTags;
        }

        // Update Properties
        if (!string.IsNullOrWhiteSpace(passwordModelForm.Name)) loadedPassword.Name = passwordModelForm.Name;
        if (passwordModelForm.Login != null) loadedPassword.Login = passwordModelForm.Login;
        if (passwordModelForm.WebsiteURL != null) loadedPassword.WebsiteURL = passwordModelForm.WebsiteURL;
        if (passwordModelForm.Notes != null) loadedPassword.Notes = passwordModelForm.Notes;

        // Update Encrypted PWs (password itself is changed)
        if (passwordModelForm.EncryptedPasswords?.Count > 0)
        {
            // Check encryptions
            var keyPairs =
                (await GetKeyPairsForSharedGroupOrCompanyAsync(baseUser, loadedPassword.GroupId, loadedPassword.CreatedById))
                .ToList();

            if (!encryptionsCheckService.EncryptionsProvidedForKeyPairs(keyPairs, passwordModelForm.EncryptedPasswords))
                throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

            // Save updated encrypted PWs
            db.EncryptedSharedPasswords.AddRange(
                passwordModelForm.EncryptedPasswords
                    .Select(ep => new EncryptedSharedPassword
                    {
                        SharedPassword = loadedPassword,
                        KeyPairId = ep.KeyPairId,
                        EncryptedPasswordString = ep.EncryptedPasswordString,
                        CreatedById = userId,
                    }));
        }

        db.SharedPasswords.Update(loadedPassword);
        await db.SaveChangesAsync();

        return loadedPassword;
    }

    public async Task<IEnumerable<SharedPassword>> BulkMoveSharedPasswordsBetweenGroupsAsync(Guid userId, BulkMoveSharedPasswordModel movePasswordModel)
    {
        if (!movePasswordModel.PasswordIds.Any())
            throw new ApiException(ErrorCode.InputInvalid, "No passwords or encryptions supplied");

        var currentUser = await userRepository.GetUserByIdAsync(userId);

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var passwordsToMove = await db.SharedPasswords
            .BaseGroup(currentUser.MandatorId)
            .Where(sp => movePasswordModel.PasswordIds.Contains(sp.Id))
            .ToListAsync();

        var destinationGroup = allGroups
            .SingleOrDefault(g => g.Id == movePasswordModel.DestinationGroupId);

        // Check authorization
        if (passwordsToMove.Any(sp => !GroupAuthorizations.UserHasGroupAuthorization(allGroups, currentUser, sp.GroupId!.Value, GroupRole.GroupEditor)))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move at least one of the given passwords.");

        if (!GroupAuthorizations.UserHasGroupAuthorization(allGroups, currentUser, movePasswordModel.DestinationGroupId, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move passwords to this group.");

        // Check validity
        if (!passwordsToMove.Any() || destinationGroup == null)
            throw new ApiException(ErrorCode.InputInvalid, "Invalid parameters");

        // Check encryptions
        var keyPairsForUsersInTargetGroup = await GetKeyPairsForSharedGroupOrCompanyAsync(currentUser, movePasswordModel.DestinationGroupId, null);
        var targetGroupsIds = passwordsToMove.Select(pw => pw.GroupId);
        var keyPairsForUsersInParentGroupWithAdmins = new List<KeyPair>();
        foreach (var targetGroupsId in targetGroupsIds)
            keyPairsForUsersInParentGroupWithAdmins.AddRange(await GetKeyPairsForSharedGroupOrCompanyAsync(currentUser, targetGroupsId, null));
        var keyPairs = keyPairsForUsersInTargetGroup.Where(kp => !keyPairsForUsersInParentGroupWithAdmins.Contains(kp));
        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswordsAndKeyPairs(passwordsToMove, keyPairs, movePasswordModel.EncryptedPasswords))
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);

        // Update DB
        foreach (var pw in passwordsToMove)
        {
            pw.GroupId = movePasswordModel.DestinationGroupId;
        }

        var keyPairIds = movePasswordModel.EncryptedPasswords.Select(esp => esp.KeyPairId).ToList();
        encryptionsHelperRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(movePasswordModel.EncryptedPasswords, keyPairIds);
        await db.SaveChangesAsync();

        return passwordsToMove;
    }

    public async Task<IEnumerable<SharedPassword>> BulkMoveGroupToCompanyPasswordsAsync(Guid userId, List<Guid> passwordsToMoveIds, Guid userToMoveToId,
        List<EncryptedSharedPassword> encryptedSharedPasswords)
    {
        var currentUser = await db.Users
            .CurrentUser(userId)
            .SingleAsync();

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var passwordsToMove = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(p => p.EncryptedSharedPasswords)
            .ByArchived(false)
            .ByIds(passwordsToMoveIds)
            .ToListAsync();

        // Check validity
        if (!db.Users.ByMandatorId(currentUser.MandatorId).ById(userToMoveToId).Any())
            throw new ApiException(ErrorCode.InputInvalid, "User to move to does not exist");

        // Check authorization
        if (passwordsToMove.Any(sp => !sp.IsSavedAsOfflinePassword && !GroupAuthorizations.UserHasGroupAuthorization(allGroups, currentUser, sp.GroupId!.Value, GroupRole.GroupEditor)))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move at least one of the given passwords.");

        if (!currentUser.Admin && userId != userToMoveToId)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move passwords to this user.");

        // Check encrypted PW 
        var keyPairs = await GetKeyPairsForSharedGroupOrCompanyAsync(currentUser, null, userToMoveToId);

        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswordsAndKeyPairs(passwordsToMove, keyPairs, encryptedSharedPasswords))
        {
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // Update DB
        foreach (var p in passwordsToMove)
        {
            p.CreatedById = userToMoveToId;
            p.GroupId = null;
            p.IsSavedAsOfflinePassword = false;
        }

        var keyPairIds = encryptedSharedPasswords.Select(esp => esp.KeyPairId).ToList();
        encryptionsHelperRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(encryptedSharedPasswords, keyPairIds);
        await db.SaveChangesAsync();

        return passwordsToMove;
    }

    public async Task<IEnumerable<SharedPassword>> BulkMoveCompanyToGroupPasswordsAsync(Guid userId, List<Guid> passwordsToMoveIds, Guid groupToMoveToId,
        List<EncryptedSharedPassword> encryptedSharedPasswords)
    {
        var currentUser = await db.Users
            .CurrentUser(userId)
            .SingleAsync();

        var allGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();

        var passwordsToMove = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(p => p.EncryptedSharedPasswords)
            .ByArchived(false)
            .ByIds(passwordsToMoveIds)
            .ToListAsync();

        // Check validity
        if (allGroups.All(g => g.Id != groupToMoveToId))
            throw new ApiException(ErrorCode.InputInvalid, "Group to move to does not exist");

        // Check authorization
        if (!currentUser.Admin && passwordsToMove.Any(sp => sp.CreatedById != userId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move at least one of the given passwords.");

        if (!GroupAuthorizations.UserHasGroupAuthorization(allGroups, currentUser, groupToMoveToId, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "User is not allowed to move passwords to this group.");

        // Check Encryptions
        var keyPairs = await GetKeyPairsForSharedGroupOrCompanyAsync(currentUser, groupToMoveToId, null);

        if (!encryptionsCheckService.EncryptionsProvidedForSharedPasswordsAndKeyPairs(passwordsToMove, keyPairs, encryptedSharedPasswords))
        {
            throw new ApiException(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly);
        }

        // Update DB
        foreach (var p in passwordsToMove)
        {
            p.GroupId = groupToMoveToId;
            p.IsSavedAsOfflinePassword = false;
        }

        var keyPairIds = encryptedSharedPasswords.Select(esp => esp.KeyPairId).ToList();
        encryptionsHelperRepository.AddEncryptionsToDatabaseAndIgnoreExistingOnes(encryptedSharedPasswords, keyPairIds);
        await db.SaveChangesAsync();

        return passwordsToMove;
    }

    public async Task<SharedPassword> DeleteSharedPasswordAsync(Guid userId, Guid passwordId)
    {
        var baseUser = await userRepository.GetUserByIdAsync(userId);

        var loadedPassword = await db.SharedPasswords
            .ByMandatorId(baseUser.MandatorId)
            .SingleOrDefaultAsync(p => p.Id == passwordId);

        if (loadedPassword == null)
            throw new ApiException(ErrorCode.UserIsNotAuthorized, "Password does not exist or you do not have sufficient permissions to remove it");

        if (!sharedPasswordService.UserHasAuthorisationForSharedPassword(baseUser, loadedPassword, GroupRole.GroupEditor))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        if (loadedPassword.IsSavedAsOfflinePassword)
        {
            db.SharedPasswords.Remove(loadedPassword);
            await db.SaveChangesAsync();
        }
        else
        {
            loadedPassword.ArchivedAt = DateTime.UtcNow;
            db.SharedPasswords.Update(loadedPassword);
            await db.SaveChangesAsync();
        }

        return loadedPassword;
    }

    public async Task<IEnumerable<SharedPasswordAccessLog>> GetAccessLogForSharedPasswordAsync(Guid userId, Guid passwordId)
    {
        var accessLogs = db.SharedPasswordAccessLogs
            .IncludeUser()
            .ByPasswordId(passwordId);

        if (!await sharedPasswordService.UserHasAuthorisationForSharedPasswordAsync(userId, passwordId, GroupRole.GroupAdmin))
        {
            accessLogs = accessLogs
                .ByUserId(userId);
        }

        return accessLogs
            .OrderByDescending(al => al.CreatedAt)
            .Take(100)
            .ToList();
    }

    public async Task<IEnumerable<Tag>> GetTagsForSharedPasswordAsync(Guid userId, Guid passwordId)
    {
        var currentUser = await db.Users.CurrentUser(userId).IncludeGroups().SingleAsync();

        var passwords = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(t => t.SharedPasswordTags)
            .ThenInclude(t => t.Tag)
            .SingleOrDefaultAsync(p => p.Id == passwordId);

        if (passwords is null) return Array.Empty<Tag>();

        return passwords.SharedPasswordTags.Select(pwAccountPasswordTag => pwAccountPasswordTag.Tag).Cast<Tag>().ToList();
    }

    public async Task<List<KeyPair>> GetMissingEncryptionsForPasswordAsync(Guid userId, Guid sharedPasswordId)
    {
        var currentUser = await db.Users.CurrentUser(userId).IncludeGroups().SingleAsync();

        // Check if requesting user can access this sharedPassword and fetch it
        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();
        var groupsToView =
            GroupAuthorizations.GetGroupsForUser(mandatorGroups, currentUser, includeSubGroups: true);
        var sharedPassword = await db.SharedPasswords
            .ByMandatorId(currentUser.MandatorId)
            .Include(sp => sp.EncryptedSharedPasswords)
            .ThenInclude(esp => esp.KeyPair)
            .Where(p => groupsToView.Contains(p.Group))
            .SingleOrDefaultAsync(p => p.Id == sharedPasswordId);
        if (sharedPassword == null) throw new ApiException(ErrorCode.UserIsNotAuthorized);

        // Get list of all users with access to this sharedPassword (including admins)
        var usersWithAccess = sharedPassword.GroupId.HasValue
            ? await userService.GetUsersWithAccessToGroupIncludingAdminsAsync(sharedPassword.GroupId.Value)
            : (await userRepository.GetAdminsAsync()).Append(currentUser);
        var usersIds = usersWithAccess.Select(u => u.Id).ToList();
        // Get all keyPairs for these users
        var keyPairs = db.KeyPairs.Where(pk => usersIds.Contains(pk.UserId)).ToList();

        // Get all encryptedPasswords for this sharedPassword and that have been updated later than the sharedPassword
        var encryptedSharedPasswords = await db.EncryptedSharedPasswords
            .Include(esp => esp.KeyPair)
            .Where(esp => esp.SharedPasswordId == sharedPasswordId && esp.UpdatedAt >= sharedPassword.UpdatedAt)
            .ToListAsync();
        var upToDateKeyPairIds = encryptedSharedPasswords.Select(esp => esp.KeyPairId).ToList();

        // Now filter out these correct encryptedPassword from the keyPair list
        var missingKeyPairs = keyPairs.Where(kp => !upToDateKeyPairIds.Contains(kp.Id)).ToList();
        return missingKeyPairs;
    }

    /// <summary>
    /// Returns a list of Guids of SharedPassword for which no encryption exists for the latest keyPair of the supplied user
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="userWithMissingEncryptionsId"></param>
    /// <returns>List of sharedPasswordGuid</returns>
    public async Task<List<Guid>> GetMissingEncryptionsForUserAsync(Guid userId, Guid userWithMissingEncryptionsId)
    {
        var currentUser = await userRepository.GetUserByIdAsync(userId);
        var targetUser = await db.Users.CurrentUser(userWithMissingEncryptionsId)
            .IncludeGroups()
            .Include(u => u.KeyPairs)
            .SingleAsync();
        var mandatorGroups = await db.Groups
            .RecursionBase(currentUser.MandatorId)
            .ToListAsync();
        var groupsToViewForTargetUser = GroupAuthorizations.GetGroupsForUser(mandatorGroups, targetUser, includeSubGroups: true).ToList();

        List<Guid> groupIdsBothHaveInCommon = null;
        if (currentUser.Admin)
        {
            groupIdsBothHaveInCommon = groupsToViewForTargetUser.Select(u => u.Id).ToList();
        }
        else
        {
            #region if not admin: get all upper-most groups both users have in common

            // First find all the groups that both users have in common, only taking the upper-most group

            var groupsToViewForCurrentUser = GroupAuthorizations.GetGroupsForUser(mandatorGroups, currentUser, includeSubGroups: true);
            var groupsWithPasswordHasAccessTo = groupsToViewForCurrentUser
                .Select(u => u.Id)
                .Union(groupsToViewForTargetUser.Select(u => u.Id))
                .ToList();
            groupIdsBothHaveInCommon = groupsWithPasswordHasAccessTo;

            #endregion
        }

        var keyPairIdOfTargetUser = (await userRepository.GetUserByIdAsync(targetUser.Id)).KeyPairs.LastActive().Id;
        var sharedPasswordsOfTargetUser = await db.SharedPasswords
            .BaseGroup(currentUser.MandatorId)
            .Include(sp => sp.EncryptedSharedPasswords)
            .Where(sp => sp.GroupId.HasValue && groupIdsBothHaveInCommon.Contains(sp.GroupId.Value))
            .ToListAsync();
        if (currentUser.Admin)
        {
            var companyPasswords = await db.SharedPasswords
                .BaseCompany(currentUser.MandatorId)
                .Include(sp => sp.EncryptedSharedPasswords)
                .ToListAsync();
            if (companyPasswords.Count > 0)
            {
                sharedPasswordsOfTargetUser = sharedPasswordsOfTargetUser
                    .Union(companyPasswords)
                    .ToList();
            }
        }

        var sharedPasswordOfTargetUserWithMissingEncryptionOnly = new List<SharedPassword>();
        foreach (var sharedPassword in sharedPasswordsOfTargetUser)
        {
            // Filter for target user
            var foundExistingEncryptedPassword = sharedPassword.EncryptedSharedPasswords
                .ByKeyPairId(keyPairIdOfTargetUser)
                .Latest()
                .Any();
            if (!foundExistingEncryptedPassword)
                sharedPasswordOfTargetUserWithMissingEncryptionOnly.Add(sharedPassword);
        }

        return sharedPasswordOfTargetUserWithMissingEncryptionOnly.Select(sp => sp.Id).ToList();
    }

    #region local helpers

    private async Task<IEnumerable<KeyPair>> GetKeyPairsForSharedGroupOrCompanyAsync(User baseUser, Guid? groupId,
        Guid? overrideCreatedBy)
    {

        if (groupId.HasValue)
        {
            return (await userService.GetUsersWithAccessToGroupIncludingAdminsAsync(groupId.Value))
                .Select(ug => ug.KeyPairs.LastActive());
        }

        return db.Users
            .ByMandatorId(baseUser.MandatorId)
            .Where(u => u.Admin)
            .Union(
                db.Users
                    .ByMandatorId(baseUser.MandatorId)
                    .ById(overrideCreatedBy ?? baseUser.Id)
            )
            .Select(u => u.KeyPairs.LastActive());
    }

    private bool HasAuthorisationOrIsCompanyPasswordOwner(List<Group> mandatorGroups, User user,
        SharedPassword password,
        GroupRole requiredRole = GroupRole.GroupViewer)
    {
        return password.GroupId.HasValue
            ? GroupAuthorizations.UserHasGroupAuthorization(mandatorGroups, user, password.GroupId.Value,
                                                            requiredRole)
            : user.Admin || password.CreatedById == user.Id;
    }

    #endregion
}