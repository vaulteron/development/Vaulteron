﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using Stripe;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronUtilities.Resolver;

public class MandatorResolver : IMandatorResolver
{
    private readonly VaulteronContext db;
    private readonly IMandatorService mandatorService;
    private readonly IUserService userService;
    private readonly IMandatorRepository mandatorRepository;
    private readonly IUserRepository userRepository;
    private readonly IInvoiceService invoiceService;

    public MandatorResolver(VaulteronContext dbContext, IUserRepository userRepository, IInvoiceService invoiceService, IUserService userService,
        IMandatorRepository mandatorRepository, IMandatorService mandatorService)
    {
        db = dbContext;
        this.userService = userService;
        this.mandatorRepository = mandatorRepository;
        this.userRepository = userRepository;
        this.invoiceService = invoiceService;
        this.mandatorService = mandatorService;
    }

    public async Task<string> GetPaymentProviderCustomerPortalLinkAsync(Guid userId)
    {
        var user = await userRepository.GetUserByIdAsync(userId);

        if (!user.Admin)
            throw new ApiException(ErrorCode.Forbidden, "Only admins can access the subscription customer portal.");

        if (user.Mandator.PaymentProviderId == null)
            throw new ApiException(ErrorCode.DataNotFound, "");

        return invoiceService.CustomerPortalUrl(user.Mandator.PaymentProviderId);
    }

    public async Task<string> GetPaymentProviderCheckoutSessionLinkAsync(Guid userId)
    {
        var user = await userRepository.GetUserByIdAsync(userId);

        if (!user.Admin)
            throw new ApiException(ErrorCode.Forbidden, "Only admins can access the subscription customer portal.");

        if (user.Mandator.PaymentProviderId == null)
            throw new ApiException(ErrorCode.DataNotFound, "");

        return invoiceService.CheckoutUrl(user.Mandator.PaymentProviderId);
    }

    public async Task<string> GetPaymentProviderClientSecretAsync(Guid userId)
    {
        var user = await userRepository.GetUserByIdAsync(userId, true);

        var clientSecret = invoiceService.ClientSecret(user.Mandator.PaymentProviderId);

        user.Mandator.PaymentProviderClientSecret = clientSecret;
        await db.SaveChangesAsync();

        return clientSecret;
    }

    public async Task<Mandator> ChangePlan(SetupIntent setupIntent)
    {
        var mandator = await db.Mandators.SingleOrDefaultAsync(m =>  m.PaymentProviderClientSecret == setupIntent.ClientSecret);

        if (mandator.SubscriptionStatus != LicenceStatus.None)
            throw new ApiException(ErrorCode.Forbidden, "You already have a license");

        var paymentProviderId = invoiceService.CreateCustomer(mandator);
        mandator.PaymentProviderId = paymentProviderId;
        mandator.IsBusinessCustomer = true;
        mandator.SubscriptionStatus = LicenceStatus.Standard;

        await db.SaveChangesAsync();

        invoiceService.CreateDefaultSubscription(paymentProviderId, setupIntent.PaymentMethodId, mandator.SubscriptionUserLimit);

        return mandator;
    }

    public async Task<Mandator> DeleteMandatorAsync(Guid userId, Guid mandatorId)
    {
        if (!await userService.UserIsSuperAdminAsync(userId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        if (await userService.UserIsOfMandatorAsync(userId, mandatorId))
            throw new ApiException(ErrorCode.ForbiddenSelfOperation);

        Mandator mandator = null;

        await db.Database.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            var transaction = await db.Database.BeginTransactionAsync();

            mandator = await mandatorService.DeleteMandatorAsync(mandatorId);
            await db.SaveChangesAsync();

            if (mandator.PaymentProviderId != null)
                invoiceService.DeleteCustomer(mandator.PaymentProviderId);

            await transaction.CommitAsync();
        });

        return mandator;
    }

    public async Task<ICollection<Mandator>> GetAllMandatorsAsync(Guid userId)
    {
        if (!await userService.UserIsSuperAdminAsync(userId))
            throw new ApiException(ErrorCode.UserIsNotAuthorized);

        return await mandatorRepository.GetAllMandatorsAsync();
    }

    public async Task<Customer> GetCustomerInformationAsync(Guid userId)
    {
        var user = await userRepository.GetUserByIdAsync(userId);
        if (user.Mandator.PaymentProviderId == null) return null;
        return invoiceService.GetCustomer(user.Mandator.PaymentProviderId); 
    }

    public Mandator GetMandator(Guid userId, Guid mandatorGuid)
    {
        throw new NotImplementedException();
    }

    public Mandator ArchiveMandator(Guid userId, Guid mandatorGuid)
    {
        throw new NotImplementedException();
    }

    public async Task<Mandator> EditMandator(Guid userId, bool twoFactorRequired)
    {
        var user = await userRepository.GetUserByIdAsync(userId, true);
        if (!user.Admin) return user.Mandator;

        user.Mandator.TwoFactorRequired = twoFactorRequired;
        await db.SaveChangesAsync();

        return user.Mandator;
    }
}