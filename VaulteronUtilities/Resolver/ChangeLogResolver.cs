﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Models;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Resolver;

public class ChangeLogResolver : IChangeLogResolver
{
    private readonly VaulteronContext db;
    private readonly IUserRepository userRepository;
    private readonly ICurrentUserClaimsService claims;

    public ChangeLogResolver(VaulteronContext dbContext, IUserRepository userRepository, ICurrentUserClaimsService claims)
    {
        db = dbContext;
        this.userRepository = userRepository;
        this.claims = claims;
    }

    public async Task<IEnumerable<GraphQLChangeLogModel>> GetChangesAsync(Guid entityId, string propertyName = null, DateTime? from = null, DateTime? to = null)
    {
        var currentUser = await userRepository.GetUserByIdAsync(claims.UserId);
        var accountPasswordName = nameof(AccountPassword);

        var entitiesFromDB = await db.ChangeLogs
            .Base(currentUser.MandatorId)
            .IncludeUser()
            .ByEntity(entityId)
            .ByProperty(propertyName)
            .ByCreatedAtBetween(from, to)
            .Where(cl => cl.EntityName == accountPasswordName ? cl.CreatedById == claims.UserId : currentUser.Admin)
            .ToListAsync();

        return entitiesFromDB
            .GroupBy(cl => MapEntityAndPropertyNameToClassName(cl.EntityName, cl.PropertyName), cl => cl)
            .SelectMany(grouping =>
            {
                switch (grouping.Key)
                {
                    case nameof(User):
                        var users = db.Users
                            .ByUserIds(GuidsFromGrouping(grouping))
                            .ToDictionary(u => u.Id, u => u.Firstname + " " + u.Lastname);

                        return grouping.Select(cl => new GraphQLChangeLogModel(cl, users));

                    case nameof(Group):
                        var groups = db.Groups
                            .ByIds(GuidsFromGrouping(grouping))
                            .ToDictionary(g => g.Id, g => g.Name);

                        return grouping.Select(cl => new GraphQLChangeLogModel(cl, groups));

                    case nameof(SharedTag):
                        var sharedTags = db.SharedTags
                            .ByIds(GuidsFromGrouping(grouping))
                            .ToDictionary(st => st.Id, st => st.Name);

                        return grouping.Select(cl => new GraphQLChangeLogModel(cl, sharedTags));

                    case nameof(AccountTag):
                        var accountTags = db.AccountTags
                            .ByIds(GuidsFromGrouping(grouping))
                            .ToDictionary(at => at.Id, at => at.Name);

                        return grouping.Select(cl => new GraphQLChangeLogModel(cl, accountTags));

                    default:
                        return grouping.Select(cl => new GraphQLChangeLogModel(cl));
                }
            })
            .OrderByDescending(cl => cl.CreatedAt)
            .ToList();
    }

    #region helper

    private static string MapEntityAndPropertyNameToClassName(string entityName, string propertyName)
    {
        return (entityName, propertyName) switch
        {
            (nameof(SharedPassword), nameof(SharedPassword.SharedPasswordTags)) => nameof(SharedTag),
            (nameof(SharedPassword), nameof(SharedPassword.GroupId)) => nameof(Group),
            (nameof(SharedPassword), nameof(SharedPassword.CreatedById)) => nameof(User),

            (nameof(AccountPassword), nameof(AccountPassword.AccountPasswordTags)) => nameof(AccountTag),
            (nameof(AccountPassword), nameof(AccountPassword.OwnerId)) => nameof(User),

            (_, _) => null,
        };
    }

    private static List<Guid> GuidsFromGrouping(IEnumerable<ChangeLog> g)
    {
        return g.SelectMany(cl => new[] {cl.NewValue, cl.OldValue})
            .Where(guidString => guidString != null)
            .Distinct()
            .Select(Guid.Parse)
            .ToList();
    }

    #endregion
}