﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using VaulteronUtilities.Helper;

namespace VaulteronUtilities;

public class GraphQLUserContext : Dictionary<string, object>
{
    public ClaimsPrincipal User { get; set; }
    public Guid UserId => new(User.Identity?.Name ?? throw new ApiException(ErrorCode.UserIdWasNotPassedWithHttpRequest, "UserId was not passed to request"));
    public GraphQLUserContext(ClaimsPrincipal user)
    {
        User = user;
    }
}