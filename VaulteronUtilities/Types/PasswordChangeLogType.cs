﻿using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class PasswordChangeLogType : EntityType<EncryptedPassword>
{
    public PasswordChangeLogType()
    {
        Field(ep => ep.EncryptedPasswordString);
        Field(ep => ep.KeyPairId);

        Field(f => f.CreatedById, nullable: true);
        Field(f => f.CreatedBy, type: typeof(UserType));
    }
}