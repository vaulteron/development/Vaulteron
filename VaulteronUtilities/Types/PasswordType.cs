﻿using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types
{
    public sealed class PasswordType : EntityType<Password>
    {
        public PasswordType()
        {
            Field(f => f.Name);
            Field(f => f.Login);
            Field(f => f.WebsiteURL);
            Field(f => f.Notes);

            Field<StringGraphType>()
                .Name("modifyLock")
                .Resolve(context =>
                {
                    if (context.Source is SharedPassword sharedPassword)
                        return sharedPassword.ModifyLock;
                    return null;
                });

            Field<KindOfPasswordType>()
                .Name("type")
                .Resolve(context =>
                {
                    if (context.Source is SharedPassword sharedPassword)
                        return sharedPassword.GroupId == null ? KindOfPassword.Company : KindOfPassword.Group;
                    return KindOfPassword.Private;
                });
        }
    }
}