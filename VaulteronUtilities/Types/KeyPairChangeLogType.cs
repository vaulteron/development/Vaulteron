﻿using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class KeyPairChangeLogType : EntityType<KeyPair>
{
    public KeyPairChangeLogType()
    {
        Field(kp => kp.PublicKeyString);
        Field(kp => kp.EncryptedPrivateKey);
        Field(kp => kp.PreviousPasswordHashEncryptedWithPublicKey, nullable: true);
    }
}