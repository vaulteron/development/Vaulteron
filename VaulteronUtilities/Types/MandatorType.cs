﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types.Base;
using VaulteronUtilities.Types.Stripe;

namespace VaulteronUtilities.Types;

public sealed class MandatorType : EntityType<Mandator>
{
    public MandatorType()
    {
        Field(f => f.Name);
        Field(f => f.TwoFactorRequired);
        Field(f => f.SubscriptionUserLimit);
        Field(f => f.IsBusinessCustomer);
        Field<NonNullGraphType<SubscriptionStatusType>>()
            .Name("SubscriptionStatus")
            .Resolve(context => context.Source.SubscriptionStatus);
        Field<StripeCustomerType>()
            .Name("customerinformation")
            .Resolve()
            .WithScope()
            .WithService<IMandatorResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetCustomerInformationAsync(context.GetUserGuid()));
    }
}