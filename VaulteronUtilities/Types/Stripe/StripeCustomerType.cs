﻿using GraphQL.Types;
using Stripe;

namespace VaulteronUtilities.Types.Stripe;

public sealed class StripeCustomerType : ObjectGraphType<Customer>
{
    public StripeCustomerType()
    {
        Field(f => f.Id, nullable: true);
        Field(f => f.Email, nullable: true);
        Field(f => f.Name, nullable: true);
        Field(f => f.InvoiceSettings.DefaultPaymentMethodId, nullable: true);
    }
}