﻿using GraphQL.Types;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Types;

public class SubscriptionStatusType : EnumerationGraphType<LicenceStatus>
{
    public SubscriptionStatusType()
    {
        Description = "Current subscription of mandator.";
    }
}