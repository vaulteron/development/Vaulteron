﻿using GraphQL.Types;
using VaulteronUtilities.Models.MutationModels;

namespace VaulteronUtilities.Types.MutationResults;

public sealed class WebauthnChallengeType : ObjectGraphType<WebauthnAttestationChallenge>
{
    public WebauthnChallengeType()
    {
        Field(f => f.OptionsJson);
        Field(f => f.Challenge);
    }
}