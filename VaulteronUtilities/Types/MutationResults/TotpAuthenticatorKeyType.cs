﻿using GraphQL.Types;
using VaulteronUtilities.Models.MutationModels;

namespace VaulteronUtilities.Types.MutationResults;

public sealed class TotpAuthenticatorKeyType : ObjectGraphType<ToptAuthenticatorKey>
{
    public TotpAuthenticatorKeyType()
    {
        Field(f => f.Key);
        Field(f => f.AuthenticatorString);
    }
}