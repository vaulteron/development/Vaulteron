﻿using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class PublicKeyType : EntityType<KeyPair>
{
    public PublicKeyType()
    {
        Field(f => f.PublicKeyString);
    }
}