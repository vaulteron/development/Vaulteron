﻿using GraphQL.Types;
using VaulteronUtilities.Models;

namespace VaulteronUtilities.Types;

public sealed class ChangeLogType : ObjectGraphType<GraphQLChangeLogModel>
{
    public ChangeLogType()
    {
        Field(f => f.Id);
        Field(f => f.EntityName);
        Field(f => f.PropertyName);
        Field(f => f.CreatedAt);

        Field(f => f.CreatedById);
        Field(f => f.CreatedBy, type: typeof(UserType));

        Field(f => f.OldValue, type: typeof(ChangeLogValueType));
        Field(f => f.NewValue, type: typeof(ChangeLogValueType));
    }
}

public sealed class ChangeLogValueType : ObjectGraphType<GraphQLChangeLogValueModel>
{
    public ChangeLogValueType()
    {
        Field(f => f.EntityId, type: typeof(GuidGraphType));
        Field(f => f.Name);
    }
}