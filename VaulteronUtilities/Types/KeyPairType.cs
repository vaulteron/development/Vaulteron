﻿using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class KeyPairType : EntityType<KeyPair>
{
    public KeyPairType()
    {
        Field(f => f.PublicKeyString);
        Field(f => f.EncryptedPrivateKey);
    }
}