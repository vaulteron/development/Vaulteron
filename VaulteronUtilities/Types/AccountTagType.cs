using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class AccountTagType : EntityType<AccountTag>
{
    public AccountTagType()
    {
        Field(f => f.Name);
        Field(f => f.Color, true);
    }
}