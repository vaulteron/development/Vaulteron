﻿using GraphQL.Types;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Types;

public sealed class TwoFATypesType : EnumerationGraphType<TwoFATypes>
{
    public TwoFATypesType()
    {
        Description = "The available types of 2FA.";
    }
}