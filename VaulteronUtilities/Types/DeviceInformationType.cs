﻿using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public class DeviceInformationType : EntityType<DeviceInformation>
{
    public DeviceInformationType()
    {
        Field(f => f.Mail);
        Field(f => f.JSONInput);
        Field(f => f.IpAddress);
        Field(f => f.CountryCode);
        Field(f => f.CountryName);
        Field(f => f.City);
        Field(f => f.CityLatLong);
        Field(f => f.Browser);
        Field(f => f.BrowserVersion);
        Field(f => f.DeviceBrand);
        Field(f => f.DeviceModel);
        Field(f => f.DeviceFamily);
        Field(f => f.Os);
        Field(f => f.OsVersion);
    }
}