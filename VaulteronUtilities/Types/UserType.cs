﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Types;

public class UserType : ObjectGraphType<User>
{
    public UserType()
    {
        Field(f => f.Id);
        Field(f => f.Email);
        Field(f => f.Firstname);
        Field(f => f.Lastname);
        Field(f => f.Admin);

        Field(f => f.EmailConfirmed);

        Field<ListGraphType<TwoFATypesType>>()
            .Name("twoFATypesEnabled")
            .Resolve(context => context.Source.TwoFATypesEnabled.FromFlags());

        Field(f => f.ForeignObjectId, true);

        Field(f => f.CreatedAt, type: typeof(DateTimeGraphType));
        Field(f => f.UpdatedAt, type: typeof(DateTimeGraphType));

        Field(f => f.ArchivedAt, true, typeof(DateTimeGraphType));
        Field(f => f.PasswordChangedAt, type: typeof(DateTimeGraphType));

        Field<BooleanGraphType>()
            .Name("passwordReset")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.IsPasswordResetEnabled(context.GetUserGuid(),context.Source.Id));
        
        Field<NonNullGraphType<SexType>>(nameof(User.Sex));

        Field<PublicKeyType>()
            .Name("publicKey")
            .Description("May return null if the user does not yet have a publicKey, which is the case when an admin just created the user")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetKeyPairAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<GroupType>>()
            .Name("groups")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetGroupsAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<UserGroupType>>()
            .Name("usergroups")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetUserGroupsAsync(context.GetUserGuid(), context.Source.Id));
    }
}