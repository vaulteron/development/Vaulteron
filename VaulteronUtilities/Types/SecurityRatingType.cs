﻿using GraphQL.Types;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Types;

public sealed class SecurityRatingType : EnumerationGraphType<SecurityRating>
{
    public SecurityRatingType()
    {
        Description = "The available security ratings.";
    }
}