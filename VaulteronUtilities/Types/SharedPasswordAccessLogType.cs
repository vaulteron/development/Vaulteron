﻿using VaulteronDatabase.Models;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class SharedPasswordAccessLogType : EntityType<SharedPasswordAccessLog>
{
    public SharedPasswordAccessLogType()
    {
        Field(f => f.UserId);

        Field<UserType>()
            .Name("user")
            .Description("User who access the password")
            .Resolve(context => context.Source.User);
    }
}