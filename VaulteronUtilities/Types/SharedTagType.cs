﻿using GraphQL.MicrosoftDI;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class SharedTagType : EntityType<SharedTag>
{
    public SharedTagType()
    {
        Field(f => f.Name);
        Field(f => f.Color, true);

        Field<GroupType>()
            .Name("group")
            .Description("Get Group for a tag")
            .Resolve()
            .WithScope()
            .WithService<ISharedTagResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetGroupForSharedTagAsync(context.GetUserGuid(), context.Source.Id));
    }
}