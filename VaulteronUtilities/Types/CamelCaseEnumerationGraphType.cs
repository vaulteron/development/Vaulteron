﻿using System;
using GraphQL;
using GraphQL.Types;

namespace VaulteronUtilities.Types;

public class CamelCaseEnumerationGraphType<T> : EnumerationGraphType<T> where T : Enum
{
    protected override string ChangeEnumCase(string val) => val.ToPascalCase();
}