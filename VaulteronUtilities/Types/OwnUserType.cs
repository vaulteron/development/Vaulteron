﻿using System;
using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Types;

public sealed class OwnUserType : UserType
{
    public OwnUserType()
    {
        Field<NonNullGraphType<KeyPairType>>()
            .Name("keyPair")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetEncryptedPrivateKeyAsync(context.GetUserGuid(), context.Source.Id));

        Field<NonNullGraphType<MandatorType>>()
            .Name("mandator")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetMandatorAsync(context.GetUserGuid()));

        Field<NonNullGraphType<BooleanGraphType>>()
            .Name("superAdmin")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, userResolver) => await userResolver.GetIsSuperAdminAsync(context.GetUserGuid()));

        Field<ListGraphType<WebauthnKeyType>>()
            .Name("registeredWebAuthnKeys")
            .Resolve()
            .WithScope()
            .Resolve(context => context.Source.WebautnCredentials);
    }
}