﻿using System;
using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class AccountPasswordType : EntityType<AccountPassword>
{
    public AccountPasswordType()
    {
        Field(f => f.Name);
        Field(f => f.Login);
        Field(f => f.WebsiteURL);
        Field(f => f.Notes);
        Field(f => f.ArchivedAt, nullable: true, type: typeof(DateTimeGraphType));

        Field<StringGraphType>()
            .Name("encryptedString")
            .Description("Should only be used for bulk operations")
            .Resolve()
            .WithScope()
            .WithService<IAccountPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetEncryptedStringForAccountPasswordAsync(
                              context.GetUserGuid(),
                              context.Source.Id,
                              Guid.Empty,
                              context.Source.ArchivedAt.HasValue));

        Field<OwnUserType>()
            .Name("owner")
            .Resolve(context => context.Source.Owner);

        Field<IntGraphType>()
            .Name("securityRating")
            .Description("The rating for this password")
            .Resolve(context => (int) context.Source.SecurityRating);

        Field<ListGraphType<AccountTagType>>()
            .Name("tags")
            .Description("Get Tags for a password")
            .Resolve()
            .WithScope()
            .WithService<IAccountPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetTagsForAccountPasswordAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<AccountPasswordAccessLogType>>()
            .Name("accessLog")
            .Description("Get timestamps of all events where the user is requesting the password value")
            .Resolve()
            .WithScope()
            .WithService<IAccountPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetAccessLogForAccountPasswordAsync(context.GetUserGuid(), context.Source.Id));
    }
}