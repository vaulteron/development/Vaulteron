﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class SharedPasswordType : EntityType<SharedPassword>
{
    public SharedPasswordType()
    {
        Field(f => f.Name);
        Field(f => f.Login);
        Field(f => f.WebsiteURL);
        Field(f => f.Notes);
        Field(f => f.GroupId, true);
        Field(f => f.ModifyLock);
        Field(f => f.IsSavedAsOfflinePassword);
        Field(f => f.ArchivedAt, true, typeof(DateTimeGraphType));

        Field<StringGraphType>()
            .Name("encryptedString")
            .Description("Should only be used for bulk operations")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetEncryptedStringForSharedPasswordAsync(context.GetUserGuid(), context.Source));

        Field<IntGraphType>()
            .Name("securityRating")
            .Resolve(context => (int) context.Source.SecurityRating);

        Field<ListGraphType<SharedTagType>>()
            .Name("tags")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetTagsForSharedPasswordAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<SharedPasswordAccessLogType>>()
            .Name("accessLog")
            .Description("Get timestamps of all events where users requested the password")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetAccessLogForSharedPasswordAsync(context.GetUserGuid(), context.Source.Id));

        Field<UserType>()
            .Name("CreatedBy")
            .Description("User who created the Password")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, resolver) => context.Source.CreatedById.HasValue
                              ? await resolver.GetCreatedByUserAsync(context.GetUserGuid(), context.Source.CreatedById.Value)
                              : null);
    }
}