﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types.Base;

namespace VaulteronUtilities.Types;

public sealed class GroupType : EntityType<Group>
{
    public GroupType()
    {
        Field(f => f.Name);
        Field(f => f.ParentGroupId, true, typeof(IdGraphType));
        Field(f => f.ArchivedAt, true, typeof(DateTimeGraphType));

        Field<ListGraphType<GroupType>>()
            .Name("children")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetSubGroupsAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<SharedPasswordType>>()
            .Name("passwords")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetPasswordsAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<UserType>>()
            .Name("users")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetUsersAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<UserType>>()
            .Name("usersWithAccess")
            .Description("List of all users that have access to the passwords managed by this group.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetUsersWithAccessAsync(context.GetUserGuid(), context.Source.Id));

        Field<ListGraphType<SharedTagType>>()
            .Name("tags")
            .Description("List of all tags in this group.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetTagsOfGroupAsync(context.GetUserGuid(), context.Source.Id));

        Field<IntGraphType>()
            .Name("countPasswords")
            .Description("The number of passwords associated with this group. Sub-groups not included.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.CountPasswordsAsync(context.GetUserGuid(), context.Source.Id));

        Field<IntGraphType>()
            .Name("countUsers")
            .Description("The number of users associated with this group. Sub-groups not included.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.CountUsersAsync(context.GetUserGuid(), context.Source.Id));

        Field<IntGraphType>()
            .Name("countSubGroups")
            .Description("The number of sub-groups (i.e. children) associated with this group.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.CountSubGroupsAsync(context.GetUserGuid(), context.Source.Id));

        Field<IntGraphType>()
            .Name("countTags")
            .Description("The number of tags associated with this group. Sub-groups not included.")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.CountTagsAsync(context.GetUserGuid(), context.Source.Id));
    }
}