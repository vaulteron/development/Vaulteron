using GraphQL.Types;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Types.Base;

public class EntityType<T> : ObjectGraphType<T> where T : Entity
{
    protected EntityType()
    {
        Field(f => f.Id, type: typeof(IdGraphType))
            .Description("GUID of the object");

        Field(f => f.UpdatedAt, type: typeof(DateTimeGraphType))
            .Description("The Datetime when this object was last modified");

        Field(f => f.CreatedAt, type: typeof(DateTimeGraphType))
            .Description("The Datetime when this object was created");
    }
}