﻿using GraphQL.Types;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Types
{
    public class WebauthnKeyType : ObjectGraphType<WebauthnPublicKeys>
    {
        public WebauthnKeyType()
        {
            Field(w => w.Id);
            Field(w => w.Description, true);
            Field(w => w.CreatedAt, true);
        }
    }
}
