﻿using VaulteronUtilities.Helper;

namespace VaulteronUtilities.Types;

public sealed class KindOfPasswordType : CamelCaseEnumerationGraphType<KindOfPassword>
{
    public KindOfPasswordType()
    {
        Description = "Type of password";
    }
}