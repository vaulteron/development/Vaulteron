﻿using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Types;

public sealed class SexType : CamelCaseEnumerationGraphType<Sex>
{
    public SexType()
    {
        Description = "The available types of human sexes.";
    }
}