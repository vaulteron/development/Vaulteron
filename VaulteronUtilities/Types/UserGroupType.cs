﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronUtilities.Types;

public sealed class UserGroupType : ObjectGraphType<UserGroup>
{
    public UserGroupType()
    {
        Field(f => f.GroupId);
        Field(f => f.UserId);
        Field<NonNullGraphType<GroupRoleType>>(nameof(UserGroup.GroupRole));

        Field<GroupType>()
            .Name("group")
            .Resolve()
            .WithScope()
            .WithService<IUserGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetGroupAsync(context.GetUserGuid(), context.Source.GroupId, context.Source.UserId));

        Field<UserType>()
            .Name("user")
            .Resolve()
            .WithScope()
            .WithService<IUserGroupResolver>()
            .ResolveAsync(async (context, resolver) => await resolver.GetUserAsync(context.GetUserGuid(), context.Source.GroupId, context.Source.UserId));
    }
}