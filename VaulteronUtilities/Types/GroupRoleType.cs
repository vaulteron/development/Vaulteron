﻿using GraphQL.Types;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Types;

public sealed class GroupRoleType : EnumerationGraphType<GroupRole>
{
    public GroupRoleType()
    {
        Description = "GroupRole";
    }
}