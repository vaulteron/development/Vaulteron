﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Helper;

public class GroupTreeNode
{
    public GroupTreeNode(Guid id, string name, GroupRole groupRole)
    {
        Id = id;
        Name = name;
        GroupRole = groupRole;
    }

    public Guid Id { get; set; }
    public string Name { get; set; }

    [JsonConverter(typeof(StringEnumConverter))]
    public GroupRole GroupRole { get; set; }

    public IEnumerable<GroupTreeNode> Children { get; set; }
}