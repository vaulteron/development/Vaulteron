﻿namespace VaulteronUtilities.Helper;

public enum KindOfPassword
{
    Group,
    Company,
    Private
}