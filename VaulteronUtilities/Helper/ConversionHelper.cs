﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.WebUtilities;

namespace VaulteronUtilities.Helper;

public static class ConversionHelper
{
    public static string ToUrlEncoded(string base64String) => HttpUtility.UrlEncode(base64String);

    public static string FromUrlEncoded(string encodedString) => HttpUtility.UrlDecode(encodedString);
    public static Guid FromUrlEncoded(Guid guid) => guid;

    public static string ToBase64(string normalString) =>
        Convert.ToBase64String(Encoding.UTF8.GetBytes(normalString));

    public static string FromBase64(string base64String) =>
        Encoding.UTF8.GetString(Convert.FromBase64String(base64String));

    public static string ConstructUrlWithQueryParameters(string host, Dictionary<string, string> queryParameters)
    {
        var encodedQueryParameters = new Dictionary<string, string>();
        foreach (var (key, value) in queryParameters)
        {
            encodedQueryParameters.Add(key, ToUrlEncoded(value));
        }

        var url = QueryHelpers.AddQueryString(host, encodedQueryParameters);
        return url;
    }
}