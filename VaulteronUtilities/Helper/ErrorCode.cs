﻿// ReSharper disable InconsistentNaming

namespace VaulteronUtilities.Helper;

public enum ErrorCode
{
    #region General (1 - 9)

    UnknownError = 1,
    GraphQlError = 2,
    DataNotFound = 3,
    DataExpired = 4,

    #endregion

    #region Input (10 - 19)

    UserIdWasNotPassedWithHttpRequest = 10,
    InputInvalidAccordingToGraphQL = 11,
    InputInvalid = 12,
    UnableToModifySharedPassword_ModifyLockInvalid = 13,
    InputInvalid_EncryptionsNotProvidedCorrectly = 14,

    #endregion

    #region Permissions / Authorizations (20 - 59)

    UserIsNotAuthorized = 20,
    UserIsNotAuthorized_NotLoggedIn = 21,
    UnableToLogin = 30,
    UnableToLogin_LdapRequired = 31,
    UnableToLogin_LdapNotApplicable = 32,
    UnableToLogin_TwoFactorRequired = 33,
    UnableToLogin_TwoFactorFailed = 34,
    UnableToRegister = 40,
    UnableToChangeLoginPassword = 49,

    Forbidden = 50,
    ForbiddenSelfOperation = 51,

    #endregion

    #region EmailVerification (60 - 69)

    EmailAlreadyVerified = 60,
    EmailTokenInvalid = 69,

    #endregion

    #region Sendgrid (70 - 79)

    UnableToSendEmail = 70,

    #endregion

    #region Licenseing (80 - 89)

    SubscriptionNotFound = 80,
    LicenseInsufficient = 81,
    LicenseInvalid = 82,

    #endregion
}