﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VaulteronUtilities.Helper;

public class ApiException : Exception
{
    public IEnumerable<string> Reasons { get; } = new List<string>();
    public ErrorCode ErrorCode { get; }

    public ApiException(ErrorCode errorCode) : base($"Exception of '{errorCode}' without specific message was thrown")
    {
        ErrorCode = errorCode;
    }

    public ApiException(ErrorCode errorCode, string message, IEnumerable<string> reasons) : this(errorCode, message)
    {
        Reasons = reasons;
    }

    public ApiException(ErrorCode errorCode, string message) : base(message)
    {
        ErrorCode = errorCode;
    }

    public override string ToString()
    {
        string str = $"ApiException[{ErrorCode}-{Message}";
        if (Reasons.Any()) str += ", Reasons: (" + string.Join("\n", Reasons) + ")";
        return str + "]";
    }
}