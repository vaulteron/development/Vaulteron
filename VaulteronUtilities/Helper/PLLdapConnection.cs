﻿using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace VaulteronUtilities.Helper;

public class PLLdapConnection
{
    private LdapConnection ldapConnection;
    private X509Certificate2 BaseCert;

    public PLLdapConnection(SecurityLevel securityLevel, string location, byte[] baseCert)
    {
        BaseCert = new X509Certificate2(baseCert);

        switch (securityLevel)
        {
            case SecurityLevel.StartTLS:
                ldapConnection = new LdapConnection(location);
                ldapConnection.SessionOptions.ProtocolVersion = 3;
                ldapConnection.SessionOptions.VerifyServerCertificate = VerifyCertificate;
                ldapConnection.SessionOptions.StartTransportLayerSecurity(null);
                break;
            case SecurityLevel.LDAPS:
                ldapConnection = new LdapConnection(new LdapDirectoryIdentifier(location, 636));
#if WIN
                ldapConnection.SessionOptions.SecureSocketLayer = true;
#endif
                ldapConnection.SessionOptions.VerifyServerCertificate = VerifyCertificate;
                break;
            case SecurityLevel.None:
                ldapConnection = new LdapConnection(location);
                break;
        }
    }

    public PLLdapConnection(bool useSslPort, string location, byte[] baseCert)
        : this(useSslPort ? PLLdapConnection.SecurityLevel.LDAPS : PLLdapConnection.SecurityLevel.StartTLS,
               location, baseCert)
    {
    }

    public PLLdapConnection(SecurityLevel securityLevel, string location, byte[] baseCert, string username, string password)
        : this(securityLevel, location, baseCert)
    {
        SetCredentials(username, password);
    }

    public PLLdapConnection(bool useSslPort, string location, byte[] baseCert, string username, string password)
        : this(useSslPort, location, baseCert)
    {
        SetCredentials(username, password);
    }

    private void SetCredentials(string username, string password)
    {
        ldapConnection.Credential = new NetworkCredential(username, password);
    }

    public bool CheckCredentials(string username, string password)
    {
        try
        {
            var credentials = new NetworkCredential(username, password);
            ldapConnection.Bind(credentials);

            return true;
        }
        catch (LdapException ldapException) when (ldapException.ErrorCode == 0x31) // Invalid credentials throw an exception with a specific error code
        {
            return false;
        }
    }

    private bool VerifyCertificate(LdapConnection con, X509Certificate cert)
    {
        X509Chain chain = new X509Chain();
        chain.ChainPolicy.ExtraStore.Add(BaseCert);

        chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
        chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority;

        var isValid = chain.Build(new X509Certificate2(cert));

        var chainRoot = chain.ChainElements[chain.ChainElements.Count - 1].Certificate;

        isValid = isValid && chainRoot.RawData.SequenceEqual(BaseCert.RawData);
        return isValid;
    }

    public static implicit operator LdapConnection(PLLdapConnection con)
    {
        return con.ldapConnection;
    }

    public enum SecurityLevel
    {
        None,
        LDAPS,
        StartTLS,
    }
}