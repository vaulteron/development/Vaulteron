﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.MutationModels;

public class BulkMoveSharedPasswordModel
{
    public IList<Guid> PasswordIds { get; set; }
    public Guid DestinationGroupId { get; set; }
    public IList<EncryptedSharedPassword> EncryptedPasswords { get; set; }
}