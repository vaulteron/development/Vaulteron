﻿using Fido2NetLib;

namespace VaulteronUtilities.Models.MutationModels;

public class WebauthnAttestationChallenge
{
    public WebauthnAttestationChallenge(CredentialCreateOptions fidoCredentials)
    {
        FidoCredentials = fidoCredentials;
    }

    private CredentialCreateOptions FidoCredentials { get; }

    public string OptionsJson => FidoCredentials.ToJson();
    public byte[] Challenge => FidoCredentials.Challenge;
}