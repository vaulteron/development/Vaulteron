﻿using System.Net;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.MutationModels;

public class ToptAuthenticatorKey
{
    public ToptAuthenticatorKey(string key, User user)
    {
        Email = user.Email;
        Key = key;
    }

    private string Email { get; }
    public string Key { get; }

    public string AuthenticatorString => string.Format(
        "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6",
        WebUtility.UrlEncode("Vaulteron"),
        WebUtility.UrlEncode(Email),
        Key);
}