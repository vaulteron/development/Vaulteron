﻿using System.Text.Json;

namespace VaulteronUtilities.Models;

public class GraphQLQuery
{
    public string OperationName { get; set; }
    public string Query { get; set; }
    public JsonElement Variables { get; set; }
}