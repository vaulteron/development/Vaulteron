﻿using System;

namespace VaulteronUtilities.Models;

public class GraphQLChangeLogValueModel
{
    public GraphQLChangeLogValueModel()
    {
    }

    public GraphQLChangeLogValueModel(string name)
    {
        Name = name;
    }

    public GraphQLChangeLogValueModel(Guid? entityId, string name)
    {
        EntityId = entityId;
        Name = name;
    }

    public Guid? EntityId { get; set; }
    public string Name { get; set; }
}