﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.EditModels;

public sealed class EditSharedPasswordModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Login { get; set; }
    public string WebsiteURL { get; set; }
    public string Notes { get; set; }
    public List<Guid> Tags { get; set; }
    public string ModifyLock { get; set; }
    public List<EncryptedSharedPassword> EncryptedPasswords { get; set; }
}