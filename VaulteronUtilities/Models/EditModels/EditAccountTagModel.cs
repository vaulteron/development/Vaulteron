﻿using System;

namespace VaulteronUtilities.Models.EditModels;

public class EditAccountTagModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}