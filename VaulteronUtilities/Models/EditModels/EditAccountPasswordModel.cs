﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.EditModels;

public class EditAccountPasswordModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Login { get; set; }
    public string WebsiteURL { get; set; }
    public string Notes { get; set; }
    public List<Guid> Tags { get; set; }
    public EncryptedAccountPassword EncryptedPassword { get; set; }
}