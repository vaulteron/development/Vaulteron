﻿using System;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Models.EditModels;

public class EditUserModel
{
    public Guid Id { get; set; }
    public Guid? ForeignObjectId { get; set; } // For Example ActiveDirectory ObjectId
    public string Email { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public string EncryptedLoginPassword { get; set; }
    public string SecurityQuestion1 { get; set; }
    public string SecurityQuestion2 { get; set; }
    public string SecurityQuestion3 { get; set; }
}