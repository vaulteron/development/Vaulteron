﻿using System;

namespace VaulteronUtilities.Models.EditModels;

public class EditSharedTagModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Color { get; set; }
    public Guid GroupId { get; set; }
}