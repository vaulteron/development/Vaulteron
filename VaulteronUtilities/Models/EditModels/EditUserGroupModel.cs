﻿using System;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Models.EditModels;

public class EditUserGroupModel
{
    public Guid UserId { get; set; }
    public Guid GroupId { get; set; }
    public GroupRole GroupRole { get; set; }
}