﻿using System;

namespace VaulteronUtilities.Models.EditModels;

public class EditGroupModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}