﻿using System;

namespace VaulteronUtilities.Models.EditModels;

public sealed class EditEncryptedAccountPasswordModel
{
    public string EncryptedPasswordString { get; set; }
    public Guid AccountPasswordId { get; set; }
    public Guid KeyPairId { get; set; }
}