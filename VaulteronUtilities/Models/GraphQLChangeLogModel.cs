﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models;

public class GraphQLChangeLogModel
{
    public GraphQLChangeLogModel()
    {
    }

    public GraphQLChangeLogModel(ChangeLog cl)
    {
        Id = cl.Id;
        EntityName = cl.EntityName;
        PropertyName = cl.PropertyName;
        CreatedBy = cl.CreatedBy;
        CreatedById = cl.CreatedById;
        CreatedAt = cl.CreatedAt;

        OldValue = cl.OldValue == null ? null : new GraphQLChangeLogValueModel(cl.OldValue);
        NewValue = cl.NewValue == null ? null : new GraphQLChangeLogValueModel(cl.NewValue);
    }

    public GraphQLChangeLogModel(ChangeLog cl, IDictionary<Guid, string> logNameResolveDictionary)
        : this(cl)
    {
        OldValue = cl.OldValue == null
            ? null
            : new GraphQLChangeLogValueModel(Guid.Parse(cl.OldValue),
                                             logNameResolveDictionary[Guid.Parse(cl.OldValue)]);
        NewValue = cl.NewValue == null
            ? null
            : new GraphQLChangeLogValueModel(Guid.Parse(cl.NewValue),
                                             logNameResolveDictionary[Guid.Parse(cl.NewValue)]);
    }

    public Guid Id { get; set; }
    public string EntityName { get; set; }
    public string PropertyName { get; set; }

    public User CreatedBy { get; set; }
    public Guid CreatedById { get; set; }

    public DateTime CreatedAt { get; set; }
    public GraphQLChangeLogValueModel OldValue { get; set; }
    public GraphQLChangeLogValueModel NewValue { get; set; }
}