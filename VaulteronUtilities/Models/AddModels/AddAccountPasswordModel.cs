﻿using System;
using System.Collections.Generic;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddAccountPasswordModel
{
    public string Name { get; set; }
    public Guid KeyPairId { get; set; }
    public string EncryptedPassword { get; set; }
    public string Login { get; set; }
    public string WebsiteUrl { get; set; }
    public string Notes { get; set; }
    public List<Guid> Tags { get; set; }
}