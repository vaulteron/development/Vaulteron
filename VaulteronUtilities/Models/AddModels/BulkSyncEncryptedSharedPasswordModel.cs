﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.AddModels;

public sealed class BulkSyncEncryptedSharedPasswordModel
{
    public Guid SharedPasswordToAddTo { get; set; }
    public List<EncryptedSharedPassword> EncryptedPasswords { get; set; }
}