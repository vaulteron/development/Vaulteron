﻿using System;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddSharedTagModel
{
    public string Name { get; set; }
    public string Color { get; set; }
    public Guid GroupId { get; set; }
}