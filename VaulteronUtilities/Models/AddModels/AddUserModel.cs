﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddUserModel
{
    public Guid? ForeignObjectId { get; set; } // For Example ActiveDirectory ObjectId

    public string Email { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public Sex Sex { get; set; }
    public bool Admin { get; set; }
    public string PublicKey { get; set; }
    public string EncryptedPrivateKey { get; set; }
    public string NewTemporaryLoginPassword { get; set; }
        public string NewTemporaryLoginPasswordHashed { get; set; }

    public List<Guid> GroupsToBeAddedTo { get; set; }
    public List<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
}