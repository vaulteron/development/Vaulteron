﻿namespace VaulteronUtilities.Models.AddModels;

public sealed class AddAccountTagModel
{
    public string Name { get; set; }
    public string Color { get; set; }
}