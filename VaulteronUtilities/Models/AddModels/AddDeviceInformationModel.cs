﻿using System;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddDeviceInformationModel
{
    public Guid ClientId { get; set; }
    public string Mail { get; set; }
    public string JSONInput { get; set; }
    public string IpAddress { get; set; }
    public string CountryCode { get; set; }
    public string CountryName { get; set; }
    public string City { get; set; }
    public string CityLatLong { get; set; }
    public string Browser { get; set; }
    public string BrowserVersion { get; set; }
    public string DeviceBrand { get; set; }
    public string DeviceModel { get; set; }
    public string DeviceFamily { get; set; }
    public string Os { get; set; }
    public string OsVersion { get; set; }
}