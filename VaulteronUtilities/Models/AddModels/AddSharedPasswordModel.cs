﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddSharedPasswordModel
{
    public Guid? GroupId { get; set; }
    public string Name { get; set; }
    public string Login { get; set; }
    public string WebsiteUrl { get; set; }
    public string Notes { get; set; }
    public bool IsSavedAsOfflinePassword { get; set; }
    public List<Guid> Tags { get; set; }
    public List<EncryptedSharedPassword> EncryptedPasswords { get; set; }
}