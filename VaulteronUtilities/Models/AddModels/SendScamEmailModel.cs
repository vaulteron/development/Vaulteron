﻿using System.Collections.Generic;

namespace VaulteronUtilities.Models.AddModels;

public sealed class SendScamEmailModel
{
    public string TemplateId { get; set; }
    public string RedirectURL { get; set; }
    public string SenderName { get; set; }
    public List<string> Mails { get; set; }
}