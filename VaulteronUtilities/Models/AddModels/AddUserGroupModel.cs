﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddUserGroupModel
{
    public Guid UserId { get; set; }
    public Guid GroupId { get; set; }
    public GroupRole GroupRole { get; set; }
    public ICollection<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
}