﻿using System;

namespace VaulteronUtilities.Models.AddModels;

public sealed class AddEncryptedAccountPasswordModel
{
    public string EncryptedPasswordString { get; set; }
    public Guid AccountPasswordId { get; set; }
    public Guid KeyPairId { get; set; }
}