﻿using System;
using System.Linq;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Extensions;

public static class ITimestampExtensions
{
    public static IQueryable<T> ByCreatedAtBetween<T>(this IQueryable<T> query, DateTime? from, DateTime? to) where T : ICreatedAt
    {
        if (from.HasValue)
            query = query.Where(hc => hc.CreatedAt >= from);

        if (to.HasValue)
            query = query.Where(hc => hc.CreatedAt <= to);

        return query;
    }
}