﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class UserGroupExtension
{
    public static IQueryable<UserGroup> Base(this IQueryable<UserGroup> query, Guid userId, Guid groupId)
    {
        return query
            .AsNoTracking()
            .ByUserId(userId)
            .ByGroupId(groupId);
    }

    public static IQueryable<UserGroup> Base(this IQueryable<UserGroup> query, Guid userId)
    {
        return query
            .AsNoTracking()
            .ByUserId(userId);
    }

    public static IQueryable<UserGroup> ByUserId(this IQueryable<UserGroup> query, Guid userId)
    {
        return query.Where(u => u.UserId == userId);
    }

    public static IQueryable<UserGroup> ByGroupId(this IQueryable<UserGroup> query, Guid groupId)
    {
        return query.Where(u => u.GroupId == groupId);
    }
}