﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class KeyPairExtensions
{
    public static IQueryable<KeyPair> Base(this IQueryable<KeyPair> query, Guid mandatorId, Guid userId)
    {
        return query
            .AsNoTracking()
            .ByMandatorId(mandatorId)
            .OrderBy(kp => kp.UpdatedAt)
            .ByUserId(userId);
    }

    public static IQueryable<KeyPair> ByMandatorId(this IQueryable<KeyPair> query, Guid mandatorId)
    {
        return query.Where(kp => kp.User.MandatorId == mandatorId);
    }

    public static IQueryable<KeyPair> ByUserId(this IQueryable<KeyPair> query, Guid userId)
    {
        return query.Where(kp => kp.UserId == userId);
    }

    public static async Task<KeyPair> LastActiveAsync(this IQueryable<KeyPair> query)
    {
        return await query
            .OrderByDescending(kp => kp.CreatedAt)
            .Take(1)
            .FirstAsync();
    }

    public static KeyPair LastActive(this IEnumerable<KeyPair> query)
    {
        return query
            .OrderByDescending(pk => pk.CreatedAt)
            .Take(1)
            .First();
    }
}