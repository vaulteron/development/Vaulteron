﻿using System;

namespace VaulteronUtilities.Extensions;

public static class GUIDExtenstion
{
    public static bool IsNullOrEmpty(this Guid? guid) => !guid.HasValue || guid.Value == Guid.Empty;
}