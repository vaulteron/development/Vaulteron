﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class AccessLogExtension
{
        public static IQueryable<SharedPasswordAccessLog> IncludeUser(this IQueryable<SharedPasswordAccessLog> query) => 
            query.Include(al => al.User);

        public static IQueryable<SharedPasswordAccessLog> ByPasswordId(this IQueryable<SharedPasswordAccessLog> query, Guid passwordId) => 
            query.Where(al => al.SharedPasswordId == passwordId);

        public static IQueryable<SharedPasswordAccessLog> ByUserId(this IQueryable<SharedPasswordAccessLog> query, Guid userId) => 
            query.Where(al => al.UserId == userId);
}