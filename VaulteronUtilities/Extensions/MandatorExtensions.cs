﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class MandatorExtensions
{
    public static IQueryable<Mandator> Base(this IQueryable<Mandator> query, Guid mandatorId)
    {
        return query
            .AsNoTracking()
            .Where(m => m.Id == mandatorId);
    }

    public static bool ExistCompanyName(this IQueryable<Mandator> query, string name)
    {
        return query.AsNoTracking().Any(m => m.Name == name);
    }
}