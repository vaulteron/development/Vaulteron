﻿using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models.Base;

namespace VaulteronUtilities.Extensions;

public static class ArchivableExtensions
{
    public static IQueryable<T> ByArchived<T>(this IQueryable<T> query, bool? byArchived) where T : IArchivable
    {
        if (!byArchived.HasValue) return query;
        return query.Where(a => byArchived.Value ? a.ArchivedAt != null : a.ArchivedAt == null);
    }

    public static IEnumerable<T> ByArchived<T>(this IEnumerable<T> query, bool? byArchived) where T : IArchivable
    {
        if (!byArchived.HasValue) return query;
        return query.Where(a => byArchived.Value ? a.ArchivedAt != null : a.ArchivedAt == null);
    }
}