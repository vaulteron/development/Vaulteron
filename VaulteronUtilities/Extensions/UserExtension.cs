﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class UserExtension
{
    public static IQueryable<User> Base(this IQueryable<User> query, Guid mandatorId, bool trackingEntity = false)
    {
        var newQuery = query.ByMandatorId(mandatorId);

        return trackingEntity
            ? newQuery
            : newQuery
                .AsNoTracking();
    }

    public static IQueryable<User> CurrentUser(this IQueryable<User> query, Guid userId, bool trackingEntity = false)
    {
        var newQuery = query
            .ById(userId)
            .ByArchived(false);

        return trackingEntity
            ? newQuery
            : newQuery.AsNoTracking();
    }

    public static IQueryable<User> ByMandatorId(this IQueryable<User> query, Guid companyId)
    {
        return query.Where(c => c.MandatorId == companyId);
    }

    public static IQueryable<User> ById(this IQueryable<User> query, Guid userId)
    {
        return query.Where(u => u.Id == userId);
    }

    public static IQueryable<User> ByKeyPairId(this IQueryable<User> query, Guid keyPairId)
    {
        return query.Where(u => u.KeyPairs.Select(kp => kp.Id).Contains(keyPairId));
    }

    public static IQueryable<User> ByKeyPairIds(this IQueryable<User> query, IEnumerable<Guid> keyPairIds)
    {
        // ReSharper disable once ConvertClosureToMethodGroup
        return query.Where(u => u.KeyPairs.Select(kp => kp.Id).Any(kp => keyPairIds.Contains(kp)));
    }

    public static IQueryable<User> IncludeGroups(this IQueryable<User> query)
    {
        return query.Include(u => u.UserGroups).ThenInclude(g => g.Group);
    }

    public static IQueryable<User> IncludeKeyPairs(this IQueryable<User> query)
    {
        return query.Include(u => u.KeyPairs);
    }

    public static IQueryable<User> IncludeLastKeyPair(this IQueryable<User> query)
    {
        return query.Include(u => u.KeyPairs
                                 .OrderByDescending(kp => kp.CreatedAt)
                                 .Take(1));
    }

    public static IQueryable<User> BySearchTerm(this IQueryable<User> query,
        string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;

        var s = $"%{searchTerm}%";
        return query.Where(u => EF.Functions.Like(u.Email, s) || EF.Functions.Like(u.Firstname, s) || EF.Functions.Like(u.Lastname, s)
        );
    }

    public static IQueryable<User> ByGroupIds(this IQueryable<User> query, List<Guid> groupIds)
    {
        if (groupIds == null) return query;
        return query.Where(u => u.UserGroups.Any(ug => groupIds.Contains(ug.GroupId)) || u.Admin);
    }

    public static IQueryable<User> ByUserIds(this IQueryable<User> query,
        List<Guid> withUserIds)
    {
        if (withUserIds == null) return query;
        return query.Where(u => withUserIds.Contains(u.Id));
    }

    public static IQueryable<User> ByWithoutUserIds(this IQueryable<User> query,
        List<Guid> withoutUserIds)
    {
        if (withoutUserIds == null) return query;
        return query.Where(u => withoutUserIds.All(wu => wu != u.Id));
    }

    public static IQueryable<User> ExcludeUsersInGroupIds(this IQueryable<User> query,
        List<Guid> withoutGroupIds)
    {
        if (withoutGroupIds == null) return query;
        return query.Where(u => !u.UserGroups.Any(ug => withoutGroupIds.Contains(ug.GroupId)));
    }


    public static async Task<bool> ExistEmailAsync(this IQueryable<User> query, string mail) => await query.AsNoTracking().AnyAsync(u => u.Email == mail);
}