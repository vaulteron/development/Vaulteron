﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class SharedTagExtension
{
    public static IQueryable<SharedTag> Base(this IQueryable<SharedTag> query, Guid mandatorId)
    {
        return query
            .AsNoTracking()
            .ByMandatorId(mandatorId);
    }

    public static IQueryable<SharedTag> ByMandatorId(this IQueryable<SharedTag> query, Guid mandatorId)
    {
        return query.Where(t => t.Group.MandatorId == mandatorId);
    }

    public static IQueryable<SharedTag> ById(this IQueryable<SharedTag> query, Guid id)
    {
        return query.Where(t => t.Id == id);
    }

    public static IQueryable<SharedTag> ByIds(this IQueryable<SharedTag> query, List<Guid> userId)
    {
        return query.Where(t => userId.Contains(t.Id));
    }

    public static IQueryable<SharedTag> BySearchTerm(this IQueryable<SharedTag> query,
        string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;

        var s = $"%{searchTerm}%";
        return query.Where(t => EF.Functions.Like(t.Name, s));
    }

    public static IQueryable<SharedTag> ByGroupId(this IQueryable<SharedTag> query, Guid groupId)
    {
        if (groupId == Guid.Empty) return query;
        return query.Where(st => st.GroupId == groupId);
    }

    public static IQueryable<SharedTag> ByGroups(this IQueryable<SharedTag> query, List<Group> groups)
    {
        if (groups == null) return query;
        return query.Where(st => groups.Contains(st.Group));
    }

    public static IQueryable<SharedTag> IncludeGroup(this IQueryable<SharedTag> query)
    {
        return query.Include(t => t.Group);
    }
}