﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class GroupExtension
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="query"></param>
    /// <param name="mandatorId"></param>
    /// <param name="byArchived">true for archives, false for active, null for all</param>
    /// <returns></returns>
    public static IQueryable<Group> Base(this IQueryable<Group> query, Guid mandatorId, bool? byArchived = false)
    {
        return query
            .AsNoTracking()
            .ByMandatorId(mandatorId)
            .ByArchived(byArchived)
            .IncludeUserGroup();
    }

    public static IQueryable<Group> RecursionBase(this IQueryable<Group> query, Guid mandatorId)
    {
        return query
            .AsTracking()
            .ByMandatorId(mandatorId)
            .Include(g => g.ChildGroups)
            .Include(g => g.ParentGroup)
            .IncludeUsers();
    }

    public static IQueryable<Group> ByMandatorId(this IQueryable<Group> query, Guid mandatorId)
    {
        return query.Where(g => g.MandatorId == mandatorId);
    }

    public static IQueryable<Group> ById(this IQueryable<Group> query, Guid groupId)
    {
        if (groupId == Guid.Empty) return query;
        return query.Where(g => g.Id == groupId);
    }

    public static IQueryable<Group> ByParentId(this IQueryable<Group> query, Guid? parentId, bool shouldBeIgnored)
    {
        if (shouldBeIgnored) return query;
        if (parentId == Guid.Empty) return query.Where(g => g.ParentGroup == null);
        return query.Where(g => g.ParentGroupId == parentId);
    }

    /// <summary>
    /// Filter by GroupID if groupID is set. It will be ignored when groupId is null or empty
    /// </summary>
    public static IQueryable<Group> ByIds(this IQueryable<Group> query, List<Guid> groupIds)
    {
        if (groupIds == null || !groupIds.Any()) return query;
        return query.Where(g => groupIds.Contains(g.Id));
    }

    public static IEnumerable<Group> ByIds(this IEnumerable<Group> query, List<Guid> groupIds)
    {
        if (groupIds == null || !groupIds.Any()) return query;
        return query.Where(g => groupIds.Contains(g.Id));
    }

        public static IQueryable<Group> ByUserId(this IQueryable<Group> query, Guid userId)
        {
            return query.Where(g => g.UserGroups.Any(ug => ug.UserId == userId));
        }
        
    public static IEnumerable<Group> ById(this IEnumerable<Group> query, Guid groupId)
    {
        return query.Where(g => groupId == g.Id);
    }

    public static IQueryable<Group> ByTagId(this IQueryable<Group> query, Guid tagId)
    {
        return query.Where(g => g.Tags.Any(t => t.Id == tagId));
    }

    public static IQueryable<Group> IncludeUnArchivedSharedPasswords(this IQueryable<Group> query)
    {
        return query.Include(g => g.SharedPasswords.Where(s => s.ArchivedAt == null));
    }

    public static IQueryable<Group> IncludeSharedPasswords(this IQueryable<Group> query)
    {
        return query.Include(g => g.SharedPasswords);
    }

    public static IQueryable<Group> IncludeSubGroups(this IQueryable<Group> query)
    {
        return query.Include(g => g.ChildGroups);
    }

    public static IQueryable<Group> IncludeUserGroup(this IQueryable<Group> query)
    {
        return query.Include(g => g.UserGroups);
    }

    public static IQueryable<Group> IncludeUsers(this IQueryable<Group> query)
    {
        return query
            .Include(g => g.UserGroups)
            .ThenInclude(ug => ug.User);
    }

    public static IQueryable<Group> IncludeUsersWithLastKeyPair(this IQueryable<Group> query)
    {
        return query
            .Include(g => g.UserGroups)
            .ThenInclude(ug => ug.User)
            .ThenInclude(u => u.KeyPairs
                             .OrderByDescending(pk => pk.CreatedAt)
                             .Take(1));
    }

    public static IQueryable<Group> IncludeTags(this IQueryable<Group> query)
    {
        return query.Include(g => g.Tags);
    }


    /**
         * Client filter by searchterm. 
         */
        public static IQueryable<Group> BySearchTerm(this IQueryable<Group> query, string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;
        return query.Where(g => g.Name.ToLower().Contains(searchTerm.ToLower()));
    }
}