﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class ChangeLogExtension
{
    public static IQueryable<ChangeLog> Base(this IQueryable<ChangeLog> query, Guid mandatorId)
    {
        return query
            .AsNoTracking()
            .ByMandatorId(mandatorId);
    }

    public static IQueryable<ChangeLog> ByMandatorId(this IQueryable<ChangeLog> query,
        Guid mandatorId)
    {
        return query
            .Include(hc => hc.CreatedBy)
            .Where(c => c.CreatedBy.MandatorId == mandatorId);
    }

    public static IQueryable<ChangeLog> ByEntity(this IQueryable<ChangeLog> query, Guid entityId)
    {
        return query.Where(hc => hc.EntityId == entityId);
    }

    public static IQueryable<ChangeLog> ByProperty(this IQueryable<ChangeLog> query,
        string propertyName)
    {
        return
            string.IsNullOrWhiteSpace(propertyName) ? query : query.Where(hc => hc.PropertyName == propertyName);
    }

    public static IQueryable<ChangeLog> IncludeUser(this IQueryable<ChangeLog> query)
    {
        return query.Include(cl => cl.CreatedBy);
    }
}