﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class AccountPasswordExtension
{
    public static IQueryable<AccountPassword> IncludeTags(this IQueryable<AccountPassword> query)
    {
        return query.Include(at => at.AccountPasswordTags).ThenInclude(t => t.Tag);
    }

    public static IQueryable<AccountPassword> ById(this IQueryable<AccountPassword> query, Guid id)
    {
        return query.Where(p => p.Id == id);
    }

    public static IQueryable<AccountPassword> ByMe(this IQueryable<AccountPassword> query, Guid userId)
    {
        return query.Where(p => p.OwnerId == userId);
    }

    public static IQueryable<AccountPassword> ByArchived(this IQueryable<AccountPassword> query, bool? byArchived)
    {
        if (!byArchived.HasValue) return query;
        return query.Where(a => byArchived.Value ? a.ArchivedAt != null : a.ArchivedAt == null);
    }

    public static IQueryable<AccountPassword> ByTagIds(this IQueryable<AccountPassword> query, List<Guid> tagIds)
    {
        if (tagIds == null || tagIds.Count == 0) return query;
        return query.Where(t => t.AccountPasswordTags.Any(apt => tagIds.Contains(apt.TagId)));
    }

    public static IQueryable<AccountPassword> BySearchTerm(this IQueryable<AccountPassword> query, string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;

        var s = $"%{searchTerm}%";
        return query.Where(p => EF.Functions.Like(p.Name, s) || EF.Functions.Like(p.Login, s));
    }
}