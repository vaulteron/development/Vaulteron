﻿using System;
using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class EncryptedSharedPasswordExtension
{
        public static IQueryable<EncryptedAccountPassword> ByKeyPairId(this IQueryable<EncryptedAccountPassword> query, Guid keyPairId) =>
            query.Where(esp => esp.KeyPairId == keyPairId);
        public static IQueryable<EncryptedSharedPassword> ByKeyPairId(this IQueryable<EncryptedSharedPassword> query, Guid keyPairId) =>
            query.Where(esp => esp.KeyPairId == keyPairId);
        public static IEnumerable<EncryptedSharedPassword> ByKeyPairId(this IEnumerable<EncryptedSharedPassword> query, Guid keyPairId) =>
            query.Where(esp => esp.KeyPairId == keyPairId);

        public static IEnumerable<EncryptedSharedPassword> Latest(this IEnumerable<EncryptedSharedPassword> query) =>
            query.OrderByDescending(esp => esp.CreatedAt).Take(1);

        public static IQueryable<EncryptedSharedPassword> ByArchived(this IQueryable<EncryptedSharedPassword> query, bool? byArchived = false)
    {
            if (!byArchived.HasValue) return query;
            return query.Where(a => byArchived.Value ? a.SharedPassword.ArchivedAt != null : a.SharedPassword.ArchivedAt == null);
    }
        
}