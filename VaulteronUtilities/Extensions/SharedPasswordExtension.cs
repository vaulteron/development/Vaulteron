﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class SharedPasswordExtension
{
        public static IQueryable<SharedPassword> BasePasswords(this IQueryable<SharedPassword> query, Guid mandatorId, List<Guid> groupIds)
    {
        return query
            .ByMandatorId(mandatorId)
            .Where(sp => !sp.GroupId.HasValue || groupIds.Contains(sp.GroupId.Value));
    }

        public static IQueryable<SharedPassword> BaseSyncedPasswords(this IQueryable<SharedPassword> query, Guid mandatorId, List<Guid> groupIds)
        {
            return query
                .BasePasswords(mandatorId, groupIds)
                .ByOfflinePassword(false);
        }

    public static IQueryable<SharedPassword> ByCompanyOrGroupPasswordForUser(this IQueryable<SharedPassword> query, Guid creatorUserId, List<Guid> groupIdsUserHasAccessTo)
    {
        return query
            .Where(sp =>
                       (sp.CreatedById.HasValue && sp.CreatedById.Value == creatorUserId)
                       || (sp.GroupId.HasValue && groupIdsUserHasAccessTo.Contains(sp.GroupId.Value)));
    }

    public static IQueryable<SharedPassword> BaseGroup(this IQueryable<SharedPassword> query, Guid mandatorId, bool? byArchived = false)
    {
        return query
            .ByMandatorId(mandatorId)
            .Include(p => p.EncryptedSharedPasswords)
            .ByGroupPasswords()
            .ByOfflinePassword(false)
            .ByArchived(byArchived);
    }

    public static IQueryable<SharedPassword> BaseCompany(this IQueryable<SharedPassword> query, Guid mandatorId, bool? byArchived = false, bool includeOfflinePasswords = false)
    {
        return query
            .ByMandatorId(mandatorId)
            .Include(p => p.EncryptedSharedPasswords)
            .ByCompany()
            .ByOfflinePassword(includeOfflinePasswords ? null : false)
            .ByArchived(byArchived);
    }

    public static IQueryable<SharedPassword> BaseOffline(this IQueryable<SharedPassword> query, Guid mandatorId)
    {
        return query
            .ByMandatorId(mandatorId)
            .Include(p => p.EncryptedSharedPasswords)
            .ByCompany()
            .ByOfflinePassword();
    }

    public static IQueryable<SharedPassword> IncludeGroup(this IQueryable<SharedPassword> query)
    {
        return query.Include(p => p.Group);
    }

    public static IQueryable<SharedPassword> IncludeTags(this IQueryable<SharedPassword> query)
    {
        return query.Include(p => p.SharedPasswordTags).ThenInclude(t => t.Tag);
    }

    public static IQueryable<SharedPassword> ById(this IQueryable<SharedPassword> query, Guid id)
    {
        return query.Where(p => p.Id == id);
    }

    public static IQueryable<SharedPassword> ByIds(this IQueryable<SharedPassword> query, List<Guid> ids)
    {
        return query.Where(p => ids.Contains(p.Id));
    }

    public static IQueryable<SharedPassword> ByGroupPasswords(this IQueryable<SharedPassword> query)
    {
        return query.Where(p => p.GroupId != null);
    }

    public static IQueryable<SharedPassword> ByCompany(this IQueryable<SharedPassword> query)
    {
        return query.Where(p => p.GroupId == null);
    }

    public static IQueryable<SharedPassword> ByCreatedBy(this IQueryable<SharedPassword> query, Guid userId)
    {
        return query.Where(p => p.CreatedById == userId);
    }

    public static IQueryable<SharedPassword> ByCreatedBy(this IQueryable<SharedPassword> query, List<Guid> userIds)
    {
        if (userIds == null || userIds.Count == 0) return query;
        return query.Where(p => p.CreatedById.HasValue && userIds.Contains(p.CreatedById!.Value));
    }

    public static IQueryable<SharedPassword> ByGroupId(this IQueryable<SharedPassword> query, Guid groupId)
    {
        return query.Where(p => p.GroupId == groupId);
    }

    public static IQueryable<SharedPassword> ByGroupIds(this IQueryable<SharedPassword> query, List<Guid> groupIds)
    {
        return query.Where(p => p.GroupId.HasValue && groupIds.Contains(p.GroupId.Value));
    }

    public static IQueryable<SharedPassword> ByMandatorId(this IQueryable<SharedPassword> query, Guid mandatorId)
    {
        return query.Where(p => p.MandatorId == mandatorId);
    }

    public static IQueryable<SharedPassword> ByUnArchived(this IQueryable<SharedPassword> query) =>
        query.Where(p => !p.ArchivedAt.HasValue);

    public static IEnumerable<SharedPassword> ByUnArchived(this IEnumerable<SharedPassword> query) =>
        query.Where(p => !p.ArchivedAt.HasValue);

    public static IQueryable<SharedPassword> ByOfflinePassword(this IQueryable<SharedPassword> query, bool? isOfflinePassword = true) =>
        isOfflinePassword is null
            ? query
            : query.Where(p => p.IsSavedAsOfflinePassword == isOfflinePassword);

    public static IQueryable<SharedPassword> ByArchived(this IQueryable<SharedPassword> query, bool? byArchived)
    {
        if (!byArchived.HasValue) return query;
        return query.Where(a => byArchived.Value ? a.ArchivedAt != null : a.ArchivedAt == null);
    }

    public static IQueryable<SharedPassword> BySearchTerm(this IQueryable<SharedPassword> query, string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;

        var s = $"%{searchTerm}%";
        return query.Where(p =>
                               EF.Functions.Like(p.Name, s)
                               || EF.Functions.Like(p.Login, s)
                               || EF.Functions.Like(p.Notes, s)
        );
    }
}