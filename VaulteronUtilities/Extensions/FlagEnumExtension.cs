﻿using System;
using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models.Enums;

namespace VaulteronUtilities.Extensions;

public static class FlagEnumExtension
{
    // See: https://github.com/graphql-dotnet/graphql-dotnet/issues/1666
    public static IEnumerable<T> FromFlags<T>(this T value) where T : Enum
    {
        return Enum.GetValues(typeof(T))
            .Cast<T>()
            .Distinct()
            .Where(x => value.HasFlag(x));
    }

    public static T CombineFlags<T>(this IEnumerable<T> values) where T : Enum
    {
        switch (values)
        {
            case IEnumerable<int> list:
                return (T)(object)list.Aggregate((a, b) => a | b);
            case IEnumerable<uint> list:
                return (T)(object)list.Aggregate((a, b) => a | b);
            case IEnumerable<long> list:
                return (T)(object)list.Aggregate((a, b) => a | b);
            case IEnumerable<ulong> list:
                return (T)(object)list.Aggregate((a, b) => a | b);
            case IEnumerable<TwoFATypes> list:
                var enumeratedList = list.ToList();
                if (!enumeratedList.Any()) return (T)(object)0;
                return (T)(object)enumeratedList.Aggregate((a, b) => a | b);
            default:
                throw new NotSupportedException("Enum type not supported");
        }
    }
}