﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;

namespace VaulteronUtilities.Extensions;

public static class AccountTagExtensions
{
    public static IQueryable<AccountTag> Base(this IQueryable<AccountTag> query, Guid userId)
    {
        return query
            .AsNoTracking()
            .ByMe(userId);
    }

    public static IQueryable<AccountTag> BySearchTerm(this IQueryable<AccountTag> query,
        string searchTerm)
    {
        if (string.IsNullOrEmpty(searchTerm)) return query;

        var s = $"%{searchTerm}%";
        return query.Where(t => EF.Functions.Like(t.Name, s));
    }

    public static IQueryable<AccountTag> ByMe(this IQueryable<AccountTag> query, Guid userId)
    {
        return query.Where(t => t.OwnerId == userId);
    }

    public static IQueryable<AccountTag> ById(this IQueryable<AccountTag> query, Guid id)
    {
        return query.Where(t => t.Id == id);
    }

    public static IQueryable<AccountTag> ByIds(this IQueryable<AccountTag> query, List<Guid> userId)
    {
        return query.Where(t => userId.Contains(t.Id));
    }
}