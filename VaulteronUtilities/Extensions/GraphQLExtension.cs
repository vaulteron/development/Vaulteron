﻿using System;
using GraphQL;
using VaulteronUtilities.Helper;

namespace VaulteronUtilities.Extensions;

public static class GraphQLExtension
{
    public static Guid GetUserGuid<T>(this IResolveFieldContext<T> context) =>
        (context.UserContext as GraphQLUserContext)?.UserId ?? throw new ApiException(ErrorCode.UnknownError, "UserId was not passed to request");
}