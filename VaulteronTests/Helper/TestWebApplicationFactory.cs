﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VaulteronDatabase;
using VaulteronUtilities.Services.Interfaces;
using VaulteronWebserver;

namespace VaulteronTests.helper;

// See: https://github.com/mmacneil/ApiIntegrationTestSamples
// Also: https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-3.1
public class TestWebApplicationFactory : WebApplicationFactory<Startup>
{
    public IServiceCollection ServiceCollection { get; set; }

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            ServiceCollection = services;

            // Create a new service provider.
            var serviceProvider = new ServiceCollection()
                .AddLogging(logger =>
                {
                    logger.AddDebug();
                    logger.AddConsole();
                    logger.SetMinimumLevel(LogLevel.None);
                })
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            services.AddHttpContextAccessor();

            // Remove the DB Context that got added through the Startup.cs
            var currentDbContextService =
                services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<VaulteronContext>));
            if (currentDbContextService is null)
                throw new Exception("Unable to replace DB context with in-memory DB");
            services.Remove(currentDbContextService);
            services.AddDbContext<VaulteronContext>(optionsBuilder =>
            {
                optionsBuilder
                    .UseInMemoryDatabase("VaulteronTestDatabase")
                    .EnableSensitiveDataLogging()
                    .UseInternalServiceProvider(serviceProvider)
                    .ConfigureWarnings(warnings => warnings.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            });

            // Replace emailSender with a fake one so that we don't send emails when running tests
            var currentEmailSenderService =
                services.SingleOrDefault(d => d.ServiceType == typeof(IEmailSenderService));
            if (currentEmailSenderService is not null)
            {
                var success = services.Remove(currentEmailSenderService);
                if (!success) throw new Exception("Test setup failed: unable to remove a service from DI");
                services.AddSingleton<IEmailSenderService, TestEmailSenderService>();
            }
        });

        builder.ConfigureLogging(logging =>
        {
            logging.ClearProviders();
            logging.AddConsole();
            logging.AddDebug();
            logging.SetMinimumLevel(LogLevel.Error);
        });
    }
}