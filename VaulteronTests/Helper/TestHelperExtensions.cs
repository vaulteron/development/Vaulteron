﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;

namespace VaulteronTests.helper;

internal static class TestHelperExtensions
{
    private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffZ";

    public static StringContent ToJson(this object o)
    {
        var jsonSettings = new JsonSerializerSettings { DateFormatString = DateTimeFormat };
        return new StringContent(JsonConvert.SerializeObject(o, jsonSettings), Encoding.UTF8, "application/json");
    }

    public static JObject ExtractJson(this HttpResponseMessage response)
    {
        var json = response.Content.ReadAsStringAsync().Result;
        return JObject.Parse(json);
    }

    public static ApiClientException ExtractApiClientException(this HttpResponseMessage response)
    {
        var jsonString = response.Content.ReadAsStringAsync().Result;
        var json = JObject.Parse(jsonString);
        var message = (string)json["message"];
        var errorCode = (int)(json["errorCode"] ?? -1);
        return new ApiClientException((ErrorCode)errorCode, message);
    }

    public static Dictionary<string, User> ByMandator(this Dictionary<string, User> dict, string mandatorName)
    {
        return dict.Where(u => u.Value.Mandator.Name == mandatorName).ToDictionary(kv => kv.Key, kv => kv.Value);
    }

    public static Dictionary<string, Group> ByMandator(this Dictionary<string, Group> dict, string mandatorName)
    {
        return dict.Where(u => u.Value.Mandator.Name == mandatorName).ToDictionary(kv => kv.Key, kv => kv.Value);
    }

    public static Dictionary<string, SharedTag> ByMandator(this Dictionary<string, SharedTag> dict,
        string mandatorName)
    {
        return dict.Where(u => u.Value.Group.Mandator.Name == mandatorName)
            .ToDictionary(kv => kv.Key, kv => kv.Value);
    }
}