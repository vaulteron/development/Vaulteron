﻿using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models;
using Xunit.Abstractions;

namespace VaulteronTests.helper;

public class HarryPotterTestUniverse : ApiClient
{
    public Mandator Mandator { get; }
    public Dictionary<string, User> TestUser { get; } = new();
    public Dictionary<string, Group> TestGroups { get; } = new();
    public Dictionary<string, SharedTag> TestTags { get; } = new();

    protected HarryPotterTestUniverse(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null) : base(factory, logger)
    {
        if (DbContext.Mandators.Any())
        {
            if (hptu != null)
            {
                Mandator = hptu.Mandator;

                TestUser = hptu.TestUser;
                TestGroups = hptu.TestGroups;
                TestTags = hptu.TestTags;
            }

            return;
        }

        (Mandator, TestUser, TestGroups, TestTags) = VaulteronSeedData.CreateTestMandatorIfItDoesNotExist();
    }
}