﻿using System;

namespace VaulteronTests.helper;

public class TestException : Exception
{
    public TestException(string message) : base(message)
    {
    }
}