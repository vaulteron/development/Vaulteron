﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Testing.Handlers;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.SeedData;
using VaulteronUtilities.Services.Interfaces;
using VaulteronWebserver.ViewModels.Users;
using Xunit;
using Xunit.Abstractions;

namespace VaulteronTests.helper;

public class ApiClient : IClassFixture<TestWebApplicationFactory>, IDisposable
{
    public TestWebApplicationFactory Factory { get; }

    private const string ApiBaseUri = "/api";
    public const string ApiLoginUri = ApiBaseUri + "/login";
    public const string ApiTwoFactorLoginUri = ApiBaseUri + "/twofactortotplogin";
    public const string ApiRegisterUri = ApiBaseUri + "/register";
    public const string ApiLogoutUri = ApiBaseUri + "/logout";
    public const string ApiGraphQLUri = ApiBaseUri + "/graph";
    public const string EmailChangedVerificationUri = "/emailChangedVerification";
    public const string ApiRequestEmailConfirmationUri = ApiBaseUri + "/requestEmailConfirmation";
    public const string ApiEmailGetPublicKeyAndSharedPasswordsUri = ApiBaseUri + "/getPublicKeyAndSharedPasswordsFromEmailToken";
    public const string ApiSetPasswordFromEmailTokenUri = ApiBaseUri + "/setPasswordFromEmailToken";

    public const string ApiEmailVerificationUri = "/emailConfirmation";

    public readonly ICryptoHelper CryptoHelper;
    public readonly ITestOutputHelper Logger;
    public readonly UserManager<User> UserManager;
    public readonly VaulteronContext DbContext;
    public readonly VaulteronSeedData VaulteronSeedData;
    public readonly TestEmailSenderService TestEmailSender;

    public ApiClient(TestWebApplicationFactory factory, ITestOutputHelper logger)
    {
        Factory = factory;
        Logger = logger;
        var handlers = new DelegatingHandler[]
        {
            new LoggingHandler(new HttpClientHandler(), logger),
            new CookieContainerHandler()
        };
        Client = factory.CreateDefaultClient(handlers);

        var serviceProvider = factory.ServiceCollection.BuildServiceProvider();
        UserManager = serviceProvider.GetRequiredService<UserManager<User>>();
        DbContext = serviceProvider.GetRequiredService<VaulteronContext>();

        // Note: if you want the actual service you must get it from like this
        // Otherwise you will get the service at this moment in time, which won't be updated by the webserver 
        using var serviceScope = factory.Services.CreateScope();
        TestEmailSender =
            serviceScope.ServiceProvider.GetRequiredService<IEmailSenderService>() as TestEmailSenderService;

        CryptoHelper = new FakeCryptoHelper();
        VaulteronSeedData = new VaulteronSeedData(DbContext, UserManager, CryptoHelper);

        Db = new TestDb(DbContext);
    }

    public HttpClient Client { get; }
    private TestDb Db { get; }

    public void Dispose() => Db.Reset();

    #region helper

    public async Task<HttpResponseMessage> QueryGraphQLAsync(string query, string variablesAsJson = "{}")
    {
        var content = "{\"query\":\"" + query + "\", \"variables\": " + variablesAsJson + "}";
        content = content
            .Replace("\r", "")
            .Replace("\n", "")
            .Replace("\t", ""); // Remove characters that are not in the JSON spec
        return await Client.PostAsync(ApiGraphQLUri, new StringContent(content, Encoding.UTF8, "application/json"));
    }

    public async Task LoginAsAsync(User user, bool skipLogin = false) => await LoginAsAsync(user.Email, VaulteronSeedData.LoginPassword, skipLogin);

    public async Task LoginAsAsync(string emailAddress, string password = VaulteronSeedData.LoginPassword,
        bool skipLogin = false)
    {
        if (skipLogin) return;

        var credentials = new LoginForm
        {
            Email = emailAddress,
            Password = CryptoHelper.ComputePasswordHash(password)
        };

        var loginResponse = await Client.PostAsync(ApiLoginUri, credentials.ToJson());

        if (loginResponse != null && !loginResponse.IsSuccessStatusCode)
            throw loginResponse.ExtractApiClientException();
    }
    
    /// <summary>
    /// Makes an async GraphQL request. Also does a Login-request to ensure the target user is used for the request
    /// </summary>
    /// <param name="userEmail">The user-email who makes the request</param>
    /// <param name="query">The GraphQL query to be executed</param>
    /// <param name="variablesJson">Optional: Query variables as JSON</param>
    /// <param name="skipLogin">Optional: Skip the login made before the request</param>
    /// <returns></returns>
    public async Task<JObject> MakeGraphQLRequestAsync(string userEmail, string query, string variablesJson = "{}", bool skipLogin = false)
    {
        await LoginAsAsync(userEmail, VaulteronSeedData.LoginPassword, skipLogin);

        var response = await QueryGraphQLAsync(query, variablesJson ?? "{}");
        if (HttpStatusCode.OK != response.StatusCode) throw response.ExtractApiClientException();
        return response.ExtractJson();
    }

    #endregion
}