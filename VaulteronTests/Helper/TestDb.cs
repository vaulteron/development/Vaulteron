﻿using VaulteronDatabase;

namespace VaulteronTests.helper;

public class TestDb
{
    public TestDb(VaulteronContext dbContext)
    {
        DbContext = dbContext;
    }

    public VaulteronContext DbContext { get; }

    public void Setup() => DbContext.Database.EnsureCreated();

    public void Teardown() => DbContext.Database.EnsureDeleted();

    public void Reset()
    {
        Teardown();
        Setup();
    }
}