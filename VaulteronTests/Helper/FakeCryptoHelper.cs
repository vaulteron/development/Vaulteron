﻿using VaulteronDatabase.SeedData;
using VaulteronUtilities.Helper;

namespace VaulteronTests.helper;

public class FakeCryptoHelper : ICryptoHelper
{
    public string ComputePasswordHash(string value) => value;

    public string EncryptSymmetrically(string plaintext, string loginPassword = VaulteronSeedData.LoginPassword) => ConversionHelper.ToBase64(plaintext);

    public string DecryptSymmetrically(string encryptedText, string loginPassword = VaulteronSeedData.LoginPassword) => ConversionHelper.FromBase64(encryptedText);

    public RSAKeyPair CreateKeyPair(string loginPassword = VaulteronSeedData.LoginPassword, bool getFromMockList = true)
    {
        var encryptedPrivateKey = EncryptSymmetrically(PrivateKey);
        return new RSAKeyPair(PublicKey, encryptedPrivateKey, PrivateKey);
    }

    public string EncryptAsymmetrical(string plaintext, string publicKeyString) => EncryptSymmetrically(plaintext, publicKeyString);

    public string DecryptWithPrivate(string encryptedText, string privateKey) => DecryptSymmetrically(encryptedText, privateKey);

    public string DecryptAsymmetrical(string encryptedText, string loginPassword, string encryptedPrivateKeyString)
    {
        var decryptedPrivateKey = DecryptSymmetrically(encryptedPrivateKeyString, loginPassword);
        return DecryptSymmetrically(encryptedText, decryptedPrivateKey);
    }

    public string DecryptAsymmetrical(string encryptedText, string privateKey) => DecryptSymmetrically(encryptedText, privateKey);

    #region keys

    private const string PublicKey = @"-----BEGIN RSA PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAuM/6h3Xod7wW7Ps16l0x
whdzdVp76ijVyLWR5znvsA6NGbI/C1NBefDHol5sAISdMOcgarVMPteVBx1k68Y3
sVnDWOqoAnfvcc2KT6xKqU7n+7kL1X3a5U8rv0ZB07+FlLZAVxI9IOnObxpksAzy
ciskdE5tXkxrJkgbGkSSvfgvvxqriBKHPwOoL6U//8t5g6XCNgHzqz52w5WLYnG5
q2CJRf6C9WVsnY8ckPFH7o+kCeKIio7rF+GLHXB2nYY6MdeY1ZlKkAs/fP4TmIEZ
G9IykWRmKuDXblU8+E4oi0wuXZeFSImQcNLgEJs+bFsKcrWsf4KXUc0CcnL/5Sl+
u/jvPTl6t2Tv60RrAKL3gjOqMubcpyRv8Q/DXqWaXX2OFHRoU362szd1xdC4lOx0
m17HAuavAYCGJvxLGP/eiDjbkY5XDGkDXhhg2UCPVlDYIvDLem4bpg0szuMzCN2P
HutMEP2n2H+Ys12QlyDl0BJZLBlJCqbuGVtfJ1ARB+Gq9nRMDV9Bk+ScoSZ43URw
YLZHzmRp8jBtwRgmt1RQybTHJTgWBKCDouTHjCv1y//oC+RBXkd3Xo/YMBgh6Nk3
x4aZoC3NC8XtJKJEt/QtjkHXR+zp5v5Y1yI/jMxuAl0mjQ+oS6WMSMVN0t0/n9OZ
lIhdEfnaH5MhFo2Lh6SgNGcCAwEAAQ==
-----END RSA PUBLIC KEY-----
";

    private const string PrivateKey = @"-----BEGIN RSA PRIVATE KEY-----
MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQC4z/qHdeh3vBbs
+zXqXTHCF3N1WnvqKNXItZHnOe+wDo0Zsj8LU0F58MeiXmwAhJ0w5yBqtUw+15UH
HWTrxjexWcNY6qgCd+9xzYpPrEqpTuf7uQvVfdrlTyu/RkHTv4WUtkBXEj0g6c5v
GmSwDPJyKyR0Tm1eTGsmSBsaRJK9+C+/GquIEoc/A6gvpT//y3mDpcI2AfOrPnbD
lYticbmrYIlF/oL1ZWydjxyQ8Ufuj6QJ4oiKjusX4YsdcHadhjox15jVmUqQCz98
/hOYgRkb0jKRZGYq4NduVTz4TiiLTC5dl4VIiZBw0uAQmz5sWwpytax/gpdRzQJy
cv/lKX67+O89OXq3ZO/rRGsAoveCM6oy5tynJG/xD8NepZpdfY4UdGhTfrazN3XF
0LiU7HSbXscC5q8BgIYm/EsY/96IONuRjlcMaQNeGGDZQI9WUNgi8Mt6bhumDSzO
4zMI3Y8e60wQ/afYf5izXZCXIOXQElksGUkKpu4ZW18nUBEH4ar2dEwNX0GT5Jyh
JnjdRHBgtkfOZGnyMG3BGCa3VFDJtMclOBYEoIOi5MeMK/XL/+gL5EFeR3dej9gw
GCHo2TfHhpmgLc0Lxe0kokS39C2OQddH7Onm/ljXIj+MzG4CXSaND6hLpYxIxU3S
3T+f05mUiF0R+dofkyEWjYuHpKA0ZwIDAQABAoICAB2cFMOHPIuScwnc5bJ64/H4
1+LBcID/seDKVmn5elF2w1yOllM2L5Nd5F47Sop6tMSVAziVe3XGDHZrWzP7OgkM
HKQuVGSw2ogdKj/BluSxY7GM4DyAQYXjvVqT4xAN61T8LirFHU4cp5pdXCxopp9s
EhcgSZ2GGNS1UMJQokIVdXDk0P6oGUsbRuL7Utmku/8/m3ITtfqFs3l6iC5pkhnH
CbdRI6I1ccV8nFMMlT2/fcSnqsCoTToyRJue3SI30zMRG8d9G9kgNDfObU1NwbwX
XUq2DzhndZyTw4T04MIQno0YLZ6Fi8LHxmZiuQmbPH1MZiq3vY65jd7QVfddQpSN
b/WynSg0C6J93Co2zrYuLyIe1sevSDg3lWR5NWLnXn1HJ2qngvlFL2O8vR+ku0QH
NWpZdmw91EGaT4fV1V9u8ZC2HcTEcipiD8TauPEWcwesQiXpwhe2AXCEv+ZlZAoU
FoaMXU17fGa7rU8Tqmc99nAOC91Oymon8SjWPoAwqB1DYcQMavUWoTjlWMTamCAA
4GxvfNP3GC04FF7iBKThxVBAtKiC2m7U14Rz99thw3zrpUUfohxq1oXvfQalNIed
ehnQ7A5ZJhYDTblUXmOJQUKpO0DHjuPEGA8BDFliyIwkmsgL5YuJ2j/l4IMa9spN
aaopWbCVe1OxVNlJuZHhAoIBAQDky29QnfifFIzUr0zEsCIOq7UF78HtOZP/BFU0
kB6Ou+T50pU3ZI8M047u1sKDR+wIsUaLk1VY36NQGfJpegUxw7OvZrMlIfT4RPVh
WARMeiZDav1mrblnNzbAavYYrW0JnoCejch1adC37oVKdxQYbDEAgaP6ZMPol1Vh
oA0Nsbf/mMgU8n7Q2qfijdXWvN3BfnScdZg6buemd6oiDVhX4Y17QOF8afRSt/is
PGKvUwVBLLebGyfQtTFa8drIEoNV4Vef2nm2Yuoj4GxcThu4HX24lOJj/yV5Rtia
G/8nRveoqZ37UfK1jAzL6aU41FJZUYPnu1vl3/X32IouexyTAoIBAQDOybai3jyl
lJounrx5tY2qkde/WvAvgqOtbYFuEaensTTmbejTOlOPKiPcsT7o3ByZojrGK5/1
UOb8nxry6cvF01Db8+MDJpVlTGLJhaL6Bti1VFet//qwdsB1FNhrGa+7oQRBwDK9
MOEVr9nVz7MXuh8rNn8WdD7c5zQnBXL/djkBvk4DoN465+wJdfJg/88yBWS0tiUB
49PhBGkBamGzP9m2RrkMCaWdtNjyy3gZUE3AxaNV4BCXeGg18pAb/abV4iNr9q8w
4ew+6QJ/NMh3FEtD46Vy3H1pfN7ij8iFsr+zFvIorW2UWIC5puZVF6KhPw7Tg3WU
lUL+nyD+tMFdAoIBAQCFdr6qklwzwDMe5lur6OsVD8j7e47uqUXR318s2F1a+fRq
XxM4Suq/mrGoVZMGLfBcab0zMnD49Qw5b8lfChT5oKWyzf/2y0YxhVh2vndTfXXQ
ohrLyM98jFLeEAcbAh+3GsUAuMiU5XMhuHv8olYQoRw8pWNYb2twxMbDui+jK2ag
LSmaPpMHlmWU3RQ+vdHGS9QOaPKyM3viOa//STWdni+AFKr0foJNto/TOS6AA7fu
+vzn+AFT2D7ARJFRYKtIb7lDwJh7clwPLZ1Igy/cSSfLgt5FYtMjWdaEn4xKBzX1
J2zG7Fje2qGkWdlNu12GSOzfQwYqpGjBeYmx75MNAoIBABACigNeFW/0a2OEoq8B
oXA9TmjgT4w+HxIEUmsFLufKzn+wrr/72hBRgOlLR8WblRNH/ijto7rIgrm4T8vS
0hYE4FPHNCJAUHzBRzy2/gFBmnGfkBJGqVAUQZ5O+NqB6JssQjoc+VCwghy9uq3V
INaLcHJWC8zfPya3N9B1mYuxyYQbCcs6HzqMTjgFS7i61X3X8Q4UtBTLYNIhF0gm
xl2sady06QrHjG0DXXCe1DORhhOQua6iZGZ1yguzNfkU0F/q3Dc1qCauty2/uJPE
sGiXx6P/8gVMkOEj69W6sBm51yG0excvJjbAC2dURqz74Xl+elVh7i6Mx90S0Okn
cXkCggEABxMHdhCBXQgFspXGsSYQrBOyeejY2jHvPBYoQGBAfxDqENGMKJVHx/VS
dHltLgtwWCv+8mrk20lc/B3AgXOajLU8mCvHzJfEErNiRYvwxGQI/6Pebo/cGymm
DrI/EJ4PakVvm8GePEhYGF47mFUBrBqjs5ib3K3AdDTXAVI2fdSETTe6IRIxyYP3
klCERt1a/np+VLmpRYQauUrA542kg2FhjKn1xyxHTcevjwFKCPDbpbyIESGy1kf3
NFAfaaGA9zHtWaG1vmDWvsOGcXYbpPfkiNwNg0/VTSQv4i55UU6oCBIDjZLo/qe7
ruR217/h68UGzM0tu86+QQpu8boczg==
-----END RSA PRIVATE KEY-----
";

    #endregion
}