﻿using System;
using System.Threading.Tasks;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using Xunit;

namespace VaulteronTests.Helper;

/// <summary>
/// Custom Assert
/// </summary>
internal static class CAssert
{
    public static async Task ApiExceptionAsync(ErrorCode expectedErrorCode, Func<Task> testCode)
    {
        var exception = await Assert.ThrowsAsync<ApiClientException>(testCode);
        Assert.Equal(expectedErrorCode, exception.ErrorCode);
    }
}