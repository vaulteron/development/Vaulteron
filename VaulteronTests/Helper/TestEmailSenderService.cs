﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VaulteronUtilities.Services;
using VaulteronUtilities.Services.Interfaces;

namespace VaulteronTests.helper;

public class TestEmailSenderService : IEmailSenderService
{
    private readonly ILogger<EMailSenderService> logger;

    public TestEmailSenderService(ILogger<EMailSenderService> logger)
    {
        this.logger = logger;
    }

    public List<SentEmail> SentEmails { get; } = new();

    public enum EmailType
    {
        SendConfirmEmailAsync,
        SendEmailAfterUserAddedAsync,
        SendEmailAfterUserChangedPasswordAsync,
        SendEmailAddMultiFactorAuthentication,
        SendEmailRemoveMultiFactorAuthentication,
        SendEmailInformationThatUserEmailWasChanged,
        SendEmailPasswordReset,
        SendEmailPasswordResetUnavailable,
    }

    public class SentEmail
    {
        public EmailType EmailType { get; set; }
        public dynamic Content { get; set; }
    }

    public Task SendConfirmEmailAsync(string emailConfirmationToken, Guid registeredUserId, string targetEmail,
        string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendConfirmEmailAsync,
                Content = new
                {
                    registeredUserId,
                    emailConfirmationToken,
                    targetEmail,
                    receiverName,
                }
            }
        );
        logger.LogCritical("emailConfirmation-email was sent to " + targetEmail + " with the token: " + emailConfirmationToken);
        return Task.CompletedTask;
    }


    public Task SendEmailAfterUserAddedAsync(string emailConfirmationToken, string tempLoginUserPassword,
        Guid registeredUserId, string targetEmail, string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailAfterUserAddedAsync,
                Content = new
                {
                    emailConfirmationToken,
                    tempLoginUserPassword,
                    registeredUserId,
                    targetEmail,
                    receiverName,
                }
            }
        );
        logger.LogCritical("userAdded-email was sent to " + targetEmail + " with the token: " + emailConfirmationToken);
        return Task.CompletedTask;
    }

    public Task SendEmailAfterUserChangedPasswordAsync(Guid userGuid, string userEmail, string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailAfterUserChangedPasswordAsync,
                Content = new
                {
                    userGuid,
                    userEmail,
                    receiverName,
                }
            }
        );
        logger.LogCritical("user-email was sent to " + userEmail + " with the userGuid " + userGuid);
        return Task.CompletedTask;
    }

    public Task SendEmailAddMultiFactorAuthenticationAsync(string userEmail, string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailAddMultiFactorAuthentication,
                Content = new
                {
                    userEmail,
                    receiverName,
                }
            }
        );
        logger.LogCritical("user-email was sent to " + userEmail);
        return Task.CompletedTask;
    }

    public Task SendEmailRemoveMultiFactorAuthenticationAsync(string userEmail, string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailRemoveMultiFactorAuthentication,
                Content = new
                {
                    userEmail,
                    receiverName,
                }
            }
        );
        logger.LogCritical("user-email was sent to " + userEmail);
        return Task.CompletedTask;
    }

    public Task SendEmailInformationThatUserEmailWasChanged(Guid targetUserId, string oldEmail, string newEmail,
        string receiverName, string changeEmailTokenAsync)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailInformationThatUserEmailWasChanged,
                Content = new
                {
                    targetUserId,
                    oldEmail,
                    newEmail,
                    receiverName,
                    changeEmailTokenAsync
                }
            }
        );
        logger.LogCritical("user-email was sent to " + oldEmail + " and " + newEmail + " about changing the user's email address");
        return Task.CompletedTask;
    }

    public Task SendEmailPasswordReset(string userEmail, Guid userId, string receiverName, string passwordResetToken, string question1, string question2, string question3)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailPasswordReset,
                Content = new
                {
                    userEmail,
                    receiverName,
                    encryptedLoginPassword = passwordResetToken,
                    question1,
                    question2,
                    question3
                }
            }
        );
        return Task.CompletedTask;
    }

    public Task SendEmailPasswordResetUnavailable(string userEmail, string receiverName)
    {
        SentEmails.Add(new SentEmail
            {
                EmailType = EmailType.SendEmailPasswordResetUnavailable,
                Content = new
                {
                    userEmail,
                    receiverName,
                }
            }
        );
        return Task.CompletedTask;
    }

    public Task SendScamMail(Guid currentUserId, string userEmail, string receiverName, string templateId, string senderName, string redirectURL)
    {
        throw new NotImplementedException();
    }
}