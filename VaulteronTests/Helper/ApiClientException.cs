﻿using VaulteronUtilities.Helper;

namespace VaulteronTests.helper;

internal class ApiClientException : ApiException
{
    public ApiClientException(ErrorCode errorCode, string message) : base(errorCode, message)
    {
    }
}