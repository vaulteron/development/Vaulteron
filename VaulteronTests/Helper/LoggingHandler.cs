﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace VaulteronTests.helper;

public class LoggingHandler : DelegatingHandler
{
    private readonly ITestOutputHelper logger;

    public LoggingHandler(HttpMessageHandler innerHandler, ITestOutputHelper logger) : base(innerHandler)
    {
        this.logger = logger;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        logger.WriteLine("-----------------------------------------------");
        logger.WriteLine("Request:");
        logger.WriteLine("-----------------------------------------------");
        logger.WriteLine(request.ToString());
        if (request.Content != null)
        {
            logger.WriteLine("Content:");
            logger.WriteLine("---------");
            logger.WriteLine(await request.Content.ReadAsStringAsync());
        }

        logger.WriteLine("");

        HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

        logger.WriteLine("-----------------------------------------------");
        logger.WriteLine("Response:");
        logger.WriteLine("-----------------------------------------------");

        logger.WriteLine(response.ToString());
        if (response.Content != null)
        {
            logger.WriteLine("Content:");
            logger.WriteLine("---------");
            logger.WriteLine(await response.Content.ReadAsStringAsync());
        }

        logger.WriteLine("");

        return response;
    }
}