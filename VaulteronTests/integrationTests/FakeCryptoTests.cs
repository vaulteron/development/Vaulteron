﻿using VaulteronDatabase.SeedData;
using VaulteronTests.helper;
using Xunit;

namespace VaulteronTests.integrationTests;

/// <summary>
/// This class tests the fake crypto that is used for all unit tests
/// </summary>
public class FakeCryptoTests
{
    private readonly ICryptoHelper cryptoHelper;

    public FakeCryptoTests()
    {
        cryptoHelper = new FakeCryptoHelper();
    }

    [Fact]
    private void TestAsymmetricalCrypto()
    {
        const string loginPassword = "password12345!";
        const string plaintext = "some-plain-text-message";

        var keyPair = cryptoHelper.CreateKeyPair(loginPassword);
        var encryptedText = cryptoHelper.EncryptAsymmetrical(plaintext, keyPair.PublicKey);
        var decryptedText =
            cryptoHelper.DecryptAsymmetrical(encryptedText, loginPassword, keyPair.EncryptedPrivateKey);
        Assert.Equal(plaintext, decryptedText);
    }

    [Fact]
    private void TestSymmetricalCrypto()
    {
        const string loginPassword = "password12345!";
        const string plaintext = "some-plain-text-message";

        var encryptedText = cryptoHelper.EncryptSymmetrically(plaintext, loginPassword);
        var decryptedText = cryptoHelper.DecryptSymmetrically(encryptedText, loginPassword);
        Assert.Equal(plaintext, decryptedText);
    }
}