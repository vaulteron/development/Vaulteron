﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class AccessLogTest : HarryPotterTestUniverse
{
    private readonly PasswordTestService passwordTestService;
    private readonly GroupTestService groupTestService;

    public AccessLogTest(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
        groupTestService = new GroupTestService(logger, this);
    }

    [Theory]
    [InlineData(harryPotter, 1)]
    [InlineData(hermioneGranger, 4)]
    public async Task GetAccessLogOfAccountPassword(string username, int numberOfAccesses)
    {
        var user = TestUser[username];

        //Create a password
        var createdPassword = await passwordTestService.AddAccountPasswordAsync(username);

        //Access the value of the created password
        for (var i = 0; i < numberOfAccesses; i++)
        {
            const string query = @"query encryptedAccountPasswordString($passwordId: Guid!) {
                      encryptedAccountPasswordString(passwordId: $passwordId)}";
            var variables = new { passwordId = createdPassword.Id };
            await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        }

        var jsonAccessLog = await passwordTestService.GetAllAccountPasswordsJsonAsync(user);

        //Get target password and check accessLog count
        var jsonIndex =
            passwordTestService.GetIndexInsideJsonOfPasswordWithTargetName(jsonAccessLog, createdPassword.Name,
                                                                           true);
        var accessLogs = JsonConvert.DeserializeObject<List<AccountPasswordAccessLog>>(
            jsonAccessLog["data"]!["accountPasswords"]![jsonIndex]!["accessLog"]!.ToString());

        Assert.NotNull(accessLogs);
        Assert.Equal(accessLogs!.Count, numberOfAccesses);
    }

    [Theory]
    [InlineData(harryPotter, gryffindor, 1)]
    [InlineData(hermioneGranger, gryffindor, 3)]
    public async Task GetAccessLogOfSharedPassword(string username, string groupName, int numberOfAccesses)
    {
        const string plaintext = "pwValue";
        //Create a password
        var createdPassword =
            await passwordTestService.AddSharedPasswordAsync(username, gryffindor, "pwName", plaintext);
        var usersWithAccess = await groupTestService.GetUsers(username, groupName, true);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(TestUser[username],
                                                                                   createdPassword.Id,
                                                                                   usersWithAccess, plaintext, true, false);

        //Get the Created Password 
        for (var i = 0; i < numberOfAccesses; i++)
        {
            const string query = @"query encryptedSharedPasswordStringQuery($passwordId: Guid!) {
                      encryptedSharedPasswordString(passwordId: $passwordId)}";
            var variables = new { passwordId = createdPassword.Id };
            var user = TestUser[username];

            await MakeGraphQLRequestAsync(user.Email, query,
                                          JsonConvert.SerializeObject(variables));
        }

        var jsonAccessLog = await passwordTestService.GetAllSharedPasswordJsonAsync(username, groupName);

        //Get target password and check accessLog count
        var jsonIndex =
            passwordTestService.GetIndexInsideJsonOfPasswordWithTargetName(jsonAccessLog, createdPassword.Name,
                                                                           false);
        var accessLogs = JsonConvert.DeserializeObject<List<AccountPasswordAccessLog>>(
            jsonAccessLog["data"]!["sharedPasswords"]![jsonIndex]!["accessLog"]!
                .ToString());

        Assert.Equal(accessLogs?.Count, numberOfAccesses);
    }


    [Theory]
    [InlineData(harryPotter, gryffindor, 1, dumbledore, dumbledore, harryPotter, hermioneGranger,
                minervaMcGonagall)]
    [InlineData(dumbledore, gryffindor, 5, dumbledore, dumbledore, harryPotter, hermioneGranger, minervaMcGonagall)]
    [InlineData(severusSnape, gryffindor, 5, dumbledore, dumbledore, harryPotter, hermioneGranger,
                minervaMcGonagall)]
    [InlineData(minervaMcGonagall, gryffindor, 4, dumbledore, harryPotter, hermioneGranger, minervaMcGonagall)]
    [InlineData(hermioneGranger, gryffindor, 4, hermioneGranger, hermioneGranger, hermioneGranger, hermioneGranger,
                harryPotter)]
    public async Task GetCorrectCountOfAccessLogByCaller(string usernameToAddAndCheckPw, string groupName,
        int expectedAccessLogsCount,
        params string[] usersWhoAccces)
    {
        const string pwName = "pwName";
        const string pwValue = "pwValue";
        var user = TestUser[usernameToAddAndCheckPw];

        List<User> usersWhoAccessThePassword = usersWhoAccces.Select(u => TestUser[u]).ToList();

        //Create a password
        var createdPassword = await passwordTestService
            .AddSharedPasswordAsync(usernameToAddAndCheckPw, groupName, pwName, pwValue);

        var usersWithAccess = await groupTestService.GetUsers(usernameToAddAndCheckPw, groupName, true);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(
            user,
            createdPassword.Id,
            usersWithAccess, pwValue, true, false);

        //Get the Created Password 
        foreach (var userToAccess in usersWhoAccessThePassword)
        {
            const string query = @"query encryptedSharedPasswordStringQuery($passwordId: Guid!) {
                      encryptedSharedPasswordString(passwordId: $passwordId)}";
            var variables = new { passwordId = createdPassword.Id };

            await MakeGraphQLRequestAsync(userToAccess.Email, query,
                                          JsonConvert.SerializeObject(variables));
        }


        var jsonAccessLog =
            await passwordTestService.GetAllSharedPasswordJsonAsync(usernameToAddAndCheckPw, groupName);

        //Get target password and check accessLog count
        var jsonIndex =
            passwordTestService.GetIndexInsideJsonOfPasswordWithTargetName(jsonAccessLog, createdPassword.Name,
                                                                           false);
        var accessLogs = JsonConvert.DeserializeObject<List<AccountPasswordAccessLog>>(
            jsonAccessLog["data"]!["sharedPasswords"]![jsonIndex]!["accessLog"]!.ToString());
        Assert.NotNull(accessLogs);
        Assert.Equal(expectedAccessLogsCount, accessLogs!.Count);
    }
}