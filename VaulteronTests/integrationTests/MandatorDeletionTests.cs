﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.Helper;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class MandatorDeletionTests : HarryPotterTestUniverse
{
    public MandatorDeletionTests(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
    }

    [Theory]
    [InlineData(seraphinaPicquery)]
    public async Task SuperAdminCanGetAndDeleteMandators(string superAdminName)
    {
        var superAdmin = TestUser[superAdminName];

        var json = await MakeGraphQLRequestAsync(superAdmin.Email, @" query { mandators { id name } }");
        var mandators = JsonConvert.DeserializeObject<List<Mandator>>(json["data"]!["mandators"]!.ToString());

        Assert.Equal(2, mandators!.Count);

        var variables = new { mandatorId = Mandator.Id };

        await MakeGraphQLRequestAsync(superAdmin.Email,
                                      @" mutation ($mandatorId: Guid!) { mandator { delete (mandatorId: $mandatorId) { id name } } }",
                                      JsonConvert.SerializeObject(variables));


        json = await MakeGraphQLRequestAsync(superAdmin.Email, @" query { mandators { id name } }");
        mandators = JsonConvert.DeserializeObject<List<Mandator>>(json["data"]!["mandators"]!.ToString());

        Assert.Single(mandators);
    }

    [Theory]
    [InlineData(seraphinaPicquery)]
    public async Task SuperAdminCannotDeleteOwnMandator(string superAdminName)
    {
        var superAdmin = TestUser[superAdminName];

        var variables = new { mandatorId = superAdmin.MandatorId };

        await CAssert.ApiExceptionAsync(ErrorCode.ForbiddenSelfOperation, () =>
                                            MakeGraphQLRequestAsync(superAdmin.Email,
                                                                    @" mutation ($mandatorId: Guid!) { mandator { delete (mandatorId: $mandatorId) { id name } } }",
                                                                    JsonConvert.SerializeObject(variables)));
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(percivalGraves)]
    public async Task NonSuperAdminCannotDeleteOwnMandator(string userName)
    {
        var superAdmin = TestUser[userName];

        await CAssert.ApiExceptionAsync(ErrorCode.UserIsNotAuthorized, () =>
                                            MakeGraphQLRequestAsync(superAdmin.Email, @" query { mandators { id name } }"));

        var variables = new { mandatorId = superAdmin.MandatorId };

        await CAssert.ApiExceptionAsync(ErrorCode.UserIsNotAuthorized, () =>
                                            MakeGraphQLRequestAsync(superAdmin.Email,
                                                                    @" mutation ($mandatorId: Guid!) { mandator { delete (mandatorId: $mandatorId) { id name } } }",
                                                                    JsonConvert.SerializeObject(variables)));
    }
}