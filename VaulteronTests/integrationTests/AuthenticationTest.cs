using System.Net;
using System.Threading.Tasks;
using VaulteronDatabase.SeedData;
using VaulteronUtilities.Helper;
using VaulteronWebserver.ViewModels.Users;
using VaulteronTests.helper;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class AuthenticationTest : HarryPotterTestUniverse
{
    public AuthenticationTest(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
    }

    [Theory]
    [InlineData(dumbledore, VaulteronSeedData.LoginPassword)]
    [InlineData(harryPotter, VaulteronSeedData.LoginPassword)]
    public async Task SuccessfulLogin(string userName, string passwordString)
    {
        var user = TestUser[userName];

        var body = new LoginForm { Email = user.Email, Password = CryptoHelper.ComputePasswordHash(passwordString) };

        var response = await Client.PostAsync(ApiLoginUri, body.ToJson());

        // Assert
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        var json = response.ExtractJson();
        Assert.Equal(user.Email, (string)json["email"]);

        // Me query successful after login
        const string query = @"{ me { email } }";
        var jsonMe = (await QueryGraphQLAsync(query)).ExtractJson();
        Assert.Equal(user.Email, jsonMe["data"]!["me"]!["email"]!.ToString());
    }

    [Fact]
    public async Task NonExistentUser()
    {
        var body = new LoginForm { Email = "thisnouser@thisnodomain.nix", Password = VaulteronSeedData.LoginPassword };

        var response = await Client.PostAsync(ApiLoginUri, body.ToJson());

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        var exception = response.ExtractApiClientException();
        Assert.Equal(ErrorCode.UnableToLogin, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, "wrongPassword")]
    [InlineData(harryPotter, "wrongPasswordToo")]
    public async Task WrongPassword(string userName, string passwordString)
    {
        var user = TestUser[userName];
        var body = new LoginForm { Email = user.Email, Password = passwordString };

        var response = await Client.PostAsync(ApiLoginUri, body.ToJson());

        // Assert
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        var exception = response.ExtractApiClientException();
        Assert.Equal(ErrorCode.UnableToLogin, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, VaulteronSeedData.LoginPassword)]
    [InlineData(harryPotter, VaulteronSeedData.LoginPassword)]
    public async Task QueryBeforeLoginAndAfterLogoutUnauthorized(string userName, string passwordString)
    {
        var user = TestUser[userName];

        // Me query before login 401
        const string query = @"{ me { email } }";
        var response = await QueryGraphQLAsync(query);
        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);


        var body = new LoginForm { Email = user.Email, Password = CryptoHelper.ComputePasswordHash(passwordString) };

        response = await Client.PostAsync(ApiLoginUri, body.ToJson());

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        var json = response.ExtractJson();
        Assert.Equal(user.Email, (string)json["email"]);

        // Logout
        await Client.PostAsync(ApiLogoutUri, new { }.ToJson());

        // Me query after logout 401
        response = await QueryGraphQLAsync(query);
        Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
    }
}