﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.ChangeLogTests;

public class PasswordChangeLogTests : HarryPotterTestUniverse
{
    private readonly PasswordTestService passwordTestService;

    public PasswordChangeLogTests(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, harryPotter)]
    public async Task SharedPasswordChangeLogsSuccessful(string username, string otherUserName)
    {
        var callingUser = TestUser[username];
        var otherUser = TestUser[otherUserName];
        var gryffindorGroup = TestGroups[gryffindor];

        var passwordToEdit = passwordTestService.AddSharedPasswordAsync(username, 
                                                                        gryffindor,
                                                                        "new Password Name", 
                                                                        "Password", 
                                                                        gryffindorGroup.Tags.Take(1).Select(t => t.Id).ToList()).Result;

        var ignoreChangesBefore = DateTime.UtcNow;

        var editedPassword = new SharedPassword
        {
            Id = passwordToEdit.Id,
            ModifyLock = passwordToEdit.ModifyLock,

            Name = "a changed name",
            Login = "a changed login",
            Notes = "changed notes",
            WebsiteURL = "a changed website url",
            SharedPasswordTags = new List<SharedPasswordTag>
            {
                new() {PasswordId = passwordToEdit.Id, TagId = gryffindorGroup.Tags.Skip(1).First().Id}
            }
        };

        await passwordTestService.EditSharedPasswordMetaAsync(otherUserName, editedPassword);
        await passwordTestService.MoveSharedPasswordToGroupAsync(username, passwordToEdit.Id, hogwarts, gryffindor);
        await passwordTestService.MoveSharedPasswordToGroupAsync(username, passwordToEdit.Id, gryffindor, hogwarts);
        await passwordTestService.MoveSharedPasswordToCompanyAsync(username, passwordToEdit.Id, otherUserName);
        await passwordTestService.MoveCompanyPasswordToSharedGroupPasswordAsync(username, passwordToEdit.Id, slytherin);

        var changes = (await GetChanges(callingUser, passwordToEdit.Id, ignoreChangesBefore)).ToList();

        // First 6 changes done by otherUser
        Assert.All(changes.Take(6), c =>
        {
            Assert.Equal(otherUserName, c.CreatedBy.Firstname);
            Assert.Equal(otherUserName, c.CreatedBy.Lastname);
            Assert.NotEqual(c.OldValue?.Name, c.NewValue?.Name);

            Assert.True(c.CreatedAt > ignoreChangesBefore);
        });

        // String Metadata
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.Name) == c.PropertyName && passwordToEdit.Name == c.OldValue.Name && editedPassword.Name == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.Login) == c.PropertyName && passwordToEdit.Login == c.OldValue.Name && editedPassword.Login == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.Notes) == c.PropertyName && passwordToEdit.Notes == c.OldValue.Name && editedPassword.Notes == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.WebsiteURL) == c.PropertyName && passwordToEdit.WebsiteURL == c.OldValue.Name && editedPassword.WebsiteURL == c.NewValue.Name
        );

        // Relational Metadata
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.SharedPasswordTags) == c.PropertyName
                            && null == c.NewValue
                            && TestGroups[gryffindor].Tags.First().Id == c.OldValue.EntityId
                            && TestGroups[gryffindor].Tags.First().Name == c.OldValue.Name
        );
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.SharedPasswordTags) == c.PropertyName
                            && null == c.OldValue
                            && TestGroups[gryffindor].Tags.Skip(1).First().Id == c.NewValue.EntityId
                            && TestGroups[gryffindor].Tags.Skip(1).First().Name == c.NewValue.Name
        );

        // Rest is done by callingUser
        Assert.All(changes.Skip(6), c =>
        {
            Assert.Equal(username, c.CreatedBy.Firstname);
            Assert.Equal(username, c.CreatedBy.Lastname);
            Assert.NotEqual(c.OldValue?.Name, c.NewValue?.Name);
        });

        // Move to Group
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.GroupId) == c.PropertyName
                            && TestGroups[hogwarts].Id == c.NewValue.EntityId
                            && TestGroups[hogwarts].Name == c.NewValue.Name
                            && TestGroups[gryffindor].Id == c.OldValue.EntityId
                            && TestGroups[gryffindor].Name == c.OldValue.Name
        );

        // Move From Group To Company
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.GroupId) == c.PropertyName 
                            && null == c.NewValue 
                            && TestGroups[gryffindor].Id == c.OldValue.EntityId 
                            && TestGroups[gryffindor].Name == c.OldValue.Name
        );
        if (username != otherUserName)
            Assert.Contains(changes, c =>
                                nameof(SharedPassword.CreatedById) == c.PropertyName
                                && otherUser.Id == c.NewValue.EntityId
                                && otherUser.Firstname + " " + otherUser.Lastname == c.NewValue.Name
                                && callingUser.Id == c.OldValue.EntityId
                                && callingUser.Firstname + " " + callingUser.Lastname == c.OldValue.Name
            );

        // Move From Company to Group
        Assert.Contains(changes, c =>
                            nameof(SharedPassword.GroupId) == c.PropertyName
                            && null == c.OldValue
                            && TestGroups[slytherin].Id == c.NewValue.EntityId
                            && TestGroups[slytherin].Name == c.NewValue.Name
        );
    }

    [Theory]
    [InlineData(harryPotter)]
    public async Task SharedPasswordChangeLogNonAdmin(string username)
    {
        var callingUser = TestUser[username];
        var gryffindorGroup = TestGroups[gryffindor];

        var passwordToEdit = passwordTestService.AddSharedPasswordAsync(username,
                                                                        gryffindor,
                                                                        "new Password Name",
                                                                        "Password",
                                                                        gryffindorGroup.Tags.Take(1).Select(t => t.Id).ToList()).Result;

        var ignoreChangesBefore = DateTime.UtcNow;

        var editedPassword = new SharedPassword
        {
            Id = passwordToEdit.Id,
            ModifyLock = passwordToEdit.ModifyLock,

            Name = "a changed name",
            Login = "a changed login",
            Notes = "changed notes",
            WebsiteURL = "a changed website url",
            SharedPasswordTags = new List<SharedPasswordTag>
            {
                new() {PasswordId = passwordToEdit.Id, TagId = gryffindorGroup.Tags.Skip(1).First().Id}
            }
        };

        await passwordTestService.EditSharedPasswordMetaAsync(username, editedPassword);

        var changes = (await GetChanges(callingUser, passwordToEdit.Id, ignoreChangesBefore)).ToList();

        Assert.Empty(changes);
    }

        // Access control on accountPassword-Changelogs not yet worked out
        // These Test cases would be for any user only allowed to access his own passwords (even admins)

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task AccountPasswordChangeLogsSuccessful(string username)
    {
        var user = TestUser[username];

        var tags = new List<Guid> {user.Tags.First().Id};
        var passwordToEdit = await passwordTestService.AddAccountPasswordAsync(username, tags: tags);

        var ignoreChangesBefore = DateTime.UtcNow;

        var editedPassword = new AccountPassword
        {
            Id = passwordToEdit.Id,

            Name = "a changed name",
            Login = "a changed login",
            Notes = "changed notes",
            WebsiteURL = "a changed website url"
        };
        tags = new List<Guid> {user.Tags.Skip(1).First().Id};

        await passwordTestService.EditAccountPasswordAsync(username, editedPassword, tags: tags);

        var changes = await GetChanges(user, passwordToEdit.Id, ignoreChangesBefore);

        // Changes done by otherUser
        Assert.All(changes, c =>
        {
            Assert.Equal(username, c.CreatedBy.Firstname);
            Assert.Equal(username, c.CreatedBy.Lastname);
            Assert.NotEqual(c.OldValue?.Name, c.NewValue?.Name);

            Assert.True(c.CreatedAt > ignoreChangesBefore);
        });

        // String Metadata
        Assert.Contains(changes, c =>
            nameof(AccountPassword.Name) == c.PropertyName &&
            passwordToEdit.Name == c.OldValue.Name &&
            editedPassword.Name == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
            nameof(AccountPassword.Login) == c.PropertyName &&
            passwordToEdit.Login == c.OldValue.Name &&
            editedPassword.Login == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
            nameof(AccountPassword.Notes) == c.PropertyName &&
            passwordToEdit.Notes == c.OldValue.Name &&
            editedPassword.Notes == c.NewValue.Name
        );
        Assert.Contains(changes, c =>
            nameof(AccountPassword.WebsiteURL) == c.PropertyName &&
            passwordToEdit.WebsiteURL == c.OldValue.Name &&
            editedPassword.WebsiteURL == c.NewValue.Name
        );
        // Relational Metadata
        Assert.Contains(changes, c =>
            nameof(AccountPassword.AccountPasswordTags) == c.PropertyName &&
            null == c.NewValue &&
            user.Tags.First().Id == c.OldValue.EntityId &&
            user.Tags.First().Name == c.OldValue.Name
        );
        Assert.Contains(changes, c =>
            nameof(AccountPassword.AccountPasswordTags) == c.PropertyName &&
            null == c.OldValue &&
            user.Tags.Skip(1).First().Id == c.NewValue.EntityId &&
            user.Tags.Skip(1).First().Name == c.NewValue.Name
        );
    }


    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(harryPotter, dumbledore)]
    public async Task AccountPasswordChangeLogNonAdmin(string username, string otherUserName)
    {
        var callingUser = TestUser[username];
        var otherUser = TestUser[otherUserName];

        var tags = new List<Guid> {callingUser.Tags.First().Id};
        var passwordToEdit = await passwordTestService.AddAccountPasswordAsync(username, tags: tags);

        var ignoreChangesBefore = DateTime.UtcNow;

        var editedPassword = new AccountPassword
        {
            Id = passwordToEdit.Id,

            Name = "a changed name",
            Login = "a changed login",
            Notes = "changed notes",
            WebsiteURL = "a changed website url"
        };
        tags = new List<Guid> {callingUser.Tags.Skip(1).First().Id};

        await passwordTestService.EditAccountPasswordAsync(username, editedPassword, tags: tags);

        var changes = (await GetChanges(otherUser, passwordToEdit.Id, ignoreChangesBefore)).ToList();

        Assert.Empty(changes);
    }

    #region helper

    private async Task<IEnumerable<GraphQLChangeLogModel>> GetChanges(User callingUser, Guid entityId, DateTime from)
    {
        const string query = @"query ($entityId: Guid!, $from: DateTime){
                            changeLogs (entityId: $entityId, from: $from){
                              id
                              createdAt
                              entityName
                              propertyName
                              createdBy {id firstname lastname}
                              oldValue {entityId name}
                              newValue {entityId name}
                           }
                        }";

        var variables = new
        {
            entityId,
            from
        };

        var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var changes =
            JsonConvert.DeserializeObject<List<GraphQLChangeLogModel>>(json["data"]!["changeLogs"]!.ToString())!.Reverse<GraphQLChangeLogModel>();

        return changes;
    }

    #endregion
}