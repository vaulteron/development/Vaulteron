using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.Helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.ChangeLogTests;

public class PasswordStringChangeLogTests : HarryPotterTestUniverse
{
    private readonly PasswordTestService passwordTestService;
    private readonly UserTestService userTestService;

    public PasswordStringChangeLogTests(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
        userTestService = new UserTestService(logger, this);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task GetAccountPasswordStringChangeLogAfterEditAndLoginPasswordChange(string userName)
    {
        var user = TestUser[userName];
        var password = DbContext.AccountPasswords.Single(pw => pw.Name == accountPassword + userName + one);

        const string newPassword = "password1";

        await passwordTestService.EditAccountPasswordAsync(userName, password, "changedPassword");
        await userTestService.ChangeLoginPassword(user, newPassword);

        var encryptedPws = await GetPasswordStringChangeLogs(user, password.Id, newPassword);

        Assert.Equal(3, encryptedPws.Count);
        Assert.Equal(2, encryptedPws.Select(ep => ep.KeyPairId).Distinct().Count());
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(harryPotter, hermioneGranger)]
    public async Task CannotGetAccountPasswordHistoryFromOtherUser(string callingUserName, string userWithPasswordName)
    {
        var callingUser = TestUser[callingUserName];
        var password = DbContext.AccountPasswords.Single(pw => pw.Name == $"{accountPassword}{userWithPasswordName}{one}");

        await CAssert.ApiExceptionAsync(ErrorCode.DataNotFound, () => GetPasswordStringChangeLogs(callingUser, password.Id));
    }

    [Theory]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(harryPotter, gryffindor)]
    public async Task GetGroupPasswordStringChangeLogAfterEditAndLoginPasswordChange(string userName, string groupForPwName)
    {
        var user = TestUser[userName];
        var password = DbContext.SharedPasswords.Single(pw => pw.Name == sharedPassword + groupForPwName + one);

        const string newPassword = "password1";

        await passwordTestService.EditSharedPasswordAsync(userName, groupForPwName, password, "changedPassword");
        await userTestService.ChangeLoginPassword(user, newPassword);

        var encryptedPws = await GetPasswordStringChangeLogs(user, password.Id, newPassword);

        Assert.Equal(3, encryptedPws.Count);
        Assert.Equal(2, encryptedPws.Select(ep => ep.KeyPairId).Distinct().Count());
    }

    [Theory]
    [InlineData(harryPotter, hogwarts)] // Parent Group
    [InlineData(ronWeasly, gryffindor)] // Group Viewer
    [InlineData(cedricDiggory, gryffindor)] // Different Group
    public async Task CannotGetGroupPasswordHistoryFromOtherUser(string callingUserName, string groupWithPasswordName)
    {
        var callingUser = TestUser[callingUserName];
        var password = DbContext.SharedPasswords.Single(pw => pw.Name == sharedPassword + groupWithPasswordName + one);

        await CAssert.ApiExceptionAsync(ErrorCode.UserIsNotAuthorized, () => GetPasswordStringChangeLogs(callingUser, password.Id));
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    [InlineData(dumbledore, harryPotter)]
    public async Task GetCompanyPasswordStringChangeLogAfterEditAndLoginPasswordChange(string userName, string userWithPasswordName = null)
    {
        userWithPasswordName ??= userName;

        var user = TestUser[userName];
        var password = DbContext.SharedPasswords.Single(pw => pw.GroupId == null && pw.Name == companyPassword + userWithPasswordName + one);

        const string newPassword = "password1";

        await passwordTestService.EditCompanyPasswordAsync(userWithPasswordName, password, "changedPassword");
        await userTestService.ChangeLoginPassword(user, newPassword);

        var encryptedPws = await GetPasswordStringChangeLogs(user, password.Id, newPassword);

        Assert.Equal(3, encryptedPws.Count);
        Assert.Equal(2, encryptedPws.Select(ep => ep.KeyPairId).Distinct().Count());
    }

    [Theory]
    [InlineData(ronWeasly, harryPotter)]
    [InlineData(harryPotter, ronWeasly)]
    public async Task CannotGetCompanyPasswordHistoryFromOtherUser(string callingUserName, string userWithPasswordName)
    {
        var callingUser = TestUser[callingUserName];
        var password = DbContext.SharedPasswords.Single(pw => pw.GroupId == null && pw.Name == companyPassword + userWithPasswordName + one);

        await CAssert.ApiExceptionAsync(ErrorCode.UserIsNotAuthorized, () => GetPasswordStringChangeLogs(callingUser, password.Id));
    }

    [Fact]
    public async Task GetKeyPairChangeLogsAfterAccountActivationAndLoginPwChange()
    {
        var admin = TestUser[dumbledore];

        var newUserModel = new AddUserTestModel
        {
            admin = false,
            email = "testUser@vaulteron.com",
            newTemporaryLoginPassword = "tmpPassword",
            newTemporaryLoginPasswordHashed = CryptoHelper.ComputePasswordHash("tmpPassword"),
            sex = "Male",
            firstname = "test",
            lastname = "User",
        };

        var newUserFromCreate = await userTestService.AddUserWithCorrectEncryptions(admin, newUserModel);
        await userTestService.ActivateLastCreatedAccount();

        const string changedLoginPw = "changedLoginPw";

        var newUser = await DbContext.Users
            .IncludeKeyPairs()
            .SingleAsync(u => u.Email == newUserFromCreate.Email);

        await userTestService.ChangeLoginPassword(newUser, changedLoginPw);

        const string query = @"
                query {
                  keyPairChangeLogs {
                    id
                    createdAt
                    encryptedPrivateKey
                    previousPasswordHashEncryptedWithPublicKey
                  } 
                }";

        var variables = new { };
        await LoginAsAsync(newUser.Email, changedLoginPw);
        var json = await MakeGraphQLRequestAsync(newUser.Email, query, JsonConvert.SerializeObject(variables), true);
        var keyPairChangeLogs = JsonConvert.DeserializeObject<List<KeyPair>>(json["data"]!["keyPairChangeLogs"]!.ToString())!;

        Assert.Equal(3, keyPairChangeLogs.Count);
    }

    private async Task<List<EncryptedPassword>> GetPasswordStringChangeLogs(User user, Guid passwordId, string loginPassword = LoginPassword)
    {
        const string query = @"
                query ($passwordId: Guid!) {
                    passwordChangeLogs (passwordId: $passwordId){
                        id
                        createdAt
                        keyPairId
                        encryptedPasswordString
                        createdBy {id firstname lastname}
                    }
                }";

        var variables = new { passwordId };

        await LoginAsAsync(user.Email, loginPassword);
        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables), true);
        var encryptedPws = JsonConvert.DeserializeObject<List<EncryptedPassword>>(json["data"]!["passwordChangeLogs"]!.ToString())!;
        return encryptedPws;
    }
}