﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.GroupRepository;

public class MoveGroup : HarryPotterTestUniverse
{
    private readonly GroupTestService groupTestService;

    public MoveGroup(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        groupTestService = new GroupTestService(logger, this);
    }

    [Theory]
    [InlineData(dumbledore, gryffindor, null)] // Move to root
    [InlineData(dumbledore, hufflepuff, null)] // Move to root; Source group has no passwords
    [InlineData(severusSnape, gryffindor, hufflepuff, new[] { cedricDiggory })] // Target group has no passwords
    [InlineData(severusSnape, gryffindor, slytherin, new[] { horaceSlughorn, albusPotter, dracoMalfoy })] // Target group has passwords
    [InlineData(severusSnape, slytherin, gryffindor, new[] { godricGryffindor, harryPotter, hermioneGranger, ronWeasly })] // Source group has no passwords
    public async Task GroupAndSysAdminCanMoveGroups(string username, string groupToMoveName, string newParentGroupName, string[] userToEncryptForNames = null)
    {
        var user = TestUser[username];
        var groupToMove = TestGroups[groupToMoveName];
        var userToEncryptForIds = userToEncryptForNames?.Select(un => TestUser[un].Id).ToList();
        var newParentGroup = newParentGroupName == null ? null : TestGroups[newParentGroupName];

        await groupTestService.MoveGroup(user, groupToMove.Id, newParentGroup?.Id, userToEncryptForIds);

        var returnedGroups = await groupTestService.GetGroupsByParent(username, newParentGroupName);
        Assert.Contains(returnedGroups, g => g.Id == groupToMove.Id);
    }

    [Theory]
    [InlineData(dumbledore, gryffindor)]
    public async Task EmptyGuidBehavesLikeNullResultingInRootGroup(string username, string groupToMoveName)
    {
        var user = TestUser[username];
        var groupToMove = TestGroups[groupToMoveName];

        await groupTestService.MoveGroup(user, groupToMove.Id, Guid.Empty);

        var returnedGroups = await groupTestService.GetGroupsByParent(username, null);

        Assert.Contains(returnedGroups, g => g.Id == groupToMove.Id);
    }

    [Theory]
    [InlineData(severusSnape, gryffindor, null)]
    [InlineData(minervaMcGonagall, slytherin, gryffindor)]
    [InlineData(harryPotter, slytherin, gryffindor)]
    [InlineData(harryPotter, gryffindor, durmstrang, new string[] { })]
    public async Task CanNotMoveGroupsWhenNotAuthorized(string username, string groupToMoveName, string newParentGroupName, string[] userToEncryptForNames = null)
    {
        var user = TestUser[username];
        var groupToMove = TestGroups[groupToMoveName];

        var newParentGroup = newParentGroupName == null ? null : TestGroups[newParentGroupName];
        var userToEncryptForIds = userToEncryptForNames?.Select(un => TestUser[un].Id);

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => groupTestService.MoveGroup(user, groupToMove.Id, newParentGroup?.Id, userToEncryptForIds));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var returnedGroups = await groupTestService.GetGroupsByParent(username, newParentGroupName);

        Assert.DoesNotContain(returnedGroups, g => g.Id == groupToMove.Id);
    }

    [Theory]
    [InlineData(dumbledore)]
    public async Task CanNotMoveInvalidGroupId(string username)
    {
        var user = TestUser[username];
        await Assert_InputInvalid(user, Guid.NewGuid(), null);
    }

    [Theory]
    [InlineData(dumbledore)]
    public async Task CanNotMoveToInvalidGroupId(string username)
    {
        var user = TestUser[username];

        await Assert_InputInvalid(user, TestGroups[hogwarts].Id, Guid.NewGuid(), Array.Empty<Guid>());
    }

    [Theory]
    [InlineData(dumbledore)]
    public async Task CanNotMoveToSubGroup(string username)
    {
        var user = TestUser[username];
        await Assert_InputInvalid(user, TestGroups[hogwarts].Id, TestGroups[gryffindor].Id);
    }

    // Helper

    private async Task Assert_InputInvalid(User user, Guid groupToMoveId, Guid? newParentGroupId, IEnumerable<Guid> userToEncryptForIds = null)
    {
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => groupTestService.MoveGroup(user, groupToMoveId, newParentGroupId, userToEncryptForIds));
        Assert.Equal(ErrorCode.InputInvalid, exception.ErrorCode);
    }
}