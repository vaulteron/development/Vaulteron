﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.GroupRepository;

public class UserGroupCRUD : HarryPotterTestUniverse
{
    private readonly Dictionary<string, SharedPassword> testPasswords = new();
    private const string PasswordGryffindor = "passwordInGryffindor";
    private const string SharedPasswordString = "somePWstring";

    private readonly GroupTestService groupTestService;
    private readonly PasswordTestService passwordTestService;

    // TODO: create tests for creating a sub-group as group-admin

    public UserGroupCRUD(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null) : base(factory, logger, hptu)
    {
        groupTestService = new GroupTestService(logger, this);
        passwordTestService = new PasswordTestService(logger, this);

        testPasswords[PasswordGryffindor] =
            passwordTestService.AddSharedPasswordAsync(dumbledore, gryffindor, PasswordGryffindor, SharedPasswordString).Result;
    }


    #region list Group Users

    [Theory]
    [InlineData(dumbledore, hogwarts,
                new[] { minervaMcGonagall, pomonaSprout, filiusFlitwick, severusSnape, nearlyHeadlessNick })]
    [InlineData(dumbledore, gryffindor,
                new[] { minervaMcGonagall, godricGryffindor, harryPotter, ronWeasly, hermioneGranger })]
    [InlineData(dumbledore, hufflepuff, new[] { pomonaSprout, cedricDiggory })]
    [InlineData(dumbledore, ravenclaw, new[] { filiusFlitwick, lunaLovegood, garrickOllivander })]
    [InlineData(dumbledore, slytherin, new[] { severusSnape, dracoMalfoy, horaceSlughorn })]
    public async Task GetAllGroupUsers(string userThatMakesRequest, string targetGroup, string[] expectedUserNames)
    {
        var expectedUsers = expectedUserNames.Select(u => TestUser[u]).ToList();
        var users = await groupTestService.GetUsers(userThatMakesRequest, targetGroup);
        var userIds = users.Select(u => u.Id).ToList();
        Assert.Equal(expectedUsers.Count, userIds.Count);
        var expectedUsersIds = expectedUsers.Select(u => u.Id).ToList();
        foreach (var expectedUser in expectedUsersIds) Assert.Contains(expectedUser, userIds);
    }

    [Theory]
    [InlineData(dumbledore, hogwarts,
                new[]
                {
                    dumbledore, doloresUmbridge, minervaMcGonagall, nearlyHeadlessNick, pomonaSprout, filiusFlitwick,
                    severusSnape
                })]
    [InlineData(dumbledore, gryffindor,
                new[]
                {
                    dumbledore, doloresUmbridge, minervaMcGonagall, nearlyHeadlessNick, pomonaSprout, filiusFlitwick,
                    severusSnape, godricGryffindor, harryPotter, hermioneGranger, ronWeasly
                })]
    [InlineData(dumbledore, hufflepuff,
                new[]
                {
                    dumbledore, doloresUmbridge, minervaMcGonagall, nearlyHeadlessNick, pomonaSprout, filiusFlitwick,
                    severusSnape, cedricDiggory
                })]
    [InlineData(dumbledore, ravenclaw,
                new[]
                {
                    dumbledore, doloresUmbridge, minervaMcGonagall, nearlyHeadlessNick, pomonaSprout, filiusFlitwick,
                    severusSnape, lunaLovegood, garrickOllivander
                })]
    [InlineData(dumbledore, slytherin,
                new[]
                {
                    dumbledore, doloresUmbridge, minervaMcGonagall, nearlyHeadlessNick, pomonaSprout, filiusFlitwick,
                    severusSnape, dracoMalfoy, horaceSlughorn, albusPotter
                })]
    public async Task GetAllUsersWithAccess(string userThatMakesRequest, string targetGroup,
        string[] expectedUserNames)
    {
        var expectedUsers = expectedUserNames.Select(u => TestUser[u]).ToList();

        var users = (await groupTestService.GetUsers(userThatMakesRequest, targetGroup, true))
            .Select(u => u.Id).ToList();
        Assert.Equal(expectedUsers.Count, users.Count);
        var expectedUsersIds = expectedUsers.Select(u => u.Id).ToList();
        foreach (var expectedUser in expectedUsersIds) Assert.Contains(expectedUser, users);
    }

    #endregion

    #region add User to Group

    [Theory]
    [InlineData(dumbledore, slytherin, harryPotter, GroupRole.GroupAdmin)]
    [InlineData(dumbledore, hogwarts, harryPotter, GroupRole.GroupAdmin)]
    [InlineData(severusSnape, hogwarts, igorKarkaroff, GroupRole.GroupEditor)]
    [InlineData(filiusFlitwick, ravenclaw, viktorKrum, GroupRole.GroupViewer)]
    public async Task GroupAdminCanAddUserToGroup(string userName, string groupToAddName, string userToAddName,
        GroupRole groupRole)
    {
        var admin = TestUser[userName];
        var group = TestGroups[groupToAddName];
        var userToAdd = TestUser[userToAddName];

        var json = await groupTestService.AddUserToGroup(admin, userToAddName, groupToAddName, groupRole);
        var resUser = JsonConvert.DeserializeObject<User>(json["data"]!["group"]!["addUser"]!.ToString());

        Assert.NotNull(resUser);
        Assert.Equal(userToAdd.Id, resUser!.Id);
        Assert.Equal(userToAdd.Firstname, resUser.Firstname);

        var users = await groupTestService.GetUsers(userName, groupToAddName);
        Assert.Contains(users, u => u.Id == userToAdd.Id && u.UserGroups.Any(ug => ug.GroupId == group.Id && ug.GroupRole == groupRole));

        var sharedPasswordsInGroup =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(userToAddName, groupToAddName);
        foreach (var sharedPassword in sharedPasswordsInGroup)
        {
            await passwordTestService.GetSingleSharedPasswordStringAsync(userToAdd, sharedPassword.Id);
        }

        Assert.True(true); // Last code must not throw
    }


    [Theory]
    [InlineData(hogwarts, severusSnape)]
    [InlineData(hogwarts, minervaMcGonagall)]
    [InlineData(hogwarts, pomonaSprout)]
    [InlineData(hogwarts, filiusFlitwick)]
    [InlineData(hogwarts, nearlyHeadlessNick)]
    [InlineData(gryffindor, minervaMcGonagall)]
    [InlineData(gryffindor, godricGryffindor)]
    [InlineData(gryffindor, harryPotter)]
    [InlineData(gryffindor, hermioneGranger)]
    [InlineData(gryffindor, ronWeasly)]
    [InlineData(hufflepuff, pomonaSprout)]
    [InlineData(hufflepuff, cedricDiggory)]
    [InlineData(ravenclaw, filiusFlitwick)]
    [InlineData(ravenclaw, lunaLovegood)]
    [InlineData(ravenclaw, garrickOllivander)]
    [InlineData(slytherin, severusSnape)]
    [InlineData(slytherin, dracoMalfoy)]
    [InlineData(slytherin, albusPotter)]
    [InlineData(slytherin, horaceSlughorn)]
    [InlineData(durmstrang, igorKarkaroff)]
    public async Task GroupAdminCannotAddUserToGroupWhenAlreadyPresentInGroup(string groupToAddName,
        string userToAddName)
    {
        var admin = TestUser[dumbledore];

        var exception = await Assert.ThrowsAsync<ApiClientException>(async () =>
                                                                         await groupTestService.AddUserToGroup(admin, userToAddName, groupToAddName, GroupRole.GroupViewer));
        Assert.Equal(ErrorCode.InputInvalid, exception.ErrorCode);
        Assert.Equal("This user is already present in this group", exception.Message);
    }

    [Fact]
    public async Task UserInParentAndSubGroupCanStillAccessPasswordsWhenRemovedFromSubGroup()
    {
        const string callingUserName = dumbledore;
        const string targetUserName = harryPotter;
        var admin = TestUser[callingUserName];
        var targetUser = TestUser[targetUserName];
        const string parentGroupName = "parent";
        const string childGroupName = "child";

        #region Arrange

        // Create parent group
        var parentGroup = await groupTestService.CreateGroup(callingUserName, parentGroupName);
        TestGroups[parentGroupName] = parentGroup;
        await groupTestService.AddUserToGroup(admin, targetUserName, parentGroupName, GroupRole.GroupViewer,
                                              Array.Empty<string>());
        var parentGroupPassword =
            await passwordTestService.AddSharedPasswordAsync(callingUserName, parentGroupName, "pw-parent",
                                                             LoginPassword);

        // Create child group
        var childGroup = await groupTestService.CreateGroup(callingUserName, childGroupName, parentGroup.Id);
        TestGroups[childGroupName] = childGroup;
        var childGroupPassword =
            await passwordTestService.AddSharedPasswordAsync(callingUserName, childGroupName, "pw-child",
                                                             LoginPassword);

        // Check that target user can access both passwords
        var allParentGroupPasswords =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(targetUserName, parentGroupName);
        var singlePasswordInParent = Assert.Single(allParentGroupPasswords);
        Assert.Equal(parentGroupPassword, singlePasswordInParent);
        await passwordTestService.GetSingleSharedPasswordStringAsync(targetUser, singlePasswordInParent.Id);
        var allChildGroupPasswords =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(targetUserName, childGroupName);
        var singlePasswordInChild = Assert.Single(allChildGroupPasswords);
        Assert.Equal(childGroupPassword, singlePasswordInChild);
        await passwordTestService.GetSingleSharedPasswordStringAsync(targetUser, singlePasswordInChild.Id);

        // Now add target user to child group
        await groupTestService.AddUserToGroup(admin, targetUserName, childGroupName, GroupRole.GroupEditor,
                                              Array.Empty<string>());
        var usersInChildGroupBeforeRemoval = await groupTestService.GetUsers(callingUserName, childGroup.Id);
        Assert.Single(usersInChildGroupBeforeRemoval);

        #endregion

        #region Act

        await groupTestService.RemoveUserFromGroup(admin, targetUser, childGroup);

        #endregion

        #region Assert

        // Verify that user is not in group anymore
        var usersInChildGroup = await groupTestService.GetUsers(callingUserName, childGroup.Id);
        Assert.Empty(usersInChildGroup);

        // Verify user can access both passwords
        var allParentGroupPasswords2 =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(targetUserName, parentGroupName);
        var singlePasswordInParent2 = Assert.Single(allParentGroupPasswords2);
        Assert.Equal(parentGroupPassword, singlePasswordInParent2);
        await passwordTestService.GetSingleSharedPasswordStringAsync(targetUser, singlePasswordInParent2.Id);
        var allChildGroupPasswords2 =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(targetUserName, childGroupName);
        var singlePasswordInChild2 = Assert.Single(allChildGroupPasswords2);
        Assert.Equal(childGroupPassword, singlePasswordInChild2);
        await passwordTestService.GetSingleSharedPasswordStringAsync(targetUser, singlePasswordInChild2.Id);

        #endregion
    }

    [Theory]
    [InlineData(dumbledore, slytherin, dumbledore, GroupRole.GroupAdmin)]
    [InlineData(severusSnape, hogwarts, dumbledore, GroupRole.GroupEditor)]
    [InlineData(godricGryffindor, gryffindor, dumbledore, GroupRole.GroupEditor)]
    [InlineData(filiusFlitwick, ravenclaw, dumbledore, GroupRole.GroupViewer)]
    public async Task CannotAddSysAdminToGroup(string userName, string groupToAddName, string userToAddName,
        GroupRole groupRole)
    {
        var normalUser = TestUser[userName];
        var userToAdd = TestUser[userToAddName];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         groupTestService.AddUserToGroup(normalUser, userToAddName, groupToAddName, groupRole));
        Assert.Equal(ErrorCode.Forbidden, exception.ErrorCode);

        var users = await groupTestService.GetUsers(userName, groupToAddName);
        Assert.DoesNotContain(users, u => u.Id == userToAdd.Id);
    }

    [Theory]
    [InlineData(dracoMalfoy, slytherin, harryPotter, GroupRole.GroupAdmin)]
    [InlineData(pomonaSprout, hogwarts, igorKarkaroff, GroupRole.GroupEditor)]
    [InlineData(garrickOllivander, ravenclaw, viktorKrum, GroupRole.GroupViewer)]
    public async Task NormalUserCannotAddUserToGroup(string userName, string groupToAddName, string userToAddName,
        GroupRole groupRole)
    {
        var normalUser = TestUser[userName];
        var group = TestGroups[groupToAddName];
        var userToAdd = TestUser[userToAddName];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         groupTestService.AddUserToGroup(normalUser, userToAddName, groupToAddName, groupRole));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var users = await groupTestService.GetUsers(userName, groupToAddName);
        Assert.DoesNotContain(users, u => u.Id == userToAdd.Id);
    }

    #endregion

    #region edit User of Group

    [Theory]
    [InlineData(dumbledore, slytherin, dracoMalfoy, GroupRole.GroupAdmin)]
    [InlineData(severusSnape, hogwarts, minervaMcGonagall, GroupRole.GroupViewer)]
    [InlineData(filiusFlitwick, ravenclaw, garrickOllivander, GroupRole.GroupEditor)]
    public async Task GroupAdminCanEditUserOfGroup(string userName, string groupName, string userToEditName,
        GroupRole groupRole)
    {
        var admin = TestUser[userName];
        var group = TestGroups[groupName];
        var userToEdit = TestUser[userToEditName];

        const string query = @"
                mutation ($user: EditUserGroupType!) {
                  group {
                    editUser (user: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new
        {
            user = new
            {
                groupId = group.Id,
                userId = userToEdit.Id,
                groupRole = groupTestService.GroupRoleToGraphQLStr(groupRole)
            }
        };

        var json = await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));
        var resUser = JsonConvert.DeserializeObject<User>(json["data"]!["group"]!["editUser"]!.ToString());

        Assert.NotNull(resUser);
        Assert.Equal(userToEdit.Id, resUser!.Id);
        Assert.Equal(userToEdit.Firstname, resUser.Firstname);

        var users = await groupTestService.GetUsers(userName, groupName);
        Assert.Contains(users, u => u.Id == userToEdit.Id && u.UserGroups.Any(ug => ug.GroupId == group.Id && ug.GroupRole == groupRole));
    }

    [Theory]
    [InlineData(dracoMalfoy, slytherin, dracoMalfoy, GroupRole.GroupAdmin)]
    [InlineData(filiusFlitwick, hogwarts, minervaMcGonagall, GroupRole.GroupViewer)]
    [InlineData(garrickOllivander, ravenclaw, filiusFlitwick, GroupRole.GroupEditor)]
    public async Task NormalUserCannotEditUserOfGroup(string userName, string groupToAddName, string userToAddName, GroupRole groupRole)
    {
        var normalUser = TestUser[userName];
        var group = TestGroups[groupToAddName];
        var userToAdd = TestUser[userToAddName];

        const string query = @"
                mutation ($user: EditUserGroupType!) {
                  group {
                    editUser (user: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new
        {
            user = new
            {
                groupId = group.Id,
                userId = userToAdd.Id,
                groupRole = groupTestService.GroupRoleToGraphQLStr(groupRole)
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var users = await groupTestService.GetUsers(userName, groupToAddName);
        Assert.DoesNotContain(users, u => u.Id == userToAdd.Id
                                  && u.UserGroups.Any(
                                      ug => ug.GroupId == group.Id && ug.GroupRole == groupRole));
    }

    #endregion

    #region remove User from Group

    [Theory]
    [InlineData(dumbledore, harryPotter, gryffindor)]
    [InlineData(severusSnape, harryPotter, gryffindor)]
    [InlineData(minervaMcGonagall, harryPotter, gryffindor)]
    public async Task GroupAdminCanRemoveUserFromGroup(string userName, string userToRemoveName,
        string groupToDeleteFromName)
    {
        var admin = TestUser[userName];
        var userToRemove = TestUser[userToRemoveName];
        var group = TestGroups[groupToDeleteFromName];

        var resUser = await groupTestService.RemoveUserFromGroup(admin, userToRemove, group);

        Assert.Equal(userToRemove.Id, resUser.Id);
        Assert.Equal(userToRemove.Firstname, resUser.Firstname);

        var users = await groupTestService.GetUsers(userName, groupToDeleteFromName);
        Assert.DoesNotContain(users, u => u.Id == userToRemove.Id);
    }

    [Theory]
    [InlineData(filiusFlitwick, harryPotter, gryffindor)]
    [InlineData(hermioneGranger, harryPotter, gryffindor)]
    [InlineData(harryPotter, hermioneGranger, gryffindor)]
    public async Task NormalUserCannotRemoveUserFromGroup(string userName, string userToRemoveName,
        string groupToDeleteFromName)
    {
        var normalUser = TestUser[userName];
        var userToRemove = TestUser[userToRemoveName];
        var group = TestGroups[groupToDeleteFromName];

        const string query = @"
                mutation ($group: Guid!, $user: Guid!) {
                  group {
                    deleteUser (groupId: $group, userId: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new { group = group.Id, user = userToRemove.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var users = await groupTestService.GetUsers(userName, groupToDeleteFromName);
        Assert.Contains(users, u => u.Id == userToRemove.Id);
    }

    [Theory]
    [InlineData(minervaMcGonagall, minervaMcGonagall, gryffindor)]
    [InlineData(severusSnape, severusSnape, slytherin)]
    [InlineData(pomonaSprout, pomonaSprout, hufflepuff)]
    [InlineData(filiusFlitwick, filiusFlitwick, ravenclaw)]
    public async Task AdminCannotRemoveHimselfFromGroup(string userName, string userToRemoveName,
        string groupToDeleteFromName)
    {
        var normalUser = TestUser[userName];
        var userToRemove = TestUser[userToRemoveName];
        var group = TestGroups[groupToDeleteFromName];

        const string query = @"
                mutation ($group: Guid!, $user: Guid!) {
                  group {
                    deleteUser (groupId: $group, userId: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new { group = group.Id, user = userToRemove.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.ForbiddenSelfOperation, exception.ErrorCode);

        var users = await groupTestService.GetUsers(userName, groupToDeleteFromName);
        Assert.Contains(users, u => u.Id == userToRemove.Id);
    }

    [Fact]
    public async Task EncryptedPasswordShouldBeDeletedWhenUserIsRemovedFromGroup()
    {
        var callingUser = TestUser[dumbledore];
        var targetGroup = TestGroups[gryffindor];
        var userToBeRemoved = TestUser[harryPotter];

        // First make sure user has access to a password inside this group
        var targetPasswordString =
            await passwordTestService.GetSingleSharedPasswordStringAsync(userToBeRemoved,
                                                                         testPasswords[PasswordGryffindor].Id);
        var decrypted = CryptoHelper.DecryptAsymmetrical(targetPasswordString, LoginPassword,
                                                         userToBeRemoved.KeyPairs.LastActive().EncryptedPrivateKey);
        Assert.Equal(SharedPasswordString, decrypted);

        // Then remove the user
        const string query = @"
                mutation ($group: Guid!, $user: Guid!) {
                  group {
                    deleteUser (groupId: $group, userId: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new { group = targetGroup.Id, user = userToBeRemoved.Id };
        await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));

        // Now check again if the user can access the password
        var exception = await Assert.ThrowsAsync<ApiClientException>(async () =>
                                                                         await passwordTestService.GetSingleSharedPasswordStringAsync(userToBeRemoved, testPasswords[PasswordGryffindor].Id));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    #endregion

    #region Helper

    #endregion
}