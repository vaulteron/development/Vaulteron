﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.GroupRepository;

public class GroupCRUD : HarryPotterTestUniverse
{
    private readonly GroupTestService groupTestService;

    public GroupCRUD(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        groupTestService = new GroupTestService(logger, this);
    }

    #region list Groups

    [Theory]
    [InlineData(dumbledore, null, new[] { hogwarts, durmstrang })]
    [InlineData(dumbledore, hogwarts, new[] { gryffindor, hufflepuff, ravenclaw, slytherin })]
    [InlineData(minervaMcGonagall, null, new[] { hogwarts })]
    [InlineData(minervaMcGonagall, hogwarts, new[] { gryffindor, hufflepuff, ravenclaw, slytherin })]
    [InlineData(harryPotter, null, new[] { gryffindor })]
    [InlineData(harryPotter, hogwarts, new[] { gryffindor })]
    [InlineData(harryPotter, gryffindor, new string[] { })]
    [InlineData(severusSnape, null, new[] { hogwarts })]
    [InlineData(dracoMalfoy, null, new[] { slytherin })]
    public async Task GetGroupByParent(string userThatMakesRequestName, string parentGroupName,
        string[] expectedGroupNames)
    {
        var expectedGroups = expectedGroupNames.Select(u => TestGroups[u]).ToList();

        List<Group> returnedGroups =
            await groupTestService.GetGroupsByParent(userThatMakesRequestName, parentGroupName);

        Assert.Equal(expectedGroups.Count, returnedGroups.Count);
        var expectedGroupIds = expectedGroups.Select(u => u.Id).ToList();
        var returnedGroupIds = returnedGroups.Select(u => u.Id).ToList();
        foreach (var expectedGroup in expectedGroupIds) Assert.Contains(expectedGroup, returnedGroupIds);
    }

    [Theory]
    [InlineData(dumbledore, hogwarts, new[] { gryffindor, hufflepuff, ravenclaw, slytherin })]
    public async Task GetGroupByParentViaSubGroups(string userName, string parentGroupName, string[] expectedGroupNames)
    {
        var user = TestUser[userName];
        var parentGroup = parentGroupName != null ? TestGroups[parentGroupName] : null;
        var expectedGroups = expectedGroupNames.Select(u => TestGroups[u]).ToList();

        const string query = @"
                query($ids: [Guid]) {
                  groups(ids: $ids ) {
                    id name 
                    countSubGroups
                    children { id name }
                  }
                }
               ";
        var variables = new { ids = new List<Guid> { parentGroup!.Id } };
        var json = await MakeGraphQLRequestAsync(user.Email, query,
                                                 JsonConvert.SerializeObject(variables));

        JsonConvert.DeserializeObject<Group>(json["data"]!["groups"]!.First().ToString());
        var subGroups = JsonConvert.DeserializeObject<List<Group>>(
            json["data"]!["groups"]!.First()
                .First(t => ((JProperty)t).Name == "children").First().ToString()
        );
        var subGroupsCount = JsonConvert.DeserializeObject<int>(
            json["data"]!["groups"]!.First()
                .First(t => ((JProperty)t).Name == "countSubGroups").First().ToString()
        );

        Assert.NotNull(subGroups);
        Assert.Equal(expectedGroups.Count, subGroups!.Count);
        Assert.Equal(expectedGroups.Count, subGroupsCount);
        var expectedGroupIds = expectedGroups.Select(u => u.Id).ToList();
        var returnedGroupIds = subGroups.Select(u => u.Id).ToList();
        foreach (var expectedGroup in expectedGroupIds) Assert.Contains(expectedGroup, returnedGroupIds);
    }

    #endregion

    #region add Group

    [Theory]
    [InlineData(dumbledore, null, "Beauxbatons")]
    [InlineData(dumbledore, hogwarts, "Teachers")]
    [InlineData(dumbledore, gryffindor, "First Grade")]
    [InlineData(minervaMcGonagall, gryffindor, "First Grade")]
    [InlineData(severusSnape, hogwarts, "First Grade")]
    [InlineData(severusSnape, gryffindor, "First Grade")]
    [InlineData(severusSnape, slytherin, "First Grade")]
    public async Task<Group> GroupAdminCanAddGroup(string userName, string parentGroupName, string newGroupName)
    {
        var admin = TestUser[userName];
        var parentGroup = parentGroupName != null ? TestGroups[parentGroupName] : null;

        const string query = @"
                mutation ($group: GroupInputType!) {
                  group { add (group: $group) {
                    id name
                  }}
                }
            ";
        var variables = new { group = new { name = newGroupName, parentGroupId = parentGroup?.Id } };

        var json = await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));
        var resGroup = JsonConvert.DeserializeObject<Group>(json["data"]!["group"]!["add"]!.ToString());

        Assert.NotNull(resGroup);
        Assert.Equal(newGroupName, resGroup!.Name);

        Assert.Contains(await groupTestService.GetGroupsByParent(userName, parentGroupName),
                        g => g.Name == resGroup.Name);

        return resGroup;
    }

    [Theory]
    [InlineData(harryPotter, null, "Dumbledore's Army")]
    [InlineData(horaceSlughorn, null, "Slug Club")]
    [InlineData(ronWeasly, gryffindor, "Harry Potter Fan Club")]
    [InlineData(harryPotter, gryffindor, "Dumbledore's Army")]
    [InlineData(minervaMcGonagall, hogwarts, "First Grade")]
    [InlineData(minervaMcGonagall, slytherin, "First Grade")]
    public async Task NormalUserCannotAddGroup(string userName, string parentGroupName, string newGroupName)
    {
        var normalUser = TestUser[userName];
        var parentGroup = parentGroupName != null ? TestGroups[parentGroupName] : null;

        const string query = @"
                mutation ($group: GroupInputType!) {
                  group { add (group: $group) {
                    id name
                  } }
                }
            ";
        var variables = new { group = new { name = newGroupName, parentGroupId = parentGroup?.Id } };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        Assert.DoesNotContain(await groupTestService.GetGroupsByParent(userName, parentGroupName),
                              g => g.Name == newGroupName);
    }

    [Theory]
    [InlineData(dumbledore, null)]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task AddedGroupNameCantBeEmpty(string userName, string parentGroupName)
    {
        const string newGroupName = "";

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         GroupAdminCanAddGroup(userName, parentGroupName, newGroupName));
        Assert.Equal(ErrorCode.InputInvalid, exception.ErrorCode);

        Assert.DoesNotContain(await groupTestService.GetGroupsByParent(userName, parentGroupName),
                              g => g.Name == newGroupName);
    }

    #endregion

    #region edit Group

    [Theory]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(dumbledore, slytherin)]
    [InlineData(dumbledore, durmstrang)]
    [InlineData(severusSnape, hogwarts)]
    [InlineData(severusSnape, gryffindor)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task AdminOrGroupAdminCanEditGroup(string username, string groupname)
    {
        var callingUser = TestUser[username];
        var targetGroup = TestGroups[groupname];
        const string newName = "abc123";

        var editedGroup = await groupTestService.EditGroup(callingUser, targetGroup, newName);
        Assert.Equal(newName, editedGroup.Name);
    }

    [Theory]
    [InlineData(harryPotter, hogwarts)]
    [InlineData(harryPotter, gryffindor)]
    [InlineData(ronWeasly, gryffindor)]
    public async Task UserWithoutPermissionsCannotEditGroup(string username, string groupname)
    {
        var callingUser = TestUser[username];
        var targetGroup = TestGroups[groupname];
        const string newName = "abc123";

        var exception = await Assert.ThrowsAsync<ApiClientException>(async () =>
                                                                         await groupTestService.EditGroup(callingUser, targetGroup, newName));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    #endregion

    #region archive Group

    [Theory]
    [InlineData(dumbledore, "TestGroup")]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(dumbledore, slytherin)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task<Group> AdminCanArchiveGroup(string userName, string groupToArchiveName)
    {
        var admin = TestUser[userName];

        Group testGroup;
        if (TestGroups.ContainsKey(groupToArchiveName))
        {
            testGroup = TestGroups[groupToArchiveName];
        }
        else
        {
            testGroup = await GroupAdminCanAddGroup(userName, null, groupToArchiveName);
        }

        await groupTestService.ArchiveGroup(admin, testGroup.Id);

        var archivedGroups = await GetGroups(userName, byArchived: true);

        Assert.All(archivedGroups,
                   g => { Assert.Equal(DateTime.UtcNow, g.ArchivedAt!.Value, new TimeSpan(0, 5, 0)); });

        Assert.Contains(archivedGroups, g => g.Id == testGroup.Id);
        Assert.Contains(await GetGroups(userName, byArchived: null), g => g.Id == testGroup.Id);
        Assert.DoesNotContain(await GetGroups(userName, byArchived: false), g => g.Id == testGroup.Id);

        return testGroup;
    }

    [Theory]
    [InlineData(dumbledore, hogwarts, new[] { slytherin, gryffindor })]
    public async Task SubgroupsArchivedToo(string userName, string groupToArchiveName, string[] someSubgroups)
    {
        var admin = TestUser[userName];
        var testGroup = TestGroups[groupToArchiveName];

        await groupTestService.ArchiveGroup(admin, testGroup.Id);

        var someSubgroupIds = someSubgroups.Select(n => TestGroups[n].Id);

        Assert.Contains(await groupTestService.GetGroupsByParent(userName, groupToArchiveName, byArchived: null),
                        g => someSubgroupIds.Contains(g.Id));
        Assert.DoesNotContain(
            await groupTestService.GetGroupsByParent(userName, groupToArchiveName, byArchived: false),
            g => someSubgroupIds.Contains(g.Id));
        Assert.Contains(await groupTestService.GetGroupsByParent(userName, groupToArchiveName, byArchived: true),
                        g => someSubgroupIds.Contains(g.Id));
    }

    [Theory]
    [InlineData(dumbledore, "TestGroup")]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(dumbledore, slytherin)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task ReactivateArchivedGroup(string userName, string groupToReactivateName)
    {
        var testGroup = await AdminCanArchiveGroup(userName, groupToReactivateName);

        var admin = TestUser[userName];

        await groupTestService.ReactivateGroup(admin, testGroup.Id);

        Assert.Contains(await GetGroups(userName, byArchived: null), g => g.Id == testGroup.Id);
        Assert.Contains(await GetGroups(userName, byArchived: false), g => g.Id == testGroup.Id);
        Assert.DoesNotContain(await GetGroups(userName, byArchived: true), g => g.Id == testGroup.Id);
    }

    [Theory]
    [InlineData(dumbledore, "TestGroup")]
    [InlineData(dumbledore, hogwarts)]
    [InlineData(dumbledore, slytherin)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task DeleteArchivedGroup(string userName, string groupToReactivateName)
    {
        var testGroup = await AdminCanArchiveGroup(userName, groupToReactivateName);

        var admin = TestUser[userName];

        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    delete (groupId: $group) {
                      id name
                    }
                  }
                }
            ";
        var variables = new { group = testGroup.Id };
        await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));

        Assert.DoesNotContain(await GetGroups(userName, byArchived: null), g => g.Id == testGroup.Id);
        Assert.DoesNotContain(await GetGroups(userName, byArchived: false), g => g.Id == testGroup.Id);
        Assert.DoesNotContain(await GetGroups(userName, byArchived: true), g => g.Id == testGroup.Id);
    }

    [Theory]
    [InlineData(dracoMalfoy, slytherin)]
    public async Task NormalUserCannotArchiveGroup(string userName, string groupToDeleteName)
    {
        var normalUser = TestUser[userName];
        var testgroup = TestGroups[groupToDeleteName];

        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    delete (groupId: $group) {
                      id name
                    }
                  }
                }
            ";
        var variables = new { group = testgroup.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var groups = await GetGroups(userName);
        Assert.Contains(groups, g => g.Id == testgroup.Id);
    }

    [Theory]
    [InlineData(minervaMcGonagall, ronWeasly, gryffindor)]
    public async Task NormalUserCannotSeeArchivedGroup(string userNameToArchive, string userNameToLook,
        string groupToArchiveName)
    {
        var group = TestGroups[groupToArchiveName];

        await AdminCanArchiveGroup(userNameToArchive, groupToArchiveName);

        Assert.DoesNotContain(await GetGroups(userNameToLook, byArchived: null), g => g.Id == group.Id);
        Assert.DoesNotContain(await GetGroups(userNameToLook, byArchived: false), g => g.Id == group.Id);
        Assert.DoesNotContain(await GetGroups(userNameToLook, byArchived: true), g => g.Id == group.Id);
    }

    #endregion

    #region delete Group

    [Theory]
    [InlineData(dumbledore, "TestGroup")]
    [InlineData(dumbledore, slytherin)]
    [InlineData(minervaMcGonagall, gryffindor)]
    public async Task AdminCanDeleteGroup(string userName, string groupToDeleteName)
    {
        var admin = TestUser[userName];

        Group testGroup;
        if (TestGroups.ContainsKey(groupToDeleteName))
        {
            testGroup = TestGroups[groupToDeleteName];
            TestGroups.Remove(groupToDeleteName);
        }
        else
        {
            testGroup = await GroupAdminCanAddGroup(userName, null, groupToDeleteName);
        }

        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    delete (groupId: $group) {
                      id name
                    }
                  }
                }
            ";
        var variables = new { group = testGroup.Id };

        var json = await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));
        var resGroups = JsonConvert.DeserializeObject<List<Group>>(json["data"]!["group"]!["delete"]!.ToString());

        var mandatorGroups = TestGroups.ByMandator(BritishMandatorName).Values.ByArchived(false).ToList();

        Assert.NotNull(resGroups);
        Assert.Equal(mandatorGroups.Count, resGroups!.Count);
        Assert.Collection(resGroups.OrderBy(rg => rg.Id),
                          mandatorGroups.OrderBy(tg => tg.Id)
                              .Select(tg => (Action<Group>)(rg => Assert.Equal(tg.Id, rg.Id)))
                              .ToArray());

        Assert.DoesNotContain(await GetGroups(userName), g => g.Id == testGroup.Id);
    }


    [Theory]
    [InlineData(dracoMalfoy, slytherin)]
    public async Task NormalUserCannotDeleteGroup(string userName, string groupToDeleteName)
    {
        var normalUser = TestUser[userName];
        var testgroup = TestGroups[groupToDeleteName];

        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    delete (groupId: $group) {
                      id name
                    }
                  }
                }
            ";
        var variables = new { group = testgroup.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        var groups = await GetGroups(userName);
        Assert.Contains(groups, g => g.Id == testgroup.Id);
    }

    #endregion

    #region GroupTree

    [Theory]
    [InlineData(dumbledore, null)]
    //[InlineData(dumbledore, null, true)] // TODO ?? GroupTree cannot show archived Subgroups of unarchived root/parent-groups
    [InlineData(dumbledore, null, false)]
    [InlineData(dumbledore, GroupRole.GroupViewer)]
    [InlineData(dumbledore, GroupRole.GroupAdmin)]
    public async Task AdminCanQueryAllGroupsViaGroupTree(string userName, GroupRole? role, bool? archived = null)
    {
        var groups = await GroupsByGroupTree(userName, role, archived);
        var expectedGroups = TestGroups.ByMandator(BritishMandatorName).Values.ByArchived(archived).ToList();

        Assert.Equal(expectedGroups.Count, groups.Count);
        foreach (var tg in expectedGroups)
        {
            Assert.Contains(groups, g => g.Id == tg.Id);
        }

        foreach (var tg in groups)
        {
            Assert.Contains(expectedGroups, g => g.Id == tg.Id);
        }
    }

    [Theory]
    [InlineData(minervaMcGonagall, GroupRole.GroupAdmin, gryffindor)]
    [InlineData(minervaMcGonagall, GroupRole.GroupEditor, hogwarts, gryffindor, hufflepuff, ravenclaw, slytherin)]
    [InlineData(minervaMcGonagall, null, hogwarts, gryffindor, hufflepuff, ravenclaw, slytherin)]
    [InlineData(harryPotter, GroupRole.GroupAdmin)]
    [InlineData(harryPotter, GroupRole.GroupEditor, gryffindor)]
    [InlineData(harryPotter, null, gryffindor)]
    public async Task NonAdminsCanOnlySeeTheirRespectiveGroupsViaGroupTree(string userName, GroupRole? role,
        params string[] groupNames)
    {
        var groups = await GroupsByGroupTree(userName, role, false);
        var expectedGroups = TestGroups.ByMandator(BritishMandatorName).Values
            .ByArchived(false)
            .Where(g => groupNames.Contains(g.Name)).ToList();

        Assert.Equal(expectedGroups.Count, groups.Count);
        foreach (var tg in expectedGroups)
        {
            Assert.Contains(groups, g => g.Id == tg.Id);
        }

        foreach (var tg in groups)
        {
            Assert.Contains(expectedGroups, g => g.Id == tg.Id);
        }
    }

    [Fact]
    public async Task GroupTreeRespectsByArchivedFilter()
    {
        const string username = dumbledore;
        var user = TestUser[dumbledore];

        var groupLevel1 = await groupTestService.CreateGroup(username, "level_1");
        var groupLevel2 = await groupTestService.CreateGroup(username, "level_2", groupLevel1.Id);
        var groupLevel3 = await groupTestService.CreateGroup(username, "level_3", groupLevel2.Id);
        await groupTestService.ArchiveGroup(user, groupLevel3.Id);

        var allGroups = await GroupsByGroupTree(dumbledore, null, null);
        var allGroupsNames = allGroups.Select(g => g.Name).ToList();
        Assert.Contains(allGroupsNames, name => string.Equals(name, "level_1", StringComparison.InvariantCulture));
        Assert.Contains(allGroupsNames, name => string.Equals(name, "level_2", StringComparison.InvariantCulture));
        Assert.Contains(allGroupsNames, name => string.Equals(name, "level_3", StringComparison.InvariantCulture));

        var activeGroups = await GroupsByGroupTree(dumbledore, null, false);
        var activeGroupsName = activeGroups.Select(g => g.Name).ToList();
        Assert.Contains(activeGroupsName, name => string.Equals(name, "level_1", StringComparison.InvariantCulture));
        Assert.Contains(activeGroupsName, name => string.Equals(name, "level_2", StringComparison.InvariantCulture));
        Assert.DoesNotContain(activeGroupsName, name => string.Equals(name, "level_3", StringComparison.InvariantCulture));

        var archivedGroups = await GroupsByGroupTree(dumbledore, null, true);
        var archivedGroupsNames = archivedGroups.Select(g => g.Name).ToList();
        Assert.DoesNotContain(archivedGroupsNames, name => string.Equals(name, "level_1", StringComparison.InvariantCulture));
        Assert.DoesNotContain(archivedGroupsNames, name => string.Equals(name, "level_2", StringComparison.InvariantCulture));
        // The archived group will not be returned because the parent is not archived
        Assert.DoesNotContain(archivedGroupsNames, name => string.Equals(name, "level_3", StringComparison.InvariantCulture));

        // After archiving the parents till root all these groups should now be returned
        await groupTestService.ArchiveGroup(user, groupLevel2.Id);
        await groupTestService.ArchiveGroup(user, groupLevel1.Id);
        var archivedGroups2 = await GroupsByGroupTree(dumbledore, null, true);
        var archivedGroupsNames2 = archivedGroups2.Select(g => g.Name).ToList();
        Assert.Contains(archivedGroupsNames2, name => string.Equals(name, "level_1", StringComparison.InvariantCulture));
        Assert.Contains(archivedGroupsNames2, name => string.Equals(name, "level_2", StringComparison.InvariantCulture));
        Assert.Contains(archivedGroupsNames2, name => string.Equals(name, "level_3", StringComparison.InvariantCulture));
    }

    #endregion

    #region Helper

    private async Task<List<Group>> GetGroups(string userName, bool? byArchived = false)
    {
        var user = TestUser[userName];
        const string query = @"
                query($byArchived: Boolean = false) {
                  groups(byArchived: $byArchived) {
                    id name archivedAt
                  }
                }
               ";
        var variables = new { byArchived };
        var json = await MakeGraphQLRequestAsync(user.Email, query,
                                                 JsonConvert.SerializeObject(variables));

        var returnedGroups = JsonConvert.DeserializeObject<List<Group>>(json["data"]!["groups"]!.ToString());
        return returnedGroups;
    }

    private async Task<List<Group>> GroupsByGroupTree(string userName, GroupRole? role, bool? archived = null)
    {
        var user = TestUser[userName];

        const string query = @"
                query($withRole: GroupRole, $byArchived: Boolean = false) {
                    groupTree(withRole: $withRole, byArchived: $byArchived)
                }";

        var variables = new { withRole = role, byArchived = archived };

        var jsonOptions = new JsonSerializerSettings { Converters = { new StringEnumConverter(typeof(SnakeCaseNamingStrategy)) } };
        var result = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables, jsonOptions));

        var jsonStr = result["data"]!["groupTree"]!.ToString();
        jsonStr = jsonStr.Replace("Children", "ChildGroups");

        var groups = JsonConvert.DeserializeObject<List<Group>>(jsonStr);

        return Flatten(groups);

        List<Group> Flatten(List<Group> groupsToFlatten)
        {
            return groupsToFlatten
                .Concat(groupsToFlatten.SelectMany(g => Flatten(g.ChildGroups.ToList())))
                .ToList();
        }
    }

    #endregion
}