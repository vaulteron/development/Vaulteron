﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

 namespace VaulteronTests.integrationTests.GroupRepository; 

 public class GroupContent : HarryPotterTestUniverse
 {
     private readonly PasswordTestService passwordTestService;

     public GroupContent(TestWebApplicationFactory factory, ITestOutputHelper logger,
         HarryPotterTestUniverse hptu = null)
         : base(factory, logger, hptu)
     {
         passwordTestService = new PasswordTestService(logger, this);
     }

     [Theory]
     [InlineData(hogwarts)]
     [InlineData(gryffindor)]
     [InlineData(hufflepuff)]
     [InlineData(ravenclaw)]
     [InlineData(slytherin)]
     [InlineData(durmstrang)]
     private async Task CheckContentOfEachGroupInHPTU(string groupName)
     {
         var group = TestGroups[groupName];
         const string callingUserName = dumbledore;
         var callingUser = TestUser[dumbledore];

         var allSharedPasswords =
             await passwordTestService.GetAllSharedPasswordsForGroupAsync(callingUserName, groupName, null);
         var allSharedTags = await LoadAllSharedTags(callingUser, group);
         var allUsersInThisGroup = await LoadAllUserInGroup(callingUser, group);
         var allSubGroups = await LoadAllSubGroups(callingUser, group);

         #region sharedPasswords

         var expectedSharedPasswords = group.SharedPasswords;
         Assert.Equal(expectedSharedPasswords.Count, allSharedPasswords.Count);
         foreach (var password in allSharedPasswords)
         {
             var foundMatch = expectedSharedPasswords.SingleOrDefault(ePassword => ePassword.Id == password.Id);
             if (foundMatch == null)
                 throw new XunitException($"This password is not in the list of expected passwords: {password}");

             PasswordTestService.ComparePasswordsMetaData(foundMatch, password);
         }

         #endregion

         #region sharedTags

         var expectedTags = group.Tags;
         Assert.Equal(expectedTags.Count, allSharedTags.Count);
         foreach (var tag in allSharedTags)
         {
             var foundMatch = expectedTags.SingleOrDefault(eTag => eTag.Id == tag.Id);
             if (foundMatch == null)
                 throw new XunitException($"This tag is not in the list of expected passwords: {tag}");

             Assert.Equal(foundMatch.Name, tag.Name);
             Assert.Equal(foundMatch.Color, tag.Color);
         }

         #endregion

         #region users in group

         var expectedUsers = group.UserGroups.Select(ug => ug.User).ToList();
         expectedUsers.Add(TestUser[dumbledore]);
         expectedUsers.Add(TestUser[doloresUmbridge]);

         Assert.Equal(expectedUsers.Count, allUsersInThisGroup.Count);
         foreach (var user in allUsersInThisGroup)
         {
             var foundMatch = expectedUsers.SingleOrDefault(eUser => eUser.Id == user.Id);
             if (foundMatch == null)
                 throw new XunitException($"This user is not in the list of expected passwords: {user}");

             Assert.Equal(foundMatch.Email, user.Email);
             Assert.Equal(foundMatch.Firstname, user.Firstname);
             Assert.Equal(foundMatch.Lastname, user.Lastname);
         }

         #endregion

         #region users in group

         var expectedSubGroups = group.ChildGroups;
         Assert.Equal(expectedSubGroups.Count, allSubGroups.Count);
         foreach (var subGroup in allSubGroups)
         {
             var foundMatch = expectedSubGroups.SingleOrDefault(eSubGroup => eSubGroup.Id == subGroup.Id);
             if (foundMatch == null)
                 throw new XunitException($"This subGroup is not in the list of expected passwords: {subGroup}");

             Assert.Equal(foundMatch.Name, subGroup.Name);
         }

         #endregion
     }

     #region helper

     private async Task<List<SharedTag>> LoadAllSharedTags(User callingUser, Group group)
     {
         const string query = @"
                query SharedTagQuery($searchTerm: String!, $groupId: Guid!) {
                    sharedTags(searchTerm: $searchTerm, groupId: $groupId) {
                        id name color
                    }
                }
            ";
         var variables = new { groupId = group.Id, searchTerm = "" };

         var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
         var foundTags = JsonConvert.DeserializeObject<List<SharedTag>>(json["data"]!["sharedTags"]!.ToString());
         return foundTags;
     }

     private async Task<List<User>> LoadAllUserInGroup(User callingUser, Group group)
     {
         const string query = @"
                query UsersQueryInGroup($searchTerm: String!, $groupIds: [Guid]!) {
                    users(searchTerm: $searchTerm, groupIds: $groupIds) {
                        id lastname firstname email
                    }
                }
            ";
         var variables = new { groupIds = new[] { group.Id }, searchTerm = "" };

         var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
         var foundUsers = JsonConvert.DeserializeObject<List<User>>(json["data"]!["users"]!.ToString());
         return foundUsers;
     }

     private async Task<List<Group>> LoadAllSubGroups(User callingUser, Group group)
     {
         const string query = @"
                query GroupQuery($parentId: Guid, $searchTerm: String) {
                    groups: groupsByParent(parentId: $parentId, searchTerm: $searchTerm) {
                        id name countPasswords countSubGroups countUsers countTags
                    }
                }
            ";
         var variables = new { parentId = group.Id, searchTerm = "" };

         var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
         var foundSubGroups = JsonConvert.DeserializeObject<List<Group>>(json["data"]!["groups"]!.ToString());
         return foundSubGroups;
     }

     #endregion
 }