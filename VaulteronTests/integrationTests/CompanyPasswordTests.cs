﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class CompanyPasswordTests : HarryPotterTestUniverse
{
    private readonly Dictionary<string, SharedPassword> testPasswords = new();
    private readonly PasswordTestService passwordTestService;

    private const string
        passwordHarryPotterForEdits = "passwordHarryPotterForEdits",
        passwordHarryPotterForDeletion = "passwordHarryPotterForDeletion",
        passwordRonWeaslyOne = "passwordRonWeaslyOne",
        passwordRonWeaslyTwo = "passwordRonWeaslyTwo",
        passwordDracoMalfoy = "passwordDracoMalfoy";

    private const string COMPANY_PASSWORD_STRING = "unencrypted";

    public CompanyPasswordTests(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);

        testPasswords[passwordHarryPotterForEdits] =
            passwordTestService.AddCompanyPasswordAsync(harryPotter, passwordHarryPotterForEdits, COMPANY_PASSWORD_STRING).Result;

        testPasswords[passwordHarryPotterForDeletion] =
            passwordTestService.AddCompanyPasswordAsync(harryPotter, passwordHarryPotterForDeletion, COMPANY_PASSWORD_STRING).Result;

        #region for testing just more

        testPasswords[passwordRonWeaslyOne] = passwordTestService.AddCompanyPasswordAsync(ronWeasly, passwordRonWeaslyOne, COMPANY_PASSWORD_STRING).Result;
        testPasswords[passwordRonWeaslyTwo] = passwordTestService.AddCompanyPasswordAsync(ronWeasly, passwordRonWeaslyTwo, COMPANY_PASSWORD_STRING).Result;
        testPasswords[passwordDracoMalfoy] = passwordTestService.AddCompanyPasswordAsync(dracoMalfoy, passwordDracoMalfoy, COMPANY_PASSWORD_STRING).Result;

        #endregion
    }

    #region access

    [Theory]
    [InlineData(harryPotter, passwordHarryPotterForEdits, passwordHarryPotterForDeletion, companyPassword + harryPotter + one, companyPassword + harryPotter + one + archived)]
    [InlineData(ronWeasly, passwordRonWeaslyOne, passwordRonWeaslyTwo, companyPassword + ronWeasly + one)]
    [InlineData(dracoMalfoy, passwordDracoMalfoy)]
    public async Task CanGetOwnCompanyPasswords(string username, params string[] passwords)
    {
        await FetchCompanyPasswordsAndCompareWithExpectedAsync(username, passwords, null);
    }

    [Theory]
    [InlineData(harryPotter, offlinePassword + harryPotter + one)]
    [InlineData(lunaLovegood, offlinePassword + lunaLovegood + one)]
    public async Task CanGetOwnOfflinePasswords(string username, params string[] passwords)
    {
        await FetchOfflinePasswordsAndCompareExpectedAsync(username, passwords);
    }

    [Theory]
    [InlineData(harryPotter, passwordHarryPotterForEdits, passwordHarryPotterForDeletion, companyPassword + harryPotter + one)]
    [InlineData(ronWeasly, passwordRonWeaslyOne, passwordRonWeaslyTwo, companyPassword + ronWeasly + one)]
    [InlineData(dracoMalfoy, passwordDracoMalfoy)]
    public async Task CompanyPasswordsDontShowUpInSharedPasswords(string username, params string[] passwords)
    {
        await FetchSharedPasswordsWhichShouldNotBeFoundAsync(username, passwords);
    }

    [Theory]
    [InlineData(harryPotter, passwordRonWeaslyOne, passwordRonWeaslyTwo, companyPassword + ronWeasly + one, companyPassword + dumbledore + one, companyPassword + dumbledore + two)]
    public async Task CompanyPasswordsDontShowUpInSharedPasswordsFromOtherUser(string username, params string[] passwords)
    {
        await FetchSharedPasswordsWhichShouldNotBeFoundAsync(username, passwords);
    }

    [Theory]
    [InlineData(dumbledore, offlinePassword + dumbledore + one, offlinePassword + dumbledore + two)]
    [InlineData(harryPotter, offlinePassword + harryPotter + one)]
    [InlineData(lunaLovegood, offlinePassword + lunaLovegood + one)]
    public async Task OfflinePasswordsDontShowUpInSharedPasswords(string username, params string[] passwords)
    {
        await FetchSharedPasswordsWhichShouldNotBeFoundAsync(username, passwords);
    }

    [Fact]
    public async Task AdminHasAccessToAllCompanyPasswords()
    {
        const string username = dumbledore;
        var passwords = new[]
        {
            companyPassword + dumbledore + one, companyPassword + dumbledore + one + archived, companyPassword + dumbledore + two,
            passwordHarryPotterForEdits, passwordHarryPotterForDeletion, companyPassword + harryPotter + one, companyPassword + harryPotter + one + archived,
            passwordRonWeaslyOne, passwordRonWeaslyTwo, companyPassword + ronWeasly + one,
            passwordDracoMalfoy,
            companyPassword + lunaLovegood + archived
        };
        await FetchCompanyPasswordsAndCompareWithExpectedAsync(username, passwords, null);
    }

    [Theory]
    [InlineData(dumbledore, offlinePassword + dumbledore + one, offlinePassword + dumbledore + two, offlinePassword + harryPotter + one, offlinePassword + lunaLovegood + one)]
    public async Task AdminHasAccessToAllOfflinePasswords(string username, params string[] passwords)
    {
        await FetchOfflinePasswordsAndCompareExpectedAsync(username, passwords);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore, companyPassword + dumbledore + one, companyPassword + dumbledore + two, companyPassword + dumbledore + one + archived)]
    [InlineData(dumbledore, harryPotter, passwordHarryPotterForEdits, passwordHarryPotterForDeletion, companyPassword + harryPotter + one, companyPassword + harryPotter + one + archived)]
    [InlineData(dumbledore, dracoMalfoy, passwordDracoMalfoy)]
    public async Task AdminHasAccessToCompanyPasswordsOfUser(string username, string ofUserName, params string[] passwords)
    {
        const string query = $@"query ($createdByIds: [Guid]) {{
                companyPasswords (createdByIds: $createdByIds) {{ {SHARED_PASSWORD_FRAGMENT} }}
            }}";
        var ofUser = TestUser[ofUserName];
        var variables = new { createdByIds = new[] { ofUser.Id } };
        await FetchPasswordsAndCompareExpectedAsync(username, passwords, query, "companyPasswords", variables);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore, offlinePassword + dumbledore + one, offlinePassword + dumbledore + two)]
    [InlineData(dumbledore, harryPotter, offlinePassword + harryPotter + one)]
    [InlineData(dumbledore, lunaLovegood, offlinePassword + lunaLovegood + one)]
    public async Task AdminHasAccessToOfflinePasswordsOfUser(string username, string ofUserName, params string[] passwords)
    {
        const string query = $@"query ($createdByIds: [Guid]){{
                offlinePasswords (createdByIds: $createdByIds) {{ {SHARED_PASSWORD_FRAGMENT} }}
            }}";
        var ofUser = TestUser[ofUserName];
        var variables = new { createdByIds = new[] { ofUser.Id } };
        await FetchPasswordsAndCompareExpectedAsync(username, passwords, query, "offlinePasswords", variables);
    }

    #endregion

    #region add

    // Normal Add already "tested" within Constructor

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    [InlineData(ronWeasly)]
    public async Task UserCanAddOfflinePassword(string username)
    {
        var user = TestUser[username];
        const string newPasswordName = "test-name";

        var keyPairs = TestUser
            .ByMandator(user.Mandator.Name)
            .Where(u => u.Key == username || u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());

        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("doesntMatter", pk.PublicKeyString)
            });

        const string query = $@"
                mutation AddOfflinePassword ($offlinePassword: AddOfflinePasswordInputType!) {{
                    sharedPassword {{
                        offlineAdd (password: $offlinePassword) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}";

        var variables = new
        {
            offlinePassword = new
            {
                name = newPasswordName,
                login = newPasswordName,
                websiteUrl = newPasswordName,
                notes = newPasswordName,
                encryptedPasswords
            }
        };

        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));

        var createdPassword = JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["offlineAdd"]!.ToString());
        var expectedPassword = new SharedPassword
        {
            Name = variables.offlinePassword.name,
            Login = variables.offlinePassword.login,
            WebsiteURL = variables.offlinePassword.websiteUrl,
            Notes = variables.offlinePassword.notes,
            IsSavedAsOfflinePassword = true
        };
        PasswordTestService.ComparePasswordsMetaData(expectedPassword, createdPassword, DateTime.UtcNow);

        // Check if user sees admin added password
        await FetchOfflinePasswordsAndCompareExpectedAsync(username, expectedPassword);
    }

    [Theory]
    [InlineData(dumbledore, hermioneGranger, "aNewPassword")]
    [InlineData(dumbledore, severusSnape, "anotherNewPassword")]
    [InlineData(dumbledore, harryPotter, "yetAnotherNewPassword")]
    public async Task AdminCanAddCompanyPasswordForUser(string admin, string forUserUserName, string newPasswordName)
    {
        var user = TestUser[admin];
        var forUser = TestUser[forUserUserName];

        var keyPairs = TestUser.ByMandator(user.Mandator.Name)
            .Where(u => u.Key == forUserUserName || u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());

        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("doesntMatter", pk.PublicKeyString)
            });

        const string query = $@"
               mutation AddSharedPassword($companyPassword: AddCompanyPasswordInputType!, $forUserId: Guid!) {{
                    sharedPassword {{
                        companyAddForUser(password: $companyPassword, forUserId: $forUserId) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                name = newPasswordName,
                login = "login for " + newPasswordName,
                websiteUrl = "www.vaulteron.com",
                notes = "some notes",
                encryptedPasswords = encryptedPasswords
            },
            forUserId = forUser.Id
        };

        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));

        var createdPassword = JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["companyAddForUser"]!.ToString());
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.companyPassword.name,
            Login = variables.companyPassword.login,
            WebsiteURL = variables.companyPassword.websiteUrl,
            Notes = variables.companyPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        // Check if User sees admin added PW

        const string getQuery = $@"query {{ companyPasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}";

        json = await MakeGraphQLRequestAsync(forUser.Email, getQuery);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["companyPasswords"]!.ToString());
        Assert.NotNull(foundPasswords);
        Assert.Contains(foundPasswords, sp => sp.Name == newPasswordName);
        var t = foundPasswords!.Single(sp => sp.Name == newPasswordName);
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.companyPassword.name,
            Login = variables.companyPassword.login,
            WebsiteURL = variables.companyPassword.websiteUrl,
            Notes = variables.companyPassword.notes,
        }, t);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminHasToIncludeEncryptionsForTargetUserWhenAddingCompanyPassword(string admin, string forUserName)
    {
        var keyPairs = TestUser
            .Where(u => u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());
        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("doesntMatter", pk.PublicKeyString)
            });

        var user = TestUser[admin];
        var forUser = TestUser[forUserName];

        const string query = $@"
               mutation AddCompanyPassword($companyPassword: AddCompanyPasswordInputType!, $forUserId: Guid!) {{
                    sharedPassword {{
                        companyAddForUser(password: $companyPassword, forUserId: $forUserId) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                name = "doesNotMatter",
                login = "login doesnt Matter too",
                websiteUrl = "www.vaulteron.com",
                notes = "Nothing else Matters",
                encryptedPasswords = encryptedPasswords
            },
            forUserId = forUser.Id
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(harryPotter)]
    public async Task UserHasToProvideAllAdminsPKs(string userName)
    {
        var keyPairs = TestUser
            .Where(u => u.Key == userName)
            .Select(u => u.Value.KeyPairs.LastActive());
        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("doesntMatter", pk.PublicKeyString)
            });

        var user = TestUser[userName];

        const string query = $@"
               mutation AddSharedPassword($companyPassword: AddCompanyPasswordInputType!) {{
                    sharedPassword {{
                        companyAdd(password: $companyPassword) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                name = "doesNotMatter",
                login = "login doesnt Matter too",
                websiteUrl = "www.vaulteron.com",
                notes = "Nothing else Matters",
                encryptedPasswords = encryptedPasswords
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(harryPotter, hermioneGranger, "aNewPassword")]
    [InlineData(severusSnape, dracoMalfoy, "anotherNewPassword")]
    public async Task UserCanNotAddForUser(string admin, string forUserName, string newPasswordName)
    {
        var keyPairs = TestUser
            .Where(u => u.Key == forUserName || u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());
        await AddCompanyPasswordForUser(admin, forUserName, newPasswordName, keyPairs);
    }
    private async Task AddCompanyPasswordForUser(string admin, string forUserName, string newPasswordName, IEnumerable<KeyPair> keyPairs)
    {

        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(newPasswordName, pk.PublicKeyString)
            });

        var user = TestUser[admin];
        var forUser = TestUser[forUserName];

        const string query = $@"
               mutation AddSharedPassword($companyPassword: AddCompanyPasswordInputType!, $forUserId: Guid!) {{
                    sharedPassword {{
                        companyAddForUser(password: $companyPassword, forUserId: $forUserId) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                name = newPasswordName,
                login = "login for " + newPasswordName,
                websiteUrl = "www.vaulteron.com",
                notes = "some notes",
                encryptedPasswords = encryptedPasswords
            },
            forUserId = forUser.Id
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    #endregion

    #region edit

    [Theory]
    [InlineData(harryPotter, passwordHarryPotterForEdits)]
    public async Task UserCanEdit(string userName, string password)
    {
        await AdminCanEditForUser(userName, userName, password);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter, passwordHarryPotterForEdits)]
    public async Task AdminCanEditForUser(string adminName, string forUserName, string password)
    {
        var admin = TestUser[adminName];
        var user = TestUser[forUserName];
        var pwToEdit = testPasswords[password];

        const string query = $@"
               mutation EditSharedPassword($companyPassword: EditSharedPasswordInputType!) {{
                    sharedPassword {{
                        edit(password: $companyPassword) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                id = pwToEdit.Id,
                name = "changed",
                login = "changed login",
                websiteURL = "changed.vaulteron.com",
                notes = "some changed notes",
                modifyLock = pwToEdit.ModifyLock,
            },
        };

        var json = await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));

        var createdPassword =
            JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["edit"]!.ToString());
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.companyPassword.name,
            Login = variables.companyPassword.login,
            WebsiteURL = variables.companyPassword.websiteURL,
            Notes = variables.companyPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        // Check if User sees changed on Query

        const string getQuery = $@"query {{ companyPasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}";

        json = await MakeGraphQLRequestAsync(user.Email, getQuery);
        var foundPasswords =
            JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["companyPasswords"]!.ToString());
        Assert.Contains(foundPasswords, sp => sp.Id == pwToEdit.Id);
        var t = foundPasswords.Single(sp => sp.Id == pwToEdit.Id);
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.companyPassword.name,
            Login = variables.companyPassword.login,
            WebsiteURL = variables.companyPassword.websiteURL,
            Notes = variables.companyPassword.notes,
        }, t);
    }

    [Theory]
    [InlineData(dracoMalfoy, harryPotter, passwordHarryPotterForEdits)]
    public async Task UserCanNotEditForUser(string userName, string forUserName, string password)
    {
        var otherUser = TestUser[userName];
        var user = TestUser[forUserName];
        var pwToEdit = testPasswords[password];

        const string query = $@"
               mutation EditSharedPassword($companyPassword: EditSharedPasswordInputType!) {{
                    sharedPassword {{
                        edit(password: $companyPassword) {{
                            {SHARED_PASSWORD_FRAGMENT}
                        }}
                    }}
                }}
               ";

        var variables = new
        {
            companyPassword = new
            {
                id = pwToEdit.Id,
                name = "changed",
                login = "changed login",
                websiteURL = "changed.vaulteron.com",
                notes = "some changed notes",
                modifyLock = pwToEdit.ModifyLock,
            },
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(otherUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        // Check if User sees changed on Query, he shouldn't

        const string getQuery = $@"query {{ companyPasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}";

        var json = await MakeGraphQLRequestAsync(user.Email, getQuery);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["companyPasswords"]!.ToString());
        Assert.Contains(foundPasswords, sp => sp.Id == pwToEdit.Id);
        var t = foundPasswords.Single(sp => sp.Id == pwToEdit.Id);
        PasswordTestService.ComparePasswordsMetaData(pwToEdit, t);
    }

    #endregion

    #region delete

    [Theory]
    [InlineData(dumbledore, harryPotter, passwordHarryPotterForDeletion)] // Admin
    [InlineData(dumbledore, harryPotter, offlinePassword + harryPotter + one)] // Admin offlinePassword
    [InlineData(harryPotter, harryPotter, passwordHarryPotterForDeletion)] // User
    [InlineData(harryPotter, harryPotter, offlinePassword + harryPotter + one)] // User offlinePassword
    public async Task UserAndAdminCanDeletePasswordForUser(string adminName, string username, string passwordName)
    {
        var admin = TestUser[adminName];
        var user = TestUser[username];
        var passwordToDelete = FindPasswordInLocalDictionaryOrDatabaseByName(passwordName);

        var deletedPassword = await passwordTestService.DeleteSharedPasswordAsync(admin, passwordToDelete.Id);
        PasswordTestService.ComparePasswordsMetaData(passwordToDelete, deletedPassword);

        if (passwordName.Contains(offlinePassword))
        {
            var exception = await Assert.ThrowsAsync<ApiClientException>(() => passwordTestService.GetSingleSharedPasswordStringAsync(admin, passwordToDelete.Id));
            Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
        }
        else
        {
            var foundPasswords = await passwordTestService.DeleteCompanyPassword(user, passwordToDelete.Id);
            Assert.Equal(passwordToDelete, foundPasswords);
        }
    }

    [Theory]
    [InlineData(ronWeasly, harryPotter, passwordHarryPotterForDeletion)]
    public async Task UserCanNotDeletePasswordForOtherUser(string userName, string otherUserName, string password)
    {
        var admin = TestUser[userName];
        var otherUser = TestUser[otherUserName];
        var pwToDel = testPasswords[password];
        var query = $@"mutation ($id: Guid!) {{ sharedPassword {{
                delete (id: $id) {{ {SHARED_PASSWORD_FRAGMENT} }}
            }} }}";
        var variables = new { id = pwToDel.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        query = $@"query {{ companyPasswords {{ {SHARED_PASSWORD_FRAGMENT}}} }}";

        var json = await MakeGraphQLRequestAsync(otherUser.Email, query);

        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["companyPasswords"]!.ToString());
        Assert.Contains(foundPasswords, p => p.Id == pwToDel.Id);
    }

    #endregion

    #region encrypted password

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(severusSnape, dumbledore)]
    [InlineData(minervaMcGonagall, dumbledore)]
    [InlineData(godricGryffindor, dumbledore)]
    [InlineData(harryPotter, dumbledore)]
    [InlineData(ronWeasly, dumbledore)]
    private async Task SuccessfullyAddAndEditEncryptedPasswordForOtherUserAsAdmin(string targetUsername, string adminUsername)
    {
        var targetUser = TestUser[targetUsername];
        var admin = TestUser[adminUsername];

        var newPassword = await passwordTestService.AddCompanyPasswordAsync(targetUsername, "pw-name", "pw-string");

        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(targetUser,
                                                                                   newPassword.Id,
                                                                                   new List<User> { admin },
                                                                                   COMPANY_PASSWORD_STRING,
                                                                                   true,
                                                                                   false);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(targetUser,
                                                                                   newPassword.Id,
                                                                                   new List<User> { admin },
                                                                                   COMPANY_PASSWORD_STRING,
                                                                                   false,
                                                                                   false);
    }

    #endregion

    #region helper

    private const string SHARED_PASSWORD_FRAGMENT = @"id name login securityRating websiteURL notes updatedAt createdAt isSavedAsOfflinePassword tags { id name color }";

    private SharedPassword FindPasswordInLocalDictionaryOrDatabaseByName(string passwordName) =>
        testPasswords.ContainsKey(passwordName)
            ? testPasswords[passwordName]
            : DbContext.SharedPasswords.Single(p => p.Name == passwordName);

    private async Task FetchCompanyPasswordsAndCompareWithExpectedAsync(string username, IReadOnlyCollection<string> passwords, bool? byArchived)
    {
        var parameters = "";
        if (byArchived is not null)
        {
            var booleanValueAsGraphqlString = byArchived.Value ? "true" : "false";
            parameters = $"(byArchived: {booleanValueAsGraphqlString})";
        }
        await FetchPasswordsAndCompareExpectedAsync(username, passwords, $@"query {{ companyPasswords {parameters} {{ {SHARED_PASSWORD_FRAGMENT} }} }}", "companyPasswords");
    }

    private async Task FetchSharedPasswordsWhichShouldNotBeFoundAsync(string username, IReadOnlyCollection<string> passwords) =>
        await FetchPasswordsWhichShouldNotBeFoundAsync(username, passwords, $@"query {{ sharedPasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}", "sharedPasswords");

    private async Task FetchOfflinePasswordsAndCompareExpectedAsync(string username, IReadOnlyCollection<string> passwords) =>
        await FetchPasswordsAndCompareExpectedAsync(username, passwords, $@"query {{ offlinePasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}", "offlinePasswords");

    private async Task FetchOfflinePasswordsAndCompareExpectedAsync(string username, SharedPassword expectedPassword) =>
        await FetchPasswordsAndCompareExpectedAsync(username, expectedPassword, $@"query {{ offlinePasswords {{ {SHARED_PASSWORD_FRAGMENT} }} }}", "offlinePasswords");

    private async Task FetchPasswordsAndCompareExpectedAsync(string username, SharedPassword expectedPassword, string query, string queryResultDataName)
    {
        var user = TestUser[username];

        var json = await MakeGraphQLRequestAsync(user.Email, query);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]![queryResultDataName]!.ToString());

        Assert.NotNull(foundPasswords);
        Assert.Contains(foundPasswords, sp => sp.Name == expectedPassword.Name);
        var actualPassword = foundPasswords!.Single(sp => sp.Name == expectedPassword.Name);
        PasswordTestService.ComparePasswordsMetaData(expectedPassword, actualPassword);
    }

    private async Task FetchPasswordsAndCompareExpectedAsync(string username, IReadOnlyCollection<string> passwordNames, string query, string queryResultDataName)
    {
        var user = TestUser[username];

        var json = await MakeGraphQLRequestAsync(user.Email, query);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]![queryResultDataName]!.ToString());

        Assert.NotNull(foundPasswords);
        Assert.Equal(passwordNames.Count, foundPasswords!.Count);
        foreach (var passwordName in passwordNames)
        {
            Assert.Contains(foundPasswords, sp => sp.Name == passwordName);
            var actualPassword = foundPasswords.Single(sp => sp.Name == passwordName);
            var expectedPassword = FindPasswordInLocalDictionaryOrDatabaseByName(passwordName);
            PasswordTestService.ComparePasswordsMetaData(expectedPassword, actualPassword);
        }
    }

    private async Task FetchPasswordsAndCompareExpectedAsync(string username, IReadOnlyCollection<string> passwordNames, string query, string queryResultDataName, object variables)
    {
        var user = TestUser[username];

        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]![queryResultDataName]!.ToString());

        Assert.NotNull(foundPasswords);
        Assert.Equal(passwordNames.Count, foundPasswords!.Count);
        foreach (var passwordName in passwordNames)
        {
            Assert.Contains(foundPasswords, sp => sp.Name == passwordName);
            var actualPassword = foundPasswords!.Single(sp => sp.Name == passwordName);
            var expectedPassword = FindPasswordInLocalDictionaryOrDatabaseByName(passwordName);
            PasswordTestService.ComparePasswordsMetaData(expectedPassword, actualPassword);
        }
    }

    private async Task FetchPasswordsWhichShouldNotBeFoundAsync(string username, IReadOnlyCollection<string> passwordNames, string query, string queryResultDataName)
    {
        var user = TestUser[username];

        var json = await MakeGraphQLRequestAsync(user.Email, query);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]![queryResultDataName]!.ToString());

        Assert.NotNull(foundPasswords);
        foreach (var passwordName in passwordNames)
        {
            Assert.DoesNotContain(foundPasswords, sp => sp.Name == passwordName);
        }
    }

    #endregion
}