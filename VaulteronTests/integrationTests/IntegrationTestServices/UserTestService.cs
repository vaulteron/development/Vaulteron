﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Models.EditModels;
using VaulteronWebserver.ViewModels.Users;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests.IntegrationTestServices;

public class UserTestService
{
    private readonly HarryPotterTestUniverse hptu;
    private readonly PasswordTestService passwordTestService;

    public UserTestService(ITestOutputHelper logger, HarryPotterTestUniverse hptu)
    {
        this.hptu = hptu;

        passwordTestService = new PasswordTestService(logger, hptu);
    }

    public async Task<User> GrantAdminPermissions(User callingUser, User targetUser, IEnumerable<object> encryptedSharedPasswords = null)
    {
        if (encryptedSharedPasswords == null)
        {
            var userGroupsExpected = callingUser.Admin
                ? hptu.DbContext.Groups.ByMandatorId(callingUser.MandatorId).ToList()
                : callingUser.UserGroups.SelectMany(g => g.Group.ChildGroups.Append(g.Group)).ToList();
            var encryptedSharedPasswordTuples = passwordTestService.GetFakeDecryptedSharedPasswordsForGroups(callingUser, userGroupsExpected).ToList();
            var encryptedCompanyPasswordsTuples = passwordTestService.GetFakeEncryptedCompanyPasswords(callingUser, true).ToList();
            var sharedPasswordTuples = encryptedSharedPasswordTuples.Concat(encryptedCompanyPasswordsTuples);
            encryptedSharedPasswords = sharedPasswordTuples.Select(pw => new
            {
                sharedPasswordId = pw.Id,
                encryptedPasswordString = pw.encryptedString
            });
        }

        const string query = @"
                mutation GrantAdminPermissions($targetUserId: Guid!, $encryptedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!) {
                  user {
                    grantAdminPermissions(targetUserId: $targetUserId, encryptedPasswords: $encryptedPasswords) {
                      id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                      publicKey { publicKeyString }
                      groups { id }
                      usergroups { groupId }
                    }
                  }
                }
                ";
        var variables = new
        {
            targetUserId = targetUser.Id,
            encryptedPasswords = encryptedSharedPasswords
        };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var returnedUser = JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["grantAdminPermissions"]!.ToString());
        return returnedUser;
    }

    public async Task<User> RevokeAdminPermissions(User callingUser, User targetUser)
    {
        const string query = @"
                mutation RevokeAdminPermissions($targetUserId: Guid!) {
                  user {
                    revokeAdminPermissions(targetUserId: $targetUserId) {
                      "
            + ComplexUserFragment
            + @"
                    }
                  }
                }
                ";
        var variables = new { targetUserId = targetUser.Id };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        return JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["revokeAdminPermissions"]!.ToString());
    }

    public async Task<User> ChangeLoginPassword(User user, string newPasswordString)
    {
        var groups = user.Admin
            ? hptu.DbContext.Groups
                .RecursionBase(user.MandatorId)
                .Where(g => g.ParentGroupId == null)
                .AsEnumerable()
            : hptu.DbContext.Groups
                .RecursionBase(user.MandatorId)
                .IncludeSharedPasswords()
                .Where(g => g.UserGroups.Any(ug => ug.UserId == user.Id))
                .AsEnumerable();

        var userGroupsExpected = GroupAuthorizations.IncludeSubGroups(groups).ToList();
        var oldKeyPair = user.KeyPairs.LastActive();
        var newKeyPair = hptu.CryptoHelper.CreateKeyPair();

        var encryptedSharedPasswordTuples = passwordTestService.GetFakeDecryptedSharedPasswordsForGroups(user, userGroupsExpected).ToList();
        var encryptedCompanyPasswordsTuples = passwordTestService.GetFakeEncryptedCompanyPasswords(user, true).ToList();
        var sharedPasswordTuples = encryptedSharedPasswordTuples.Concat(encryptedCompanyPasswordsTuples);
        var encryptedSharedPasswords = sharedPasswordTuples.Select(pw => new
        {
            sharedPasswordId = pw.Id,
            encryptedPasswordString = pw.encryptedString
        });

        var encryptedAccountPasswordTuples = passwordTestService.GetEncryptedAccountPasswords(user, oldKeyPair, newKeyPair.PublicKey).ToList();
        var encryptedAccountPasswords = encryptedAccountPasswordTuples.Select(eap => new
        {
            accountPasswordId = eap.Id,
            encryptedPasswordString = eap.encryptedString
        });

        var previousPasswordHashEncryptedWithPublicKey = hptu.CryptoHelper.EncryptAsymmetrical(LoginPassword, newKeyPair.PublicKey);

        const string query = @"
                mutation ChangeOwnLoginPassword(
                        $publicKeyString: String!,
                        $encryptedPrivateKey: String!,
                        $previousPasswordHashEncryptedWithPublicKey: String!,
                        $currentPassword: String!,
                        $newPassword: String!,
                        $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!
                        $encryptedAccountPasswords: [AddEncryptedAccountPasswordWithoutPublicKeyInputType]!) {
                    user {
                        changeOwnLoginPassword(
                                publicKeyString: $publicKeyString,
                                encryptedPrivateKey: $encryptedPrivateKey,
                                previousPasswordHashEncryptedWithPublicKey: $previousPasswordHashEncryptedWithPublicKey,
                                currentPassword: $currentPassword,
                                newPassword: $newPassword,
                                encryptedSharedPasswords: $encryptedSharedPasswords,
                                encryptedAccountPasswords: $encryptedAccountPasswords) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var newUserData = new
        {
            publicKeyString = newKeyPair.PublicKey,
            encryptedPrivateKey = newKeyPair.EncryptedPrivateKey,
            previousPasswordHashEncryptedWithPublicKey,
            currentPassword = hptu.CryptoHelper.ComputePasswordHash(LoginPassword),
            newPassword = hptu.CryptoHelper.ComputePasswordHash(newPasswordString),
            encryptedSharedPasswords,
            encryptedAccountPasswords
        };
        var variables = newUserData;
        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        return JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["changeOwnLoginPassword"]!.ToString());
    }

    public async Task<User> GetMyself(User user, bool skipLogin = false) => await GetMyself(user.Email, skipLogin);

    public async Task<User> GetMyself(string userEmail, bool skipLogin = false)
    {
        const string query = @"
                query myself {
                    me { "
            + SimpleUserFragment
            + @" }
                }";

        var json = await hptu.MakeGraphQLRequestAsync(userEmail, query, "{}", skipLogin);
        var newCustomUser = JsonConvert.DeserializeObject<CustomUser>(json["data"]!["me"]!.ToString());
        return newCustomUser?.ToUser();
    }

    public async Task<List<User>> GetAdminsAsync(User callingUser)
    {
        const string query = @"
                query GetAdmins {
                    admins { "
            + ComplexUserFragment
            + @" }
                }";

        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query);
        var admins = JsonConvert.DeserializeObject<List<CustomUser>>(json["data"]!["admins"]!.ToString());
        return admins?.Select(u => u.ToUser()).ToList();
    }

    public async Task<User> ResendAccountActivationLinkAsync(User callingUser, User targetUser, string oldTempPassword = LoginPassword, bool fakeEncryption = true)
    {
        const string query = @"
                mutation ResendAccountActivationMail(
                    $targetUserId: Guid!,
                    $publicKey: String!,
                    $encryptedPrivateKey: String!,
                    $newTempLoginPassword: String!,
                    $newTempLoginPasswordHashed: String!,
                    $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]
                ) {
                    user {
                        resendAccountActivationEmail(
                            targetUserId: $targetUserId, 
                            publicKey: $publicKey,
                            encryptedPrivateKey: $encryptedPrivateKey,
                            newTempLoginPassword: $newTempLoginPassword,
                            newTempLoginPasswordHashed: $newTempLoginPasswordHashed,
                            encryptedSharedPasswords: $encryptedSharedPasswords
                        ) {"
            + SimpleUserFragment
            + @"
                        }
                    }
                }
            ";

        var keyPair = hptu.CryptoHelper.CreateKeyPair();
        const string newTempLoginPassword = LoginPassword;
        var newTempLoginPasswordHashed = hptu.CryptoHelper.ComputePasswordHash(newTempLoginPassword);
        var encryptedPrivateKey = hptu.CryptoHelper.EncryptSymmetrically(keyPair.PrivateKey, newTempLoginPasswordHashed);

        IEnumerable<SharedPassword> passwords = new List<SharedPassword>();
        if (targetUser.Admin)
        {
            passwords = hptu.DbContext.SharedPasswords
                .Include(sp => sp.EncryptedSharedPasswords)
                .ByMandatorId(targetUser.MandatorId);
        }
        else
        {
            var groups = hptu.DbContext.Groups
                .RecursionBase(targetUser.MandatorId)
                .IncludeSharedPasswords()
                .ByUserId(targetUser.Id)
                .ToList();
            var groupsForEncryptedPasswords = GroupAuthorizations.IncludeSubGroups(groups);
            var groupsIdsTargetUserHasAccessTo = groupsForEncryptedPasswords.Select(g => g.Id).ToList();

            if (groupsIdsTargetUserHasAccessTo.Count > 0)
            {
                passwords = hptu.DbContext.SharedPasswords
                    .Include(sp => sp.EncryptedSharedPasswords)
                    .Where(sp =>
                               (sp.CreatedById.HasValue && sp.CreatedById.Value == targetUser.Id)
                               || (sp.GroupId.HasValue && groupsIdsTargetUserHasAccessTo.Contains(sp.GroupId.Value)));
            }
        }
        var oldKeyPair = targetUser.KeyPairs.LastActive();
        var oldPrivateKey = fakeEncryption
            ? "FAKE"
            : hptu.CryptoHelper.DecryptSymmetrically(oldKeyPair.EncryptedPrivateKey, oldTempPassword);
        var decryptedPasswords = fakeEncryption
            ? passwords
                .Select(sp => sp.EncryptedSharedPasswords.Single(esp => esp.KeyPairId == oldKeyPair.Id))
                .Select(p => new { sharedPasswordId = p.SharedPasswordId, encryptedPasswordString = "FAKE" })
            : passwords
                .Select(sp => sp.EncryptedSharedPasswords.Single(esp => esp.KeyPairId == oldKeyPair.Id))
                .Select(esp => new
                {
                    sharedPasswordId = esp.SharedPasswordId,
                    encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(
                        hptu.CryptoHelper.DecryptAsymmetrical(esp.EncryptedPasswordString, oldPrivateKey),
                        keyPair.PublicKey)
                });

        var variables = new
        {
            targetUserId = targetUser.Id,
            publicKey = keyPair.PublicKey,
            encryptedPrivateKey,
            newTempLoginPassword,
            newTempLoginPasswordHashed,
            encryptedSharedPasswords = decryptedPasswords.ToList(),
        };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var newCustomUser = JsonConvert.DeserializeObject<CustomUser>(json["data"]!["user"]!["resendAccountActivationEmail"]!.ToString());
        return newCustomUser?.ToUser();
    }

    /// <summary>
    /// Generates the necessary encryptions. If groups are provided the user will be added to those.
    /// Note that the user must not be an admin if the user should be added to groups.
    /// </summary>
    /// <param name="callingUser"></param>
    /// <param name="newUserData"></param>
    /// <param name="groupIdsToBeAddedTo"></param>
    /// <returns></returns>
    public async Task<User> AddUserWithCorrectEncryptions(User callingUser, AddUserTestModel newUserData, List<Group> groupIdsToBeAddedTo = null)
    {
        var adminPrivateKey = callingUser.KeyPairs.Last();
        var tempKeyPair = hptu.CryptoHelper.CreateKeyPair();

        var encryptedSharedPasswords = new List<EncryptedSharedPassword>();
        if (newUserData.admin)
        {
            if (groupIdsToBeAddedTo != null)
                throw new TestException("A new user cannot be an admin and also be added to groups");

            encryptedSharedPasswords = hptu.DbContext.SharedPasswords
                .Include(sp => sp.EncryptedSharedPasswords)
                .ByMandatorId(callingUser.MandatorId)
                .Select(sp => sp.EncryptedSharedPasswords
                            .Single(ep => ep.KeyPairId == adminPrivateKey.Id)
                )
                .ToList();
        }
        else if (groupIdsToBeAddedTo != null)
        {
            // Get all groups this user has access to
            var mandatorGroups = hptu.DbContext.Groups.RecursionBase(callingUser.MandatorId).ToList();
            var groupsRequested = new HashSet<Group>();
            foreach (var group in groupIdsToBeAddedTo)
                groupsRequested.Add(mandatorGroups.Single(g => g.Id == group.Id));
            var groupsWithAccess = GroupAuthorizations.IncludeSubGroups(groupsRequested);

            var groupIdsWithAccess = groupsWithAccess.Select(g => g.Id).ToList();

            newUserData.groupsToBeAddedTo = groupIdsWithAccess; // Add the groups to the request-object

            // Get passwords this user has access to
            encryptedSharedPasswords = hptu.DbContext.SharedPasswords
                .Include(sp => sp.EncryptedSharedPasswords)
                .ByMandatorId(callingUser.MandatorId)
                .Where(sp => sp.GroupId != null && groupIdsWithAccess.Contains(sp.GroupId.Value))
                .Select(sp => sp.EncryptedSharedPasswords.Single(ep => ep.KeyPairId == adminPrivateKey.Id))
                .ToList();
        }

        var encryptionsForAdding = encryptedSharedPasswords
            .Select(ep => new
            {
                sharedPasswordId = ep.SharedPasswordId,
                encryptedPasswordString =
                    hptu.CryptoHelper.EncryptAsymmetrical(
                        hptu.CryptoHelper.DecryptAsymmetrical(ep.EncryptedPasswordString, LoginPassword, adminPrivateKey.EncryptedPrivateKey),
                        tempKeyPair.PublicKey)
            })
            .ToList();

        newUserData.publicKey = tempKeyPair.PublicKey;
        const string tempNewUserPassword = "tmpPassword";
        newUserData.newTemporaryLoginPassword = tempNewUserPassword;
        newUserData.newTemporaryLoginPasswordHashed = hptu.CryptoHelper.ComputePasswordHash(tempNewUserPassword);
        newUserData.encryptedPrivateKey = hptu.CryptoHelper.EncryptSymmetrically(tempKeyPair.PrivateKey, tempNewUserPassword);
        newUserData.encryptedSharedPasswords = encryptionsForAdding;

        return await AddUser(callingUser, newUserData, groupIdsToBeAddedTo, encryptionsForAdding);
    }

    public async Task<User> AddUser(User callingUser, AddUserTestModel newUserData = null,
        IEnumerable<Group> groupsToBeAddedTo = null, IEnumerable<object> encryptedSharedPasswords = null)
    {
        var keyPair = hptu.CryptoHelper.CreateKeyPair();
        const string tempPassword = "tmpPassword";
        newUserData ??= new AddUserTestModel
        {
            firstname = "newUser",
            lastname = "newUser lastname",
            email = "newuser@vaulteron.com",
            sex = Sex.Unset.ToString(),
            admin = false,
            publicKey = keyPair.PublicKey,
            newTemporaryLoginPassword = tempPassword,
            newTemporaryLoginPasswordHashed = hptu.CryptoHelper.ComputePasswordHash(tempPassword),
            encryptedPrivateKey = hptu.CryptoHelper.EncryptSymmetrically(keyPair.PrivateKey, tempPassword),
            groupsToBeAddedTo = groupsToBeAddedTo?.Select(g => g.Id).ToList(),
            encryptedSharedPasswords = encryptedSharedPasswords
        };

        const string query = @"
                mutation AddUser($user: AddUserInputType!) {
                    user {
                        add(user: $user) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var variables = new { user = newUserData };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var newUser = JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["add"]!.ToString());

        return newUser;
    }

    public async Task<User> EditUser(User callingUser, EditUserModel editUserModel)
    {
        const string query = @"
                mutation ($user: EditUserInputType!) {
                  user {
                    edit(user: $user) {
                      id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                      publicKey { publicKeyString }
                      groups { id }
                      usergroups { groupId }
                    }
                  }
                }
            ";
        var variables = new
        {
            user = new
            {
                id = editUserModel.Id,
                firstname = editUserModel.Firstname,
                lastname = editUserModel.Lastname,
                email = editUserModel.Email,
            }
        };

        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var resUser = JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["edit"]!.ToString());
        return resUser;
    }
    public async Task<User> ArchiveUser(User admin, User userToBeArchived, bool reactivate = false)
    {
        const string query = @"
                mutation ArchiveUser($id: Guid!, $reactivate: Boolean) {
                    user {
                        archive(id: $id, reactivate: $reactivate) {
                            id firstname lastname email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var variables = new { id = userToBeArchived.Id, reactivate = reactivate };
        var json = await hptu.MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));
        var modifiedUser = JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["archive"]!.ToString());
        return modifiedUser;
    }

    private const string SimpleUserFragment =
        @"id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt twoFATypesEnabled";

    private const string ComplexUserFragment =
        @"id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
              publicKey { id publicKeyString }
              groups { id }
              usergroups { groupId }";


    public async Task ActivateLastCreatedAccount(string newLoginPassword = LoginPassword)
    {
        var testEmailSender = hptu.TestEmailSender;
        var cryptoHelper = hptu.CryptoHelper;
        var client = hptu.Client;

        // Retrieve email with account activation data
        Assert.NotEmpty(testEmailSender.SentEmails);
        var dataSendConfirmEmailAsync = testEmailSender.SentEmails.Last(e => e.EmailType == TestEmailSenderService.EmailType.SendEmailAfterUserAddedAsync);
        testEmailSender.SentEmails.Remove(dataSendConfirmEmailAsync);
        var newUserId = (Guid) dataSendConfirmEmailAsync.Content.registeredUserId;
        var activationToken = (string) dataSendConfirmEmailAsync.Content.emailConfirmationToken;
        var tempLoginPassword = (string) dataSendConfirmEmailAsync.Content.tempLoginUserPassword;

        // Get all passwords that are already encrypted for this user
        var body = new EmailGetOldEncryptionsForm
        {
            UserId = newUserId,
            Token = activationToken,
            OldPasswordHashed = tempLoginPassword
        };
        var getOldEncryptionsResponse = await client.PostAsync(ApiClient.ApiEmailGetPublicKeyAndSharedPasswordsUri, body.ToJson());
        Assert.True(getOldEncryptionsResponse.IsSuccessStatusCode);
        var getOldEncryptionsJson = await getOldEncryptionsResponse.Content.ReadAsStringAsync();
        var passwordsView = JsonConvert.DeserializeObject<EncryptedPrivateKeyAndSharedPasswordsView>(getOldEncryptionsJson);
        Assert.NotNull(passwordsView);

        // Encrypt with new keypair and activate account
        var oldPrivateKey = cryptoHelper.DecryptSymmetrically(passwordsView!.EncryptedPrivateKey, tempLoginPassword);

        var newKeyPair = cryptoHelper.CreateKeyPair();
        var accountActivationModel = new EmailSetPasswordModel
        {
            UserId = newUserId,
            Token = activationToken,
            OldPassword = tempLoginPassword,
            NewPassword = newLoginPassword,
            PublicKey = newKeyPair.PublicKey,
            EncryptedPrivateKey = cryptoHelper.EncryptSymmetrically(newKeyPair.PrivateKey),
            EncryptedSharedPasswords = new List<EncryptedSharedPassword>(),
            PreviousPasswordHashEncryptedWithPublicKey = cryptoHelper.EncryptAsymmetrical(tempLoginPassword, newKeyPair.PublicKey)
        };
        foreach (var encryptedPassword in passwordsView.encryptedSharedPasswords)
        {
            var decryptedString = cryptoHelper.DecryptAsymmetrical(encryptedPassword.EncryptedPasswordString, oldPrivateKey);
            var newlyEncryptedString = cryptoHelper.EncryptAsymmetrical(decryptedString, newKeyPair.PublicKey);
            accountActivationModel.EncryptedSharedPasswords.Add(new EncryptedSharedPassword
            {
                SharedPasswordId = encryptedPassword.SharedPasswordId,
                EncryptedPasswordString = newlyEncryptedString,
            });
        }

        var activationResponse = await client.PostAsync(ApiClient.ApiSetPasswordFromEmailTokenUri, accountActivationModel.ToJson());
        Assert.True(activationResponse.IsSuccessStatusCode);
    }
}

internal class CustomUser : IdentityUser<Guid>
{
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public Sex Sex { get; set; }
    public bool Admin { get; set; }
    public DateTime? ArchivedAt { get; set; }
    public List<TwoFATypes> TwoFATypesEnabled { get; set; }
    public KeyPair KeyPair { get; set; }

    public User ToUser()
    {
        var newUser = new User
        {
            Id = Id,
            Firstname = Firstname,
            Lastname = Lastname,
            Sex = Sex,
            Email = Email,
            Admin = Admin,
            EmailConfirmed = EmailConfirmed,
            UpdatedAt = UpdatedAt,
            CreatedAt = CreatedAt,
            ArchivedAt = ArchivedAt,
            TwoFATypesEnabled = TwoFATypesEnabled?.CombineFlags() ?? 0,
            KeyPairs = new List<KeyPair> { KeyPair }
        };
        if (KeyPair != null)
        {
            KeyPair.User = newUser;
            KeyPair.UserId = newUser.Id;
        }

        return newUser;
    }
}