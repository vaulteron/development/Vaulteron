﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronTests.helper;
using Xunit;
using Xunit.Abstractions;

namespace VaulteronTests.integrationTests.IntegrationTestServices;

public class GroupTestService
{
    private readonly ITestOutputHelper logger;
    private readonly HarryPotterTestUniverse hptu;

    private readonly Mandator mandator;
    private readonly Dictionary<string, User> testUser;
    private readonly Dictionary<string, Group> testGroups;
    private readonly Dictionary<string, SharedTag> testTags;

    public GroupTestService(ITestOutputHelper logger, HarryPotterTestUniverse hptu)
    {
        this.logger = logger;
        this.hptu = hptu;

        mandator = hptu.Mandator;
        testUser = hptu.TestUser;
        testGroups = hptu.TestGroups;
        testTags = hptu.TestTags;
    }

    public async Task<List<Group>> GetGroupsByParent(string userName, string parentGroupName,
        bool? byArchived = false)
    {
        var user = testUser[userName];
        var parentGroup = parentGroupName != null ? testGroups[parentGroupName] : null;

        const string query = @"
                query($parentId: Guid, $byArchived: Boolean = false) {
                  groupsByParent(parentId: $parentId, byArchived: $byArchived ) {
                    id name archivedAt
                  }
                }
               ";
        var variables = new { parentId = parentGroup?.Id ?? Guid.Empty, byArchived };
        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query,
                                                      JsonConvert.SerializeObject(variables));

        var returnedGroups =
            JsonConvert.DeserializeObject<List<Group>>(json["data"]!["groupsByParent"]!.ToString());
        return returnedGroups;
    }

    public async Task<List<KeyPair>> GetKeyPairsOfUsersWithAccessToGroup(User user, Group targetGroup = null)
    {
        var usersWithAccess = await GetUsers(user, targetGroup?.Id ?? Guid.Empty, true);
        var keyPairs = usersWithAccess.Select(u => u.KeyPairs.LastActive()).ToList();
        return keyPairs;
    }

    public async Task<List<KeyPair>> GetKeyPairsOfUsersWithAccessToGroup(string executingUsername, string targetGroupName)
    {
        var user = testUser[executingUsername];
        var targetGroup = hptu.TestGroups[targetGroupName];
        return await GetKeyPairsOfUsersWithAccessToGroup(user, targetGroup);
    }

    public async Task<List<User>> GetUsers(string userThatMakesRequestName, string targetGroupName, bool withAccess = false)
    {
        var targetGroupId = hptu.TestGroups[targetGroupName].Id;
        return await GetUsers(userThatMakesRequestName, targetGroupId, withAccess);
    }

    public async Task<List<User>> GetUsers(string userThatMakesRequestName, Guid targetGroup, bool withAccess = false)
    {
        var user = testUser[userThatMakesRequestName];
        return await GetUsers(user, targetGroup, withAccess);
    }

    public async Task<List<User>> GetUsers(User userThatMakesRequest, Guid targetGroup, bool withAccess = false)
    {
        var query = @"
                query($ids: [Guid]) {
                  groups(ids: $ids) {
                    id
                    "
            + (withAccess ? "usersWithAccess" : "users")
            + @" { id firstname lastname admin sex email
                                 usergroups { groupId groupRole }
                                 publicKey { id publicKeyString }
                              }
                    "
            + (withAccess ? "" : "countUsers")
            + @"
                  }
                }
               ";
        var variables = new { ids = new List<Guid> { targetGroup } };
        var json = await hptu.MakeGraphQLRequestAsync(userThatMakesRequest.Email, query,
                                                      JsonConvert.SerializeObject(variables));

        var returnedGroups = JsonConvert.DeserializeObject<List<Group>>(json["data"]!["groups"]!.ToString());
        Assert.NotNull(returnedGroups);
        Assert.NotEmpty(returnedGroups!);
        Assert.True(returnedGroups!.Count == 1);
        Assert.Equal(targetGroup, returnedGroups.Single().Id);

        foreach (var x in json["data"]!["groups"]!.ToList().First().Children().ToList()
                     .First(t => ((JProperty) t).Name == "users" + (withAccess ? "WithAccess" : "")).Children().First()
                     .SelectMany(gr => gr.Children().First(t => ((JProperty) t).Name == "usergroups").Children().First().Children()))
        {
            x["groupRole"] = GraphQLStrToGroupRole(x["groupRole"]!.ToString()).ToString();
        }

        List<User> users;
        if (withAccess)
        {
            var usersJson = json["data"]!["groups"]![0]!["usersWithAccess"]!;
            users = JsonConvert.DeserializeObject<List<User>>(usersJson.ToString())!.ToList();
            for (var i = 0; i < users.Count; i++)
            {
                var keyPair = JsonConvert.DeserializeObject<KeyPair>(usersJson[i]!["publicKey"]!.ToString());
                users[i].KeyPairs.Add(keyPair);
            }
        }
        else
        {
            users = JsonConvert.DeserializeObject<List<User>>(
                    json["data"]!["groups"]!.ToList().First().Children().ToList()
                        .First(t => ((JProperty) t).Name == "users" + (withAccess ? "WithAccess" : "")).Children()
                        .First()
                        .ToString()
                )!
                .ToList();
        }

        if (!withAccess)
        {
            var usersCount = JsonConvert.DeserializeObject<int>(
                json["data"]!["groups"]!.ToList().First().Children().ToList()
                    .First(t => ((JProperty) t).Name == "countUsers").Children().First().ToString()
            );
            Assert.Equal(usersCount, users.Count);
        }

        return users;
    }

    public async Task<JObject> AddUserToGroup(User callingUser, string targetUserName, string targetGroupName,
        GroupRole role, string[] groupnamesThatAreSubgroupsOfTargetGroups = null)
    {
        var targetUser = testUser[targetUserName];
        var targetGroup = testGroups[targetGroupName];

        groupnamesThatAreSubgroupsOfTargetGroups ??= targetGroup.ChildGroups.Select(g => g.Name).ToArray();

        var groupIds = groupnamesThatAreSubgroupsOfTargetGroups.Select(groupName => testGroups[groupName].Id)
            .Append(targetGroup.Id).ToList();
        var encryptedSharedPasswords =
            hptu.DbContext.SharedPasswords
                .Where(sp => sp.GroupId.HasValue && groupIds.Contains(sp.GroupId.Value))
                .Select(sp => new
                {
                    sharedPasswordId = sp.Id,
                    encryptedPasswordString =
                        hptu.CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                              targetUser.KeyPairs.LastActive().PublicKeyString)
                });

        return await AddUserToGroupJson(callingUser, targetUser.Id, targetGroup.Id, role, encryptedSharedPasswords);
    }

    public async Task<JObject> AddUserToGroupJson(User callingUser, Guid targetUserId, Guid targetGroupId,
        GroupRole groupRole, IEnumerable<object> encryptedSharedPasswords)
    {
        const string query = @"
                mutation ($user: AddUserGroupType!) {
                  group {
                    addUser (user: $user) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new
        {
            user = new
            {
                groupId = targetGroupId,
                userId = targetUserId,
                groupRole = GroupRoleToGraphQLStr(groupRole),
                encryptedSharedPasswords
            }
        };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query,
                                                      JsonConvert.SerializeObject(variables));
        return json;
    }

    public async Task<User> RemoveUserFromGroup(User callingUser, User targetUser, Group targetGroup)
    {
        const string query = @"
                mutation ($groupId: Guid!, $userId: Guid!) {
                  group {
                    deleteUser (groupId: $groupId, userId: $userId) {
                      id firstname
                    }
                  }
                }
            ";
        var variables = new { groupId = targetGroup.Id, userId = targetUser.Id };

        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query,
                                                      JsonConvert.SerializeObject(variables));
        return JsonConvert.DeserializeObject<User>(json["data"]!["group"]!["deleteUser"]!.ToString());
    }

    public async Task<Group> CreateGroup(string creatorUsername, string newGroupName, Guid? parentGroupGuid = null)
    {
        var userThatMakesRequest = testUser[creatorUsername];

        const string query = @"
                mutation ($group: GroupInputType!) {
                  group { add (group: $group) {
                    id name
                  }}
                }
            ";
        var variables = new { group = new { name = newGroupName, parentGroupId = parentGroupGuid } };
        var json = await hptu.MakeGraphQLRequestAsync(userThatMakesRequest.Email, query,
                                                      JsonConvert.SerializeObject(variables));
        var resGroup = JsonConvert.DeserializeObject<Group>(json["data"]!["group"]!["add"]!.ToString());
        return resGroup;
    }

    public async Task<Group> EditGroup(User callingUser, Group targetGroup, string newName)
    {
        const string query = @"
                mutation ($group: EditGroupInputType!) {
                  group { edit (group: $group) {
                    id name
                  } }
                }
            ";
        var variables = new { group = new { name = newName, id = targetGroup.Id } };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query,
                                                      JsonConvert.SerializeObject(variables));
        var resGroup = JsonConvert.DeserializeObject<Group>(json["data"]!["group"]!["edit"]!.ToString());
        return resGroup;
    }

    public async Task ArchiveGroup(User callingUser, Guid groupGuid)
    {
        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    archive (groupId: $group) {
                      id name archivedAt
                    }
                  }
                }
            ";
        var variables = new { group = groupGuid };

        await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task ReactivateGroup(User callingUser, Guid groupGuid)
    {
        const string query = @"
                mutation ($group: Guid!) {
                  group {
                    archive (groupId: $group, reactivate: true) {
                      id name archivedAt
                    }
                  }
                }
            ";
        var variables = new { group = groupGuid };

        await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<Group> MoveGroup(User callingUser, Guid groupToMoveId, Guid? destinationParentGroupId, IEnumerable<Guid> userToEncryptForIds = null)
    {
        var targetUser = new List<User>();

        if (userToEncryptForIds != null)
        {
            targetUser = hptu.TestUser.Values.Where(u => userToEncryptForIds.Contains(u.Id)).ToList();
        }
        else if (destinationParentGroupId.HasValue && destinationParentGroupId.Value != Guid.Empty)
        {
            var users = await GetUsers(callingUser.Firstname, destinationParentGroupId.Value, true);
            targetUser = users.Where(u => !u.Admin).ToList();
        }

        var encryptedSharedPasswords =
            targetUser.Join(
                    hptu.DbContext.SharedPasswords.Where(sp => sp.GroupId.HasValue && groupToMoveId == sp.GroupId.Value),
                    _ => true, _ => true,
                    (u, sp) => new
                    {
                        sharedPasswordId = sp.Id,
                        keyPairId = u.KeyPairs.LastActive().Id,
                        encryptedPasswordString =
                            hptu.CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                                  u.KeyPairs.LastActive().PublicKeyString)
                    })
                .ToList();

        const string query = @"
                mutation MoveGroup($groupToMoveId: Guid!, $newParentGroupId: Guid, $encryptedSharedPasswords: [EncryptedSharedPasswordInputType]) {
                    group {
                        move (groupToMoveId: $groupToMoveId, newParentGroupId: $newParentGroupId, encryptedSharedPasswords: $encryptedSharedPasswords){
                            name
                        }
                    }
                }
            ";
        var variables = new
        {
            groupToMoveId = groupToMoveId,
            newParentGroupId = destinationParentGroupId,
            encryptedSharedPasswords
        };
        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query,
                                                      JsonConvert.SerializeObject(variables));
        var resGroup = JsonConvert.DeserializeObject<Group>(json["data"]!["group"]!["move"]!.ToString());
        return resGroup;
    }

    public string GroupRoleToGraphQLStr(GroupRole gr) => gr switch
    {
        GroupRole.GroupAdmin => "GROUP_ADMIN",
        GroupRole.GroupEditor => "GROUP_EDITOR",
        GroupRole.GroupViewer => "GROUP_VIEWER",
        _ => throw new Exception("Should not be")
    };

    public GroupRole GraphQLStrToGroupRole(string gr) => gr switch
    {
        "GROUP_ADMIN" => GroupRole.GroupAdmin,
        "GROUP_EDITOR" => GroupRole.GroupEditor,
        "GROUP_VIEWER" => GroupRole.GroupViewer,
        _ => throw new Exception("Should not be")
    };
}