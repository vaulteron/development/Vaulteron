﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.SeedData;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;

namespace VaulteronTests.integrationTests.IntegrationTestServices;

public class PasswordTestService
{
    private readonly HarryPotterTestUniverse hptu;
    private const string PwString = "super_secure_pw_1234?!";

    private readonly Mandator mandator;
    private readonly Dictionary<string, User> testUser;
    private readonly Dictionary<string, Group> testGroups;
    private readonly Dictionary<string, SharedTag> testTags;

    private readonly GroupTestService groupTestService;

    public PasswordTestService(ITestOutputHelper logger, HarryPotterTestUniverse hptu)
    {
        this.hptu = hptu;

        mandator = hptu.Mandator;
        testUser = hptu.TestUser;
        testGroups = hptu.TestGroups;
        testTags = hptu.TestTags;

        groupTestService = new GroupTestService(logger, hptu);
    }

    #region AccountPassword

    public async Task<JObject> GetAllAccountPasswordsJsonAsync(User user, bool? byArchived = false)
    {
        const string query = @"query GetAccountPasswords($byArchived: Boolean = false) {
                accountPasswords(byArchived: $byArchived) { "
            + AccountPasswordFragment
            + @" } }";
        var variables = new { byArchived = byArchived };
        return await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<List<AccountPassword>> GetAllAccountPasswordsAsync(User user, bool? byArchived = false)
    {
        var json = await GetAllAccountPasswordsJsonAsync(user, byArchived);
        var accountPasswords =
            JsonConvert.DeserializeObject<List<AccountPassword>>(json["data"]!["accountPasswords"]!.ToString());
        return accountPasswords;
    }

    public async Task<AccountPassword> AddAccountPasswordAsync(string userName, string pwString = PwString, List<Guid> tags = null)
    {
        var user = testUser[userName];
        var keyPair = user.KeyPairs.LastActive();

        const string query = @"
                mutation AddAccountPassword($accountPassword: AddAccountPasswordInputType!) {
                    accountPassword {
                        add(password: $accountPassword) {"
            + AccountPasswordFragment
            + @" }
                    }
                }";

        var encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(pwString, keyPair.PublicKeyString);

        var newPassword = new AccountPassword
        {
            Name = "new PW",
            Login = "my@email.com",
            WebsiteURL = "unknown.com",
            Notes = "some notes",
        };

        var variables = new
        {
            accountPassword = new
            {
                name = newPassword.Name,
                login = newPassword.Login,
                websiteUrl = newPassword.WebsiteURL,
                notes = newPassword.Notes,
                tags = tags ?? new List<Guid>(),
                encryptedPassword = encryptedPasswordString,
                keyPairId = keyPair.Id,
            }
        };
        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var createdPassword = JsonConvert.DeserializeObject<AccountPassword>(
            json["data"]!["accountPassword"]!["add"]!.ToString()
        );
        return createdPassword;
    }

    public async Task<AccountPassword> EditAccountPasswordAsync(string userName, AccountPassword differentPassword, string pwString = PwString, List<Guid> tags = null)
    {
        var user = testUser[userName];
        var keyPair = user.KeyPairs.LastActive();

        const string query = @"
                mutation EditAccountPassword($accountPassword: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $accountPassword) {"
            + AccountPasswordFragment
            + @" }
                    }
                }";

        var encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(pwString, keyPair.PublicKeyString);

        var variables = new
        {
            accountPassword = new
            {
                id = differentPassword.Id,
                name = differentPassword.Name,
                login = differentPassword.Login,
                websiteURL = differentPassword.WebsiteURL,
                notes = differentPassword.Notes,
                tags = tags ?? new List<Guid>(),
                encryptedPassword = new
                {
                    keyPairId = keyPair.Id,
                    encryptedPasswordString
                },
            }
        };
        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var editedPassword = JsonConvert.DeserializeObject<AccountPassword>(
            json["data"]!["accountPassword"]!["edit"]!.ToString()
        );
        return editedPassword;
    }

    public async Task<JObject> DeleteAccountPasswordJsonAsync(User user, Guid targetAccountPasswordId)
    {
        const string query = @"
                            mutation DeleteAccountPassword($id: Guid!) {
                                accountPassword {
                                    delete(id: $id) {
                                        id name login securityRating websiteURL notes updatedAt createdAt
                                        tags { id name color }
                                        owner { id }
                                    }
                                }
                            }";

        var variables = new { id = targetAccountPasswordId };
        return await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<AccountPassword> DeleteAccountPasswordAsync(User user, Guid targetAccountPasswordId)
    {
        var json = await DeleteAccountPasswordJsonAsync(user, targetAccountPasswordId);
        return JsonConvert.DeserializeObject<AccountPassword>(
            json["data"]!["accountPassword"]!["delete"]!.ToString());
    }

    private const string AccountPasswordFragment =
        @"id name login securityRating updatedAt notes websiteURL
                    tags { id name } accessLog{ id createdAt}";

    #endregion

    #region SharedPasswords

    public async Task<string> GetSingleSharedPasswordStringAsync(User user, Guid addedPasswordId)
    {
        const string query = @"query EncryptedSharedPasswordString($passwordId: Guid!) { encryptedSharedPasswordString(passwordId: $passwordId) }";

        var variables = new
        {
            passwordId = addedPasswordId,
            // KeyPairId = user.KeyPairs.LastActive().Id
        };

        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        return json["data"]!["encryptedSharedPasswordString"]!.ToString();
    }

    public async Task<JObject> GetAllSharedPasswordJsonAsync(string userName, string groupName = null, bool? byArchived = false)
    {
        var user = testUser[userName];
        var groupIds = groupName != null ? new[] { testGroups[groupName].Id } : Array.Empty<Guid>();

        const string query = @"query GetSharedPasswordsByGroupId($groupIds: [Guid]!, $byArchived: Boolean = false) {
                sharedPasswords(groupIds: $groupIds, byArchived: $byArchived) { "
            + SharedPasswordFragment
            + @" } }";

        var variables = new { groupIds = groupIds, byArchived = byArchived };

        return await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public Task MoveSharedPasswordToGroupAsync(string callingUserName, Guid passwordsToMoveId, string destinationGroupName, string sourceGroupName)
        => MoveSharedPasswordsToGroupAsync(callingUserName, passwordsToMoveId, destinationGroupName, sourceGroupName);

    public async Task MoveSharedPasswordsToGroupAsync(string callingUserName, Guid passwordsToMoveId,
        string destinationGroupName, string sourceGroupName, string plaintextPassword = VaulteronSeedData.unencryptedPasswordString)
    {
        var keyPairsForUsersInTargetGroup = await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(callingUserName, destinationGroupName);
        var keyPairsForUsersInParentGroupWithAdmins = await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(callingUserName, sourceGroupName);
        var keyPairs = keyPairsForUsersInTargetGroup
            .Where(pk => !keyPairsForUsersInParentGroupWithAdmins.Contains(pk))
            .ToList();
        var encryptions = keyPairs
            .Select(pk => new
            {
                sharedPasswordId = passwordsToMoveId,
                keyPairId = pk.Id,
                encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, pk.PublicKeyString)
            });

        await MoveSharedPasswordToAnotherGroupJsonAsync(callingUserName, new List<Guid> { passwordsToMoveId }, destinationGroupName, encryptions);
    }

    public Task MoveSharedPasswordToCompanyAsync(string callingUserName, Guid passwordsToMoveId, string destinationUserName)
        => MoveSharedPasswordToCompanyAsync(callingUserName, new List<Guid> { passwordsToMoveId }, destinationUserName);

    // TODO: modify all these move-helper here so that we actually get all the passwords and decrypt them (instead of creating new ones here)
    public async Task MoveSharedPasswordToCompanyAsync(string callingUserName, List<Guid> passwordsToMoveIds,
        string destinationUserName, string plaintextPassword = VaulteronSeedData.unencryptedPasswordString)
    {
        var callingUser = hptu.TestUser[destinationUserName];

        var keyPairs = hptu.TestUser
            .ByMandator(callingUser.Mandator.Name)
            .Where(u => u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive())
            .Union(new List<KeyPair> { callingUser.KeyPairs.LastActive() });
        var encryptedPasswords = keyPairs
            .Join(passwordsToMoveIds, _ => true, _ => true, (pk, pId) => new
            {
                keyPairId = pk.Id,
                sharedPasswordId = pId,
                encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, pk.PublicKeyString)
            });

        await MoveSharedPasswordToCompanyJsonAsync(callingUserName, passwordsToMoveIds, destinationUserName, encryptedPasswords);
    }

    public Task MoveCompanyPasswordToSharedGroupPasswordAsync(string callingUserName, Guid passwordsToMoveId, string destinationGroupName)
        => MoveCompanyPasswordToSharedGroupPasswordAsync(callingUserName, new List<Guid> { passwordsToMoveId }, destinationGroupName);

    public async Task MoveCompanyPasswordToSharedGroupPasswordAsync(string callingUserName, List<Guid> passwordsToMoveIds,
        string destinationGroupName, string plaintextPassword = VaulteronSeedData.unencryptedPasswordString)
    {
        var keyPairs =
            await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(callingUserName, destinationGroupName);
        var encryptedPasswords = keyPairs
            .Join(passwordsToMoveIds, _ => true, _ => true, (pk, pId) => new
            {
                keyPairId = pk.Id,
                sharedPasswordId = pId,
                encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, pk.PublicKeyString)
            });

        await MoveCompanyPasswordToSharedJsonAsync(callingUserName, passwordsToMoveIds, destinationGroupName, encryptedPasswords);
    }

    public async Task<List<SharedPassword>> GetAllSharedPasswordsAsync(string userName)
    {
        return await GetAllSharedPasswordsForGroupAsync(userName, null);
    }

    public async Task<List<SharedPassword>> GetAllSharedPasswordsForGroupAsync(string userName, string groupName, bool? byArchived = false)
    {
        var json = await GetAllSharedPasswordJsonAsync(userName, groupName, byArchived);

        var sharedPasswords =
            JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["sharedPasswords"]!.ToString());
        return sharedPasswords;
    }

    public async Task<SharedPassword> DeleteSharedPasswordAsync(User user, Guid passwordGuid)
    {
        var json = await DeleteSharedPasswordJsonAsync(user, passwordGuid);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["delete"]!.ToString());
    }

    public async Task<JObject> DeleteSharedPasswordJsonAsync(User user, Guid passwordGuid)
    {
        const string query = @"
               mutation DeleteSharedPassword($sharedPasswordGuid: Guid!) {
                    sharedPassword {
                        delete(id: $sharedPasswordGuid) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";
        var variables = new { sharedPasswordGuid = passwordGuid };

        return await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<SharedPassword> AddSharedPasswordAsync(string username, string groupName, string passwordName,
        string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        var json = await AddSharedPasswordJsonAsync(username, groupName, passwordName, plaintextPassword, tags);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());
    }

    public async Task<JObject> AddSharedPasswordJsonAsync(string username, string groupName, string passwordName,
        string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        var keyPairs =
            await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(username, groupName);
        var encryptedPasswords = keyPairs
            .Select(kp => new EncryptedPasswordTestModel
            {
                keyPairId = kp.Id,
                encryptedPasswordString =
                    hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, kp.PublicKeyString)
            });

        return await AddSharedPasswordJsonAsync(username, groupName, passwordName, encryptedPasswords, tags);
    }

    public Task EditSharedPasswordMetaAsync(string username, SharedPassword editedPassword)
        => EditSharedPasswordAsync(username, null, editedPassword, null);

    public async Task<SharedPassword> EditSharedPasswordAsync(string username, string groupName, SharedPassword newSharedPassword,
        string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        var json = await EditSharedPasswordJsonAsync(username, groupName, newSharedPassword, plaintextPassword,
                                                     tags);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["edit"]!.ToString());
    }

    public async Task<JObject> EditSharedPasswordJsonAsync(string username, string groupName, SharedPassword newSharedPassword,
        string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        plaintextPassword = groupName == null ? null : plaintextPassword;
        var keyPairs =
            plaintextPassword == null
                ? null
                : await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(username, groupName);

        var encryptedPasswords =
            plaintextPassword == null
                ? null
                : keyPairs!
                    .Select(kp => new EncryptedPasswordTestModel
                    {
                        keyPairId = kp.Id,
                        encryptedPasswordString = hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, kp.PublicKeyString)
                    });

        return await EditSharedPasswordJsonAsync(username, newSharedPassword, encryptedPasswords);
    }

    private const string SharedPasswordFragment =
        @"id name login securityRating websiteURL notes updatedAt createdAt archivedAt modifyLock groupId isSavedAsOfflinePassword
              tags { id name color } accessLog { id createdAt }";

    public async Task<List<KeyPair>> RequestMissingEncryptionsForSharedPassword(User callingUser, Guid sharedPasswordGuid)
    {
        const string query = @"query MissingEncryptions($passwordId: Guid!) 
	            {missingEncryptionsForPassword(passwordId: $passwordId) { id publicKeyString } }";

        var variables = new
        {
            passwordId = sharedPasswordGuid
        };

        var json = await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var keyPairs =
            JsonConvert.DeserializeObject<List<KeyPair>>(
                json["data"]!["missingEncryptionsForPassword"]!.ToString());
        return keyPairs;
    }

    #endregion

    #region CompanyPasswords

    public async Task<string> GetSingleCompanyPasswordAsync(User user, Guid addedPasswordId)
    {
        const string query = @"query EncryptedSharedPasswordString($passwordId: Guid!) 
	            {encryptedSharedPasswordString(passwordId: $passwordId)}";

        var variables = new
        {
            passwordId = addedPasswordId,
            // KeyPairId = user.KeyPairs.LastActive().Id
        };

        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        return json["data"]!["encryptedSharedPasswordString"]!.ToString();
    }

    public async Task<JObject> GetAllCompanyPasswordJsonAsync(string userName)
    {
        var user = testUser[userName];

        const string query = @"query GetCompanyPasswordsByGroupId {
                companyPasswords { "
            + SharedPasswordFragment
            + @" } }";

        var variables = new { };

        return await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<List<SharedPassword>> GetAllCompanyPasswordsAsync(string userName)
    {
        var json = await GetAllCompanyPasswordJsonAsync(userName);

        var sharedPasswords =
            JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["companyPasswords"]!.ToString());
        return sharedPasswords;
    }

    public async Task<SharedPassword> AddCompanyPasswordAsync(string username, string passwordName, string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        var json = await AddCompanyPasswordJsonAsync(username, passwordName, plaintextPassword, tags);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["companyAdd"]!
                                                                 .ToString());
    }

    public async Task<JObject> AddCompanyPasswordJsonAsync(string username, string passwordName, string plaintextPassword, IEnumerable<Guid> tags = null)
    {
        var user = testUser[username];
        var keyPairs = testUser.ByMandator(user.Mandator.Name).Where(u => u.Key == username || u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());
        var encryptedPasswords = keyPairs
            .Select(kp => new EncryptedPasswordTestModel
            {
                keyPairId = kp.Id,
                encryptedPasswordString =
                    hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, kp.PublicKeyString)
            });

        return await AddCompanyPasswordJsonAsync(username, passwordName, encryptedPasswords, tags);
    }


    public async Task<SharedPassword> EditCompanyPasswordAsync(string username, SharedPassword newSharedPassword, string plaintextPassword)
    {
        var json = await EditCompanyPasswordJsonAsync(username, newSharedPassword, plaintextPassword);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["edit"]!.ToString());
    }

    public async Task<JObject> EditCompanyPasswordJsonAsync(string username, SharedPassword newSharedPassword, string plaintextPassword)
    {
        var user = testUser[username];
        var keyPairs = testUser.ByMandator(user.Mandator.Name).Where(u => u.Key == username || u.Value.Admin)
            .Select(u => u.Value.KeyPairs.LastActive());

        var encryptedPasswords =
            plaintextPassword == null
                ? null
                : keyPairs!
                    .Select(kp => new EncryptedPasswordTestModel
                    {
                        keyPairId = kp.Id,
                        encryptedPasswordString =
                            hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, kp.PublicKeyString)
                    });

        return await EditSharedPasswordJsonAsync(username, newSharedPassword, encryptedPasswords);
    }

    public async Task<SharedPassword> DeleteCompanyPassword(User user, Guid passwordGuid)
    {
        return await DeleteSharedPasswordAsync(user, passwordGuid);
    }

    #endregion

    #region Encryptions

    public IEnumerable<(string Id, string encryptedString)> GetFakeDecryptedSharedPasswordsForGroups(string keyPair, List<Group> groups)
    {
        var groupIds = groups.Select(g => g.Id).ToList();
        var groupPasswords = hptu.DbContext.Groups
            .Where(g => groupIds.Contains(g.Id))
            .SelectMany(g => g.SharedPasswords)
            .AsEnumerable()
            .Select(sp => (sp.Id.ToString(), hptu.CryptoHelper.EncryptAsymmetrical("filler", keyPair)));
        return groupPasswords;
    }
    public IEnumerable<(string Id, string encryptedString)> GetFakeDecryptedSharedPasswordsForGroups(User user, List<Group> groups) =>
        GetFakeDecryptedSharedPasswordsForGroups(user.KeyPairs.LastActive().PublicKeyString, groups);

    public IEnumerable<(string Id, string encryptedString)> GetFakeEncryptedCompanyPasswords(User user, bool includeOfflinePasswords = false)
    {
        var keyPair = user.KeyPairs.LastActive();
        var companyPasswords = hptu.DbContext.SharedPasswords
            .BaseCompany(user.MandatorId, null, includeOfflinePasswords)
            .Where(pw => user.Admin || pw.CreatedById.HasValue && pw.CreatedById.Value == user.Id)
            .AsEnumerable()
            .Select(sp => (sp.Id.ToString(), hptu.CryptoHelper.EncryptAsymmetrical("filler", keyPair.PublicKeyString)));
        return companyPasswords;
    }

    public IEnumerable<(string Id, string encryptedString)> GetEncryptedAccountPasswords(User user, KeyPair oldKeyPair, string newPublicKeyString)
    {
        var encryptedAccountPasswords = hptu.DbContext.AccountPasswords
            .Include(pw => pw.EncryptedAccountPasswords)
            .ByMe(user.Id)
            .Select(pw => pw.EncryptedAccountPasswords
                        .OrderByDescending(eap => eap.CreatedAt)
                        .First(eap => eap.KeyPairId == oldKeyPair.Id)
            )
            .ToList();
        var decryptedPasswords = encryptedAccountPasswords
            .Select(pw =>
            {
                var decrypted = hptu.CryptoHelper.DecryptAsymmetrical(pw.EncryptedPasswordString, VaulteronSeedData.LoginPassword, oldKeyPair.EncryptedPrivateKey);
                var newlyEncrypted = hptu.CryptoHelper.EncryptAsymmetrical(decrypted, newPublicKeyString);
                return (
                    pw.AccountPasswordId.ToString(),
                    newlyEncrypted
                );
            });
        return decryptedPasswords;
    }

    #endregion

    #region helper

    public int GetIndexInsideJsonOfPasswordWithTargetName(JObject json, string name, bool isAccountPassword)
    {
        var passwordAccessString = isAccountPassword ? "accountPasswords" : "sharedPasswords";
        var accountPasswordListAsJObject =
            json["data"]![passwordAccessString]!.ToList().Select(o => o.ToList()).ToList();
        var index = -1;
        for (var i = 0; i < accountPasswordListAsJObject.Count; i++)
        {
            var jToken = accountPasswordListAsJObject[i];
            var properties = jToken.ToList();
            foreach (var property in properties)
            {
                var propertyName = (property as JProperty)?.Name;
                if (!(propertyName is "name")) continue;

                var value = (property as JProperty)?.Value.ToString();
                if (value != name) continue;

                index = i;
                break;
            }
        }

        return index;
    }

    public async Task<SharedPassword> AddSharedPasswordAsync(string username, string groupName, string passwordName,
        IEnumerable<EncryptedPasswordTestModel> newlyEncryptedPasswords, IEnumerable<Guid> tags = null)
    {
        var json = await AddSharedPasswordJsonAsync(username, groupName, passwordName, newlyEncryptedPasswords,
                                                    tags);
        return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());
    }

    public async Task<JObject> AddSharedPasswordJsonAsync(string username, string groupName, string passwordName,
        IEnumerable<EncryptedPasswordTestModel> newlyEncryptedPasswords, IEnumerable<Guid> tags = null)
    {
        var user = testUser[username];
        var group = testGroups[groupName];

        const string query = @"
               mutation AddSharedPassword($sharedPassword: AddSharedPasswordInputType!) {
                    sharedPassword {
                        add(password: $sharedPassword) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";

        var variables = new
        {
            sharedPassword = new
            {
                name = passwordName,
                login = "login for " + passwordName,
                websiteUrl = "www.vaulteron.com",
                notes = "some notes",
                tags = tags ?? (group.Tags?.Count > 0 ? new List<Guid> { group.Tags.First().Id } : null),
                groupId = group.Id,
                encryptedPasswords = newlyEncryptedPasswords
            }
        };

        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        // Return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());

        var createdPassword =
            JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());
        ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.sharedPassword.name,
            Login = variables.sharedPassword.login,
            WebsiteURL = variables.sharedPassword.websiteUrl,
            Notes = variables.sharedPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        return json;
    }

    public async Task<JObject> EditSharedPasswordJsonAsync(string username, SharedPassword newSharedPassword,
        IEnumerable<EncryptedPasswordTestModel> newlyEncryptedPasswords)
    {
        var user = testUser[username];

        const string query = @"
                mutation EditSharedPassword($sharedPassword: EditSharedPasswordInputType!) {
                    sharedPassword { edit(password: $sharedPassword) { "
            + SharedPasswordFragment
            + @"
                } } }
               ";

        var tagsOfPassword = newSharedPassword.SharedPasswordTags == null
            ? new List<Guid>()
            : newSharedPassword.SharedPasswordTags.Select(t => t.TagId);
        var variables = new
        {
            sharedPassword = new
            {
                id = newSharedPassword.Id,
                name = newSharedPassword.Name,
                login = newSharedPassword.Login,
                websiteURL = newSharedPassword.WebsiteURL,
                notes = newSharedPassword.Notes,
                tags = tagsOfPassword,
                encryptedPasswords = newlyEncryptedPasswords,
                modifyLock = newSharedPassword.ModifyLock
            }
        };

        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));

        var createdPassword =
            JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["edit"]!.ToString());
        ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.sharedPassword.name,
            Login = variables.sharedPassword.login,
            WebsiteURL = variables.sharedPassword.websiteURL,
            Notes = variables.sharedPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        return json;
    }

    public async Task MoveSharedPasswordToAnotherGroupJsonAsync(string callingUserName, List<Guid> passwordsToMoveIds,
        string destinationGroupName, IEnumerable<object> encryptedPasswords)
    {
        var user = hptu.TestUser[callingUserName];
        var destinationGroup = hptu.TestGroups[destinationGroupName];

        const string query = @"
                  mutation ($input: MoveSharedPasswordToGroupInputType!) {
                    sharedPassword { moveToGroup(input: $input) { "
            + SharedPasswordFragment
            + @" } }
                  }";
        var variables = new
        {
            input = new
            {
                passwordIds = passwordsToMoveIds,
                destinationGroupId = destinationGroup.Id,
                encryptedPasswords
            }
        };

        await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    private async Task MoveSharedPasswordToCompanyJsonAsync(string callingUserName, List<Guid> passwordsToMoveIds,
        string destinationUserName, IEnumerable<object> encryptedPasswords)
    {
        var user = hptu.TestUser[callingUserName];
        var destinationUser = hptu.TestUser[destinationUserName];

        const string query = @"mutation ($passwordsToMoveIds: [Guid]!, $destinationUserId: Guid! , $encryptedPasswords: [EncryptedSharedPasswordInputType]) {
                    sharedPassword {
                      sharedMoveToCompany(passwordsToMoveIds: $passwordsToMoveIds, destinationUserId: $destinationUserId, encryptedPasswords: $encryptedPasswords) {
                            "
            + SharedPasswordFragment
            + @"
                      }
                    }
                  }";
        var variables = new
        {
            passwordsToMoveIds = passwordsToMoveIds,
            destinationUserId = destinationUser.Id,
            encryptedPasswords = encryptedPasswords
        };

        await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    private async Task MoveCompanyPasswordToSharedJsonAsync(string callingUserName, List<Guid> passwordsToMoveIds,
        string destinationGroupName, IEnumerable<object> encryptedPasswords)
    {
        var user = hptu.TestUser[callingUserName];
        var destinationGroup = hptu.TestGroups[destinationGroupName];

        const string query =
            @"mutation ($passwordsToMoveIds: [Guid]!, $destinationGroupId: Guid!, $encryptedPasswords: [EncryptedSharedPasswordInputType]) {
                    sharedPassword {
                      companyMoveToShared(passwordsToMoveIds: $passwordsToMoveIds, destinationGroupId: $destinationGroupId, encryptedPasswords: $encryptedPasswords) {
                            "
            + SharedPasswordFragment
            + @"
                      }
                    }
                  }";
        var variables = new
        {
            passwordsToMoveIds = passwordsToMoveIds,
            destinationGroupId = destinationGroup.Id,
            encryptedPasswords = encryptedPasswords
        };

        await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
    }

    public async Task<JObject> AddCompanyPasswordJsonAsync(string username, string passwordName,
        IEnumerable<EncryptedPasswordTestModel> newlyEncryptedPasswords, IEnumerable<Guid> tags = null)
    {
        var user = testUser[username];

        const string query = @"
               mutation AddCompanyPassword($companyPassword: AddCompanyPasswordInputType!) {
                    sharedPassword {
                        companyAdd(password: $companyPassword) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";

        var variables = new
        {
            companyPassword = new
            {
                name = passwordName,
                login = "login for " + passwordName,
                websiteUrl = "www.vaulteron.com",
                notes = "some notes",
                encryptedPasswords = newlyEncryptedPasswords
            }
        };

        var json = await hptu.MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        // Return JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());

        var createdPassword =
            JsonConvert.DeserializeObject<SharedPassword>(
                json["data"]!["sharedPassword"]!["companyAdd"]!.ToString());
        ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.companyPassword.name,
            Login = variables.companyPassword.login,
            WebsiteURL = variables.companyPassword.websiteUrl,
            Notes = variables.companyPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        return json;
    }

    public async Task SuccessfulSyncEncryptedPasswordForOtherUsersKeyPairsAsync(User callingUser,
        Guid newSharedPasswordId,
        IEnumerable<KeyPair> keyPairsToAddFor, string plaintextPassword, bool shouldFail)
    {
        const string query = @"
                mutation SyncEncryptedSharedPassword($encryptedPasswords: BulkSyncEncryptedSharedPasswordInputType!) {
                    sharedPassword {
                        bulkSyncEncryptedPassword(encryptedPasswords: $encryptedPasswords) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";

        var list = new List<object>();
        foreach (var keyPair in keyPairsToAddFor)
        {
            var encryptedPassword =
                hptu.CryptoHelper.EncryptAsymmetrical(plaintextPassword, keyPair.PublicKeyString);
            list.Add(new
            {
                encryptedPasswordString = encryptedPassword,
                keyPairId = keyPair.Id
            });
        }

        var variables = new
        {
            encryptedPasswords = new
            {
                sharedPasswordToAddTo = newSharedPasswordId,
                encryptedPasswords = list
            }
        };

        if (shouldFail)
        {
            var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                             hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables)));
            Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
        }
        else
        {
            await hptu.MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        }
    }

    public async Task SuccessfulSyncEncryptedPasswordForOtherUserAsync(User callingUser, Guid newSharedPasswordId,
        List<User> usersToAddTo, string plaintextPassword, bool doAdd, bool shouldFail)
    {
        var keyPairs = usersToAddTo.Select(u => u.KeyPairs.LastActive()).ToList();
        await SuccessfulSyncEncryptedPasswordForOtherUsersKeyPairsAsync(callingUser, newSharedPasswordId, keyPairs, plaintextPassword, shouldFail);

        if (!shouldFail)
        {
            foreach (var user in usersToAddTo)
            {
                var usersKeyPair = user.KeyPairs.LastActive();
                if (usersKeyPair.EncryptedPrivateKey != null)
                    await CheckEncryptedSharedPasswordEntityAsync(user.Email, newSharedPasswordId, plaintextPassword, usersKeyPair.EncryptedPrivateKey);
            }
        }
    }

    // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
    public static void ComparePasswordsMetaData(Password expected, Password actual, DateTime? creationDateTime = null)
    {
        Assert.True(actual != null);
        Assert.Equal(expected.Name, actual!.Name);
        Assert.Equal(expected.Login, actual.Login);
        Assert.Equal(expected.WebsiteURL, actual.WebsiteURL);
        Assert.Equal(expected.Notes, actual.Notes);

        if (creationDateTime != null)
        {
            Assert.Equal(creationDateTime.Value, actual.CreatedAt, TimeSpan.FromMinutes(5));
            Assert.Equal(creationDateTime.Value, actual.UpdatedAt, TimeSpan.FromMinutes(5));
        }
    }
    public static void ComparePasswordsMetaData(SharedPassword expected, SharedPassword actual, DateTime? creationDateTime = null)
    {
        ComparePasswordsMetaData(expected, (Password) actual, creationDateTime);

        Assert.Equal(expected.IsSavedAsOfflinePassword, actual.IsSavedAsOfflinePassword);
    }

    public async Task CheckEncryptedAccountPasswordEntityAsync(string userEmail, Guid passwordId, string originalPasswordString, string encryptedPrivateKey)
    {
        await CheckEncryptedPasswordEntityAsync(userEmail, passwordId, originalPasswordString, encryptedPrivateKey, false);
    }

    public async Task CheckEncryptedSharedPasswordEntityAsync(string userEmail, Guid passwordId, string expectedEncryptedString, string encryptedPrivateKey)
    {
        await CheckEncryptedPasswordEntityAsync(userEmail, passwordId, expectedEncryptedString, encryptedPrivateKey, true);
    }

    private async Task CheckEncryptedPasswordEntityAsync(string userEmail, Guid passwordGuid,
        string originalPasswordString, string encryptedPrivateKey, bool isSharedPassword)
    {
        var queryName = isSharedPassword ? "encryptedSharedPasswordString" : "encryptedAccountPasswordString";
        var query = @$"query {queryName}($passwordId: Guid!) {{ {queryName} (passwordId: $passwordId) }}";

        var variables = new { passwordId = passwordGuid };
        var json = await hptu.MakeGraphQLRequestAsync(userEmail, query, JsonConvert.SerializeObject(variables));

        var receivedPasswordString = json["data"]![queryName]!.Value<string>();
        var decrypted = hptu.CryptoHelper.DecryptAsymmetrical(receivedPasswordString, VaulteronSeedData.LoginPassword, encryptedPrivateKey);
        Assert.Equal(originalPasswordString, decrypted);
    }

    #endregion
}