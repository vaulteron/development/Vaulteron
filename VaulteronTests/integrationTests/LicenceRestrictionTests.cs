﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.EditModels;
using VaulteronTests.helper;
using VaulteronTests.Helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class LicenceRestrictionTests : HarryPotterTestUniverse
{
    private readonly PasswordTestService passwordTestService;
    private readonly UserTestService userTestService;
    private readonly GroupTestService groupTestService;

    public LicenceRestrictionTests(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
        userTestService = new UserTestService(logger, this);
        groupTestService = new GroupTestService(logger, this);
    }

    [Fact]
    public async Task CanDoAnythingElseWhenLimitReached()
    {
        SetMandatorLicenceCount(TestUser.ByMandator(BritishMandatorName).Count(u => u.Value.ArchivedAt == null));

        var admin = TestUser[dumbledore];
        var userToEdit = TestUser[harryPotter];

        await userTestService.EditUser(admin, new EditUserModel { Id = userToEdit.Id, Firstname = "changed " + userToEdit.Firstname });
        await userTestService.GetAdminsAsync(admin);
        await userTestService.GetMyself(admin);
        await userTestService.GrantAdminPermissions(admin, userToEdit);
        await userTestService.RevokeAdminPermissions(admin, userToEdit);

        await passwordTestService.GetAllAccountPasswordsAsync(admin);
        await passwordTestService.GetAllCompanyPasswordsAsync(dumbledore);
        await passwordTestService.GetAllSharedPasswordsAsync(dumbledore);

        var ap = await passwordTestService.AddAccountPasswordAsync(dumbledore);
        await passwordTestService.EditAccountPasswordAsync(dumbledore, ap, "changedPw");
        await passwordTestService.DeleteAccountPasswordAsync(admin, ap.Id);

        var gp = await passwordTestService.AddSharedPasswordAsync(dumbledore, hogwarts, "pw", "pw");
        await passwordTestService.EditSharedPasswordAsync(dumbledore, hogwarts, gp, "changedPW");
        await passwordTestService.DeleteSharedPasswordAsync(admin, gp.Id);

        var cp = await passwordTestService.AddCompanyPasswordAsync(dumbledore, "pw", "pw");
        await passwordTestService.DeleteCompanyPassword(admin, cp.Id);

        await groupTestService.GetGroupsByParent(dumbledore, hogwarts);

        var ng = await groupTestService.CreateGroup(dumbledore, "new Group");
        await groupTestService.EditGroup(admin, ng, "renamed new Group");

        await groupTestService.AddUserToGroupJson(admin, userToEdit.Id, ng.Id, GroupRole.GroupEditor, new List<object>());
        await groupTestService.RemoveUserFromGroup(admin, userToEdit, ng);

        await groupTestService.ArchiveGroup(admin, ng.Id);
        await groupTestService.ReactivateGroup(admin, ng.Id);
        
        Assert.True(true);
    }

    [Fact]
    public async Task CannotAddOrEditWhenNoValidLicence()
    {
        SetMandatorNoValidLicence();

        var admin = TestUser[dumbledore];
        var userToEdit = TestUser[doloresUmbridge];
        var archivedUser = TestUser[albusPotter];

        var groupToEdit = TestGroups[gryffindor];

        var ap = DbContext.AccountPasswords.First(p => p.OwnerId == admin.Id);
        var gp = DbContext.SharedPasswords.First(p => p.Group.Name == hogwarts);
        var cp = DbContext.SharedPasswords.First(p => p.CreatedById == admin.Id);


        await CAssert.ApiExceptionAsync(ErrorCode.LicenseInvalid, () => userTestService.AddUser(admin));
        await CAssert.ApiExceptionAsync(ErrorCode.LicenseInvalid, () => userTestService.GrantAdminPermissions(admin, userToEdit));
        await CAssert.ApiExceptionAsync(ErrorCode.LicenseInvalid, () => userTestService.RevokeAdminPermissions(admin, userToEdit));
        await CAssert.ApiExceptionAsync(ErrorCode.LicenseInvalid, () => userTestService.ArchiveUser(admin, userToEdit, false));
        await CAssert.ApiExceptionAsync(ErrorCode.LicenseInvalid, () => userTestService.ArchiveUser(admin, archivedUser, true));
    }

    [Fact]
    public async Task CanDoAnythingElseWhenNoValidLicence()
    {
        SetMandatorNoValidLicence();

        var admin = TestUser[dumbledore];

        var ap = DbContext.AccountPasswords.First(p => p.OwnerId == admin.Id);
        var gp = DbContext.SharedPasswords.First(p => p.Group.Name == hogwarts);
        var cp = DbContext.SharedPasswords.First(p => p.CreatedById == admin.Id);

        await userTestService.GetAdminsAsync(admin);
        await userTestService.GetMyself(admin);

        await passwordTestService.GetAllAccountPasswordsAsync(admin);
        await passwordTestService.GetAllCompanyPasswordsAsync(dumbledore);
        await passwordTestService.GetAllSharedPasswordsAsync(dumbledore);

        await passwordTestService.DeleteAccountPasswordAsync(admin, ap.Id);
        await passwordTestService.DeleteSharedPasswordAsync(admin, gp.Id);
        await passwordTestService.DeleteCompanyPassword(admin, cp.Id);

        await groupTestService.GetGroupsByParent(dumbledore, hogwarts);
        
        Assert.True(true);
    }

    #region Helper

    private void SetMandatorNoValidLicence()
    {
        Mandator.SubscriptionStatus = LicenceStatus.None;
        DbContext.Update(Mandator);
        DbContext.SaveChanges();
    }

    private void SetMandatorLicenceCount(long count = 5)
    {
        Mandator.SubscriptionUserLimit = count;
        DbContext.Update(Mandator);
        DbContext.SaveChanges();
    }

    #endregion
}