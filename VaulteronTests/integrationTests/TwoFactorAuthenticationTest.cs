using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OtpNet;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Helper;
using VaulteronWebserver.ViewModels.Users;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;
using static VaulteronTests.helper.TestEmailSenderService;

namespace VaulteronTests.integrationTests;

public class TwoFactorAuthenticationTest : HarryPotterTestUniverse
{
    private readonly UserTestService userTestService;

    public TwoFactorAuthenticationTest(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null) : base(factory, logger, hptu)
    {
        userTestService = new UserTestService(logger, this);
    }

    [Fact]
    public void TwoFATypesParsingTests()
    {
        Assert.Equal("TOTP, WebAuthn", (TwoFATypes.WebAuthn | TwoFATypes.TOTP).ToString());
        Assert.Equal(TwoFATypes.WebAuthn | TwoFATypes.TOTP, Enum.Parse<TwoFATypes>("TOTP, WebAuthn"));
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task SuccessfulTotpLogin(string userName)
    {
        User user = TestUser[userName];

        await LoginAsAsync(user);
        var totp = await EnableTotp(user);

        var ex = await Assert.ThrowsAsync<ApiClientException>(() => LoginAsAsync(user));
        Assert.Equal(ErrorCode.UnableToLogin_TwoFactorRequired, ex.ErrorCode);
        Assert.Equal("Two-Factor-Authentication required", ex.Message);

        var credentials = new TwoFactorLoginFrom
        {
            Code = totp.ComputeTotp(),
            IsPersistent = false,
            RememberClient = false
        };
        var response = await Client.PostAsync(ApiTwoFactorLoginUri, credentials.ToJson());

        // Assert
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        var json = response.ExtractJson();
        Assert.Equal(user.Email, (string)json["email"]);
        var actualUser = await userTestService.GetMyself(user, true);
        Assert.True(actualUser.TwoFATypesEnabled.HasFlag(TwoFATypes.TOTP));
        Assert.False(actualUser.TwoFATypesEnabled.HasFlag(TwoFATypes.WebAuthn));
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CantChangeAuthenticatorKeyWhenEnabled(string userName)
    {
        User user = TestUser[userName];

        await LoginAsAsync(user);

        await EnableTotp(user);

        var response = await GetTotpAuthenticatorKey();

        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CantSetEnabledWithoutOrWithOutdatedAuthenticatorKey(string userName)
    {
        User user = TestUser[userName];

        await LoginAsAsync(user);

        var response = await TwoFactorSetTotpEnabled(user, "Random", true);

        var ex = response.ExtractApiClientException();
        Assert.Equal(ErrorCode.InputInvalid, ex.ErrorCode);
        Assert.Equal("Invalid Token", ex.Message);

        var totp = await EnableTotp(user);

        response = await TwoFactorSetTotpEnabled(user, totp.ComputeTotp(DateTime.UtcNow.AddMinutes(-4)), true);

        ex = response.ExtractApiClientException();
        Assert.Equal(ErrorCode.InputInvalid, ex.ErrorCode);
        Assert.Equal("Invalid Token", ex.Message);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CantLoginWithoutOrWithOutdatedAuthenticatorKey(string userName)
    {
        User user = TestUser[userName];

        await LoginAsAsync(user);
        var totp = await EnableTotp(user);

        var ex = await Assert.ThrowsAsync<ApiClientException>(() => LoginAsAsync(user));
        Assert.Equal(ErrorCode.UnableToLogin_TwoFactorRequired, ex.ErrorCode);
        Assert.Equal("Two-Factor-Authentication required", ex.Message);

        await Assert_FailedTwoFactorLogin("Random");
        await Assert_FailedTwoFactorLogin(totp.ComputeTotp(DateTime.UtcNow.AddMinutes(-4)));

        async Task Assert_FailedTwoFactorLogin(string totpCode)
        {
            var credentials = new TwoFactorLoginFrom
            {
                Code = totpCode,
                IsPersistent = false,
                RememberClient = false
            };

            var response = await Client.PostAsync(ApiTwoFactorLoginUri, credentials.ToJson());
            var exception = response.ExtractApiClientException();

            Assert.Equal(ErrorCode.UnableToLogin_TwoFactorFailed, exception.ErrorCode);
            Assert.Equal("Two-Factor-Authentication failed", exception.Message);
        }
    }

    private async Task<Totp> EnableTotp(User user)
    {
        var response = await GetTotpAuthenticatorKey();
        var json = response.ExtractJson();

        var key = json["data"]!["user"]!["twofactor"]!["getTotpAuthenticatorKey"]!["key"]!.ToString();
        var totp = new Totp(Base32Encoding.ToBytes(key));

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);

        response = await TwoFactorSetTotpEnabled(user, totp.ComputeTotp(), true);
        json = response.ExtractJson();

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        Assert.True((json["data"]?["user"]?["twofactor"]?["enableTotp"]?["twoFATypesEnabled"]?.ToString()
                        .Contains("TOTP"))
                    ?? false);

        return totp;
    }

    private async Task<HttpResponseMessage> TwoFactorSetTotpEnabled(User user, string code, bool enabled)
    {
        const string querySetEnabled = @"
                     mutation ($code: String!) { user { twofactor {
                       enableTotp(totp: $code) {
                         twoFATypesEnabled
                       }
                     }}}
                ";
        const string querySetDisabled = @"
                     mutation { user { twofactor {
                       disableTotp() {
                         twoFATypesEnabled
                       }
                     }}}
                ";
        var variables = new { code };
        var response = await QueryGraphQLAsync(enabled ? querySetEnabled : querySetDisabled,
                                               enabled ? JsonConvert.SerializeObject(variables) : "{}");

        Assert.NotEmpty(TestEmailSender.SentEmails);
        Assert.Contains(TestEmailSender.SentEmails, m =>
                            m.EmailType == (enabled ? TestEmailSenderService.EmailType.SendEmailAddMultiFactorAuthentication : TestEmailSenderService.EmailType.SendEmailRemoveMultiFactorAuthentication)
                            && m.Content.receiverName == user.Firstname + " " + user.Lastname
                            && m.Content.userEmail == user.Email
        );

        return response;
    }

    private async Task<HttpResponseMessage> GetTotpAuthenticatorKey()
    {
        const string queryGetKey = @"
                     mutation { user { twofactor {
                       getTotpAuthenticatorKey {
       	                 key
                         authenticatorString
                       }
                     }}}
                ";
        return await QueryGraphQLAsync(queryGetKey);
    }
}