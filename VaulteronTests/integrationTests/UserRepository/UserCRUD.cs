﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Authorization;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.EditModels;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.IntegrationTests.UserRepository;

/// <summary>
/// Test all CRUD functionality with Groups as well as all additional features with Groups (like user access)
/// </summary>
public class UserCRUD : HarryPotterTestUniverse
{
    private readonly UserTestService userTestService;
    private readonly GroupTestService groupTestService;
    private readonly PasswordTestService passwordTestService;

    public UserCRUD(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null) : base(factory, logger, hptu)
    {
        userTestService = new UserTestService(logger, this);
        groupTestService = new GroupTestService(logger, this);
        passwordTestService = new PasswordTestService(logger, this);
    }

    #region Myself (me-routes)

    [Fact]
    public async Task GetMyself()
    {
        var allUnarchivedUsers = TestUser.Where(tu => !tu.Value.IsArchived());

        foreach (var (_, expectedUser) in allUnarchivedUsers)
        {
            var actualUser = await userTestService.GetMyself(expectedUser);

            Assert.NotNull(actualUser);
            CompareUsers(expectedUser, actualUser);
        }
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    [InlineData(ronWeasly)]
    public async Task VerifyOwnEmail(string username)
    {
        var user = TestUser[username];

        await ConfirmEmail(user);

        var myselfAfter = await userTestService.GetMyself(user);
        Assert.True(myselfAfter.EmailConfirmed);
    }

    private async Task ConfirmEmail(User user)
    {
        var myselfBefore = await userTestService.GetMyself(user);
        Assert.False(myselfBefore.EmailConfirmed);

        var responseMessageResendConfirmationEmail = await Client.PostAsync(ApiRequestEmailConfirmationUri, new StringContent(""));
        Assert.True(responseMessageResendConfirmationEmail.IsSuccessStatusCode);
        Assert.NotEmpty(TestEmailSender.SentEmails);
        var dataSendConfirmEmailAsync = TestEmailSender.SentEmails.Single(e => e.EmailType == TestEmailSenderService.EmailType.SendConfirmEmailAsync);
        TestEmailSender.SentEmails.Remove(dataSendConfirmEmailAsync);
        var url = ConversionHelper.ConstructUrlWithQueryParameters(
            ApiEmailVerificationUri,
            new Dictionary<string, string>
            {
                { "userId", dataSendConfirmEmailAsync.Content.registeredUserId.ToString() },
                { "token", dataSendConfirmEmailAsync.Content.emailConfirmationToken },
            });
        var responseMessage = await Client.GetAsync(url);
        Assert.Equal(HttpStatusCode.Redirect, responseMessage.StatusCode);
    }

    #endregion

    #region change login password

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    [InlineData(ronWeasly)]
    private async Task UserCanChangeOwnLoginPassword(string username)
    {
        var user = TestUser[username];

        const string newPasswordString = LoginPassword + "-new";
        await userTestService.ChangeLoginPassword(user, newPasswordString);

        await LoginAsAsync(user.Email, newPasswordString);
        Assert.True(true); // The last LOC should not throw
    }

    [Theory]
    [InlineData("",
                "abc", LoginPassword, "new-login", ErrorCode.InputInvalid)] // Bad public Key -> empty
    [InlineData(" ",
                "abc", LoginPassword, "new-login", ErrorCode.InputInvalid)] // Bad public Key -> empty
    [InlineData("abc-----END RSA PUBLIC KEY-----",
                "abc", LoginPassword, "new-login", ErrorCode.InputInvalid)] // Bad public Key -> start missing
    [InlineData("-----BEGIN RSA PUBLIC KEY-----abc",
                "abc", LoginPassword, "new-login", ErrorCode.InputInvalid)] // Bad public Key -> end missing
    [InlineData("-----BEGIN RSA PUBLIC KEY-----abc-----END RSA PUBLIC KEY-----\r\n",
                "", LoginPassword, "new-login", ErrorCode.InputInvalid)] // Bad encrypted private key -> empty
    [InlineData("-----BEGIN RSA PUBLIC KEY-----abc-----END RSA PUBLIC KEY-----\r\n",
                "abc", "", "new-login", ErrorCode.InputInvalid)] // Bad current password -> empty
    [InlineData("-----BEGIN RSA PUBLIC KEY-----abc-----END RSA PUBLIC KEY-----\r\n",
                "abc", "wrong current password", "new-login",
                ErrorCode.UnableToChangeLoginPassword)] // Bad current password -> wrong
    [InlineData("-----BEGIN RSA PUBLIC KEY-----abc-----END RSA PUBLIC KEY-----\r\n",
                "abc", "wrong current password", "", ErrorCode.InputInvalid)] // Bad new password -> wrong
    // The next one is disabled. will be enabled through a policy later on
    // [InlineData("-----BEGIN RSA PUBLIC KEY-----abc-----END RSA PUBLIC KEY-----\r\n",
    //     "abc", LoginPassword, LoginPassword)] // Bad new password -> same as current
    private async Task UserCannotChangeOwnLoginPasswordWithBadInputData(string publicKeyString, string encryptedPrivateKey, string currentPassword, string newPassword, ErrorCode expectedErrorCode)
    {
        var user = TestUser[dumbledore];

        var prevUserPass = user.PasswordHash;

        var userGroupsExpected = user.Admin
            ? TestGroups.ByMandator(BritishMandatorName).Values.ToList()
            : user.UserGroups.SelectMany(g => g.Group.ChildGroups.Append(g.Group)).ToList();

        var oldKeyPair = user.KeyPairs.LastActive();
        var newKeyPair = CryptoHelper.CreateKeyPair();

        var encryptedSharedPasswordTuples = passwordTestService.GetFakeDecryptedSharedPasswordsForGroups(user, userGroupsExpected).ToList();
        var encryptedCompanyPasswordsTuples = passwordTestService.GetFakeEncryptedCompanyPasswords(user, true).ToList();
        var sharedPasswordTuples = encryptedSharedPasswordTuples.Concat(encryptedCompanyPasswordsTuples);
        var encryptedSharedPasswords = sharedPasswordTuples.Select(pw => new
        {
            sharedPasswordId = pw.Id,
            encryptedPasswordString = pw.encryptedString
        });

        var encryptedAccountPasswordTuples = passwordTestService.GetEncryptedAccountPasswords(user, oldKeyPair, newKeyPair.PublicKey).ToList();
        var encryptedAccountPasswords = encryptedAccountPasswordTuples.Select(eap => new
        {
            accountPasswordId = eap.Id,
            encryptedPasswordString = eap.encryptedString
        });

        var previousPasswordHashEncryptedWithPublicKey = CryptoHelper.EncryptAsymmetrical(currentPassword, newKeyPair.PublicKey);

        const string query = @"
                mutation ChangeOwnLoginPassword(
                        $publicKeyString: String!,
                        $encryptedPrivateKey: String!,
                        $previousPasswordHashEncryptedWithPublicKey: String!,
                        $currentPassword: String!,
                        $newPassword: String!,
                        $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!
                        $encryptedAccountPasswords: [AddEncryptedAccountPasswordWithoutPublicKeyInputType]!) {
                    user {
                        changeOwnLoginPassword(
                                publicKeyString: $publicKeyString,
                                encryptedPrivateKey: $encryptedPrivateKey,
                                previousPasswordHashEncryptedWithPublicKey: $previousPasswordHashEncryptedWithPublicKey,
                                currentPassword: $currentPassword,
                                newPassword: $newPassword,
                                encryptedSharedPasswords: $encryptedSharedPasswords,
                                encryptedAccountPasswords: $encryptedAccountPasswords) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var variables = new
        {
            publicKeyString,
            encryptedPrivateKey,
            previousPasswordHashEncryptedWithPublicKey,
            currentPassword,
            newPassword,
            encryptedSharedPasswords,
            encryptedAccountPasswords
        };
        var exception = await Assert.ThrowsAsync<ApiClientException>(
            async () => await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(expectedErrorCode, exception.ErrorCode);

        Assert.Equal(prevUserPass, user.PasswordHash);
        Assert.Equal(prevUserPass, DbContext.Users.CurrentUser(user.Id).Single().PasswordHash);
    }

    [Theory]
    [InlineData(LoginPassword, "some new password")]
    private async Task UserCannotChangeOwnLoginPasswordWithBadInputDataEncryptions(string currentPassword, string newPassword)
    {
        var user = TestUser[dumbledore];

        var oldKeyPair = user.KeyPairs.LastActive();
        var prevUserPass = user.PasswordHash;

        var userGroupsExpected = user.Admin
            ? TestGroups.ByMandator(BritishMandatorName).Values.ToList()
            : user.UserGroups.SelectMany(g => g.Group.ChildGroups.Append(g.Group)).ToList();

        var encryptedSharedPasswords = userGroupsExpected
            .SelectMany(g => g.SharedPasswords)
            .Select(_ => new
            {
                sharedPasswordId = Guid.NewGuid(),
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", oldKeyPair.PublicKeyString)
            });

        var encryptedAccountPasswords = DbContext.Users
            .Include(u => u.AccountPasswords)
            .ById(user.Id)
            .Single()
            .AccountPasswords.Select(ap => new
            {
                accountPasswordId = Guid.NewGuid(),
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", oldKeyPair.PublicKeyString)
            });

        var keyPair = CryptoHelper.CreateKeyPair();

        var previousPasswordHashEncryptedWithPublicKey = CryptoHelper.EncryptAsymmetrical(currentPassword, keyPair.PublicKey);

        const string query = @"
                mutation ChangeOwnLoginPassword(
                        $publicKeyString: String!,
                        $encryptedPrivateKey: String!,
                        $currentPassword: String!,
                        $newPassword: String!,
                        $previousPasswordHashEncryptedWithPublicKey: String!,
                        $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!
                        $encryptedAccountPasswords: [AddEncryptedAccountPasswordWithoutPublicKeyInputType]!) {
                    user {
                        changeOwnLoginPassword(
                                publicKeyString: $publicKeyString,
                                encryptedPrivateKey: $encryptedPrivateKey,
                                previousPasswordHashEncryptedWithPublicKey: $previousPasswordHashEncryptedWithPublicKey,
                                currentPassword: $currentPassword,
                                newPassword: $newPassword,
                                encryptedSharedPasswords: $encryptedSharedPasswords,
                                encryptedAccountPasswords: $encryptedAccountPasswords) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var variables = new
        {
            publicKeyString = keyPair.PublicKey,
            encryptedPrivateKey = keyPair.EncryptedPrivateKey,
            previousPasswordHashEncryptedWithPublicKey,
            currentPassword,
            newPassword,
            encryptedSharedPasswords,
            encryptedAccountPasswords
        };
        var exception = await Assert.ThrowsAsync<ApiClientException>(
            async () => await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);

        Assert.Equal(prevUserPass, user.PasswordHash);
        Assert.Equal(prevUserPass, DbContext.Users.CurrentUser(user.Id).Single().PasswordHash);
    }

    #endregion

    #region Admin resends Account-activation-link for another user

    [Theory]
    [InlineData(severusSnape, harryPotter)]
    [InlineData(harryPotter, harryPotter)]
    [InlineData(dracoMalfoy, harryPotter)]
    public async Task NormalUserCannotResendAccountActivationLink(string username, string targetUsername)
    {
        var user = TestUser[username];
        var targetUser = TestUser[targetUsername];

        var ex = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.ResendAccountActivationLinkAsync(user, targetUser));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, ex.ErrorCode);
    }

    [Fact]
    public async Task AdminCannotResendAccountActivationLinkWhenAlreadyActivated()
    {
        var admin = TestUser[dumbledore];
        var targetUser = TestUser[harryPotter];

        await ConfirmEmail(targetUser);

        var ex = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.ResendAccountActivationLinkAsync(admin, targetUser));
        Assert.Equal(ErrorCode.InputInvalid, ex.ErrorCode);
    }

    [Theory]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    [InlineData(albusPotter)]
    public async Task AdminCanResendAccountActivationLink(string targetUsername)
    {
        var admin = TestUser[dumbledore];
        var targetUser = TestUser[targetUsername];

        await userTestService.ResendAccountActivationLinkAsync(admin, targetUser);
        Assert.True(true); // The last LOC should not throw
    }

    #endregion

    #region Adding a user

    [Fact]
    public async Task AdminCanAddNewAdmin()
    {
        var admin = TestUser[dumbledore];

        var newUserData = new AddUserTestModel
        {
            firstname = "newUser",
            lastname = "newUser lastname",
            email = "newuser@vaulteron.com",
            sex = Sex.Unset.ToString(),

            admin = true,
        };
        var newUser = await userTestService.AddUserWithCorrectEncryptions(admin, newUserData);

        var tmpUser = new User
        {
            Firstname = newUserData.firstname,
            Lastname = newUserData.lastname,
            Email = newUserData.email,
            Sex = Sex.Unset,
            Admin = newUserData.admin,
            CreatedAt = DateTime.UtcNow,
            UpdatedAt = DateTime.UtcNow,
        };
        CompareUsers(tmpUser, newUser);
    }


    [Theory]
    // Note: emails are very forgiving (http://haacked.com/archive/2007/08/21/i-knew-how-to-validate-an-email-address-until-i.aspx/)
    [InlineData("", "valid", "valid", "Unset")]
    [InlineData("a", "valid", "valid", "Unset")]
    [InlineData("a@a.co", "", "valid", "Unset")]
    [InlineData("a@a.co", "1", "valid", "Unset")]
    [InlineData("a@a.co", "12", "", "Unset")]
    [InlineData("a@a.co", "12", "1", "Unset")]
    [InlineData("a@a.co", "12", "12", "bad")]
    public async Task AdminCannotAddNewUserWithBadInputData(string email, string firstname, string lastname, string sex)
    {
        var admin = TestUser[dumbledore];

        var newUserData = new AddUserTestModel
        {
            firstname = firstname,
            lastname = lastname,
            email = email,
            sex = sex,
            admin = true,
        };
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.AddUserWithCorrectEncryptions(admin, newUserData));

        if (exception.ErrorCode == ErrorCode.GraphQlError)
        {
            Assert.Equal("Unable to convert 'bad' to the scalar type 'Sex'", exception.Message);
            return;
        }

        Assert.Equal(ErrorCode.UnableToRegister, exception.ErrorCode);
        Assert.Equal("Unable to register new user", exception.Message);
        foreach (var reason in exception.Reasons) Assert.Contains("This is not a valid value for", reason);
    }

    [Theory]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    [InlineData(dracoMalfoy)]
    public async Task NormalUserCannotAddUser(string normalUserName)
    {
        var normalUser = TestUser[normalUserName];

        const string query = @"mutation AddUser($user: AddUserInputType!) { user { add(user: $user) { id } } }";
        var keyPair = CryptoHelper.CreateKeyPair();
        const string tmpPassword = "tmpPassword";
        var newUserData = new AddUserTestModel
        {
            firstname = "newUser",
            lastname = "newUser lastname",
            email = "newuser@vaulteron.com",
            sex = Sex.Unset.ToString(),
            admin = true,
            publicKey = keyPair.PublicKey,
            newTemporaryLoginPassword = tmpPassword,
            newTemporaryLoginPasswordHashed = CryptoHelper.ComputePasswordHash(tmpPassword),
            encryptedPrivateKey = CryptoHelper.EncryptSymmetrically(keyPair.PrivateKey, tmpPassword)
        };
        var variables = new { user = newUserData };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, new[] { hogwarts }, new[] { hogwarts, gryffindor, hufflepuff, ravenclaw, slytherin, roomOfRequirement })]
    [InlineData(dumbledore, new[] { gryffindor })]
    [InlineData(dumbledore, new[] { gryffindor, slytherin, hufflepuff })]
    public async Task UserCanBeAddedToGroupsWhenAdded(string callingUsername, string[] groupsToBeAddedTo, string[] expectedUserGroups = null)
    {
        expectedUserGroups ??= groupsToBeAddedTo;

        var admin = TestUser[callingUsername];
        var groupIdsToBeAddedTo = groupsToBeAddedTo.Select(groupname => TestGroups[groupname].Id).ToList();
        var userGroupsExpected = expectedUserGroups.Select(groupname => TestGroups[groupname]).ToList();

        var keyPair = CryptoHelper.CreateKeyPair();

        var encryptedSharedPasswords =
            passwordTestService.GetFakeDecryptedSharedPasswordsForGroups(keyPair.PublicKey, userGroupsExpected)
                .Select(enc => new { sharedPasswordId = enc.Id, encryptedPasswordString = enc.encryptedString })
                .ToList();

        const string query = @"
                mutation AddUser($user: AddUserInputType!) {
                    user {
                        add(user: $user) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        const string tmpPassword = "tmpPassword";
        var newUserData = new AddUserTestModel
        {
            firstname = "newUser",
            lastname = "newUser lastname",
            email = "newuser@vaulteron.com",
            sex = Sex.Unset.ToString(),
            admin = false,
            groupsToBeAddedTo = groupIdsToBeAddedTo,
            publicKey = keyPair.PublicKey,
            newTemporaryLoginPassword = tmpPassword,
            newTemporaryLoginPasswordHashed = CryptoHelper.ComputePasswordHash(tmpPassword),
            encryptedPrivateKey = CryptoHelper.EncryptSymmetrically(keyPair.PrivateKey, tmpPassword),
            encryptedSharedPasswords = encryptedSharedPasswords
        };
        var variables = new { user = newUserData };
        var json = await MakeGraphQLRequestAsync(admin.Email, query, JsonConvert.SerializeObject(variables));
        var newUser = JsonConvert.DeserializeObject<User>(json["data"]!["user"]!["add"]!.ToString())!;

        var tmpUser = new User
        {
            Firstname = newUserData.firstname,
            Lastname = newUserData.lastname,
            Email = newUserData.email,
            Sex = Sex.Unset,
            Admin = newUserData.admin,
            CreatedAt = DateTime.UtcNow,
            UpdatedAt = DateTime.UtcNow,
        };
        CompareUsers(tmpUser, newUser);
        foreach (var userGroup in newUser.UserGroups)
        {
            Assert.Contains(userGroup.GroupId, userGroupsExpected.Select(g => g.Id));
        }
    }

    [Theory]
    [InlineData("test_admin", true)]
    [InlineData("test_no_group", false)]
    [InlineData("test_gryffindor", false, new[] { gryffindor })]
    [InlineData("test_hogwarts", false, new[] { hogwarts })]
    [InlineData("test_gryffindor_hogwarts", false, new[] { hogwarts, gryffindor })]
    public async Task ActivateAccountAfterAddUser(string newUserName, bool shouldBeAdmin, string[] groupNamesToAddTo = null)
    {
        var admin = TestUser[dumbledore];
        List<Group> groupsIdToBeAddedTo = null;
        if (groupNamesToAddTo != null)
        {
            var groups = DbContext.Groups
                .RecursionBase(admin.MandatorId)
                .Where(g => g.UserGroups.Any(ug => groupNamesToAddTo.Contains(ug.Group.Name)))
                .IncludeSharedPasswords()
                .AsEnumerable();

            groupsIdToBeAddedTo = GroupAuthorizations.IncludeSubGroups(groups).ToList();
        }

        #region add new admin

        var newUserData = new AddUserTestModel
        {
            firstname = newUserName,
            lastname = $"{newUserName} lastname",
            email = $"{newUserName}@vaulteron.com",
            sex = Sex.Unset.ToString(),
            admin = shouldBeAdmin,
        };
        var newUser = await userTestService.AddUserWithCorrectEncryptions(admin, newUserData, groupsIdToBeAddedTo);

        #endregion

        await userTestService.ActivateLastCreatedAccount();

        #region verify passwords can be obtained

        TestUser[newUser.Email] = newUser;

        var companyPasswords = await passwordTestService.GetAllCompanyPasswordsAsync(newUser.Email);
        foreach (var companyPassword in companyPasswords)
            await passwordTestService.GetSingleSharedPasswordStringAsync(newUser, companyPassword.Id);

        var groupPasswords = await passwordTestService.GetAllSharedPasswordsAsync(newUser.Email);
        foreach (var sharedPassword in groupPasswords)
            await passwordTestService.GetSingleSharedPasswordStringAsync(newUser, sharedPassword.Id);

        Assert.True(true); // The last LOCs should not throw

        #endregion
    }

    #endregion

    #region Archiving a user

    [Theory]
    [InlineData(dumbledore, horaceSlughorn)]
    [InlineData(dumbledore, cedricDiggory)]
    [InlineData(doloresUmbridge, dumbledore)]
    public async Task AdminCanArchiveUser(string adminUserName, string userToBeArchivedName)
    {
        var admin = TestUser[adminUserName];
        var userToBeArchived = TestUser[userToBeArchivedName];

        var modifiedUser = await userTestService.ArchiveUser(admin, userToBeArchived);

        CompareUsers(userToBeArchived, modifiedUser, false);
        Assert.NotNull(modifiedUser!.ArchivedAt);
        Assert.Equal(DateTime.UtcNow, modifiedUser.ArchivedAt!.Value, TimeSpan.FromMinutes(5));

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => LoginAsAsync(userToBeArchived)); // Archived User isn't allowed to login
        Assert.Equal(ErrorCode.UnableToLogin, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dracoMalfoy, dumbledore)]
    [InlineData(severusSnape, dumbledore)] // But how did he?
    public async Task NormalUsersCannotArchiveUser(string normalUserName, string userToBeArchivedName)
    {
        var normalUser = TestUser[normalUserName];
        var userToBeArchived = TestUser[userToBeArchivedName];

        const string query = @"
                mutation ArchiveUser($id: Guid!) {
                    user {
                        archive(id: $id) {
                            id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                            publicKey { publicKeyString }
                            groups { id }
                            usergroups { groupId }
                        }
                    }
                }
            ";
        var variables = new { id = userToBeArchived.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(normalUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(albusPotter)]
    public async Task ArchivedUserCannotLogin(string archivedUserName)
    {
        var user = TestUser[archivedUserName];
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => LoginAsAsync(user));

        Assert.Equal(ErrorCode.UnableToLogin, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, albusPotter)]
    public async Task CannotGrantAdminToArchivedUser(string adminUserName, string archivedUserName)
    {
        var callingUser = TestUser[adminUserName];
        var targetUser = TestUser[archivedUserName];
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.GrantAdminPermissions(callingUser, targetUser));

        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    #endregion

    #region Get user by Id

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, ronWeasly)]
    [InlineData(dumbledore, viktorKrum)]
    [InlineData(viktorKrum, viktorKrum)]
    [InlineData(viktorKrum, dumbledore)]
    [InlineData(viktorKrum, harryPotter)]
    [InlineData(viktorKrum, ronWeasly)]
    private async Task GetUserById(string callingUsername, string targetUsername)
    {
        var callingUser = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];

        const string query = @"
                query UsersQuery($userIds: [Guid]!) 
	                { users(userIds: $userIds) {
                        id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                        publicKey { publicKeyString }
                        groups { id }
                        usergroups { groupId }
                    }
                }
            ";
        var variables = new
        {
            userIds = new[] { targetUser.Id },
        };
        var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var users = JsonConvert.DeserializeObject<List<User>>(json["data"]!["users"]!.ToString());

        Assert.NotNull(users);
        Assert.Single(users);
        var returnedUser = users![0];
        CompareUsers(targetUser, returnedUser);
    }

    [Theory]
    [InlineData(dumbledore, albusPotter)]
    private async Task AdminsCanListArchivedUser(string callingUsername, string targetUsername)
    {
        await GetUserById(callingUsername, targetUsername);
    }

    [Theory]
    [InlineData(viktorKrum, albusPotter)]
    private async Task NonAdminsCannotListArchivedUser(string callingUsername, string targetUsername)
    {
        var callingUser = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];

        const string query = @"
                query UsersQuery($userIds: [Guid]!) 
	                { users(userIds: $userIds) {
                        id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                        publicKey { publicKeyString }
                        groups { id }
                        usergroups { groupId }
                    }
                }
            ";
        var variables = new
        {
            userIds = new[] { targetUser.Id },
        };
        var json = await MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables));
        var users = JsonConvert.DeserializeObject<List<User>>(json["data"]!["users"]!.ToString());

        Assert.Empty(users);
    }

    #endregion

    #region List all Users

    [Theory]
    [InlineData(filiusFlitwick)]
    [InlineData(lunaLovegood)]
    public async Task NonAdminsCanListNotArchivedUsers(string userName)
    {
        var user = TestUser[userName];

        const string query = @"
                query {
                  users {
                    id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                    publicKey { publicKeyString }
                    groups { id }
                    usergroups { groupId }
                  }
                }
            ";
        var variables = new { };

        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var users = JsonConvert.DeserializeObject<List<User>>(json["data"]!["users"]!.ToString());

        var mandatorUsers = TestUser.ByMandator(BritishMandatorName).Values.ByArchived(false).ToList();

        Assert.NotNull(users);
        Assert.Equal(mandatorUsers.Count, users!.Count);
        Assert.All(users.Concat(mandatorUsers).GroupBy(u => u.Id), // matches users to mandatorUsers by Id
                   g =>
                   {
                       Assert.Equal(2, g.Count()); // there are exactly 2 for every Id
                       CompareUsers(g.First(), g.Last()); // compare those 2 users
                   });
    }

    [Theory]
    [InlineData(dumbledore)]
    public async Task AdminsCanListUsersIncludingArchived(string userName)
    {
        var user = TestUser[userName];

        const string query = @"
                query {
                  users {
                    id firstname lastname sex email admin emailConfirmed updatedAt createdAt archivedAt
                    publicKey { publicKeyString }
                    groups { id }
                    usergroups { groupId }
                  }
                }
            ";
        var variables = new { };

        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var users = JsonConvert.DeserializeObject<List<User>>(json["data"]!["users"]!.ToString());

        var mandatorUsers = TestUser.ByMandator(BritishMandatorName);

        Assert.NotNull(users);
        Assert.Equal(mandatorUsers.Count, users!.Count);
        Assert.All(users.Concat(mandatorUsers.Values).GroupBy(u => u.Id),
                   g =>
                   {
                       Assert.Equal(2, g.Count());
                       CompareUsers(g.First(), g.Last());
                   });
    }

    #endregion

    #region Edit a user

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, severusSnape)]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminCanEditUsers(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var changeUser = TestUser[changeUserName];

        changeUser.Firstname = "changed" + changeUser.Firstname;
        changeUser.Lastname = "changed" + changeUser.Lastname;

        var resUser = await userTestService.EditUser(user, new EditUserModel
        {
            Id = changeUser.Id,
            Firstname = changeUser.Firstname,
            Lastname = changeUser.Lastname,
        });

        CompareUsers(changeUser, resUser);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, severusSnape)]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminCannotEditUsersWhenNoDataIsChanged(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var changeUser = TestUser[changeUserName];

        var apiClientException = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.EditUser(user,
                                                                                                             new EditUserModel
                                                                                                             {
                                                                                                                 Id = changeUser.Id,
                                                                                                             }));
        Assert.Equal(ErrorCode.InputInvalid, apiClientException.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, severusSnape)]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminCanChangeEmailAddressOfUsers(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var targetUser = TestUser[changeUserName];

        const string newEmail = "testNewEmail@vaulteron.com";

        #region edit the user

        var resUser = await userTestService.EditUser(user, new EditUserModel
        {
            Id = targetUser.Id,
            Email = newEmail
        });
        Assert.NotNull(resUser);
        Assert.Equal(targetUser.Email, resUser!.Email);

        #endregion

        #region confirm email change

        Assert.NotEmpty(TestEmailSender.SentEmails);
        var emailChangedMailData = TestEmailSender.SentEmails.Last();
        TestEmailSender.SentEmails.Remove(emailChangedMailData);
        Assert.Equal(targetUser.Email, (string) emailChangedMailData.Content.oldEmail);
        Assert.Equal(newEmail, (string) emailChangedMailData.Content.newEmail);
        Assert.Equal(targetUser.Id, emailChangedMailData.Content.targetUserId);
        var url = ConversionHelper.ConstructUrlWithQueryParameters(EmailChangedVerificationUri,
                                                                   new Dictionary<string, string>
                                                                   {
                                                                       { "token", emailChangedMailData.Content.changeEmailTokenAsync },
                                                                       { "newEmail", newEmail },
                                                                       { "userId", targetUser.Id.ToString() },
                                                                   });
        var responseMessage = await Client.GetAsync(url);
        Assert.Equal(HttpStatusCode.Redirect, responseMessage.StatusCode);
        Assert.DoesNotContain(responseMessage.Headers, h => h.Value.First().Contains("/error"));

        #endregion

        #region verify that user's email has changed

        var updatedUser = await userTestService.GetMyself(newEmail);
        Assert.NotNull(updatedUser);
        Assert.Equal(newEmail, updatedUser!.Email);
        var serviceProvider = Factory.ServiceCollection.BuildServiceProvider();
        var refreshedDbContext = serviceProvider.GetRequiredService<VaulteronContext>();
        var dbUser = refreshedDbContext.Users.ById(updatedUser.Id).Single();
        Assert.Equal(newEmail, dbUser.UserName);

        #endregion
    }

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, severusSnape)]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminCannotChangeEmailAddressOfUsersWhenAlreadyExistingPart1(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var targetUser = TestUser[changeUserName];

        var newEmail = TestUser[hermioneGranger].Email; // someone not in the list of InlineData

        var apiClientException = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.EditUser(user,
                                                                                                             new EditUserModel
                                                                                                             {
                                                                                                                 Id = targetUser.Id,
                                                                                                                 Email = newEmail
                                                                                                             }));
        Assert.Equal(ErrorCode.InputInvalid, apiClientException.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, dumbledore)]
    [InlineData(dumbledore, severusSnape)]
    [InlineData(dumbledore, harryPotter)]
    public async Task AdminCannotChangeEmailAddressOfUsersWhenAlreadyExistingPart2(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var targetUser = TestUser[changeUserName];

        var newEmail = TestUser[harryPotter].Email;
        const string tmpEmail = "someFakeEmail@vaulteron.com";

        #region edit the user

        var resUser = await userTestService.EditUser(user, new EditUserModel
        {
            Id = targetUser.Id,
            Email = tmpEmail
        });
        Assert.NotNull(resUser);
        Assert.Equal(targetUser.Email, resUser!.Email);

        #endregion

        #region confirm email change

        Assert.NotEmpty(TestEmailSender.SentEmails);
        var emailChangedMailData = TestEmailSender.SentEmails.Last();
        TestEmailSender.SentEmails.Remove(emailChangedMailData);
        Assert.Equal(targetUser.Email, (string) emailChangedMailData.Content.oldEmail);
        Assert.Equal(tmpEmail, (string) emailChangedMailData.Content.newEmail);
        Assert.Equal(targetUser.Id, emailChangedMailData.Content.targetUserId);
        var url = ConversionHelper.ConstructUrlWithQueryParameters(EmailChangedVerificationUri,
                                                                   new Dictionary<string, string>
                                                                   {
                                                                       { "token", emailChangedMailData.Content.changeEmailTokenAsync },
                                                                       { "newEmail", newEmail },
                                                                       { "userId", targetUser.Id.ToString() },
                                                                   });
        var responseMessage = await Client.GetAsync(url);

        #endregion

        #region verify

        Assert.Equal(HttpStatusCode.Redirect, responseMessage.StatusCode);
        Assert.Contains(responseMessage.Headers, h => h.Value.First().Contains("/error"));

        #endregion
    }

    [Theory]
    [InlineData(severusSnape, dumbledore)]
    [InlineData(harryPotter, dracoMalfoy)]
    public async Task NormalUserCannotEditEmailOfAnotherUser(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var changeUser = TestUser[changeUserName];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.EditUser(user,
                                                                                                    new EditUserModel
                                                                                                    {
                                                                                                        Id = changeUser.Id,
                                                                                                        Firstname = changeUser.Firstname,
                                                                                                        Lastname = changeUser.Lastname,
                                                                                                        Email = "changed" + changeUser.Email,
                                                                                                    }));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(severusSnape, dumbledore)]
    [InlineData(harryPotter, dracoMalfoy)]
    public async Task NormalUserCannotEditOtherUsersUncriticalProperties(string userName, string changeUserName)
    {
        var user = TestUser[userName];
        var changeUser = TestUser[changeUserName];

        changeUser.Firstname = "changed" + changeUser.Firstname;
        changeUser.Lastname = "changed" + changeUser.Lastname;
        changeUser.Sex = changeUser.Sex != Sex.Male ? Sex.Male : Sex.Female;

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         userTestService.EditUser(user, new EditUserModel
                                                                         {
                                                                             Id = changeUser.Id,
                                                                             Firstname = changeUser.Firstname,
                                                                             Lastname = changeUser.Lastname,
                                                                             Email = changeUser.Email,
                                                                         }));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    public async Task NormalUserCanEditHimselfUncriticalProperties(string userName)
    {
        var user = TestUser[userName];

        user.Firstname = "changed" + user.Firstname;
        user.Lastname = "changed" + user.Lastname;

        var resUser = await userTestService.EditUser(user, new EditUserModel
        {
            Id = user.Id,
            Firstname = user.Firstname,
            Lastname = user.Lastname,
        });

        CompareUsers(user, resUser);
    }

    [Theory]
    [InlineData(severusSnape)]
    [InlineData(harryPotter)]
    public async Task NormalUserCannotEditHisOwnEmail(string userName)
    {
        var user = TestUser[userName];

        // TODO: opinion: this route should throw an error when this happens and not just ignore it
        var resUser = await userTestService.EditUser(user, new EditUserModel
        {
            Id = user.Id,
            Email = "changed" + user.Email,
        });

        CompareUsers(user, resUser);
    }

    [Fact]
    public async Task AdminCanGrantOtherUserAdminPermissions()
    {
        var callingAdmin = TestUser[dumbledore];
        var targetNonAdmin = TestUser[harryPotter];

        var updatedUser = await userTestService.GrantAdminPermissions(callingAdmin, targetNonAdmin);

        targetNonAdmin.Admin = true;
        CompareUsers(targetNonAdmin, updatedUser);
    }

    [Fact]
    public async Task AdminCanRevokeOtherUserAdminPermissions()
    {
        // Also check if all the admin-things are now deleted, like company-passwords, shared passwords from groups user is not in, etc

        var callingAdmin = TestUser[dumbledore];
        var targetAdmin = TestUser[doloresUmbridge];

        var updatedUser = await userTestService.RevokeAdminPermissions(callingAdmin, targetAdmin);

        targetAdmin.Admin = false;
        CompareUsers(targetAdmin, updatedUser);

        // Now check if this user cannot access admin-related stuff anymore
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => groupTestService.CreateGroup(doloresUmbridge, "test"));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Fact]
    public async Task NormalUserCannotGrantOtherUserAdminPermissions()
    {
        var callingAdmin = TestUser[ronWeasly];
        var targetNonAdmin = TestUser[harryPotter];

        var exception = await Assert.ThrowsAsync<ApiClientException>(async () => await userTestService.GrantAdminPermissions(callingAdmin, targetNonAdmin));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Fact]
    public async Task NormalUserCannotRevokeOtherUserAdminPermissions()
    {
        var callingNonAdmin = TestUser[harryPotter];
        var targetAdmin = TestUser[dumbledore];

        var exception = await Assert.ThrowsAsync<ApiClientException>(async () => await userTestService.RevokeAdminPermissions(callingNonAdmin, targetAdmin));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    #endregion

    #region encryptionErrors

    [Theory]
    [InlineData(dumbledore, new[] { gryffindor })]
    [InlineData(dumbledore, new[] { gryffindor, slytherin, hufflepuff })]
    public async Task CannotAddUserWithMissingEncryptions(string callingUsername, string[] groupNamesToBeAddedTo)
    {
        var admin = TestUser[callingUsername];
        var groupsToBeAddedTo = groupNamesToBeAddedTo.Select(g => TestGroups[g]);

        var encryptedSharedPasswords = groupsToBeAddedTo.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", admin.KeyPairs.LastActive().PublicKeyString)
            });

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.AddUser(admin, null, groupsToBeAddedTo, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, new[] { gryffindor })]
    [InlineData(dumbledore, new[] { gryffindor, slytherin, hufflepuff })]
    public async Task CannotAddUserWithDuplicateEncryptions(string callingUsername, string[] groupNamesToBeAddedTo)
    {
        var admin = TestUser[callingUsername];
        var groupsToBeAddedTo = groupNamesToBeAddedTo.Select(g => TestGroups[g]);

        var encryptedSharedPasswords = groupsToBeAddedTo.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                     admin.KeyPairs.LastActive().PublicKeyString)
            })
            .ToList();

        encryptedSharedPasswords.Add(encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.AddUser(admin, null, groupsToBeAddedTo, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, new[] { gryffindor })]
    [InlineData(dumbledore, new[] { gryffindor, slytherin, hufflepuff })]
    public async Task CannotAddUserWithSameCountButMissingAndDuplicateEncryptions(string callingUsername, string[] groupsNamesToBeAddedTo)
    {
        var admin = TestUser[callingUsername];
        var groupsToBeAddedTo = groupsNamesToBeAddedTo.Select(g => TestGroups[g]);

        var encryptedSharedPasswords = groupsToBeAddedTo.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                     admin.KeyPairs.LastActive().PublicKeyString)
            })
            .ToList();

        encryptedSharedPasswords.Add(encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         userTestService.AddUser(admin, null, groupsToBeAddedTo, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWithMissingEncryptions(string callingUsername, string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values
            .SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                     targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWithDuplicateEncryptions(string callingUsername, string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values
            .SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                     targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        encryptedSharedPasswords =
            encryptedSharedPasswords.Append(
                encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWithSameCountButMissingAndDuplicateEncryptions(string callingUsername,
        string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values
            .SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something",
                                                     targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        encryptedSharedPasswords =
            encryptedSharedPasswords.Append(
                encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWhithMissingEncryptions(string callingUsername, string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWhithDuplicateEncryptions(string callingUsername, string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        encryptedSharedPasswords = encryptedSharedPasswords.Append(encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(dumbledore, dracoMalfoy)]
    public async Task CannotGrantAdminWhithSameCountButMissingAndDuplicateEncryptions(string callingUsername, string targetUserName)
    {
        var admin = TestUser[callingUsername];
        var targetUser = TestUser[targetUserName];

        var encryptedSharedPasswords = TestGroups.ByMandator(admin.Mandator.Name).Values.SelectMany(g => g.SharedPasswords.ByUnArchived())
            .Skip(1)
            .Select(sp => new
            {
                sharedPasswordId = sp.Id,
                encryptedPasswordString =
                    CryptoHelper.EncryptAsymmetrical("not the Real Pw but something", targetUser.KeyPairs.LastActive().PublicKeyString)
            });

        encryptedSharedPasswords = encryptedSharedPasswords.Append(encryptedSharedPasswords.First());

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => userTestService.GrantAdminPermissions(admin, targetUser, encryptedSharedPasswords));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    #endregion

    #region helper

    private static void CompareUsers(User expected, User actual, bool testArchivedDatetime = true)
    {
        Assert.NotNull(expected);
        Assert.NotNull(actual);
        Assert.Equal(expected.Firstname, actual.Firstname);
        Assert.Equal(expected.Lastname, actual.Lastname);
        Assert.Equal(expected.Email, actual.Email);
        Assert.Equal(expected.Admin, actual.Admin);
        Assert.False(actual.EmailConfirmed);

        Assert.Equal(expected.TwoFATypesEnabled, actual.TwoFATypesEnabled);
        Assert.Equal(expected.CreatedAt, actual.CreatedAt, new TimeSpan(0, 1, 0));
        if (testArchivedDatetime) Assert.Equal(expected.ArchivedAt, actual.ArchivedAt);
    }

    #endregion
}