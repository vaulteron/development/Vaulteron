﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VaulteronUtilities.Extensions;
using VaulteronTests.helper;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.IntegrationTests.UserRepository;

/// <summary>
/// Test if a user can access his/her own public + private key
/// Check whether other users can access public keys from other users
/// </summary>
public sealed class PrivatePublicKeysTests : HarryPotterTestUniverse
{
    public PrivatePublicKeysTests(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
    }

    #region happy paths

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(godricGryffindor)]
    [InlineData(ronWeasly)]
    [InlineData(dobby)]
    public async Task GetOwnPublicKey(string userName)
    {
        var user = TestUser[userName];

        var publicKey = await MakeRequestForKeyPair(user.Email);

        Assert.True(publicKey == user.KeyPairs.LastActive().PublicKeyString);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(godricGryffindor)]
    [InlineData(ronWeasly)]
    [InlineData(dobby)]
    public async Task GetOwnPrivateKey(string userName)
    {
        var user = TestUser[userName];

        var privateKey = await MakeRequestForPrivateKey(user.Email);

        Assert.True(privateKey == user.KeyPairs.LastActive().EncryptedPrivateKey);
    }

    [Fact]
    public async Task CannotUpdatePublicKeyInDB()
    {
        var pk = DbContext.KeyPairs.First();

        pk.PublicKeyString = "filler";
        pk.EncryptedPrivateKey = "filler";

        await Assert.ThrowsAsync<InvalidOperationException>(() => DbContext.SaveChangesAsync());

        var keyPairFromDB = DbContext.KeyPairs.AsNoTracking().First();
        Assert.NotEqual(pk.PublicKeyString, keyPairFromDB.PublicKeyString);
        Assert.NotEqual(pk.EncryptedPrivateKey, keyPairFromDB.EncryptedPrivateKey);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(godricGryffindor)]
    [InlineData(ronWeasly)]
    [InlineData(dobby)]
    public async Task GetFromOthersPublicKey(string userName)
    {
        var user = TestUser[userName];

        const string query = @"
               {
                  users {
                    publicKey {
                      publicKeyString
                    }
                  }
               }
               ";

        var json = await MakeGraphQLRequestAsync(user.Email, query);
        var usersWithKeys = JsonConvert.DeserializeObject<List<JObject>>(json["data"]!["users"]!.ToString());
        Assert.NotNull(usersWithKeys);
        var keyPairs = usersWithKeys!.Select(u => u["publicKey"]!).ToList().Children().ToList().Children()
            .ToList().Select(t => t.Value<string>()).ToList();

        var testuser = TestUser.ByMandator(BritishMandatorName).Values.ByArchived(user.Admin ? null : false).ToList();

        Assert.Equal(testuser.Count, usersWithKeys!.Count);
        foreach (var sampleUser in testuser)
        {
            Assert.Contains(sampleUser.KeyPairs.LastActive().PublicKeyString, keyPairs);
        }
    }

    #endregion

    #region helper

    private async Task<string> MakeRequestForKeyPair(string userEmail)
    {
        const string query = @"
                {
                  me {
                    keyPair {
                      publicKeyString
                    }
                  }
                }
                ";

        var json = await MakeGraphQLRequestAsync(userEmail, query);
        return json["data"]!["me"]!["keyPair"]!["publicKeyString"]!.ToString();
    }

    private async Task<string> MakeRequestForPrivateKey(string userEmail)
    {
        const string query = @"
                {
                  me {
                    keyPair {
                      encryptedPrivateKey
                    }
                  }
                }
                ";

        var json = await MakeGraphQLRequestAsync(userEmail, query);
        return json["data"]!["me"]!["keyPair"]!["encryptedPrivateKey"]!.ToString();
    }

    #endregion
}