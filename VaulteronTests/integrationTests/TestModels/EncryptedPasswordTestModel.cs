﻿using System;

namespace VaulteronTests.integrationTests.TestModels;

public class EncryptedPasswordTestModel
{
    // ReSharper disable once InconsistentNaming
    public Guid keyPairId { get; set; }

    // ReSharper disable once InconsistentNaming
    public string encryptedPasswordString { get; set; }
}