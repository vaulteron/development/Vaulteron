﻿using System;
using System.Collections.Generic;

namespace VaulteronTests.integrationTests.TestModels;

public class AddUserTestModel
{
    public string firstname { get; internal set; }
    public string lastname { get; internal set; }
    public string email { get; internal set; }
    public string sex { get; internal set; }
    public bool admin { get; internal set; }
    public string publicKey { get; internal set; }
    public string encryptedPrivateKey { get; internal set; }
    public string newTemporaryLoginPassword { get; set; }
    public string newTemporaryLoginPasswordHashed { get; set; }
    public IEnumerable<Guid> groupsToBeAddedTo { get; internal set; }
    public IEnumerable<object> encryptedSharedPasswords { get; internal set; }
}