﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class MovePasswordsTest : HarryPotterTestUniverse
{
    private readonly PasswordTestService passwordTestService;

    // TODO: add missing tests (other variations, moving multiple passwords at once, unhappy paths)

    public MovePasswordsTest(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null) : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
    }

    [Theory]
    [InlineData(dumbledore)] // Sysadmin
    [InlineData(minervaMcGonagall)] // Group admin
    [InlineData(harryPotter)] // Group editor
    public async Task MoveSharedPasswordFromGroupToCompany(string callingUserName)
    {
        const string decryptedPasswordString = "decryptedPW";
        const string targetGroupName = gryffindor;
        var adminName = callingUserName;
        var validationUserName = ronWeasly;
        var validationUser = TestUser[validationUserName];

        var targetPassword =
            await passwordTestService.AddSharedPasswordAsync(adminName, targetGroupName,
                                                             "MoveSharedPasswordFromGroupToCompany",
                                                             decryptedPasswordString);
        // Verify password is not there
        var allCompanyPasswordsBefore = await passwordTestService.GetAllCompanyPasswordsAsync(adminName);
        Assert.DoesNotContain(targetPassword, allCompanyPasswordsBefore);
        // Verify a user in that group can access it
        var passwordString =
            await passwordTestService.GetSingleSharedPasswordStringAsync(validationUser, targetPassword.Id);
        Assert.Equal(decryptedPasswordString, CryptoHelper.DecryptSymmetrically(passwordString));

        await passwordTestService.MoveSharedPasswordToCompanyAsync(adminName, targetPassword.Id, adminName);

        // Verify password is there
        var allCompanyPasswordsAfter = await passwordTestService.GetAllCompanyPasswordsAsync(adminName);
        Assert.Contains(targetPassword, allCompanyPasswordsAfter);
        // Verify a user in that group can not access it
        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.GetSingleSharedPasswordStringAsync(validationUser, targetPassword.Id));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)] // Sysadmin
    [InlineData(minervaMcGonagall)] // Group admin
    [InlineData(harryPotter)] // Group editor
    [InlineData(ronWeasly)] // Group viewer
    public async Task MoveOwnCompanyPasswordToGroup(string validationUserInTargetGroup)
    {
        const string decryptedPasswordString = "decryptedPW";
        const string targetGroupName = gryffindor;
        var callingUser = hermioneGranger;
        var validationUserName = validationUserInTargetGroup;
        var validationUser = TestUser[validationUserName];

        var targetPassword = await passwordTestService.AddCompanyPasswordAsync(callingUser,
                                                                               "MoveOwnCompanyPasswordToGroup", decryptedPasswordString);
        // Verify password is not there
        var allGroupPasswordsBefore =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(validationUserName, targetGroupName);
        Assert.DoesNotContain(targetPassword, allGroupPasswordsBefore);

        await passwordTestService.MoveCompanyPasswordToSharedGroupPasswordAsync(callingUser,
                                                                                new List<Guid> { targetPassword.Id },
                                                                                targetGroupName, decryptedPasswordString);

        // Verify password is there
        var allGroupPasswordsAfter =
            await passwordTestService.GetAllSharedPasswordsForGroupAsync(callingUser, targetGroupName);
        Assert.Contains(targetPassword, allGroupPasswordsAfter);
        // Verify a user in that group can access it
        var passwordString =
            await passwordTestService.GetSingleSharedPasswordStringAsync(validationUser, targetPassword.Id);
        Assert.Equal(decryptedPasswordString,
                     CryptoHelper.DecryptAsymmetrical(passwordString, ""));
    }
}