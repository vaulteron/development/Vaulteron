﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class AccountPasswordTests : HarryPotterTestUniverse
{
    private readonly Dictionary<string, AccountPassword> testPasswords = new();

    private readonly Dictionary<string, string> testPasswordStrings = new();

    private readonly PasswordTestService passwordTestService;

    public AccountPasswordTests(TestWebApplicationFactory factory, ITestOutputHelper logger,
        HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);

        testPasswordStrings[harryPotter] = "super_pw123";
        testPasswordStrings[dumbledore] = "super_secure_pw_1234?!";

        testPasswords[harryPotter] = passwordTestService.AddAccountPasswordAsync(harryPotter,
                                                                                 testPasswordStrings[harryPotter], TestUser[harryPotter].Tags.Select(t => t.Id).ToList()).Result;
        testPasswords[dumbledore] = passwordTestService.AddAccountPasswordAsync(dumbledore,
                                                                                testPasswordStrings[dumbledore], TestUser[dumbledore].Tags.Select(t => t.Id).ToList()).Result;
    }

    [Theory]
    [InlineData(dumbledore, dumbledore + tagNormalSuffix, dumbledore + tagSpecialSuffix)]
    [InlineData(harryPotter, harryPotter + tagNormalSuffix)]
    public async Task SuccessfulGetPasswords(string username, params string[] tags)
    {
        var user = TestUser[username];
        var password = testPasswords[username];
        var passwordString = testPasswordStrings[username];
        const string query = @"
                {
                  accountPasswords (byArchived: false) {
                    id name login securityRating updatedAt notes websiteURL
                    tags { name }
                  }
                }";

        var json = await MakeGraphQLRequestAsync(user.Email, query);
        var accountPasswords =
            JsonConvert.DeserializeObject<List<AccountPassword>>(json["data"]!["accountPasswords"]!.ToString());
        Assert.NotNull(accountPasswords);
        var foundPassword = accountPasswords!.Find(accountPassword => accountPassword.Name == password.Name);
        PasswordTestService.ComparePasswordsMetaData(password, foundPassword);

        #region get tags for target accountPassword

        var index = passwordTestService.GetIndexInsideJsonOfPasswordWithTargetName(json, password.Name, true);
        var tagsOfFoundPassword =
            JsonConvert.DeserializeObject<List<Tag>>(json["data"]!["accountPasswords"]![index]!["tags"]!
                                                         .ToString());
        Assert.NotNull(tagsOfFoundPassword);
        var tagStringsOfFoundPassword = tagsOfFoundPassword!.Select(t => t.Name).ToList();

        #endregion

        foreach (string tag in tags) Assert.Contains(tag, tagStringsOfFoundPassword);

        // Now check if we can get the encrypted strings for the passwords
        foreach (var accountPassword in accountPasswords)
        {
            var unencryptedString = testPasswords.Values.Select(p => p.Id).Contains(accountPassword.Id)
                ? passwordString
                : unencryptedPasswordString;
            await passwordTestService.CheckEncryptedAccountPasswordEntityAsync(user.Email, accountPassword.Id,
                                                                               unencryptedString, user.KeyPairs.LastActive().EncryptedPrivateKey);
        }
    }

    [Theory]
    [InlineData(dumbledore, accountPassword + dumbledore + archived)]
    [InlineData(harryPotter, accountPassword + harryPotter + archived)]
    public async Task SuccessfulGetArchivedPasswords(string username, string password)
    {
        var user = TestUser[username];

        var pws = await passwordTestService.GetAllAccountPasswordsAsync(user, true);

        Assert.Single(pws);
        Assert.Contains(pws, pw => pw.Name == password);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task FailGetArchivedPasswordString(string username)
    {
        var user = TestUser[username];
        var pws = await passwordTestService.GetAllAccountPasswordsAsync(user, true);
        Assert.Single(pws);
        var pw = pws.Single();

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.CheckEncryptedAccountPasswordEntityAsync(
                                                                             user.Email, pw.Id, unencryptedPasswordString, user.KeyPairs.LastActive().EncryptedPrivateKey));

        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task FailEditArchivedPasswordString(string username)
    {
        var user = TestUser[username];
        var pws = await passwordTestService.GetAllAccountPasswordsAsync(user, true);
        Assert.Single(pws);
        var pw = pws.Single();

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.EditAccountPasswordAsync(username, pw, "changedPasswordString"));

        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, accountPassword + dumbledore + archived)]
    [InlineData(harryPotter, accountPassword + harryPotter + archived)]
    public async Task FailEditArchivedPasswords(string username, string password)
    {
        var user = TestUser[username];
        var pws = await passwordTestService.GetAllAccountPasswordsAsync(user, true);
        Assert.Single(pws);
        var pwToChange = pws.Single(pw => pw.Name == password);

        pwToChange.Login = "chagendLogin";
        pwToChange.Notes = "changedNotes";

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.EditAccountPasswordAsync(username, pwToChange));

        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);

        // Check if archived password is still unchanged
        pws = await passwordTestService.GetAllAccountPasswordsAsync(user, true);
        var pw = pws.Single(pw => pw.Name == password);

        Assert.NotEqual(pwToChange.Login, pw.Login);
        Assert.NotEqual(pwToChange.Notes, pw.Notes);

    }

    [Theory]
    [InlineData(dumbledore, "th1s4S3cur3P4ssw0rd")]
    [InlineData(harryPotter)]
    public async Task SuccessfulCreatePassword(string username, string newPW = "thisARandomString")
    {
        var user = TestUser[username];
        var keyPair = user.KeyPairs.LastActive();

        const string query = @"
                mutation AddAccountPassword($accountPassword: AddAccountPasswordInputType!) {
                    accountPassword {
                        add(password: $accountPassword) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(newPW, keyPair.PublicKeyString);

        var newPassword = new AccountPassword
        {
            Name = "new PW",
            Login = "my@email.com",
            WebsiteURL = "unknown.com",
            Notes = "some notes",
        };

        var variables = new
        {
            accountPassword = new
            {
                name = newPassword.Name,
                login = newPassword.Login,
                websiteUrl = newPassword.WebsiteURL,
                notes = newPassword.Notes,
                tags = new List<Guid> { user.Tags.First().Id },
                encryptedPassword = encryptedPasswordString,
                keyPairId = keyPair.Id,
            }
        };
        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var createdPassword = JsonConvert.DeserializeObject<AccountPassword>(json["data"]!["accountPassword"]!["add"]!.ToString());
        Assert.NotNull(createdPassword);
        PasswordTestService.ComparePasswordsMetaData(newPassword, createdPassword, DateTime.UtcNow);
        Assert.Equal(user.Id, createdPassword!.Owner.Id);

        // Now compare the tags
        var createdPasswordTags = JsonConvert.DeserializeObject<List<Tag>>(json["data"]!["accountPassword"]!["add"]!["tags"]!.ToString());
        Assert.NotNull(createdPasswordTags);
        Assert.NotEmpty(createdPasswordTags);
        Assert.True(createdPasswordTags!.Count == 1);
        Assert.Equal(createdPasswordTags.First().Name, user.Tags.First().Name);
        Assert.Equal(createdPasswordTags.First().Color, user.Tags.First().Color);
        Assert.Equal(createdPasswordTags.First().Id, user.Tags.First().Id);

        // Now check if a EncryptedPassword entity was created and equals the sent string
        await passwordTestService.CheckEncryptedAccountPasswordEntityAsync(user.Email, createdPassword.Id, newPW,
                                                                           keyPair.EncryptedPrivateKey);

        // Now Check for change in Query
        var accountPasswords = await passwordTestService.GetAllAccountPasswordsAsync(user);
        var actual = accountPasswords.Find(a => a.Id == createdPassword.Id);
        PasswordTestService.ComparePasswordsMetaData(newPassword, actual);
    }

    [Theory]
    [InlineData(dumbledore, "", "somePW", false)]
    [InlineData(dumbledore, "someName", "", false)]
    [InlineData(dumbledore, "someName", "somePW", true)]
    public async Task FailToAddPasswordWithBadInputData(string username, string passwordName,
        string encryptedPasswordString, bool randomKeyPairGuid)
    {
        var user = TestUser[username];
        const string query = @"
                mutation AddAccountPassword($accountPassword: AddAccountPasswordInputType!) {
                    accountPassword {
                        add(password: $accountPassword) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var variables = new
        {
            accountPassword = new
            {
                name = passwordName,
                login = "",
                websiteUrl = "",
                notes = "",
                encryptedPassword = encryptedPasswordString,
                keyPairId = randomKeyPairGuid ? Guid.NewGuid() : user.KeyPairs.LastActive().Id,
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.InputInvalid, exception.ErrorCode);
    }

    [Theory]
    [InlineData(harryPotter, "")]
    public async Task FailToEditPasswordWithBadInputData(string passwordOwnerUsername, string passwordName)
    {
        var passwordToEdit = testPasswords[passwordOwnerUsername];
        var user = TestUser[passwordOwnerUsername];
        const string query = @"
                mutation EditAccountPassword($accountPassword: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $accountPassword) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";
        var encryptedPassword = new
        {
            keyPairId = TestUser[harryPotter].KeyPairs.LastActive().Id,
            // ReSharper disable once RedundantAnonymousTypePropertyName
            encryptedPasswordString = "encryptedPasswordString"
        };
        var variables = new
        {
            accountPassword = new
            {
                id = passwordToEdit.Id,
                name = passwordName,
                login = "",
                websiteURL = "",
                notes = "",
                encryptedPassword
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.InputInvalid, exception.ErrorCode);
    }


    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task SuccessfulDeleteAccountPassword(string username)
    {
        var user = TestUser[username];
        var passwordToDelete = testPasswords[username];

        // First check how many accountPasswords this user has
        var numAccountPasswordsBefore = (await passwordTestService.GetAllAccountPasswordsAsync(user)).Count;

        var returnedPassword = await passwordTestService.DeleteAccountPasswordAsync(user, passwordToDelete.Id);
        PasswordTestService.ComparePasswordsMetaData(passwordToDelete, returnedPassword);

        // Now Check for change in Query
        var numAccountPasswordsAfter = (await passwordTestService.GetAllAccountPasswordsAsync(user)).Count;
        Assert.Equal(numAccountPasswordsBefore - 1, numAccountPasswordsAfter);
    }


    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(harryPotter, dumbledore)]
    public async Task CannotDeletePasswordFromOtherUser(string callingUsername, string targetUsername)
    {
        var callingUser = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];

        var passwordToDelete = testPasswords[targetUsername];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.DeleteAccountPasswordJsonAsync(callingUser, passwordToDelete.Id));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);

        // Now Check for no change in Query
        var accountPasswords = await passwordTestService.GetAllAccountPasswordsAsync(targetUser);
        var actual = accountPasswords.Find(a => a.Id == passwordToDelete.Id);
        PasswordTestService.ComparePasswordsMetaData(passwordToDelete, actual);
    }


    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CannotDeletePasswordThatDoesNotExist(string username)
    {
        var user = TestUser[username];
        var randomGuid = Guid.NewGuid();

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.DeleteAccountPasswordJsonAsync(user, randomGuid));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task SuccessfulEditPassword(string username)
    {
        var user = TestUser[username];
        var passwordToEdit = testPasswords[username];
        var editedPass = new AccountPassword
        {
            Id = passwordToEdit.Id,
            Name = passwordToEdit.Name + "edited",
            Login = passwordToEdit.Login + "edited",
            WebsiteURL = passwordToEdit.WebsiteURL + "edited",
            Notes = passwordToEdit.Notes + "edited",
        };

        const string query = @"
                mutation ($password: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $password) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var variables = new
        {
            password = new
            {
                id = editedPass.Id,
                name = editedPass.Name,
                login = editedPass.Login,
                websiteURL = editedPass.WebsiteURL,
                notes = editedPass.Notes,

                tags = user.Tags.Select(t => t.Id)
            }
        };
        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var returnedPassword =
            JsonConvert.DeserializeObject<AccountPassword>(json["data"]!["accountPassword"]!["edit"]!
                                                               .ToString());
        PasswordTestService.ComparePasswordsMetaData(editedPass, returnedPassword);

        // Now Check for change in Query
        var accountPasswords = await passwordTestService.GetAllAccountPasswordsAsync(user);
        var actual = accountPasswords.Find(a => a.Id == passwordToEdit.Id);
        PasswordTestService.ComparePasswordsMetaData(editedPass, actual);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CannotEditPasswordThatDoesNotExist(string username)
    {
        var user = TestUser[username];
        var passwordToEdit = testPasswords[username];
        var randomGuid = Guid.NewGuid();

        const string query = @"
                mutation ($password: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $password) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var variables = new
        {
            password = new
            {
                id = randomGuid,
                name = passwordToEdit.Name + "edited",
                login = passwordToEdit.Login + "edited",
                websiteURL = passwordToEdit.WebsiteURL + "edited",
                notes = passwordToEdit.Notes + "edited",
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(harryPotter)]
    public async Task CannotEditPasswordWithTagsThatDoNotExist(string username)
    {
        var user = TestUser[username];
        var passwordToEdit = testPasswords[username];
        var randomGuid = Guid.NewGuid();

        const string query = @"
                mutation ($password: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $password) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var variables = new
        {
            password = new
            {
                id = passwordToEdit.Id,
                name = passwordToEdit.Name + "edited",
                login = passwordToEdit.Login + "edited",
                websiteURL = passwordToEdit.WebsiteURL + "edited",
                notes = passwordToEdit.Notes + "edited",
                tags = new[] { randomGuid }
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, harryPotter)]
    [InlineData(harryPotter, dumbledore)]
    public async Task CannotEditPasswordFromOtherUser(string callingUsername, string targetUsername)
    {
        var callingUser = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];
        var passwordToEdit = testPasswords[targetUsername];

        var editedPass = new AccountPassword
        {
            Id = passwordToEdit.Id,
            Name = passwordToEdit.Name + "edited",
            Login = passwordToEdit.Login + "edited",
            WebsiteURL = passwordToEdit.WebsiteURL + "edited",
            Notes = passwordToEdit.Notes + "edited",
        };

        const string query = @"
                mutation ($password: EditAccountPasswordInputType!) {
                    accountPassword {
                        edit(password: $password) {
                            id name login securityRating websiteURL notes updatedAt createdAt
                            tags { id name color }
                            owner { id }
                        }
                    }
                }";

        var variables = new
        {
            password = new
            {
                id = editedPass.Id,
                name = editedPass.Name,
                login = editedPass.Login,
                websiteURL = editedPass.WebsiteURL,
                notes = editedPass.Notes,

                tags = callingUser.Tags.Select(t => t.Id)
            }
        };
        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);

        // Now Check for No change in Query
        var accountPasswords = await passwordTestService.GetAllAccountPasswordsAsync(targetUser);
        var actual = accountPasswords.Find(a => a.Id == passwordToEdit.Id);
        PasswordTestService.ComparePasswordsMetaData(passwordToEdit, actual);
    }
}