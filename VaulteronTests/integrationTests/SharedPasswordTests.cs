﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Helper;
using VaulteronTests.helper;
using VaulteronTests.integrationTests.IntegrationTestServices;
using VaulteronTests.integrationTests.TestModels;
using Xunit;
using Xunit.Abstractions;
using static VaulteronDatabase.SeedData.VaulteronSeedData;

namespace VaulteronTests.integrationTests;

public class SharedPasswordTest : HarryPotterTestUniverse
{
    private readonly Dictionary<string, SharedPassword> testPasswords = new();
    private readonly PasswordTestService passwordTestService;
    private readonly GroupTestService groupTestService;

    private const string PasswordGryffindorForEdits = "passwordGryffindorForEdits",
        PasswordGryffindorForDeletion = "passwordGryffindorForDeletion",
        PasswordForMovingBetweenGroups = "passwordForMovingbetweenGroups",
        PasswordHogwartsOne = "passwordHogwartsOne",
        PasswordHogwartsTwo = "passwordHogwartsTwo",
        PasswordSlytherin = "passwordSlytherin";

    private const string SharedPasswordString = "unencrypted";

    public SharedPasswordTest(TestWebApplicationFactory factory, ITestOutputHelper logger, HarryPotterTestUniverse hptu = null)
        : base(factory, logger, hptu)
    {
        passwordTestService = new PasswordTestService(logger, this);
        groupTestService = new GroupTestService(logger, this);

        testPasswords[PasswordGryffindorForEdits] =
            passwordTestService.AddSharedPasswordAsync(dumbledore, gryffindor, PasswordGryffindorForEdits, SharedPasswordString).Result;

        testPasswords[PasswordGryffindorForDeletion] =
            passwordTestService.AddSharedPasswordAsync(dumbledore, gryffindor, PasswordGryffindorForDeletion, SharedPasswordString).Result;

        #region for testing just more

        testPasswords[PasswordHogwartsOne] = passwordTestService
            .AddSharedPasswordAsync(dumbledore, hogwarts, PasswordHogwartsOne, SharedPasswordString).Result;
        testPasswords[PasswordHogwartsTwo] = passwordTestService
            .AddSharedPasswordAsync(dumbledore, hogwarts, PasswordHogwartsTwo, SharedPasswordString).Result;
        testPasswords[PasswordSlytherin] = passwordTestService
            .AddSharedPasswordAsync(dumbledore, hogwarts, PasswordSlytherin, SharedPasswordString).Result;

        #endregion
    }

    #region access

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(severusSnape)]
    [InlineData(minervaMcGonagall)]
    [InlineData(pomonaSprout)]
    [InlineData(filiusFlitwick)]
    [InlineData(nearlyHeadlessNick)]
    [InlineData(harryPotter)]
    [InlineData(hermioneGranger)]
    [InlineData(ronWeasly)]
    public async Task AccessToSharedPasswordOfGryffindorAllowed(string callingUsername)
    {
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        var foundPasswords = await passwordTestService.GetAllSharedPasswordsForGroupAsync(callingUsername, gryffindor);
        Assert.NotEmpty(foundPasswords);
        Assert.Contains(sharedPasswordOfGryffindor.Id, foundPasswords.Select(sp => sp.Id));
        var targetPassword = foundPasswords.Single(sp => sp.Id == sharedPasswordOfGryffindor.Id);
        PasswordTestService.ComparePasswordsMetaData(sharedPasswordOfGryffindor, targetPassword);
    }

    [Theory]
    [InlineData(lunaLovegood)] // Wrong sub-group
    [InlineData(garrickOllivander)] // Wrong sub-group
    [InlineData(dracoMalfoy)] // Wrong sub-group
    [InlineData(horaceSlughorn)] // Wrong sub-group
    [InlineData(cedricDiggory)] // Wrong sub-group
    [InlineData(viktorKrum)] // Wrong root-group
    public async Task AccessToSharedPasswordOfGryffindorNotAllowed(string callingUsername)
    {
        var foundPasswords = await passwordTestService.GetAllSharedPasswordsForGroupAsync(callingUsername, gryffindor);
        Assert.Empty(foundPasswords);
    }

    #endregion

    #region add

    [Theory]
    [InlineData(dumbledore)] // Admin (not in group or parent-group)
    [InlineData(severusSnape)] // Parent-group-admin
    [InlineData(minervaMcGonagall)] // Parent-group-editor
    [InlineData(godricGryffindor)] // Group-admin
    [InlineData(harryPotter)] // Group-editor
    public async Task AddSharedPasswordToGryffindorAllowed(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var group = TestGroups[gryffindor];
        var ownKeyPair = user.KeyPairs.LastActive();

        var variables = new
        {
            sharedPassword = new
            {
                name = "sharedPassword created by " + user.Firstname,
                login = "login for " + "sharedPassword created by " + user.Firstname,
                websiteUrl = "www.vaulteron.com",
                notes = "some notes",
                tags = new List<Guid> { group.Tags.First().Id },
                groupId = group.Id
            }
        };

        var json = await passwordTestService.AddSharedPasswordJsonAsync(callingUsername, group.Name, variables.sharedPassword.name, SharedPasswordString);
        var createdPassword = JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["add"]!.ToString());
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.sharedPassword.name,
            Login = variables.sharedPassword.login,
            WebsiteURL = variables.sharedPassword.websiteUrl,
            Notes = variables.sharedPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        // Now compare the tags
        var createdPasswordTags =
            JsonConvert.DeserializeObject<List<Tag>>(json["data"]!["sharedPassword"]!["add"]!["tags"]!.ToString());
        Assert.NotEmpty(createdPasswordTags);
        Assert.True(createdPasswordTags.Count == 1);
        Assert.Equal(createdPasswordTags.First().Name, group.Tags.First().Name);
        Assert.Equal(createdPasswordTags.First().Color, group.Tags.First().Color);
        Assert.Equal(createdPasswordTags.First().Id, group.Tags.First().Id);

        // Now check if a EncryptedPassword entity was created and equals the sent string
        await passwordTestService.CheckEncryptedSharedPasswordEntityAsync(user.Email, createdPassword.Id, SharedPasswordString, ownKeyPair.EncryptedPrivateKey);

        // Edit for oneself
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   createdPassword.Id,
                                                                                   new List<User> { user },
                                                                                   SharedPasswordString,
                                                                                   false,
                                                                                   false);
    }

    [Theory]
    [InlineData(ronWeasly)] // Group-viewer
    [InlineData(nearlyHeadlessNick)] // Parent-group-viewer
    public async Task AddSharedPasswordToGryffindorNotAllowed(string callingUsername)
    {
        var group = TestGroups[gryffindor];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.AddSharedPasswordAsync(callingUsername,
                                                                                                                    group.Name,
                                                                                                                    "pw-name",
                                                                                                                    SharedPasswordString)
        );
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Fact]
    public async Task AddSharedPasswordToGryffindorNotAllowedWhenNotEncryptedForAllAdmins()
    {
        const string plaintextPassword = "some-test";
        const string username = dumbledore;
        const string groupName = gryffindor;
        const string passwordName = "some password name";

        // Get nonAdmins
        var usersWithAccess = await groupTestService.GetUsers(username, groupName, true);
        var nonAdmins = usersWithAccess.Where(u => !u.Admin).ToList();
        var keyPairs = nonAdmins.Select(u => u.KeyPairs.LastActive()).ToList();
        // Send to API
        var encryptedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(plaintextPassword, pk.PublicKeyString)
            });

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.AddSharedPasswordJsonAsync(username,
                                                                                                                        groupName,
                                                                                                                        passwordName,
                                                                                                                        encryptedPasswords,
                                                                                                                        new List<Guid>()));
        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, gryffindor, tagGryffindorIceSpells)]
    [InlineData(minervaMcGonagall, gryffindor, tagGryffindorFireSpells)]
    [InlineData(dracoMalfoy, slytherin, tagSlytherinFireSpells)]
    [InlineData(severusSnape, slytherin, tagSlytherinIceSpells)]
    public async Task AddSharedPasswordToGroupWithTagAllowed(string callingUsername, string groupName,
        string tagName)
    {
        var user = TestUser[callingUsername];
        var tag = TestTags[tagName];

        var json = await passwordTestService.AddSharedPasswordJsonAsync(callingUsername,
                                                                        groupName,
                                                                        $"sharedPassword created by {user.Firstname}",
                                                                        SharedPasswordString,
                                                                        new List<Guid> { tag.Id });
        var createdPasswordTags =
            JsonConvert.DeserializeObject<List<Tag>>(json["data"]!["sharedPassword"]!["add"]!["tags"]!.ToString());
        Logger.WriteLine(createdPasswordTags.ToList().ToString());
        Assert.NotEmpty(createdPasswordTags);
        Assert.True(createdPasswordTags.Count == 1);
        Assert.Equal(createdPasswordTags.First().Name, tagName);
    }

    [Theory]
    [InlineData(dumbledore, hufflepuff, tagGryffindorIceSpells)]
    [InlineData(dumbledore, ravenclaw, tagGryffindorFireSpells)]
    [InlineData(dumbledore, gryffindor, tagSlytherinFireSpells)]
    [InlineData(dumbledore, ravenclaw, tagSlytherinIceSpells)]
    public async Task AddSharedPasswordToGroupWithTagNotAllowed(string callingUsername, string groupName,
        string tagName)
    {
        var user = TestUser[callingUsername];
        var tag = TestTags[tagName];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() =>
                                                                         passwordTestService.AddSharedPasswordAsync(callingUsername,
                                                                                                                    groupName,
                                                                                                                    $"sharedPassword created by {user.Firstname}",
                                                                                                                    SharedPasswordString,
                                                                                                                    new List<Guid> { tag.Id }));
        Assert.Equal(ErrorCode.DataNotFound, exception.ErrorCode);
    }

    #endregion

    #region edit

    [Fact]
    public async Task EditSharedPasswordToGryffindorNotAllowedWhenNotEncryptedForAllAdmins()
    {
        const string plaintextPassword = "some-test";
        const string username = dumbledore;
        const string groupName = gryffindor;
        var user = TestUser[username];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        // Get nonAdmins
        var usersWithAccess = await groupTestService.GetUsers(username, groupName, true);
        var nonAdmins = usersWithAccess.Where(u => !u.Admin).ToList();
        var keyPairs = nonAdmins.Select(u => u.KeyPairs.LastActive()).ToList();
        // Send to API
        var encryptedSharedPasswords = keyPairs
            .Select(pk => new EncryptedPasswordTestModel
            {
                keyPairId = pk.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(plaintextPassword, pk.PublicKeyString)
            });

        const string query = @"
               mutation EditSharedPassword($sharedPassword: EditSharedPasswordInputType!) {
                    sharedPassword {
                        edit(password: $sharedPassword) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";
        var variables = new
        {
            sharedPassword = new
            {
                id = sharedPasswordOfGryffindor.Id,
                name = sharedPasswordOfGryffindor.Name,
                modifyLock = sharedPasswordOfGryffindor.ModifyLock,
                encryptedPasswords = encryptedSharedPasswords
            }
        };
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));

        Assert.Equal(ErrorCode.InputInvalid_EncryptionsNotProvidedCorrectly, exception.ErrorCode);
    }

    [Theory]
    [InlineData(ronWeasly)] // Group-viewer
    [InlineData(nearlyHeadlessNick)] // Parent-group-viewer
    [InlineData(lunaLovegood)] // Wrong sub-group
    [InlineData(garrickOllivander)] // Wrong sub-group
    [InlineData(dracoMalfoy)] // Wrong sub-group
    [InlineData(horaceSlughorn)] // Wrong sub-group
    [InlineData(cedricDiggory)] // Wrong sub-group
    [InlineData(viktorKrum)] // Wrong root-group
    public async Task EditSharedPasswordMetaDataOfGryffindorNotAllowed(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var group = TestGroups[gryffindor];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        const string query = @"
               mutation EditSharedPassword($sharedPassword: EditSharedPasswordInputType!) {
                    sharedPassword {
                        edit(password: $sharedPassword) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";

        var variables = new
        {
            sharedPassword = new
            {
                id = sharedPasswordOfGryffindor.Id,
                name = "edited password by  " + user.Firstname,
                login = "edited login for " + user.Firstname,
                websiteURL = "www.edit.com",
                notes = "edited some notes",
                tags = new List<Guid> { group.Tags.First().Id },
                modifyLock = sharedPasswordOfGryffindor.ModifyLock
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)]
    [InlineData(minervaMcGonagall)]
    public async Task EditSharedPasswordMetaDataOfGryffindorNotAllowedWhenArchived(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var group = TestGroups[gryffindor];

        var newPassword = await passwordTestService.AddSharedPasswordAsync(callingUsername, gryffindor, "pw-string", "pw-name");
        await passwordTestService.DeleteSharedPasswordJsonAsync(user, newPassword.Id);

        const string query = @"
               mutation EditSharedPassword($sharedPassword: EditSharedPasswordInputType!) {
                    sharedPassword {
                        edit(password: $sharedPassword) {
                            "
            + SharedPasswordFragment
            + @"
                        }
                    }
                }
               ";

        var variables = new
        {
            sharedPassword = new
            {
                id = newPassword.Id,
                name = "edited password by  " + user.Firstname,
                login = "edited login for " + user.Firstname,
                websiteURL = "www.edit.com",
                notes = "edited some notes",
                tags = new List<Guid> { group.Tags.First().Id },
                modifyLock = newPassword.ModifyLock
            }
        };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.DataExpired, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)] // Sys-admin
    [InlineData(severusSnape)] // Parent-group-admin
    [InlineData(minervaMcGonagall)] // Parent-group-editor
    [InlineData(pomonaSprout)] // Parent-group-editor
    [InlineData(filiusFlitwick)] // Parent-group-editor
    [InlineData(harryPotter)] // Group-editor
    [InlineData(hermioneGranger)] // Group-editor
    public async Task EditSharedPasswordMetaDataOfGryffindorAllowed(string callingUsername)
    {
        // FYI
        // This test edits the PW without changing the encryptedPasswordString
        // The reason is that for these users the encryptedPassword is not yet set
        var user = TestUser[callingUsername];
        var group = TestGroups[gryffindor];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        const string query = @"
                mutation EditSharedPassword($sharedPassword: EditSharedPasswordInputType!) {
                    sharedPassword { edit(password: $sharedPassword) { "
            + SharedPasswordFragment
            + @"
                } } }
               ";
        var variables = new
        {
            sharedPassword = new
            {
                id = sharedPasswordOfGryffindor.Id,
                name = "edited password by  " + user.Firstname,
                login = "edited login for " + user.Firstname,
                websiteURL = "www.edit.com",
                notes = "edited some notes",
                tags = new List<Guid> { group.Tags.First().Id },
                modifyLock = sharedPasswordOfGryffindor.ModifyLock
            }
        };
        var json = await MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables));
        var createdPassword =
            JsonConvert.DeserializeObject<SharedPassword>(json["data"]!["sharedPassword"]!["edit"]!.ToString());
        PasswordTestService.ComparePasswordsMetaData(new SharedPassword
        {
            Name = variables.sharedPassword.name,
            Login = variables.sharedPassword.login,
            WebsiteURL = variables.sharedPassword.websiteURL,
            Notes = variables.sharedPassword.notes,
        }, createdPassword, DateTime.UtcNow);

        // Now compare the tags
        var createdPasswordTags =
            JsonConvert.DeserializeObject<List<Tag>>(json["data"]!["sharedPassword"]!["edit"]!["tags"]!.ToString());
        Assert.NotEmpty(createdPasswordTags);
        Assert.True(createdPasswordTags.Count == 1);
        Assert.Equal(createdPasswordTags.First().Name, group.Tags.First().Name);
        Assert.Equal(createdPasswordTags.First().Color, group.Tags.First().Color);
        Assert.Equal(createdPasswordTags.First().Id, group.Tags.First().Id);

        // Addition to catch b-ug: after changing the meta-data the user should still be able to get the password-string as it was not changed
        // (the b-ug prevented this because it said: the updatedAt was changed and therefore the encryptedPassword was changed (which is false) and would not provide the password-string
        // Compare password-string
        await passwordTestService.CheckEncryptedSharedPasswordEntityAsync(user.Email, sharedPasswordOfGryffindor.Id, SharedPasswordString, user.KeyPairs.LastActive().EncryptedPrivateKey);
    }


    [Theory]
    [InlineData(dumbledore)] // Sys-admin
    [InlineData(severusSnape)] // Parent-group-admin
    [InlineData(minervaMcGonagall)] // Parent-group-editor
    [InlineData(pomonaSprout)] // Parent-group-editor
    [InlineData(filiusFlitwick)] // Parent-group-editor
    [InlineData(harryPotter)] // Group-editor
    [InlineData(hermioneGranger)] // Group-editor
    public async Task EditSharedPasswordOfGryffindorAllowed(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        var newSharedPassword = new SharedPassword
        {
            Id = sharedPasswordOfGryffindor.Id,
            Name = sharedPasswordOfGryffindor.Name + "_edited",
            Login = sharedPasswordOfGryffindor.Login + "_edited",
            WebsiteURL = sharedPasswordOfGryffindor.WebsiteURL,
            Notes = sharedPasswordOfGryffindor.Notes,
            SharedPasswordTags = sharedPasswordOfGryffindor.SharedPasswordTags,
            ModifyLock = sharedPasswordOfGryffindor.ModifyLock
        };
        const string newPasswordString = "superSecurePassword123!";
        var editedSharedPassword = await passwordTestService.EditSharedPasswordAsync(callingUsername,
                                                                                     gryffindor,
                                                                                     newSharedPassword,
                                                                                     newPasswordString);

        PasswordTestService.ComparePasswordsMetaData(newSharedPassword, editedSharedPassword, DateTime.UtcNow);

        // Addition to catch b-ug: after changing the meta-data the user should still be able to get the password-string as it was not changed
        // (the b-ug prevented this because it said: the updatedAt was changed and therefore the encryptedPassword was changed (which is false) and would not provide the password-string
        // Compare password-string
        await passwordTestService.CheckEncryptedSharedPasswordEntityAsync(user.Email, sharedPasswordOfGryffindor.Id,
                                                                          newPasswordString, user.KeyPairs.LastActive().EncryptedPrivateKey);
    }

    #endregion

    #region delete

    [Theory]
    [InlineData(ronWeasly)] // Group-viewer
    [InlineData(nearlyHeadlessNick)] // Parent-group-viewer
    [InlineData(lunaLovegood)] // Wrong sub-group
    [InlineData(garrickOllivander)] // Wrong sub-group
    [InlineData(dracoMalfoy)] // Wrong sub-group
    [InlineData(horaceSlughorn)] // Wrong sub-group
    [InlineData(cedricDiggory)] // Wrong sub-group
    [InlineData(viktorKrum)] // Wrong root-group
    public async Task DeleteSharedPasswordOfGryffindorNotAllowed(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForDeletion];

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => passwordTestService.DeleteSharedPasswordJsonAsync(user, sharedPasswordOfGryffindor.Id));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore)] // Sys-admin
    [InlineData(severusSnape)] // Parent-group-admin
    [InlineData(minervaMcGonagall)] // Parent-group-editor
    [InlineData(pomonaSprout)] // Parent-group-editor
    [InlineData(filiusFlitwick)] // Parent-group-editor
    [InlineData(harryPotter)] // Group-editor
    [InlineData(hermioneGranger)] // Group-editor
    public async Task DeleteSharedPasswordOfGryffindorAllowed(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var passwordToBeDeleted = testPasswords[PasswordGryffindorForDeletion];

        await passwordTestService.DeleteSharedPasswordJsonAsync(user, passwordToBeDeleted.Id);

        var foundPasswords = await passwordTestService.GetAllSharedPasswordsForGroupAsync(callingUsername, gryffindor);
        Assert.NotNull(foundPasswords);
        Assert.DoesNotContain(foundPasswords!, sp => sp.Id == passwordToBeDeleted.Id);
    }

    #endregion

    #region encrypted password

    [Theory]
    // From sys-admin --to--> sys-admin
    [InlineData(dumbledore, dumbledore)] // Admin (not in group or parent-group)
    // From user with access --to--> external sys-admin
    [InlineData(severusSnape, dumbledore)] // Parent-group-admin
    [InlineData(minervaMcGonagall, dumbledore)] // Parent-group-editor
    [InlineData(godricGryffindor, dumbledore)] // Group-admin
    [InlineData(harryPotter, dumbledore)] // Group-editor
    // [InlineData(ronWeasly, dumbledore)] // Group-viewer // cannot encrypt/change for other users anymore
    // From user with access --to--> group-viewer
    [InlineData(minervaMcGonagall, ronWeasly)] // Parent-group-editor
    [InlineData(godricGryffindor, ronWeasly)] // Group-admin
    [InlineData(harryPotter, ronWeasly)] // Group-editor
    private async Task SuccessfullyAddAndEditEncryptedPasswordForOtherUser(string callingUsername, string targetUsername)
    {
        var user = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   true,
                                                                                   false);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   false,
                                                                                   false);
    }


    [Theory]
    [InlineData(dumbledore)] // Admin (not in group or parent-group)
    [InlineData(severusSnape)] // Parent-group-admin
    [InlineData(minervaMcGonagall)] // Parent-group-editor
    [InlineData(godricGryffindor)] // Group-admin
    [InlineData(harryPotter)] // Group-editor
    private async Task AddSharedPasswordToGryffindorAndAddEncryptedPasswordsForUsersWithAccess(string username)
    {
        const string plaintext = "some-pw";
        var sharedPassword =
            await passwordTestService.AddSharedPasswordAsync(username, gryffindor, "password of " + username, plaintext);
        var usersWithAccess = await groupTestService.GetUsers(username, gryffindor, true);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(TestUser[username], sharedPassword.Id, usersWithAccess, plaintext, true, false);
    }

    [Theory]
    // From user with access --to--> user of another group (which is not parent-group)
    [InlineData(dumbledore, cedricDiggory)] // Hufflepuff group-editor
    [InlineData(minervaMcGonagall, cedricDiggory)] // Hufflepuff group-editor
    [InlineData(godricGryffindor, cedricDiggory)] // Hufflepuff group-editor
    [InlineData(harryPotter, cedricDiggory)] // Hufflepuff group-editor
    private async Task AddAndEditEncryptedPasswordForOtherUserThatHasNoAccess(string callingUsername, string targetUsername)
    {
        var user = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   true,
                                                                                   true);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   false,
                                                                                   true);
    }

    [Theory]
    // From user without access --to--> sys-admin
    [InlineData(lunaLovegood, dumbledore)]
    [InlineData(garrickOllivander, dumbledore)]
    [InlineData(dracoMalfoy, dumbledore)]
    [InlineData(horaceSlughorn, dumbledore)]
    [InlineData(cedricDiggory, dumbledore)]
    [InlineData(viktorKrum, dumbledore)]
    private async Task AddAndEditEncryptedPasswordWithoutHavingAccess(string callingUsername, string targetUsername)
    {
        var user = TestUser[callingUsername];
        var targetUser = TestUser[targetUsername];
        var sharedPasswordOfGryffindor = testPasswords[PasswordGryffindorForEdits];

        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   true,
                                                                                   true);
        await passwordTestService.SuccessfulSyncEncryptedPasswordForOtherUserAsync(user,
                                                                                   sharedPasswordOfGryffindor.Id,
                                                                                   new List<User> { targetUser },
                                                                                   SharedPasswordString,
                                                                                   false,
                                                                                   true);
    }

    [Theory]
    [InlineData(dumbledore)] // Sys-admin
    [InlineData(godricGryffindor)] // Some group-admin
    [InlineData(harryPotter)] // Some group-editor
    [InlineData(ronWeasly)] // Some group-viewer
    private async Task GetEncryptedPasswordForNonExistingSharedPassword(string callingUsername)
    {
        var user = TestUser[callingUsername];
        var randomGuid = Guid.NewGuid();

        const string query = @"query encryptedSharedPasswordString($passwordId: Guid!) {
                      encryptedSharedPasswordString(passwordId: $passwordId)
                    }";

        var variables = new { passwordId = randomGuid };
        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(user.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    #endregion

    #region move

    // TODO: add variations: multiple passwords, from multiple source groups (yes, the API allows that)
    [Theory]
    [InlineData(dumbledore, gryffindor, slytherin, ronWeasly, dracoMalfoy)]
    [InlineData(severusSnape, slytherin, hogwarts, dracoMalfoy, nearlyHeadlessNick)]
    private async Task AdminCanMovePasswordsToAnotherGroup(string callingUsername, string sourceGroupname, string destinationGroupName, string sourceGroupUserName,
        string destinationGroupUsername)
    {
        var sourceUser = TestUser[sourceGroupUserName];
        var destinationUser = TestUser[destinationGroupUsername];

        var passwordToBeMoved = await passwordTestService.AddSharedPasswordAsync(callingUsername, sourceGroupname, PasswordForMovingBetweenGroups, SharedPasswordString);

        var keyPairsForUsersInTargetGroup = await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(callingUsername, destinationGroupName);
        var keyPairsForUsersInParentGroupWithAdmins = await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(callingUsername, sourceGroupname);
        var keyPairs = keyPairsForUsersInTargetGroup
            .Where(pk => !keyPairsForUsersInParentGroupWithAdmins.Contains(pk))
            .ToList();
        var encryptions = keyPairs
            .Select(kp => new
            {
                sharedPasswordId = passwordToBeMoved.Id,
                keyPairId = kp.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(SharedPasswordString, kp.PublicKeyString)
            });

        await passwordTestService.MoveSharedPasswordToAnotherGroupJsonAsync(callingUsername, new List<Guid> { passwordToBeMoved.Id }, destinationGroupName, encryptions);

        const string queryAccess = @"{ sharedPasswords { " + SharedPasswordFragment + @" } }";

        var json = await MakeGraphQLRequestAsync(sourceUser.Email, queryAccess);
        var foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["sharedPasswords"]!.ToString());
        Assert.NotNull(foundPasswords);
        Assert.DoesNotContain(foundPasswords!, sp => sp.Id == passwordToBeMoved.Id);

        json = await MakeGraphQLRequestAsync(destinationUser.Email, queryAccess);
        foundPasswords = JsonConvert.DeserializeObject<List<SharedPassword>>(json["data"]!["sharedPasswords"]!.ToString());
        Assert.Contains(foundPasswords!, sp => sp.Id == passwordToBeMoved.Id);
    }

    [Theory]
    [InlineData(dracoMalfoy, slytherin, hogwarts)]
    [InlineData(harryPotter, slytherin, gryffindor, dracoMalfoy)]
    private async Task NonAdminCannotMovePasswordsToAnotherGroup(string callingUsername, string sourceGroupname, string destinationGroupName, string userToCreatePassword = null)
    {
        userToCreatePassword ??= callingUsername;

        var passwordToBeMoved = await passwordTestService.AddSharedPasswordAsync(userToCreatePassword,
                                                                                 sourceGroupname,
                                                                                 PasswordForMovingBetweenGroups,
                                                                                 SharedPasswordString);

        var keyPairs = await groupTestService.GetKeyPairsOfUsersWithAccessToGroup(dumbledore, destinationGroupName);
        var encryptions = keyPairs
            .Select(kp => new
            {
                sharedPasswordId = passwordToBeMoved.Id,
                keyPairId = kp.Id,
                encryptedPasswordString = CryptoHelper.EncryptAsymmetrical(SharedPasswordString, kp.PublicKeyString)
            });

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => passwordTestService.MoveSharedPasswordToAnotherGroupJsonAsync(callingUsername,
                                                                         new List<Guid> { passwordToBeMoved.Id }, destinationGroupName, encryptions));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);
    }

    [Theory]
    [InlineData(dumbledore, gryffindor, harryPotter)]
    [InlineData(harryPotter, gryffindor)]
    private async Task AdminAndGroupEditorCanMoveSharedToCompany(string callingUsername, string sourceGroupname, string moveToUsername = null)
    {
        moveToUsername ??= callingUsername;

        var sourceGroup = TestGroups[sourceGroupname];
        var moveToUser = TestUser[moveToUsername];

        var passwordToBeMoved = await passwordTestService.AddSharedPasswordAsync(moveToUsername,
                                                                                 sourceGroupname,
                                                                                 PasswordForMovingBetweenGroups,
                                                                                 SharedPasswordString);

        await passwordTestService.MoveSharedPasswordToCompanyAsync(callingUsername, passwordToBeMoved.Id,
                                                                   moveToUsername);

        Assert.DoesNotContain(DbContext.Groups.Where(g => g.Id == sourceGroup.Id).SelectMany(g => g.SharedPasswords),
                              sp => sp.Id == passwordToBeMoved.Id);
        Assert.Contains(DbContext.SharedPasswords.Where(sp => sp.GroupId == null && sp.CreatedById == moveToUser.Id),
                        sp => sp.Id == passwordToBeMoved.Id);
    }

    [Theory]
    [InlineData(dumbledore, gryffindor, harryPotter)]
    [InlineData(harryPotter, gryffindor)]
    private async Task AdminAndGroupEditorCanMoveCompanyToShared(string callingUsername, string destinationGroupname, string moveFromUsername = null)
    {
        moveFromUsername ??= callingUsername;

        var destinationGroup = TestGroups[destinationGroupname];
        var moveFromUser = TestUser[moveFromUsername];

        var passwordToBeMoved = await passwordTestService.AddCompanyPasswordAsync(moveFromUsername,
                                                                                  PasswordForMovingBetweenGroups,
                                                                                  SharedPasswordString);

        await passwordTestService.MoveCompanyPasswordToSharedGroupPasswordAsync(callingUsername,
                                                                                passwordToBeMoved.Id,
                                                                                destinationGroupname);

        Assert.DoesNotContain(DbContext.SharedPasswords.Where(sp => sp.GroupId == null && sp.CreatedById == moveFromUser.Id),
                              sp => sp.Id == passwordToBeMoved.Id);
        Assert.Contains(DbContext.Groups.Where(g => g.Id == destinationGroup.Id).SelectMany(g => g.SharedPasswords),
                        sp => sp.Id == passwordToBeMoved.Id);
    }

    [Theory]
    [InlineData(harryPotter, gryffindor, dumbledore)]
    [InlineData(dracoMalfoy, gryffindor, harryPotter, harryPotter)]
    [InlineData(dracoMalfoy, slytherin, harryPotter)]
    private async Task NotAuthorisedWhenMoveSharedToCompany(string callingUsername, string sourceGroupname, string moveToUsername, string userToCreatePassword = null)
    {
        userToCreatePassword ??= callingUsername;

        var callingUser = TestUser[callingUsername];
        var sourceGroup = TestGroups[sourceGroupname];
        var moveToUser = TestUser[moveToUsername];

        var passwordToBeMoved = await passwordTestService.AddSharedPasswordAsync(userToCreatePassword,
                                                                                 sourceGroupname,
                                                                                 PasswordForMovingBetweenGroups,
                                                                                 SharedPasswordString);

        const string query = @"mutation ($passwordsToMoveIds: [Guid]!, $destinationUserId: Guid!) {
                    sharedPassword {
                      sharedMoveToCompany(passwordsToMoveIds: $passwordsToMoveIds, destinationUserId: $destinationUserId) {
                            "
            + SharedPasswordFragment
            + @"
                      }
                    }
                  }";
        var variables = new { passwordsToMoveIds = new[] { passwordToBeMoved.Id }, destinationUserId = moveToUser.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        Assert.Contains(DbContext.Groups.Where(g => g.Id == sourceGroup.Id).SelectMany(g => g.SharedPasswords),
                        sp => sp.Id == passwordToBeMoved.Id);
        Assert.DoesNotContain(DbContext.SharedPasswords.Where(sp => sp.GroupId == null && sp.CreatedById == moveToUser.Id),
                              sp => sp.Id == passwordToBeMoved.Id);
    }

    [Theory]
    [InlineData(harryPotter, gryffindor, dumbledore)]
    [InlineData(harryPotter, gryffindor, dracoMalfoy)]
    [InlineData(dracoMalfoy, gryffindor)]
    private async Task NotAuthorisedWhenMoveCompanyToShared(string callingUsername, string destinationGroupname, string moveFromUsername = null)
    {
        moveFromUsername ??= callingUsername;

        var callingUser = TestUser[callingUsername];
        var destinationGroup = TestGroups[destinationGroupname];
        var moveFromUser = TestUser[moveFromUsername];

        var passwordToBeMoved = await passwordTestService.AddCompanyPasswordAsync(moveFromUsername,
                                                                                  PasswordForMovingBetweenGroups,
                                                                                  SharedPasswordString);

        const string query = @"mutation ($passwordsToMoveIds: [Guid]!, $destinationGroupId: Guid!) {
                    sharedPassword {
                      companyMoveToShared(passwordsToMoveIds: $passwordsToMoveIds, destinationGroupId: $destinationGroupId) {
                            "
            + SharedPasswordFragment
            + @"
                      }
                    }
                  }";
        var variables = new { passwordsToMoveIds = new[] { passwordToBeMoved.Id }, destinationGroupId = destinationGroup.Id };

        var exception = await Assert.ThrowsAsync<ApiClientException>(() => MakeGraphQLRequestAsync(callingUser.Email, query, JsonConvert.SerializeObject(variables)));
        Assert.Equal(ErrorCode.UserIsNotAuthorized, exception.ErrorCode);

        Assert.Contains(DbContext.SharedPasswords.Where(sp => sp.GroupId == null && sp.CreatedById == moveFromUser.Id),
                        sp => sp.Id == passwordToBeMoved.Id);
        Assert.DoesNotContain(
            DbContext.Groups.Where(g => g.Id == destinationGroup.Id).SelectMany(g => g.SharedPasswords),
            sp => sp.Id == passwordToBeMoved.Id);
    }

    #endregion

    #region missing encryptions

    [Fact]
    private async Task GetMissingEncryptionsAfterAddPassword()
    {
        const string username = dumbledore;
        var user = TestUser[username];
        const string groupname = gryffindor;
        var addedPassword = await passwordTestService.AddSharedPasswordAsync(username, groupname, "new name", "some text",
                                                                             new List<Guid>());

        var missingEncryptions = await passwordTestService.RequestMissingEncryptionsForSharedPassword(user, addedPassword.Id);

        Assert.Empty(missingEncryptions);
    }


    [Theory]
    [InlineData(dumbledore, viktorKrum, new[] { gryffindor }, null)]
    [InlineData(dumbledore, viktorKrum, new[] { gryffindor, slytherin, hufflepuff }, null)]
    // TODO: remove this list of subgroups with a generic call to the API
    [InlineData(dumbledore, viktorKrum, new[] { hogwarts }, new[] { gryffindor, slytherin, hufflepuff, ravenclaw, roomOfRequirement })]
    private async Task GetMissingEncryptionsAfterUserAddedToGroup(string addingUsername, string usernameAdded,
        string[] groupNamesToBeAddedTo, string[] groupnamesThatAreSubgroupsOfTargetGroups)
    {
        var userAdding = TestUser[addingUsername];
        var userToBeAdded = TestUser[usernameAdded];

        foreach (var groupNameToBeAdded in groupNamesToBeAddedTo)
        {
            await groupTestService.AddUserToGroup(userAdding, usernameAdded, groupNameToBeAdded, GroupRole.GroupViewer, groupnamesThatAreSubgroupsOfTargetGroups);
        }

        const string query = @"query MissingEncryptions($userId: Guid!) { missingEncryptionsForUser(userId: $userId) }";
        var variables = new
        {
            userId = userToBeAdded.Id
        };
        var json = await MakeGraphQLRequestAsync(userToBeAdded.Email, query, JsonConvert.SerializeObject(variables));
        var sharedPasswordGuidsOfMissingEncryptions = JsonConvert.DeserializeObject<List<Guid>>(json["data"]!["missingEncryptionsForUser"]!.ToString());

        Assert.Empty(sharedPasswordGuidsOfMissingEncryptions);
    }

    #endregion

    #region helper

    private const string SharedPasswordFragment = @"id name login securityRating websiteURL notes updatedAt createdAt isSavedAsOfflinePassword tags { id name color }";

    #endregion
}