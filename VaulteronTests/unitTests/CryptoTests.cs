﻿using System;
using System.Linq;
using VaulteronDatabase.SeedData;
using Xunit;
using Xunit.Abstractions;

namespace VaulteronTests.unitTests;

/// <summary>
/// This class is used to test the crypto implementation in C#
/// These crypto functions are only used to generate good seed data
/// This class not only tests that the crypto works within itself but that it also works the same way as the JS crypto
/// </summary>
public class CryptoTests
{
    private readonly ITestOutputHelper testLogger;
    private readonly ICryptoHelper cryptoHelper;

    public CryptoTests(ITestOutputHelper testLogger)
    {
        this.testLogger = testLogger;
        cryptoHelper = new CryptoHelper();
    }


    [Fact]
    private void TestAsymmetricalCrypto()
    {
        const string loginPassword = "password12345!";
        const string plaintext = "some-plain-text-message";

        var keyPair = cryptoHelper.CreateKeyPair(loginPassword, false);
        var encryptedText = cryptoHelper.EncryptAsymmetrical(plaintext, keyPair.PublicKey);
        var decryptedText =
            cryptoHelper.DecryptAsymmetrical(encryptedText, loginPassword, keyPair.EncryptedPrivateKey);
        Assert.Equal(plaintext, decryptedText);
    }

    [Fact]
    private void TestSymmetricalCrypto()
    {
        const string loginPassword = "login-12345";
        const string plaintext = "some-plaintext";

        var encryptedText = cryptoHelper.EncryptSymmetrically(plaintext, loginPassword);
        var decryptedText = cryptoHelper.DecryptSymmetrically(encryptedText, loginPassword);
        Assert.Equal(plaintext, decryptedText);

        // Test using different sessions aka new objects
        var secondCryptoHelper = new CryptoHelper();
        var secondDecryptedText = secondCryptoHelper.DecryptSymmetrically(encryptedText, loginPassword);
        Assert.Equal(plaintext, secondDecryptedText);
    }


    [Theory]
    [InlineData("test", "6f822c65b20051dc47eccbb330b690af8ec966187d3b3307424c0de1375a3e6de4ee3353c5f71c28f6b6863008ce0b5e80338d01f9bdf38ef1f96653a3dfdfe1")]
    [InlineData("test-test_", "e9ce404fa235719c50aad1bb7ff50434642e0955b670fd37411e7e202289f23c9d1612f6d0a95db76639cb7933fafd81282680d4982da15ed5ff9e0e39dd021a")]
    [InlineData("test-numbers_1234567890", "c8e80405cf72b46b47c922a5551a2ce06a89184dbc3573f73ad528fec3f491702ededde648c14808247507885957a90e94f7c176221b37ed009bbb9338e84e0e")]
    [InlineData("test-germanSpecialChars_ülöäß", "78304ffd173836e2bd040d0944da4ead4f7477a238b6db38a0111ec5b1d4b47932b096286061d05b9004e0dbbd6cc51b164656b8b6151391a0aebb916b56033d")]
    [InlineData("test-specialChars_!§$%&/()=?*';:", "5c4ea82ee347d23a78aaad16fc45fb8f0fc8802f53f1b063df0d958f4818dd100da830628c3b5e063c22eede5510a7cefeadf04ffac119ca34d0daf0d2051417")]
    private void Manual_CheckIfHashingIsJSCompliant(string input, string jsHash)
    {
        /*
let inlineData = `
    [InlineData("test", "")]
    [InlineData("test-test_", "")]
    [InlineData("test-numbers_1234567890", "")]
    [InlineData("test-germanSpecialChars_ülöäß", "")]
    [InlineData("test-specialChars_!§$%&/()=?*';:", "")]`;
let resultingInlineData = "";
function f(){
return new Promise(async (resolve, reject)=>{
    for (let line of inlineData.replaceAll("\r","\n").replaceAll("\n\n","\n").split("\n")) {
        if (line == "") continue;
        let regex = /.*\[InlineData\("(.*)".*""\)\]/;
        let match = regex.exec(line);
        let input = match[1];
        let hash = await window.globalCryptoManager.createHash(input);
        resultingInlineData += `[InlineData("${input}", "${hash}")]\n`;
    }
    console.log(resultingInlineData);
    resolve();
});
}
f();
         */

        var csHash = cryptoHelper.ComputePasswordHash(input);
        Assert.Equal(jsHash, csHash);
    }

    /// <summary>
    /// Use the following JS code to convert sample InlineData into the ones with the JS encrypted texts
    /// Note: Everytime the JS or C# crypto is changes this manual procedure must be done again!
    /// </summary>
    /// <param name="loginPassword"></param>
    /// <param name="plaintext"></param>
    /// <param name="jsEncryptedText"></param>
    [Theory]
    [InlineData("12345", "test",
                "d4133f292c14d5024c607c4d1eeaacc4140edcf302e3b5a7187aodkrzf8pVmNAdMKKRCda4h8Kjmk=")]
    [InlineData("12345", "test-test_",
                "3e01469bd019f2a4472b82a57eca776a7468df9f62d6db5b109fMfwjcPD0feTWo3/7xtG9P1vFq5YcuIiLDxk=")]
    [InlineData("12345", "test-numbers_1234567890",
                "fbdf4fef323f0fe0efa0e6a20a477fd1f513ca70e1f45e6ee72emJSNdG+cSCOaP54tAqcMq9We+g0Lvph8fABIQ1m0Vy5VprqRTaOa")]
    [InlineData("12345", "test-germanSpecialChars_ülöäß",
                "24137ab5499a01e4b02a0eaa06f1e53cab836a6bead54e0d7d9aLvnh4woQQ0AYOticL62ymGU7G9m2BJktBhwMXa4VkzuYxh/+bZqEAicM4DolpKEtGw==")]
    [InlineData("12345", "test-specialChars_!§$%&/()=?*';:",
                "25f0a6546ab9a8b1aec06f8c9fb9ae2b565a4fa7566412bfee087obgH7GrybT/3VpHiBxXDE55mvZKksh5tQMtimCmjO4FdeN7AsVJblTOuctj6VEqCA==")]
    private void Manual_CheckIfCryptoIsJSCompliant(string loginPassword, string plaintext, string jsEncryptedText)
    {
        /*
let inlineData = `
    [InlineData("12345", "test", "")]
    [InlineData("12345", "test-test_", "")]
    [InlineData("12345", "test-numbers_1234567890", "")]
    [InlineData("12345", "test-germanSpecialChars_ülöäß", "")]
    [InlineData("12345", "test-specialChars_!§$%&/()=?*';:", "")]`;
let resultingInlineData = "";
function f(){
return new Promise(async (resolve, reject)=>{
    for (let line of inlineData.replaceAll("\r","\n").replaceAll("\n\n","\n").split("\n")) {
        if (line == "") continue;
        let regex = /.*\[InlineData\("(.*)".*"(.*)".*""\)\]/;
        let match = regex.exec(line);
        let loginPassword = match[1];
        let plaintext = match[2];
        let encrypted = await window.globalCryptoManager.encryptUsingUserCredentials(plaintext, loginPassword);
        resultingInlineData += `[InlineData("${loginPassword}", "${plaintext}", "${encrypted}")]\n`;
    }
    console.log(resultingInlineData);
    resolve();
});
}
f();
         */
        var csEncryptedText = cryptoHelper.EncryptSymmetrically(plaintext, loginPassword);

        // Check if C# crypto within itself
        var csDecryptedText = cryptoHelper.DecryptSymmetrically(csEncryptedText, loginPassword);
        Assert.Equal(plaintext, csDecryptedText);

        // Check if C# crypto encrypt the same way as JS
        const int ALGORITHM_SALT_SIZE = 16;
        const int ALGORITHM_NONCE_SIZE = 10;

        var saltString = jsEncryptedText.Substring(0, 2 * ALGORITHM_SALT_SIZE);
        var salt = Enumerable.Range(0, ALGORITHM_SALT_SIZE)
            .Select(i => Convert.ToByte(saltString.Substring(2 * i, 2), 16))
            .ToArray();

        var ivString = jsEncryptedText.Substring(2 * ALGORITHM_SALT_SIZE, 2 * ALGORITHM_NONCE_SIZE);
        var ivBytes = Enumerable.Range(0, ALGORITHM_NONCE_SIZE)
            .Select(i => Convert.ToByte(ivString.Substring(2 * i, 2), 16))
            .ToArray();

        var csEncrypted = new AESCrypto().EncryptString(plaintext, loginPassword, ivBytes, salt);
        Assert.Equal(jsEncryptedText, csEncrypted);

        // Check if C# can decrypt JS encrypted
        var csDecryptedJSText = cryptoHelper.DecryptSymmetrically(jsEncryptedText, loginPassword);
        Assert.Equal(plaintext, csDecryptedJSText);
    }

    /// <summary>
    /// TODO: an automated solution would be to create a Resolver for testing that has routes with the C# crypto
    ///         and when run it will boot up the JS code will automatically do JS crypto and use the C# routes to verify it
    ///         at the end there could be a route which will be called when some crypto error occured which will triggered Assert.True(false)
    /// </summary>
    [Fact]
    private void Manual_CrossTestAsymmetricalCryptoWithJS()
    {
        /*
         * PART ONE - PART ONE - PART ONE - PART ONE - PART ONE - PART ONE - PART ONE - PART ONE - PART ONE   
         * Please start the server and execute the following JS which you must then copy into the variable below
         * 
var asymmPlainText = "some-asymm-plain-text-message";
var loginPassword = "some-login-PW12345!!";
window.globalCryptoManager.createHash(loginPassword).then(hashedLoginPassword => {
window.globalCryptoManager.createAsymmetricalKeyPair().then(kp => {
    window.globalCryptoManager.encryptWithPublicKey(asymmPlainText, kp.publicKey).then(encryptedMessage => {
        window.globalCryptoManager.encryptUsingUserCredentials(kp.privateKey, hashedLoginPassword).then(r3 => {
            console.log(`
#region JS reponse
const string plaintext = "${asymmPlainText}";
const string loginPassword = "${loginPassword}";
const string hashedLoginPassword = "${hashedLoginPassword}";
const string jsResponse = @"${kp.publicKey}###${kp.privateKey}###${r3}###${encryptedMessage}";
#endregion`
            );
        });
    });
});
});
         */

        // ----------------- PASTE HERE ---------- PASTE HERE ----------------------

        #region JS reponse

        const string plaintext = "some-asymm-plain-text-message";
        const string loginPassword = "some-login-PW12345!!";
        const string hashedLoginPassword =
            "5809c4376590145ff2955ef8106a7fcf0ca727dc56048e43d7df263a2cb9362bf1872f684b46abd90ff9234de0990674b6ad6a191e77cc47b67895b933a6278d";
        const string jsResponse = @"-----BEGIN RSA PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxdxYbCgYooo6XBM8ELh6
NGApEkg6+d0HbJrcV51NOits7je0hwaeojj+CsUrnB0kC4/0uZtDb/ZeMN8CNSTs
V3X62San1Ocv+TMhM6sp83AZIYGC5j/pfoCYZzoY1AJno7U3QLILRRDiv5EbVwSF
TeBA1HDnr3g1X71OoAIJ6kHvo63oEh2ikjXwbdua1PZMcta6n+4HTV9dNQhsgqcS
fdIFr90yT06ay5pBVEvEu76k9ObzDzAoqn6bW5u9TCSzaM6EtzZU9qftDcZjvMt/
2Bj5Ex+B7DLTeGr02gmNGWoWMgEnGvbSVJODd4C7f3cslDV8MiThH7lN8xl3mvzn
HvbD8OyEDfQa/C5QbQYLR+ouLQJ2Dagikhn0bpye+iVTbVRpPRstRvRRlJZ4DyyI
bcr0Q36QJQN49H+dHlzVpBYQxjohJgDXq9Hsax7/OMOQzyiUXknDmo059Rj0Ag68
mXumyGslcsOsMsGrjCIAt9aLR9HWuDjQ+2MHVdExZ4sFvc3qNhLz3rcsc2L2HOP8
ELQiuYFDz9yufz1vz9uTt2pElI/J+aM3Zvn2Z7JhOpw4+pg5BU/zX/mUaB7krlRt
4jIJmRb+TBG7KWOemwkdFO+onOWaZuyE8zKYwO8T+h+0iLhgdZeAfUbWgXzh1cBc
Xrvy6p0IIVJW++9sSZ98ZYcCAwEAAQ==
-----END RSA PUBLIC KEY-----
###-----BEGIN RSA PRIVATE KEY-----
MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDF3FhsKBiiijpc
EzwQuHo0YCkSSDr53QdsmtxXnU06K2zuN7SHBp6iOP4KxSucHSQLj/S5m0Nv9l4w
3wI1JOxXdfrZJqfU5y/5MyEzqynzcBkhgYLmP+l+gJhnOhjUAmejtTdAsgtFEOK/
kRtXBIVN4EDUcOeveDVfvU6gAgnqQe+jregSHaKSNfBt25rU9kxy1rqf7gdNX101
CGyCpxJ90gWv3TJPTprLmkFUS8S7vqT05vMPMCiqfptbm71MJLNozoS3NlT2p+0N
xmO8y3/YGPkTH4HsMtN4avTaCY0ZahYyASca9tJUk4N3gLt/dyyUNXwyJOEfuU3z
GXea/Oce9sPw7IQN9Br8LlBtBgtH6i4tAnYNqCKSGfRunJ76JVNtVGk9Gy1G9FGU
lngPLIhtyvRDfpAlA3j0f50eXNWkFhDGOiEmANer0exrHv84w5DPKJReScOajTn1
GPQCDryZe6bIayVyw6wywauMIgC31otH0da4OND7YwdV0TFniwW9zeo2EvPetyxz
YvYc4/wQtCK5gUPP3K5/PW/P25O3akSUj8n5ozdm+fZnsmE6nDj6mDkFT/Nf+ZRo
HuSuVG3iMgmZFv5MEbspY56bCR0U76ic5Zpm7ITzMpjA7xP6H7SIuGB1l4B9RtaB
fOHVwFxeu/LqnQghUlb772xJn3xlhwIDAQABAoICAF3tgf5qNHOo6hZ+vRN09/J4
OdcnefPWxSj+BR8Emj5EkjQuNqGo91KsepETU+YLAWsJvlQETInQq5tIqsweyKGm
bNx8WM0dqO0EpDzagxJBQ3Tf/gufCJHYq0DJ2DKBJ8EUDHRGcUoqv3HQiWmQSFYV
YMFZDeuDRdc6z6HvBdTy0zfhHVvIPL5rI8WLLF/vwo/7tuGoRkgpPt6nrpeBh6o6
Mx/vAmyatKp2ApBpXa4AzV9DadxqcMurScY90ynb/F2DOIafIMttIYBaD0v3USv9
gvRcjzlZUYGHSNBfG2AWuA2p9W38ARJMausmrhvPL9b14w0M6+A7ADGpA9oMOAzo
hjWcDOCOUUiCURvGAlhI8O4ceHnMd4fSb5Vb2rIGWdHCax104AiwphmyiT4tp3eq
ezTNOKJVf+4DOJ93J40i2Q42+Xpj6jhuh86AyXmIJ58yCBAKsHQ9CD8Fv+kxoXv2
EOTRCPj7CW/iibm0WRcDtJ1bSFMDNWc5GgtrV9bupWUyg/kIAVuajfvrjzXsGDXE
TdWR63osEk4w7n9zkICYKeGzPNUKF0WW5rw+H/b/n/VUVXYOR1Gdez8bLZUe0XXk
G8GvlpxVkqOdO8mHK17X6BYPcLdDLgvcsR0jdy+ya5/KhgYujHwpWfpqQtf19qQp
LLm0NVY45x3/FzSZNtWtAoIBAQD67os+1blUCsxPjbKEyI/3Las29VCM5PwYaSVu
o/cEYe8WULmzb89hLoRVApfOVh8gLD/CEzVGQdAd4hnGTuhQDIL0DGdosLwnWNrb
2DtXPSOOoZbxOKtWkLZRCqQENjUpz1eDASoLC4QWl6wIQbO0vO+qgg0u10rdEA2s
4OjgrKVmjj3ZP6w5Qye5iKjqGf3J2fd4Q33dAmvuEXGFQ7x5xySqql4/OU5uDnkc
Y+9wVZFgp755ePSm4uO7LwA0CHcO7jZJ7cmuauxlRmkiPr5tDbyeQMUD/Ts0oNfB
XUCTKcNS9IAoG11xtQyy3VNsB5VuL3pQY4uz1BTDIHICx0e7AoIBAQDJ22UGuKom
fSqP3Xl0LBmlyMVjdRGKCMCxBWTXEiwX/wQF7eQPnxuM2Aqs+PazQt9oAy/yzPIO
9otfROSBzHR8q62+6yrLpA0jfNk7SkmVtN/b7ilzqvsVTIW0yxU7La4USzdwlGex
ba3EeEPLzLBZ+gMnhtfH7zHXhSxgNRNEf1nLzpAhn8s5rAGKF6vguCe0S+Co0G4L
iKABtO/j2LlGXdlf8PpknHrmZEu91YZBVhAQNVUsh0f8zTvo3pPL2Af3kaWt3z30
UeDYDdxkVke0cxNEJaB6OItGz6Vv4mqoXS8B4inBWFb15CTJLzV7MYOU007qwsVq
mFr/1g//ld6lAoIBAQDjZVTuhkPCc16fISuLxS0htFMaJMjwRXpmrLJtR01MvWsq
79C5WTO3P65QU186bzUZtL+6oil/F5j/26u72DoSgKQTgUIczzxS8+SxoYx1DESy
tqDQzKgIdG+jlV75Kh6XWvxVDbMJ+0+iIaSWfuPZf/Y4sE4+CoAfQLMD3XVsVUg2
XeSSgW8THVZcfgmEzDT+vPXUGGVw6Qbh4RDc0Xt1aBBEUMvDw6zdc2QxZCbRx9lM
m/ayofgjXC0/reIjRSCPeTPdXw2Lb/318Q7JJ87tiKtLRirD6LHEiZW790PTmN8W
x9dzgQj3SDjPxl4Y4e56wa+4Nd2eAy0lfruS5QCNAoIBAGIWUEMx3MyTKel6r5f6
DgR3SSOJMZqnoiri+htgYzjN3MCc1qOYpySLnrRqzkZRionacqRDXqRSvWrv6TIB
jeE+81DAqa0G7LN/6XdcI+GxYM5YLqBcR2IUKyytOoXlLqAB5NC57KFVMIk1/4u6
zT8LtBiy94vZ+ZhGyEd/wvsX/ySAHZ2ZLoG5yY227vhlxDViYarmjXLhUw9BQwiu
uWkuFYD/ck0NlNL7jaqTZYe+MFQP9nGK0zpO6HvCHIODWGu+xIl2334ng2C5a4z+
OB3BhW0p848whs1D/ZoepcFmdn8tzSh8qtUVpaJ1ZEGWz9+zxrdhKdkga6fB90g1
JVkCggEBANBhZRj3J64ulgZwfJNzR8ANYLilk571+Z1Y61mXLEi8I6HyJgCfkFX8
qfXtAlYeOzLakpLFWu+vrcN+/Sm6DmkVmE8A+KlOFBR5LLYSfg3uegqnxyMXqRK4
fUgsiupBxIltCgi+khbduVaLpK42wGVx8ZTSB5/ACjAgYbuR7I1PkhiMTf801y9H
NrelAhPkvuvm93GozN4whapR0gbY3jQOWjYuKklgNxpuOMpmufQrtjFCI/6QEerC
b7eysn5XdJHHS+2ytjslUH3AEPbhzNcTKh8Le94SH/fJb8aJpHdCYkpioc8qDltO
bg5sHZpFKXSLV2oEJadXh9spEoNoBJA=
-----END RSA PRIVATE KEY-----
###9ff8e73e2be88bcc994bca1e1ce2589b13febb7a46983f3d624d1z9jY6xnIudKomyDjpIbuBLfbfQw9CP4FAErexEojJHWPZKWjAaM0+/KW89obPo7AiU/mftqEC3rGv2VDWfPGNZgHaL904zAhO9tXhbEzH68uUMJGThJQOVylYjKl54zJVObxeAIXF4uJ9/4h6NVwSg/fqSlArBOeLOagWt3DfEt52Zbck1k210Ix0dgt6Cs0Ptm8+yGIly2A3XUb2Ssy/3fTHblqsXlXwBkAUcqwyJqH03hVMPbQ1fHYdvUoijZ7f0cCuh5/SOpzwYzkckymOxYWeRSPjOlJynWXu5qT/cbLpFFlBte6Xno/dlPrM0J92TUuEx787PCBPTbBy5A9g8r6WMjZR/AC2+N5qet3c/H51vY/JRaP7elPF+5bFOTOMVZBVSldH8mTUqmMel6p5oQdHmBUaVz4aSAAGqQbJWK70DB+JbMI9g+ZuH8snSABCm9DyytusDuD6b9t10RQnFQ1KCIbBeRYLWgjrh1kg8/cNVticiOB48lHo3JylX3ZlZP+EKHBXMrcf1MRJ/g2/usTYaIboTrk9av+GczdvhEplWE6WoIUxbrwfFBJwfhaP95Ob6mqp9bPhDQXPyS0pKx9JbHODh4raOje2IWHFfr3BYINhwRm1dAKBKKOBlqkFzTvUsNB4/koRDGEBSscw88W+WJu23VOH7iVU6NfEhF+TJ5wmBgI/5BMbbTo7NX74ZSs0FNyTO5CEWEdFxvdbexd2sKSe95B8ACRvAlID5OQGd/vknk7EFqC2SzlBreKKWelJhyeBuxE/lgQcxwZHKnbNF3mEN93Amd/hODAVY4OfjAhfCZX3kpmuXRN+IV5+K1G/ulWeIK27w8AjOv9cAKz1lCLl+yMiWsxRke3SBzwIZ8+OB4mVJl9K9iDX8D4cFPBGgQuuDv3iKO6DA3Yn8ZOCxqOEn0BiMu3ajEycP5bah/ygpDc7AMl3njZqTkM7booZClx3Ao/t6M3I5P0oOTbNW9wfTZip4D0MQYWcmsKXnaSLxVeO1W+1nht7erap/6Lw7s1hiBY9S8Y7Gt5PBAu0OwJz112lgI9xX/ps8MY1npiM98woJ1CFtgsyzohqwK7IKdX1WcbMdyO7X1vzaPJ85EpIPEk/T4hGPj46Q+VM4MvZ9E441Du4SML1mg59MlnZMbBdTXpQtf4CeJ64HDi+CPsU66fRxa43W34zIZnOqmVQF18GPyVF5980pxqU7liZ6WqL3cFvnBWD8djOZJK4w9nrp5hbE/4VxmAA2pqF5aIsKpziAzRu3d0qqlRwldzegYjV1osKmo2saDEB+jR+iogFi0WgDtVk9oh8vhwwq4weKXTdOjpkR/lS4VWtuLigoMMr9uckxlpPnwi15YUUrZvihrSGEJHyunL0jX6n/IhgDZN3+pO8vFmQ9lIBiPcshHVHLn59yh79dNdIeB2ScXKlqNHBOoUyJ5zBEjJ3R+8NLb6yA0Nc1crXcLE43+5UpDC/6XrtiiFhQI8a+bT+7lSV1dO+o+Wd+xSRZHi5hnTRfpQCWjy3jADARMV4uZ6kQFPyAphPe2rQAScSSfCnATpfwtrO37ub9dSjZo86psicoji0S+CvQ7ViOJ5fXKIb+RM1ZCTCRWOW9aYt8VoNqaUDwfpTG9cL9ke3hKpdhbaaOJyyyn78Yutq4tr5ikSGZXms2e9+0Oo9Z71nJPodlfW2j6AxyPvD1yCs/LFzv/VWIPEuChU9U0b+lPrVL0x90x5ZjDcqkp2sH6urabw0iG7/s6yBCFcPu3nIik5USbXWQ7CI3vr5enwCrkkqaL9dWQ+2RZX5SgmZMusLp887M7JbwN5DtHDEoBn8LrFFTPXPDSDqE3Mjr2leFVgos5ITvU6gO0f7mugu/1n18tt8XWTbPD9IfW7UpztU+F1j6XZU6v1rKEjs/DlG21DSVROAb6Ou3UpHFdILjKuKnCKQcYlyEguitmXvqfiPC5kR0atGXcPhgyQw2+vQPhk1QkzISQvwCoiWyVNZ8IGToQGYRHO8X2GR0cArYUc2RMUjsXnOloRQGb2ClOLPakyKPicGhTP36b2JdHHGuWpe2muow0U+uv6adPuaxZwTtwIhDryse3eXmWqsV3+qK3QN5BIC4tUTYSBEYppmey9rZtZEXYAAifGGJJRvbAHsX4wd7w1T6TEWZiaB11CtOr32hJa3+CQZ3MACn8KQ93VkEnzuVDSdEAP8XXP4rWGOkHOd6r+dCo0gQAPnOh5w3qUcGbqQ8bDUqTJcRDqfaNRVjjcQntBLTba8xucsK8DqL/Gkp9KAHHRKa6sXSiUCivd0iFgSdH18uSzdpUek9KoxTuuJkGSwc2ZsTICsPGCwGu0FgUPPiYKdm356YhY8yhCwYATxXrrkoF7NFt+mfjlcWnffWkwbzBJTABUGvxygX8758t24oo6IpceQkGPtxxJn60TEB4EfqvUd8/YWJXzIKDKdF6uuPZTNXcBE+aJPy7PClTgJHiMaEOEwipYGooxOsWpHIMcYAU1MIgYSlQKxtie+aE3xUeiO8pG3XVUdc/b8JVHytMm0gdAV8PCU+oeoj949CopdqiwGPbP2Gv34/6mWXjtV+25IDH1+H6BCFhYi9F3KGbgdcDVE8a++TdoKA02WAVyH3BdUJvRwbJoMl0i+jotlZ8jjq0G5kW33lyqkBjEjYUpAow9KW3xyJSWfYdcxDCYesphZj2CE2UYaA93p6M3n66W6cUe+wgHfyQ3QRFWtR7/v8Z7k6xy2B66q5++JxGhLs3nDof7PhRdRW9bfUoLGQ0E9ze3z0p+ugW2A4oofM78rK4ojmcOpZkowATpCiTtqtfhEYRa2Fcdp8s7thNNlQdg5CIIiTOiBCzAwMH8rFIcQYfVj9nHWKNmTPdtIob7FHsG4/3WV7XXDq/NkmZoP/ZoCafssdjliOp2MFhc1wFbIy16b0NQaBMWkLEfn6Ywl+4HwKzOx27P+fovubiTAsLHFd9FoOYi1IL2pp1UD38hmqCWFmRUIaZWltOSKageWKUGiku/lZKG74ajLnphQ83YkK6rKOgHl5tLktSfdgvcc9P8D1j6ibHRBn28+px8YgAOJNANnEXKsTl1xryeXN6cOMMcR+uT5LUUoHo5wwldEnMp/ysKd3ht06cYYjLpUUTFWtwGF+cDCN2UmYqEc4a9jXzIEWKzvNfD8/FjBEETQ9h+k4vPebhgdfXUhbltitBKEsLvxcoeBY8jDUDFOudmrYy6ovHpGkvur27JBAdMLSYXiVz2jH+7tJzi6tWKWpVhJRoKjJY0UtNKqu7HfVon/mshwv9PKM0EpWcX3SXxmdXfRzJrlS0tgcwYtQlayQsKpUVJtiNe34pjKWrHo9HqBAZsFCnFz2FYwCPKhaFCBbDuy7N+VnklB6zWcTvz0FZhPR0XXioFHy23JYot7pzLZ0NfpNgqEp130FfULlTHbH8s9Cct4wS+25UbLUNOpxQp+NNXoBlxGlonFb88NskGz7SGJwKOwUP8mn5aF5yCf/+wuOX1JM0se60viDxid4AhDewtqsP7J3/E1UUZsJOGpCluvFJvz4i9zyeG8JpXCCQIijEM/N3Rc8tHfylOxPf74DJPIo8Te6tDPquczl/Ocomp+KEHBLbtt6aV6So+wqwivq0Q+BxH5fccL5DoJpHZHKXdR1qaK80CFeRcbo6nZUtcgiyWP9N40gkyseZsa9TofVcjzMHl/ibsmJ8B0T2uh0Uv5q6Yq52Fm4bs7LDTmwyh+4rOJeP9oCX/WnBDFqupkHopPWqhjPiAhkWXSVJLC9HPJvqmsCwhVKdlZlcKz6CRrslLfaFVZLM9Rwqy1kfxuG6cmgKocr3NSpywsGrsEVzi5pQptScWws7ZRGiwmNmXbRNJrQGSAoGKhMKg/Tof9J4dQr1tJkelMoLA5Uf5hf9JuLHD51AB4hFHla99zrVom0qDQ9PjR74zw52/7Vb2c2gfuyB4arGSRtksXFsK6u7AMRtNJWIeonAqdsWAD/AzjKPGNk1zI03FNRHlDDYUtWVxEZb51dONmMtpl/awIBko5zqySQ5YYcgSTaVggxunSBLqAx6t0sU2rDlAP2XujoasJbenBtCo7XwF/oH6zyKvi2oEcDcyMmvdHybK00uyEuYjRVH927cCVLAUUuCMYK96Un6k2Jt2CczB2nPnYQ5+j9VSdzf8QtXzcPrh1kl2ja1id7Uxi0Vp/NMpbhrtBTE5V+rXUKNRhYZ6ww4eh3hJ3tjKM4xJrs0xl9KDty2AxADKNaSHQ5y9iSBxa4t+NFc9ZMu+JWuQMobVUDvQYw7NfmamSvjGpL9115xE5vuqTd61B4YeBRwW72YB5cQ6g3Lwl/leiYMUfTNiv03XM/tPGkoc4gg+IVBok9x6vAbATKqMa2W5r4Jsc+s2bC6aEFdy8mcH/kMdCeVFlzrMBbkM9T01G7+GH5s21r6jrc7cRi7CiJtdkgl###hwJRvlV/u8b5bbTm/lu94qZXNBOdN47ZfGvN1fXvBPYpRJqbP5vXeAwJofdthVHMiTjP6nGRCcVp4DPB4vWHrrnlWKQTXn4sp8NPS6TH6Dm7j6sUzVDvkgOEQgvEhYf3r00DbdbVuPo4L3U/+IFZrHyiG77EN94iif5grTcLgkJmMZjT709rlLzClPx6AaccsekvlljB1d/vMfr+ogKNK4Kg6qyX/HX9d0eyTpz8RWI+yPYe2P0Hcq9oI2eO8tNwtkMPRvBssnD1jCEYwrBY9LCNDOSGPbotvRtplYlO10CR/VY3RFIfdXItuomnzJBOVUncpu+hn+D2MKjvLFwOQ/2Anjeecz5x1bggHjwxIa1eOy4f4AG3Ghptf0EBv4rFgJqb+3sCvBMagU6mFxLbEuBtVLnvcgY/QPOSIxLKstI/TojOqn0akSGBvo687E20pRnsTocjjsUlWBKELPPdpK09KnovbBWuoK14MGvUt0eFEisFAZ8ImL9B6frgoX/weCmfWNgBXGKSxnlYjrNoMOvu6wC6QnLm3jPHu/NqAaOOLe6HuoJD6j29Y3oTEDeV22HCTRyWMKQ6uEaCPLpjLzGqw1QDtvAlzo35AqTN5BKmSee2CrFG/LeKnuhxTK+h3Rs9TjPAZOsq+vypPQLZfRe5UNPyRYr6iblxOTRVGIM=";

        #endregion

        // ----------------- END OF PASTE ---------- END OF PASTE ----------------------


        var splitter = jsResponse.Split("###");
        var publicKey = splitter[0];
        var privateKey = splitter[1];
        var encryptedPrivateKey = splitter[2];
        var encryptedMessage = splitter[3];

        // Verify public key is good
        var encryptedWithPublicKey = cryptoHelper.EncryptAsymmetrical(plaintext, publicKey);

        // Verify private key is good
        var decryptedWithPrivateKey = cryptoHelper.DecryptWithPrivate(encryptedWithPublicKey, privateKey);
        Assert.Equal(plaintext, decryptedWithPrivateKey);

        // Check that hashing the loginPasswords works the same
        var loginPasswordHash = cryptoHelper.ComputePasswordHash(loginPassword);
        Assert.Equal(hashedLoginPassword, loginPasswordHash);

        // Verify encrypted private key can be decrypted and used to asymmetrically decrypt the message
        var decryptedText =
            cryptoHelper.DecryptAsymmetrical(encryptedMessage, loginPassword, encryptedPrivateKey);
        Assert.Equal(plaintext, decryptedText);

        /*
         * PART TWO - PART TWO - PART TWO - PART TWO - PART TWO - PART TWO - PART TWO - PART TWO - PART TWO
         * The following C# code will generate an asymmetric keypair and the build some JS code you must execute in the browser console to verify
         */

        var csKeyPair = cryptoHelper.CreateKeyPair(loginPassword);
        var csEncryptedText = cryptoHelper.EncryptAsymmetrical(plaintext, csKeyPair.PublicKey);

        testLogger.WriteLine($@"
var publicKey = `{csKeyPair.PublicKey}`;
var privateKey = `{csKeyPair.PrivateKey}`;
var plaintext = `{plaintext}`;
var loginPassword = `{loginPassword}`;
var encryptedPrivateKey = `{csKeyPair.EncryptedPrivateKey}`;
var encryptedText = `{csEncryptedText}`;
// First test the the public and private keys are good
window.globalCryptoManager.encryptWithPublicKey(plaintext, publicKey).then(enc=>{{
    window.globalCryptoManager.decryptWithPrivateKey(enc, privateKey).then(res=>{{
        console.assert(plaintext === res);
        window.globalCryptoManager.decryptSharedPassword(encryptedPrivateKey, loginPassword, encryptedText).then(res2=>{{
            console.assert(plaintext === res2);
        }})
    }})
}})
");
    }
}