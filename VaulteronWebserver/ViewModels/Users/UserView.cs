﻿using System;
using VaulteronDatabase.Models;

namespace VaulteronWebserver.ViewModels.Users;

public class UserView
{
    public Guid Id { get; set; }
    public string Email { get; set; }

    public UserView(User user)
    {
        Id = user.Id;
        Email = user.Email;
    }
}