﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using VaulteronDatabase.Models;

namespace VaulteronWebserver.ViewModels.Users;

public class EncryptedPrivateKeyAndSharedPasswordsView
{
    public Guid PublicKeyId { get; set; }
    public string EncryptedPrivateKey { get; set; }
    public List<EncryptedSharedPasswordView> encryptedSharedPasswords { get; set; }

    [JsonConstructor]
    public EncryptedPrivateKeyAndSharedPasswordsView(Guid publicKeyId, string encryptedPrivateKey, List<EncryptedSharedPasswordView> encryptedSharedPasswords)
    {
        PublicKeyId = publicKeyId;
        EncryptedPrivateKey = encryptedPrivateKey;
        this.encryptedSharedPasswords = encryptedSharedPasswords;
    }

    public EncryptedPrivateKeyAndSharedPasswordsView(KeyPair keypair, List<EncryptedSharedPassword> encryptedSharedPasswords)
    {
        PublicKeyId = keypair.Id;
        EncryptedPrivateKey = keypair.EncryptedPrivateKey;
        this.encryptedSharedPasswords = encryptedSharedPasswords.Select(enc => new EncryptedSharedPasswordView(enc)).ToList();
    }
}

public class EncryptedSharedPasswordView
{
    public string EncryptedPasswordString { get; private set; }
    public Guid SharedPasswordId { get; private set; }

    [JsonConstructor]
    public EncryptedSharedPasswordView(string encryptedPasswordString, Guid sharedPasswordId)
    {
        EncryptedPasswordString = encryptedPasswordString;
        SharedPasswordId = sharedPasswordId;
    }

    public EncryptedSharedPasswordView(EncryptedSharedPassword enc)
    {
        EncryptedPasswordString = enc.EncryptedPasswordString;
        SharedPasswordId = enc.SharedPasswordId;
    }
}