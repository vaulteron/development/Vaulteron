﻿namespace VaulteronWebserver.ViewModels.Users;

public class EncryptedResetPasswordView
{
    public string EncryptedResetPassword { get; set; }
}