﻿using Fido2NetLib;

namespace VaulteronWebserver.ViewModels.Users;

public class WebAuthnLoginForm
{
    public AuthenticatorAssertionRawResponse AssertionResponse { get; set; }
    public bool IsPersistent { get; set; }
}