﻿using System.ComponentModel.DataAnnotations;

namespace VaulteronWebserver.ViewModels.Users;

public class RegisterForm
{
    [Required] public string Email { get; set; }
    [Required] public string Password { get; set; }
    [Required] public string Firstname { get; set; }
    [Required] public string Lastname { get; set; }
    public string CompanyName { get; set; }
    [Required] public string PublicKey { get; set; }
    [Required] public string EncryptedPrivateKey { get; set; }
}