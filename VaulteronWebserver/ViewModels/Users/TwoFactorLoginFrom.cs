﻿using System.ComponentModel.DataAnnotations;

namespace VaulteronWebserver.ViewModels.Users;

public class TwoFactorLoginFrom
{
    [Required] public string Code { get; set; }
    [Required] public bool IsPersistent { get; set; }
    [Required] public bool RememberClient { get; set; }
}