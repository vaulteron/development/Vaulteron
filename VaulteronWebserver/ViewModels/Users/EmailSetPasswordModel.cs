﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VaulteronDatabase.Models;

namespace VaulteronWebserver.ViewModels.Users;

public class EmailSetPasswordModel
{
    [Required] public Guid UserId { get; set; }
    [Required] public string Token { get; set; }
    [Required] public string OldPassword { get; set; }
    [Required] public string NewPassword { get; set; }
    [Required] public string PublicKey { get; set; }
    [Required] public string EncryptedPrivateKey { get; set; }
    [Required] public string PreviousPasswordHashEncryptedWithPublicKey { get; set; }
    [Required] public ICollection<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
}