﻿using System.ComponentModel.DataAnnotations;

namespace VaulteronWebserver.ViewModels.Users;

public class LoginForm
{
    [Required] public string Email { get; set; }
    [Required] public string Password { get; set; }
    [Required] public bool IsPersistent { get; set; }
}