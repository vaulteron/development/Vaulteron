﻿using System.ComponentModel.DataAnnotations;

namespace VaulteronWebserver.ViewModels.Users;

public class EmailPasswordReset
{
    [Required] public string Email { get; set; }

}