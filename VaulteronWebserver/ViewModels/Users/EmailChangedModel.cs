﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VaulteronWebserver.ViewModels.Users;

public class EmailChangedModel
{
    [Required] public Guid UserId { get; set; }
    [Required] public string Token { get; set; }
    [Required] public string NewEmail { get; set; }
}