﻿using System.Collections.Generic;
using VaulteronUtilities.Helper;

namespace VaulteronWebserver.ViewModels;

public class ApiExceptionResponseViewModel
{
    public string Message { get; set; }
    public IEnumerable<string> Reasons { get; set; }
    public ErrorCode ErrorCode { get; set; }

    public ApiExceptionResponseViewModel(ApiException apiException)
    {
        Message = apiException.Message;
        Reasons = apiException.Reasons;
        ErrorCode = apiException.ErrorCode;
    }
}