﻿using System;

namespace VaulteronWebserver.ViewModels;

public class GenericExceptionViewModel
{
    public string Message { get; set; }
    public string StackTrace { get; set; }

    public GenericExceptionViewModel(Exception e)
    {
        Message = e.Message;
    }

    public GenericExceptionViewModel(Exception e, bool isDevelopmentEnvironment) : this(e)
    {
        if (isDevelopmentEnvironment)
        {
            StackTrace = e.StackTrace;
        }
    }
}