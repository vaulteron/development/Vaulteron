﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using VaulteronDatabase.Models;
using VaulteronUtilities.Helper;

namespace VaulteronWebserver.ControllerHelper;

public static class ResponseErrorHelper
{
    public static async Task CreateCookieAndSignInAsync(User authUser, HttpContext httpContext)
    {
        // TODO: This is not how claims should be used. The .NET framework can manage this for us with the table AspnetUserClaims.
        var claims = new List<Claim>
        {
            new(ClaimTypes.Name, authUser.Id.ToString())
        };

        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

        var authProperties = new AuthenticationProperties
        {
            AllowRefresh = true,
            // Refreshing the authentication session should be allowed.

            ExpiresUtc = DateTimeOffset.UtcNow.AddDays(7),
            // The time at which the authentication ticket expires. A value set here overrides the ExpireTimeSpan option of CookieAuthenticationOptions set with AddCookie.

            IsPersistent = true,
            // Whether the authentication session is persisted across multiple requests. When used with cookies, controls
            // Whether the cookie's lifetime is absolute (matching the lifetime of the authentication ticket) or session-based.

            // IssuedUtc = <DateTimeOffset>,
            // The time at which the authentication ticket was issued.

            RedirectUri = "https://vaulteron.com"
            // The full path or absolute URI to be used as an http redirect response value.
        };

        await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
    }

    public static string PrepareRedirectToErrorPageUri(ApiException apiException) => PrepareRedirectToErrorPageUri(apiException.ToString());
    public static string PrepareRedirectToErrorPageUri(string errorMessage) => "/error?message=" + Uri.EscapeDataString(errorMessage);
}