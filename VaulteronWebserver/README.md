﻿## GraphQL Playground

Deployment URL: https://localhost:3000/api/ui

Settings:
```
{
"editor.cursorShape": "line",
"editor.fontFamily": "'Source Code Pro', 'Consolas', 'Inconsolata', 'Droid Sans Mono', 'Monaco', monospace",
"editor.fontSize": 14,
"editor.reuseHeaders": true,
"editor.theme": "dark",
"general.betaUpdates": false,
"prettier.printWidth": 80,
"prettier.tabWidth": 2,
"prettier.useTabs": false,
"request.credentials": "include",
"request.globalHeaders": {},
"schema.disableComments": true,
"schema.polling.enable": true,
"schema.polling.endpointFilter": "*localhost*",
"schema.polling.interval": 2000,
"tracing.hideTracingResponse": true,
"tracing.tracingSupported": true
}
```

# Useful commands

## Migrations
    
You can create new migrations and deleting the old ones by executing the .bat file in the Database project
Make sure you have dotnet-ef installed with `dotnet tool install --global dotnet-ef` 

