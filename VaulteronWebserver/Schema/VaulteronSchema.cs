﻿using System;
using Microsoft.Extensions.DependencyInjection;
using VaulteronWebserver.Mutations;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Schema;

public class VaulteronSchema : GraphQL.Types.Schema
{
    public VaulteronSchema(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        Query = serviceProvider.GetRequiredService<RootQuery>();
        Mutation = serviceProvider.GetRequiredService<RootMutation>();
    }
}