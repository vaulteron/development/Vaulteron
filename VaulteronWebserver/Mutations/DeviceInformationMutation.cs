﻿using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Resolver.Interfaces;

namespace VaulteronWebserver.Mutations;

public class DeviceInformationMutation : ObjectGraphType
{
    public DeviceInformationMutation(IDeviceInformationResolver deviceInformationResolver)
    {
        Field<BooleanGraphType>()
            .Name("Send")
            .Argument<NonNullGraphType<SendScamMailType>>("mail")
            .ResolveAsync(async context => await deviceInformationResolver.SendScamEmailAsync(
                    context.GetUserGuid(),
                    context.GetArgument<SendScamEmailModel>("mail")
                )
            );
    }
}