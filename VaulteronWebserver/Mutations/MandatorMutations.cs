﻿using System;
using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public class MandatorMutations : ObjectGraphType
{
    public MandatorMutations(IMandatorResolver mandatorResolver)
    {
        Field<MandatorType>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("mandatorId")
            .ResolveAsync(async context => await mandatorResolver.DeleteMandatorAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("mandatorId")));

        Field<MandatorType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<BooleanGraphType>>("twoFactorRequired")
            .ResolveAsync(async context => await mandatorResolver.EditMandator(
                context.GetUserGuid(),
                context.GetArgument<bool>("twoFactorRequired")));
    }
}