﻿using System;
using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class AccountTagMutation : ObjectGraphType
{
    public AccountTagMutation(IAccountTagResolver tagResolver)
    {
        Field<AccountTagType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddAccountTagInputType>>("tag")
            .ResolveAsync(async context => await tagResolver.AddAccountTagAsync(context.GetUserGuid(),
                                                                                context.GetArgument<AddAccountTagModel>("tag")
                          )
            );

        Field<AccountTagType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditAccountTagInputType>>("tag")
            .ResolveAsync(async context => await tagResolver.EditAccountTagAsync(context.GetUserGuid(),
                                                                                 context.GetArgument<EditAccountTagModel>("tag")
                          )
            );

        Field<AccountTagType>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("id")
            .ResolveAsync(async context => await tagResolver.RemoveAccountTagAsync(context.GetUserGuid(),
                                                                                   context.GetArgument<Guid>("id")
                          )
            );
    }
}