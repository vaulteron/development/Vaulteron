﻿using System;
using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronUtilities.Types.MutationResults;

namespace VaulteronWebserver.Mutations;

public sealed class TwoFactorMutation : ObjectGraphType
{
    public TwoFactorMutation(IAuthenticationResolver authResolver)
    {
        #region TOTP

        Field<TotpAuthenticatorKeyType>()
            .Name("getTotpAuthenticatorKey")
            .ResolveAsync(async context => await authResolver.GetTotpAuthenticatorKeyAsync(context.GetUserGuid()));

        Field<UserType>()
            .Name("enableTotp")
            .Argument<StringGraphType>("totp")
            .ResolveAsync(async context => await authResolver.SetEnabledTotpAsync(
                    context.GetUserGuid(),
                    context.GetArgument<string>("totp"),
                    true
                )
            );

        Field<UserType>()
            .Name("disableTotp")
            .ResolveAsync(async context => await authResolver.SetEnabledTotpAsync(
                    context.GetUserGuid(),
                    null,
                    false
                )
            );

        #endregion

        #region WebAuthn / FIDO2

        Field<WebauthnChallengeType>()
            .Name("getWebauthnChallenge")
            .ResolveAsync(async context => await
                authResolver.GetWebAuthnChallengeAsync(
                    context.GetUserGuid()
                )
            );
        Field<OwnUserType>()
            .Name("addWebauthnKey")
            .Argument<NonNullGraphType<StringGraphType>>("attestationResponse")
            .Argument<StringGraphType>("description")
            .ResolveAsync(async context => await
                authResolver.AddWebAuthnAsync(
                    context.GetUserGuid(),
                    context.GetArgument<string>("attestationResponse"),
                    context.GetArgument<string>("description")
                )
            );
        Field<OwnUserType>()
            .Name("removeWebauthnKey")
            .Argument<NonNullGraphType<GuidGraphType>>("webauthnKeyId")
            .ResolveAsync(async context => await
                authResolver.RemoveWebAuthnAsync(
                    context.GetUserGuid(),
                    context.GetArgument<Guid>("webauthnKeyId")
                )
            );

        #endregion
    }
}