﻿using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class UserMutation : ObjectGraphType
{
    public UserMutation(IUserResolver userResolver, IAuthenticationResolver authResolver)
    {
        Field<TwoFactorMutation>("twofactor", resolve: _ => new { });

        Field<UserType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddUserInputType>>("user")
            .ResolveAsync(async context => await userResolver.AddUserAsync(
                    context.GetUserGuid(),
                    context.GetArgument<AddUserModel>("user")
                )
            );

        Field<UserType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditUserInputType>>("user")
            .ResolveAsync(async context => await userResolver.EditUserAsync(
                    context.GetUserGuid(),
                    context.GetArgument<EditUserModel>("user")
                )
            );

        Field<UserType>()
            .Name(MasterDataQueryNames.ARCHIVE)
            .Argument<NonNullGraphType<GuidGraphType>>("id")
            .Argument<BooleanGraphType>("reactivate")
            .Argument<ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>(
                "encryptedPasswords", "Encryptions for SharedPasswords of all groups the user has access.")
            .ResolveAsync(async context => await userResolver.ArchiveUserAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("id"),
                context.GetArgument("reactivate", false),
                context.GetArgument<List<EncryptedSharedPassword>>("encryptedPasswords")));

        Field<UserType>()
            .Name("grantAdminPermissions")
            .Argument<NonNullGraphType<GuidGraphType>>("targetUserId")
            .Argument<NonNullGraphType<ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>(
                "encryptedPasswords", "Encryptions for Sharepasswords of all groups of mandator.")
            .ResolveAsync(async context => await userResolver.GrantAdminPermissionsAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("targetUserId"),
                context.GetArgument<List<EncryptedSharedPassword>>("encryptedPasswords")));


        Field<UserType>()
            .Name("revokeAdminPermissions")
            .Argument<NonNullGraphType<GuidGraphType>>("targetUserId")
            .ResolveAsync(async context => await userResolver.RevokeAdminPermissionsAsync(context.GetUserGuid(), context.GetArgument<Guid>("targetUserId")));

        Field<UserType>()
            .Name("changeOwnLoginPassword")
            .Argument<NonNullGraphType<StringGraphType>>("publicKeyString")
            .Argument<NonNullGraphType<StringGraphType>>("encryptedPrivateKey")
            .Argument<NonNullGraphType<StringGraphType>>("previousPasswordHashEncryptedWithPublicKey")
            .Argument<NonNullGraphType<StringGraphType>>("currentPassword")
            .Argument<NonNullGraphType<StringGraphType>>("newPassword")
            .Argument<NonNullGraphType<ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>(
                "encryptedSharedPasswords", "Encryptions for SharedPasswords of all groups the user has access.")
            .Argument<NonNullGraphType<ListGraphType<AddEncryptedAccountPasswordWithoutPublicKeyInputType>>>(
                "encryptedAccountPasswords", "Encryptions for all AccountPasswords of the user.")
            .ResolveAsync(async context => await authResolver.ChangeLoginPasswordAsync(
                context.GetUserGuid(),
                context.GetArgument<string>("publicKeyString"),
                context.GetArgument<string>("encryptedPrivateKey"),
                context.GetArgument<string>("previousPasswordHashEncryptedWithPublicKey"),
                context.GetArgument<string>("currentPassword"),
                context.GetArgument<string>("newPassword"),
                context.GetArgument<List<EncryptedSharedPassword>>("encryptedSharedPasswords"),
                context.GetArgument<List<EncryptedAccountPassword>>("encryptedAccountPasswords")
            ));

        Field<UserType>()
            .Name("resendAccountActivationEmail")
            .Argument<NonNullGraphType<GuidGraphType>>("targetUserId")
            .Argument<NonNullGraphType<StringGraphType>>("publicKey")
            .Argument<NonNullGraphType<StringGraphType>>("encryptedPrivateKey")
            .Argument<NonNullGraphType<StringGraphType>>("newTempLoginPassword")
            .Argument<NonNullGraphType<StringGraphType>>("newTempLoginPasswordHashed")
            .Argument<ListGraphType<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>("encryptedSharedPasswords")
            .ResolveAsync(async context => await userResolver.ResendAccountActivationEmailAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("targetUserId"),
                context.GetArgument<string>("publicKey"),
                context.GetArgument<string>("encryptedPrivateKey"),
                context.GetArgument<string>("newTempLoginPassword"),
                context.GetArgument<string>("newTempLoginPasswordHashed"),
                context.GetArgument<List<EncryptedSharedPassword>>("encryptedSharedPasswords")
            ));
    }
}