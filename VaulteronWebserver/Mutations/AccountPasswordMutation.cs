﻿using System;
using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class AccountPasswordMutation : ObjectGraphType
{
    public AccountPasswordMutation(IAccountPasswordResolver resolver)
    {
        Field<AccountPasswordType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddAccountPasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.AddAccountPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddAccountPasswordModel>("password")
                          )
            );

        Field<AccountPasswordType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditAccountPasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.EditAccountPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<EditAccountPasswordModel>("password")
                          )
            );

        Field<AccountPasswordType>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("id")
            .ResolveAsync(async context => await resolver.RemoveAccountPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("id")
                          )
            );
    }
}