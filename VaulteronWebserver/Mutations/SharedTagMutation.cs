﻿using System;
using GraphQL;
using GraphQL.Types;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class SharedTagMutation : ObjectGraphType
{
    public SharedTagMutation(ISharedTagResolver tagResolver)
    {
        Field<SharedTagType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddSharedTagInputType>>("tag")
            .ResolveAsync(async context => await tagResolver.AddSharedTagAsync(context.GetUserGuid(),
                                                                               context.GetArgument<AddSharedTagModel>("tag")
                          )
            );

        Field<SharedTagType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditSharedTagInputType>>("tag")
            .ResolveAsync(async context => await tagResolver.EditSharedTagAsync(context.GetUserGuid(),
                                                                                context.GetArgument<EditSharedTagModel>("tag")
                          )
            );

        Field<SharedTagType>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("id")
            .ResolveAsync(async context => await tagResolver.RemoveSharedTagAsync(context.GetUserGuid(),
                                                                                  context.GetArgument<Guid>("id")
                          )
            );
    }
}