﻿using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class GroupMutations : ObjectGraphType
{
    public GroupMutations(IGroupResolver groupResolver)
    {
        Field<GroupType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<GroupInputType>>("group")
            .ResolveAsync(async context => await groupResolver.AddGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Group>("group")
                          )
            );

        Field<ListGraphType<GroupType>>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("groupId")
            .ResolveAsync(async context => await groupResolver.RemoveGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("groupId")
                          )
            );

        Field<GroupType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditGroupInputType>>("group")
            .ResolveAsync(async context => await groupResolver.EditGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<EditGroupModel>("group")
                          )
            );

        Field<ListGraphType<GroupType>>()
            .Name(MasterDataQueryNames.ARCHIVE)
            .Argument<NonNullGraphType<GuidGraphType>>("groupId")
            .Argument<BooleanGraphType>("reactivate", "default: false to archive; true to un-archive")
            .ResolveAsync(async context => await groupResolver.ArchiveGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("groupId"),
                              context.GetArgument<bool?>("reactivate") ?? false
                          )
            );

        Field<GroupType>()
            .Name("move")
            .Argument<NonNullGraphType<GuidGraphType>>("groupToMoveId")
            .Argument<GuidGraphType>("newParentGroupId")
            .Argument<ListGraphType<EncryptedSharedPasswordInputType>>("encryptedSharedPasswords",
                                                                       "Encryptions of all SharedPasswords of moved Group and Subgroups, for all users having access to new ParentGroup, but did not have access to previous ParentGroup.")
            .ResolveAsync(async context => await groupResolver.MoveGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("groupToMoveId"),
                              context.GetArgument<Guid?>("newParentGroupId"),
                              context.GetArgument<ICollection<EncryptedSharedPassword>>("encryptedSharedPasswords"))
            );

        Field<UserType>()
            .Name("addUser")
            .Argument<NonNullGraphType<AddUserGroupType>>("user")
            .ResolveAsync(async context => await groupResolver.AddUserToGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddUserGroupModel>("user")
                          )
            );

        Field<UserType>()
            .Name("editUser")
            .Argument<NonNullGraphType<EditUserGroupInputType>>("user")
            .ResolveAsync(async context => await groupResolver.EditUserInGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<EditUserGroupModel>("user")
                          )
            );

        Field<UserType>()
            .Name("deleteUser")
            .Argument<NonNullGraphType<GuidGraphType>>("groupId")
            .Argument<NonNullGraphType<GuidGraphType>>("userId")
            .ResolveAsync(async context => await groupResolver.RemoveUserFromGroupAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("groupId"),
                              context.GetArgument<Guid>("userId")
                          )
            );
    }
}