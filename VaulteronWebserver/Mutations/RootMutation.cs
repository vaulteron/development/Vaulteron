﻿using GraphQL.MicrosoftDI;
using GraphQL.Types;

namespace VaulteronWebserver.Mutations;

public sealed class RootMutation : ObjectGraphType
{
    public RootMutation()
    {
        Name = "Vaulteron_root_mutation";

        Field<UserMutation>()
            .Name("user")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });

        Field<AccountPasswordMutation>()
            .Name("accountPassword")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });
        Field<SharedPasswordMutations>()
            .Name("sharedPassword")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });

        Field<AccountTagMutation>()
            .Name("accountTag")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });
        Field<SharedTagMutation>()
            .Name("sharedTag")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });

        Field<GroupMutations>()
            .Name("group")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });

        Field<MandatorMutations>()
            .Name("mandator")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });
        
        Field<DeviceInformationMutation>()
            .Name("device")
            .Resolve()
            .WithScope()
            .Resolve(_ => new { });
    }
}