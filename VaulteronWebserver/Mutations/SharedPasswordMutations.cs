﻿using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.Types;
using VaulteronDatabase.Models;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.InputTypes;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.InputTypes.MutationInputTypes;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Models.EditModels;
using VaulteronUtilities.Models.MutationModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;
using VaulteronWebserver.Queries;

namespace VaulteronWebserver.Mutations;

public sealed class SharedPasswordMutations : ObjectGraphType
{
    public SharedPasswordMutations(ISharedPasswordResolver resolver)
    {
        Field<SharedPasswordType>()
            .Name(MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddSharedPasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.AddSharedPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddSharedPasswordModel>("password")
                          )
            );

        Field<SharedPasswordType>()
            .Name(MasterDataQueryNames.EDIT)
            .Argument<NonNullGraphType<EditSharedPasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.EditSharedPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<EditSharedPasswordModel>("password")
                          )
            );

        Field<SharedPasswordType>()
            .Name(MasterDataQueryNames.DELETE)
            .Argument<NonNullGraphType<GuidGraphType>>("id")
            .ResolveAsync(async context => await resolver.DeleteSharedPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<Guid>("id")
                          )
            );

        Field<SharedPasswordType>()
            .Name("bulkSyncEncryptedPassword")
            .DeprecationReason(
                "Encryptions should be provided within every mutation changing public-keys, password-text or access to passwords. "
                + "Can still be used if ihe migration did not work, or if something else breaks (just in case).")
            .Description("For SharedPassword AND CompanyPassword. " + "Adds or edits multiple encrypted password entities for this password that was encrypted for this user's publicKey")
            .Argument<NonNullGraphType<BulkSyncEncryptedSharedPasswordInputType>>("encryptedPasswords")
            .ResolveAsync(async context => await resolver.BulkSyncEncryptedPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<BulkSyncEncryptedSharedPasswordModel>("encryptedPasswords")
                          )
            );

        Field<ListGraphType<SharedPasswordType>>()
            .Name("moveToGroup")
            .Description("Moves multiple SharedPasswords to another Group")
            .Argument<NonNullGraphType<MoveSharedPasswordToGroupInputType>>("input")
            .ResolveAsync(async context => await resolver.BulkMoveSharedPasswordsBetweenGroupsAsync(
                              context.GetUserGuid(),
                              context.GetArgument<BulkMoveSharedPasswordModel>("input")
                          )
            );

        Field<ListGraphType<SharedPasswordType>>()
            .Name("companyMoveToShared")
            .Description("Moves multiple Company-Passwords to a Group making them SharedPasswords")
            .Argument<NonNullGraphType<ListGraphType<GuidGraphType>>>("passwordsToMoveIds")
            .Argument<NonNullGraphType<GuidGraphType>>("destinationGroupId")
            .Argument<ListGraphType<EncryptedSharedPasswordInputType>>("encryptedPasswords",
                                                                       "Are to be be provided for all users (including admins) with access to destinationGroup.")
            .ResolveAsync(async context => await resolver.BulkMoveCompanyToGroupPasswordsAsync(
                              context.GetUserGuid(),
                              context.GetArgument<List<Guid>>("passwordsToMoveIds"),
                              context.GetArgument<Guid>("destinationGroupId"),
                              context.GetArgument<List<EncryptedSharedPassword>>("encryptedPasswords")
                          )
            );

        Field<ListGraphType<SharedPasswordType>>()
            .Name("sharedMoveToCompany")
            .Description("Moves multiple SharedPasswords from a Group to make them Company-Passwords")
            .Argument<NonNullGraphType<ListGraphType<GuidGraphType>>>("passwordsToMoveIds")
            .Argument<NonNullGraphType<GuidGraphType>>("destinationUserId")
            .Argument<ListGraphType<EncryptedSharedPasswordInputType>>("encryptedPasswords",
                                                                       "Are to be be provided for destinationUser and admins.")
            .ResolveAsync(async context => await resolver.BulkMoveGroupToCompanyPasswordsAsync(
                              context.GetUserGuid(),
                              context.GetArgument<List<Guid>>("passwordsToMoveIds"),
                              context.GetArgument<Guid>("destinationUserId"),
                              context.GetArgument<List<EncryptedSharedPassword>>("encryptedPasswords")
                          )
            );

        #region Company-PW specific

        Field<SharedPasswordType>()
            .Name("company" + MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddCompanyPasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.AddCompanyPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddSharedPasswordModel>("password")
                          )
            );

        Field<SharedPasswordType>()
            .Name("company" + MasterDataQueryNames.ADD + "ForUser")
            .Argument<NonNullGraphType<AddCompanyPasswordInputType>>("password")
            .Argument<GuidGraphType>("forUserId")
            .ResolveAsync(async context => await resolver.AddCompanyPasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddSharedPasswordModel>("password"),
                              context.GetArgument<Guid>("forUserId")
                          )
            );

        #endregion

        #region Offline-PW specific

        Field<SharedPasswordType>()
            .Name("offline" + MasterDataQueryNames.ADD)
            .Argument<NonNullGraphType<AddOfflinePasswordInputType>>("password")
            .ResolveAsync(async context => await resolver.AddOfflinePasswordAsync(
                              context.GetUserGuid(),
                              context.GetArgument<AddSharedPasswordModel>("password")
                          )
            );

        #endregion
    }
}