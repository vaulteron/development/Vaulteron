﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Fido2NetLib.Objects;
using GraphQL;
using GraphQL.Caching;
using GraphQL.DataLoader;
using GraphQL.MicrosoftDI;
using GraphQL.Server;
using GraphQL.Server.Transports.AspNetCore;
using GraphQL.Server.Ui.Playground;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VaulteronDatabase;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronDatabase.SeedData;
using VaulteronUtilities;
using VaulteronUtilities.Helper;
using VaulteronUtilities.InputTypes;
using VaulteronUtilities.InputTypes.AddInputTypes;
using VaulteronUtilities.InputTypes.EditInputType;
using VaulteronUtilities.InputTypes.MutationInputTypes;
using VaulteronUtilities.Repositories;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services;
using VaulteronUtilities.Services.BusinessLogic;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronUtilities.Services.Helper;
using VaulteronUtilities.Services.Helper.Interfaces;
using VaulteronUtilities.Services.Interfaces;
using VaulteronUtilities.Types;
using VaulteronUtilities.Types.MutationResults;
using VaulteronUtilities.Types.Stripe;
using Stripe;
using VaulteronWebserver.Middleware;
using VaulteronWebserver.Mutations;
using VaulteronWebserver.Schema;
using VaulteronWebserver.ViewModels;
using ServiceLifetime = GraphQL.DI.ServiceLifetime;

namespace VaulteronWebserver;

public class Startup
{
    private readonly IConfiguration configuration;
    private readonly bool isDevelopmentEnvironment;
    private const string CorsPolicyName = "vaulteronPolicyName";

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        this.configuration = configuration;
        var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", false, true)
            .AddEnvironmentVariables();

        isDevelopmentEnvironment = env.IsDevelopment();
        if (env.IsDevelopment()) builder.AddUserSecrets<Startup>();

        Configuration = builder.Build();
    }

    private IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        // CORS (before calling services.AddMvc)
        if (isDevelopmentEnvironment)
        {
            services.AddCors(o =>
                o.AddPolicy(CorsPolicyName, builder => builder
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins(Configuration["DomainUrl"])
                    .AllowCredentials()
                ));
        }
        else
        {
            services.AddCors(o =>
                o.AddPolicy(CorsPolicyName, builder => builder
                    .AllowAnyHeader()
                    .WithMethods("GET", "POST", "OPTIONS")
                    .WithOrigins("https://app.vaulteron.com", Configuration["DomainUrl"])
                    .AllowCredentials()
                ));
        }

        services.AddControllers()
            .AddJsonOptions(opts => { opts.JsonSerializerOptions.Converters.Add(new InputsJsonConverter()); });

        #region GraphQL

        // GraphQL types
        services.AddScoped<MandatorType>();
        services.AddScoped<SubscriptionStatusType>();
        services.AddScoped<GroupType>();
        services.AddScoped<UserType>();
        services.AddScoped<OwnUserType>();
        services.AddScoped<AccountPasswordType>();
        services.AddScoped<SharedPasswordType>();
        services.AddScoped<PasswordType>();
        services.AddScoped<SexType>();
        services.AddScoped<TwoFATypesType>();
        services.AddScoped<SecurityRatingType>();
        services.AddScoped<UserGroupType>();
        services.AddScoped<AccountTagType>();
        services.AddScoped<SharedTagType>();
        services.AddScoped<GroupRoleType>();
        services.AddScoped<PublicKeyType>();
        services.AddScoped<KeyPairType>();
        services.AddScoped<KeyPairChangeLogType>();
        services.AddScoped<AccountPasswordAccessLogType>();
        services.AddScoped<SharedPasswordAccessLogType>();
        services.AddScoped<ChangeLogType>();
        services.AddScoped<ChangeLogValueType>();
        services.AddScoped<PasswordChangeLogType>();
        services.AddScoped<StripeCustomerType>();
        services.AddScoped<AddSharedPasswordInputType>();
        services.AddScoped<AddAccountPasswordInputType>();
        services.AddScoped<AddCompanyPasswordInputType>();
        services.AddScoped<AddOfflinePasswordInputType>();
        services.AddScoped<EditAccountPasswordInputType>();
        services.AddScoped<EditSharedPasswordInputType>();
        services.AddScoped<AddAccountTagInputType>();
        services.AddScoped<EditAccountTagInputType>();
        services.AddScoped<AddSharedTagInputType>();
        services.AddScoped<EditSharedTagInputType>();
        services.AddScoped<BulkSyncEncryptedSharedPasswordInputType>();
        services.AddScoped<EncryptedSharedPasswordInputType>();
        services.AddScoped<EncryptedAccountPasswordInputType>();
        services.AddScoped<EditEncryptedAccountPasswordInputType>();
        services.AddScoped<AddEncryptedAccountPasswordInputType>();
        services.AddScoped<MoveSharedPasswordToGroupInputType>();
        services.AddScoped<AddEncryptedSharedPasswordWithoutPublicKeyInputType>();
        services.AddScoped<AddEncryptedAccountPasswordWithoutPublicKeyInputType>();
        services.AddScoped<AddUserInputType>();
        services.AddScoped<EditUserInputType>();
        services.AddScoped<GroupInputType>();
        services.AddScoped<EditGroupInputType>();
        services.AddScoped<AddUserGroupType>();
        services.AddScoped<EditUserGroupInputType>();
        services.AddScoped<TwoFactorMutation>();
        services.AddScoped<TotpAuthenticatorKeyType>();
        services.AddScoped<WebauthnChallengeType>();
        services.AddScoped<EnumerationGraphType<TwoFATypes>>();
        services.AddScoped<KindOfPasswordType>();
        services.AddScoped<EnumerationGraphType<PublicKeyCredentialType>>();
        services.AddScoped<DeviceInformationType>();
        services.AddScoped<SendScamMailType>();
        services.AddScoped<WebauthnKeyType>();

        // GraphQL resolver
        services.AddScoped<IMandatorResolver, MandatorResolver>();
        services.AddScoped<IGroupResolver, GroupResolver>();
        services.AddScoped<IUserResolver, UserResolver>();
        services.AddScoped<IAuthenticationResolver, AuthenticationResolver>();
        services.AddScoped<IAccountPasswordResolver, AccountPasswordResolver>();
        services.AddScoped<ISharedPasswordResolver, SharedPasswordResolver>();
        services.AddScoped<IAllPasswordResolver, AllPasswordResolver>();
        services.AddScoped<IAccountTagResolver, AccountTagResolver>();
        services.AddScoped<ISharedTagResolver, SharedTagResolver>();
        services.AddScoped<IUserGroupResolver, UserGroupResolver>();
        services.AddScoped<IChangeLogResolver, ChangeLogResolver>();
        services.AddScoped<IDeviceInformationResolver, DeviceInformationResolver>();
        services.AddScoped<IPwnedResolver, PwnedResolver>();

        // Add GraphQL
        services.AddGraphQL(builder => builder
            .AddSystemTextJson()
            .AddUserContextBuilder(httpContext => new GraphQLUserContext(httpContext.User))
            .AddHttpMiddleware<VaulteronSchema>()
            .AddSchema<VaulteronSchema>(ServiceLifetime.Scoped)
            .AddGraphTypes(typeof(VaulteronSchema).Assembly)
            .AddDataLoader()
            .AddMemoryCache()
            .AddErrorInfoProvider(options => options.ExposeExceptionStackTrace = isDevelopmentEnvironment)
        );

        #endregion

        #region Additional Services for DI

        services.AddScoped<IEmailSenderService, EMailSenderService>();
        services.AddScoped<ICurrentUserClaimsService, CurrentUserClaimsService>();
        services.AddScoped<ICurrentUserInfoService, CurrentUserInfoService>();

        // Helper Services
        services.AddScoped<ICryptoHelper, CryptoHelper>();

        // Configure DI for application services
        services.AddScoped<IUserHelperService, UserHelperService>();

        #endregion

        #region business-logic (services, repositories)

        // BL-Services
        services.AddScoped<IMandatorService, MandatorService>();
        services.AddScoped<IGroupService, GroupService>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<ISharedPasswordService, SharedPasswordService>();
        services.AddScoped<IPasswordsEncryptionsCheckService, PasswordsEncryptionsCheckService>();
        services.AddScoped<IAllPasswordService, AllPasswordService>();

        // Repositories
        services.AddScoped<IMandatorRepository, MandatorRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IKeyPairRepository, KeyPairRepository>();
        services.AddScoped<IGroupRepository, GroupRepository>();
        services.AddScoped<IAccountEncryptionsRepository, AccountEncryptionsRepository>();
        services.AddScoped<ISharedEncryptionsRepository, SharedEncryptionsRepository>();
        services.AddScoped<IAccountPasswordRepository, AccountPasswordRepository>();
        services.AddScoped<ISharedPasswordRepository, SharedPasswordRepository>();
        services.AddScoped<IAccountPasswordRepository, AccountPasswordRepository>();

        #endregion

        #region Database

        services.AddDbContext<VaulteronContext>(options => options
            .UseMySql(configuration.GetConnectionString("VaulteronDB"),
                ServerVersion.AutoDetect(configuration.GetConnectionString("VaulteronDB")),
                optionsBuilder => { optionsBuilder.EnableRetryOnFailure(); })
            .EnableSensitiveDataLogging(isDevelopmentEnvironment)
        );
        services.AddScoped<VaulteronSeedData>();

        #endregion

        #region Logging

        services.AddLogging(builder =>
        {
            builder.ClearProviders();
            builder.AddSimpleConsole(options => options.TimestampFormat = "[dd.MM.yy HH:mm] ");
        });

        #endregion

        #region Auhtorization and Authentication

        services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = false)
            .AddSignInManager<CustomSignInManager<User>>()
            .AddEntityFrameworkStores<VaulteronContext>()
            .AddDefaultTokenProviders();

        services.Configure<IdentityOptions>(options =>
        {
            // Password settings.
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
            options.Password.RequiredLength = 5;
            options.Password.RequiredUniqueChars = 1;

            // Lockout settings.
            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
            options.Lockout.MaxFailedAccessAttempts = 5;
            options.Lockout.AllowedForNewUsers = true;

            // User settings.
            options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
            options.User.RequireUniqueEmail = true;
        });

        // Authentication
        services
            .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.LoginPath = "/api/login";
                options.LogoutPath = "/api/logout";
                options.AccessDeniedPath = "/api/login";

                options.ExpireTimeSpan = TimeSpan.FromDays(7);
                options.Cookie.MaxAge = options.ExpireTimeSpan;
                options.SlidingExpiration = true; // will be extended indefinitely (see https://brokul.dev/authentication-cookie-lifetime-and-sliding-expiration)

                options.Cookie.SameSite = isDevelopmentEnvironment ? SameSiteMode.None : SameSiteMode.Strict; // Cookie sameSite flag
                if (!isDevelopmentEnvironment) options.Cookie.SecurePolicy = CookieSecurePolicy.Always; // Cookie secure flag
                options.Cookie.HttpOnly = true; // HttpOnly flag

                options.Events.OnRedirectToLogin = context =>
                {
                    if (context.Request.Path.StartsWithSegments("/api") && context.Response.StatusCode == StatusCodes.Status200OK)
                    {
                        context.Response.Clear();
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        return Task.CompletedTask;
                    }

                    context.Response.Redirect(context.RedirectUri);
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return Task.CompletedTask;
                };
            });

        services.ConfigureApplicationCookie(options =>
        {
            options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            if (isDevelopmentEnvironment) options.Cookie.SameSite = SameSiteMode.None;
        });

        // Services Required for Fido2 / Webauthn
        services.AddSession(options =>
        {
            options.Cookie.IsEssential = true;
            options.IdleTimeout = TimeSpan.FromMinutes(10);
        });
        services.AddDistributedMemoryCache(); // Required for session management
        services
            .AddFido2(options =>
            {
                options.ServerName = "Vaulteron";
                options.ServerDomain = Configuration["Fido2:ServerDomain"];
                options.Origin = Configuration["Fido2:Origin"];
                options.TimestampDriftTolerance = Configuration.GetValue<int>("Fido2:TimestampDriftTolerance");
                options.MDSAccessKey = Configuration["Fido2:MDSAccessKey"];
                options.MDSCacheDirPath = Configuration["Fido2:MDSCacheDirPath"];
            })
            .AddCachedMetadataService(config =>
            {
                if (!string.IsNullOrWhiteSpace(Configuration["Fido2:MDSAccessKey"]))
                {
                    config.AddFidoMetadataRepository(Configuration["Fido2:MDSAccessKey"]);
                }

                config.AddStaticMetadataRepository();
            });

        #endregion

        #region ApiController and REST stuff

        services.AddHttpContextAccessor();

        services.Configure<ApiBehaviorOptions>(options =>
        {
            options.InvalidModelStateResponseFactory = context =>
            {
                // Extracts all fields and gives information as well as error messages for each one
                var reasons = context.ModelState
                    .Select(pair => $"{{param: {pair.Key}, "
                                    + $"status: {pair.Value?.ValidationState.ToString()}, "
                                    + $"errors: [{string.Join(",", pair.Value?.Errors.Select(e => e.ErrorMessage) ?? Array.Empty<string>())}]}}");
                var apiException = new ApiException(ErrorCode.GraphQlError, "Data sent to API is invalid", reasons);
                return new BadRequestObjectResult(new ApiExceptionResponseViewModel(apiException));
            };
        });

        #endregion

        #region include react

        // In production, the React files will be served from this directory
        services.AddSpaStaticFiles(config => { config.RootPath = "ClientApp/build"; });

        #endregion

        #region Stripe

        services.AddScoped<IInvoiceService, StripeInvoiceService>();

        #endregion

        services.AddScoped<IPwnedService, PwnedService>();
    }

    public void Configure(IApplicationBuilder app, VaulteronSeedData seedData, VaulteronContext dbContext, IHostApplicationLifetime hostApplicationLifetime)
    {
        // Always make sure the DB is created and all migrations are applied
        seedData.EnsureCreatedAndMigrated();

        // Order see: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-6.0#middleware-order

        if (isDevelopmentEnvironment)
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseSpaStaticFiles();

        app.UseRouting();

        app.UseCors(CorsPolicyName);

        app.UseAuthentication();
        app.UseAuthorization();

        #region custom middleware

        app.UseGraphQLPlayground(new PlaygroundOptions { GraphQLEndPoint = "/api/graph" }, "/api/ui");

        app.UseMiddleware<HttpRequestLoggingMiddleware>();

        app.Use(async (context, next) =>
        {
            var cspHeader = $"default-src https: {(isDevelopmentEnvironment ? "http:" : "")}; "
                            + "script-src 'self' 'unsafe-inline' https: https://*.stripe.com/; "
                            + $"connect-src 'self' {(isDevelopmentEnvironment ? "wss:" : "")} https: https://*.vaulteron.com/; "
                            + "img-src 'self' data:; "
                            + "style-src 'self' 'unsafe-inline'; "
                            + "font-src 'self' data:; "
                            + "frame-ancestors 'none'";
            SetHttpHeader(context, "Content-Security-Policy", cspHeader);
            SetHttpHeader(context, "Cache-Control", "no-store");
            SetHttpHeader(context, "X-Content-Type-Options", "nosniff");
            SetHttpHeader(context, "X-Frame-Options", "DENY");
            SetHttpHeader(context, "Referrer-Policy", "strict-origin-when-cross-origin");
            SetHttpHeader(context, "Cross-Origin-Opener-Policy", "same-origin");
            SetHttpHeader(context, "Permissions-Policy",
                "accelerometer=(), camera=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()");

            await next();
        });

        #endregion

        app.UseSession(); // Required by Fido2

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapDefaultControllerRoute();
            endpoints.MapGraphQL<VaulteronSchema, GraphQLHttpMiddleware<VaulteronSchema>>();
            endpoints.MapGraphQLPlayground();
        });

        #region React

        app.UseSpa(spa =>
        {
            spa.Options.SourcePath = "ClientApp";
            spa.Options.PackageManagerCommand = "yarn";
            if (isDevelopmentEnvironment) spa.UseReactDevelopmentServer("start");
        });

        #endregion

        #region Stripe

        StripeConfiguration.ApiKey = Configuration.GetValue<string>("Stripe:ApiKey");

        #endregion
    }

    private static void SetHttpHeader(HttpContext context, string headerName, string value)
    {
        if (context.Response.Headers.ContainsKey(headerName))
            context.Response.Headers.Remove(headerName);

        context.Response.Headers.Add(headerName, value);
    }
}