﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VaulteronDatabase;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Services.Interfaces;
using Stripe;
using VaulteronUtilities.Repositories.Interfaces;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Services.BusinessLogic.Interfaces;
using VaulteronWebserver.ViewModels;

namespace VaulteronWebserver.Controllers;

[Route("/stripe")]
public class StripeController : Controller
{
    private readonly VaulteronContext db;
    private readonly string endpointSecret;
    private readonly ILogger<StripeController> logger;
    private readonly bool isDevelopmentEnvironment;
    private readonly IInvoiceService invoiceService;
    private readonly IMandatorResolver mandatorResolver;
    public StripeController(IConfiguration config, ILogger<StripeController> logger, VaulteronContext dbContext, IHostEnvironment env, IInvoiceService invoiceService, IMandatorResolver mandatorResolver)
    {
        this.logger = logger;
        db = dbContext;
        endpointSecret = config.GetValue<string>("Stripe:WebhookSecret");
        this.invoiceService = invoiceService;
        this.mandatorResolver = mandatorResolver;
        isDevelopmentEnvironment = env.IsDevelopment();
    }

    [HttpPost("webhook")]
    public async Task<IActionResult> Index()
    {
        var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();

        try
        {
            var signatureHeader = Request.Headers["Stripe-Signature"];
            var stripeEvent = EventUtility.ConstructEvent(json, signatureHeader, endpointSecret);

            switch (stripeEvent.Type)
            {
                case Events.SetupIntentSucceeded:
                    var setupIntent = stripeEvent.Data.Object as  SetupIntent;
                    await mandatorResolver.ChangePlan(setupIntent);
                    break;
                case Events.CheckoutSessionCompleted:
                     var checkoutSession = stripeEvent.Data.Object as Stripe.Checkout.Session;
                     invoiceService.UpdateDefaultPayment(checkoutSession.CustomerId);
                    break;
                case Events.CustomerSubscriptionDeleted:
                case Events.CustomerSubscriptionCreated:
                case Events.CustomerSubscriptionUpdated:
                {
                    var subscription = stripeEvent.Data.Object as Subscription;
                    // Extract Mandator
                    var mandator = await db.Mandators.SingleOrDefaultAsync(m => subscription != null && m.PaymentProviderId == subscription.CustomerId);

                    if (mandator == null)
                    {
                        throw new ApiException(ErrorCode.SubscriptionNotFound, "Mandator with this subscription wasn't found");
                    }

                    // Subscription status
                    switch (subscription?.Status)
                    {
                        case "trialing":
                            mandator.SubscriptionStatus = LicenceStatus.Trial;
                            break;
                        case "active":
                            mandator.SubscriptionStatus = LicenceStatus.Standard;
                            break;
                        case "incomplete_expired":
                        case "past_due":
                        case "unpaid":
                        case "canceled":
                            mandator.SubscriptionStatus = LicenceStatus.None;
                            break;
                    }

                    db.Update(mandator);
                    await db.SaveChangesAsync();
                    break;
                }
                case Events.CustomerUpdated:
                    break;
                case Events.CustomerTaxIdUpdated:
                    break;
                case Events.InvoicePaymentFailed:
                    break;
                case Events.CustomerSubscriptionTrialWillEnd:
                    // TODO? Remind Customer to set payment method or cancel
                    break;
                default:
                    logger.LogError("Unhandled event type: {Type}", stripeEvent.Type);
                    break;
            }

            return Ok();
        }
        catch (StripeException e)
        {
            logger.LogError(e, "Stripe error: {Message}", e.Message);
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
        catch (Exception e)
        {
            logger.LogError(e, "Unknown Stripe error: {Message}", e.Message);
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }
}