﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Models.AddModels;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronWebserver.ControllerHelper;
using VaulteronWebserver.ViewModels;
using VaulteronWebserver.ViewModels.Users;

namespace VaulteronWebserver.Controllers;

[Route("")]
public class IndexController : Controller
{
    private readonly ILogger<IndexController> logger;
    private readonly IAuthenticationResolver authResolver;
    private readonly IDeviceInformationResolver deviceInformationResolver;
    private readonly bool isDevelopmentEnvironment;

    public IndexController(ILogger<IndexController> logger, IAuthenticationResolver authResolver, IDeviceInformationResolver deviceInformationResolver, IHostEnvironment env)
    {
        this.logger = logger;
        this.authResolver = authResolver;
        this.deviceInformationResolver = deviceInformationResolver;
        
        isDevelopmentEnvironment = env.IsDevelopment();
    }

    [AllowAnonymous]
    [HttpGet("/emailConfirmation")]
    [Produces("application/json")]
    public async Task<IActionResult> EmailConfirmation([FromQuery] EmailConfirmationModel emailConfirmationModel)
    {
        try
        {
            var decodedUserId = ConversionHelper.FromUrlEncoded(emailConfirmationModel.UserId);
            var decodedToken = ConversionHelper.FromUrlEncoded(emailConfirmationModel.Token);
            await authResolver.ValidateEmailConfirmationTokenAsync(decodedUserId, decodedToken);

            return Redirect("/");
        }
        catch (ApiException apiException)
        {
            if (apiException.ErrorCode != ErrorCode.EmailTokenInvalid)
                return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(apiException));

            var modifiedApiException = new ApiException(ErrorCode.EmailTokenInvalid,
                                                        "This email token is invalid. A new email has been sent to the user's email address.",
                                                        apiException.Reasons);
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(modifiedApiException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(e.Message));
        }
    }

    [AllowAnonymous]
    [HttpGet("/emailChangedVerification")]
    [Produces("application/json")]
    public async Task<IActionResult> EmailChangedVerification([FromQuery] EmailChangedModel emailChangedModel)
    {
        try
        {
            var decodedUserId = ConversionHelper.FromUrlEncoded(emailChangedModel.UserId);
            var decodedToken = ConversionHelper.FromUrlEncoded(emailChangedModel.Token);
            var decodedNewEmail = ConversionHelper.FromUrlEncoded(emailChangedModel.NewEmail);

            await authResolver.ChangeEmailAndUsernameViaTokenAsync(decodedUserId, decodedNewEmail, decodedToken);

            return Redirect("/");
        }
        catch (ApiException apiException)
        {
            if (apiException.ErrorCode != ErrorCode.EmailTokenInvalid)
                return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(apiException));

            var modifiedApiException = new ApiException(ErrorCode.EmailTokenInvalid,
                                                        "This email token is invalid. Please ask an administrator to resend this email.",
                                                        apiException.Reasons);
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(modifiedApiException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(e.Message));
        }
    }

    [AllowAnonymous]
    [HttpPost("/addDeviceInformation")]
    [Produces("application/json")]
    public async Task<IActionResult> AddDeviceInformation([FromBody] AddDeviceInformationModel deviceInformationModel)
    {
        try
        {
            await deviceInformationResolver.AddDeviceInformationAsync(deviceInformationModel);

            return Ok();
        }
        catch (ApiException e)
        {
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }
}