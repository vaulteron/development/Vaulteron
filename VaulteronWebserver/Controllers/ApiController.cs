﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Execution;
using GraphQL.Transport;
using GraphQL.Types;
using GraphQL.Validation.Complexity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Helper;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronWebserver.ControllerHelper;
using VaulteronWebserver.ViewModels;
using VaulteronWebserver.ViewModels.Users;

namespace VaulteronWebserver.Controllers;

[Microsoft.AspNetCore.Authorization.Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme)]
[ApiController]
[Route("api")]
public class ApiController : Controller
{
    private readonly IDocumentExecuter documentExecutor;
    private readonly ISchema schema;
    private readonly ILogger<ApiController> logger;

    private readonly IAuthenticationResolver authResolver;
    private readonly bool isDevelopmentEnvironment;

    public ApiController(ISchema schema, IDocumentExecuter documentExecutor, ILogger<ApiController> logger, IAuthenticationResolver authResolver, IHostEnvironment env)
    {
        this.schema = schema;
        this.documentExecutor = documentExecutor;
        this.logger = logger;
        this.authResolver = authResolver;

        isDevelopmentEnvironment = env.IsDevelopment();
    }

    [HttpPost("graph")]
    public async Task<IActionResult> GraphQLPost([FromBody] GraphQLRequest request)
    {
        try
        {
            if (User.Identity?.Name == null)
                throw new ApiException(ErrorCode.UserIsNotAuthorized_NotLoggedIn, "You must be logged in");

            if (request == null) throw new ArgumentNullException(nameof(request));

            var executionOptions = new ExecutionOptions
            {
                Schema = schema,
                Query = request.Query,
                Variables = request.Variables,
                OperationName = request.OperationName,
                RequestServices = HttpContext.RequestServices,
                ComplexityConfiguration = new ComplexityConfiguration { MaxDepth = 50 },
                CancellationToken = HttpContext.RequestAborted,
            };
            var dataLoaderDocumentListener = HttpContext.RequestServices.GetRequiredService<DataLoaderDocumentListener>();
            executionOptions.Listeners.Add(dataLoaderDocumentListener);

            var result = await documentExecutor.ExecuteAsync(executionOptions);

            if (result.Errors == null || result.Errors.Count == 0)
            {
                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int) HttpStatusCode.OK;
                var writer = new GraphQL.SystemTextJson.GraphQLSerializer(options => { options.PropertyNamingPolicy = null; });
                await writer.WriteAsync(HttpContext.Response.Body, result);
                return new EmptyResult();
            }

            if (result.Errors.Count > 1)
                throw new ApiException(ErrorCode.UnknownError,
                                       "Multiple internal errors occured. Please contact support.",
                                       result.Errors!.Select(e => e.Message));

            var actualThrownException = result.Errors!.First().InnerException ?? result.Errors!.First();
            throw actualThrownException switch
            {
                null => new ApiException(ErrorCode.GraphQlError, "Unknown graphQL error"),
                ApiException => actualThrownException,
                DocumentError ive => new ApiException(ErrorCode.InputInvalidAccordingToGraphQL, ive.Message),
                _ => new ApiException(ErrorCode.GraphQlError, actualThrownException.Message)
            };
        }
        catch (ApiException authException)
        {
            logger.LogError(authException, "An unknown error occured while processing the request");
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("login")]
    [Produces("application/json")]
    public async Task<IActionResult> Login([FromBody] LoginForm loginForm)
    {
        try
        {
            var user = await authResolver.LoginAsync(loginForm.Email, loginForm.Password, loginForm.IsPersistent);

            if (user == null) throw new ApiException(ErrorCode.UnableToLogin, "Username or password is incorrect");

            await ResponseErrorHelper.CreateCookieAndSignInAsync(user, HttpContext);

            return Ok(new UserView(user));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("ldaplogin")]
    [Produces("application/json")]
    public async Task<IActionResult> LDAPLogin([FromBody] LoginForm loginForm)
    {
        try
        {
            var user = await authResolver.LdapLoginAsync(loginForm.Email, loginForm.Password,
                                                         loginForm.IsPersistent);

            if (user == null) throw new ApiException(ErrorCode.UnableToLogin, "Username or password is incorrect");

            await ResponseErrorHelper.CreateCookieAndSignInAsync(user, HttpContext);

            return Ok(new UserView(user));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    #region TwoFactor

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("twofactortotplogin")]
    [Produces("application/json")]
    public async Task<IActionResult> TwoFactorTotpLogin([FromBody] TwoFactorLoginFrom twofactorForm)
    {
        try
        {
            var user = await authResolver.TwoFATOTPLoginAsync(
                twofactorForm.Code,
                twofactorForm.IsPersistent,
                twofactorForm.RememberClient);

            await ResponseErrorHelper.CreateCookieAndSignInAsync(user, HttpContext);

            return Ok(new UserView(user));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpGet("twofactorwebauthnchallenge")]
    [Produces("application/json")]
    public async Task<IActionResult> TwoFactorWebAuthnChallenge()
    {
        try
        {
            var options = await authResolver.AssertionOptionsAsync();

            // Fido2 Lib is build to work with Newtonsoft.Json, instead of Text.Json 
            return new ContentResult { Content = JsonConvert.SerializeObject(options) };
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("twofactorwebauthnlogin")]
    [Produces("application/json")]
    public async Task<IActionResult> TwoFactorWebAuthnLogin()
    {
        try
        {
            // Reading body. Text.Json can not parse this one
            using StreamReader reader = new(Request.Body, Encoding.UTF8);
            var str = await reader.ReadToEndAsync();

            // Parsing Body with Newtonsoft.Json
            var form = JsonConvert.DeserializeObject<WebAuthnLoginForm>(str);
            if (form is null) throw new ApiException(ErrorCode.UnknownError, "Unable to deserialize http body data into WebAuthnLoginForm model");

            var user = await authResolver.LoginUsingWebauthnAssertion(form.AssertionResponse, form.IsPersistent);

            await ResponseErrorHelper.CreateCookieAndSignInAsync(user, HttpContext);

            return Ok(new UserView(user));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    #endregion

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("register")]
    [Produces("application/json")]
    public async Task<IActionResult> Register([FromBody] RegisterForm registerForm)
    {
        try
        {
            var user = new User
            {
                Admin = true,
                Email = registerForm.Email,
                UserName = registerForm.Email,
                Firstname = registerForm.Firstname,
                Lastname = registerForm.Lastname,
                Sex = Sex.Unset,
                PasswordChangedAt = DateTime.UtcNow,
                Mandator = new Mandator
                {
                    Name = registerForm.CompanyName,
                    SubscriptionStatus = LicenceStatus.None,
                    SubscriptionUserLimit = 1,
                    IsBusinessCustomer = false,
                },
                KeyPairs = new List<KeyPair>
                {
                    new()
                    {
                        EncryptedPrivateKey = registerForm.EncryptedPrivateKey,
                        PublicKeyString = registerForm.PublicKey
                    }
                }
            };

            var registeredUser = await authResolver.RegisterAsync(user, registerForm.Password);

            await ResponseErrorHelper.CreateCookieAndSignInAsync(registeredUser, HttpContext);

            return Ok(new UserView(registeredUser));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [HttpPost("logout")]
    [Produces("application/json")]
    public async Task<IActionResult> Logout()
    {
        await authResolver.LogoutAsync();
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return SignOut(IdentityConstants.ApplicationScheme);
    }

    #region email routes

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("getPublicKeyAndSharedPasswordsFromEmailToken")]
    [Produces("application/json")]
    public async Task<IActionResult> EmailGetPublicKeyAndSharedPasswords([FromBody] EmailGetOldEncryptionsForm model)
    {
        try
        {
            var (publicKey, encryptedShared) = await authResolver.GetKeyPairAndSharedPasswordsViaEmailTokenAsync(model.UserId, model.Token, model.OldPasswordHashed);

            return Ok(new EncryptedPrivateKeyAndSharedPasswordsView(publicKey, encryptedShared));
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("setPasswordFromEmailToken")]
    [Produces("application/json")]
    public async Task<IActionResult> EmailSetPassword([FromBody] EmailSetPasswordModel model)
    {
        try
        {
            var user = await authResolver.AddUserViaEmailTokenAsync(model.UserId,
                                                                    model.Token,
                                                                    model.OldPassword,
                                                                    model.NewPassword,
                                                                    model.PublicKey,
                                                                    model.EncryptedPrivateKey,
                                                                    model.PreviousPasswordHashEncryptedWithPublicKey,
                                                                    model.EncryptedSharedPasswords);

            await ResponseErrorHelper.CreateCookieAndSignInAsync(user, HttpContext);

            return Ok();
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [HttpPost("requestEmailConfirmation")]
    [Produces("application/json")]
    public async Task<IActionResult> RequestAnotherEmailConfirmation()
    {
        try
        {
            if (User.Identity?.Name == null)
                throw new ApiException(ErrorCode.UserIsNotAuthorized_NotLoggedIn,
                                       "You must be logged in to request an confirmation email");

            await authResolver.RequestEmailConfirmationAsync(Guid.Parse(User.Identity.Name));

            return Ok();
        }
        catch (ApiException authException)
        {
            return BadRequest(new ApiExceptionResponseViewModel(authException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while sending another confirmation email");
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("resetPassword")]
    [Produces("application/json")]
    public async Task<IActionResult> RequestResetPassword([FromBody] EmailPasswordReset model)
    {
        try
        {
            await authResolver.ResetPassword(model.Email);

            return Ok();
        }
        catch (Exception e)
        {
            return BadRequest(new GenericExceptionViewModel(e, isDevelopmentEnvironment));
        }
    }

    [Microsoft.AspNetCore.Authorization.AllowAnonymous]
    [HttpPost("encryptedResetPasswordByToken")]
    [Produces("application/json")]
    public async Task<IActionResult> EncryptedResetPasswordByToken([FromBody] PasswordResetByTokenModel emailChangedModel)
    {
        try
        {
            var encryptedResetPassword = await authResolver.GetEncryptedResetPasswordByToken(emailChangedModel.UserId, emailChangedModel.Token);

            return Ok(new EncryptedResetPasswordView { EncryptedResetPassword = encryptedResetPassword });
        }
        catch (ApiException apiException)
        {
            if (apiException.ErrorCode != ErrorCode.EmailTokenInvalid)
                return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(apiException));

            var modifiedApiException = new ApiException(ErrorCode.EmailTokenInvalid,
                                                        "This email token is invalid. Please request another password reset on the login page.",
                                                        apiException.Reasons);
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(modifiedApiException));
        }
        catch (Exception e)
        {
            logger.LogError(e, "An unknown error occured while processing the login request");
            return Redirect(ResponseErrorHelper.PrepareRedirectToErrorPageUri(e.Message));
        }
    }

    #endregion
}