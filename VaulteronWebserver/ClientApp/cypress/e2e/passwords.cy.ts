Cypress.Commands.add("login", (username, password) => {
	cy.visit("/login");
	cy.get("input[name=email]").type(username);
	cy.get("input[name=password]").type(`${password}{enter}`);
	cy.getCookie(".AspNetCore.Cookies").should("exist");
	cy.getCookie(".AspNetCore.Identity.Application").should("exist");
});

Cypress.Commands.add("gotoAllPasswordsPage", () => {
	cy.get('[data-testid="btnAllPasswords"]').click();
	cy.url().should("include", "/all");
});

Cypress.Commands.add("gotoMyPasswordsPage", () => {
	cy.get('[data-testid="btnMyPasswords"]').click();
	cy.url().should("include", "/personal");
});

Cypress.Commands.add("gotoEmployeePasswordsPage", () => {
	cy.get('[data-testid="btnEmployeePasswords"]').click();
	cy.url().should("include", "/shared");
});

Cypress.Commands.add("gotoGroupPasswordsPage", () => {
	cy.get('[data-testid="btnGroupPasswords"]').click();
	cy.url().should("include", "/group");
});

Cypress.Commands.add("gotoPrivatePasswordsPage", () => {
	cy.get('[data-testid="btnPrivatePasswords"]').click();
	cy.url().should("include", "/secret");
});

Cypress.Commands.add("fillOutPassword", (id) => {
	cy.get('[data-testid="pw-name"]').type(`name-${id}`);
	cy.get('[data-testid="pw-login"]').type(`login-${id}`);
	cy.get('[data-testid="pw-field"]').type(`${id}`);
	cy.get('[data-testid="pw-website"]').type(`website-${id}`);
	cy.get('[data-testid="pw-description"]').type(`description-${id}`);
	cy.get('[data-testid="btn-ok"]').click();
});

Cypress.Commands.add("checkPassword", (id) => {
	cy.get(`td:contains(name-${id})`).first().click();
	cy.get('[data-testid="btn-showpassword"]').click();

	cy.get('[data-testid="pw-field"]').within(() => {
		cy.get("input").should("have.value", id);
	});
	cy.get('[data-testid="btn-ok"]').click();
});

Cypress.Commands.add("passwordDontExist", (id) => {
	cy.get(`td:contains(name-${id})`).should("not.exist");
});

Cypress.Commands.add("clickAddBtn", () => {
	cy.get('[data-testid="btnAdd"]').click();
});

describe("Add password tests", () => {
	it("Add and view password by systemadmin", () => {
		cy.login("dumbledore@vaulteron.com", "12345");

		const p1 = Cypress._.random(0, 1e6);
		const p2 = Cypress._.random(0, 1e6);
		const p3 = Cypress._.random(0, 1e6);
		const p4 = Cypress._.random(0, 1e6);

		cy.gotoAllPasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p1);
		cy.checkPassword(p1);

		cy.gotoMyPasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p2);
		cy.checkPassword(p2);

		cy.gotoGroupPasswordsPage();
		cy.get('[data-testid="btn-add-Gryffindor"]').click();
		cy.fillOutPassword(p3);
		cy.checkPassword(p3);

		cy.gotoPrivatePasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p4);
		cy.checkPassword(p4);
	});

	it("Add and view password by member and systemadmin", () => {
		cy.login("harrypotter@vaulteron.com", "12345");

		const p1 = Cypress._.random(0, 1e6);
		const p2 = Cypress._.random(0, 1e6);
		const p3 = Cypress._.random(0, 1e6);
		const p4 = Cypress._.random(0, 1e6);

		cy.gotoAllPasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p1);
		cy.checkPassword(p1);

		cy.gotoMyPasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p2);
		cy.checkPassword(p2);

		cy.gotoGroupPasswordsPage();
		cy.get('[data-testid="btn-add-Gryffindor"]').click();
		cy.fillOutPassword(p3);
		cy.checkPassword(p3);

		cy.gotoPrivatePasswordsPage();
		cy.clickAddBtn();
		cy.fillOutPassword(p4);
		cy.checkPassword(p4);

		cy.login("dumbledore@vaulteron.com", "12345");
		cy.gotoEmployeePasswordsPage();
		cy.checkPassword(p1);
		cy.checkPassword(p2);
		cy.passwordDontExist(p3);
		cy.passwordDontExist(p4);

		cy.gotoGroupPasswordsPage();
		cy.checkPassword(p3);
	});
});
