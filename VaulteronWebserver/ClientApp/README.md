
# Update graphql schema
1. Go to the GraphQL Playground https://localhost:3000/api/ui
2. On the right side click on `SCHEMA` and then on `Download` an choose `SDL`
3. Copy the SDL into the `VaulteronWebserver/ClientApp/src/jsdoc/input/schema.graphql` file


# Generate Graphql routes and JSDoc types

Edit routes inside: `./ClientApp/src/graphql`

run `yarn run generate` in the `./ClientApp` directory


# Developing PWA

The files for the service workers will be generated on build. If you don´t want to restart the whole Webserver when you want to update the page run the following:

    yarn build
    serve -s build -l 3001

Note: For this to work the CORS must be configured to allow `http://localhost:3001` and the Same-Site cookie policy hast to be set to `None`.

Note: On localhost you do not need `https`. Service workers will be installed on `http` on localhost.

If you do not want to use the production build you will have to change the following `if` inside the file `serviceWorkerRegistration.js`

    if (process.env.NODE_ENV === "production" && "serviceWorker" in navigator) {

to the following:

    if ("serviceWorker" in navigator) {

## Icons created using IconKitchen

[https://icon.kitchen](https://icon.kitchen/i/H4sIAAAAAAAAAyWNQQ7CIBRE7zJuMbEm3bD1Ai7cGRc0wJcE%2BglQTdNwd3%2FtLGbxkjez4WPi4ir0Bk%2BPNTtohGTIQWGiG0cuQk7%2BH2Ge7sbaMNNuNM7Qw6hQAr0b9PUiErfG6cDReaHD2LtCYrvE%2FegJM9vCwcpa4Cr9dZN0jmY918bF4dV%2FQ4SeA5gAAAA%3D)