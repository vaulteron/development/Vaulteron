﻿const { app, BrowserWindow, screen: electronScreen } = require("electron");
const electron = require("electron");
const isEnvSet = "ELECTRON_IS_DEV" in process.env;
const getFromEnv = Number.parseInt(process.env.ELECTRON_IS_DEV, 10) === 1;
const isDev = isEnvSet ? getFromEnv : !electron.app.isPackaged;

const createMainWindow = () => {
	let mainWindow = new BrowserWindow({
		width: electronScreen.getPrimaryDisplay().workArea.width,
		height: electronScreen.getPrimaryDisplay().workArea.height,
		show: false,
		backgroundColor: "white",
		webPreferences: {
			nodeIntegration: false,
			devTools: isDev,
		},
	});
	const startURL = isDev ? "http://localhost:3000" : "http://app.vaulteron.com";

	mainWindow.loadURL(startURL);

	mainWindow.setMenu(null);

	mainWindow.once("ready-to-show", () => mainWindow.show());

	mainWindow.on("closed", () => {
		mainWindow = null;
	});

	mainWindow.webContents.on("new-window", (event, url) => {
		event.preventDefault();
		mainWindow.loadURL(url);
	});
};

app.whenReady().then(() => {
	createMainWindow();

	app.on("activate", () => {
		if (!BrowserWindow.getAllWindows().length) {
			createMainWindow();
		}
	});
});

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});
