import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Byte: any;
  DateTime: any;
  Guid: any;
  Long: any;
};

export type AccountPasswordAccessLogType = {
  __typename?: 'AccountPasswordAccessLogType';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type AccountPasswordMutation = {
  __typename?: 'AccountPasswordMutation';
  add?: Maybe<AccountPasswordType>;
  delete?: Maybe<AccountPasswordType>;
  edit?: Maybe<AccountPasswordType>;
};


export type AccountPasswordMutationAddArgs = {
  password: AddAccountPasswordInputType;
};


export type AccountPasswordMutationDeleteArgs = {
  id: Scalars['Guid'];
};


export type AccountPasswordMutationEditArgs = {
  password: EditAccountPasswordInputType;
};

export type AccountPasswordType = {
  __typename?: 'AccountPasswordType';
  accessLog?: Maybe<Array<Maybe<AccountPasswordAccessLogType>>>;
  archivedAt?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  encryptedString?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['ID']>;
  login: Scalars['String'];
  name: Scalars['String'];
  notes: Scalars['String'];
  owner?: Maybe<OwnUserType>;
  securityRating?: Maybe<Scalars['Int']>;
  tags?: Maybe<Array<Maybe<AccountTagType>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  websiteURL: Scalars['String'];
};

export type AccountTagMutation = {
  __typename?: 'AccountTagMutation';
  add?: Maybe<AccountTagType>;
  delete?: Maybe<AccountTagType>;
  edit?: Maybe<AccountTagType>;
};


export type AccountTagMutationAddArgs = {
  tag: AddAccountTagInputType;
};


export type AccountTagMutationDeleteArgs = {
  id: Scalars['Guid'];
};


export type AccountTagMutationEditArgs = {
  tag: EditAccountTagInputType;
};

export type AccountTagType = {
  __typename?: 'AccountTagType';
  color?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type AddAccountPasswordInputType = {
  encryptedPassword: Scalars['String'];
  keyPairId: Scalars['ID'];
  login: Scalars['String'];
  name: Scalars['String'];
  notes: Scalars['String'];
  tags?: InputMaybe<Array<Scalars['ID']>>;
  websiteUrl: Scalars['String'];
};

export type AddAccountTagInputType = {
  color: Scalars['String'];
  name: Scalars['String'];
};

export type AddCompanyPasswordInputType = {
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  login?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  notes?: InputMaybe<Scalars['String']>;
  websiteUrl?: InputMaybe<Scalars['String']>;
};

export type AddEncryptedAccountPasswordWithoutPublicKeyInputType = {
  accountPasswordId: Scalars['ID'];
  encryptedPasswordString: Scalars['String'];
};

export type AddEncryptedSharedPasswordWithoutPublicKeyInputType = {
  encryptedPasswordString: Scalars['String'];
  sharedPasswordId: Scalars['ID'];
};

export type AddOfflinePasswordInputType = {
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  login?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  notes?: InputMaybe<Scalars['String']>;
  websiteUrl?: InputMaybe<Scalars['String']>;
};

export type AddSharedPasswordInputType = {
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  groupId?: InputMaybe<Scalars['Guid']>;
  login?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  notes?: InputMaybe<Scalars['String']>;
  tags?: InputMaybe<Array<Scalars['ID']>>;
  websiteUrl?: InputMaybe<Scalars['String']>;
};

export type AddSharedTagInputType = {
  color: Scalars['String'];
  groupId: Scalars['ID'];
  name: Scalars['String'];
};

export type AddUserGroupType = {
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>;
  groupId: Scalars['ID'];
  groupRole: GroupRole;
  userId: Scalars['ID'];
};

export type AddUserInputType = {
  admin: Scalars['Boolean'];
  email: Scalars['String'];
  encryptedPrivateKey: Scalars['String'];
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>;
  firstname: Scalars['String'];
  foreignObjectId?: InputMaybe<Scalars['ID']>;
  groupsToBeAddedTo?: InputMaybe<Array<Scalars['ID']>>;
  lastname: Scalars['String'];
  newTemporaryLoginPassword: Scalars['String'];
  newTemporaryLoginPasswordHashed: Scalars['String'];
  publicKey: Scalars['String'];
  sex: Sex;
};

export type BulkSyncEncryptedSharedPasswordInputType = {
  encryptedPasswords: Array<InputMaybe<EncryptedSharedPasswordInputType>>;
  sharedPasswordToAddTo: Scalars['ID'];
};

export type ChangeLogType = {
  __typename?: 'ChangeLogType';
  createdAt: Scalars['DateTime'];
  createdBy?: Maybe<UserType>;
  createdById: Scalars['ID'];
  entityName: Scalars['String'];
  id: Scalars['ID'];
  newValue?: Maybe<ChangeLogValueType>;
  oldValue?: Maybe<ChangeLogValueType>;
  propertyName: Scalars['String'];
};

export type ChangeLogValueType = {
  __typename?: 'ChangeLogValueType';
  entityId?: Maybe<Scalars['Guid']>;
  name: Scalars['String'];
};

export type DeviceInformationMutation = {
  __typename?: 'DeviceInformationMutation';
  send?: Maybe<Scalars['Boolean']>;
};


export type DeviceInformationMutationSendArgs = {
  mail: SendScamMailType;
};

export type DeviceInformationType = {
  __typename?: 'DeviceInformationType';
  browser: Scalars['String'];
  browserVersion: Scalars['String'];
  city: Scalars['String'];
  cityLatLong: Scalars['String'];
  countryCode: Scalars['String'];
  countryName: Scalars['String'];
  createdAt?: Maybe<Scalars['DateTime']>;
  deviceBrand: Scalars['String'];
  deviceFamily: Scalars['String'];
  deviceModel: Scalars['String'];
  id?: Maybe<Scalars['ID']>;
  ipAddress: Scalars['String'];
  jSONInput: Scalars['String'];
  mail: Scalars['String'];
  os: Scalars['String'];
  osVersion: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type EditAccountPasswordInputType = {
  encryptedPassword?: InputMaybe<EncryptedAccountPasswordInputType>;
  id: Scalars['ID'];
  login: Scalars['String'];
  name: Scalars['String'];
  notes: Scalars['String'];
  tags?: InputMaybe<Array<Scalars['ID']>>;
  websiteURL: Scalars['String'];
};

export type EditAccountTagInputType = {
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type EditGroupInputType = {
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type EditSharedPasswordInputType = {
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  id: Scalars['ID'];
  login?: InputMaybe<Scalars['String']>;
  modifyLock: Scalars['String'];
  name?: InputMaybe<Scalars['String']>;
  notes?: InputMaybe<Scalars['String']>;
  tags?: InputMaybe<Array<Scalars['ID']>>;
  websiteURL?: InputMaybe<Scalars['String']>;
};

export type EditSharedTagInputType = {
  groupId: Scalars['ID'];
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type EditUserGroupType = {
  groupId: Scalars['ID'];
  groupRole: GroupRole;
  userId: Scalars['ID'];
};

export type EditUserInputType = {
  email?: InputMaybe<Scalars['String']>;
  encryptedLoginPassword?: InputMaybe<Scalars['String']>;
  firstname?: InputMaybe<Scalars['String']>;
  foreignObjectId?: InputMaybe<Scalars['ID']>;
  id?: InputMaybe<Scalars['ID']>;
  lastname?: InputMaybe<Scalars['String']>;
  securityQuestion1?: InputMaybe<Scalars['String']>;
  securityQuestion2?: InputMaybe<Scalars['String']>;
  securityQuestion3?: InputMaybe<Scalars['String']>;
};

export type EncryptedAccountPasswordInputType = {
  encryptedPasswordString: Scalars['String'];
  keyPairId: Scalars['ID'];
};

export type EncryptedSharedPasswordInputType = {
  encryptedPasswordString: Scalars['String'];
  keyPairId: Scalars['ID'];
  sharedPasswordId?: InputMaybe<Scalars['ID']>;
};

export type GroupInputType = {
  name: Scalars['String'];
  parentGroupId?: InputMaybe<Scalars['ID']>;
};

export type GroupMutations = {
  __typename?: 'GroupMutations';
  add?: Maybe<GroupType>;
  addUser?: Maybe<UserType>;
  archive?: Maybe<Array<Maybe<GroupType>>>;
  delete?: Maybe<Array<Maybe<GroupType>>>;
  deleteUser?: Maybe<UserType>;
  edit?: Maybe<GroupType>;
  editUser?: Maybe<UserType>;
  move?: Maybe<GroupType>;
};


export type GroupMutationsAddArgs = {
  group: GroupInputType;
};


export type GroupMutationsAddUserArgs = {
  user: AddUserGroupType;
};


export type GroupMutationsArchiveArgs = {
  groupId: Scalars['Guid'];
  reactivate?: InputMaybe<Scalars['Boolean']>;
};


export type GroupMutationsDeleteArgs = {
  groupId: Scalars['Guid'];
};


export type GroupMutationsDeleteUserArgs = {
  groupId: Scalars['Guid'];
  userId: Scalars['Guid'];
};


export type GroupMutationsEditArgs = {
  group: EditGroupInputType;
};


export type GroupMutationsEditUserArgs = {
  user: EditUserGroupType;
};


export type GroupMutationsMoveArgs = {
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  groupToMoveId: Scalars['Guid'];
  newParentGroupId?: InputMaybe<Scalars['Guid']>;
};

export enum GroupRole {
  GroupAdmin = 'GROUP_ADMIN',
  GroupEditor = 'GROUP_EDITOR',
  GroupViewer = 'GROUP_VIEWER'
}

export type GroupType = {
  __typename?: 'GroupType';
  archivedAt?: Maybe<Scalars['DateTime']>;
  children?: Maybe<Array<Maybe<GroupType>>>;
  countPasswords?: Maybe<Scalars['Int']>;
  countSubGroups?: Maybe<Scalars['Int']>;
  countTags?: Maybe<Scalars['Int']>;
  countUsers?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  parentGroupId?: Maybe<Scalars['ID']>;
  passwords?: Maybe<Array<Maybe<SharedPasswordType>>>;
  tags?: Maybe<Array<Maybe<SharedTagType>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  users?: Maybe<Array<Maybe<UserType>>>;
  usersWithAccess?: Maybe<Array<Maybe<UserType>>>;
};

export type KeyPairChangeLogType = {
  __typename?: 'KeyPairChangeLogType';
  createdAt?: Maybe<Scalars['DateTime']>;
  encryptedPrivateKey: Scalars['String'];
  id?: Maybe<Scalars['ID']>;
  previousPasswordHashEncryptedWithPublicKey?: Maybe<Scalars['String']>;
  publicKeyString: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type KeyPairType = {
  __typename?: 'KeyPairType';
  createdAt?: Maybe<Scalars['DateTime']>;
  encryptedPrivateKey: Scalars['String'];
  id?: Maybe<Scalars['ID']>;
  publicKeyString: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export enum KindOfPassword {
  Company = 'Company',
  Group = 'Group',
  Private = 'Private'
}

export enum LicenceStatus {
  None = 'NONE',
  Standard = 'STANDARD',
  Trial = 'TRIAL'
}

export type MandatorMutations = {
  __typename?: 'MandatorMutations';
  delete?: Maybe<MandatorType>;
  edit?: Maybe<MandatorType>;
};


export type MandatorMutationsDeleteArgs = {
  mandatorId: Scalars['Guid'];
};


export type MandatorMutationsEditArgs = {
  twoFactorRequired: Scalars['Boolean'];
};

export type MandatorType = {
  __typename?: 'MandatorType';
  createdAt?: Maybe<Scalars['DateTime']>;
  customerinformation?: Maybe<StripeCustomerType>;
  id?: Maybe<Scalars['ID']>;
  isBusinessCustomer: Scalars['Boolean'];
  name: Scalars['String'];
  subscriptionStatus: LicenceStatus;
  subscriptionUserLimit: Scalars['Long'];
  twoFactorRequired: Scalars['Boolean'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type MoveSharedPasswordToGroupInputType = {
  destinationGroupId: Scalars['ID'];
  encryptedPasswords: Array<InputMaybe<EncryptedSharedPasswordInputType>>;
  passwordIds: Array<InputMaybe<Scalars['Guid']>>;
};

export type OwnUserType = {
  __typename?: 'OwnUserType';
  admin: Scalars['Boolean'];
  archivedAt?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  email: Scalars['String'];
  emailConfirmed: Scalars['Boolean'];
  firstname: Scalars['String'];
  foreignObjectId?: Maybe<Scalars['ID']>;
  groups?: Maybe<Array<Maybe<GroupType>>>;
  id: Scalars['ID'];
  keyPair: KeyPairType;
  lastname: Scalars['String'];
  mandator: MandatorType;
  passwordChangedAt?: Maybe<Scalars['DateTime']>;
  passwordReset?: Maybe<Scalars['Boolean']>;
  publicKey?: Maybe<PublicKeyType>;
  registeredWebAuthnKeys?: Maybe<Array<Maybe<WebauthnKeyType>>>;
  sex: Sex;
  superAdmin: Scalars['Boolean'];
  twoFATypesEnabled?: Maybe<Array<Maybe<TwoFaTypes>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  usergroups?: Maybe<Array<Maybe<UserGroupType>>>;
};

export type PasswordChangeLogType = {
  __typename?: 'PasswordChangeLogType';
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<UserType>;
  createdById?: Maybe<Scalars['ID']>;
  encryptedPasswordString: Scalars['String'];
  id?: Maybe<Scalars['ID']>;
  keyPairId: Scalars['ID'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type PasswordType = {
  __typename?: 'PasswordType';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  login: Scalars['String'];
  modifyLock?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  notes: Scalars['String'];
  type?: Maybe<KindOfPassword>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  websiteURL: Scalars['String'];
};

export type PublicKeyType = {
  __typename?: 'PublicKeyType';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  publicKeyString: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type SendScamMailType = {
  mails: Array<InputMaybe<Scalars['String']>>;
  redirectURL: Scalars['String'];
  senderName: Scalars['String'];
  templateId: Scalars['String'];
};

export enum Sex {
  Female = 'Female',
  Male = 'Male',
  Unset = 'Unset'
}

export type SharedPasswordAccessLogType = {
  __typename?: 'SharedPasswordAccessLogType';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  user?: Maybe<UserType>;
  userId: Scalars['ID'];
};

export type SharedPasswordMutations = {
  __typename?: 'SharedPasswordMutations';
  add?: Maybe<SharedPasswordType>;
  /** @deprecated Encryptions should be provided within every mutation changing public-keys, password-text or access to passwords. Can still be used if ihe migration did not work, or if something else breaks (just in case). */
  bulkSyncEncryptedPassword?: Maybe<SharedPasswordType>;
  companyAdd?: Maybe<SharedPasswordType>;
  companyAddForUser?: Maybe<SharedPasswordType>;
  companyMoveToShared?: Maybe<Array<Maybe<SharedPasswordType>>>;
  delete?: Maybe<SharedPasswordType>;
  edit?: Maybe<SharedPasswordType>;
  moveToGroup?: Maybe<Array<Maybe<SharedPasswordType>>>;
  offlineAdd?: Maybe<SharedPasswordType>;
  sharedMoveToCompany?: Maybe<Array<Maybe<SharedPasswordType>>>;
};


export type SharedPasswordMutationsAddArgs = {
  password: AddSharedPasswordInputType;
};


export type SharedPasswordMutationsBulkSyncEncryptedPasswordArgs = {
  encryptedPasswords: BulkSyncEncryptedSharedPasswordInputType;
};


export type SharedPasswordMutationsCompanyAddArgs = {
  password: AddCompanyPasswordInputType;
};


export type SharedPasswordMutationsCompanyAddForUserArgs = {
  forUserId?: InputMaybe<Scalars['Guid']>;
  password: AddCompanyPasswordInputType;
};


export type SharedPasswordMutationsCompanyMoveToSharedArgs = {
  destinationGroupId: Scalars['Guid'];
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  passwordsToMoveIds: Array<InputMaybe<Scalars['Guid']>>;
};


export type SharedPasswordMutationsDeleteArgs = {
  id: Scalars['Guid'];
};


export type SharedPasswordMutationsEditArgs = {
  password: EditSharedPasswordInputType;
};


export type SharedPasswordMutationsMoveToGroupArgs = {
  input: MoveSharedPasswordToGroupInputType;
};


export type SharedPasswordMutationsOfflineAddArgs = {
  password: AddOfflinePasswordInputType;
};


export type SharedPasswordMutationsSharedMoveToCompanyArgs = {
  destinationUserId: Scalars['Guid'];
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>>>;
  passwordsToMoveIds: Array<InputMaybe<Scalars['Guid']>>;
};

export type SharedPasswordType = {
  __typename?: 'SharedPasswordType';
  accessLog?: Maybe<Array<Maybe<SharedPasswordAccessLogType>>>;
  archivedAt?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<UserType>;
  encryptedString?: Maybe<Scalars['String']>;
  groupId?: Maybe<Scalars['ID']>;
  id?: Maybe<Scalars['ID']>;
  isSavedAsOfflinePassword: Scalars['Boolean'];
  login: Scalars['String'];
  modifyLock: Scalars['String'];
  name: Scalars['String'];
  notes: Scalars['String'];
  securityRating?: Maybe<Scalars['Int']>;
  tags?: Maybe<Array<Maybe<SharedTagType>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  websiteURL: Scalars['String'];
};

export type SharedTagMutation = {
  __typename?: 'SharedTagMutation';
  add?: Maybe<SharedTagType>;
  delete?: Maybe<SharedTagType>;
  edit?: Maybe<SharedTagType>;
};


export type SharedTagMutationAddArgs = {
  tag: AddSharedTagInputType;
};


export type SharedTagMutationDeleteArgs = {
  id: Scalars['Guid'];
};


export type SharedTagMutationEditArgs = {
  tag: EditSharedTagInputType;
};

export type SharedTagType = {
  __typename?: 'SharedTagType';
  color?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  group?: Maybe<GroupType>;
  id?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type StripeCustomerType = {
  __typename?: 'StripeCustomerType';
  defaultPaymentMethodId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type TotpAuthenticatorKeyType = {
  __typename?: 'TotpAuthenticatorKeyType';
  authenticatorString: Scalars['String'];
  key: Scalars['String'];
};

export enum TwoFaTypes {
  Totp = 'TOTP',
  WebAuthn = 'WEB_AUTHN'
}

export type TwoFactorMutation = {
  __typename?: 'TwoFactorMutation';
  addWebauthnKey?: Maybe<OwnUserType>;
  disableTotp?: Maybe<UserType>;
  enableTotp?: Maybe<UserType>;
  getTotpAuthenticatorKey?: Maybe<TotpAuthenticatorKeyType>;
  getWebauthnChallenge?: Maybe<WebauthnChallengeType>;
  removeWebauthnKey?: Maybe<OwnUserType>;
};


export type TwoFactorMutationAddWebauthnKeyArgs = {
  attestationResponse: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
};


export type TwoFactorMutationEnableTotpArgs = {
  totp?: InputMaybe<Scalars['String']>;
};


export type TwoFactorMutationRemoveWebauthnKeyArgs = {
  webauthnKeyId: Scalars['Guid'];
};

export type UserGroupType = {
  __typename?: 'UserGroupType';
  group?: Maybe<GroupType>;
  groupId: Scalars['ID'];
  groupRole: GroupRole;
  user?: Maybe<UserType>;
  userId: Scalars['ID'];
};

export type UserMutation = {
  __typename?: 'UserMutation';
  add?: Maybe<UserType>;
  archive?: Maybe<UserType>;
  changeOwnLoginPassword?: Maybe<UserType>;
  edit?: Maybe<UserType>;
  grantAdminPermissions?: Maybe<UserType>;
  resendAccountActivationEmail?: Maybe<UserType>;
  revokeAdminPermissions?: Maybe<UserType>;
  twofactor?: Maybe<TwoFactorMutation>;
};


export type UserMutationAddArgs = {
  user: AddUserInputType;
};


export type UserMutationArchiveArgs = {
  encryptedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>;
  id: Scalars['Guid'];
  reactivate?: InputMaybe<Scalars['Boolean']>;
};


export type UserMutationChangeOwnLoginPasswordArgs = {
  currentPassword: Scalars['String'];
  encryptedAccountPasswords: Array<InputMaybe<AddEncryptedAccountPasswordWithoutPublicKeyInputType>>;
  encryptedPrivateKey: Scalars['String'];
  encryptedSharedPasswords: Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>;
  newPassword: Scalars['String'];
  previousPasswordHashEncryptedWithPublicKey: Scalars['String'];
  publicKeyString: Scalars['String'];
};


export type UserMutationEditArgs = {
  user: EditUserInputType;
};


export type UserMutationGrantAdminPermissionsArgs = {
  encryptedPasswords: Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>;
  targetUserId: Scalars['Guid'];
};


export type UserMutationResendAccountActivationEmailArgs = {
  encryptedPrivateKey: Scalars['String'];
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>>;
  newTempLoginPassword: Scalars['String'];
  newTempLoginPasswordHashed: Scalars['String'];
  publicKey: Scalars['String'];
  targetUserId: Scalars['Guid'];
};


export type UserMutationRevokeAdminPermissionsArgs = {
  targetUserId: Scalars['Guid'];
};

export type UserType = {
  __typename?: 'UserType';
  admin: Scalars['Boolean'];
  archivedAt?: Maybe<Scalars['DateTime']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  email: Scalars['String'];
  emailConfirmed: Scalars['Boolean'];
  firstname: Scalars['String'];
  foreignObjectId?: Maybe<Scalars['ID']>;
  groups?: Maybe<Array<Maybe<GroupType>>>;
  id: Scalars['ID'];
  lastname: Scalars['String'];
  passwordChangedAt?: Maybe<Scalars['DateTime']>;
  passwordReset?: Maybe<Scalars['Boolean']>;
  publicKey?: Maybe<PublicKeyType>;
  sex: Sex;
  twoFATypesEnabled?: Maybe<Array<Maybe<TwoFaTypes>>>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  usergroups?: Maybe<Array<Maybe<UserGroupType>>>;
};

export type Vaulteron_Root_Mutation = {
  __typename?: 'Vaulteron_root_mutation';
  accountPassword?: Maybe<AccountPasswordMutation>;
  accountTag?: Maybe<AccountTagMutation>;
  device?: Maybe<DeviceInformationMutation>;
  group?: Maybe<GroupMutations>;
  mandator?: Maybe<MandatorMutations>;
  sharedPassword?: Maybe<SharedPasswordMutations>;
  sharedTag?: Maybe<SharedTagMutation>;
  user?: Maybe<UserMutation>;
};

export type Vaulteron_Root_Query = {
  __typename?: 'Vaulteron_root_query';
  accountPasswords?: Maybe<Array<Maybe<AccountPasswordType>>>;
  accountTags?: Maybe<Array<Maybe<AccountTagType>>>;
  admins?: Maybe<Array<Maybe<UserType>>>;
  billingSecret?: Maybe<Scalars['String']>;
  changeLogs?: Maybe<Array<Maybe<ChangeLogType>>>;
  companyPasswords?: Maybe<Array<Maybe<SharedPasswordType>>>;
  customerBillingPortalUrl?: Maybe<Scalars['String']>;
  customerCheckoutUrl?: Maybe<Scalars['String']>;
  deviceInformation?: Maybe<Array<Maybe<DeviceInformationType>>>;
  encryptedAccountPasswordString?: Maybe<Scalars['String']>;
  encryptedSharedPasswordString?: Maybe<Scalars['String']>;
  groupTree?: Maybe<Scalars['String']>;
  groups?: Maybe<Array<Maybe<GroupType>>>;
  groupsByParent?: Maybe<Array<Maybe<GroupType>>>;
  keyPairChangeLogs?: Maybe<Array<Maybe<KeyPairChangeLogType>>>;
  mandators?: Maybe<Array<Maybe<MandatorType>>>;
  me?: Maybe<OwnUserType>;
  missingEncryptionsForPassword?: Maybe<Array<Maybe<PublicKeyType>>>;
  missingEncryptionsForUser?: Maybe<Array<Maybe<Scalars['Guid']>>>;
  offlinePasswords?: Maybe<Array<Maybe<SharedPasswordType>>>;
  passwordChangeLogs?: Maybe<Array<Maybe<PasswordChangeLogType>>>;
  passwordsByURL?: Maybe<Array<Maybe<PasswordType>>>;
  pwned?: Maybe<Scalars['String']>;
  sharedPasswords?: Maybe<Array<Maybe<SharedPasswordType>>>;
  sharedTags?: Maybe<Array<Maybe<SharedTagType>>>;
  users?: Maybe<Array<Maybe<UserType>>>;
};


export type Vaulteron_Root_QueryAccountPasswordsArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  searchTerm?: InputMaybe<Scalars['String']>;
  tagIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
};


export type Vaulteron_Root_QueryAccountTagsArgs = {
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryChangeLogsArgs = {
  entityId: Scalars['Guid'];
  from?: InputMaybe<Scalars['DateTime']>;
  propertyName?: InputMaybe<Scalars['String']>;
  to?: InputMaybe<Scalars['DateTime']>;
};


export type Vaulteron_Root_QueryCompanyPasswordsArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  createdByIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryEncryptedAccountPasswordStringArgs = {
  passwordId: Scalars['Guid'];
  publicKeyId?: InputMaybe<Scalars['Guid']>;
};


export type Vaulteron_Root_QueryEncryptedSharedPasswordStringArgs = {
  passwordId: Scalars['Guid'];
};


export type Vaulteron_Root_QueryGroupTreeArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  withRole?: InputMaybe<GroupRole>;
};


export type Vaulteron_Root_QueryGroupsArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  ids?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryGroupsByParentArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  ids?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  parentId?: InputMaybe<Scalars['Guid']>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryKeyPairChangeLogsArgs = {
  from?: InputMaybe<Scalars['DateTime']>;
  to?: InputMaybe<Scalars['DateTime']>;
};


export type Vaulteron_Root_QueryMissingEncryptionsForPasswordArgs = {
  passwordId: Scalars['Guid'];
};


export type Vaulteron_Root_QueryMissingEncryptionsForUserArgs = {
  userId: Scalars['Guid'];
};


export type Vaulteron_Root_QueryOfflinePasswordsArgs = {
  createdByIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryPasswordChangeLogsArgs = {
  passwordId: Scalars['Guid'];
};


export type Vaulteron_Root_QueryPasswordsByUrlArgs = {
  url?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryPwnedArgs = {
  mail: Scalars['String'];
};


export type Vaulteron_Root_QuerySharedPasswordsArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  groupIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QuerySharedTagsArgs = {
  groupId?: InputMaybe<Scalars['Guid']>;
  searchTerm?: InputMaybe<Scalars['String']>;
};


export type Vaulteron_Root_QueryUsersArgs = {
  byArchived?: InputMaybe<Scalars['Boolean']>;
  excludeUsersInGroupIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  groupIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  searchTerm?: InputMaybe<Scalars['String']>;
  userIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
  withoutUserIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>>>;
};

export type WebauthnChallengeType = {
  __typename?: 'WebauthnChallengeType';
  challenge: Array<Scalars['Byte']>;
  optionsJson: Scalars['String'];
};

export type WebauthnKeyType = {
  __typename?: 'WebauthnKeyType';
  createdAt?: Maybe<Scalars['DateTime']>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
};

export type AccountPasswordFieldsFragment = { __typename?: 'AccountPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null };

export type GetAccountPasswordQueryVariables = Exact<{
  tagIds?: Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>;
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetAccountPasswordQuery = { __typename?: 'Vaulteron_root_query', accountPasswords?: Array<{ __typename?: 'AccountPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null } | null> | null };

export type GetAccountPasswordWithEncryptedStringQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAccountPasswordWithEncryptedStringQuery = { __typename?: 'Vaulteron_root_query', accountPasswords?: Array<{ __typename?: 'AccountPasswordType', encryptedString?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null } | null> | null };

export type AddAccountPasswordMutationVariables = Exact<{
  accountPassword: AddAccountPasswordInputType;
}>;


export type AddAccountPasswordMutation = { __typename?: 'Vaulteron_root_mutation', accountPassword?: { __typename?: 'AccountPasswordMutation', add?: { __typename?: 'AccountPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null } | null } | null };

export type DeleteAccountPasswordMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteAccountPasswordMutation = { __typename?: 'Vaulteron_root_mutation', accountPassword?: { __typename?: 'AccountPasswordMutation', delete?: { __typename?: 'AccountPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null } | null } | null };

export type GetCustomerBillingPortalQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCustomerBillingPortalQuery = { __typename?: 'Vaulteron_root_query', customerBillingPortalUrl?: string | null };

export type GetCustomerCheckoutUrlQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCustomerCheckoutUrlQuery = { __typename?: 'Vaulteron_root_query', customerCheckoutUrl?: string | null };

export type GetBillingSecretQueryVariables = Exact<{ [key: string]: never; }>;


export type GetBillingSecretQuery = { __typename?: 'Vaulteron_root_query', billingSecret?: string | null };

export type GetCompanyPasswordQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetCompanyPasswordQuery = { __typename?: 'Vaulteron_root_query', companyPasswords?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type AddCompanyPasswordMutationVariables = Exact<{
  sharedPassword: AddCompanyPasswordInputType;
}>;


export type AddCompanyPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', companyAdd?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type AddCompanyPasswordForUserMutationVariables = Exact<{
  sharedPassword: AddCompanyPasswordInputType;
  forUserId: Scalars['Guid'];
}>;


export type AddCompanyPasswordForUserMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', companyAddForUser?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type DeviceFieldsFragment = { __typename?: 'DeviceInformationType', id?: string | null, browser: string, browserVersion: string, city: string, cityLatLong: string, countryCode: string, countryName: string, deviceBrand: string, deviceFamily: string, deviceModel: string, ipAddress: string, mail: string, os: string, osVersion: string, createdAt?: any | null };

export type GetDeviceInformationQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDeviceInformationQuery = { __typename?: 'Vaulteron_root_query', deviceInformation?: Array<{ __typename?: 'DeviceInformationType', id?: string | null, browser: string, browserVersion: string, city: string, cityLatLong: string, countryCode: string, countryName: string, deviceBrand: string, deviceFamily: string, deviceModel: string, ipAddress: string, mail: string, os: string, osVersion: string, createdAt?: any | null } | null> | null };

export type SendScamMailMutationVariables = Exact<{
  mail: SendScamMailType;
}>;


export type SendScamMailMutation = { __typename?: 'Vaulteron_root_mutation', device?: { __typename?: 'DeviceInformationMutation', send?: boolean | null } | null };

export type GroupFieldsFragment = { __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null };

export type GetGroupTreeQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetGroupTreeQuery = { __typename?: 'Vaulteron_root_query', groupTree?: string | null };

export type GetGroupTreeByRoleQueryVariables = Exact<{
  withRole?: InputMaybe<GroupRole>;
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetGroupTreeByRoleQuery = { __typename?: 'Vaulteron_root_query', groupTree?: string | null };

export type GetGroupsByParentQueryVariables = Exact<{
  parentId?: InputMaybe<Scalars['Guid']>;
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetGroupsByParentQuery = { __typename?: 'Vaulteron_root_query', groups?: Array<{ __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null> | null };

export type GetAllGroupsQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetAllGroupsQuery = { __typename?: 'Vaulteron_root_query', groups?: Array<{ __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null> | null };

export type AddUserToGroupMutationVariables = Exact<{
  user: AddUserGroupType;
}>;


export type AddUserToGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', addUser?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type DeleteUserFromGroupMutationVariables = Exact<{
  groupId: Scalars['Guid'];
  userId: Scalars['Guid'];
}>;


export type DeleteUserFromGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', deleteUser?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type AddGroupMutationVariables = Exact<{
  group: GroupInputType;
}>;


export type AddGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', add?: { __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null } | null };

export type EditGroupMutationVariables = Exact<{
  group: EditGroupInputType;
}>;


export type EditGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', edit?: { __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null } | null };

export type DeleteGroupMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', delete?: Array<{ __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null> | null } | null };

export type ArchiveGroupMutationVariables = Exact<{
  groupId: Scalars['Guid'];
  reactivate?: InputMaybe<Scalars['Boolean']>;
}>;


export type ArchiveGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', archive?: Array<{ __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null> | null } | null };

export type MoveGroupMutationVariables = Exact<{
  groupToMoveId: Scalars['Guid'];
  newParentGroupId?: InputMaybe<Scalars['Guid']>;
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>> | InputMaybe<EncryptedSharedPasswordInputType>>;
}>;


export type MoveGroupMutation = { __typename?: 'Vaulteron_root_mutation', group?: { __typename?: 'GroupMutations', move?: { __typename?: 'GroupType', id?: string | null, parentGroupId?: string | null, name: string, archivedAt?: any | null, countPasswords?: number | null, countSubGroups?: number | null, countUsers?: number | null, countTags?: number | null } | null } | null };

export type GetAllGroupPasswordQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetAllGroupPasswordQuery = { __typename?: 'Vaulteron_root_query', sharedPasswords?: Array<{ __typename?: 'SharedPasswordType', groupId?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type GetGroupPasswordQueryVariables = Exact<{
  groupIds: Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>;
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetGroupPasswordQuery = { __typename?: 'Vaulteron_root_query', sharedPasswords?: Array<{ __typename?: 'SharedPasswordType', groupId?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type AddGroupPasswordMutationVariables = Exact<{
  sharedPassword: AddSharedPasswordInputType;
}>;


export type AddGroupPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', add?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type DeleteGroupPasswordMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteGroupPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', delete?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type MandatorFieldsFragment = { __typename?: 'MandatorType', id?: string | null, updatedAt?: any | null, createdAt?: any | null, name: string, twoFactorRequired: boolean, subscriptionUserLimit: any };

export type GetMandatorsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMandatorsQuery = { __typename?: 'Vaulteron_root_query', mandators?: Array<{ __typename?: 'MandatorType', id?: string | null, updatedAt?: any | null, createdAt?: any | null, name: string, twoFactorRequired: boolean, subscriptionUserLimit: any } | null> | null };

export type EditMandatorMutationVariables = Exact<{
  twoFactorRequired: Scalars['Boolean'];
}>;


export type EditMandatorMutation = { __typename?: 'Vaulteron_root_mutation', mandator?: { __typename?: 'MandatorMutations', edit?: { __typename?: 'MandatorType', id?: string | null, updatedAt?: any | null, createdAt?: any | null, name: string, twoFactorRequired: boolean, subscriptionUserLimit: any } | null } | null };

export type DeleteMandatorMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteMandatorMutation = { __typename?: 'Vaulteron_root_mutation', mandator?: { __typename?: 'MandatorMutations', delete?: { __typename?: 'MandatorType', id?: string | null, updatedAt?: any | null, createdAt?: any | null, name: string, twoFactorRequired: boolean, subscriptionUserLimit: any } | null } | null };

export type GetOfflinePasswordQueryVariables = Exact<{ [key: string]: never; }>;


export type GetOfflinePasswordQuery = { __typename?: 'Vaulteron_root_query', offlinePasswords?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type GetOfflinePasswordWithEncryptedStringQueryVariables = Exact<{
  searchTerm: Scalars['String'];
  createdByIds?: InputMaybe<Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>>;
}>;


export type GetOfflinePasswordWithEncryptedStringQuery = { __typename?: 'Vaulteron_root_query', offlinePasswords?: Array<{ __typename?: 'SharedPasswordType', encryptedString?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type AddOfflinePasswordMutationVariables = Exact<{
  offlinePassword: AddOfflinePasswordInputType;
}>;


export type AddOfflinePasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', offlineAdd?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type DeleteOfflinePasswordMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteOfflinePasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', delete?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string, firstname: string, lastname: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type SharedPasswordFieldsFragment = { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null };

export type GetSyncedPasswordQueryVariables = Exact<{ [key: string]: never; }>;


export type GetSyncedPasswordQuery = { __typename?: 'Vaulteron_root_query', companyPasswords?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, sharedPasswords?: Array<{ __typename?: 'SharedPasswordType', groupId?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, offlinePasswords?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type GetSyncedPasswordWithEncryptedStringQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetSyncedPasswordWithEncryptedStringQuery = { __typename?: 'Vaulteron_root_query', companyPasswords?: Array<{ __typename?: 'SharedPasswordType', encryptedString?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, sharedPasswords?: Array<{ __typename?: 'SharedPasswordType', groupId?: string | null, encryptedString?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, offlinePasswords?: Array<{ __typename?: 'SharedPasswordType', encryptedString?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null };

export type GetAllPasswordQueryVariables = Exact<{
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetAllPasswordQuery = { __typename?: 'Vaulteron_root_query', companyPasswords?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, createdBy?: { __typename?: 'UserType', id: string } | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, sharedPasswords?: Array<{ __typename?: 'SharedPasswordType', groupId?: string | null, id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null, accountPasswords?: Array<{ __typename?: 'AccountPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, updatedAt?: any | null, archivedAt?: any | null, tags?: Array<{ __typename?: 'AccountTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'AccountPasswordAccessLogType', id?: string | null, updatedAt?: any | null } | null> | null } | null> | null };

export type DeleteSharedPasswordMutationVariables = Exact<{
  id: Scalars['Guid'];
}>;


export type DeleteSharedPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', delete?: { __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null } | null };

export type MoveCompanyPasswordToSharedPasswordMutationVariables = Exact<{
  passwordsToMoveIds: Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>;
  destinationGroupId: Scalars['Guid'];
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>> | InputMaybe<EncryptedSharedPasswordInputType>>;
}>;


export type MoveCompanyPasswordToSharedPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', companyMoveToShared?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null } | null };

export type MoveSharedPasswordToCompanyPasswordMutationVariables = Exact<{
  passwordsToMoveIds: Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>;
  destinationUserId: Scalars['Guid'];
  encryptedPasswords?: InputMaybe<Array<InputMaybe<EncryptedSharedPasswordInputType>> | InputMaybe<EncryptedSharedPasswordInputType>>;
}>;


export type MoveSharedPasswordToCompanyPasswordMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', sharedMoveToCompany?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null } | null };

export type MoveSharedPasswordsToGroupMutationVariables = Exact<{
  input: MoveSharedPasswordToGroupInputType;
}>;


export type MoveSharedPasswordsToGroupMutation = { __typename?: 'Vaulteron_root_mutation', sharedPassword?: { __typename?: 'SharedPasswordMutations', moveToGroup?: Array<{ __typename?: 'SharedPasswordType', id?: string | null, name: string, login: string, securityRating?: number | null, websiteURL: string, notes: string, modifyLock: string, isSavedAsOfflinePassword: boolean, updatedAt?: any | null, archivedAt?: any | null, groupId?: string | null, tags?: Array<{ __typename?: 'SharedTagType', id?: string | null, name: string, color?: string | null } | null> | null, accessLog?: Array<{ __typename?: 'SharedPasswordAccessLogType', id?: string | null, updatedAt?: any | null, user?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null } | null> | null } | null };

export type GetPasswordHistoryQueryVariables = Exact<{
  entityId: Scalars['Guid'];
  from?: InputMaybe<Scalars['DateTime']>;
  to?: InputMaybe<Scalars['DateTime']>;
  propertyName?: InputMaybe<Scalars['String']>;
}>;


export type GetPasswordHistoryQuery = { __typename?: 'Vaulteron_root_query', changeLogs?: Array<{ __typename?: 'ChangeLogType', id: string, entityName: string, propertyName: string, createdAt: any, createdById: string, createdBy?: { __typename?: 'UserType', firstname: string, lastname: string } | null, oldValue?: { __typename?: 'ChangeLogValueType', entityId?: any | null, name: string } | null, newValue?: { __typename?: 'ChangeLogValueType', entityId?: any | null, name: string } | null } | null> | null };

export type GetPasswordChangeLogsQueryVariables = Exact<{
  passwordId: Scalars['Guid'];
}>;


export type GetPasswordChangeLogsQuery = { __typename?: 'Vaulteron_root_query', passwordChangeLogs?: Array<{ __typename?: 'PasswordChangeLogType', id?: string | null, keyPairId: string, createdAt?: any | null, encryptedPasswordString: string, createdById?: string | null, createdBy?: { __typename?: 'UserType', firstname: string, lastname: string } | null } | null> | null };

export type GetKeyPairHistoryQueryVariables = Exact<{ [key: string]: never; }>;


export type GetKeyPairHistoryQuery = { __typename?: 'Vaulteron_root_query', keyPairChangeLogs?: Array<{ __typename?: 'KeyPairChangeLogType', id?: string | null, createdAt?: any | null, encryptedPrivateKey: string, previousPasswordHashEncryptedWithPublicKey?: string | null } | null> | null };

export type GetPwnedMailQueryVariables = Exact<{
  mail: Scalars['String'];
}>;


export type GetPwnedMailQuery = { __typename?: 'Vaulteron_root_query', pwned?: string | null };

export type UserFieldsFragment = { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null };

export type GetUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetUsersQuery = { __typename?: 'Vaulteron_root_query', users?: Array<{ __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null> | null };

export type GetUsersInGroupQueryVariables = Exact<{
  groupIds: Array<InputMaybe<Scalars['Guid']>> | InputMaybe<Scalars['Guid']>;
  byArchived?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetUsersInGroupQuery = { __typename?: 'Vaulteron_root_query', users?: Array<{ __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, admin: boolean, usergroups?: Array<{ __typename?: 'UserGroupType', groupRole: GroupRole, group?: { __typename?: 'GroupType', id?: string | null, name: string } | null } | null> | null } | null> | null };

export type GetMyselfQueryVariables = Exact<{ [key: string]: never; }>;


export type GetMyselfQuery = { __typename?: 'Vaulteron_root_query', me?: { __typename?: 'OwnUserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, superAdmin: boolean, emailConfirmed: boolean, twoFATypesEnabled?: Array<TwoFaTypes | null> | null, passwordChangedAt?: any | null, passwordReset?: boolean | null, registeredWebAuthnKeys?: Array<{ __typename?: 'WebauthnKeyType', id: string, description?: string | null, createdAt?: any | null } | null> | null, keyPair: { __typename?: 'KeyPairType', id?: string | null, encryptedPrivateKey: string, publicKeyString: string }, usergroups?: Array<{ __typename?: 'UserGroupType', groupId: string, userId: string, groupRole: GroupRole } | null> | null, mandator: { __typename?: 'MandatorType', id?: string | null, createdAt?: any | null, subscriptionUserLimit: any, subscriptionStatus: LicenceStatus, isBusinessCustomer: boolean, twoFactorRequired: boolean, customerinformation?: { __typename?: 'StripeCustomerType', id?: string | null, name?: string | null, defaultPaymentMethodId?: string | null, email?: string | null } | null } } | null };

export type GetWebAuthnChallengeMutationVariables = Exact<{ [key: string]: never; }>;


export type GetWebAuthnChallengeMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', getWebauthnChallenge?: { __typename?: 'WebauthnChallengeType', optionsJson: string } | null } | null } | null };

export type GetTotpAuthenticatorKeyMutationVariables = Exact<{ [key: string]: never; }>;


export type GetTotpAuthenticatorKeyMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', getTotpAuthenticatorKey?: { __typename?: 'TotpAuthenticatorKeyType', authenticatorString: string } | null } | null } | null };

export type AddWebautnMutationVariables = Exact<{
  attestationResponse: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
}>;


export type AddWebautnMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', addWebauthnKey?: { __typename?: 'OwnUserType', twoFATypesEnabled?: Array<TwoFaTypes | null> | null } | null } | null } | null };

export type RemoveWebauthnMutationVariables = Exact<{
  webauthnKeyId: Scalars['Guid'];
}>;


export type RemoveWebauthnMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', removeWebauthnKey?: { __typename?: 'OwnUserType', twoFATypesEnabled?: Array<TwoFaTypes | null> | null } | null } | null } | null };

export type EnableTotpMutationVariables = Exact<{
  totp: Scalars['String'];
}>;


export type EnableTotpMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', enableTotp?: { __typename?: 'UserType', twoFATypesEnabled?: Array<TwoFaTypes | null> | null } | null } | null } | null };

export type DisableTwoFaMutationVariables = Exact<{ [key: string]: never; }>;


export type DisableTwoFaMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', twofactor?: { __typename?: 'TwoFactorMutation', disableTotp?: { __typename?: 'UserType', twoFATypesEnabled?: Array<TwoFaTypes | null> | null } | null } | null } | null };

export type AddUserMutationVariables = Exact<{
  user: AddUserInputType;
}>;


export type AddUserMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', add?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type EditUserMutationVariables = Exact<{
  user: EditUserInputType;
}>;


export type EditUserMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', edit?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type GrantAdminPermissionsMutationVariables = Exact<{
  targetUserId: Scalars['Guid'];
  encryptedPasswords: Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>> | InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>;
}>;


export type GrantAdminPermissionsMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', grantAdminPermissions?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type RevokeAdminPermissionsMutationVariables = Exact<{
  targetUserId: Scalars['Guid'];
}>;


export type RevokeAdminPermissionsMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', revokeAdminPermissions?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type ArchiveUserMutationVariables = Exact<{
  targetUserId: Scalars['Guid'];
  reactivate?: InputMaybe<Scalars['Boolean']>;
  encryptedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>> | InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>;
}>;


export type ArchiveUserMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', archive?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type ResendAccountActivationMailMutationVariables = Exact<{
  targetUserId: Scalars['Guid'];
  publicKey: Scalars['String'];
  encryptedPrivateKey: Scalars['String'];
  newTempLoginPassword: Scalars['String'];
  newTempLoginPasswordHashed: Scalars['String'];
  encryptedSharedPasswords?: InputMaybe<Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>> | InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>>;
}>;


export type ResendAccountActivationMailMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', resendAccountActivationEmail?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export type ChangeOwnLoginPasswordMutationVariables = Exact<{
  publicKeyString: Scalars['String'];
  encryptedPrivateKey: Scalars['String'];
  previousPasswordHashEncryptedWithPublicKey: Scalars['String'];
  currentPassword: Scalars['String'];
  newPassword: Scalars['String'];
  encryptedSharedPasswords: Array<InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>> | InputMaybe<AddEncryptedSharedPasswordWithoutPublicKeyInputType>;
  encryptedAccountPasswords: Array<InputMaybe<AddEncryptedAccountPasswordWithoutPublicKeyInputType>> | InputMaybe<AddEncryptedAccountPasswordWithoutPublicKeyInputType>;
}>;


export type ChangeOwnLoginPasswordMutation = { __typename?: 'Vaulteron_root_mutation', user?: { __typename?: 'UserMutation', changeOwnLoginPassword?: { __typename?: 'UserType', id: string, lastname: string, firstname: string, email: string, sex: Sex, admin: boolean, emailConfirmed: boolean, archivedAt?: any | null, passwordReset?: boolean | null } | null } | null };

export const AccountPasswordFieldsFragmentDoc = gql`
    fragment accountPasswordFields on AccountPasswordType {
  id
  name
  login
  securityRating
  websiteURL
  notes
  tags {
    id
    name
    color
  }
  accessLog {
    id
    updatedAt
  }
  updatedAt
  archivedAt
}
    `;
export const DeviceFieldsFragmentDoc = gql`
    fragment deviceFields on DeviceInformationType {
  id
  browser
  browserVersion
  city
  cityLatLong
  countryCode
  countryName
  deviceBrand
  deviceFamily
  deviceModel
  ipAddress
  mail
  os
  osVersion
  createdAt
}
    `;
export const GroupFieldsFragmentDoc = gql`
    fragment groupFields on GroupType {
  id
  parentGroupId
  name
  archivedAt
  countPasswords
  countSubGroups
  countUsers
  countTags
}
    `;
export const MandatorFieldsFragmentDoc = gql`
    fragment mandatorFields on MandatorType {
  id
  updatedAt
  createdAt
  name
  twoFactorRequired
  subscriptionUserLimit
}
    `;
export const SharedPasswordFieldsFragmentDoc = gql`
    fragment sharedPasswordFields on SharedPasswordType {
  id
  name
  login
  securityRating
  websiteURL
  notes
  modifyLock
  isSavedAsOfflinePassword
  tags {
    id
    name
    color
  }
  updatedAt
  archivedAt
  groupId
  accessLog {
    id
    updatedAt
    user {
      firstname
      lastname
    }
  }
}
    `;
export const UserFieldsFragmentDoc = gql`
    fragment userFields on UserType {
  id
  lastname
  firstname
  email
  sex
  admin
  emailConfirmed
  archivedAt
  passwordReset
}
    `;
export const GetAccountPasswordDocument = gql`
    query GetAccountPassword($tagIds: [Guid]! = [], $byArchived: Boolean = false) {
  accountPasswords(tagIds: $tagIds, byArchived: $byArchived) {
    ...accountPasswordFields
  }
}
    ${AccountPasswordFieldsFragmentDoc}`;

/**
 * __useGetAccountPasswordQuery__
 *
 * To run a query within a React component, call `useGetAccountPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAccountPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAccountPasswordQuery({
 *   variables: {
 *      tagIds: // value for 'tagIds'
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetAccountPasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetAccountPasswordQuery, GetAccountPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAccountPasswordQuery, GetAccountPasswordQueryVariables>(GetAccountPasswordDocument, options);
      }
export function useGetAccountPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAccountPasswordQuery, GetAccountPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAccountPasswordQuery, GetAccountPasswordQueryVariables>(GetAccountPasswordDocument, options);
        }
export type GetAccountPasswordQueryHookResult = ReturnType<typeof useGetAccountPasswordQuery>;
export type GetAccountPasswordLazyQueryHookResult = ReturnType<typeof useGetAccountPasswordLazyQuery>;
export type GetAccountPasswordQueryResult = Apollo.QueryResult<GetAccountPasswordQuery, GetAccountPasswordQueryVariables>;
export const GetAccountPasswordWithEncryptedStringDocument = gql`
    query GetAccountPasswordWithEncryptedString {
  accountPasswords {
    ...accountPasswordFields
    encryptedString
  }
}
    ${AccountPasswordFieldsFragmentDoc}`;

/**
 * __useGetAccountPasswordWithEncryptedStringQuery__
 *
 * To run a query within a React component, call `useGetAccountPasswordWithEncryptedStringQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAccountPasswordWithEncryptedStringQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAccountPasswordWithEncryptedStringQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAccountPasswordWithEncryptedStringQuery(baseOptions?: Apollo.QueryHookOptions<GetAccountPasswordWithEncryptedStringQuery, GetAccountPasswordWithEncryptedStringQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAccountPasswordWithEncryptedStringQuery, GetAccountPasswordWithEncryptedStringQueryVariables>(GetAccountPasswordWithEncryptedStringDocument, options);
      }
export function useGetAccountPasswordWithEncryptedStringLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAccountPasswordWithEncryptedStringQuery, GetAccountPasswordWithEncryptedStringQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAccountPasswordWithEncryptedStringQuery, GetAccountPasswordWithEncryptedStringQueryVariables>(GetAccountPasswordWithEncryptedStringDocument, options);
        }
export type GetAccountPasswordWithEncryptedStringQueryHookResult = ReturnType<typeof useGetAccountPasswordWithEncryptedStringQuery>;
export type GetAccountPasswordWithEncryptedStringLazyQueryHookResult = ReturnType<typeof useGetAccountPasswordWithEncryptedStringLazyQuery>;
export type GetAccountPasswordWithEncryptedStringQueryResult = Apollo.QueryResult<GetAccountPasswordWithEncryptedStringQuery, GetAccountPasswordWithEncryptedStringQueryVariables>;
export const AddAccountPasswordDocument = gql`
    mutation AddAccountPassword($accountPassword: AddAccountPasswordInputType!) {
  accountPassword {
    add(password: $accountPassword) {
      ...accountPasswordFields
    }
  }
}
    ${AccountPasswordFieldsFragmentDoc}`;
export type AddAccountPasswordMutationFn = Apollo.MutationFunction<AddAccountPasswordMutation, AddAccountPasswordMutationVariables>;

/**
 * __useAddAccountPasswordMutation__
 *
 * To run a mutation, you first call `useAddAccountPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddAccountPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addAccountPasswordMutation, { data, loading, error }] = useAddAccountPasswordMutation({
 *   variables: {
 *      accountPassword: // value for 'accountPassword'
 *   },
 * });
 */
export function useAddAccountPasswordMutation(baseOptions?: Apollo.MutationHookOptions<AddAccountPasswordMutation, AddAccountPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddAccountPasswordMutation, AddAccountPasswordMutationVariables>(AddAccountPasswordDocument, options);
      }
export type AddAccountPasswordMutationHookResult = ReturnType<typeof useAddAccountPasswordMutation>;
export type AddAccountPasswordMutationResult = Apollo.MutationResult<AddAccountPasswordMutation>;
export type AddAccountPasswordMutationOptions = Apollo.BaseMutationOptions<AddAccountPasswordMutation, AddAccountPasswordMutationVariables>;
export const DeleteAccountPasswordDocument = gql`
    mutation DeleteAccountPassword($id: Guid!) {
  accountPassword {
    delete(id: $id) {
      ...accountPasswordFields
    }
  }
}
    ${AccountPasswordFieldsFragmentDoc}`;
export type DeleteAccountPasswordMutationFn = Apollo.MutationFunction<DeleteAccountPasswordMutation, DeleteAccountPasswordMutationVariables>;

/**
 * __useDeleteAccountPasswordMutation__
 *
 * To run a mutation, you first call `useDeleteAccountPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAccountPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAccountPasswordMutation, { data, loading, error }] = useDeleteAccountPasswordMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteAccountPasswordMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAccountPasswordMutation, DeleteAccountPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAccountPasswordMutation, DeleteAccountPasswordMutationVariables>(DeleteAccountPasswordDocument, options);
      }
export type DeleteAccountPasswordMutationHookResult = ReturnType<typeof useDeleteAccountPasswordMutation>;
export type DeleteAccountPasswordMutationResult = Apollo.MutationResult<DeleteAccountPasswordMutation>;
export type DeleteAccountPasswordMutationOptions = Apollo.BaseMutationOptions<DeleteAccountPasswordMutation, DeleteAccountPasswordMutationVariables>;
export const GetCustomerBillingPortalDocument = gql`
    query GetCustomerBillingPortal {
  customerBillingPortalUrl
}
    `;

/**
 * __useGetCustomerBillingPortalQuery__
 *
 * To run a query within a React component, call `useGetCustomerBillingPortalQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCustomerBillingPortalQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCustomerBillingPortalQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCustomerBillingPortalQuery(baseOptions?: Apollo.QueryHookOptions<GetCustomerBillingPortalQuery, GetCustomerBillingPortalQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCustomerBillingPortalQuery, GetCustomerBillingPortalQueryVariables>(GetCustomerBillingPortalDocument, options);
      }
export function useGetCustomerBillingPortalLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCustomerBillingPortalQuery, GetCustomerBillingPortalQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCustomerBillingPortalQuery, GetCustomerBillingPortalQueryVariables>(GetCustomerBillingPortalDocument, options);
        }
export type GetCustomerBillingPortalQueryHookResult = ReturnType<typeof useGetCustomerBillingPortalQuery>;
export type GetCustomerBillingPortalLazyQueryHookResult = ReturnType<typeof useGetCustomerBillingPortalLazyQuery>;
export type GetCustomerBillingPortalQueryResult = Apollo.QueryResult<GetCustomerBillingPortalQuery, GetCustomerBillingPortalQueryVariables>;
export const GetCustomerCheckoutUrlDocument = gql`
    query GetCustomerCheckoutUrl {
  customerCheckoutUrl
}
    `;

/**
 * __useGetCustomerCheckoutUrlQuery__
 *
 * To run a query within a React component, call `useGetCustomerCheckoutUrlQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCustomerCheckoutUrlQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCustomerCheckoutUrlQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCustomerCheckoutUrlQuery(baseOptions?: Apollo.QueryHookOptions<GetCustomerCheckoutUrlQuery, GetCustomerCheckoutUrlQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCustomerCheckoutUrlQuery, GetCustomerCheckoutUrlQueryVariables>(GetCustomerCheckoutUrlDocument, options);
      }
export function useGetCustomerCheckoutUrlLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCustomerCheckoutUrlQuery, GetCustomerCheckoutUrlQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCustomerCheckoutUrlQuery, GetCustomerCheckoutUrlQueryVariables>(GetCustomerCheckoutUrlDocument, options);
        }
export type GetCustomerCheckoutUrlQueryHookResult = ReturnType<typeof useGetCustomerCheckoutUrlQuery>;
export type GetCustomerCheckoutUrlLazyQueryHookResult = ReturnType<typeof useGetCustomerCheckoutUrlLazyQuery>;
export type GetCustomerCheckoutUrlQueryResult = Apollo.QueryResult<GetCustomerCheckoutUrlQuery, GetCustomerCheckoutUrlQueryVariables>;
export const GetBillingSecretDocument = gql`
    query GetBillingSecret {
  billingSecret
}
    `;

/**
 * __useGetBillingSecretQuery__
 *
 * To run a query within a React component, call `useGetBillingSecretQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetBillingSecretQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetBillingSecretQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetBillingSecretQuery(baseOptions?: Apollo.QueryHookOptions<GetBillingSecretQuery, GetBillingSecretQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetBillingSecretQuery, GetBillingSecretQueryVariables>(GetBillingSecretDocument, options);
      }
export function useGetBillingSecretLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetBillingSecretQuery, GetBillingSecretQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetBillingSecretQuery, GetBillingSecretQueryVariables>(GetBillingSecretDocument, options);
        }
export type GetBillingSecretQueryHookResult = ReturnType<typeof useGetBillingSecretQuery>;
export type GetBillingSecretLazyQueryHookResult = ReturnType<typeof useGetBillingSecretLazyQuery>;
export type GetBillingSecretQueryResult = Apollo.QueryResult<GetBillingSecretQuery, GetBillingSecretQueryVariables>;
export const GetCompanyPasswordDocument = gql`
    query GetCompanyPassword($byArchived: Boolean = false) {
  companyPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    createdBy {
      id
      firstname
      lastname
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetCompanyPasswordQuery__
 *
 * To run a query within a React component, call `useGetCompanyPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompanyPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompanyPasswordQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetCompanyPasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetCompanyPasswordQuery, GetCompanyPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCompanyPasswordQuery, GetCompanyPasswordQueryVariables>(GetCompanyPasswordDocument, options);
      }
export function useGetCompanyPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCompanyPasswordQuery, GetCompanyPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCompanyPasswordQuery, GetCompanyPasswordQueryVariables>(GetCompanyPasswordDocument, options);
        }
export type GetCompanyPasswordQueryHookResult = ReturnType<typeof useGetCompanyPasswordQuery>;
export type GetCompanyPasswordLazyQueryHookResult = ReturnType<typeof useGetCompanyPasswordLazyQuery>;
export type GetCompanyPasswordQueryResult = Apollo.QueryResult<GetCompanyPasswordQuery, GetCompanyPasswordQueryVariables>;
export const AddCompanyPasswordDocument = gql`
    mutation AddCompanyPassword($sharedPassword: AddCompanyPasswordInputType!) {
  sharedPassword {
    companyAdd(password: $sharedPassword) {
      ...sharedPasswordFields
      createdBy {
        id
        firstname
        lastname
      }
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type AddCompanyPasswordMutationFn = Apollo.MutationFunction<AddCompanyPasswordMutation, AddCompanyPasswordMutationVariables>;

/**
 * __useAddCompanyPasswordMutation__
 *
 * To run a mutation, you first call `useAddCompanyPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddCompanyPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addCompanyPasswordMutation, { data, loading, error }] = useAddCompanyPasswordMutation({
 *   variables: {
 *      sharedPassword: // value for 'sharedPassword'
 *   },
 * });
 */
export function useAddCompanyPasswordMutation(baseOptions?: Apollo.MutationHookOptions<AddCompanyPasswordMutation, AddCompanyPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddCompanyPasswordMutation, AddCompanyPasswordMutationVariables>(AddCompanyPasswordDocument, options);
      }
export type AddCompanyPasswordMutationHookResult = ReturnType<typeof useAddCompanyPasswordMutation>;
export type AddCompanyPasswordMutationResult = Apollo.MutationResult<AddCompanyPasswordMutation>;
export type AddCompanyPasswordMutationOptions = Apollo.BaseMutationOptions<AddCompanyPasswordMutation, AddCompanyPasswordMutationVariables>;
export const AddCompanyPasswordForUserDocument = gql`
    mutation AddCompanyPasswordForUser($sharedPassword: AddCompanyPasswordInputType!, $forUserId: Guid!) {
  sharedPassword {
    companyAddForUser(password: $sharedPassword, forUserId: $forUserId) {
      ...sharedPasswordFields
      createdBy {
        id
        firstname
        lastname
      }
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type AddCompanyPasswordForUserMutationFn = Apollo.MutationFunction<AddCompanyPasswordForUserMutation, AddCompanyPasswordForUserMutationVariables>;

/**
 * __useAddCompanyPasswordForUserMutation__
 *
 * To run a mutation, you first call `useAddCompanyPasswordForUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddCompanyPasswordForUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addCompanyPasswordForUserMutation, { data, loading, error }] = useAddCompanyPasswordForUserMutation({
 *   variables: {
 *      sharedPassword: // value for 'sharedPassword'
 *      forUserId: // value for 'forUserId'
 *   },
 * });
 */
export function useAddCompanyPasswordForUserMutation(baseOptions?: Apollo.MutationHookOptions<AddCompanyPasswordForUserMutation, AddCompanyPasswordForUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddCompanyPasswordForUserMutation, AddCompanyPasswordForUserMutationVariables>(AddCompanyPasswordForUserDocument, options);
      }
export type AddCompanyPasswordForUserMutationHookResult = ReturnType<typeof useAddCompanyPasswordForUserMutation>;
export type AddCompanyPasswordForUserMutationResult = Apollo.MutationResult<AddCompanyPasswordForUserMutation>;
export type AddCompanyPasswordForUserMutationOptions = Apollo.BaseMutationOptions<AddCompanyPasswordForUserMutation, AddCompanyPasswordForUserMutationVariables>;
export const GetDeviceInformationDocument = gql`
    query GetDeviceInformation {
  deviceInformation {
    ...deviceFields
  }
}
    ${DeviceFieldsFragmentDoc}`;

/**
 * __useGetDeviceInformationQuery__
 *
 * To run a query within a React component, call `useGetDeviceInformationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDeviceInformationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetDeviceInformationQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetDeviceInformationQuery(baseOptions?: Apollo.QueryHookOptions<GetDeviceInformationQuery, GetDeviceInformationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetDeviceInformationQuery, GetDeviceInformationQueryVariables>(GetDeviceInformationDocument, options);
      }
export function useGetDeviceInformationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetDeviceInformationQuery, GetDeviceInformationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetDeviceInformationQuery, GetDeviceInformationQueryVariables>(GetDeviceInformationDocument, options);
        }
export type GetDeviceInformationQueryHookResult = ReturnType<typeof useGetDeviceInformationQuery>;
export type GetDeviceInformationLazyQueryHookResult = ReturnType<typeof useGetDeviceInformationLazyQuery>;
export type GetDeviceInformationQueryResult = Apollo.QueryResult<GetDeviceInformationQuery, GetDeviceInformationQueryVariables>;
export const SendScamMailDocument = gql`
    mutation SendScamMail($mail: SendScamMailType!) {
  device {
    send(mail: $mail)
  }
}
    `;
export type SendScamMailMutationFn = Apollo.MutationFunction<SendScamMailMutation, SendScamMailMutationVariables>;

/**
 * __useSendScamMailMutation__
 *
 * To run a mutation, you first call `useSendScamMailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendScamMailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendScamMailMutation, { data, loading, error }] = useSendScamMailMutation({
 *   variables: {
 *      mail: // value for 'mail'
 *   },
 * });
 */
export function useSendScamMailMutation(baseOptions?: Apollo.MutationHookOptions<SendScamMailMutation, SendScamMailMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SendScamMailMutation, SendScamMailMutationVariables>(SendScamMailDocument, options);
      }
export type SendScamMailMutationHookResult = ReturnType<typeof useSendScamMailMutation>;
export type SendScamMailMutationResult = Apollo.MutationResult<SendScamMailMutation>;
export type SendScamMailMutationOptions = Apollo.BaseMutationOptions<SendScamMailMutation, SendScamMailMutationVariables>;
export const GetGroupTreeDocument = gql`
    query GetGroupTree($byArchived: Boolean = false) {
  groupTree(byArchived: $byArchived)
}
    `;

/**
 * __useGetGroupTreeQuery__
 *
 * To run a query within a React component, call `useGetGroupTreeQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGroupTreeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGroupTreeQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetGroupTreeQuery(baseOptions?: Apollo.QueryHookOptions<GetGroupTreeQuery, GetGroupTreeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetGroupTreeQuery, GetGroupTreeQueryVariables>(GetGroupTreeDocument, options);
      }
export function useGetGroupTreeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGroupTreeQuery, GetGroupTreeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetGroupTreeQuery, GetGroupTreeQueryVariables>(GetGroupTreeDocument, options);
        }
export type GetGroupTreeQueryHookResult = ReturnType<typeof useGetGroupTreeQuery>;
export type GetGroupTreeLazyQueryHookResult = ReturnType<typeof useGetGroupTreeLazyQuery>;
export type GetGroupTreeQueryResult = Apollo.QueryResult<GetGroupTreeQuery, GetGroupTreeQueryVariables>;
export const GetGroupTreeByRoleDocument = gql`
    query GetGroupTreeByRole($withRole: GroupRole, $byArchived: Boolean = false) {
  groupTree(withRole: $withRole, byArchived: $byArchived)
}
    `;

/**
 * __useGetGroupTreeByRoleQuery__
 *
 * To run a query within a React component, call `useGetGroupTreeByRoleQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGroupTreeByRoleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGroupTreeByRoleQuery({
 *   variables: {
 *      withRole: // value for 'withRole'
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetGroupTreeByRoleQuery(baseOptions?: Apollo.QueryHookOptions<GetGroupTreeByRoleQuery, GetGroupTreeByRoleQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetGroupTreeByRoleQuery, GetGroupTreeByRoleQueryVariables>(GetGroupTreeByRoleDocument, options);
      }
export function useGetGroupTreeByRoleLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGroupTreeByRoleQuery, GetGroupTreeByRoleQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetGroupTreeByRoleQuery, GetGroupTreeByRoleQueryVariables>(GetGroupTreeByRoleDocument, options);
        }
export type GetGroupTreeByRoleQueryHookResult = ReturnType<typeof useGetGroupTreeByRoleQuery>;
export type GetGroupTreeByRoleLazyQueryHookResult = ReturnType<typeof useGetGroupTreeByRoleLazyQuery>;
export type GetGroupTreeByRoleQueryResult = Apollo.QueryResult<GetGroupTreeByRoleQuery, GetGroupTreeByRoleQueryVariables>;
export const GetGroupsByParentDocument = gql`
    query GetGroupsByParent($parentId: Guid, $byArchived: Boolean = false) {
  groups: groupsByParent(parentId: $parentId, byArchived: $byArchived) {
    ...groupFields
  }
}
    ${GroupFieldsFragmentDoc}`;

/**
 * __useGetGroupsByParentQuery__
 *
 * To run a query within a React component, call `useGetGroupsByParentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGroupsByParentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGroupsByParentQuery({
 *   variables: {
 *      parentId: // value for 'parentId'
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetGroupsByParentQuery(baseOptions?: Apollo.QueryHookOptions<GetGroupsByParentQuery, GetGroupsByParentQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetGroupsByParentQuery, GetGroupsByParentQueryVariables>(GetGroupsByParentDocument, options);
      }
export function useGetGroupsByParentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGroupsByParentQuery, GetGroupsByParentQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetGroupsByParentQuery, GetGroupsByParentQueryVariables>(GetGroupsByParentDocument, options);
        }
export type GetGroupsByParentQueryHookResult = ReturnType<typeof useGetGroupsByParentQuery>;
export type GetGroupsByParentLazyQueryHookResult = ReturnType<typeof useGetGroupsByParentLazyQuery>;
export type GetGroupsByParentQueryResult = Apollo.QueryResult<GetGroupsByParentQuery, GetGroupsByParentQueryVariables>;
export const GetAllGroupsDocument = gql`
    query GetAllGroups($byArchived: Boolean = false) {
  groups(byArchived: $byArchived) {
    ...groupFields
  }
}
    ${GroupFieldsFragmentDoc}`;

/**
 * __useGetAllGroupsQuery__
 *
 * To run a query within a React component, call `useGetAllGroupsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllGroupsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllGroupsQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetAllGroupsQuery(baseOptions?: Apollo.QueryHookOptions<GetAllGroupsQuery, GetAllGroupsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAllGroupsQuery, GetAllGroupsQueryVariables>(GetAllGroupsDocument, options);
      }
export function useGetAllGroupsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAllGroupsQuery, GetAllGroupsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAllGroupsQuery, GetAllGroupsQueryVariables>(GetAllGroupsDocument, options);
        }
export type GetAllGroupsQueryHookResult = ReturnType<typeof useGetAllGroupsQuery>;
export type GetAllGroupsLazyQueryHookResult = ReturnType<typeof useGetAllGroupsLazyQuery>;
export type GetAllGroupsQueryResult = Apollo.QueryResult<GetAllGroupsQuery, GetAllGroupsQueryVariables>;
export const AddUserToGroupDocument = gql`
    mutation AddUserToGroup($user: AddUserGroupType!) {
  group {
    addUser(user: $user) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type AddUserToGroupMutationFn = Apollo.MutationFunction<AddUserToGroupMutation, AddUserToGroupMutationVariables>;

/**
 * __useAddUserToGroupMutation__
 *
 * To run a mutation, you first call `useAddUserToGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddUserToGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addUserToGroupMutation, { data, loading, error }] = useAddUserToGroupMutation({
 *   variables: {
 *      user: // value for 'user'
 *   },
 * });
 */
export function useAddUserToGroupMutation(baseOptions?: Apollo.MutationHookOptions<AddUserToGroupMutation, AddUserToGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddUserToGroupMutation, AddUserToGroupMutationVariables>(AddUserToGroupDocument, options);
      }
export type AddUserToGroupMutationHookResult = ReturnType<typeof useAddUserToGroupMutation>;
export type AddUserToGroupMutationResult = Apollo.MutationResult<AddUserToGroupMutation>;
export type AddUserToGroupMutationOptions = Apollo.BaseMutationOptions<AddUserToGroupMutation, AddUserToGroupMutationVariables>;
export const DeleteUserFromGroupDocument = gql`
    mutation DeleteUserFromGroup($groupId: Guid!, $userId: Guid!) {
  group {
    deleteUser(groupId: $groupId, userId: $userId) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type DeleteUserFromGroupMutationFn = Apollo.MutationFunction<DeleteUserFromGroupMutation, DeleteUserFromGroupMutationVariables>;

/**
 * __useDeleteUserFromGroupMutation__
 *
 * To run a mutation, you first call `useDeleteUserFromGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteUserFromGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteUserFromGroupMutation, { data, loading, error }] = useDeleteUserFromGroupMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useDeleteUserFromGroupMutation(baseOptions?: Apollo.MutationHookOptions<DeleteUserFromGroupMutation, DeleteUserFromGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteUserFromGroupMutation, DeleteUserFromGroupMutationVariables>(DeleteUserFromGroupDocument, options);
      }
export type DeleteUserFromGroupMutationHookResult = ReturnType<typeof useDeleteUserFromGroupMutation>;
export type DeleteUserFromGroupMutationResult = Apollo.MutationResult<DeleteUserFromGroupMutation>;
export type DeleteUserFromGroupMutationOptions = Apollo.BaseMutationOptions<DeleteUserFromGroupMutation, DeleteUserFromGroupMutationVariables>;
export const AddGroupDocument = gql`
    mutation AddGroup($group: GroupInputType!) {
  group {
    add(group: $group) {
      ...groupFields
    }
  }
}
    ${GroupFieldsFragmentDoc}`;
export type AddGroupMutationFn = Apollo.MutationFunction<AddGroupMutation, AddGroupMutationVariables>;

/**
 * __useAddGroupMutation__
 *
 * To run a mutation, you first call `useAddGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addGroupMutation, { data, loading, error }] = useAddGroupMutation({
 *   variables: {
 *      group: // value for 'group'
 *   },
 * });
 */
export function useAddGroupMutation(baseOptions?: Apollo.MutationHookOptions<AddGroupMutation, AddGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddGroupMutation, AddGroupMutationVariables>(AddGroupDocument, options);
      }
export type AddGroupMutationHookResult = ReturnType<typeof useAddGroupMutation>;
export type AddGroupMutationResult = Apollo.MutationResult<AddGroupMutation>;
export type AddGroupMutationOptions = Apollo.BaseMutationOptions<AddGroupMutation, AddGroupMutationVariables>;
export const EditGroupDocument = gql`
    mutation EditGroup($group: EditGroupInputType!) {
  group {
    edit(group: $group) {
      ...groupFields
    }
  }
}
    ${GroupFieldsFragmentDoc}`;
export type EditGroupMutationFn = Apollo.MutationFunction<EditGroupMutation, EditGroupMutationVariables>;

/**
 * __useEditGroupMutation__
 *
 * To run a mutation, you first call `useEditGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editGroupMutation, { data, loading, error }] = useEditGroupMutation({
 *   variables: {
 *      group: // value for 'group'
 *   },
 * });
 */
export function useEditGroupMutation(baseOptions?: Apollo.MutationHookOptions<EditGroupMutation, EditGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditGroupMutation, EditGroupMutationVariables>(EditGroupDocument, options);
      }
export type EditGroupMutationHookResult = ReturnType<typeof useEditGroupMutation>;
export type EditGroupMutationResult = Apollo.MutationResult<EditGroupMutation>;
export type EditGroupMutationOptions = Apollo.BaseMutationOptions<EditGroupMutation, EditGroupMutationVariables>;
export const DeleteGroupDocument = gql`
    mutation DeleteGroup($id: Guid!) {
  group {
    delete(groupId: $id) {
      ...groupFields
    }
  }
}
    ${GroupFieldsFragmentDoc}`;
export type DeleteGroupMutationFn = Apollo.MutationFunction<DeleteGroupMutation, DeleteGroupMutationVariables>;

/**
 * __useDeleteGroupMutation__
 *
 * To run a mutation, you first call `useDeleteGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteGroupMutation, { data, loading, error }] = useDeleteGroupMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteGroupMutation(baseOptions?: Apollo.MutationHookOptions<DeleteGroupMutation, DeleteGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteGroupMutation, DeleteGroupMutationVariables>(DeleteGroupDocument, options);
      }
export type DeleteGroupMutationHookResult = ReturnType<typeof useDeleteGroupMutation>;
export type DeleteGroupMutationResult = Apollo.MutationResult<DeleteGroupMutation>;
export type DeleteGroupMutationOptions = Apollo.BaseMutationOptions<DeleteGroupMutation, DeleteGroupMutationVariables>;
export const ArchiveGroupDocument = gql`
    mutation ArchiveGroup($groupId: Guid!, $reactivate: Boolean) {
  group {
    archive(groupId: $groupId, reactivate: $reactivate) {
      ...groupFields
    }
  }
}
    ${GroupFieldsFragmentDoc}`;
export type ArchiveGroupMutationFn = Apollo.MutationFunction<ArchiveGroupMutation, ArchiveGroupMutationVariables>;

/**
 * __useArchiveGroupMutation__
 *
 * To run a mutation, you first call `useArchiveGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useArchiveGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [archiveGroupMutation, { data, loading, error }] = useArchiveGroupMutation({
 *   variables: {
 *      groupId: // value for 'groupId'
 *      reactivate: // value for 'reactivate'
 *   },
 * });
 */
export function useArchiveGroupMutation(baseOptions?: Apollo.MutationHookOptions<ArchiveGroupMutation, ArchiveGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ArchiveGroupMutation, ArchiveGroupMutationVariables>(ArchiveGroupDocument, options);
      }
export type ArchiveGroupMutationHookResult = ReturnType<typeof useArchiveGroupMutation>;
export type ArchiveGroupMutationResult = Apollo.MutationResult<ArchiveGroupMutation>;
export type ArchiveGroupMutationOptions = Apollo.BaseMutationOptions<ArchiveGroupMutation, ArchiveGroupMutationVariables>;
export const MoveGroupDocument = gql`
    mutation MoveGroup($groupToMoveId: Guid!, $newParentGroupId: Guid = null, $encryptedSharedPasswords: [EncryptedSharedPasswordInputType]) {
  group {
    move(
      groupToMoveId: $groupToMoveId
      newParentGroupId: $newParentGroupId
      encryptedSharedPasswords: $encryptedSharedPasswords
    ) {
      ...groupFields
    }
  }
}
    ${GroupFieldsFragmentDoc}`;
export type MoveGroupMutationFn = Apollo.MutationFunction<MoveGroupMutation, MoveGroupMutationVariables>;

/**
 * __useMoveGroupMutation__
 *
 * To run a mutation, you first call `useMoveGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMoveGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moveGroupMutation, { data, loading, error }] = useMoveGroupMutation({
 *   variables: {
 *      groupToMoveId: // value for 'groupToMoveId'
 *      newParentGroupId: // value for 'newParentGroupId'
 *      encryptedSharedPasswords: // value for 'encryptedSharedPasswords'
 *   },
 * });
 */
export function useMoveGroupMutation(baseOptions?: Apollo.MutationHookOptions<MoveGroupMutation, MoveGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MoveGroupMutation, MoveGroupMutationVariables>(MoveGroupDocument, options);
      }
export type MoveGroupMutationHookResult = ReturnType<typeof useMoveGroupMutation>;
export type MoveGroupMutationResult = Apollo.MutationResult<MoveGroupMutation>;
export type MoveGroupMutationOptions = Apollo.BaseMutationOptions<MoveGroupMutation, MoveGroupMutationVariables>;
export const GetAllGroupPasswordDocument = gql`
    query GetAllGroupPassword($byArchived: Boolean = false) {
  sharedPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    groupId
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetAllGroupPasswordQuery__
 *
 * To run a query within a React component, call `useGetAllGroupPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllGroupPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllGroupPasswordQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetAllGroupPasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetAllGroupPasswordQuery, GetAllGroupPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAllGroupPasswordQuery, GetAllGroupPasswordQueryVariables>(GetAllGroupPasswordDocument, options);
      }
export function useGetAllGroupPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAllGroupPasswordQuery, GetAllGroupPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAllGroupPasswordQuery, GetAllGroupPasswordQueryVariables>(GetAllGroupPasswordDocument, options);
        }
export type GetAllGroupPasswordQueryHookResult = ReturnType<typeof useGetAllGroupPasswordQuery>;
export type GetAllGroupPasswordLazyQueryHookResult = ReturnType<typeof useGetAllGroupPasswordLazyQuery>;
export type GetAllGroupPasswordQueryResult = Apollo.QueryResult<GetAllGroupPasswordQuery, GetAllGroupPasswordQueryVariables>;
export const GetGroupPasswordDocument = gql`
    query GetGroupPassword($groupIds: [Guid]!, $byArchived: Boolean = false) {
  sharedPasswords(groupIds: $groupIds, byArchived: $byArchived) {
    ...sharedPasswordFields
    groupId
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetGroupPasswordQuery__
 *
 * To run a query within a React component, call `useGetGroupPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetGroupPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetGroupPasswordQuery({
 *   variables: {
 *      groupIds: // value for 'groupIds'
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetGroupPasswordQuery(baseOptions: Apollo.QueryHookOptions<GetGroupPasswordQuery, GetGroupPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetGroupPasswordQuery, GetGroupPasswordQueryVariables>(GetGroupPasswordDocument, options);
      }
export function useGetGroupPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetGroupPasswordQuery, GetGroupPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetGroupPasswordQuery, GetGroupPasswordQueryVariables>(GetGroupPasswordDocument, options);
        }
export type GetGroupPasswordQueryHookResult = ReturnType<typeof useGetGroupPasswordQuery>;
export type GetGroupPasswordLazyQueryHookResult = ReturnType<typeof useGetGroupPasswordLazyQuery>;
export type GetGroupPasswordQueryResult = Apollo.QueryResult<GetGroupPasswordQuery, GetGroupPasswordQueryVariables>;
export const AddGroupPasswordDocument = gql`
    mutation AddGroupPassword($sharedPassword: AddSharedPasswordInputType!) {
  sharedPassword {
    add(password: $sharedPassword) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type AddGroupPasswordMutationFn = Apollo.MutationFunction<AddGroupPasswordMutation, AddGroupPasswordMutationVariables>;

/**
 * __useAddGroupPasswordMutation__
 *
 * To run a mutation, you first call `useAddGroupPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddGroupPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addGroupPasswordMutation, { data, loading, error }] = useAddGroupPasswordMutation({
 *   variables: {
 *      sharedPassword: // value for 'sharedPassword'
 *   },
 * });
 */
export function useAddGroupPasswordMutation(baseOptions?: Apollo.MutationHookOptions<AddGroupPasswordMutation, AddGroupPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddGroupPasswordMutation, AddGroupPasswordMutationVariables>(AddGroupPasswordDocument, options);
      }
export type AddGroupPasswordMutationHookResult = ReturnType<typeof useAddGroupPasswordMutation>;
export type AddGroupPasswordMutationResult = Apollo.MutationResult<AddGroupPasswordMutation>;
export type AddGroupPasswordMutationOptions = Apollo.BaseMutationOptions<AddGroupPasswordMutation, AddGroupPasswordMutationVariables>;
export const DeleteGroupPasswordDocument = gql`
    mutation DeleteGroupPassword($id: Guid!) {
  sharedPassword {
    delete(id: $id) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type DeleteGroupPasswordMutationFn = Apollo.MutationFunction<DeleteGroupPasswordMutation, DeleteGroupPasswordMutationVariables>;

/**
 * __useDeleteGroupPasswordMutation__
 *
 * To run a mutation, you first call `useDeleteGroupPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteGroupPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteGroupPasswordMutation, { data, loading, error }] = useDeleteGroupPasswordMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteGroupPasswordMutation(baseOptions?: Apollo.MutationHookOptions<DeleteGroupPasswordMutation, DeleteGroupPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteGroupPasswordMutation, DeleteGroupPasswordMutationVariables>(DeleteGroupPasswordDocument, options);
      }
export type DeleteGroupPasswordMutationHookResult = ReturnType<typeof useDeleteGroupPasswordMutation>;
export type DeleteGroupPasswordMutationResult = Apollo.MutationResult<DeleteGroupPasswordMutation>;
export type DeleteGroupPasswordMutationOptions = Apollo.BaseMutationOptions<DeleteGroupPasswordMutation, DeleteGroupPasswordMutationVariables>;
export const GetMandatorsDocument = gql`
    query GetMandators {
  mandators {
    ...mandatorFields
  }
}
    ${MandatorFieldsFragmentDoc}`;

/**
 * __useGetMandatorsQuery__
 *
 * To run a query within a React component, call `useGetMandatorsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMandatorsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMandatorsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetMandatorsQuery(baseOptions?: Apollo.QueryHookOptions<GetMandatorsQuery, GetMandatorsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMandatorsQuery, GetMandatorsQueryVariables>(GetMandatorsDocument, options);
      }
export function useGetMandatorsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMandatorsQuery, GetMandatorsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMandatorsQuery, GetMandatorsQueryVariables>(GetMandatorsDocument, options);
        }
export type GetMandatorsQueryHookResult = ReturnType<typeof useGetMandatorsQuery>;
export type GetMandatorsLazyQueryHookResult = ReturnType<typeof useGetMandatorsLazyQuery>;
export type GetMandatorsQueryResult = Apollo.QueryResult<GetMandatorsQuery, GetMandatorsQueryVariables>;
export const EditMandatorDocument = gql`
    mutation EditMandator($twoFactorRequired: Boolean!) {
  mandator {
    edit(twoFactorRequired: $twoFactorRequired) {
      ...mandatorFields
    }
  }
}
    ${MandatorFieldsFragmentDoc}`;
export type EditMandatorMutationFn = Apollo.MutationFunction<EditMandatorMutation, EditMandatorMutationVariables>;

/**
 * __useEditMandatorMutation__
 *
 * To run a mutation, you first call `useEditMandatorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditMandatorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editMandatorMutation, { data, loading, error }] = useEditMandatorMutation({
 *   variables: {
 *      twoFactorRequired: // value for 'twoFactorRequired'
 *   },
 * });
 */
export function useEditMandatorMutation(baseOptions?: Apollo.MutationHookOptions<EditMandatorMutation, EditMandatorMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditMandatorMutation, EditMandatorMutationVariables>(EditMandatorDocument, options);
      }
export type EditMandatorMutationHookResult = ReturnType<typeof useEditMandatorMutation>;
export type EditMandatorMutationResult = Apollo.MutationResult<EditMandatorMutation>;
export type EditMandatorMutationOptions = Apollo.BaseMutationOptions<EditMandatorMutation, EditMandatorMutationVariables>;
export const DeleteMandatorDocument = gql`
    mutation DeleteMandator($id: Guid!) {
  mandator {
    delete(mandatorId: $id) {
      ...mandatorFields
    }
  }
}
    ${MandatorFieldsFragmentDoc}`;
export type DeleteMandatorMutationFn = Apollo.MutationFunction<DeleteMandatorMutation, DeleteMandatorMutationVariables>;

/**
 * __useDeleteMandatorMutation__
 *
 * To run a mutation, you first call `useDeleteMandatorMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMandatorMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMandatorMutation, { data, loading, error }] = useDeleteMandatorMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMandatorMutation(baseOptions?: Apollo.MutationHookOptions<DeleteMandatorMutation, DeleteMandatorMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteMandatorMutation, DeleteMandatorMutationVariables>(DeleteMandatorDocument, options);
      }
export type DeleteMandatorMutationHookResult = ReturnType<typeof useDeleteMandatorMutation>;
export type DeleteMandatorMutationResult = Apollo.MutationResult<DeleteMandatorMutation>;
export type DeleteMandatorMutationOptions = Apollo.BaseMutationOptions<DeleteMandatorMutation, DeleteMandatorMutationVariables>;
export const GetOfflinePasswordDocument = gql`
    query GetOfflinePassword {
  offlinePasswords {
    ...sharedPasswordFields
    createdBy {
      id
      firstname
      lastname
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetOfflinePasswordQuery__
 *
 * To run a query within a React component, call `useGetOfflinePasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOfflinePasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOfflinePasswordQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetOfflinePasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetOfflinePasswordQuery, GetOfflinePasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOfflinePasswordQuery, GetOfflinePasswordQueryVariables>(GetOfflinePasswordDocument, options);
      }
export function useGetOfflinePasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOfflinePasswordQuery, GetOfflinePasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOfflinePasswordQuery, GetOfflinePasswordQueryVariables>(GetOfflinePasswordDocument, options);
        }
export type GetOfflinePasswordQueryHookResult = ReturnType<typeof useGetOfflinePasswordQuery>;
export type GetOfflinePasswordLazyQueryHookResult = ReturnType<typeof useGetOfflinePasswordLazyQuery>;
export type GetOfflinePasswordQueryResult = Apollo.QueryResult<GetOfflinePasswordQuery, GetOfflinePasswordQueryVariables>;
export const GetOfflinePasswordWithEncryptedStringDocument = gql`
    query GetOfflinePasswordWithEncryptedString($searchTerm: String!, $createdByIds: [Guid]) {
  offlinePasswords(searchTerm: $searchTerm, createdByIds: $createdByIds) {
    encryptedString
    ...sharedPasswordFields
    createdBy {
      id
      firstname
      lastname
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetOfflinePasswordWithEncryptedStringQuery__
 *
 * To run a query within a React component, call `useGetOfflinePasswordWithEncryptedStringQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetOfflinePasswordWithEncryptedStringQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetOfflinePasswordWithEncryptedStringQuery({
 *   variables: {
 *      searchTerm: // value for 'searchTerm'
 *      createdByIds: // value for 'createdByIds'
 *   },
 * });
 */
export function useGetOfflinePasswordWithEncryptedStringQuery(baseOptions: Apollo.QueryHookOptions<GetOfflinePasswordWithEncryptedStringQuery, GetOfflinePasswordWithEncryptedStringQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetOfflinePasswordWithEncryptedStringQuery, GetOfflinePasswordWithEncryptedStringQueryVariables>(GetOfflinePasswordWithEncryptedStringDocument, options);
      }
export function useGetOfflinePasswordWithEncryptedStringLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetOfflinePasswordWithEncryptedStringQuery, GetOfflinePasswordWithEncryptedStringQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetOfflinePasswordWithEncryptedStringQuery, GetOfflinePasswordWithEncryptedStringQueryVariables>(GetOfflinePasswordWithEncryptedStringDocument, options);
        }
export type GetOfflinePasswordWithEncryptedStringQueryHookResult = ReturnType<typeof useGetOfflinePasswordWithEncryptedStringQuery>;
export type GetOfflinePasswordWithEncryptedStringLazyQueryHookResult = ReturnType<typeof useGetOfflinePasswordWithEncryptedStringLazyQuery>;
export type GetOfflinePasswordWithEncryptedStringQueryResult = Apollo.QueryResult<GetOfflinePasswordWithEncryptedStringQuery, GetOfflinePasswordWithEncryptedStringQueryVariables>;
export const AddOfflinePasswordDocument = gql`
    mutation AddOfflinePassword($offlinePassword: AddOfflinePasswordInputType!) {
  sharedPassword {
    offlineAdd(password: $offlinePassword) {
      ...sharedPasswordFields
      createdBy {
        id
        firstname
        lastname
      }
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type AddOfflinePasswordMutationFn = Apollo.MutationFunction<AddOfflinePasswordMutation, AddOfflinePasswordMutationVariables>;

/**
 * __useAddOfflinePasswordMutation__
 *
 * To run a mutation, you first call `useAddOfflinePasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddOfflinePasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addOfflinePasswordMutation, { data, loading, error }] = useAddOfflinePasswordMutation({
 *   variables: {
 *      offlinePassword: // value for 'offlinePassword'
 *   },
 * });
 */
export function useAddOfflinePasswordMutation(baseOptions?: Apollo.MutationHookOptions<AddOfflinePasswordMutation, AddOfflinePasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddOfflinePasswordMutation, AddOfflinePasswordMutationVariables>(AddOfflinePasswordDocument, options);
      }
export type AddOfflinePasswordMutationHookResult = ReturnType<typeof useAddOfflinePasswordMutation>;
export type AddOfflinePasswordMutationResult = Apollo.MutationResult<AddOfflinePasswordMutation>;
export type AddOfflinePasswordMutationOptions = Apollo.BaseMutationOptions<AddOfflinePasswordMutation, AddOfflinePasswordMutationVariables>;
export const DeleteOfflinePasswordDocument = gql`
    mutation DeleteOfflinePassword($id: Guid!) {
  sharedPassword {
    delete(id: $id) {
      ...sharedPasswordFields
      createdBy {
        id
        firstname
        lastname
      }
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type DeleteOfflinePasswordMutationFn = Apollo.MutationFunction<DeleteOfflinePasswordMutation, DeleteOfflinePasswordMutationVariables>;

/**
 * __useDeleteOfflinePasswordMutation__
 *
 * To run a mutation, you first call `useDeleteOfflinePasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteOfflinePasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteOfflinePasswordMutation, { data, loading, error }] = useDeleteOfflinePasswordMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteOfflinePasswordMutation(baseOptions?: Apollo.MutationHookOptions<DeleteOfflinePasswordMutation, DeleteOfflinePasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteOfflinePasswordMutation, DeleteOfflinePasswordMutationVariables>(DeleteOfflinePasswordDocument, options);
      }
export type DeleteOfflinePasswordMutationHookResult = ReturnType<typeof useDeleteOfflinePasswordMutation>;
export type DeleteOfflinePasswordMutationResult = Apollo.MutationResult<DeleteOfflinePasswordMutation>;
export type DeleteOfflinePasswordMutationOptions = Apollo.BaseMutationOptions<DeleteOfflinePasswordMutation, DeleteOfflinePasswordMutationVariables>;
export const GetSyncedPasswordDocument = gql`
    query GetSyncedPassword {
  companyPasswords {
    ...sharedPasswordFields
  }
  sharedPasswords {
    ...sharedPasswordFields
    groupId
  }
  offlinePasswords {
    ...sharedPasswordFields
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetSyncedPasswordQuery__
 *
 * To run a query within a React component, call `useGetSyncedPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSyncedPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSyncedPasswordQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetSyncedPasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetSyncedPasswordQuery, GetSyncedPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSyncedPasswordQuery, GetSyncedPasswordQueryVariables>(GetSyncedPasswordDocument, options);
      }
export function useGetSyncedPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSyncedPasswordQuery, GetSyncedPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSyncedPasswordQuery, GetSyncedPasswordQueryVariables>(GetSyncedPasswordDocument, options);
        }
export type GetSyncedPasswordQueryHookResult = ReturnType<typeof useGetSyncedPasswordQuery>;
export type GetSyncedPasswordLazyQueryHookResult = ReturnType<typeof useGetSyncedPasswordLazyQuery>;
export type GetSyncedPasswordQueryResult = Apollo.QueryResult<GetSyncedPasswordQuery, GetSyncedPasswordQueryVariables>;
export const GetSyncedPasswordWithEncryptedStringDocument = gql`
    query GetSyncedPasswordWithEncryptedString($byArchived: Boolean = false) {
  companyPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    encryptedString
  }
  sharedPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    groupId
    encryptedString
  }
  offlinePasswords {
    ...sharedPasswordFields
    encryptedString
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;

/**
 * __useGetSyncedPasswordWithEncryptedStringQuery__
 *
 * To run a query within a React component, call `useGetSyncedPasswordWithEncryptedStringQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSyncedPasswordWithEncryptedStringQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetSyncedPasswordWithEncryptedStringQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetSyncedPasswordWithEncryptedStringQuery(baseOptions?: Apollo.QueryHookOptions<GetSyncedPasswordWithEncryptedStringQuery, GetSyncedPasswordWithEncryptedStringQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetSyncedPasswordWithEncryptedStringQuery, GetSyncedPasswordWithEncryptedStringQueryVariables>(GetSyncedPasswordWithEncryptedStringDocument, options);
      }
export function useGetSyncedPasswordWithEncryptedStringLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetSyncedPasswordWithEncryptedStringQuery, GetSyncedPasswordWithEncryptedStringQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetSyncedPasswordWithEncryptedStringQuery, GetSyncedPasswordWithEncryptedStringQueryVariables>(GetSyncedPasswordWithEncryptedStringDocument, options);
        }
export type GetSyncedPasswordWithEncryptedStringQueryHookResult = ReturnType<typeof useGetSyncedPasswordWithEncryptedStringQuery>;
export type GetSyncedPasswordWithEncryptedStringLazyQueryHookResult = ReturnType<typeof useGetSyncedPasswordWithEncryptedStringLazyQuery>;
export type GetSyncedPasswordWithEncryptedStringQueryResult = Apollo.QueryResult<GetSyncedPasswordWithEncryptedStringQuery, GetSyncedPasswordWithEncryptedStringQueryVariables>;
export const GetAllPasswordDocument = gql`
    query GetAllPassword($byArchived: Boolean = false) {
  companyPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    createdBy {
      id
    }
  }
  sharedPasswords(byArchived: $byArchived) {
    ...sharedPasswordFields
    groupId
  }
  accountPasswords(byArchived: $byArchived) {
    ...accountPasswordFields
  }
}
    ${SharedPasswordFieldsFragmentDoc}
${AccountPasswordFieldsFragmentDoc}`;

/**
 * __useGetAllPasswordQuery__
 *
 * To run a query within a React component, call `useGetAllPasswordQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllPasswordQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllPasswordQuery({
 *   variables: {
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetAllPasswordQuery(baseOptions?: Apollo.QueryHookOptions<GetAllPasswordQuery, GetAllPasswordQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAllPasswordQuery, GetAllPasswordQueryVariables>(GetAllPasswordDocument, options);
      }
export function useGetAllPasswordLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAllPasswordQuery, GetAllPasswordQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAllPasswordQuery, GetAllPasswordQueryVariables>(GetAllPasswordDocument, options);
        }
export type GetAllPasswordQueryHookResult = ReturnType<typeof useGetAllPasswordQuery>;
export type GetAllPasswordLazyQueryHookResult = ReturnType<typeof useGetAllPasswordLazyQuery>;
export type GetAllPasswordQueryResult = Apollo.QueryResult<GetAllPasswordQuery, GetAllPasswordQueryVariables>;
export const DeleteSharedPasswordDocument = gql`
    mutation DeleteSharedPassword($id: Guid!) {
  sharedPassword {
    delete(id: $id) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type DeleteSharedPasswordMutationFn = Apollo.MutationFunction<DeleteSharedPasswordMutation, DeleteSharedPasswordMutationVariables>;

/**
 * __useDeleteSharedPasswordMutation__
 *
 * To run a mutation, you first call `useDeleteSharedPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSharedPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSharedPasswordMutation, { data, loading, error }] = useDeleteSharedPasswordMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteSharedPasswordMutation(baseOptions?: Apollo.MutationHookOptions<DeleteSharedPasswordMutation, DeleteSharedPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteSharedPasswordMutation, DeleteSharedPasswordMutationVariables>(DeleteSharedPasswordDocument, options);
      }
export type DeleteSharedPasswordMutationHookResult = ReturnType<typeof useDeleteSharedPasswordMutation>;
export type DeleteSharedPasswordMutationResult = Apollo.MutationResult<DeleteSharedPasswordMutation>;
export type DeleteSharedPasswordMutationOptions = Apollo.BaseMutationOptions<DeleteSharedPasswordMutation, DeleteSharedPasswordMutationVariables>;
export const MoveCompanyPasswordToSharedPasswordDocument = gql`
    mutation MoveCompanyPasswordToSharedPassword($passwordsToMoveIds: [Guid]!, $destinationGroupId: Guid!, $encryptedPasswords: [EncryptedSharedPasswordInputType]) {
  sharedPassword {
    companyMoveToShared(
      passwordsToMoveIds: $passwordsToMoveIds
      destinationGroupId: $destinationGroupId
      encryptedPasswords: $encryptedPasswords
    ) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type MoveCompanyPasswordToSharedPasswordMutationFn = Apollo.MutationFunction<MoveCompanyPasswordToSharedPasswordMutation, MoveCompanyPasswordToSharedPasswordMutationVariables>;

/**
 * __useMoveCompanyPasswordToSharedPasswordMutation__
 *
 * To run a mutation, you first call `useMoveCompanyPasswordToSharedPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMoveCompanyPasswordToSharedPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moveCompanyPasswordToSharedPasswordMutation, { data, loading, error }] = useMoveCompanyPasswordToSharedPasswordMutation({
 *   variables: {
 *      passwordsToMoveIds: // value for 'passwordsToMoveIds'
 *      destinationGroupId: // value for 'destinationGroupId'
 *      encryptedPasswords: // value for 'encryptedPasswords'
 *   },
 * });
 */
export function useMoveCompanyPasswordToSharedPasswordMutation(baseOptions?: Apollo.MutationHookOptions<MoveCompanyPasswordToSharedPasswordMutation, MoveCompanyPasswordToSharedPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MoveCompanyPasswordToSharedPasswordMutation, MoveCompanyPasswordToSharedPasswordMutationVariables>(MoveCompanyPasswordToSharedPasswordDocument, options);
      }
export type MoveCompanyPasswordToSharedPasswordMutationHookResult = ReturnType<typeof useMoveCompanyPasswordToSharedPasswordMutation>;
export type MoveCompanyPasswordToSharedPasswordMutationResult = Apollo.MutationResult<MoveCompanyPasswordToSharedPasswordMutation>;
export type MoveCompanyPasswordToSharedPasswordMutationOptions = Apollo.BaseMutationOptions<MoveCompanyPasswordToSharedPasswordMutation, MoveCompanyPasswordToSharedPasswordMutationVariables>;
export const MoveSharedPasswordToCompanyPasswordDocument = gql`
    mutation MoveSharedPasswordToCompanyPassword($passwordsToMoveIds: [Guid]!, $destinationUserId: Guid!, $encryptedPasswords: [EncryptedSharedPasswordInputType]) {
  sharedPassword {
    sharedMoveToCompany(
      passwordsToMoveIds: $passwordsToMoveIds
      destinationUserId: $destinationUserId
      encryptedPasswords: $encryptedPasswords
    ) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type MoveSharedPasswordToCompanyPasswordMutationFn = Apollo.MutationFunction<MoveSharedPasswordToCompanyPasswordMutation, MoveSharedPasswordToCompanyPasswordMutationVariables>;

/**
 * __useMoveSharedPasswordToCompanyPasswordMutation__
 *
 * To run a mutation, you first call `useMoveSharedPasswordToCompanyPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMoveSharedPasswordToCompanyPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moveSharedPasswordToCompanyPasswordMutation, { data, loading, error }] = useMoveSharedPasswordToCompanyPasswordMutation({
 *   variables: {
 *      passwordsToMoveIds: // value for 'passwordsToMoveIds'
 *      destinationUserId: // value for 'destinationUserId'
 *      encryptedPasswords: // value for 'encryptedPasswords'
 *   },
 * });
 */
export function useMoveSharedPasswordToCompanyPasswordMutation(baseOptions?: Apollo.MutationHookOptions<MoveSharedPasswordToCompanyPasswordMutation, MoveSharedPasswordToCompanyPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MoveSharedPasswordToCompanyPasswordMutation, MoveSharedPasswordToCompanyPasswordMutationVariables>(MoveSharedPasswordToCompanyPasswordDocument, options);
      }
export type MoveSharedPasswordToCompanyPasswordMutationHookResult = ReturnType<typeof useMoveSharedPasswordToCompanyPasswordMutation>;
export type MoveSharedPasswordToCompanyPasswordMutationResult = Apollo.MutationResult<MoveSharedPasswordToCompanyPasswordMutation>;
export type MoveSharedPasswordToCompanyPasswordMutationOptions = Apollo.BaseMutationOptions<MoveSharedPasswordToCompanyPasswordMutation, MoveSharedPasswordToCompanyPasswordMutationVariables>;
export const MoveSharedPasswordsToGroupDocument = gql`
    mutation MoveSharedPasswordsToGroup($input: MoveSharedPasswordToGroupInputType!) {
  sharedPassword {
    moveToGroup(input: $input) {
      ...sharedPasswordFields
    }
  }
}
    ${SharedPasswordFieldsFragmentDoc}`;
export type MoveSharedPasswordsToGroupMutationFn = Apollo.MutationFunction<MoveSharedPasswordsToGroupMutation, MoveSharedPasswordsToGroupMutationVariables>;

/**
 * __useMoveSharedPasswordsToGroupMutation__
 *
 * To run a mutation, you first call `useMoveSharedPasswordsToGroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMoveSharedPasswordsToGroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [moveSharedPasswordsToGroupMutation, { data, loading, error }] = useMoveSharedPasswordsToGroupMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useMoveSharedPasswordsToGroupMutation(baseOptions?: Apollo.MutationHookOptions<MoveSharedPasswordsToGroupMutation, MoveSharedPasswordsToGroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MoveSharedPasswordsToGroupMutation, MoveSharedPasswordsToGroupMutationVariables>(MoveSharedPasswordsToGroupDocument, options);
      }
export type MoveSharedPasswordsToGroupMutationHookResult = ReturnType<typeof useMoveSharedPasswordsToGroupMutation>;
export type MoveSharedPasswordsToGroupMutationResult = Apollo.MutationResult<MoveSharedPasswordsToGroupMutation>;
export type MoveSharedPasswordsToGroupMutationOptions = Apollo.BaseMutationOptions<MoveSharedPasswordsToGroupMutation, MoveSharedPasswordsToGroupMutationVariables>;
export const GetPasswordHistoryDocument = gql`
    query GetPasswordHistory($entityId: Guid!, $from: DateTime, $to: DateTime, $propertyName: String) {
  changeLogs(
    entityId: $entityId
    from: $from
    to: $to
    propertyName: $propertyName
  ) {
    id
    entityName
    propertyName
    createdAt
    createdById
    createdBy {
      firstname
      lastname
    }
    oldValue {
      entityId
      name
    }
    newValue {
      entityId
      name
    }
  }
}
    `;

/**
 * __useGetPasswordHistoryQuery__
 *
 * To run a query within a React component, call `useGetPasswordHistoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPasswordHistoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPasswordHistoryQuery({
 *   variables: {
 *      entityId: // value for 'entityId'
 *      from: // value for 'from'
 *      to: // value for 'to'
 *      propertyName: // value for 'propertyName'
 *   },
 * });
 */
export function useGetPasswordHistoryQuery(baseOptions: Apollo.QueryHookOptions<GetPasswordHistoryQuery, GetPasswordHistoryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPasswordHistoryQuery, GetPasswordHistoryQueryVariables>(GetPasswordHistoryDocument, options);
      }
export function useGetPasswordHistoryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPasswordHistoryQuery, GetPasswordHistoryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPasswordHistoryQuery, GetPasswordHistoryQueryVariables>(GetPasswordHistoryDocument, options);
        }
export type GetPasswordHistoryQueryHookResult = ReturnType<typeof useGetPasswordHistoryQuery>;
export type GetPasswordHistoryLazyQueryHookResult = ReturnType<typeof useGetPasswordHistoryLazyQuery>;
export type GetPasswordHistoryQueryResult = Apollo.QueryResult<GetPasswordHistoryQuery, GetPasswordHistoryQueryVariables>;
export const GetPasswordChangeLogsDocument = gql`
    query GetPasswordChangeLogs($passwordId: Guid!) {
  passwordChangeLogs(passwordId: $passwordId) {
    id
    keyPairId
    createdAt
    encryptedPasswordString
    createdById
    createdBy {
      firstname
      lastname
    }
  }
}
    `;

/**
 * __useGetPasswordChangeLogsQuery__
 *
 * To run a query within a React component, call `useGetPasswordChangeLogsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPasswordChangeLogsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPasswordChangeLogsQuery({
 *   variables: {
 *      passwordId: // value for 'passwordId'
 *   },
 * });
 */
export function useGetPasswordChangeLogsQuery(baseOptions: Apollo.QueryHookOptions<GetPasswordChangeLogsQuery, GetPasswordChangeLogsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPasswordChangeLogsQuery, GetPasswordChangeLogsQueryVariables>(GetPasswordChangeLogsDocument, options);
      }
export function useGetPasswordChangeLogsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPasswordChangeLogsQuery, GetPasswordChangeLogsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPasswordChangeLogsQuery, GetPasswordChangeLogsQueryVariables>(GetPasswordChangeLogsDocument, options);
        }
export type GetPasswordChangeLogsQueryHookResult = ReturnType<typeof useGetPasswordChangeLogsQuery>;
export type GetPasswordChangeLogsLazyQueryHookResult = ReturnType<typeof useGetPasswordChangeLogsLazyQuery>;
export type GetPasswordChangeLogsQueryResult = Apollo.QueryResult<GetPasswordChangeLogsQuery, GetPasswordChangeLogsQueryVariables>;
export const GetKeyPairHistoryDocument = gql`
    query GetKeyPairHistory {
  keyPairChangeLogs {
    id
    createdAt
    encryptedPrivateKey
    previousPasswordHashEncryptedWithPublicKey
  }
}
    `;

/**
 * __useGetKeyPairHistoryQuery__
 *
 * To run a query within a React component, call `useGetKeyPairHistoryQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetKeyPairHistoryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetKeyPairHistoryQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetKeyPairHistoryQuery(baseOptions?: Apollo.QueryHookOptions<GetKeyPairHistoryQuery, GetKeyPairHistoryQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetKeyPairHistoryQuery, GetKeyPairHistoryQueryVariables>(GetKeyPairHistoryDocument, options);
      }
export function useGetKeyPairHistoryLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetKeyPairHistoryQuery, GetKeyPairHistoryQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetKeyPairHistoryQuery, GetKeyPairHistoryQueryVariables>(GetKeyPairHistoryDocument, options);
        }
export type GetKeyPairHistoryQueryHookResult = ReturnType<typeof useGetKeyPairHistoryQuery>;
export type GetKeyPairHistoryLazyQueryHookResult = ReturnType<typeof useGetKeyPairHistoryLazyQuery>;
export type GetKeyPairHistoryQueryResult = Apollo.QueryResult<GetKeyPairHistoryQuery, GetKeyPairHistoryQueryVariables>;
export const GetPwnedMailDocument = gql`
    query GetPwnedMail($mail: String!) {
  pwned(mail: $mail)
}
    `;

/**
 * __useGetPwnedMailQuery__
 *
 * To run a query within a React component, call `useGetPwnedMailQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPwnedMailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPwnedMailQuery({
 *   variables: {
 *      mail: // value for 'mail'
 *   },
 * });
 */
export function useGetPwnedMailQuery(baseOptions: Apollo.QueryHookOptions<GetPwnedMailQuery, GetPwnedMailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPwnedMailQuery, GetPwnedMailQueryVariables>(GetPwnedMailDocument, options);
      }
export function useGetPwnedMailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPwnedMailQuery, GetPwnedMailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPwnedMailQuery, GetPwnedMailQueryVariables>(GetPwnedMailDocument, options);
        }
export type GetPwnedMailQueryHookResult = ReturnType<typeof useGetPwnedMailQuery>;
export type GetPwnedMailLazyQueryHookResult = ReturnType<typeof useGetPwnedMailLazyQuery>;
export type GetPwnedMailQueryResult = Apollo.QueryResult<GetPwnedMailQuery, GetPwnedMailQueryVariables>;
export const GetUsersDocument = gql`
    query GetUsers {
  users {
    ...userFields
  }
}
    ${UserFieldsFragmentDoc}`;

/**
 * __useGetUsersQuery__
 *
 * To run a query within a React component, call `useGetUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetUsersQuery(baseOptions?: Apollo.QueryHookOptions<GetUsersQuery, GetUsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUsersQuery, GetUsersQueryVariables>(GetUsersDocument, options);
      }
export function useGetUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUsersQuery, GetUsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUsersQuery, GetUsersQueryVariables>(GetUsersDocument, options);
        }
export type GetUsersQueryHookResult = ReturnType<typeof useGetUsersQuery>;
export type GetUsersLazyQueryHookResult = ReturnType<typeof useGetUsersLazyQuery>;
export type GetUsersQueryResult = Apollo.QueryResult<GetUsersQuery, GetUsersQueryVariables>;
export const GetUsersInGroupDocument = gql`
    query GetUsersInGroup($groupIds: [Guid]!, $byArchived: Boolean = false) {
  users(groupIds: $groupIds, byArchived: $byArchived) {
    id
    lastname
    firstname
    email
    admin
    usergroups {
      group {
        id
        name
      }
      groupRole
    }
  }
}
    `;

/**
 * __useGetUsersInGroupQuery__
 *
 * To run a query within a React component, call `useGetUsersInGroupQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersInGroupQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUsersInGroupQuery({
 *   variables: {
 *      groupIds: // value for 'groupIds'
 *      byArchived: // value for 'byArchived'
 *   },
 * });
 */
export function useGetUsersInGroupQuery(baseOptions: Apollo.QueryHookOptions<GetUsersInGroupQuery, GetUsersInGroupQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUsersInGroupQuery, GetUsersInGroupQueryVariables>(GetUsersInGroupDocument, options);
      }
export function useGetUsersInGroupLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUsersInGroupQuery, GetUsersInGroupQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUsersInGroupQuery, GetUsersInGroupQueryVariables>(GetUsersInGroupDocument, options);
        }
export type GetUsersInGroupQueryHookResult = ReturnType<typeof useGetUsersInGroupQuery>;
export type GetUsersInGroupLazyQueryHookResult = ReturnType<typeof useGetUsersInGroupLazyQuery>;
export type GetUsersInGroupQueryResult = Apollo.QueryResult<GetUsersInGroupQuery, GetUsersInGroupQueryVariables>;
export const GetMyselfDocument = gql`
    query GetMyself {
  me {
    id
    lastname
    firstname
    email
    sex
    admin
    superAdmin
    emailConfirmed
    twoFATypesEnabled
    passwordChangedAt
    passwordReset
    registeredWebAuthnKeys {
      id
      description
      createdAt
    }
    keyPair {
      id
      encryptedPrivateKey
      publicKeyString
    }
    usergroups {
      groupId
      userId
      groupRole
    }
    mandator {
      id
      createdAt
      subscriptionUserLimit
      subscriptionStatus
      isBusinessCustomer
      twoFactorRequired
      customerinformation {
        id
        name
        defaultPaymentMethodId
        email
      }
    }
  }
}
    `;

/**
 * __useGetMyselfQuery__
 *
 * To run a query within a React component, call `useGetMyselfQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMyselfQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetMyselfQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetMyselfQuery(baseOptions?: Apollo.QueryHookOptions<GetMyselfQuery, GetMyselfQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetMyselfQuery, GetMyselfQueryVariables>(GetMyselfDocument, options);
      }
export function useGetMyselfLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetMyselfQuery, GetMyselfQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetMyselfQuery, GetMyselfQueryVariables>(GetMyselfDocument, options);
        }
export type GetMyselfQueryHookResult = ReturnType<typeof useGetMyselfQuery>;
export type GetMyselfLazyQueryHookResult = ReturnType<typeof useGetMyselfLazyQuery>;
export type GetMyselfQueryResult = Apollo.QueryResult<GetMyselfQuery, GetMyselfQueryVariables>;
export const GetWebAuthnChallengeDocument = gql`
    mutation GetWebAuthnChallenge {
  user {
    twofactor {
      getWebauthnChallenge {
        optionsJson
      }
    }
  }
}
    `;
export type GetWebAuthnChallengeMutationFn = Apollo.MutationFunction<GetWebAuthnChallengeMutation, GetWebAuthnChallengeMutationVariables>;

/**
 * __useGetWebAuthnChallengeMutation__
 *
 * To run a mutation, you first call `useGetWebAuthnChallengeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGetWebAuthnChallengeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [getWebAuthnChallengeMutation, { data, loading, error }] = useGetWebAuthnChallengeMutation({
 *   variables: {
 *   },
 * });
 */
export function useGetWebAuthnChallengeMutation(baseOptions?: Apollo.MutationHookOptions<GetWebAuthnChallengeMutation, GetWebAuthnChallengeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<GetWebAuthnChallengeMutation, GetWebAuthnChallengeMutationVariables>(GetWebAuthnChallengeDocument, options);
      }
export type GetWebAuthnChallengeMutationHookResult = ReturnType<typeof useGetWebAuthnChallengeMutation>;
export type GetWebAuthnChallengeMutationResult = Apollo.MutationResult<GetWebAuthnChallengeMutation>;
export type GetWebAuthnChallengeMutationOptions = Apollo.BaseMutationOptions<GetWebAuthnChallengeMutation, GetWebAuthnChallengeMutationVariables>;
export const GetTotpAuthenticatorKeyDocument = gql`
    mutation GetTotpAuthenticatorKey {
  user {
    twofactor {
      getTotpAuthenticatorKey {
        authenticatorString
      }
    }
  }
}
    `;
export type GetTotpAuthenticatorKeyMutationFn = Apollo.MutationFunction<GetTotpAuthenticatorKeyMutation, GetTotpAuthenticatorKeyMutationVariables>;

/**
 * __useGetTotpAuthenticatorKeyMutation__
 *
 * To run a mutation, you first call `useGetTotpAuthenticatorKeyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGetTotpAuthenticatorKeyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [getTotpAuthenticatorKeyMutation, { data, loading, error }] = useGetTotpAuthenticatorKeyMutation({
 *   variables: {
 *   },
 * });
 */
export function useGetTotpAuthenticatorKeyMutation(baseOptions?: Apollo.MutationHookOptions<GetTotpAuthenticatorKeyMutation, GetTotpAuthenticatorKeyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<GetTotpAuthenticatorKeyMutation, GetTotpAuthenticatorKeyMutationVariables>(GetTotpAuthenticatorKeyDocument, options);
      }
export type GetTotpAuthenticatorKeyMutationHookResult = ReturnType<typeof useGetTotpAuthenticatorKeyMutation>;
export type GetTotpAuthenticatorKeyMutationResult = Apollo.MutationResult<GetTotpAuthenticatorKeyMutation>;
export type GetTotpAuthenticatorKeyMutationOptions = Apollo.BaseMutationOptions<GetTotpAuthenticatorKeyMutation, GetTotpAuthenticatorKeyMutationVariables>;
export const AddWebautnDocument = gql`
    mutation AddWebautn($attestationResponse: String!, $description: String) {
  user {
    twofactor {
      addWebauthnKey(
        attestationResponse: $attestationResponse
        description: $description
      ) {
        twoFATypesEnabled
      }
    }
  }
}
    `;
export type AddWebautnMutationFn = Apollo.MutationFunction<AddWebautnMutation, AddWebautnMutationVariables>;

/**
 * __useAddWebautnMutation__
 *
 * To run a mutation, you first call `useAddWebautnMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddWebautnMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addWebautnMutation, { data, loading, error }] = useAddWebautnMutation({
 *   variables: {
 *      attestationResponse: // value for 'attestationResponse'
 *      description: // value for 'description'
 *   },
 * });
 */
export function useAddWebautnMutation(baseOptions?: Apollo.MutationHookOptions<AddWebautnMutation, AddWebautnMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddWebautnMutation, AddWebautnMutationVariables>(AddWebautnDocument, options);
      }
export type AddWebautnMutationHookResult = ReturnType<typeof useAddWebautnMutation>;
export type AddWebautnMutationResult = Apollo.MutationResult<AddWebautnMutation>;
export type AddWebautnMutationOptions = Apollo.BaseMutationOptions<AddWebautnMutation, AddWebautnMutationVariables>;
export const RemoveWebauthnDocument = gql`
    mutation RemoveWebauthn($webauthnKeyId: Guid!) {
  user {
    twofactor {
      removeWebauthnKey(webauthnKeyId: $webauthnKeyId) {
        twoFATypesEnabled
      }
    }
  }
}
    `;
export type RemoveWebauthnMutationFn = Apollo.MutationFunction<RemoveWebauthnMutation, RemoveWebauthnMutationVariables>;

/**
 * __useRemoveWebauthnMutation__
 *
 * To run a mutation, you first call `useRemoveWebauthnMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveWebauthnMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeWebauthnMutation, { data, loading, error }] = useRemoveWebauthnMutation({
 *   variables: {
 *      webauthnKeyId: // value for 'webauthnKeyId'
 *   },
 * });
 */
export function useRemoveWebauthnMutation(baseOptions?: Apollo.MutationHookOptions<RemoveWebauthnMutation, RemoveWebauthnMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RemoveWebauthnMutation, RemoveWebauthnMutationVariables>(RemoveWebauthnDocument, options);
      }
export type RemoveWebauthnMutationHookResult = ReturnType<typeof useRemoveWebauthnMutation>;
export type RemoveWebauthnMutationResult = Apollo.MutationResult<RemoveWebauthnMutation>;
export type RemoveWebauthnMutationOptions = Apollo.BaseMutationOptions<RemoveWebauthnMutation, RemoveWebauthnMutationVariables>;
export const EnableTotpDocument = gql`
    mutation EnableTotp($totp: String!) {
  user {
    twofactor {
      enableTotp(totp: $totp) {
        twoFATypesEnabled
      }
    }
  }
}
    `;
export type EnableTotpMutationFn = Apollo.MutationFunction<EnableTotpMutation, EnableTotpMutationVariables>;

/**
 * __useEnableTotpMutation__
 *
 * To run a mutation, you first call `useEnableTotpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEnableTotpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [enableTotpMutation, { data, loading, error }] = useEnableTotpMutation({
 *   variables: {
 *      totp: // value for 'totp'
 *   },
 * });
 */
export function useEnableTotpMutation(baseOptions?: Apollo.MutationHookOptions<EnableTotpMutation, EnableTotpMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EnableTotpMutation, EnableTotpMutationVariables>(EnableTotpDocument, options);
      }
export type EnableTotpMutationHookResult = ReturnType<typeof useEnableTotpMutation>;
export type EnableTotpMutationResult = Apollo.MutationResult<EnableTotpMutation>;
export type EnableTotpMutationOptions = Apollo.BaseMutationOptions<EnableTotpMutation, EnableTotpMutationVariables>;
export const DisableTwoFaDocument = gql`
    mutation DisableTwoFA {
  user {
    twofactor {
      disableTotp {
        twoFATypesEnabled
      }
    }
  }
}
    `;
export type DisableTwoFaMutationFn = Apollo.MutationFunction<DisableTwoFaMutation, DisableTwoFaMutationVariables>;

/**
 * __useDisableTwoFaMutation__
 *
 * To run a mutation, you first call `useDisableTwoFaMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDisableTwoFaMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [disableTwoFaMutation, { data, loading, error }] = useDisableTwoFaMutation({
 *   variables: {
 *   },
 * });
 */
export function useDisableTwoFaMutation(baseOptions?: Apollo.MutationHookOptions<DisableTwoFaMutation, DisableTwoFaMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DisableTwoFaMutation, DisableTwoFaMutationVariables>(DisableTwoFaDocument, options);
      }
export type DisableTwoFaMutationHookResult = ReturnType<typeof useDisableTwoFaMutation>;
export type DisableTwoFaMutationResult = Apollo.MutationResult<DisableTwoFaMutation>;
export type DisableTwoFaMutationOptions = Apollo.BaseMutationOptions<DisableTwoFaMutation, DisableTwoFaMutationVariables>;
export const AddUserDocument = gql`
    mutation AddUser($user: AddUserInputType!) {
  user {
    add(user: $user) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type AddUserMutationFn = Apollo.MutationFunction<AddUserMutation, AddUserMutationVariables>;

/**
 * __useAddUserMutation__
 *
 * To run a mutation, you first call `useAddUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addUserMutation, { data, loading, error }] = useAddUserMutation({
 *   variables: {
 *      user: // value for 'user'
 *   },
 * });
 */
export function useAddUserMutation(baseOptions?: Apollo.MutationHookOptions<AddUserMutation, AddUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddUserMutation, AddUserMutationVariables>(AddUserDocument, options);
      }
export type AddUserMutationHookResult = ReturnType<typeof useAddUserMutation>;
export type AddUserMutationResult = Apollo.MutationResult<AddUserMutation>;
export type AddUserMutationOptions = Apollo.BaseMutationOptions<AddUserMutation, AddUserMutationVariables>;
export const EditUserDocument = gql`
    mutation EditUser($user: EditUserInputType!) {
  user {
    edit(user: $user) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type EditUserMutationFn = Apollo.MutationFunction<EditUserMutation, EditUserMutationVariables>;

/**
 * __useEditUserMutation__
 *
 * To run a mutation, you first call `useEditUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editUserMutation, { data, loading, error }] = useEditUserMutation({
 *   variables: {
 *      user: // value for 'user'
 *   },
 * });
 */
export function useEditUserMutation(baseOptions?: Apollo.MutationHookOptions<EditUserMutation, EditUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<EditUserMutation, EditUserMutationVariables>(EditUserDocument, options);
      }
export type EditUserMutationHookResult = ReturnType<typeof useEditUserMutation>;
export type EditUserMutationResult = Apollo.MutationResult<EditUserMutation>;
export type EditUserMutationOptions = Apollo.BaseMutationOptions<EditUserMutation, EditUserMutationVariables>;
export const GrantAdminPermissionsDocument = gql`
    mutation GrantAdminPermissions($targetUserId: Guid!, $encryptedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!) {
  user {
    grantAdminPermissions(
      targetUserId: $targetUserId
      encryptedPasswords: $encryptedPasswords
    ) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type GrantAdminPermissionsMutationFn = Apollo.MutationFunction<GrantAdminPermissionsMutation, GrantAdminPermissionsMutationVariables>;

/**
 * __useGrantAdminPermissionsMutation__
 *
 * To run a mutation, you first call `useGrantAdminPermissionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useGrantAdminPermissionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [grantAdminPermissionsMutation, { data, loading, error }] = useGrantAdminPermissionsMutation({
 *   variables: {
 *      targetUserId: // value for 'targetUserId'
 *      encryptedPasswords: // value for 'encryptedPasswords'
 *   },
 * });
 */
export function useGrantAdminPermissionsMutation(baseOptions?: Apollo.MutationHookOptions<GrantAdminPermissionsMutation, GrantAdminPermissionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<GrantAdminPermissionsMutation, GrantAdminPermissionsMutationVariables>(GrantAdminPermissionsDocument, options);
      }
export type GrantAdminPermissionsMutationHookResult = ReturnType<typeof useGrantAdminPermissionsMutation>;
export type GrantAdminPermissionsMutationResult = Apollo.MutationResult<GrantAdminPermissionsMutation>;
export type GrantAdminPermissionsMutationOptions = Apollo.BaseMutationOptions<GrantAdminPermissionsMutation, GrantAdminPermissionsMutationVariables>;
export const RevokeAdminPermissionsDocument = gql`
    mutation RevokeAdminPermissions($targetUserId: Guid!) {
  user {
    revokeAdminPermissions(targetUserId: $targetUserId) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type RevokeAdminPermissionsMutationFn = Apollo.MutationFunction<RevokeAdminPermissionsMutation, RevokeAdminPermissionsMutationVariables>;

/**
 * __useRevokeAdminPermissionsMutation__
 *
 * To run a mutation, you first call `useRevokeAdminPermissionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRevokeAdminPermissionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [revokeAdminPermissionsMutation, { data, loading, error }] = useRevokeAdminPermissionsMutation({
 *   variables: {
 *      targetUserId: // value for 'targetUserId'
 *   },
 * });
 */
export function useRevokeAdminPermissionsMutation(baseOptions?: Apollo.MutationHookOptions<RevokeAdminPermissionsMutation, RevokeAdminPermissionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RevokeAdminPermissionsMutation, RevokeAdminPermissionsMutationVariables>(RevokeAdminPermissionsDocument, options);
      }
export type RevokeAdminPermissionsMutationHookResult = ReturnType<typeof useRevokeAdminPermissionsMutation>;
export type RevokeAdminPermissionsMutationResult = Apollo.MutationResult<RevokeAdminPermissionsMutation>;
export type RevokeAdminPermissionsMutationOptions = Apollo.BaseMutationOptions<RevokeAdminPermissionsMutation, RevokeAdminPermissionsMutationVariables>;
export const ArchiveUserDocument = gql`
    mutation ArchiveUser($targetUserId: Guid!, $reactivate: Boolean, $encryptedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]) {
  user {
    archive(
      id: $targetUserId
      reactivate: $reactivate
      encryptedPasswords: $encryptedPasswords
    ) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type ArchiveUserMutationFn = Apollo.MutationFunction<ArchiveUserMutation, ArchiveUserMutationVariables>;

/**
 * __useArchiveUserMutation__
 *
 * To run a mutation, you first call `useArchiveUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useArchiveUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [archiveUserMutation, { data, loading, error }] = useArchiveUserMutation({
 *   variables: {
 *      targetUserId: // value for 'targetUserId'
 *      reactivate: // value for 'reactivate'
 *      encryptedPasswords: // value for 'encryptedPasswords'
 *   },
 * });
 */
export function useArchiveUserMutation(baseOptions?: Apollo.MutationHookOptions<ArchiveUserMutation, ArchiveUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ArchiveUserMutation, ArchiveUserMutationVariables>(ArchiveUserDocument, options);
      }
export type ArchiveUserMutationHookResult = ReturnType<typeof useArchiveUserMutation>;
export type ArchiveUserMutationResult = Apollo.MutationResult<ArchiveUserMutation>;
export type ArchiveUserMutationOptions = Apollo.BaseMutationOptions<ArchiveUserMutation, ArchiveUserMutationVariables>;
export const ResendAccountActivationMailDocument = gql`
    mutation ResendAccountActivationMail($targetUserId: Guid!, $publicKey: String!, $encryptedPrivateKey: String!, $newTempLoginPassword: String!, $newTempLoginPasswordHashed: String!, $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]) {
  user {
    resendAccountActivationEmail(
      targetUserId: $targetUserId
      publicKey: $publicKey
      encryptedPrivateKey: $encryptedPrivateKey
      newTempLoginPassword: $newTempLoginPassword
      newTempLoginPasswordHashed: $newTempLoginPasswordHashed
      encryptedSharedPasswords: $encryptedSharedPasswords
    ) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type ResendAccountActivationMailMutationFn = Apollo.MutationFunction<ResendAccountActivationMailMutation, ResendAccountActivationMailMutationVariables>;

/**
 * __useResendAccountActivationMailMutation__
 *
 * To run a mutation, you first call `useResendAccountActivationMailMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useResendAccountActivationMailMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [resendAccountActivationMailMutation, { data, loading, error }] = useResendAccountActivationMailMutation({
 *   variables: {
 *      targetUserId: // value for 'targetUserId'
 *      publicKey: // value for 'publicKey'
 *      encryptedPrivateKey: // value for 'encryptedPrivateKey'
 *      newTempLoginPassword: // value for 'newTempLoginPassword'
 *      newTempLoginPasswordHashed: // value for 'newTempLoginPasswordHashed'
 *      encryptedSharedPasswords: // value for 'encryptedSharedPasswords'
 *   },
 * });
 */
export function useResendAccountActivationMailMutation(baseOptions?: Apollo.MutationHookOptions<ResendAccountActivationMailMutation, ResendAccountActivationMailMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ResendAccountActivationMailMutation, ResendAccountActivationMailMutationVariables>(ResendAccountActivationMailDocument, options);
      }
export type ResendAccountActivationMailMutationHookResult = ReturnType<typeof useResendAccountActivationMailMutation>;
export type ResendAccountActivationMailMutationResult = Apollo.MutationResult<ResendAccountActivationMailMutation>;
export type ResendAccountActivationMailMutationOptions = Apollo.BaseMutationOptions<ResendAccountActivationMailMutation, ResendAccountActivationMailMutationVariables>;
export const ChangeOwnLoginPasswordDocument = gql`
    mutation ChangeOwnLoginPassword($publicKeyString: String!, $encryptedPrivateKey: String!, $previousPasswordHashEncryptedWithPublicKey: String!, $currentPassword: String!, $newPassword: String!, $encryptedSharedPasswords: [AddEncryptedSharedPasswordWithoutPublicKeyInputType]!, $encryptedAccountPasswords: [AddEncryptedAccountPasswordWithoutPublicKeyInputType]!) {
  user {
    changeOwnLoginPassword(
      publicKeyString: $publicKeyString
      encryptedPrivateKey: $encryptedPrivateKey
      previousPasswordHashEncryptedWithPublicKey: $previousPasswordHashEncryptedWithPublicKey
      currentPassword: $currentPassword
      newPassword: $newPassword
      encryptedSharedPasswords: $encryptedSharedPasswords
      encryptedAccountPasswords: $encryptedAccountPasswords
    ) {
      ...userFields
    }
  }
}
    ${UserFieldsFragmentDoc}`;
export type ChangeOwnLoginPasswordMutationFn = Apollo.MutationFunction<ChangeOwnLoginPasswordMutation, ChangeOwnLoginPasswordMutationVariables>;

/**
 * __useChangeOwnLoginPasswordMutation__
 *
 * To run a mutation, you first call `useChangeOwnLoginPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeOwnLoginPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeOwnLoginPasswordMutation, { data, loading, error }] = useChangeOwnLoginPasswordMutation({
 *   variables: {
 *      publicKeyString: // value for 'publicKeyString'
 *      encryptedPrivateKey: // value for 'encryptedPrivateKey'
 *      previousPasswordHashEncryptedWithPublicKey: // value for 'previousPasswordHashEncryptedWithPublicKey'
 *      currentPassword: // value for 'currentPassword'
 *      newPassword: // value for 'newPassword'
 *      encryptedSharedPasswords: // value for 'encryptedSharedPasswords'
 *      encryptedAccountPasswords: // value for 'encryptedAccountPasswords'
 *   },
 * });
 */
export function useChangeOwnLoginPasswordMutation(baseOptions?: Apollo.MutationHookOptions<ChangeOwnLoginPasswordMutation, ChangeOwnLoginPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeOwnLoginPasswordMutation, ChangeOwnLoginPasswordMutationVariables>(ChangeOwnLoginPasswordDocument, options);
      }
export type ChangeOwnLoginPasswordMutationHookResult = ReturnType<typeof useChangeOwnLoginPasswordMutation>;
export type ChangeOwnLoginPasswordMutationResult = Apollo.MutationResult<ChangeOwnLoginPasswordMutation>;
export type ChangeOwnLoginPasswordMutationOptions = Apollo.BaseMutationOptions<ChangeOwnLoginPasswordMutation, ChangeOwnLoginPasswordMutationVariables>;