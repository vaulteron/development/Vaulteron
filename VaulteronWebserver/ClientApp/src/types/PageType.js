﻿import { oneOf } from "prop-types";

export const PageTypeList = {
    default: "default",
    adminConsole: "adminConsole",
    superAdminConsole: "superAdminConsole",
};
export const PageTypes = Object.freeze(PageTypeList);
export const PageType = oneOf(Object.values(PageTypeList));
