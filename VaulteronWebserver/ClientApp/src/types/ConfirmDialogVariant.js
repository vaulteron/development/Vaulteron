﻿import { oneOf } from "prop-types";

export const ConfirmDialogVariantList = {
    info: "info",
    warning: "warning",
    error: "error",
};
export const ConfirmDialogVariants = Object.freeze(ConfirmDialogVariantList);
export const ConfirmDialogVariant = oneOf(Object.values(ConfirmDialogVariantList));
