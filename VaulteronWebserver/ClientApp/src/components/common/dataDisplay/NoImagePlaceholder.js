﻿import React from "react";
import PropTypes from "prop-types";
import Icon from "./Icon";
import makeStyles from "@mui/styles/makeStyles";
import classNames from "classnames";
import { IconTypes } from "../../../types/IconType";

const useStyles = makeStyles((theme) => ({
    container: {
        width: (props) => props.width,
        height: (props) => props.height,
        backgroundColor: theme.colors.iconDefault,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        cursor: (props) => (props.hasOnClick ? "pointer" : "inherit"),
    },
}));

const NoImagePlaceholder = ({ width, height, onClick, className }) => {
    const classes = useStyles({ width: width, height: height, hasOnClick: onClick !== undefined });

    return (
        <div className={classNames(classes.container, className)} onClick={onClick}>
            <Icon iconType={IconTypes.image} />
        </div>
    );
};

NoImagePlaceholder.defaultProps = {
    width: 150,
    height: 60,
};

NoImagePlaceholder.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

export default NoImagePlaceholder;
