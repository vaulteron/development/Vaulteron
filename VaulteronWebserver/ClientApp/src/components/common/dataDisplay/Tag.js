﻿import React from "react";
import PropTypes from "prop-types";
import makeStyles from "@mui/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    container: {
        backgroundColor: (props) => props.color,
        display: "inline-block",
        borderRadius: "5px",
        padding: "1px 5px",
        margin: "0 2px",
    },
}));

const Tag = ({ name, color, onClick }) => {
    const classes = useStyles({ color: color ? color : "#F3F3F3" });

    return (
        <label className={classes.container} onClick={onClick}>
            {name}
        </label>
    );
};

Tag.defaultProps = {
    color: "#F3F3F3",
};

Tag.propTypes = {
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
    onClick: PropTypes.func,
};

export default Tag;
