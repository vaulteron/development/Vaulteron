﻿import React from "react";
import PropTypes from "prop-types";
import { ListItemText, MenuItem, Select } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";

const htmlColors = require("../../../../htmlColor.json");

const useStyles = makeStyles((theme) => ({
    colorCircle: {
        width: 20,
        height: 20,
        borderRadius: "100%",
        marginRight: 10,
    },
    select: {
        width: "100%",
        marginTop: "6px",
        maxHeight: "38px",
        "& .MuiSelect-select": {
            display: "flex",
            alignItems: "center",
            padding: "6px 0 7px 14px",
        },
    },
}));
const ColorSelect = ({ value, onChange, variant, size }) => {
    const classes = useStyles();

    return (
        <Select className={classes.select} value={value} onChange={onChange} variant={variant}>
            {Object.entries(htmlColors).map(([name, value]) => {
                return (
                    <MenuItem key={name} value={value} selected={value === htmlColors["Grün"]}>
                        <div className={classes.colorCircle} style={{ backgroundColor: value }} />
                        <ListItemText primary={name} />
                    </MenuItem>
                );
            })}
        </Select>
    );
};

ColorSelect.defaultProps = {
    value: htmlColors["Grün"],
};

ColorSelect.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    variant: PropTypes.string,
    size: PropTypes.string,
};

export default ColorSelect;
