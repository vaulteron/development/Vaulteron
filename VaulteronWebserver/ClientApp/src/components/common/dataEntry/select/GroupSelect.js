﻿import React from "react";
import { ListItemText, MenuItem, Select } from "@mui/material";
import PropTypes from "prop-types";
import { useAllGroupsQuery } from "../../../../hooks/queries/groupQueryHooks";

const GroupSelect = (props) => {
    const { groups } = useAllGroupsQuery();

    const { value, onChange, variant, size } = props;

    return (
        <Select
            style={{ width: "100%", marginTop: "6px", maxHeight: "38px", overflowY: "none" }}
            value={value || ""}
            onChange={onChange}
            displayEmpty
            input={<Select variant={variant} size={size} />}
        >
            <MenuItem value="" disabled>
                Pls choose a group
            </MenuItem>
            {groups.map((d) => {
                const { name, id } = d;
                return (
                    <MenuItem key={id} value={id}>
                        <ListItemText primary={name} />
                    </MenuItem>
                );
            })}
        </Select>
    );
};

GroupSelect.defaultProps = {};

GroupSelect.propTypes = {
    value: PropTypes.array,
    onChange: PropTypes.func.isRequired,
    variant: PropTypes.string,
    size: PropTypes.string,
};

export default GroupSelect;
