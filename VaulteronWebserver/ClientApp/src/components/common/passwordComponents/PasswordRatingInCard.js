﻿import React from "react";
import PropTypes from "prop-types";
import makeStyles from "@mui/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        "& > div": {
            height: "5px",
            width: "30px",
            marginRight: "5px",
        },
    },
    dangerous: { backgroundColor: "red" },
    weak: { backgroundColor: "orange" },
    secure: { backgroundColor: "#aefad4" },
    verySecure: { backgroundColor: "#00FF00" },
    empty: { backgroundColor: "gray" },
}));

const PasswordRatingInCard = ({ rating, skeleton }) => {
    const classes = useStyles();

    let ratingClassname, numBarsColored;

    if (skeleton) rating = -1;

    switch (rating) {
        case 4:
            ratingClassname = classes.dangerous;
            numBarsColored = 1;
            break;
        case 3:
            ratingClassname = classes.weak;
            numBarsColored = 2;
            break;
        case 2:
            ratingClassname = classes.secure;
            numBarsColored = 3;
            break;
        case 1:
            ratingClassname = classes.verySecure;
            numBarsColored = 4;
            break;
        default:
            ratingClassname = classes.empty;
            numBarsColored = 0;
            break;
    }

    return (
        <div className={classes.container}>
            {Array(...Array(numBarsColored)).map((_, i) => (
                <div className={ratingClassname} key={"a" + i} />
            ))}
            {Array(...Array(4 - numBarsColored)).map((_, i) => (
                <div className={classes.empty} key={"b" + i} />
            ))}
        </div>
    );
};

PasswordRatingInCard.defaultProps = {
    rating: -1,
};

PasswordRatingInCard.propTypes = {
    rating: PropTypes.number,
    skeleton: PropTypes.bool,
};

export default PasswordRatingInCard;
