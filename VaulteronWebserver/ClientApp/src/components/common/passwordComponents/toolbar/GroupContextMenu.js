﻿import React from "react";
import PropTypes from "prop-types";

import { Menu, MenuItem } from "@mui/material";

export default function GroupContextMenu({ anchorEl, onClose }) {
    return (
        <Menu anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={onClose}>
            <MenuItem onClick={onClose}>Profile</MenuItem>
        </Menu>
    );
}

GroupContextMenu.propTypes = {
    onClose: PropTypes.func,
    anchorEl: PropTypes.string,
};
