﻿export const VaulteronPasswordColumns = Object.freeze({
    passwortname: "Passwortname",
    username: "Benutzername oder E-Mail",
    password: "Passwort",
    website: "Webseite",
    notes: "Zusätzliche Informationen",
});
