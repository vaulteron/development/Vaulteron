﻿import React from "react";
import { ListItemIcon, Menu, MenuItem, Typography, Divider } from "@mui/material";
import Icon from "../common/dataDisplay/Icon";
import PropTypes from "prop-types";
import makeStyles from "@mui/styles/makeStyles";
import { IconTypes } from "../../types/IconType";
import { useMyselfRole } from "../../hooks/queries/userQueryHooks";
import useTranslation from "../common/translation/translation";

const useStyles = makeStyles((theme) => ({
    icon: {
        minWidth: "40px",
    },
    divider: {
        marginTop: "5px",
        marginBottom: "5px",
    },
}));

const GroupBreadcrumbContextMenu = ({ anchorEl, onClose, onEdit, onArchive, onMove, groupId }) => {
    const classes = useStyles();
    const { isViewer, isAdmin } = useMyselfRole(groupId);
    const { i18n } = useTranslation();

    return (
        <>
            <Menu keepMounted open={Boolean(anchorEl)} onClose={onClose} anchorReference="anchorPosition" anchorPosition={anchorEl}>
                <MenuItem
                    onClick={() => {
                        onEdit();
                        onClose();
                    }}
                >
                    <ListItemIcon>
                        <Icon className={classes.icon} iconType={isViewer ? IconTypes.visibleEye : IconTypes.editPasswordPen} />
                    </ListItemIcon>
                    <Typography variant="inherit">{isViewer ? i18n("general.actions.view") : i18n("general.actions.edit")}</Typography>
                </MenuItem>
                {isAdmin && (
                    <MenuItem
                        onClick={() => {
                            onClose();
                            onMove();
                        }}
                    >
                        <ListItemIcon>
                            <Icon className={classes.icon} iconType={IconTypes.expandArrows} />
                        </ListItemIcon>
                        <Typography variant="inherit">{i18n("general.actions.move")}</Typography>
                    </MenuItem>
                )}

                {isAdmin && <Divider className={classes.divider} />}

                {isAdmin && (
                    <MenuItem
                        onClick={() => {
                            onClose();
                            onArchive();
                        }}
                    >
                        <ListItemIcon>
                            <Icon className={classes.icon} iconType={IconTypes.deletePassword} />
                        </ListItemIcon>
                        <Typography variant="inherit">{i18n("general.actions.archive")}</Typography>
                    </MenuItem>
                )}
            </Menu>
        </>
    );
};

GroupBreadcrumbContextMenu.propTypes = {
    onEdit: PropTypes.func.isRequired,
    anchorEl: PropTypes.object,
    onClose: PropTypes.func.isRequired,
    onArchive: PropTypes.func.isRequired,
    onMove: PropTypes.func.isRequired,
    groupId: PropTypes.string.isRequired,
};

export default GroupBreadcrumbContextMenu;
