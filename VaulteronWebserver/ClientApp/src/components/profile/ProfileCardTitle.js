﻿import React from "react";
import Grid from "@mui/material/Grid";
import makeStyles from "@mui/styles/makeStyles";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
    container: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
}));

const ProfileCardTitle = (props) => {
    const classes = useStyles();
    const { title } = props;

    return (
        <>
            <Grid item md={12} className={classes.container}>
                <h1>{title}</h1>
            </Grid>
        </>
    );
};

ProfileCardTitle.propTypes = {
    title: PropTypes.string.isRequired,
};
export default ProfileCardTitle;
