﻿import React from "react";
import Page from "../common/layout/Page";
import makeStyles from "@mui/styles/makeStyles";
import ContentContainer from "../common/layout/ContentContainer";

const useStyles = makeStyles((theme) => ({}));

const ArchivePage = () => {
    const classes = useStyles();

    return (
        <Page>
            <ContentContainer>Archive page</ContentContainer>
        </Page>
    );
};

export default ArchivePage;
