﻿import React from "react";
import CustomModal from "../common/feedback/modals/CustomModal";
import { Grid } from "@mui/material";
import PropTypes from "prop-types";
import TextField from "@mui/material/TextField";
import { useMaterialUIInput } from "../../hooks/inputHook";
import ColorSelect from "../common/dataEntry/select/ColorSelect";
import { useAccountTagMutation } from "../../hooks/queries/tagQueryHooks";

const AccountTagModal = (props) => {
    const { tag, onClose } = props;

    const { value: name, onChange: setName } = useMaterialUIInput(tag ? tag.name : "");
    const { value: color, onChange: setColor } = useMaterialUIInput("black");

    const { addAccountTag, updateAccountTag } = useAccountTagMutation();

    const handleOk = async () => {
        const accountTag = {
            name: name,
            color: color,
        };

        if (tag) {
            await updateAccountTag({ variables: { accountTag: { ...accountTag, id: tag.id } } });
        } else {
            await addAccountTag({ variables: { accountTag: accountTag } });
        }

        onClose();
    };

    return (
        <CustomModal maxWidth="sm" onClose={onClose} saveText={tag ? "Updaten" : "Hinzufügen"} onOk={handleOk} title="AccountTag">
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    <TextField margin="dense" fullWidth onChange={setName} value={name} label="New name of tag" variant="outlined" />
                </Grid>
                <Grid item xs={4}>
                    <ColorSelect variant="outlined" onChange={setColor} value={color} />
                </Grid>
            </Grid>
        </CustomModal>
    );
};

AccountTagModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    tag: PropTypes.object,
};

export default AccountTagModal;
