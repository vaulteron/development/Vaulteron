﻿import React from "react";
import { ListItemIcon, Menu, MenuItem, Typography } from "@mui/material";
import Icon from "../common/dataDisplay/Icon";
import PropTypes from "prop-types";
import makeStyles from "@mui/styles/makeStyles";
import { IconTypes } from "../../types/IconType";

const useStyles = makeStyles((theme) => ({
    contextMenuDescription: {
        marginLeft: "-25px",
    },
}));

const TagContextMenu = (props) => {
    const classes = useStyles();

    const { anchorEl, onClose, onConfirmDeleteModal, sharedTag, onOpenAccountTagModal, onOpenSharedTagModal } = props;
    return (
        <Menu keepMounted open={Boolean(anchorEl)} onClose={onClose} anchorReference="anchorPosition" anchorPosition={anchorEl}>
            <MenuItem
                onClick={() => {
                    onClose();
                    if (sharedTag) onOpenSharedTagModal();
                    else onOpenAccountTagModal();
                }}
            >
                <ListItemIcon>
                    {" "}
                    <Icon iconType={IconTypes.editPasswordPen} />{" "}
                </ListItemIcon>
                <Typography className={classes.contextMenuDescription} variant="inherit">
                    Bearbeiten
                </Typography>
            </MenuItem>

            <MenuItem
                onClick={() => {
                    onClose();
                    onConfirmDeleteModal();
                }}
            >
                <ListItemIcon>
                    {" "}
                    <Icon iconType={IconTypes.deletePassword} />{" "}
                </ListItemIcon>
                <Typography className={classes.contextMenuDescription} variant="inherit">
                    Löschen
                </Typography>
            </MenuItem>
        </Menu>
    );
};

TagContextMenu.propTypes = {
    anchorEl: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    onConfirmDeleteModal: PropTypes.func.isRequired,
    onOpenAccountTagModal: PropTypes.func.isRequired,
    onOpenSharedTagModal: PropTypes.func.isRequired,

    /**
     * If value is true context menu of shared tag will be rendered
     */
    sharedTag: PropTypes.bool.isRequired,
};

export default TagContextMenu;
