﻿import React, { useState } from "react";
import Page from "../common/layout/Page";
import TagSearchBar from "./TagSearchBar";
import TagListContainer from "./TagListContainer";
import AccountTagTable from "./AccountTagTable";
import SharedTagTable from "./SharedTagTable";
import AccountTagModal from "./AccountTagModal";
import { useContextMenu, useModalStatus } from "../../hooks/modalHook";
import SharedTagModal from "./SharedTagModal";
import TagContextMenu from "./TagContextMenu";
import ConfirmDeleteTagModal from "./ConfirmDeleteTagModal";
import { useAccountTagQuery, useSharedTagQuery } from "../../hooks/queries/tagQueryHooks";
import ContentContainer from "../common/layout/ContentContainer";

const TagPage = (props) => {
    const [searchTerm, setSearchTerm] = useState("");
    const { modalState: accountTagModalState, open: openAccountTagModal, close: closeAccountTagModal } = useModalStatus();
    const { modalState: sharedTagModalState, open: openSharedTagModal, close: closeSharedTagModal } = useModalStatus();
    const { modalState: removeModalState, open: openRemoveModal, close: closeRemoveModal } = useModalStatus();

    const { anchorEl, open: openContextMenu, close: closeContextMenu } = useContextMenu();

    const [currentAccountTag, setCurrentAccountTag] = useState(null);
    const [currentSharedTag, setCurrentSharedTag] = useState(null);

    const { accountTags } = useAccountTagQuery(searchTerm);
    const { sharedTags } = useSharedTagQuery(searchTerm);

    return (
        <Page>
            <TagSearchBar onSearchTerm={setSearchTerm} />
            <ContentContainer>
                <TagListContainer
                    title="Tags von Ihren eigenen Passwörtern"
                    onAdd={() => {
                        setCurrentAccountTag(null);
                        openAccountTagModal();
                    }}
                >
                    <AccountTagTable
                        tags={accountTags}
                        onContextMenu={(e, tag) => {
                            setCurrentAccountTag(tag);
                            setCurrentSharedTag(null);
                            openContextMenu(e);
                        }}
                    />
                </TagListContainer>

                <TagListContainer
                    title="Tags von Ihren Gruppen"
                    onAdd={() => {
                        setCurrentSharedTag(null);
                        openSharedTagModal();
                    }}
                >
                    <SharedTagTable
                        tags={sharedTags}
                        onContextMenu={(e, tag) => {
                            setCurrentAccountTag(null);
                            setCurrentSharedTag(tag);
                            openContextMenu(e);
                        }}
                    />
                </TagListContainer>
            </ContentContainer>

            {accountTagModalState && <AccountTagModal tag={currentAccountTag} onClose={closeAccountTagModal} />}
            {sharedTagModalState && <SharedTagModal tag={currentSharedTag} onClose={closeSharedTagModal} />}

            {removeModalState && (
                <ConfirmDeleteTagModal
                    tag={currentSharedTag !== null ? currentSharedTag : currentAccountTag}
                    sharedTag={currentSharedTag !== null}
                    onClose={closeRemoveModal}
                />
            )}

            <TagContextMenu
                anchorEl={anchorEl}
                sharedTag={currentSharedTag !== null}
                onClose={closeContextMenu}
                onOpenAccountTagModal={openAccountTagModal}
                onOpenSharedTagModal={openSharedTagModal}
                onConfirmDeleteModal={openRemoveModal}
            />
        </Page>
    );
};

export default TagPage;
