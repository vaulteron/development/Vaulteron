﻿import React, { useState } from "react";
import PropTypes from "prop-types";
import CustomModal from "../common/feedback/modals/CustomModal";
import { Grid, TextField } from "@mui/material";
import { useMaterialUIInput } from "../../hooks/inputHook";
import { useSharedTagMutation } from "../../hooks/queries/tagQueryHooks";
import ColorSelect from "../common/dataEntry/select/ColorSelect";
import GroupSelect from "../common/dataEntry/select/GroupSelect";

const htmlColors = require("../../htmlColor.json");

const SharedTagModal = (props) => {
    const { tag, onClose, currentGroupId } = props;
    const [errorMessage, setErrorMessage] = useState("");

    const { value: name, onChange: setName } = useMaterialUIInput(tag ? tag.name : "");
    const { value: color, onChange: setColor } = useMaterialUIInput(htmlColors["Grün"]);
    const { value: groupId, onChange: setGroupId } = useMaterialUIInput(tag ? tag.group.id : undefined);

    const { addSharedTag, updateSharedTag } = useSharedTagMutation();

    const handleOk = async () => {
        try {
            const sharedTag = {
                name: name,
                color: color,
                groupId: currentGroupId ? currentGroupId : groupId,
            };
            if (tag) {
                await updateSharedTag({ variables: { sharedTag: { ...sharedTag, id: tag.id } } });
            } else {
                await addSharedTag({ variables: { sharedTag: sharedTag } });
            }
            onClose();
        } catch (e) {
            console.error(e);
            setErrorMessage(e);
        }
    };

    return (
        <CustomModal
            maxWidth="sm"
            onClose={onClose}
            saveText={tag ? "Updaten" : "Hinzufügen"}
            onOk={handleOk}
            okButtonEnabled={Boolean(name)}
            title="SharedTag"
            error={errorMessage !== ""}
            errorMessage={errorMessage}
            loading={false}
        >
            <Grid container spacing={2} style={{ minHeight: "120px" }}>
                {!currentGroupId && (
                    <Grid item xs={12}>
                        <GroupSelect variant="outlined" value={groupId} onChange={setGroupId} />
                    </Grid>
                )}
                <Grid item xs={8}>
                    <TextField margin="dense" fullWidth onChange={setName} value={name} label="Name des neuen Tags" variant="outlined" />
                </Grid>
                <Grid item xs={4}>
                    <ColorSelect variant="outlined" onChange={setColor} value={color} />
                </Grid>
            </Grid>
        </CustomModal>
    );
};
SharedTagModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    currentGroupId: PropTypes.string,
    tag: PropTypes.object,
};

export default SharedTagModal;
