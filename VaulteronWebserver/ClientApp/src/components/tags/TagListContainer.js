﻿import React from "react";
import PropTypes from "prop-types";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ContentContainer from "../common/layout/ContentContainer";
import makeStyles from "@mui/styles/makeStyles";

import Button from "@mui/material/Button";
import Icon from "../common/dataDisplay/Icon";
import { IconTypes } from "../../types/IconType";

const useStyles = makeStyles((theme) => ({
    groupHeader: {
        fontSize: "1.3rem",
        width: "100%",
        borderBottom: "2px #0000003b solid",
    },
    cards: {
        display: "flex",
        flexWrap: "wrap",
    },
    cardContainer: {
        width: "100%",
        marginBottom: "20px",
    },
    onAdd: {
        float: "right",
    },
}));

const TagListContainer = ({ children, title, onAdd }) => {
    const classes = useStyles();

    return (
        <ContentContainer>
            <Grid container>
                <Grid item className={classes.groupHeader}>
                    {title}
                    {onAdd && (
                        <div className={classes.onAdd}>
                            <Button startIcon={<Icon iconType={IconTypes.plus} />} onClick={onAdd}>
                                Hinzufügen
                            </Button>
                        </div>
                    )}
                </Grid>
                <Grid item className={classes.cardContainer}>
                    <List className={classes.cards}>
                        <Grid container spacing={1}>
                            {children}
                        </Grid>
                    </List>
                </Grid>
            </Grid>
        </ContentContainer>
    );
};

TagListContainer.propTypes = {
    children: PropTypes.arrayOf(PropTypes.node).isRequired,
    title: PropTypes.string.isRequired,
    onAdd: PropTypes.func,
};

export default TagListContainer;
