﻿import React, { useEffect } from "react";
import SearchBar from "../common/dataEntry/SearchBar";
import { useDelayedMaterialUIInput } from "../../hooks/inputHook";
import PropTypes from "prop-types";
import { useIsMobile } from "../../hooks/mobileHook";

const TagSearchBar = ({ onSearchTerm }) => {
    const { value: searchTerm, localValue: localSearchTerm, onChange: setLocalSearchTerm } = useDelayedMaterialUIInput("");
    const isMobile = useIsMobile();

    useEffect(() => {
        onSearchTerm(searchTerm);
    }, [searchTerm]);

    return <SearchBar placeholder={isMobile ? "Tag suchen" : "Nach welchem Tag suchen Sie?"} value={localSearchTerm} onChange={setLocalSearchTerm} />;
};
TagSearchBar.propTypes = {
    onSearchTerm: PropTypes.func.isRequired,
};

export default TagSearchBar;
