﻿import React from "react";
import PropTypes from "prop-types";
import { useAccountTagMutation, useSharedTagMutation } from "../../hooks/queries/tagQueryHooks";
import ConfirmationDialog from "../common/feedback/modals/ConfirmationDialog";

const ConfirmDeleteTagModal = ({ onClose, tag, sharedTag }) => {
    const { deleteAccountTag } = useAccountTagMutation();
    const { deleteSharedTag } = useSharedTagMutation();

    const handleDelete = async () => {
        if (sharedTag) {
            await deleteSharedTag({ variables: { id: tag.id } });
        } else {
            await deleteAccountTag({ variables: { id: tag.id } });
        }
        onClose();
    };

    return (
        <ConfirmationDialog
            onOk={handleDelete}
            onClose={onClose}
            open
            cancelText="Abbrechen"
            okText="Löschen"
            title={`Löschen des Tag ${tag.name}`}
            description="Wollen Sie den Tag wirklich löschen?"
        />
    );
};

ConfirmDeleteTagModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    tag: PropTypes.object,

    /**
     * If value is true context menu of shared tag will be rendered
     */
    sharedTag: PropTypes.bool.isRequired,
};

export default ConfirmDeleteTagModal;
