﻿import React from "react";
import { Table, TableBody, TableCell, TableRow } from "@mui/material";
import PropTypes from "prop-types";

const AccountTagTable = (props) => {
    const { tags, onContextMenu, onRowClick } = props;
    return (
        <Table size="small">
            <TableBody>
                {tags.length > 0 &&
                    tags.map((tag, i) => (
                        <TableRow key={i} onContextMenu={(e) => onContextMenu(e, tag)} hover>
                            <TableCell onClick={onRowClick}>{tag.name}</TableCell>
                            <TableCell onClick={onRowClick}>{tag.color}</TableCell>
                        </TableRow>
                    ))}

                {tags.length === 0 && <h4>Keine Tags gefunden</h4>}
            </TableBody>
        </Table>
    );
};
AccountTagTable.propTypes = {
    tags: PropTypes.array.isRequired,
    onRowClick: PropTypes.func.isRequired,
    onContextMenu: PropTypes.func.isRequired,
};

export default AccountTagTable;
