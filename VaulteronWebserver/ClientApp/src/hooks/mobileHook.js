﻿import { useMediaQuery, useTheme } from "@mui/material";
import { useDeviceInfos } from "./deviceInfosHook";

export const useIsMobile = () => {
    const { isMobileDevice } = useDeviceInfos();

    const theme = useTheme();
    return useMediaQuery(theme.breakpoints.down("xs")) || isMobileDevice;
};
