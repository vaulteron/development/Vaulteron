export const useDeviceInfos = () => {
    const isMobileDevice = /Mobi|Android/i.test(navigator.userAgent);

    return {
        isMobileDevice,
        isChromium: Boolean(window.chrome),
    };
};
