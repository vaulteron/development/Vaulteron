﻿const guid = {
    empty: "00000000-0000-0000-0000-000000000000",
};

const isGuidEmpty = (guidString) => {
    return !guidString || guidString === "" || guidString === guid.empty;
};

export { guid, isGuidEmpty };
