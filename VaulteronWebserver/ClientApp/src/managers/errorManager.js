﻿export const extractApolloErrorMessage = (e, humanReadableMessage = "Es ist ein Fehler aufgetreten: ") => {
    if (e.networkError && e.networkError.result && e.networkError.result.message)
        return `${humanReadableMessage} ${e.networkError.result.message}: ${e.networkError.result.reasons.join(", ")}`;
    else return `${humanReadableMessage} ${e.message}`;
};
