﻿/**
 * @typedef {Object} ApolloError
 * @property {string} message
 * @property {*} graphQLErrors
 * @property {*} clientErrors
 * @property {Error | null} networkError
 * @property {*} extraInfo
 */

/**
 * @typedef GroupTreeNode
 * @property {string} Id
 * @property {string} Name
 * @property {GroupTreeNode[]} Children
 */
