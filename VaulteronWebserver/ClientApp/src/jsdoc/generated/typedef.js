/**
 * @typedef {Object} AccountPasswordAccessLogType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} AccountPasswordMutation
 * @property {AccountPasswordType} [add]
 * @property {AccountPasswordType} [delete]
 * @property {AccountPasswordType} [edit]
 */

/**
 * @typedef {Object} AccountPasswordType
 * @property {Array<(AccountPasswordAccessLogType|null|undefined)>} [accessLog]
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} [encryptedString]
 * @property {string} [id]
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {OwnUserType} [owner]
 * @property {number} [securityRating]
 * @property {Array<(AccountTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} AccountTagMutation
 * @property {AccountTagType} [add]
 * @property {AccountTagType} [delete]
 * @property {AccountTagType} [edit]
 */

/**
 * @typedef {Object} AccountTagType
 * @property {string} [color]
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} name
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} AddAccountPasswordInputType
 * @property {string} encryptedPassword
 * @property {string} keyPairId
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {Array<string>} [tags]
 * @property {string} websiteUrl
 */

/**
 * @typedef {Object} AddAccountTagInputType
 * @property {string} color
 * @property {string} name
 */

/**
 * @typedef {Object} AddCompanyPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {string} [login]
 * @property {string} name
 * @property {string} [notes]
 * @property {string} [websiteUrl]
 */

/**
 * @typedef {Object} AddEncryptedAccountPasswordWithoutPublicKeyInputType
 * @property {string} accountPasswordId
 * @property {string} encryptedPasswordString
 */

/**
 * @typedef {Object} AddEncryptedSharedPasswordWithoutPublicKeyInputType
 * @property {string} encryptedPasswordString
 * @property {string} sharedPasswordId
 */

/**
 * @typedef {Object} AddOfflinePasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {string} [login]
 * @property {string} name
 * @property {string} [notes]
 * @property {string} [websiteUrl]
 */

/**
 * @typedef {Object} AddSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {Guid} [groupId]
 * @property {string} [login]
 * @property {string} name
 * @property {string} [notes]
 * @property {Array<string>} [tags]
 * @property {string} [websiteUrl]
 */

/**
 * @typedef {Object} AddSharedTagInputType
 * @property {string} color
 * @property {string} groupId
 * @property {string} name
 */

/**
 * @typedef {Object} AddUserGroupType
 * @property {Array<(AddEncryptedSharedPasswordWithoutPublicKeyInputType|null|undefined)>} [encryptedSharedPasswords]
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {string} userId
 */

/**
 * @typedef {Object} AddUserInputType
 * @property {boolean} admin
 * @property {string} email
 * @property {string} encryptedPrivateKey
 * @property {Array<(AddEncryptedSharedPasswordWithoutPublicKeyInputType|null|undefined)>} [encryptedSharedPasswords]
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<string>} [groupsToBeAddedTo]
 * @property {string} lastname
 * @property {string} newTemporaryLoginPassword
 * @property {string} newTemporaryLoginPasswordHashed
 * @property {string} publicKey
 * @property {Sex} sex
 */

/**
 * @typedef {Object} BulkSyncEncryptedSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} encryptedPasswords
 * @property {string} sharedPasswordToAddTo
 */

/**
 * @typedef {*} Byte
 */

/**
 * @typedef {Object} ChangeLogType
 * @property {DateTime} createdAt
 * @property {UserType} [createdBy]
 * @property {string} createdById
 * @property {string} entityName
 * @property {string} id
 * @property {ChangeLogValueType} [newValue]
 * @property {ChangeLogValueType} [oldValue]
 * @property {string} propertyName
 */

/**
 * @typedef {Object} ChangeLogValueType
 * @property {Guid} [entityId]
 * @property {string} name
 */

/**
 * @typedef {*} DateTime
 */

/**
 * @typedef {Object} DeviceInformationMutation
 * @property {boolean} [send]
 */

/**
 * @typedef {Object} DeviceInformationType
 * @property {string} browser
 * @property {string} browserVersion
 * @property {string} city
 * @property {string} cityLatLong
 * @property {string} countryCode
 * @property {string} countryName
 * @property {DateTime} [createdAt]
 * @property {string} deviceBrand
 * @property {string} deviceFamily
 * @property {string} deviceModel
 * @property {string} [id]
 * @property {string} ipAddress
 * @property {string} jSONInput
 * @property {string} mail
 * @property {string} os
 * @property {string} osVersion
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} EditAccountPasswordInputType
 * @property {EncryptedAccountPasswordInputType} [encryptedPassword]
 * @property {string} id
 * @property {string} login
 * @property {string} name
 * @property {string} notes
 * @property {Array<string>} [tags]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} EditAccountTagInputType
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} EditGroupInputType
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} EditSharedPasswordInputType
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} [encryptedPasswords]
 * @property {string} id
 * @property {string} [login]
 * @property {string} modifyLock
 * @property {string} [name]
 * @property {string} [notes]
 * @property {Array<string>} [tags]
 * @property {string} [websiteURL]
 */

/**
 * @typedef {Object} EditSharedTagInputType
 * @property {string} groupId
 * @property {string} id
 * @property {string} name
 */

/**
 * @typedef {Object} EditUserGroupType
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {string} userId
 */

/**
 * @typedef {Object} EditUserInputType
 * @property {string} [email]
 * @property {string} [encryptedLoginPassword]
 * @property {string} [firstname]
 * @property {string} [foreignObjectId]
 * @property {string} [id]
 * @property {string} [lastname]
 * @property {string} [securityQuestion1]
 * @property {string} [securityQuestion2]
 * @property {string} [securityQuestion3]
 */

/**
 * @typedef {Object} EncryptedAccountPasswordInputType
 * @property {string} encryptedPasswordString
 * @property {string} keyPairId
 */

/**
 * @typedef {Object} EncryptedSharedPasswordInputType
 * @property {string} encryptedPasswordString
 * @property {string} keyPairId
 * @property {string} [sharedPasswordId]
 */

/**
 * @typedef {Object} GroupInputType
 * @property {string} name
 * @property {string} [parentGroupId]
 */

/**
 * @typedef {Object} GroupMutations
 * @property {GroupType} [add]
 * @property {UserType} [addUser]
 * @property {Array<(GroupType|null|undefined)>} [archive]
 * @property {Array<(GroupType|null|undefined)>} [delete]
 * @property {UserType} [deleteUser]
 * @property {GroupType} [edit]
 * @property {UserType} [editUser]
 * @property {GroupType} [move]
 */

/**
 * @typedef {("GROUP_ADMIN"|"GROUP_EDITOR"|"GROUP_VIEWER")} GroupRole
 */

/**
 * @typedef {Object} GroupType
 * @property {DateTime} [archivedAt]
 * @property {Array<(GroupType|null|undefined)>} [children]
 * @property {number} [countPasswords]
 * @property {number} [countSubGroups]
 * @property {number} [countTags]
 * @property {number} [countUsers]
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} name
 * @property {string} [parentGroupId]
 * @property {Array<(SharedPasswordType|null|undefined)>} [passwords]
 * @property {Array<(SharedTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserType|null|undefined)>} [users]
 * @property {Array<(UserType|null|undefined)>} [usersWithAccess]
 */

/**
 * @typedef {*} Guid
 */

/**
 * @typedef {Object} KeyPairChangeLogType
 * @property {DateTime} [createdAt]
 * @property {string} encryptedPrivateKey
 * @property {string} [id]
 * @property {string} [previousPasswordHashEncryptedWithPublicKey]
 * @property {string} publicKeyString
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} KeyPairType
 * @property {DateTime} [createdAt]
 * @property {string} encryptedPrivateKey
 * @property {string} [id]
 * @property {string} publicKeyString
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {("Company"|"Group"|"Private")} KindOfPassword
 */

/**
 * @typedef {("NONE"|"STANDARD"|"TRIAL")} LicenceStatus
 */

/**
 * @typedef {*} Long
 */

/**
 * @typedef {Object} MandatorMutations
 * @property {MandatorType} [delete]
 * @property {MandatorType} [edit]
 */

/**
 * @typedef {Object} MandatorType
 * @property {DateTime} [createdAt]
 * @property {StripeCustomerType} [customerinformation]
 * @property {string} [id]
 * @property {boolean} isBusinessCustomer
 * @property {string} name
 * @property {LicenceStatus} subscriptionStatus
 * @property {Long} subscriptionUserLimit
 * @property {boolean} twoFactorRequired
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} MoveSharedPasswordToGroupInputType
 * @property {string} destinationGroupId
 * @property {Array<(EncryptedSharedPasswordInputType|null|undefined)>} encryptedPasswords
 * @property {Array<(Guid|null|undefined)>} passwordIds
 */

/**
 * @typedef {Object} OwnUserType
 * @property {boolean} admin
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} email
 * @property {boolean} emailConfirmed
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {string} id
 * @property {KeyPairType} keyPair
 * @property {string} lastname
 * @property {MandatorType} mandator
 * @property {DateTime} [passwordChangedAt]
 * @property {boolean} [passwordReset]
 * @property {PublicKeyType} [publicKey]
 * @property {Array<(WebauthnKeyType|null|undefined)>} [registeredWebAuthnKeys]
 * @property {Sex} sex
 * @property {boolean} superAdmin
 * @property {Array<(TwoFATypes|null|undefined)>} [twoFATypesEnabled]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserGroupType|null|undefined)>} [usergroups]
 */

/**
 * @typedef {Object} PasswordChangeLogType
 * @property {DateTime} [createdAt]
 * @property {UserType} [createdBy]
 * @property {string} [createdById]
 * @property {string} encryptedPasswordString
 * @property {string} [id]
 * @property {string} keyPairId
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} PasswordType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} login
 * @property {string} [modifyLock]
 * @property {string} name
 * @property {string} notes
 * @property {KindOfPassword} [type]
 * @property {DateTime} [updatedAt]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} PublicKeyType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {string} publicKeyString
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} SendScamMailType
 * @property {Array<(string|null|undefined)>} mails
 * @property {string} redirectURL
 * @property {string} senderName
 * @property {string} templateId
 */

/**
 * @typedef {("Female"|"Male"|"Unset")} Sex
 */

/**
 * @typedef {Object} SharedPasswordAccessLogType
 * @property {DateTime} [createdAt]
 * @property {string} [id]
 * @property {DateTime} [updatedAt]
 * @property {UserType} [user]
 * @property {string} userId
 */

/**
 * @typedef {Object} SharedPasswordMutations
 * @property {SharedPasswordType} [add]
 * @property {SharedPasswordType} [bulkSyncEncryptedPassword] - DEPRECATED: Encryptions should be provided within every mutation changing public-keys, password-text or access to passwords. Can still be used if ihe migration did not work, or if something else breaks (just in case).
 * @property {SharedPasswordType} [companyAdd]
 * @property {SharedPasswordType} [companyAddForUser]
 * @property {Array<(SharedPasswordType|null|undefined)>} [companyMoveToShared]
 * @property {SharedPasswordType} [delete]
 * @property {SharedPasswordType} [edit]
 * @property {Array<(SharedPasswordType|null|undefined)>} [moveToGroup]
 * @property {SharedPasswordType} [offlineAdd]
 * @property {Array<(SharedPasswordType|null|undefined)>} [sharedMoveToCompany]
 */

/**
 * @typedef {Object} SharedPasswordType
 * @property {Array<(SharedPasswordAccessLogType|null|undefined)>} [accessLog]
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {UserType} [createdBy]
 * @property {string} [encryptedString]
 * @property {string} [groupId]
 * @property {string} [id]
 * @property {boolean} isSavedAsOfflinePassword
 * @property {string} login
 * @property {string} modifyLock
 * @property {string} name
 * @property {string} notes
 * @property {number} [securityRating]
 * @property {Array<(SharedTagType|null|undefined)>} [tags]
 * @property {DateTime} [updatedAt]
 * @property {string} websiteURL
 */

/**
 * @typedef {Object} SharedTagMutation
 * @property {SharedTagType} [add]
 * @property {SharedTagType} [delete]
 * @property {SharedTagType} [edit]
 */

/**
 * @typedef {Object} SharedTagType
 * @property {string} [color]
 * @property {DateTime} [createdAt]
 * @property {GroupType} [group]
 * @property {string} [id]
 * @property {string} name
 * @property {DateTime} [updatedAt]
 */

/**
 * @typedef {Object} StripeCustomerType
 * @property {string} [defaultPaymentMethodId]
 * @property {string} [email]
 * @property {string} [id]
 * @property {string} [name]
 */

/**
 * @typedef {Object} TotpAuthenticatorKeyType
 * @property {string} authenticatorString
 * @property {string} key
 */

/**
 * @typedef {("TOTP"|"WEB_AUTHN")} TwoFATypes
 */

/**
 * @typedef {Object} TwoFactorMutation
 * @property {OwnUserType} [addWebauthnKey]
 * @property {UserType} [disableTotp]
 * @property {UserType} [enableTotp]
 * @property {TotpAuthenticatorKeyType} [getTotpAuthenticatorKey]
 * @property {WebauthnChallengeType} [getWebauthnChallenge]
 * @property {OwnUserType} [removeWebauthnKey]
 */

/**
 * @typedef {Object} UserGroupType
 * @property {GroupType} [group]
 * @property {string} groupId
 * @property {GroupRole} groupRole
 * @property {UserType} [user]
 * @property {string} userId
 */

/**
 * @typedef {Object} UserMutation
 * @property {UserType} [add]
 * @property {UserType} [archive]
 * @property {UserType} [changeOwnLoginPassword]
 * @property {UserType} [edit]
 * @property {UserType} [grantAdminPermissions]
 * @property {UserType} [resendAccountActivationEmail]
 * @property {UserType} [revokeAdminPermissions]
 * @property {TwoFactorMutation} [twofactor]
 */

/**
 * @typedef {Object} UserType
 * @property {boolean} admin
 * @property {DateTime} [archivedAt]
 * @property {DateTime} [createdAt]
 * @property {string} email
 * @property {boolean} emailConfirmed
 * @property {string} firstname
 * @property {string} [foreignObjectId]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {string} id
 * @property {string} lastname
 * @property {DateTime} [passwordChangedAt]
 * @property {boolean} [passwordReset]
 * @property {PublicKeyType} [publicKey]
 * @property {Sex} sex
 * @property {Array<(TwoFATypes|null|undefined)>} [twoFATypesEnabled]
 * @property {DateTime} [updatedAt]
 * @property {Array<(UserGroupType|null|undefined)>} [usergroups]
 */

/**
 * @typedef {Object} Vaulteron_root_mutation
 * @property {AccountPasswordMutation} [accountPassword]
 * @property {AccountTagMutation} [accountTag]
 * @property {DeviceInformationMutation} [device]
 * @property {GroupMutations} [group]
 * @property {MandatorMutations} [mandator]
 * @property {SharedPasswordMutations} [sharedPassword]
 * @property {SharedTagMutation} [sharedTag]
 * @property {UserMutation} [user]
 */

/**
 * @typedef {Object} Vaulteron_root_query
 * @property {Array<(AccountPasswordType|null|undefined)>} [accountPasswords]
 * @property {Array<(AccountTagType|null|undefined)>} [accountTags]
 * @property {Array<(UserType|null|undefined)>} [admins]
 * @property {string} [billingSecret]
 * @property {Array<(ChangeLogType|null|undefined)>} [changeLogs]
 * @property {Array<(SharedPasswordType|null|undefined)>} [companyPasswords]
 * @property {string} [customerBillingPortalUrl]
 * @property {string} [customerCheckoutUrl]
 * @property {Array<(DeviceInformationType|null|undefined)>} [deviceInformation]
 * @property {string} [encryptedAccountPasswordString]
 * @property {string} [encryptedSharedPasswordString]
 * @property {string} [groupTree]
 * @property {Array<(GroupType|null|undefined)>} [groups]
 * @property {Array<(GroupType|null|undefined)>} [groupsByParent]
 * @property {Array<(KeyPairChangeLogType|null|undefined)>} [keyPairChangeLogs]
 * @property {Array<(MandatorType|null|undefined)>} [mandators]
 * @property {OwnUserType} [me]
 * @property {Array<(PublicKeyType|null|undefined)>} [missingEncryptionsForPassword]
 * @property {Array<(Guid|null|undefined)>} [missingEncryptionsForUser]
 * @property {Array<(SharedPasswordType|null|undefined)>} [offlinePasswords]
 * @property {Array<(PasswordChangeLogType|null|undefined)>} [passwordChangeLogs]
 * @property {Array<(PasswordType|null|undefined)>} [passwordsByURL]
 * @property {string} [pwned]
 * @property {Array<(SharedPasswordType|null|undefined)>} [sharedPasswords]
 * @property {Array<(SharedTagType|null|undefined)>} [sharedTags]
 * @property {Array<(UserType|null|undefined)>} [users]
 */

/**
 * @typedef {Object} WebauthnChallengeType
 * @property {Array<Byte>} challenge
 * @property {string} optionsJson
 */

/**
 * @typedef {Object} WebauthnKeyType
 * @property {DateTime} [createdAt]
 * @property {string} [description]
 * @property {string} id
 */