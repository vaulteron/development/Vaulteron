﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace VaulteronWebserver.Middleware;

public sealed class HttpRequestLoggingMiddleware
{
    private readonly RequestDelegate next;
        private readonly ILogger<HttpRequestLoggingMiddleware> logger;

        public HttpRequestLoggingMiddleware(RequestDelegate next)
    {
        this.next = next;
        logger = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    .AddConsole();
                // .AddEventLog(); // Not supported on kestrel. see github: https://github.com/dotnet/runtime/issues/30796
            })
                .CreateLogger<HttpRequestLoggingMiddleware>();
    }

    [ExcludeFromCodeCoverage]
    public async Task Invoke(HttpContext context)
    {
            logger.LogInformation("Header: {header}", JsonConvert.SerializeObject(context.Request.Headers));

        context.Request.EnableBuffering();
        var body = await new StreamReader(context.Request.Body).ReadToEndAsync();
            logger.LogInformation("Body: {body}", body);
        context.Request.Body.Position = 0;

            logger.LogInformation("Host: {host}", context.Request.Host.Host);
            logger.LogInformation("Client IP: {RemoteIpAddress}", context.Connection.RemoteIpAddress);
        await next(context);
    }
}