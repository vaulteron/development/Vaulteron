﻿using System;
using System.Collections.Generic;
using GraphQL;
using GraphQL.MicrosoftDI;
using GraphQL.Types;
using Microsoft.VisualBasic.CompilerServices;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;
using VaulteronUtilities.Extensions;
using VaulteronUtilities.FilterTypes;
using VaulteronUtilities.Models;
using VaulteronUtilities.Resolver.Interfaces;
using VaulteronUtilities.Types;

namespace VaulteronWebserver.Queries;

public sealed class RootQuery : ObjectGraphType
{
    public RootQuery()
    {
        Name = "Vaulteron_root_query";

        #region users

        Field<ListGraphType<UserType>, List<User>>()
            .Name("users")
            .Argument<StringGraphType>("searchTerm")
            .Argument<ListGraphType<GuidGraphType>>("groupIds")
            .Argument<ListGraphType<GuidGraphType>>("userIds")
            .Argument<ListGraphType<GuidGraphType>>("withoutUserIds")
            .Argument<ListGraphType<GuidGraphType>>("excludeUsersInGroupIds")
            .Argument<BooleanGraphType>("byArchived")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, service) => await service.GetUsersAsync(
                context.GetUserGuid(),
                new UserFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    GroupIds = context.GetArgument<List<Guid>>("groupIds"),
                    UserIds = context.GetArgument<List<Guid>>("userIds"),
                    ExcludeUsersInGroupIds = context.GetArgument<List<Guid>>("excludeUsersInGroupIds"),
                    WithoutUserIds = context.GetArgument<List<Guid>>("withoutUserIds"),
                    ByArchived = context.GetArgument<bool?>("byArchived")
                })
            );

        Field<OwnUserType, User>()
            .Name("me")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, service) => await service.GetUserAsync(context.GetUserGuid()));

        Field<ListGraphType<UserType>, List<User>>()
            .Name("admins")
            .Description("Get all sys-admins of your mandator")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, service) => await service.GetAdminsAsync(context.GetUserGuid()));

        #endregion

        #region Encrypted password strings

        Field<StringGraphType, string>()
            .Name("encryptedAccountPasswordString")
            .Description("If no publicKeyId is supplied the latest from the calling user is used")
            .Argument<NonNullGraphType<GuidGraphType>>("passwordId")
            .Argument<GuidGraphType>("publicKeyId")
            .Resolve()
            .WithScope()
            .WithService<IAccountPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetEncryptedStringForAccountPasswordAsync(
                    context.GetUserGuid(),
                    context.GetArgument<Guid>("passwordId"),
                    context.GetArgument<Guid>("publicKeyId")
                )
            );

        Field<StringGraphType, string>()
            .Name("encryptedSharedPasswordString")
            .Description("For SharedPassword AND CompanyPassword. If no publicKeyId is supplied the latest from the calling user is used")
            .Argument<NonNullGraphType<GuidGraphType>>("passwordId")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetEncryptedStringForSharedPasswordAsync(
                context.GetUserGuid(), context.GetArgument<Guid>("passwordId"))
            );

        #endregion

        #region passwords

        Field<ListGraphType<AccountPasswordType>, List<AccountPassword>>()
            .Name("accountPasswords")
            .Argument<StringGraphType>("searchTerm")
            .Argument<ListGraphType<GuidGraphType>>("tagIds")
            .Argument<BooleanGraphType>("byArchived", "If false active Passwords, if true archived Passwords, if null (or not set to null) active AND archived Passwords are returned")
            .Resolve()
            .WithScope()
            .WithService<IAccountPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetAccountPasswordsAsync(
                context.GetUserGuid(),
                new AccountPasswordFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    TagIds = context.GetArgument<List<Guid>>("tagIds"),
                    ByArchived = context.GetArgument<bool?>("byArchived")
                })
            );

        Field<ListGraphType<SharedPasswordType>, IEnumerable<SharedPassword>>()
            .Name("sharedPasswords")
            .Argument<StringGraphType>("searchTerm")
            .Argument<ListGraphType<GuidGraphType>>("groupIds")
            .Argument<BooleanGraphType>("byArchived",
                "If false active Passwords, if true archived Passwords, if null (or not set to null) active AND archived Passwords are returned")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetGroupPasswordsAsync(
                context.GetUserGuid(),
                new SharedPasswordFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    GroupIds = context.GetArgument<List<Guid>>("groupIds"),
                    ByArchived = context.GetArgument<bool?>("byArchived")
                })
            );

        Field<ListGraphType<SharedPasswordType>, IEnumerable<SharedPassword>>()
            .Name("companyPasswords")
            .Argument<StringGraphType>("searchTerm")
            .Argument<ListGraphType<GuidGraphType>>("createdByIds")
            .Argument<BooleanGraphType>("byArchived",
                "If false active Passwords, if true archived Passwords, if null (or not set to null) active AND archived Passwords are returned")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetCompanyPasswordsAsync(
                context.GetUserGuid(),
                new SharedPasswordFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    CreatedByIds = context.GetArgument<List<Guid>>("createdByIds"),
                    GroupIds = null,
                    ByArchived = context.GetArgument<bool?>("byArchived")
                })
            );

        Field<ListGraphType<SharedPasswordType>, IEnumerable<SharedPassword>>()
            .Name("offlinePasswords")
            .Argument<StringGraphType>("searchTerm")
            .Argument<ListGraphType<GuidGraphType>>("createdByIds")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetOfflinePasswordsAsync(
                context.GetUserGuid(),
                new SharedPasswordFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    CreatedByIds = context.GetArgument<List<Guid>>("createdByIds"),
                    GroupIds = null,
                    ByArchived = null
                }));

        Field<ListGraphType<PasswordType>>()
            .Name("passwordsByURL")
            .Argument<StringGraphType>("url")
            .Resolve()
            .WithScope()
            .WithService<IAllPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetPasswordsByUrlAsync(
                context.GetUserGuid(),
                context.GetArgument<string>("url")));

        Field<ListGraphType<PublicKeyType>, List<KeyPair>>()
            .Name("missingEncryptionsForPassword")
            .Argument<NonNullGraphType<GuidGraphType>>("passwordId")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetMissingEncryptionsForPasswordAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("passwordId")));

        Field<ListGraphType<GuidGraphType>, List<Guid>>()
            .Name("missingEncryptionsForUser")
            .Description("Returns a list of sharedPassword IDs that are not yet encrypted for target userId")
            .Argument<NonNullGraphType<GuidGraphType>>("userId")
            .Resolve()
            .WithScope()
            .WithService<ISharedPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetMissingEncryptionsForUserAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("userId")));

        #endregion

        #region tags

        Field<ListGraphType<AccountTagType>, IEnumerable<AccountTag>>()
            .Name("accountTags")
            .Argument<StringGraphType>("searchTerm")
            .Resolve()
            .WithScope()
            .WithService<IAccountTagResolver>()
            .ResolveAsync(async (context, service) => await service.GetTagsAsync(
                context.GetUserGuid(),
                new AccountTagFilterType { SearchTerm = context.GetArgument<string>("searchTerm") })
            );

        Field<ListGraphType<SharedTagType>, IEnumerable<Tag>>()
            .Name("sharedTags")
            .Argument<StringGraphType>("searchTerm")
            .Argument<GuidGraphType>("groupId")
            .Resolve()
            .WithScope()
            .WithService<ISharedTagResolver>()
            .ResolveAsync(async (context, service) => await service.GetTagsAsync(
                context.GetUserGuid(),
                new SharedTagFilterType
                {
                    SearchTerm = context.GetArgument<string>("searchTerm"),
                    GroupId = context.GetArgument<Guid>("groupId")
                })
            );

        #endregion

        #region groups

        Field<ListGraphType<GroupType>, IEnumerable<Group>>()
            .Name("groups")
            .Argument<ListGraphType<GuidGraphType>>("ids", "Limits the result to contain only groups with one of these IDs.")
            .Argument<StringGraphType>("searchTerm", "The search term (string) to search for. Must match a part of the name.")
            .Argument<BooleanGraphType>("byArchived", "If false active Groups, if true archived Groups, if null(or not set) active AND archived Groups are returned")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, service) => await service.GetGroupsAsync(
                    context.GetUserGuid(),
                    new GroupFilterType
                    {
                        Ids = context.GetArgument<List<Guid>>("ids"),
                        IgnoreParentIdArgument = true,
                        SearchTerm = context.GetArgument<string>("searchTerm"),
                        ParentId = null,
                        ByArchived = context.GetArgument<bool?>("byArchived")
                    }
                )
            );

        Field<ListGraphType<GroupType>, IEnumerable<Group>>()
            .Name("groupsByParent")
            .Argument<ListGraphType<GuidGraphType>>("ids", "Limits the result to contain only groups with one of these IDs.")
            .Argument<StringGraphType>("searchTerm", "The search term (string) to search for. Must match a part of the name.")
            .Argument<GuidGraphType>("parentId",
                "Limits the search to contain only groups that are a child of this parent group. "
                + "A null value or Guid.Empty just means that only groups without a parent (parentId == null) will be fetched. "
                + "If you want to disable this behaviour you should use query{groups ... } !")
            .Argument<BooleanGraphType>("byArchived", "If false active Groups, if true archived Groups, if null(or not set) active AND archived Groups are returned")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, service) => await service.GetGroupsAsync(
                    context.GetUserGuid(),
                    new GroupFilterType
                    {
                        Ids = context.GetArgument<List<Guid>>("ids"),
                        IgnoreParentIdArgument = false,
                        SearchTerm = context.GetArgument<string>("searchTerm"),
                        ParentId = context.GetArgument<Guid>("parentId"),
                        ByArchived = context.GetArgument<bool?>("byArchived")
                    }
                )
            );

        Field<StringGraphType, string>()
            .Name("groupTree")
            .Description("Gets the tree of all groups this user can access")
            .Argument<GroupRoleType>("withRole", "Builds GroupTree for user where he has at least the given role")
            .Argument<BooleanGraphType>("byArchived", "If false active Groups, if true archived Groups, if null(or not set) active AND archived Groups are returned")
            .Resolve()
            .WithScope()
            .WithService<IGroupResolver>()
            .ResolveAsync(async (context, service) => await service.GetGroupTreeJsonAsync(
                    context.GetUserGuid(),
                    context.GetArgument<GroupRole?>("withRole") ?? GroupRole.GroupViewer,
                    context.GetArgument<bool?>("byArchived")
                )
            );

        #endregion

        #region changeLogs

        Field<ListGraphType<ChangeLogType>, IEnumerable<GraphQLChangeLogModel>>()
            .Name("changeLogs")
            .Description("Gets ChangeLogs for given entity")
            .Argument<NonNullGraphType<GuidGraphType>>("entityId")
            .Argument<DateTimeGraphType>("from")
            .Argument<DateTimeGraphType>("to")
            .Argument<StringGraphType>("propertyName")
            .Resolve()
            .WithScope()
            .WithService<IChangeLogResolver>()
            .ResolveAsync(async (context, service) => await service.GetChangesAsync(
                context.GetArgument<Guid>("entityId"),
                context.GetArgument<string>("propertyName"),
                context.GetArgument<DateTime?>("from"),
                context.GetArgument<DateTime?>("to")
            ));

        Field<ListGraphType<KeyPairChangeLogType>, ICollection<KeyPair>>()
            .Name("keyPairChangeLogs")
            .Description("Gets ChangeLogs for this Users KeyPairs")
            .Argument<DateTimeGraphType>("from")
            .Argument<DateTimeGraphType>("to")
            .Resolve()
            .WithScope()
            .WithService<IUserResolver>()
            .ResolveAsync(async (context, service) => await service.GetKeyPairChangeLogsAsync(
                context.GetUserGuid(),
                context.GetArgument<DateTime?>("from"),
                context.GetArgument<DateTime?>("to")
            ));

        Field<ListGraphType<PasswordChangeLogType>, ICollection<EncryptedPassword>>()
            .Name("passwordChangeLogs")
            .Description("Gets ChangeLogs for this Users KeyPairs")
            .Argument<NonNullGraphType<GuidGraphType>>("passwordId")
            .Resolve()
            .WithScope()
            .WithService<IAllPasswordResolver>()
            .ResolveAsync(async (context, service) => await service.GetEncryptedPasswordChangeLogAsync(
                context.GetUserGuid(),
                context.GetArgument<Guid>("passwordId")
            ));

        #endregion

        #region Mandator & Billing

        Field<StringGraphType, string>()
            .Name("customerBillingPortalUrl")
            .Resolve()
            .WithScope()
            .WithService<IMandatorResolver>()
            .ResolveAsync(async (context, service) => await service.GetPaymentProviderCustomerPortalLinkAsync(context.GetUserGuid()));

        Field<StringGraphType, string>()
            .Name("customerCheckoutUrl")
            .Resolve()
            .WithScope()
            .WithService<IMandatorResolver>()
            .ResolveAsync(async (context, service) => await service.GetPaymentProviderCheckoutSessionLinkAsync(context.GetUserGuid()));

        Field<StringGraphType, string>()
            .Name("billingSecret")
            .Resolve()
            .WithScope()
            .WithService<IMandatorResolver>()
            .ResolveAsync(async (context, service) => await service.GetPaymentProviderClientSecretAsync(context.GetUserGuid()));

        Field<ListGraphType<MandatorType>, ICollection<Mandator>>()
            .Name("mandators")
            .Resolve()
            .WithScope()
            .WithService<IMandatorResolver>()
            .ResolveAsync(async (context, service) => await service.GetAllMandatorsAsync(context.GetUserGuid()));

        #endregion

        #region DeviceInformation

        Field<ListGraphType<DeviceInformationType>, ICollection<DeviceInformation>>()
            .Name("deviceInformation")
            .Resolve()
            .WithScope()
            .WithService<IDeviceInformationResolver>()
            .ResolveAsync(async (context, service) => await service.GetDeviceInformationAsync(context.GetUserGuid()));

        #endregion

        #region Haveibeenpwned

        Field<StringGraphType, string>()
            .Name("pwned")
            .Argument<NonNullGraphType<StringGraphType>>("mail")
            .Resolve()
            .WithScope()
            .WithService<IPwnedResolver>()
            .ResolveAsync(async (context, service) => await service.GetBreach(context.GetUserGuid(), context.GetArgument<string>("mail")));

        #endregion
    }
}