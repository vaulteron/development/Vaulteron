﻿namespace VaulteronWebserver.Queries;

public static class MasterDataQueryNames
{
    public const string
        SELF = "Self",
        ADD = "Add",
        EDIT = "Edit",
        DELETE = "Delete",
        ARCHIVE = "Archive";
}