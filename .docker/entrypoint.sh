#!/bin/sh

echo "Replacing config variables with ENV vars"
sed -i "s#VAULTERON_CONFIG_VERSION#$VAULTERON_VERSION#g" /app/ClientApp/build/static/js/main.*.js
echo "Replaced config environment variables"

echo "Starting up Vaulteron Service with Version $VAULTERON_VERSION"
exec "$@"