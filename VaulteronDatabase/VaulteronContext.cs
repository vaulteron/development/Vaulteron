﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase;

public class IdentityUserContext<TUser, TKey> : IdentityUserContext<
    TUser, TKey, IdentityUserClaim<TKey>, IdentityUserLogin<TKey>, IdentityUserToken<TKey>
>
    where TUser : IdentityUser<TKey>
    where TKey : IEquatable<TKey>
{
    public IdentityUserContext(DbContextOptions<VaulteronContext> options) : base(options)
    {
    }
}

public class VaulteronContext : IdentityUserContext<User, Guid>
{
    private ICurrentUserClaimsService CurrentUserService { get; }

    public VaulteronContext(DbContextOptions<VaulteronContext> options, ICurrentUserClaimsService currentUserService) :
        base(options)
    {
        CurrentUserService = currentUserService;
    }

    public DbSet<Mandator> Mandators { get; set; }

    public DbSet<KeyPair> KeyPairs { get; set; }
    public DbSet<AccountTag> AccountTags { get; set; }

    public DbSet<AccountPassword> AccountPasswords { get; set; }
    public DbSet<EncryptedAccountPassword> EncryptedAccountPasswords { get; set; }
    public DbSet<AccountPasswordAccessLog> AccountPasswordAccessLogs { get; set; }
    public DbSet<AccountPasswordTag> AccountPasswordTags { get; set; }

    public DbSet<Group> Groups { get; set; }
    public DbSet<UserGroup> UserGroups { get; set; }
    public DbSet<SharedTag> SharedTags { get; set; }

    public DbSet<SharedPassword> SharedPasswords { get; set; }
    public DbSet<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
    public DbSet<SharedPasswordTag> SharedPasswordTags { get; set; }
    public DbSet<SharedPasswordAccessLog> SharedPasswordAccessLogs { get; set; }
    public DbSet<ChangeLog> ChangeLogs { get; set; }
    public DbSet<DeviceInformation> DeviceInformations { get; set; }

    public DbSet<WebauthnPublicKeys> WebauthnPublicKeys { get; set; }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.UseCollation("utf8mb4_general_ci");
        builder.UseGuidCollation("utf8mb4_general_ci");

        #region Tags

        builder.Ignore<Tag>();

        builder.Entity<AccountPasswordTag>().HasKey(pt => new { pt.TagId, pt.PasswordId });

        builder.Entity<AccountTag>()
            .HasOne(pt => pt.Owner)
            .WithMany(p => p.Tags)
            .HasForeignKey(pt => pt.OwnerId)
            .OnDelete(DeleteBehavior.Restrict);

        builder.Entity<SharedPasswordTag>().HasKey(pt => new { pt.TagId, pt.PasswordId });

        builder.Entity<SharedTag>()
            .HasOne(st => st.Group)
            .WithMany(g => g.Tags)
            .HasForeignKey(st => st.GroupId)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region Passwords

        builder.Ignore<Password>();

        builder.Entity<AccountPassword>()
            .HasOne(m => m.Mandator)
            .WithMany(m => m.AccountPasswords)
            .HasForeignKey(pt => pt.MandatorId)
            .OnDelete(DeleteBehavior.Restrict);

        builder.Entity<SharedPassword>()
            .HasOne(m => m.Mandator)
            .WithMany(m => m.SharedPasswords)
            .HasForeignKey(pt => pt.MandatorId)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region AccessLog

        builder.Entity<SharedPasswordAccessLog>()
            .HasOne(pt => pt.User)
            .WithMany(p => p.AccessLogs)
            .HasForeignKey(pt => pt.UserId)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region Group

        builder.Entity<Group>()
            .HasOne(x => x.ParentGroup)
            .WithMany(x => x.ChildGroups)
            .HasForeignKey(x => x.ParentGroupId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region UserGroup

        builder.Entity<UserGroup>().HasKey(pt => new { pt.UserId, pt.GroupId });

        builder.Entity<UserGroup>()
            .HasOne(pt => pt.Group)
            .WithMany(p => p.UserGroups)
            .HasForeignKey(pt => pt.GroupId)
            .OnDelete(DeleteBehavior.Cascade);

        builder.Entity<UserGroup>()
            .HasOne(pt => pt.User)
            .WithMany(t => t.UserGroups)
            .HasForeignKey(pt => pt.UserId)
            .OnDelete(DeleteBehavior.Cascade);

        #endregion

        #region User

        builder.Entity<User>()
            .HasMany(u => u.AccountPasswords)
            .WithOne(p => p.Owner)
            .HasForeignKey(p => p.OwnerId)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region ChangeLog

        builder.Entity<ChangeLog>()
            .HasOne(hc => hc.CreatedBy)
            .WithMany(u => u.ChangeLogs)
            .HasForeignKey(hc => hc.CreatedById)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion


        #region DeviceInformation

        builder.Entity<DeviceInformation>()
            .HasOne(d => d.Client)
            .WithMany(u => u.DeviceInformations)
            .HasForeignKey(d => d.ClientId)
            .OnDelete(DeleteBehavior.Restrict);

        #endregion

        #region Custom Attributes

        foreach (var t in builder.Model.GetEntityTypes())
        {
            foreach (var prop in t.GetProperties())
            {
                // Insert property based behaviour here

                if (PreventUpdateAttribute.IsDefined(t.ClrType) || PreventUpdateAttribute.IsDefined(prop.PropertyInfo))
                {
                    builder.Entity(t.ClrType)
                        .Property(prop.ClrType, prop.Name).Metadata
                        .SetAfterSaveBehavior(PropertySaveBehavior.Throw);
                }
            }

            // Insert class based behaviour here
        }

        #endregion
    }

    public override int SaveChanges()
    {
        CheckChangedEntities();
        return base.SaveChanges();
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        CheckChangedEntities();
        return await base.SaveChangesAsync(cancellationToken);
    }

    private void CheckChangedEntities()
    {
        var logTime = DateTime.UtcNow;
        var changedEntities = ChangeTracker.Entries().ToList();

        foreach (var changedEntity in changedEntities)
        {
            var changedEntityType = changedEntity.Entity.GetType();

            SetCreatedAtAndUpdatedAt(logTime, changedEntity, changedEntityType);
            CreateChangeLogs(logTime, changedEntity, changedEntityType);
        }
    }

    private void SetCreatedAtAndUpdatedAt(DateTime logTime, EntityEntry changedEntity, Type changedEntityType)
    {
        if (changedEntityType.IsAssignableTo(typeof(ITimestamp)))
        {
            var entity = (ITimestamp) changedEntity.Entity;

            switch (changedEntity.State)
            {
                case EntityState.Added:
                    entity.CreatedAt = logTime;
                    entity.UpdatedAt = logTime;
                    break;
                case EntityState.Modified:
                    entity.UpdatedAt = logTime;
                    break;
            }
        }
    }

    private void CreateChangeLogs(DateTime logTime, EntityEntry changedEntity, Type changedEntityType)
    {
        if (changedEntity.State == EntityState.Modified)
        {
            CreateChangeLogsForEntity(changedEntity, changedEntityType, logTime);
        }

        if (HistorizeAddRemoveForRelationAttribute.IsDefined(changedEntityType) && (changedEntity.State == EntityState.Added || changedEntity.State == EntityState.Deleted))
        {
            CreateChangeLogForRelationOfEntity(changedEntity, changedEntityType, logTime);
        }
    }

    private void CreateChangeLogsForEntity(EntityEntry changedEntity, Type changedEntityType, DateTime logTime)
    {
        ChangeLogs.AddRange(
            changedEntity.CurrentValues.Properties
                .Where(prop => HistorizeAttribute.IsDefined(prop.PropertyInfo) || HistorizeAttribute.IsDefined(changedEntityType) && !HistorizeExcludeAttribute.IsDefined(prop.PropertyInfo))
                .Select(prop => new ChangeLog
                {
                    CreatedAt = logTime,

                    EntityName = changedEntityType.Name,
                    PropertyName = prop.Name,

                    CreatedById = CurrentUserService.UserId,

                    EntityId = (Guid) changedEntity.CurrentValues["Id"]!,

                    NewValue = changedEntity.CurrentValues[prop.Name]?.ToString(),
                    OldValue = changedEntity.OriginalValues[prop.Name]?.ToString(),
                })
                .Where(cl => cl.OldValue != cl.NewValue));
    }

    private void CreateChangeLogForRelationOfEntity(EntityEntry changedEntity, Type changedEntityType, DateTime logTime)
    {
        var historizeForRelation = HistorizeAddRemoveForRelationAttribute.Get(changedEntityType);

        ChangeLogs.Add(new ChangeLog
        {
            CreatedAt = logTime,

            EntityName = historizeForRelation.ForEntityName,
            PropertyName = historizeForRelation.ForPropertyName,

            CreatedById = CurrentUserService.UserId,
            EntityId = (Guid) changedEntity.CurrentValues[historizeForRelation.ForEntityIdPropertyName]!,

            NewValue = changedEntity.State == EntityState.Added ? changedEntity.CurrentValues[historizeForRelation.ValuePropertyName]?.ToString() : null,
            OldValue = changedEntity.State == EntityState.Deleted ? changedEntity.OriginalValues[historizeForRelation.ValuePropertyName]?.ToString() : null,
        });
    }
}