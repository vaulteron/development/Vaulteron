﻿using System;

namespace VaulteronDatabase;

public interface ICurrentUserClaimsService
{
    Guid UserId { get; }
}