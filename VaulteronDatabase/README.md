﻿# Commands for the DB

Please open a shell in the ./VaulteronDatabase/ folder and execute this command there

    dotnet ef migrations add <insert-name-here> -s ..\VaulteronWebserver\ -c VaulteronContext -o .\Migrations\

If you get the following error message:
> More than one project was found in directory '..\VaulteronWebserver\'. Specify one using its file name. Then please remove all the unnecessary *.csproj files in the VaulteronWebserver folder

A database update/migration is not done via the commands, however it is automatically executed when the server is
started. So no need to do that.

## possible errors when adding a migration

If you add a table and get the error message `Foreign key constraint is incorrectly formed error` then the chances are
high that the character set (collation) of the foreign key and the corresponding primary key do not match.

To identify the error:

1. Open a mysql console and issue the command `SHOW ENGINE INNODB STATUS`
2. Get the result and look for the heading `LATEST FOREIGN KEY ERROR`

You may have to manually change the generated migration file to use the correct collation.

## create seed data

For creating the keypair that goes into the DB use the following code inside your console.

> Requirements: The cryptoManager must be globally exposed onto the window object (see cryptoManager file)
> Consider changing the Login password used in the code below to fit your needs

```
window.globalCryptoManager.createAsynchronousKeyPair().then(kp => {
    let loginPW = "12345";
    let userName = "Mako";
    console.log(kp);
    let result = ""; 
    result += `public const string ${userName}PublicKey = @"${kp.publicKey}";`;
    result += `public const string ${userName}PrivateKey = @"${kp.privateKey}";`;
    window.globalCryptoManager.encryptUsingUserCredentials(kp.privateKey, loginPW).then(privKey => {
        result += `public const string ${userName}EncryptedPrivateKey = @"${privKey}";`;
        console.log(result);
    });
});
```