﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;

namespace VaulteronDatabase.Models;

[HistorizeAddRemoveForRelation(nameof(AccountPassword), nameof(AccountPassword.AccountPasswordTags), nameof(PasswordId), nameof(TagId))]
public class AccountPasswordTag
{
    [Key] public Guid TagId { get; set; }
    [ForeignKey("TagId")] public AccountTag Tag { get; set; }

    [Key] public Guid PasswordId { get; set; }
    [ForeignKey("PasswordId")] public AccountPassword Password { get; set; }
}