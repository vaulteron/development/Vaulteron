﻿using System;

namespace VaulteronDatabase.Models.Base;

public interface ICreatedBy
{
    Guid? CreatedById { get; set; }
    User CreatedBy { get; set; }
}