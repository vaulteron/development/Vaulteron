﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models.Base;

public abstract class Password : Entity
{
    [Required] public string Name { get; set; }

    /// <summary>
    /// This is the username/email field for a login with this password
    /// </summary>
    [Required] public string Login { get; set; }

    public DateTime? ArchivedAt { get; set; }
    public SecurityRating SecurityRating { get; set; }
    public string WebsiteURL { get; set; }
    public string Notes { get; set; }

    public Guid MandatorId { get; set; }
    [ForeignKey("MandatorId")] public Mandator Mandator { get; set; }
}