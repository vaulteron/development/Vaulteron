﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VaulteronDatabase.Models.Base;

public interface IArchivable
{
    [Required] public DateTime? ArchivedAt { get; set; }

    public bool IsArchived() => ArchivedAt != null;

}