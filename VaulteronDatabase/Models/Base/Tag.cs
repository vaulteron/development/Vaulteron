using System.ComponentModel.DataAnnotations;

namespace VaulteronDatabase.Models.Base;

public class Tag : Entity
{
    [Required] public string Name { get; set; }
    public string Color { get; set; }
}