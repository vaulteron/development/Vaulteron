﻿using System;

namespace VaulteronDatabase.Models.Base;

public interface ICreatedAt
{
    DateTime CreatedAt { get; set; }
}