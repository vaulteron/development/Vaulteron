﻿using System;
using VaulteronDatabase.Attributes;

namespace VaulteronDatabase.Models.Base;

[PreventUpdate]
public class EncryptedPassword : Entity, ICreatedBy
{
    public string EncryptedPasswordString { get; set; }

    public Guid KeyPairId { get; set; }
    public KeyPair KeyPair { get; set; }

    public Guid? CreatedById { get; set; }
    public User CreatedBy { get; set; }
}