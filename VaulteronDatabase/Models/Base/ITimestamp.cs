﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VaulteronDatabase.Models.Base;

public interface ITimestamp : ICreatedAt
{
    [Required] public DateTime UpdatedAt { get; set; }
}