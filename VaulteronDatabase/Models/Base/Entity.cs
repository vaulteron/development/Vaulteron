﻿using System;
using System.ComponentModel.DataAnnotations;
using VaulteronDatabase.Attributes;

namespace VaulteronDatabase.Models.Base;

public abstract class Entity : ITimestamp
{
    [Key] public Guid Id { get; set; }
    [PreventUpdate] public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    [HistorizeExclude] public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;

    protected bool Equals(Entity other) => Id.Equals(other.Id);

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((Entity)obj);
    }

    public override int GetHashCode() => Id.GetHashCode();
}