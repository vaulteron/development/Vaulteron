﻿namespace VaulteronDatabase.Models.Enums;

public enum Sex
{
    Unset = 0, // Default when not submitted via graphQL request
    Male,
    Female,
}