﻿using System;

namespace VaulteronDatabase.Models.Enums;

[Flags]
public enum TwoFATypes
{
    TOTP = 0b_0001,
    WebAuthn = 0b_0010,
}