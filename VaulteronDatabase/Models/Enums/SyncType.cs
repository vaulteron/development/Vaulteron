﻿namespace VaulteronDatabase.Models.Enums;

public enum SyncType
{
    None = 0,
    ActiveDirectoryCloud,
    ActiveDirectoryClient,
}