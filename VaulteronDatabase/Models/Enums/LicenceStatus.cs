﻿namespace VaulteronDatabase.Models.Enums;

public enum LicenceStatus
{
    None = 0,
    Trial,
    Standard,
}