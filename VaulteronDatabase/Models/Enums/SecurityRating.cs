﻿namespace VaulteronDatabase.Models.Enums;

public enum SecurityRating
{
    Dangerous = 0,
    Weak = 1,
    Secure = 2,
    VerySecure = 3
}