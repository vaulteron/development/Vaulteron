﻿using System.Runtime.Serialization;

namespace VaulteronDatabase.Models.Enums;

public enum GroupRole
{
    [EnumMember(Value = "GROUP_ADMIN")] GroupAdmin,
    [EnumMember(Value = "GROUP_EDITOR")] GroupEditor,
    [EnumMember(Value = "GROUP_VIEWER")] GroupViewer
}