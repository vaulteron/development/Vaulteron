﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

public class SharedTag : Tag
{
    public Guid GroupId { get; set; }
    [ForeignKey("GroupId")] public Group Group { get; set; }

    public ICollection<SharedPasswordTag> SharedPasswordTags { get; set; }
}