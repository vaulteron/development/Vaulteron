﻿using System;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[PreventUpdate]
public sealed class EncryptedSharedPassword : EncryptedPassword
{
    public Guid SharedPasswordId { get; set; }
    public SharedPassword SharedPassword { get; set; }
}