﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

public class Group : Entity, IArchivable
{
    [Required] public string Name { get; set; }

    [Required] public Guid MandatorId { get; set; }
    [ForeignKey("MandatorId")] public Mandator Mandator { get; set; }

    public Guid? ParentGroupId { get; set; }
    [ForeignKey("ParentGroupId")] public Group ParentGroup { get; set; }
    public DateTime? ArchivedAt { get; set; }
    public ICollection<Group> ChildGroups { get; set; }
    public ICollection<SharedTag> Tags { get; set; }
    public ICollection<UserGroup> UserGroups { get; set; }
    public ICollection<SharedPassword> SharedPasswords { get; set; }

    public bool IsArchived() => ArchivedAt != null;
}