﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;

namespace VaulteronDatabase.Models;

[PreventUpdate]
[HistorizeAddRemoveForRelation(nameof(SharedPassword), nameof(SharedPassword.SharedPasswordTags), nameof(PasswordId), nameof(TagId))]
public sealed class SharedPasswordTag
{
    [Key] public Guid TagId { get; set; }
    [ForeignKey("TagId")] public SharedTag Tag { get; set; }

    [Key] public Guid PasswordId { get; set; }
    [ForeignKey("PasswordId")] public SharedPassword Password { get; set; }
}