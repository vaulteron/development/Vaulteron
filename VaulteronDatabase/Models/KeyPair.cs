﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[PreventUpdate]
public sealed class KeyPair : Entity
{
    public string PublicKeyString { get; set; }
    public string EncryptedPrivateKey { get; set; }

    public string PreviousPasswordHashEncryptedWithPublicKey { get; set; }

    public ICollection<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
    public ICollection<EncryptedAccountPassword> EncryptedAccountPasswords { get; set; }

    [Required] public Guid UserId { get; set; }
    [ForeignKey("UserId")] public User User { get; set; }
}