﻿using System;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[PreventUpdate]
public sealed class EncryptedAccountPassword : EncryptedPassword
{
    public Guid AccountPasswordId { get; set; }
    public AccountPassword AccountPassword { get; set; }
}