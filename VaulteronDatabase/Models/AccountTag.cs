﻿using System;
using System.Collections.Generic;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

public sealed class AccountTag : Tag
{
    public Guid OwnerId { get; set; }

    // [ForeignKey("OwnerId")] 
    public User Owner { get; set; }

    public ICollection<AccountPasswordTag> AccountPasswordTags { get; set; } = new List<AccountPasswordTag>();
}