﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[Historize]
public class SharedPassword : Password, ICreatedBy
{
    public SharedPassword()
    {
    }

    public ICollection<SharedPasswordAccessLog> AccessLogs { get; set; }
    [Required] public string ModifyLock { get; set; }

    public Guid? GroupId { get; set; }
    [ForeignKey("GroupId")] public Group Group { get; set; }

    public Guid? CreatedById { get; set; }
    [ForeignKey("CreatedById")] public User CreatedBy { get; set; }

    public ICollection<SharedPasswordTag> SharedPasswordTags { get; set; }
    public ICollection<EncryptedSharedPassword> EncryptedSharedPasswords { get; set; }
    public bool IsSavedAsOfflinePassword { get; set; }

    public void SetNewModifyLock()
    {
        ModifyLock = Guid.NewGuid().ToString();
    }
}