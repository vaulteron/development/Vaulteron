﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

[Index(nameof(CreatedAt), Name = "OrderByOptimisation_CreatedAt")]
public class SharedPasswordAccessLog : Entity
{
    [Required] public Guid SharedPasswordId { get; set; }
    [ForeignKey("SharedPasswordId")] public SharedPassword SharedPassword { get; set; }

    [Required] public Guid UserId { get; set; }
    [ForeignKey("UserId")] public User User { get; set; }

    [Required] public Agent Agent { get; set; }
}