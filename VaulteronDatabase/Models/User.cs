﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

public class User : IdentityUser<Guid>, ITimestamp, IArchivable
{
    [PreventUpdate] public DateTime CreatedAt { get; set; }
    [HistorizeExclude] public DateTime UpdatedAt { get; set; }
    public string Firstname { get; set; }
    public string Lastname { get; set; }
    public Sex Sex { get; set; }
    public bool Admin { get; set; }
    public DateTime? ArchivedAt { get; set; }
    public DateTime PasswordChangedAt { get; set; }

    public Guid MandatorId { get; set; }

    // [ForeignKey("MandatorId")]
    public Mandator Mandator { get; set; }

    public Guid? ForeignObjectId { get; set; } // For Example ActiveDirectory ObjectId

    public ICollection<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    public ICollection<AccountTag> Tags { get; set; } = new List<AccountTag>();
    public ICollection<AccountPassword> AccountPasswords { get; set; } = new List<AccountPassword>();
    public ICollection<SharedPasswordAccessLog> AccessLogs { get; set; } = new List<SharedPasswordAccessLog>();
    public ICollection<KeyPair> KeyPairs { get; set; } = new List<KeyPair>();

    public ICollection<DeviceInformation> DeviceInformations { get; set; } = new List<DeviceInformation>();

    public TwoFATypes TwoFATypesEnabled { get; set; }
    public ICollection<WebauthnPublicKeys> WebautnCredentials { get; set; }
    public ICollection<ChangeLog> ChangeLogs { get; set; }

    public string EncryptedLoginPassword { get; set; }
    public string SecurityQuestion1 { get; set; }
    public string SecurityQuestion2 { get; set; }
    public string SecurityQuestion3 { get; set; }

    public void MarkPasswordChange() => PasswordChangedAt = DateTime.UtcNow;
    public bool IsArchived() => ArchivedAt != null;
}