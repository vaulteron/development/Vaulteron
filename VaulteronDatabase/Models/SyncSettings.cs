﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

[Table("Mandators")]
public class SyncSettings : Entity
{
    [ForeignKey("Id")]
    public Mandator Mandator { get; set; }

    [Required] public SyncType SyncType { get; set; }

    [Required]
    [Column("SyncSSLCertificate")]
    public byte[] SSLCertificate { get; set; }

    [Column("SyncDueAt")] public DateTime? DueAt { get; set; }
    [Column("SyncInterval")] public TimeSpan? Interval { get; set; }

    [Required] [Column("SyncLocation")] public string Location { get; set; }

    [Required] [Column("SyncUserName")] public string UserName { get; set; }

    [Required]
    [Column("SyncUserPassword")]
    public string UserPassword { get; set; }

    [Required] [Column("SyncBaseOU")] public string BaseOU { get; set; }

    [Required]
    [Column("SyncAdminGroupCN")]
    public string AdminGroupCN { get; set; }

    [Required] [Column("SyncUseSSLPort")] public bool UseSSLPort { get; set; }
}