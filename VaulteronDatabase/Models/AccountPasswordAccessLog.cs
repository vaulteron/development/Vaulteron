﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

public class AccountPasswordAccessLog : Entity
{
    [Required] public Guid AccountPasswordId { get; set; }
    [ForeignKey("AccountPasswordId")] public AccountPassword AccountPassword { get; set; }

    [Required] public Agent Agent { get; set; }
}