﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[Historize]
public sealed class AccountPassword : Password
{
    public Guid OwnerId { get; set; }
    [ForeignKey("OwnerId")] public User Owner { get; set; }
    public ICollection<AccountPasswordTag> AccountPasswordTags { get; set; }
    public ICollection<EncryptedAccountPassword> EncryptedAccountPasswords { get; set; }
    public ICollection<AccountPasswordAccessLog> AccountPasswordAccessLogs { get; set; }
}