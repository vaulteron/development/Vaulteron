﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VaulteronDatabase.Models.Base;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

public class Mandator : Entity
{
    [Required] public string Name { get; set; }

    public string InternalDescription { get; set; }

    [Required] public bool TwoFactorRequired { get; set; }

    public ICollection<User> Users { get; set; }

    public ICollection<Group> Groups { get; set; }

    public ICollection<SharedPassword> SharedPasswords { get; set; }
    public ICollection<AccountPassword> AccountPasswords { get; set; }

    public SyncSettings SyncSettings { get; set; }

    public string PaymentProviderId { get; set; }
    public string PaymentProviderClientSecret { get; set; }

    public long SubscriptionUserLimit { get; set; }
    public LicenceStatus SubscriptionStatus { get; set; }
    public bool IsBusinessCustomer { get; set; }
}