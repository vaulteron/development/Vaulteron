﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Attributes;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

[PreventUpdate]
public class ChangeLog : ICreatedAt
{
    [Key] public Guid Id { get; set; }

    public Guid CreatedById { get; set; }
    [ForeignKey("CreatedById")] public User CreatedBy { get; set; }

    [Required] public Guid EntityId { get; set; }
    [Required] public string EntityName { get; set; }
    [Required] public string PropertyName { get; set; }

    public string OldValue { get; set; }
    public string NewValue { get; set; }

    public DateTime CreatedAt { get; set; }
}