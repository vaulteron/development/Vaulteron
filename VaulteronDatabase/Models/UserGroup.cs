﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.Models;

public class UserGroup
{
    [Key] public Guid GroupId { get; set; }
    [ForeignKey("GroupId")] public Group Group { get; set; }

    [Key] public Guid UserId { get; set; }
    [ForeignKey("UserId")] public User User { get; set; }

    public GroupRole GroupRole { get; set; }
}