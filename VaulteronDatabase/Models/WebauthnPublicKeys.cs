﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VaulteronDatabase.Models.Base;

namespace VaulteronDatabase.Models;

public class WebauthnPublicKeys : Entity
{
    public Guid UserId { get; set; }
    [ForeignKey("UserId")] public User User { get; set; }
    public byte[] CredentialId { get; set; }
    public byte[] PublicKey { get; set; }
    public uint Counter { get; set; }

    public string Description { get; set; }
}