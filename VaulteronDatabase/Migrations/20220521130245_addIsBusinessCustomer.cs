﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VaulteronDatabase.Migrations
{
    public partial class addIsBusinessCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBusinessCustomer",
                table: "Mandators",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBusinessCustomer",
                table: "Mandators");
        }
    }
}
