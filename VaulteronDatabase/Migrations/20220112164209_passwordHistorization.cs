﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class passwordHistorization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedAccountPasswords_PublicKeys_PublicKeyId",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedSharedPasswords_PublicKeys_PublicKeyId",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropForeignKey(
                name: "FK_PublicKeys_AspNetUsers_UserId",
                table: "PublicKeys");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EncryptedSharedPasswords",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EncryptedAccountPasswords",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PublicKeys",
                table: "PublicKeys");

            migrationBuilder.DropColumn(
                name: "EncryptedPasswordUpdated",
                table: "SharedPasswords");

            migrationBuilder.RenameTable(
                name: "PublicKeys",
                newName: "KeyPairs");

            migrationBuilder.RenameColumn(
                name: "PublicKeyId",
                table: "EncryptedSharedPasswords",
                newName: "KeyPairId");

            migrationBuilder.RenameColumn(
                name: "PublicKeyId",
                table: "EncryptedAccountPasswords",
                newName: "KeyPairId");

            migrationBuilder.RenameIndex(
                name: "IX_PublicKeys_UserId",
                table: "KeyPairs",
                newName: "IX_KeyPairs_UserId");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "EncryptedSharedPasswords",
                type: "char(36)",
                nullable: false,
                defaultValueSql: "UUID()",
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "EncryptedAccountPasswords",
                type: "char(36)",
                nullable: false,
                defaultValueSql: "UUID()",
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "PreviousPasswordHashEncryptedWithPublicKey",
                table: "KeyPairs",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EncryptedSharedPasswords",
                table: "EncryptedSharedPasswords",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EncryptedAccountPasswords",
                table: "EncryptedAccountPasswords",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KeyPairs",
                table: "KeyPairs",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedSharedPasswords_KeyPairId",
                table: "EncryptedSharedPasswords",
                column: "KeyPairId");

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedAccountPasswords_KeyPairId",
                table: "EncryptedAccountPasswords",
                column: "KeyPairId");

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedAccountPasswords_KeyPairs_KeyPairId",
                table: "EncryptedAccountPasswords",
                column: "KeyPairId",
                principalTable: "KeyPairs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedSharedPasswords_KeyPairs_KeyPairId",
                table: "EncryptedSharedPasswords",
                column: "KeyPairId",
                principalTable: "KeyPairs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_KeyPairs_AspNetUsers_UserId",
                table: "KeyPairs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedAccountPasswords_KeyPairs_KeyPairId",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedSharedPasswords_KeyPairs_KeyPairId",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropForeignKey(
                name: "FK_KeyPairs_AspNetUsers_UserId",
                table: "KeyPairs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EncryptedSharedPasswords",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropIndex(
                name: "IX_EncryptedSharedPasswords_KeyPairId",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EncryptedAccountPasswords",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropIndex(
                name: "IX_EncryptedAccountPasswords_KeyPairId",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KeyPairs",
                table: "KeyPairs");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropColumn(
                name: "PreviousPasswordHashEncryptedWithPublicKey",
                table: "KeyPairs");

            migrationBuilder.RenameTable(
                name: "KeyPairs",
                newName: "PublicKeys");

            migrationBuilder.RenameColumn(
                name: "KeyPairId",
                table: "EncryptedSharedPasswords",
                newName: "PublicKeyId");

            migrationBuilder.RenameColumn(
                name: "KeyPairId",
                table: "EncryptedAccountPasswords",
                newName: "PublicKeyId");

            migrationBuilder.RenameIndex(
                name: "IX_KeyPairs_UserId",
                table: "PublicKeys",
                newName: "IX_PublicKeys_UserId");

            migrationBuilder.AddColumn<DateTime>(
                name: "EncryptedPasswordUpdated",
                table: "SharedPasswords",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.Sql(@"DELETE FROM encryptedsharedpasswords WHERE NOT (SharedPasswordId, PublicKeyId, CreatedAt) IN
                                   (SELECT SharedPasswordId, PublicKeyId, MAX(CreatedAt) AS CreatedAt FROM encryptedsharedpasswords GROUP BY SharedPasswordId, PublicKeyId);");

            migrationBuilder.Sql(@"DELETE FROM encryptedaccountpasswords WHERE NOT (AccountPasswordId, PublicKeyId, CreatedAt) IN
                                   (SELECT AccountPasswordId, PublicKeyId, MAX(CreatedAt) AS CreatedAt FROM encryptedaccountpasswords GROUP BY AccountPasswordId, PublicKeyId);");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EncryptedSharedPasswords",
                table: "EncryptedSharedPasswords",
                columns: new[] { "PublicKeyId", "SharedPasswordId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_EncryptedAccountPasswords",
                table: "EncryptedAccountPasswords",
                columns: new[] { "PublicKeyId", "AccountPasswordId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_PublicKeys",
                table: "PublicKeys",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedAccountPasswords_PublicKeys_PublicKeyId",
                table: "EncryptedAccountPasswords",
                column: "PublicKeyId",
                principalTable: "PublicKeys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedSharedPasswords_PublicKeys_PublicKeyId",
                table: "EncryptedSharedPasswords",
                column: "PublicKeyId",
                principalTable: "PublicKeys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PublicKeys_AspNetUsers_UserId",
                table: "PublicKeys",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
