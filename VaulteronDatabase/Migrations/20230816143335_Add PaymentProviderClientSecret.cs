﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VaulteronDatabase.Migrations
{
    public partial class AddPaymentProviderClientSecret : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentProviderClientSecret",
                table: "Mandators",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentProviderClientSecret",
                table: "Mandators");
        }
    }
}
