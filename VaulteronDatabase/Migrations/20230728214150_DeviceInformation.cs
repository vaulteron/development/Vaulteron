﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VaulteronDatabase.Migrations
{
    public partial class DeviceInformation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeviceInformations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "utf8mb4_general_ci"),
                    ClientId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "utf8mb4_general_ci"),
                    Mail = table.Column<string>(type: "longtext", nullable: false, collation: "utf8mb4_general_ci"),
                    JSONInput = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    IpAddress = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    CountryCode = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    CountryName = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    City = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    CityLatLong = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    Browser = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    BrowserVersion = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    DeviceBrand = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    DeviceModel = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    DeviceFamily = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    Os = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    OsVersion = table.Column<string>(type: "longtext", nullable: true, collation: "utf8mb4_general_ci"),
                    CreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceInformations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceInformations_AspNetUsers_ClientId",
                        column: x => x.ClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("Relational:Collation", "utf8mb4_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceInformations_ClientId",
                table: "DeviceInformations",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceInformations");
        }
    }
}
