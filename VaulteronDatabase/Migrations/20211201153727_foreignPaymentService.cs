﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class foreignPaymentService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "CompanyName",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "PostCode",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "Mandators");

            migrationBuilder.AddColumn<string>(
                name: "InternalDescription",
                table: "Mandators",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "PaymentProviderId",
                table: "Mandators",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<int>(
                name: "SubscriptionStatus",
                table: "Mandators",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "SubscriptionUserLimit",
                table: "Mandators",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InternalDescription",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "PaymentProviderId",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "SubscriptionStatus",
                table: "Mandators");

            migrationBuilder.DropColumn(
                name: "SubscriptionUserLimit",
                table: "Mandators");

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Mandators",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "CompanyName",
                table: "Mandators",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Mandators",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "PostCode",
                table: "Mandators",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Mandators",
                type: "longtext",
                nullable: false,
                collation: "utf8mb4_general_ci");
        }
    }
}
