﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class add_prop_to_sharedPassword_EncryptedPasswordUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EncryptedPasswordUpdated",
                table: "SharedPasswords",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EncryptedPasswordUpdated",
                table: "SharedPasswords");
        }
    }
}
