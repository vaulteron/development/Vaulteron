﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mandators",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    CompanyName = table.Column<string>(nullable: false),
                    Street = table.Column<string>(nullable: false),
                    PostCode = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    TwoFactorRequired = table.Column<bool>(nullable: false),
                    SyncType = table.Column<int>(nullable: true),
                    SyncSSLCertificate = table.Column<byte[]>(nullable: true),
                    SyncDueAt = table.Column<DateTime>(nullable: true),
                    SyncInterval = table.Column<TimeSpan>(nullable: true),
                    SyncLocation = table.Column<string>(nullable: true),
                    SyncUserName = table.Column<string>(nullable: true),
                    SyncUserPassword = table.Column<string>(nullable: true),
                    SyncBaseOU = table.Column<string>(nullable: true),
                    SyncAdminGroupCN = table.Column<string>(nullable: true),
                    SyncUseSSLPort = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mandators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Sex = table.Column<int>(nullable: false),
                    Admin = table.Column<bool>(nullable: false),
                    ArchivedAt = table.Column<DateTime>(nullable: true),
                    PasswordChangedAt = table.Column<DateTime>(nullable: false),
                    MandatorId = table.Column<Guid>(nullable: false),
                    ForeignObjectId = table.Column<Guid>(nullable: true),
                    TwoFATypesEnabled = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Mandators_MandatorId",
                        column: x => x.MandatorId,
                        principalTable: "Mandators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    MandatorId = table.Column<Guid>(nullable: false),
                    ParentGroupId = table.Column<Guid>(nullable: true),
                    ArchivedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_Mandators_MandatorId",
                        column: x => x.MandatorId,
                        principalTable: "Mandators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Groups_Groups_ParentGroupId",
                        column: x => x.ParentGroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountPasswords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Login = table.Column<string>(nullable: false),
                    ArchivedAt = table.Column<DateTime>(nullable: true),
                    SecurityRating = table.Column<int>(nullable: false),
                    WebsiteURL = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    MandatorId = table.Column<Guid>(nullable: false),
                    OwnerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPasswords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountPasswords_Mandators_MandatorId",
                        column: x => x.MandatorId,
                        principalTable: "Mandators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountPasswords_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountTags",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Color = table.Column<string>(nullable: true),
                    OwnerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountTags_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PublicKeys",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    PublicKeyString = table.Column<string>(nullable: true),
                    EncryptedPrivateKey = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicKeys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PublicKeys_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WebauthnPublicKeys",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CredentialId = table.Column<byte[]>(nullable: true),
                    PublicKey = table.Column<byte[]>(nullable: true),
                    Counter = table.Column<uint>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebauthnPublicKeys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WebauthnPublicKeys_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SharedPasswords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Login = table.Column<string>(nullable: false),
                    ArchivedAt = table.Column<DateTime>(nullable: true),
                    SecurityRating = table.Column<int>(nullable: false),
                    WebsiteURL = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    MandatorId = table.Column<Guid>(nullable: false),
                    ModifyLock = table.Column<string>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true),
                    CreatedById = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SharedPasswords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SharedPasswords_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SharedPasswords_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SharedPasswords_Mandators_MandatorId",
                        column: x => x.MandatorId,
                        principalTable: "Mandators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SharedTags",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Color = table.Column<string>(nullable: true),
                    GroupId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SharedTags", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SharedTags_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserGroups",
                columns: table => new
                {
                    GroupId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    GroupRole = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroups", x => new { x.UserId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_UserGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserGroups_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountPasswordAccessLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    AccountPasswordId = table.Column<Guid>(nullable: false),
                    Agent = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPasswordAccessLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountPasswordAccessLogs_AccountPasswords_AccountPasswordId",
                        column: x => x.AccountPasswordId,
                        principalTable: "AccountPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountPasswordTags",
                columns: table => new
                {
                    TagId = table.Column<Guid>(nullable: false),
                    PasswordId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountPasswordTags", x => new { x.TagId, x.PasswordId });
                    table.ForeignKey(
                        name: "FK_AccountPasswordTags_AccountPasswords_PasswordId",
                        column: x => x.PasswordId,
                        principalTable: "AccountPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountPasswordTags_AccountTags_TagId",
                        column: x => x.TagId,
                        principalTable: "AccountTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EncryptedAccountPasswords",
                columns: table => new
                {
                    AccountPasswordId = table.Column<Guid>(nullable: false),
                    PublicKeyId = table.Column<Guid>(nullable: false),
                    EncryptedPasswordString = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EncryptedAccountPasswords", x => new { x.PublicKeyId, x.AccountPasswordId });
                    table.ForeignKey(
                        name: "FK_EncryptedAccountPasswords_AccountPasswords_AccountPasswordId",
                        column: x => x.AccountPasswordId,
                        principalTable: "AccountPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EncryptedAccountPasswords_PublicKeys_PublicKeyId",
                        column: x => x.PublicKeyId,
                        principalTable: "PublicKeys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EncryptedSharedPasswords",
                columns: table => new
                {
                    SharedPasswordId = table.Column<Guid>(nullable: false),
                    PublicKeyId = table.Column<Guid>(nullable: false),
                    EncryptedPasswordString = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EncryptedSharedPasswords", x => new { x.PublicKeyId, x.SharedPasswordId });
                    table.ForeignKey(
                        name: "FK_EncryptedSharedPasswords_PublicKeys_PublicKeyId",
                        column: x => x.PublicKeyId,
                        principalTable: "PublicKeys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EncryptedSharedPasswords_SharedPasswords_SharedPasswordId",
                        column: x => x.SharedPasswordId,
                        principalTable: "SharedPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SharedPasswordAccessLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    SharedPasswordId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Agent = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SharedPasswordAccessLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SharedPasswordAccessLog_SharedPasswords_SharedPasswordId",
                        column: x => x.SharedPasswordId,
                        principalTable: "SharedPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SharedPasswordAccessLog_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SharedPasswordTags",
                columns: table => new
                {
                    TagId = table.Column<Guid>(nullable: false),
                    PasswordId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SharedPasswordTags", x => new { x.TagId, x.PasswordId });
                    table.ForeignKey(
                        name: "FK_SharedPasswordTags_SharedPasswords_PasswordId",
                        column: x => x.PasswordId,
                        principalTable: "SharedPasswords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SharedPasswordTags_SharedTags_TagId",
                        column: x => x.TagId,
                        principalTable: "SharedTags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountPasswordAccessLogs_AccountPasswordId",
                table: "AccountPasswordAccessLogs",
                column: "AccountPasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPasswords_MandatorId",
                table: "AccountPasswords",
                column: "MandatorId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPasswords_OwnerId",
                table: "AccountPasswords",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountPasswordTags_PasswordId",
                table: "AccountPasswordTags",
                column: "PasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountTags_OwnerId",
                table: "AccountTags",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_MandatorId",
                table: "AspNetUsers",
                column: "MandatorId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedAccountPasswords_AccountPasswordId",
                table: "EncryptedAccountPasswords",
                column: "AccountPasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedSharedPasswords_SharedPasswordId",
                table: "EncryptedSharedPasswords",
                column: "SharedPasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_MandatorId",
                table: "Groups",
                column: "MandatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ParentGroupId",
                table: "Groups",
                column: "ParentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicKeys_UserId",
                table: "PublicKeys",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswordAccessLog_SharedPasswordId",
                table: "SharedPasswordAccessLog",
                column: "SharedPasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswordAccessLog_UserId",
                table: "SharedPasswordAccessLog",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswords_CreatedById",
                table: "SharedPasswords",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswords_GroupId",
                table: "SharedPasswords",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswords_MandatorId",
                table: "SharedPasswords",
                column: "MandatorId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedPasswordTags_PasswordId",
                table: "SharedPasswordTags",
                column: "PasswordId");

            migrationBuilder.CreateIndex(
                name: "IX_SharedTags_GroupId",
                table: "SharedTags",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserGroups_GroupId",
                table: "UserGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_WebauthnPublicKeys_UserId",
                table: "WebauthnPublicKeys",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountPasswordAccessLogs");

            migrationBuilder.DropTable(
                name: "AccountPasswordTags");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "EncryptedAccountPasswords");

            migrationBuilder.DropTable(
                name: "EncryptedSharedPasswords");

            migrationBuilder.DropTable(
                name: "SharedPasswordAccessLog");

            migrationBuilder.DropTable(
                name: "SharedPasswordTags");

            migrationBuilder.DropTable(
                name: "UserGroups");

            migrationBuilder.DropTable(
                name: "WebauthnPublicKeys");

            migrationBuilder.DropTable(
                name: "AccountTags");

            migrationBuilder.DropTable(
                name: "AccountPasswords");

            migrationBuilder.DropTable(
                name: "PublicKeys");

            migrationBuilder.DropTable(
                name: "SharedPasswords");

            migrationBuilder.DropTable(
                name: "SharedTags");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Mandators");
        }
    }
}
