﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class addOfflinePropertyToSharedPasswords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSavedAsOfflinePassword",
                table: "SharedPasswords",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSavedAsOfflinePassword",
                table: "SharedPasswords");
        }
    }
}
