﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class accessLogOrderByCreatedAtOptimisation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SharedPasswordAccessLog_AspNetUsers_UserId",
                table: "SharedPasswordAccessLog");

            migrationBuilder.DropForeignKey(
                name: "FK_SharedPasswordAccessLog_SharedPasswords_SharedPasswordId",
                table: "SharedPasswordAccessLog");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SharedPasswordAccessLog",
                table: "SharedPasswordAccessLog");

            migrationBuilder.RenameTable(
                name: "SharedPasswordAccessLog",
                newName: "SharedPasswordAccessLogs");

            migrationBuilder.RenameIndex(
                name: "IX_SharedPasswordAccessLog_UserId",
                table: "SharedPasswordAccessLogs",
                newName: "IX_SharedPasswordAccessLogs_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_SharedPasswordAccessLog_SharedPasswordId",
                table: "SharedPasswordAccessLogs",
                newName: "IX_SharedPasswordAccessLogs_SharedPasswordId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SharedPasswordAccessLogs",
                table: "SharedPasswordAccessLogs",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "OrderByOptimisation_CreatedAt",
                table: "SharedPasswordAccessLogs",
                column: "CreatedAt");

            migrationBuilder.AddForeignKey(
                name: "FK_SharedPasswordAccessLogs_AspNetUsers_UserId",
                table: "SharedPasswordAccessLogs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SharedPasswordAccessLogs_SharedPasswords_SharedPasswordId",
                table: "SharedPasswordAccessLogs",
                column: "SharedPasswordId",
                principalTable: "SharedPasswords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SharedPasswordAccessLogs_AspNetUsers_UserId",
                table: "SharedPasswordAccessLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_SharedPasswordAccessLogs_SharedPasswords_SharedPasswordId",
                table: "SharedPasswordAccessLogs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SharedPasswordAccessLogs",
                table: "SharedPasswordAccessLogs");

            migrationBuilder.DropIndex(
                name: "OrderByOptimisation_CreatedAt",
                table: "SharedPasswordAccessLogs");

            migrationBuilder.RenameTable(
                name: "SharedPasswordAccessLogs",
                newName: "SharedPasswordAccessLog");

            migrationBuilder.RenameIndex(
                name: "IX_SharedPasswordAccessLogs_UserId",
                table: "SharedPasswordAccessLog",
                newName: "IX_SharedPasswordAccessLog_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_SharedPasswordAccessLogs_SharedPasswordId",
                table: "SharedPasswordAccessLog",
                newName: "IX_SharedPasswordAccessLog_SharedPasswordId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SharedPasswordAccessLog",
                table: "SharedPasswordAccessLog",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SharedPasswordAccessLog_AspNetUsers_UserId",
                table: "SharedPasswordAccessLog",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SharedPasswordAccessLog_SharedPasswords_SharedPasswordId",
                table: "SharedPasswordAccessLog",
                column: "SharedPasswordId",
                principalTable: "SharedPasswords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
