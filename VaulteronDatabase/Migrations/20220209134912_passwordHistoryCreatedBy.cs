﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VaulteronDatabase.Migrations
{
    public partial class passwordHistoryCreatedBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChangeLogs_AspNetUsers_UserId",
                table: "ChangeLogs");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ChangeLogs",
                newName: "CreatedById");

            migrationBuilder.RenameIndex(
                name: "IX_ChangeLogs_UserId",
                table: "ChangeLogs",
                newName: "IX_ChangeLogs_CreatedById");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "EncryptedSharedPasswords",
                type: "char(36)",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<Guid>(
                name: "CreatedById",
                table: "EncryptedAccountPasswords",
                type: "char(36)",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedSharedPasswords_CreatedById",
                table: "EncryptedSharedPasswords",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EncryptedAccountPasswords_CreatedById",
                table: "EncryptedAccountPasswords",
                column: "CreatedById");

            migrationBuilder.AddForeignKey(
                name: "FK_ChangeLogs_AspNetUsers_CreatedById",
                table: "ChangeLogs",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedAccountPasswords_AspNetUsers_CreatedById",
                table: "EncryptedAccountPasswords",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EncryptedSharedPasswords_AspNetUsers_CreatedById",
                table: "EncryptedSharedPasswords",
                column: "CreatedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChangeLogs_AspNetUsers_CreatedById",
                table: "ChangeLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedAccountPasswords_AspNetUsers_CreatedById",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropForeignKey(
                name: "FK_EncryptedSharedPasswords_AspNetUsers_CreatedById",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropIndex(
                name: "IX_EncryptedSharedPasswords_CreatedById",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropIndex(
                name: "IX_EncryptedAccountPasswords_CreatedById",
                table: "EncryptedAccountPasswords");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "EncryptedSharedPasswords");

            migrationBuilder.DropColumn(
                name: "CreatedById",
                table: "EncryptedAccountPasswords");

            migrationBuilder.RenameColumn(
                name: "CreatedById",
                table: "ChangeLogs",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_ChangeLogs_CreatedById",
                table: "ChangeLogs",
                newName: "IX_ChangeLogs_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChangeLogs_AspNetUsers_UserId",
                table: "ChangeLogs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
