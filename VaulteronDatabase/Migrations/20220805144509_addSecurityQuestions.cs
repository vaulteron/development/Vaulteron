﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VaulteronDatabase.Migrations
{
    public partial class addSecurityQuestions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EncryptedLoginPassword",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "SecurityQuestion1",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "SecurityQuestion2",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "SecurityQuestion3",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EncryptedLoginPassword",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SecurityQuestion1",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SecurityQuestion2",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SecurityQuestion3",
                table: "AspNetUsers");
        }
    }
}
