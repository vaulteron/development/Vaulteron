﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace VaulteronDatabase.Migrations
{
    public partial class MultipleWebAuthn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "WebauthnPublicKeys",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "WebauthnPublicKeys",
                type: "longtext",
                nullable: true,
                collation: "utf8mb4_general_ci");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "WebauthnPublicKeys",
                type: "datetime(6)",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "WebauthnPublicKeys");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "WebauthnPublicKeys");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "WebauthnPublicKeys");
        }
    }
}
