﻿using System;
using System.Linq;

namespace VaulteronDatabase.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
public class HistorizeAddRemoveForRelationAttribute : Attribute
{
    public HistorizeAddRemoveForRelationAttribute(string forEntityName, string forPropertyName, string forEntityIdPropertyName, string valuePropertyName)
    {
        ForEntityName = forEntityName;
        ForPropertyName = forPropertyName;
        ForEntityIdPropertyName = forEntityIdPropertyName;
        ValuePropertyName = valuePropertyName;
    }

    public string ForEntityName { get; }
    public string ForPropertyName { get; }
    public string ForEntityIdPropertyName { get; }
    public string ValuePropertyName { get; }

    public static HistorizeAddRemoveForRelationAttribute Get(Type type)
    {
        Attribute[] attrs = GetCustomAttributes(type);
        return attrs
            .OfType<HistorizeAddRemoveForRelationAttribute>()
            .Single();
    }

    public static bool IsDefined(Type type)
        => IsDefined(type, typeof(HistorizeAddRemoveForRelationAttribute));
}