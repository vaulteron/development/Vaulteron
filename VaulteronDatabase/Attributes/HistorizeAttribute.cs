﻿using System;
using System.Reflection;

namespace VaulteronDatabase.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, Inherited = false)]
public class HistorizeAttribute : Attribute
{
    public static bool IsDefined(Type type)
        => IsDefined(type, typeof(HistorizeAttribute));

    public static bool IsDefined(PropertyInfo prop)
        => IsDefined(prop, typeof(HistorizeAttribute));
}

[AttributeUsage(AttributeTargets.Property)]
public class HistorizeExcludeAttribute : Attribute
{
    public static bool IsDefined(PropertyInfo prop)
        => IsDefined(prop, typeof(HistorizeExcludeAttribute));
}