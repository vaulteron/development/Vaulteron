﻿using System;
using System.Reflection;

namespace VaulteronDatabase.Attributes;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, Inherited = false)]
public class PreventUpdateAttribute : Attribute
{
    public static bool IsDefined(Type type)
        => IsDefined(type, typeof(PreventUpdateAttribute));
    public static bool IsDefined(PropertyInfo prop)
        => IsDefined(prop, typeof(PreventUpdateAttribute));
}