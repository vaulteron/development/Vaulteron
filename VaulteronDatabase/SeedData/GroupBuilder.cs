﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.SeedData;

public class GroupBuilder
{
    private readonly Group group;
    private readonly ICryptoHelper cryptoHelper;
    private readonly VaulteronContext vaulteronContext;

    public GroupBuilder(ICryptoHelper cryptoHelper, VaulteronContext vaulteronContext, Mandator mandator, string name)
    {
        this.cryptoHelper = cryptoHelper;
        this.vaulteronContext = vaulteronContext;

        group = new Group
        {
            Name = name,
            ParentGroup = null,
            SharedPasswords = new List<SharedPassword>(),
            Tags = new List<SharedTag>(),
            UserGroups = new List<UserGroup>(),
            ChildGroups = new List<Group>(),
            Mandator = mandator
        };
        mandator.Groups.Add(group);
    }

    public GroupBuilder SetParentGroup(Group parentGroup)
    {
        group.ParentGroup = parentGroup;
        return this;
    }

    public GroupBuilder AddArchivedSharedPassword(User creator, string name, string login, string password, params string[] tags)
        => AddSharedPassword(creator, name, login, password, tags, true);

    public GroupBuilder AddSharedPassword(User creator, string name, string login, string password, params string[] tags)
        => AddSharedPassword(creator, name, login, password, tags, false);

    private GroupBuilder AddSharedPassword(User creator, string name, string login, string password, string[] tags, bool archived)
    {
        if (group.UserGroups.Count == 0)
            throw new SeedDataCreationException(
                "Added a sharedPassword without having users in the group does not make any sense");

        var timestampOfCreation = DateTime.UtcNow;

        var usersWithAccess = group.UserGroups.Select(userGroup => userGroup.User).ToList();
        // Add all users from all parent group
        var currentParentGroup = group.ParentGroup;
        while (currentParentGroup != null)
        {
            usersWithAccess.AddRange(currentParentGroup.UserGroups.Select(userGroup => userGroup.User).ToList());
            currentParentGroup = currentParentGroup.ParentGroup;
        }

        // Add all sys-admins as well
        var sysAdmins = vaulteronContext.Users
            .Include(u => u.KeyPairs)
            .Where(u => u.Admin)
            .ToList();
        usersWithAccess.AddRange(sysAdmins);

        // Now add the encryptedPassword for all users, but only once
        var userWithAccessUnique = usersWithAccess
            .GroupBy(u => u.Id)
            .Select(u => u.First())
            .Select(u => u.KeyPairs.Last())
            .ToList();
        var encryptedPasswords = userWithAccessUnique
            .Select(keyPair => new EncryptedSharedPassword
            {
                EncryptedPasswordString = cryptoHelper.EncryptAsymmetrical(password, keyPair.PublicKeyString),
                KeyPair = keyPair,
            })
            .ToList();

        var pw = new SharedPassword
        {
            Name = name,
            Login = login,
            SecurityRating = SecurityRating.Dangerous,
            EncryptedSharedPasswords = encryptedPasswords,
            GroupId = group.Id,
            WebsiteURL = "website.com",
            Notes = "",
            Mandator = group.Mandator,
            CreatedBy = creator,
            ArchivedAt = archived ? timestampOfCreation : null,
            IsSavedAsOfflinePassword = false
        };
        pw.SetNewModifyLock();

        var sharedPasswordTags = new List<SharedPasswordTag>();

        foreach (var t in group.Tags)
        {
            if (tags.Contains(t.Name))
            {
                sharedPasswordTags.Add(new SharedPasswordTag { Tag = t, Password = pw, });
            }
        }

        pw.SharedPasswordTags = sharedPasswordTags;

        group.SharedPasswords.Add(pw);
        group.Mandator.SharedPasswords.Add(pw);
        return this;
    }

    public GroupBuilder AddTags(params string[] tagNames)
    {
        foreach (var tagName in tagNames)
        {
            group.Tags.Add(new SharedTag { Name = tagName, Color = "black" });
        }

        return this;
    }

    public GroupBuilder AddUsers(GroupRole groupRole, params User[] users)
    {
        if (group.SharedPasswords.Count > 0)
            throw new SeedDataCreationException("Please add all users before adding the sharedPasswords");

        foreach (var user in users)
        {
            var userGroup = new UserGroup { Group = group, UserId = user.Id, User = user, GroupRole = groupRole };
            group.UserGroups.Add(userGroup);
        }

        return this;
    }

    public GroupBuilder AddGroupViewers(params User[] users) => AddUsers(GroupRole.GroupViewer, users);
    public GroupBuilder AddGroupEditors(params User[] users) => AddUsers(GroupRole.GroupEditor, users);
    public GroupBuilder AddGroupAdmins(params User[] users) => AddUsers(GroupRole.GroupAdmin, users);
    public GroupBuilder Archive()
    {
        group.ArchivedAt = DateTime.UtcNow;
        return this;
    }

    /// <summary>
    /// Creates the configured Group 
    /// </summary>
    /// <returns>The newly created Group fetched from the DB</returns>
    public Group Build()
    {
        try
        {
            // First remove all User-references
            foreach (var userGroup in group.UserGroups)
                userGroup.User = null;

            // Now persist data
            vaulteronContext.Groups.Add(group);
            vaulteronContext.SaveChanges();
            return group;
        }
        catch (Exception e)
        {
            throw new SeedDataCreationException("Unable to create Group", e);
        }
    }
}