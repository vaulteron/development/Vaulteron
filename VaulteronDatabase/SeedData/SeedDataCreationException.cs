﻿using System;

namespace VaulteronDatabase.SeedData;

public class SeedDataCreationException : Exception
{
    public SeedDataCreationException(string message) : base(message)
    {
    }

    public SeedDataCreationException(string message, Exception innerException) : base(message, innerException)
    {
    }
}