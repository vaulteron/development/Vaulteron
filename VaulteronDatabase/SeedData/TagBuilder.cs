﻿using System.Linq;
using VaulteronDatabase.Models;

namespace VaulteronDatabase.SeedData;

public class TagBuilder
{
    private readonly SharedTag tag;


    public TagBuilder(string name)
    {
        tag = new SharedTag
        {
            Name = name,
            Color = "black"
        };
    }

    public TagBuilder AddToGroup(Group g)
    {
        tag.Group = g;
        return this;
    }

    public SharedTag Build(VaulteronContext vaulteronContext)
    {
        if (tag.Group == null)
            throw new SeedDataCreationException("No group was supplied to tag during seeding data");

        vaulteronContext.SharedTags.Add(tag);
        vaulteronContext.SaveChanges();
        return vaulteronContext.SharedTags.Single(m => m.Name == tag.Name);
    }
}