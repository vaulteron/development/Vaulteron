﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.SeedData;

public class VaulteronSeedData
{
    private readonly VaulteronContext dbContext;
    private readonly UserManager<User> userManager;
    private readonly ICryptoHelper cryptoHelper;

    public const string BritishMandatorName = "HarryPotterMandator"; // British Ministry of Magic
    public const string UsaMandatorName = "MACUSAMandator"; // Magical Congress of the United States of America

    public const string LoginPassword = "12345"; //12345
    // ---------- To create the keys please see the Readme

    public const string // British Ministry Users
        dumbledore = "Dumbledore", // Sys-admin
        doloresUmbridge = "DoloresUmbridge", // Sys-admin, no groups assigned
        minervaMcGonagall = "MinervaMcGonagall", // Head of gryffindor, editor of hogwarts
        godricGryffindor = "GodricGryffindor", // Group-admin of gryffindor
        nearlyHeadlessNick = "NearlyHeadlessNick", // Viewer of hogwarts
        harryPotter = "HarryPotter", // Editor of gryffindor
        ronWeasly = "RonWeasly", // Viewer of gryffindor
        hermioneGranger = "HermioneGranger", // Editor of gryffindor
        pomonaSprout = "PomonaSprout", // Head of hufflepuff, editor of hogwarts
        cedricDiggory = "CedricDiggory", // Editor of hufflepuff
        filiusFlitwick = "FiliusFlitwick", // Head of ravenclaw, editor of hogwarts
        lunaLovegood = "LunaLovegood", // Editor of ravenclaw
        garrickOllivander = "GarrickOllivander", // Viewer of ravenclaw
        severusSnape = "SeverusSnape", // Group-admin of slytherin, group-admin of hogwarts
        dracoMalfoy = "DracoMalfoy", // Editor of slytherin
        horaceSlughorn = "HoraceSlughorn", // Viewer of slytherin
        albusPotter = "AlbusPotter", // Viewer of slytherin
        igorKarkaroff = "IgorKarkaroff", // Sys-admin, group-admin of durmstrang
        viktorKrum = "ViktorKrum", // Editor of durmstrang
        dobby = "Dobby"; // User without Group

    public const string // MACUSA Users
        seraphinaPicquery = "SeraphinaPicquery", // Sysadmin/President of MACUSA 
        percivalGraves = "PercivalGraves", // Auror of MACUSA, Admin of "Department of Aurors"
        porpentinaGoldstein = "PorpentinaGoldstein"; // Auror of MACUSA, Viewer of "Department of Aurors"

    public const string
        hogwarts = "Hogwarts",
        gryffindor = "Gryffindor",
        hufflepuff = "Hufflepuff",
        ravenclaw = "Ravenclaw",
        slytherin = "Slytherin",
        roomOfRequirement = "RoomOfRequirement", // archived Group
        durmstrang = "Durmstrang";

    public const string
        macusaDepartmentOfAurors = "MacusaDepartmentOfAurors"; // DOA of MACUSA

    public const string
        tagGryffindorFireSpells = "tagGryffindorFireSpells",
        tagGryffindorIceSpells = "tagGryffindorIceSpells",
        tagSlytherinFireSpells = "tagSlytherinFireSpells",
        tagSlytherinIceSpells = "tagSlytherinIceSpells";

    public const string
        tagNormalSuffix = "_tag_one",
        tagSpecialSuffix = "-tag:.!?";

    public const string
        unencryptedPasswordString = "superPassword123?!";

    public const string
        sharedPassword = "sharedPassword_",
        accountPassword = "accountPassword_",
        companyPassword = "companyPassword_",
        offlinePassword = "offlinePassword_",
        archived = "_archived",
        one = "_one",
        two = "_two",
        three = "_three";

    public VaulteronSeedData(VaulteronContext dbContext, UserManager<User> userManager, ICryptoHelper cryptoHelper)
    {
        this.dbContext = dbContext;
        this.userManager = userManager;
        this.cryptoHelper = cryptoHelper;
    }

    public void EnsureCreatedAndMigrated()
    {
        if (dbContext.Database.IsMySql())
        {
            dbContext.Database.Migrate();
            CreateTestMandatorIfItDoesNotExist();
            dbContext.SaveChanges();
        }
        else
        {
            dbContext.Database
                .EnsureCreated(); // We called inMemory from xUnit tests which does not support migration
        }
    }

    public (Mandator, Dictionary<string, User>, Dictionary<string, Group>, Dictionary<string, SharedTag>) CreateTestMandatorIfItDoesNotExist()
    {
        var hasTestData = dbContext.Mandators.Any(m => m.Name == BritishMandatorName);
        if (hasTestData) return (null, null, null, null);

        var userCollection = new Dictionary<string, User>();
        var groupCollection = new Dictionary<string, Group>();
        var tagCollection = new Dictionary<string, SharedTag>();

        var mandator = new MandatorBuilder(BritishMandatorName)
            .SetLicense(LicenceStatus.Standard, 25)
            .Build(dbContext);

        #region users

        // Admins
        foreach (var username in new[] { dumbledore, doloresUmbridge })
        {
            userCollection[username] = new UserBuilder(cryptoHelper, mandator, username, true)
                .AddKeyPair(cryptoHelper.CreateKeyPair())
                .AddAccountTags(dumbledore + tagNormalSuffix, dumbledore + tagSpecialSuffix)
                .AddAccountPasswordSimple(accountPassword + username + one, username, unencryptedPasswordString)
                .AddAccountPasswordSimple(accountPassword + username + two, username, unencryptedPasswordString)
                .AddArchivedAccountPasswordSimple(accountPassword + username + archived, accountPassword + username + archived, unencryptedPasswordString)
                .Build(userManager, dbContext);
        }

        // Non-admins
        foreach (var username in new[]
                 {
                     harryPotter, minervaMcGonagall, godricGryffindor, nearlyHeadlessNick, ronWeasly, hermioneGranger, pomonaSprout, cedricDiggory,
                     filiusFlitwick, lunaLovegood, garrickOllivander, severusSnape, dracoMalfoy, horaceSlughorn, igorKarkaroff, viktorKrum, dobby
                 })
        {
            userCollection[username] = new UserBuilder(cryptoHelper, mandator, username)
                .AddKeyPair(cryptoHelper.CreateKeyPair())
                .AddAccountTags(username + tagNormalSuffix, username + tagSpecialSuffix)
                .AddAccountPasswordSimple(accountPassword + username + one, accountPassword + username + one, unencryptedPasswordString)
                .AddAccountPasswordSimple(accountPassword + username + two, accountPassword + username + two, unencryptedPasswordString)
                .AddArchivedAccountPasswordSimple(accountPassword + username + archived, accountPassword + username + archived, unencryptedPasswordString)
                .Build(userManager, dbContext);
        }

        // Archived non-admins
        foreach (var username in new[] { albusPotter })
        {
            userCollection[username] = new UserBuilder(cryptoHelper, mandator, username)
                .AddKeyPair(cryptoHelper.CreateKeyPair())
                .AddAccountTags(username + tagNormalSuffix, username + tagSpecialSuffix)
                .AddAccountPasswordSimple(accountPassword + username + one, accountPassword + username + one, unencryptedPasswordString)
                .AddAccountPasswordSimple(accountPassword + username + two, accountPassword + username + two, unencryptedPasswordString)
                .AddArchivedAccountPasswordSimple(accountPassword + username + archived, accountPassword + username + archived, unencryptedPasswordString)
                .Archive()
                .Build(userManager, dbContext);
        }

        #endregion

        #region groups

        groupCollection[hogwarts] = new GroupBuilder(cryptoHelper, dbContext, mandator, hogwarts)
            .AddGroupAdmins(userCollection[severusSnape])
            .AddGroupEditors(userCollection[minervaMcGonagall], userCollection[pomonaSprout], userCollection[filiusFlitwick])
            .AddGroupViewers(userCollection[nearlyHeadlessNick])
            .AddSharedPassword(userCollection[dumbledore], sharedPassword + hogwarts + one, sharedPassword + hogwarts + one, unencryptedPasswordString)
            .AddSharedPassword(userCollection[dumbledore], sharedPassword + hogwarts + two, sharedPassword + hogwarts + two, unencryptedPasswordString)
            .AddArchivedSharedPassword(userCollection[dumbledore], sharedPassword + hogwarts + archived, sharedPassword + hogwarts + archived, unencryptedPasswordString)
            .Build();

        groupCollection[gryffindor] = new GroupBuilder(cryptoHelper, dbContext, mandator, gryffindor)
            .SetParentGroup(groupCollection[hogwarts])
            .AddGroupAdmins(userCollection[minervaMcGonagall], userCollection[godricGryffindor])
            .AddGroupEditors(userCollection[harryPotter], userCollection[hermioneGranger])
            .AddGroupViewers(userCollection[ronWeasly])
            .AddSharedPassword(userCollection[minervaMcGonagall], sharedPassword + gryffindor + one, sharedPassword + gryffindor + one, unencryptedPasswordString)
            .AddSharedPassword(userCollection[minervaMcGonagall], sharedPassword + gryffindor + two, sharedPassword + gryffindor + two, unencryptedPasswordString)
            .AddSharedPassword(userCollection[minervaMcGonagall], sharedPassword + gryffindor + three, sharedPassword + gryffindor + three, unencryptedPasswordString)
            .AddSharedPassword(userCollection[minervaMcGonagall], "Gryffindor Tower - Fat Lady", "", "Caput Draconis")
            .AddArchivedSharedPassword(userCollection[minervaMcGonagall], sharedPassword + gryffindor + archived, sharedPassword + gryffindor + archived, unencryptedPasswordString)
            .Build();

        groupCollection[hufflepuff] = new GroupBuilder(cryptoHelper, dbContext, mandator, hufflepuff)
            .SetParentGroup(groupCollection[hogwarts])
            .AddGroupAdmins(userCollection[pomonaSprout])
            .AddGroupEditors(userCollection[cedricDiggory])
            .Build();

        groupCollection[ravenclaw] = new GroupBuilder(cryptoHelper, dbContext, mandator, ravenclaw)
            .SetParentGroup(groupCollection[hogwarts])
            .AddGroupAdmins(userCollection[filiusFlitwick])
            .AddGroupEditors(userCollection[lunaLovegood])
            .AddGroupViewers(userCollection[garrickOllivander])
            .AddArchivedSharedPassword(userCollection[lunaLovegood], sharedPassword + ravenclaw + archived + one, sharedPassword + ravenclaw + archived + one, unencryptedPasswordString)
            .AddArchivedSharedPassword(userCollection[filiusFlitwick], sharedPassword + ravenclaw + archived + two, sharedPassword + ravenclaw + archived + two, unencryptedPasswordString)
            .Build();

        groupCollection[slytherin] = new GroupBuilder(cryptoHelper, dbContext, mandator, slytherin)
            .SetParentGroup(groupCollection[hogwarts])
            .AddGroupAdmins(userCollection[severusSnape])
            .AddGroupEditors(userCollection[dracoMalfoy])
            .AddGroupViewers(userCollection[albusPotter], userCollection[horaceSlughorn])
            .AddSharedPassword(userCollection[dracoMalfoy], "slytherin entrance password", "sly", "slyTheFox")
            .Build();

        groupCollection[roomOfRequirement] = new GroupBuilder(cryptoHelper, dbContext, mandator, roomOfRequirement)
            .SetParentGroup(groupCollection[hogwarts])
            .AddGroupAdmins(userCollection[harryPotter])
            .AddGroupEditors(userCollection[hermioneGranger], userCollection[ronWeasly])
            .AddGroupViewers(userCollection[lunaLovegood], userCollection[cedricDiggory], userCollection[garrickOllivander])
            .AddSharedPassword(userCollection[hermioneGranger], sharedPassword + roomOfRequirement + one, sharedPassword + roomOfRequirement + one, unencryptedPasswordString)
            .AddArchivedSharedPassword(userCollection[harryPotter], sharedPassword + roomOfRequirement + archived, sharedPassword + roomOfRequirement + archived, unencryptedPasswordString)
            .Archive()
            .Build();

        groupCollection[durmstrang] = new GroupBuilder(cryptoHelper, dbContext, mandator, durmstrang)
            .AddGroupAdmins(userCollection[igorKarkaroff])
            .AddGroupEditors(userCollection[viktorKrum])
            .Build();

        #endregion

        #region company passwords

        new CompanyPasswordBuilder(cryptoHelper, dbContext, mandator)
            .AddSimpleCompanyPassword(userCollection[dumbledore], companyPassword + dumbledore + one)
            .AddSimpleCompanyPassword(userCollection[dumbledore], companyPassword + dumbledore + two)
            .AddSimpleArchivedCompanyPassword(userCollection[dumbledore], companyPassword + dumbledore + one + archived)
            .AddSimpleCompanyPassword(userCollection[harryPotter], companyPassword + harryPotter + one)
            .AddSimpleArchivedCompanyPassword(userCollection[harryPotter], companyPassword + harryPotter + one + archived)
            .AddSimpleCompanyPassword(userCollection[ronWeasly], companyPassword + ronWeasly + one)
            .AddSimpleArchivedCompanyPassword(userCollection[lunaLovegood], companyPassword + lunaLovegood + archived)
            .AddSyncedOfflinePassword(userCollection[dumbledore], offlinePassword + dumbledore + one)
            .AddSyncedOfflinePassword(userCollection[dumbledore], offlinePassword + dumbledore + two)
            .AddSyncedOfflinePassword(userCollection[harryPotter], offlinePassword + harryPotter + one)
            .AddSyncedOfflinePassword(userCollection[lunaLovegood], offlinePassword + lunaLovegood + one)
            .Build();

        #endregion

        #region tags

        tagCollection[tagGryffindorFireSpells] = new TagBuilder(tagGryffindorFireSpells)
            .AddToGroup(groupCollection[gryffindor])
            .Build(dbContext);
        tagCollection[tagGryffindorIceSpells] = new TagBuilder(tagGryffindorIceSpells)
            .AddToGroup(groupCollection[gryffindor])
            .Build(dbContext);
        tagCollection[tagSlytherinFireSpells] = new TagBuilder(tagSlytherinFireSpells)
            .AddToGroup(groupCollection[slytherin])
            .Build(dbContext);
        tagCollection[tagSlytherinIceSpells] = new TagBuilder(tagSlytherinIceSpells)
            .AddToGroup(groupCollection[slytherin])
            .Build(dbContext);

        #endregion

        #region Magical Congress of the USA (macusa)

        var magicalCongressUSA = new MandatorBuilder(UsaMandatorName)
            .SetLicense(LicenceStatus.Trial, 5)
            .Build(dbContext);

        userCollection[seraphinaPicquery] = new UserBuilder(cryptoHelper, magicalCongressUSA, seraphinaPicquery, true)
            .AddKeyPair(cryptoHelper.CreateKeyPair())
            .Build(userManager, dbContext);

        userCollection[percivalGraves] = new UserBuilder(cryptoHelper, magicalCongressUSA, percivalGraves, false)
            .AddKeyPair(cryptoHelper.CreateKeyPair())
            .Build(userManager, dbContext);

        userCollection[porpentinaGoldstein] = new UserBuilder(cryptoHelper, magicalCongressUSA, porpentinaGoldstein, false)
            .AddKeyPair(cryptoHelper.CreateKeyPair())
            .Build(userManager, dbContext);

        groupCollection[macusaDepartmentOfAurors] = new GroupBuilder(cryptoHelper, dbContext, magicalCongressUSA, macusaDepartmentOfAurors)
            .AddGroupAdmins(userCollection[percivalGraves])
            .AddGroupViewers(userCollection[porpentinaGoldstein])
            .AddSharedPassword(userCollection[percivalGraves], "DoA HQ", "", unencryptedPasswordString,
                               macusaDepartmentOfAurors + tagNormalSuffix, macusaDepartmentOfAurors + tagSpecialSuffix)
            .Build();

        tagCollection[macusaDepartmentOfAurors + tagNormalSuffix] =
            new TagBuilder(macusaDepartmentOfAurors + tagNormalSuffix)
                .AddToGroup(groupCollection[macusaDepartmentOfAurors])
                .Build(dbContext);
        tagCollection[macusaDepartmentOfAurors + tagSpecialSuffix] =
            new TagBuilder(macusaDepartmentOfAurors + tagSpecialSuffix)
                .AddToGroup(groupCollection[macusaDepartmentOfAurors])
                .Build(dbContext);

        #endregion

        return (mandator, userCollection, groupCollection, tagCollection);
    }
}