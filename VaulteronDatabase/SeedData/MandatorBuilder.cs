﻿using System.Collections.Generic;
using System.Linq;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.SeedData;

public class MandatorBuilder
{
    private readonly Mandator mandator;

    public MandatorBuilder(string name)
    {
        mandator = new Mandator
        {
            Name = name,
            InternalDescription = "SeedData Mandator: " + name,
            Groups = new List<Group>(),
            Users = new List<User>(),
            AccountPasswords = new List<AccountPassword>(),
            SharedPasswords = new List<SharedPassword>(),
                IsBusinessCustomer = true
        };
    }

    /// <summary>
    /// Creates the configured Mandator. 
    /// </summary>
    /// <param name="vaulteronContext"></param>
    /// <returns>The newly created Mandator fetched from the DB</returns>
    public Mandator Build(VaulteronContext vaulteronContext)
    {
        vaulteronContext.Mandators.Add(mandator);
        vaulteronContext.SaveChanges();
        return vaulteronContext.Mandators.Single(m => m.Name == mandator.Name);
    }

    public MandatorBuilder SetLicense(LicenceStatus status, long userLimit)
    {
        mandator.SubscriptionStatus = status;
        mandator.SubscriptionUserLimit = userLimit;
        return this;
    }
}