﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.SeedData;

public class UserBuilder
{
    private readonly User user;
    private bool shouldEmailBeConfirmed;
    private bool userShouldBeArchived;
    private readonly ICryptoHelper cryptoHelper;

    public UserBuilder(ICryptoHelper cryptoHelper, Mandator mandator, string firstname, string lastname, string email, Sex sex, bool isAdmin = false)
    {
        this.cryptoHelper = cryptoHelper;

        var now = DateTime.UtcNow;
        user = new User
        {
            UserName = email,
            NormalizedUserName = email.ToUpper(),
            NormalizedEmail = email.ToUpper(),
            Email = email,
            Firstname = firstname,
            Lastname = lastname,
            AccountPasswords = new List<AccountPassword>(),
            Sex = sex,
            Admin = isAdmin,
            Tags = new List<AccountTag>(),
            KeyPairs = new List<KeyPair>(),
            Mandator = mandator,
            MandatorId = mandator.Id,
            PasswordChangedAt = now
        };
    }

    public UserBuilder(ICryptoHelper cryptoHelper, Mandator mandator, string name, bool isAdmin = false)
        : this(cryptoHelper, mandator, name, name, $"{name}@vaulteron.com", Sex.Unset, isAdmin)
    {
    }

    private UserBuilder AddKeyPair(string publicKeyString, string privateKeyEncryptedWithLoginPassword)
    {
        var kp = new KeyPair
        {
            User = user,
            PublicKeyString = publicKeyString,
            EncryptedPrivateKey = privateKeyEncryptedWithLoginPassword
        };

        user.KeyPairs.Add(kp);
        return this;
    }

    public UserBuilder AddKeyPair(RSAKeyPair rsaKeyPair)
    {
        return AddKeyPair(rsaKeyPair.PublicKey, rsaKeyPair.EncryptedPrivateKey);
    }

    public UserBuilder AddAccountTags(params string[] names)
    {
        foreach (var name in names)
        {
            var tag = new AccountTag
            {
                Name = name, Color = "black",
                AccountPasswordTags = new List<AccountPasswordTag>(),
                Owner = user,
            };
            user.Tags.Add(tag);
        }

        return this;
    }

    public UserBuilder AddAccountPasswordSimple(string name, string login, string password, params string[] tags)
    {
        return AddAccountPassword(name, login, password, "", "", SecurityRating.Secure, tags);
    }
    public UserBuilder AddArchivedAccountPasswordSimple(string name, string login, string password, params string[] tags)
    {
        return AddAccountPassword(name, login, password, "", "", SecurityRating.Secure, tags, true);
    }
    public UserBuilder AddAccountPassword(string name, string login, string password, string websiteUrl,
        string notes, SecurityRating securityRating, string[] tags, bool archived = false)
    {
        var keyPair = user.KeyPairs.Last();
        var encryptedPassword = cryptoHelper.EncryptAsymmetrical(password, keyPair.PublicKeyString);

        #region crypto check - decrypt it to verify encryption worked correctly

        var decrypted = cryptoHelper.DecryptAsymmetrical(encryptedPassword, VaulteronSeedData.LoginPassword, keyPair.EncryptedPrivateKey);
        if (password != decrypted)
            throw new SeedDataCreationException("The encryption failed for seed-data");

        #endregion

        var accountPassword = new AccountPassword
        {
            Name = name,
            Login = login,
            SecurityRating = securityRating,
            EncryptedAccountPasswords = new List<EncryptedAccountPassword>
            {
                new()
                {
                    EncryptedPasswordString = encryptedPassword,
                    KeyPair = keyPair
                }
            },
            Owner = user,
            WebsiteURL = websiteUrl ?? "",
            Notes = notes ?? "",
            AccountPasswordTags = new List<AccountPasswordTag>(),
            MandatorId = user.Mandator.Id,
            ArchivedAt = archived ? DateTime.UtcNow : null
        };

        foreach (var tagNameToAdd in tags)
        {
            AccountTag userTag;
            if (!user.Tags.Select(t => t.Name).Contains(tagNameToAdd))
            {
                userTag = new AccountTag { Color = "black", Name = tagNameToAdd };
                user.Tags.Add(userTag);
            }
            else
            {
                userTag = user.Tags.ToList().Find(t => t.Name == tagNameToAdd);
                if (userTag == null)
                    throw new SeedDataCreationException("Unable to add new Tag to the user: " + tagNameToAdd);
            }

            var accountPasswordTag = new AccountPasswordTag { Tag = userTag, Password = accountPassword };
            accountPassword.AccountPasswordTags.Add(accountPasswordTag);
            userTag.AccountPasswordTags.Add(accountPasswordTag);
        }

        user.AccountPasswords.Add(accountPassword);

        return this;
    }

    public UserBuilder ConfirmEmail()
    {
        shouldEmailBeConfirmed = true;
        return this;
    }
    public UserBuilder Archive()
    {
        userShouldBeArchived = true;
        return this;
    }

    /// <summary>
    /// Creates the configured User with the userManager and then returns the newly created User from the DB.
    /// </summary>
    /// <param name="userManager"></param>
    /// <param name="vaulteronContext"></param>
    /// <returns>The newly created User fetched from the DB</returns>
    /// <exception cref="Exception">If waiting for task fails or the userManager throws</exception>
    public User Build(UserManager<User> userManager, VaulteronContext vaulteronContext)
    {
        try
        {
            // UserManager.CreateAsync does not like this and only accepts setting the MandatorId
            user.Mandator = null;

            var task = userManager.CreateAsync(user, cryptoHelper.ComputePasswordHash(VaulteronSeedData.LoginPassword));
            task.Wait();
            if (!task.Result.Succeeded)
                throw new SeedDataCreationException("Unable to create User");

            if (shouldEmailBeConfirmed)
            {
                user.EmailConfirmed = true;
            }

            if (userShouldBeArchived)
            {
                user.ArchivedAt = DateTime.UtcNow;
            }

            vaulteronContext.SaveChanges();

            return vaulteronContext.Users
                .Include(u => u.KeyPairs)
                .Include(u => u.Tags)
                .Include(u => u.AccountPasswords)
                .Single(u => u.Id == user.Id);
        }
        catch (Exception e)
        {
            throw new SeedDataCreationException("Unable to create User:" + e.Message, e);
        }
    }
}