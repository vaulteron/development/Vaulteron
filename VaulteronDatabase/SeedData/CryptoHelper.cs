﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Security;

namespace VaulteronDatabase.SeedData;

public class CryptoHelper : ICryptoHelper
{
    private readonly AESCrypto aesCrypto;
    private readonly RSACryptoBouncy rsaCryptoBouncy;

    private const int generatedKeyPairCount = 200;
    private static Queue<RSAKeyPair> _generatedKeyPairs;

    private Queue<RSAKeyPair> GeneratedKeyPairs {
        get {
            // When null, load from resource
            var assembly = Assembly.GetExecutingAssembly();
            if (_generatedKeyPairs == null)
            {
                using var stream = assembly.GetManifestResourceStream("VaulteronDatabase.SeedData.generatedKeyPairs.json");
                using var streamReader = new StreamReader(stream!);
                try
                {
                    _generatedKeyPairs = JsonConvert.DeserializeObject<Queue<RSAKeyPair>>(streamReader.ReadToEnd());
                }
                catch
                {
                    // Error while reading File, continue with generating KeyPairs
                }
            }

            // Check if we already have enough keypairs
            if (_generatedKeyPairs is { Count: >= generatedKeyPairCount })
                return _generatedKeyPairs;
            
            // When still null or not yet enough generated KeyPairs
            // Generate new KeyPairs
            _generatedKeyPairs = new Queue<RSAKeyPair>(
                Enumerable.Range(0, generatedKeyPairCount - (_generatedKeyPairs?.Count ?? 0))
                    .Select(_ => CreateKeyPair(VaulteronSeedData.LoginPassword, false))
                    .Concat(_generatedKeyPairs ?? new Queue<RSAKeyPair>())
            );
            // Save new KeyPair
            using var streamWriter = new StreamWriter(@"..\VaulteronDatabase\SeedData\generatedKeyPairs.json");
            streamWriter.Write(JsonConvert.SerializeObject(_generatedKeyPairs));

            return _generatedKeyPairs;
        }
    }

    private RSAKeyPair GetNextGeneratedKeyPair()
    {
        var gkp = GeneratedKeyPairs; // Load KeyPairs
        var keyPair = gkp.Dequeue(); // Get next KeyPair
        gkp.Enqueue(keyPair); // Re-queue KeyPair
        return keyPair;
    }

    public CryptoHelper()
    {
        aesCrypto = new AESCrypto();
        rsaCryptoBouncy = new RSACryptoBouncy();
    }

    public string ComputePasswordHash(string value)
    {
        var salt = Encoding.UTF8.GetBytes("vaulteron-global-salt");
        const int iterations = 50000;

        var pbkdf2 = new Rfc2898DeriveBytes(value, salt, iterations, HashAlgorithmName.SHA512);
        return BitConverter.ToString(pbkdf2.GetBytes(64)).Replace("-", "").ToLower();
    }

    #region symmetrical crypto

    public string EncryptSymmetrically(string plaintext, string loginPassword = VaulteronSeedData.LoginPassword)
    {
        return aesCrypto.EncryptString(plaintext, loginPassword);
    }


    public string DecryptSymmetrically(string encryptedText, string loginPassword = VaulteronSeedData.LoginPassword)
    {
        return aesCrypto.DecryptString(encryptedText, loginPassword);
    }

    #endregion

    #region asymmetrical crypto

    public RSAKeyPair CreateKeyPair(string loginPassword = VaulteronSeedData.LoginPassword,
        bool getFromMockList = true)
    {
        if (getFromMockList)
            return GetNextGeneratedKeyPair();

        var asymmetricCipherKeyPair = rsaCryptoBouncy.CreateKeyPair();

        var publicKey = rsaCryptoBouncy.ExportPublicKey(asymmetricCipherKeyPair);
        var jsPublicKey = publicKey
            .Replace("-----BEGIN PUBLIC KEY-----", "-----BEGIN RSA PUBLIC KEY-----")
            .Replace("-----END PUBLIC KEY-----", "-----END RSA PUBLIC KEY-----");

        var privateKey = rsaCryptoBouncy.ExportPrivateKey(asymmetricCipherKeyPair);
        var jsPrivateKey = privateKey
            .Replace("-----BEGIN PRIVATE KEY-----", "-----BEGIN RSA PRIVATE KEY-----")
            .Replace("-----END PRIVATE KEY-----", "-----END RSA PRIVATE KEY-----");

        var passwordHash = ComputePasswordHash(loginPassword);
        var encryptedPrivateKeyString = EncryptSymmetrically(jsPrivateKey, passwordHash);

        return new RSAKeyPair(jsPublicKey, encryptedPrivateKeyString, jsPrivateKey);
    }

    public string EncryptAsymmetrical(string plaintext, string publicKeyString)
    {
        return rsaCryptoBouncy.RsaEncryptWithPublic(plaintext, publicKeyString);
    }

    public string DecryptWithPrivate(string encryptedText, string privateKey)
    {
        return rsaCryptoBouncy.RSADecryptWithPrivate(encryptedText, privateKey);
    }

    public string DecryptAsymmetrical(string encryptedText, string loginPassword,
        string encryptedPrivateKeyString)
    {
        var passwordHash = ComputePasswordHash(loginPassword);
        var privateKeyString = DecryptSymmetrically(encryptedPrivateKeyString, passwordHash);
        return rsaCryptoBouncy.RSADecryptWithPrivate(encryptedText, privateKeyString);
    }

    #endregion

    public string DecryptAsymmetrical(string encryptedText, string privateKey)
    {
        return rsaCryptoBouncy.RSADecryptWithPrivate(encryptedText, privateKey);
    }
}

public class AESCrypto
{
    private const string AlgorithmName = "AES";
    private const int AlgorithmNonceSize = 10;
    private const int AlgorithmKeySize = 256;
    private const int Pbkdf2Iterations = 100_000;
    private const int AlgorithmSaltSize = 16;

    public string EncryptString(string plaintext, string password, byte[] ivBytes, byte[] salt)
    {
        var saltString = BitConverter.ToString(salt).Replace("-", "").ToLower();
        var ivString = BitConverter.ToString(ivBytes).Replace("-", "").ToLower();

        // Var encryptedTextAsBase64 = AESCrypto.Encrypt(ivBytes, salt, plaintext, loginPassword);
        // Return ivString + ";;;" + encryptedTextAsBase64;

        // SecureRandom rand = new SecureRandom();
        // Byte[] salt = new byte[PBKDF2_SALT_SIZE];
        // Rand.NextBytes(salt);

        // Create an instance of PBKDF2 and derive a key.
        Pkcs5S2ParametersGenerator pbkdf2 = new Pkcs5S2ParametersGenerator(new Sha256Digest());
        pbkdf2.Init(Encoding.UTF8.GetBytes(password), salt, Pbkdf2Iterations);
        byte[] key = ((KeyParameter) pbkdf2.GenerateDerivedMacParameters(AlgorithmKeySize)).GetKey();

        // Encrypt and prepend salt.
        var plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
        byte[] ciphertext = Encrypt(ivBytes, plaintextBytes, key);
        return saltString + ivString + Convert.ToBase64String(ciphertext);
        // Byte[] ciphertextAndNonceAndSalt = new byte[salt.Length + ciphertext.Length];
        // Array.Copy(salt, 0, ciphertextAndNonceAndSalt, 0, salt.Length);
        // Array.Copy(ciphertext, 0, ciphertextAndNonceAndSalt, salt.Length, ciphertext.Length);

        // Return as base64 string.
        // Return Convert.ToBase64String(ciphertextAndNonceAndSalt);
    }

    public string EncryptString(string plaintext, string password)
    {
        SecureRandom rand = new SecureRandom();

        byte[] salt = new byte[AlgorithmSaltSize];
        rand.NextBytes(salt);

        byte[] ivBytes = new byte[AlgorithmNonceSize];
        rand.NextBytes(ivBytes);

        return EncryptString(plaintext, password, ivBytes, salt);
    }

    public string DecryptString(byte[] ciphertextBytes, string password, byte[] ivBytes,
        byte[] salt)
    {
        // Return AESCrypto.Decrypt(ivBytes, salt, encryptedTextAsBase64, loginPassword);

        // Retrieve the salt and ciphertextAndNonce.
        // Byte[] salt = new byte[PBKDF2_SALT_SIZE];
        // Byte[] ciphertextAndNonce = new byte[ciphertextAndNonceAndSalt.Length - PBKDF2_SALT_SIZE];
        // Array.Copy(ciphertextAndNonceAndSalt, 0, salt, 0, salt.Length);
        // Array.Copy(ciphertextAndNonceAndSalt, salt.Length, ciphertextAndNonce, 0, ciphertextAndNonce.Length);

        // Create an instance of PBKDF2 and derive a key.
        Pkcs5S2ParametersGenerator pbkdf2 = new Pkcs5S2ParametersGenerator(new Sha256Digest());
        pbkdf2.Init(Encoding.UTF8.GetBytes(password), salt, Pbkdf2Iterations);
        byte[] key = ((KeyParameter) pbkdf2.GenerateDerivedMacParameters(AlgorithmKeySize)).GetKey();

        // Decrypt and return result.
        return Encoding.UTF8.GetString(Decrypt(ivBytes, ciphertextBytes, key));
    }

    public string DecryptString(string base64CiphertextAndNonceAndSalt, string password)
    {
        var saltString = base64CiphertextAndNonceAndSalt.Substring(0, 2 * AlgorithmSaltSize);
        var salt = Enumerable.Range(0, AlgorithmSaltSize)
            .Select(i => Convert.ToByte(saltString.Substring(2 * i, 2), 16))
            .ToArray();

        var ivString = base64CiphertextAndNonceAndSalt.Substring(2 * AlgorithmSaltSize, 2 * AlgorithmNonceSize);
        var ivBytes = Enumerable.Range(0, AlgorithmNonceSize)
            .Select(i => Convert.ToByte(ivString.Substring(2 * i, 2), 16))
            .ToArray();

        var encryptedTextAsBase64 =
            base64CiphertextAndNonceAndSalt.Substring(2 * AlgorithmSaltSize + 2 * AlgorithmNonceSize);
        var ciphertextBytes = Convert.FromBase64String(encryptedTextAsBase64);

        return DecryptString(ciphertextBytes, password, ivBytes, salt);
    }

    private byte[] Encrypt(byte[] nonce, byte[] plaintext, byte[] key)
    {
        // SecureRandom rand = new SecureRandom();
        // Byte[] nonce = new byte[ALGORITHM_NONCE_SIZE];
        // Rand.NextBytes(nonce);

        // Create the cipher instance and initialize.
        GcmBlockCipher cipher = new GcmBlockCipher(new AesEngine());
        KeyParameter keyParam = ParameterUtilities.CreateKeyParameter(AlgorithmName, key);
        ParametersWithIV cipherParameters = new ParametersWithIV(keyParam, nonce);
        cipher.Init(true, cipherParameters);

        // Encrypt and prepend nonce.
        byte[] ciphertext = new byte[cipher.GetOutputSize(plaintext.Length)];
        var length = cipher.ProcessBytes(plaintext, 0, plaintext.Length, ciphertext, 0);
        cipher.DoFinal(ciphertext, length);
        return ciphertext;
    }

    private byte[] Decrypt(byte[] nonce, byte[] ciphertext, byte[] key)
    {
        // Retrieve the nonce and ciphertext.
        // Byte[] nonce = new byte[ALGORITHM_NONCE_SIZE];
        // Byte[] ciphertext = new byte[ciphertextAndNonce.Length - ALGORITHM_NONCE_SIZE];
        // Array.Copy(ciphertextAndNonce, 0, nonce, 0, nonce.Length);
        // Array.Copy(ciphertextAndNonce, nonce.Length, ciphertext, 0, ciphertext.Length);

        // Create the cipher instance and initialize.
        GcmBlockCipher cipher = new GcmBlockCipher(new AesEngine());
        KeyParameter keyParam = ParameterUtilities.CreateKeyParameter(AlgorithmName, key);
        ParametersWithIV cipherParameters = new ParametersWithIV(keyParam, nonce);
        cipher.Init(false, cipherParameters);

        // Decrypt and return result.
        byte[] plaintext = new byte[cipher.GetOutputSize(ciphertext.Length)];
        var length = cipher.ProcessBytes(ciphertext, 0, ciphertext.Length, plaintext, 0);
        cipher.DoFinal(plaintext, length);

        return plaintext;
    }
}

public class RSACryptoBouncy
{
    private const int RSAKeySize = 4096;

    public string ExportPublicKey(AsymmetricCipherKeyPair keyPair)
    {
        var stringWriter = new StringWriter();
        var pemWriter = new PemWriter(stringWriter);
        pemWriter.WriteObject(keyPair.Public);
        pemWriter.Writer.Flush();
        return pemWriter.Writer.ToString();
    }

    public string ExportPrivateKey(AsymmetricCipherKeyPair keyPair)
    {
        var pkcs8Generator = new Pkcs8Generator(keyPair.Private);
        var pemWriter = new PemWriter(new StringWriter());
        pemWriter.WriteObject(pkcs8Generator.Generate());
        pemWriter.Writer.Flush();
        return pemWriter.Writer.ToString();
    }

    public AsymmetricCipherKeyPair CreateKeyPair()
    {
        var keyGenerationParameters =
            new KeyGenerationParameters(SecureRandom.GetInstance("SHA256PRNG", true), RSAKeySize);

        var keyPairGenerator = new RsaKeyPairGenerator();
        keyPairGenerator.Init(keyGenerationParameters);
        return keyPairGenerator.GenerateKeyPair();
    }

    public string RsaEncryptWithPublic(string clearText, string publicKey)
    {
        var bouncyCastleCompliantPublicKey = publicKey
            .Replace("-----BEGIN RSA PUBLIC KEY-----", "-----BEGIN PUBLIC KEY-----")
            .Replace("-----END RSA PUBLIC KEY-----", "-----END PUBLIC KEY-----");
        var bytesToEncrypt = Encoding.UTF8.GetBytes(clearText);

        var encryptEngine = new OaepEncoding(new RsaEngine(), new Sha256Digest(), new Sha256Digest(), null);

        using (var textReader = new StringReader(bouncyCastleCompliantPublicKey))
        {
            var keyParameter = (AsymmetricKeyParameter) new PemReader(textReader).ReadObject();
            encryptEngine.Init(true, keyParameter);
        }

        var encrypted =
            Convert.ToBase64String(encryptEngine.ProcessBlock(bytesToEncrypt, 0, bytesToEncrypt.Length));
        return encrypted;
    }

    public string RSADecryptWithPrivate(string base64Input, string privateKey)
    {
        var bouncyCastleCompliantPrivateKey = privateKey
            .Replace("-----BEGIN RSA PRIVATE KEY-----", "-----BEGIN PRIVATE KEY-----")
            .Replace("-----END RSA PRIVATE KEY-----", "-----END PRIVATE KEY-----");
        var bytesToDecrypt = Convert.FromBase64String(base64Input);

        var decryptEngine = new OaepEncoding(new RsaEngine(), new Sha256Digest(), new Sha256Digest(), null);

        using (var textReader = new StringReader(bouncyCastleCompliantPrivateKey))
        {
            var keyPair = (RsaPrivateCrtKeyParameters) new PemReader(textReader).ReadObject();
            decryptEngine.Init(false, keyPair);
        }

        var decrypted = Encoding.UTF8.GetString(decryptEngine.ProcessBlock(bytesToDecrypt, 0, bytesToDecrypt.Length));
        return decrypted;
    }
}