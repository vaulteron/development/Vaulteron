﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using VaulteronDatabase.Models;
using VaulteronDatabase.Models.Enums;

namespace VaulteronDatabase.SeedData;

public class CompanyPasswordBuilder
{
    private readonly ICryptoHelper cryptoHelper;
    private readonly VaulteronContext vaulteronContext;
    private readonly Mandator mandator;
    private readonly List<SharedPassword> passwords = new();

    public CompanyPasswordBuilder(ICryptoHelper cryptoHelper, VaulteronContext vaulteronContext, Mandator mandator)
    {
        this.cryptoHelper = cryptoHelper;
        this.vaulteronContext = vaulteronContext;
        this.mandator = mandator;
    }

    public CompanyPasswordBuilder AddSyncedOfflinePassword(User owner, string name, string password = VaulteronSeedData.LoginPassword) =>
        AddCompanyPassword(owner, name, $"login of offline PW for{owner.Firstname}", password, false, true);

    public CompanyPasswordBuilder AddSimpleCompanyPassword(User owner, string name, string password = VaulteronSeedData.LoginPassword) =>
        AddCompanyPassword(owner, name, $"login for {owner.Firstname}", password);

    public CompanyPasswordBuilder AddSimpleArchivedCompanyPassword(User owner, string name, string password = VaulteronSeedData.LoginPassword) =>
        AddCompanyPassword(owner, name, $"login for {owner.Firstname}", password, true);

    public CompanyPasswordBuilder AddCompanyPassword(User owner, string name, string login, string password, bool archived = false, bool isOfflinePassword = false)
    {
        var timestampOfCreation = DateTime.UtcNow;

        var usersWithAccess = vaulteronContext.Users
            .Include(u => u.KeyPairs)
            .Where(u => u.Id == owner.Id || u.Admin)
            .ToList();

        // Now add the encryptedPassword for all users, but only once
        var encryptedPasswords = usersWithAccess
            .Select(u => u.KeyPairs.Last())
            .Select(keyPair => new EncryptedSharedPassword
            {
                EncryptedPasswordString = cryptoHelper.EncryptAsymmetrical(password, keyPair.PublicKeyString),
                KeyPair = keyPair,
            })
            .ToList();

        var newPassword = new SharedPassword
        {
            Name = name,
            Login = login,
            SecurityRating = SecurityRating.Dangerous,
            EncryptedSharedPasswords = encryptedPasswords,
            GroupId = null,
            WebsiteURL = "website.com",
            Notes = "",
            Mandator = mandator,
            CreatedBy = owner,
            SharedPasswordTags = new List<SharedPasswordTag>(),
            ArchivedAt = archived ? timestampOfCreation : null,
            IsSavedAsOfflinePassword = isOfflinePassword
        };
        newPassword.SetNewModifyLock();

        passwords.Add(newPassword);

        return this;
    }

    /// <summary>
    /// Creates the configured company passwords 
    /// </summary>
    /// <returns>The newly created CompanyPassword fetched from the DB</returns>
    public List<SharedPassword> Build()
    {
        try
        {
            // Now persist data
            vaulteronContext.SharedPasswords.AddRange(passwords);
            vaulteronContext.SaveChanges();
            return passwords;
        }
        catch (Exception e)
        {
            throw new SeedDataCreationException("Unable to create CompanyPasswords", e);
        }
    }
}