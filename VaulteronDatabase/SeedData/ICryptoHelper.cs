﻿namespace VaulteronDatabase.SeedData;

public interface ICryptoHelper
{
    string ComputePasswordHash(string value);

    #region symmetrical crypto

    public string EncryptSymmetrically(string plaintext, string loginPassword = VaulteronSeedData.LoginPassword);
    public string DecryptSymmetrically(string encryptedText, string loginPassword = VaulteronSeedData.LoginPassword);

    #endregion

    #region asymmetrical crypto

    public RSAKeyPair CreateKeyPair(string loginPassword = VaulteronSeedData.LoginPassword, bool getFromMockList = true);

    public string EncryptAsymmetrical(string plaintext, string publicKeyString);
    public string DecryptWithPrivate(string encryptedText, string privateKey);
    public string DecryptAsymmetrical(string encryptedText, string loginPassword, string encryptedPrivateKeyString);
    public string DecryptAsymmetrical(string encryptedText, string privateKey);

    #endregion
}