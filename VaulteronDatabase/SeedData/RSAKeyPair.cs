﻿namespace VaulteronDatabase.SeedData;

public class RSAKeyPair
{
    public string PrivateKey { get; }
    public string PublicKey { get; }
    public string EncryptedPrivateKey { get; }

    public RSAKeyPair(string publicKey, string encryptedPrivateKey, string privateKey)
    {
        PrivateKey = privateKey;
        PublicKey = publicKey;
        EncryptedPrivateKey = encryptedPrivateKey;
    }
}